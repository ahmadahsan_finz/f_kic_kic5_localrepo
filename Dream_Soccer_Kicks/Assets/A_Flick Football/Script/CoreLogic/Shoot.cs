﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;
using UnityEngine.SceneManagement;
using System.Linq;

public class Shoot : MonoBehaviour {

	public static Shoot share;

	public static Action EventShoot = delegate {};
    public static Action<float> EventChangeSpeedZ = delegate {};
    public static Action<float> EventChangeBallZ = delegate { };
    public static Action<float> EventChangeBallX = delegate { };
    public static Action<float> EventChangeBallLimit = delegate { };
    public static Action<Collision> EventOnCollisionEnter = delegate { };
 //   public static Action EventDidPrepareNewTurn = delegate { };

    public float _ballControlLimit;

	public Transform _goalKeeper;
	public Transform _ballTarget;
	protected Vector3 beginPos;
	protected bool _isShoot = false;

	public float minDistance = 100;		// 40f


	public Rigidbody _ball;
	public Rigidbody KickBallArea;

	public float factorUp = 0.012f;				// 10f
	public float factorDown = 0.003f;			// 1f
	public float factorLeftRight = 0.025f;		// 2f
    public float factorLeftRightMultiply = 0.8f;		// 2f
	public float _zVelocity = 24f;

	public AnimationCurve _curve;
	protected Camera _mainCam;

	protected float factorUpConstant = 0.02f * 960f;// 0.017f * 960f; 	// 0.015f * 960f;
	protected float factorDownConstant = 0.012f * 960f; // 0.006f * 960f; // 0.005f * 960f;
	protected float factorLeftRightConstant =  0.0235f * 640f; // 0.03f * 640f; // 0.03f * 640f;

	public Transform _ballShadow;


	public float _speedMin = 18f; // 20f;
	public float _speedMax = 30f;    // 36f;
	[SerializeField] float _incrementSpeed=2;


	[SerializeField]  float _distanceMinZ = 16.5f;
	[SerializeField]  float _distanceMaxZ = 35f;
	[SerializeField]  float _IncrementDistance=0;
	//[SerializeField]  float _IncrementDistanceMaxZ = 5;

	[SerializeField]  float _distanceMinX = -25f;
	[SerializeField]  float _distanceMaxX = 25f;

	public bool _isShooting = false;   
	public bool _canControlBall = false;

    public Transform _cachedTrans;

	public bool _enableTouch = false;
    public float screenWidth;
    public float screenHeight;

    Vector3 _prePos, _curPos;
    public float angle;
    protected ScreenOrientation orientation;

    protected Transform _ballParent;

    protected RaycastHit _hit;
    public bool _isInTutorial = false;
    public Vector3 ballVelocity;

    private float _ballPostitionZ = -22f;
    private float _ballPostitionX = 0f;

																					//new variable working 		//ahmad 
	public GameObject PenaltySpot;
	public static bool DartBoardHitBool;
	public GameObject[] DartboardArray;
	public GameObject[] DartboardArrayRewardShot;
	public GameObject[] BrickwallArray;
	public GameObject[] PillarsVariations;
	public GameObject[] Boxes;
	public GameObject[] DrumSet;
	public GameObject[] Circles1;
	public GameObject[] Circles2;
	public GameObject[] BlocksArray;
	public GameObject GoalPost;
	public GameObject GoalDetermineComponents;
	public static bool Circle2xHit;
	public float BallDistanceFromGoal_Zaxis;
	public float BallDistanceFromGoal_Xaxis;
	public string GameplaySceneName;
	public GameObject GoalKeeperObject;
	public static float CircleMultiplier=1;
	public static float GoalCircleNum=0;
	public static bool GoalCircle;
	public static bool TrampolineHitBool;
	public GameObject Player;
	public GameObject[] HurdleInstance;

	public GameObject ParentLevelHurdles;
	public GameObject BallRenderObject;
	public GameObject PoleGlow;
	public GameObject[] DefendersArray;
	public bool BallShot;
    public GameObject FloatingText;
    public GameObject FloatingText2;
    public GameObject DartBoardMoving;
    public GameObject[] DartBoardMovingPoint;
	public bool GoalScoredBool;

	public bool TouchonFootball;
	bool rayCastEmerge;

	[Header ("Reward Kick Component")]
	public GameObject RewardKickItemPanel;


	public float BallPositionZ
    {
        get { return _ballPostitionZ; }
        set { _ballPostitionZ = value; }
    }

    public float BallPositionX
    {
        get { return _ballPostitionX; }
        set { _ballPostitionX = value; }
    }
    public TrailRenderer _effect;
	private bool _isWallKick;
/*	public bool IsWallKick
	{
		get { return _isWallKick; }
		set
		{
			_isWallKick = true;
			Wall.share.IsWall = _isWallKick;
			if (_isWallKick)
			{
				Wall.share.setWall(Shoot.share._ball.transform.position);
			}
		}
	}

	*/
	protected virtual void Awake() {
		share = this;
		_cachedTrans = transform;
		_isShooting = true;
		_ballParent = _ball.transform.parent;

	    _distanceMinX = -15f;
	    _distanceMaxX = 15f;
	    _distanceMaxZ = 30f;

	}

	// Use this for initialization
	protected virtual void Start () {
//		Application.targetFrameRate = 30;
		_mainCam = CameraManager.share._cameraMainComponent;

#if UNITY_WP8 || UNITY_ANDROID
		Time.maximumDeltaTime = 0.2f;
		Time.fixedDeltaTime = 0.008f;
#else
		Time.maximumDeltaTime = 0.1f;
		Time.fixedDeltaTime = 0.005f;
#endif

		orientation = Screen.orientation;
		calculateFactors();

        //_ballControlLimit = 6f;
	    EventChangeBallLimit(_ballControlLimit);

	
		reset();
		CameraManager.share.reset();
	    GoalKeeper.share.reset();
		GoalDetermine.EventFinishShoot += goalEvent;
	}

	void OnDestroy() {
		GoalDetermine.EventFinishShoot -= goalEvent;
	}

	public virtual void goalEvent(bool isGoal, Area area) {
		_canControlBall = false;
		_isShooting = false;
		GoalScoredBool = isGoal;
		//disableTouch();
		//Debug.Log("touch disable");
		_enableTouch = false;
	}

	public void calculateFactors() {
		screenHeight = Screen.height;
		screenWidth = Screen.width;
		
		minDistance = (100 * screenHeight) / 960f;
		factorUp = factorUpConstant / screenHeight;
		factorDown = factorDownConstant / screenHeight;
		factorLeftRight = factorLeftRightConstant / screenWidth;

		/*Debug.Log("Orientation : " + orientation + "\t Screen height = " + screenHeight 
            + "\t Screen width = " + screenWidth + "\t factorUp = " + factorUp + "\t factorDown = " + factorDown 
            + "\t factorLeftRight = " + factorLeftRight + "\t minDistance = " + minDistance);*/
	}

    protected void LateUpdate()
    {
		if(screenHeight != Screen.height) {
			orientation = Screen.orientation;
			calculateFactors();
		    CameraManager.share.reset();
		}
	}
	void FixedUpdate() {
		ballVelocity = _ball.velocity;

		Vector3 pos = _ball.transform.position;
		pos.y = 0.015f;
		_ballShadow.position = pos;
	}

	protected virtual void Update() {
		if(_isShooting) {		// neu banh chua vao luoi hoac trung thu mon, khung thanh thi banh duoc phep bay voi van toc dang co
			if( _enableTouch && !_isInTutorial ) {
				
				if (Input.GetMouseButtonDown(0)) {			// touch phase began
					mouseBegin(Input.mousePosition);
				}
				else if( Input.GetMouseButton(0) ) {			
					mouseMove(Input.mousePosition);
				}
				else if(Input.GetMouseButtonUp(0)) {	// touch ended
					mouseEnd();
				}
			
			}
			if(_isShoot) {
				//	Debug.Log(" inside shoot.cs update");

				PlayerScript.instance.ApplyShootAnim();                     //player kick anim			//ahmad
			     
				if(GamePlayManager.FireBallActive)			//if fire is enabled we want one particle system to be disabled
                {
					//Shoot.share._effect.gameObject.SetActive(false);				//disable trail
					GamePlayManager.instance. FireParticle.SetActive(true);                       //Fire particles turned on  
					GamePlayManager.instance.FireParticle2.SetActive(false);                       //Fire particles turned on  
				}

				if(DefenderScript.instance!=null)
                {
					DefenderScript.instance.PlayJumpAnimation();				//defender Jump Animation
                }

				Vector3 speed = _ballParent.InverseTransformDirection(_ball.velocity);

				BallShot = true;

				_ball.velocity = _ballParent.TransformDirection(speed);
			

				BallRenderObject.transform.Rotate(0, 1000 * Time.deltaTime, 0, Space.Self); //added for ball rotation when shoot //ahmad

                /*if(GamePlayManager.instance.TutorialScreen)
                {
					GamePlayManager.instance.TutorialScreen.SetActive(false);

				}*/

                if (TutorialManager.instance.TutorialScreen)
                {
					TutorialManager.instance.TurnOffGameplayTutorial();

				}

			}


		}
       
	}

	public void mouseBegin(Vector3 pos) {
		if (Physics.Raycast(_mainCam.ScreenPointToRay(pos), out _hit, 100f) )
		{
			if (_hit.rigidbody == KickBallArea)
            {

	//		Debug.Log("RAYCAST mouseBegin ");
		//	Debug.DrawRay(_mainCam.ScreenPointToRay(pos).origin, _mainCam.ScreenPointToRay(pos).direction * 10f, Color.green);

			TouchonFootball = true;
			_prePos = _curPos = pos;
			beginPos = _curPos;
            }
			else if(_canControlBall == true)
            {
				//Debug.Log("RAYCAST mouseBegin  _canControlBall");

				_prePos = _curPos = pos;
				beginPos = _curPos;
			}
		}
	}

	public void mouseEnd() {
		TouchonFootball = false;
	///	Debug.Log("RAYCAST mouseEnd ");
		if (_isShoot == true) {      // neu da sut' roi` thi ko cho dieu khien banh nua, tranh' truong` hop nguoi choi tao ra nhung cu sut ko the~ do~ noi~

			//_canControlBall = false;
		

		}
	}

	public void mouseMove(Vector3 pos) {
		if(_curPos != pos) {		// touch phase moved
			_prePos = _curPos;
			_curPos = pos;

			//Debug.Log("RAYCAST mouseMove ");

			Vector3 distance = _curPos - beginPos;
			
			rayCastEmerge=Physics.Raycast(_mainCam.ScreenPointToRay(_curPos), out _hit, 100f);

			if(_isShoot == false) {             // CHUA SUT
												//Debug.Log("RAYCAST mouseMove 1");

				if (rayCastEmerge && !TouchonFootball)
                {
                    if (_hit.rigidbody == KickBallArea)//|| _hit.transform.tag.Equals("KickPlayer"))
                    {
                     //   Debug.Log("RAYCAST mouseMove 3 ");

                        TouchonFootball = true;
                    }
                }
                if (distance.y > 0 && distance.magnitude >= minDistance) {
				//	Debug.Log("RAYCAST mouseMove 2");

				//	Debug.DrawRay(_mainCam.ScreenPointToRay(_curPos).origin, _mainCam.ScreenPointToRay(_curPos).direction * 10f, Color.yellow);
				

					 if (rayCastEmerge && TouchonFootball) {
						if (_hit.rigidbody != KickBallArea)
						{
						//	Debug.Log("RAYCAST mouseMove 4");

							_isShoot = true;

							Vector3 point1 = _hit.point;        // contact point
							point1.y = 0;
							point1 = _ball.transform.InverseTransformPoint(point1);     // dua point1 ve he truc toa do cua ball, coi ball la goc toa do cho de~
							point1 -= Vector3.zero;         // vector tao boi point va goc' toa do

							Vector3 diff = point1;
							diff.Normalize();               // normalized rat' quan trong khi tinh' goc

							float angle = 90 - Mathf.Atan2(diff.z, diff.x) * Mathf.Rad2Deg;     // doi ra degree va lay 90 tru` vi nguoc
																								//								Debug.Log("angle = " + angle);

							float x = _zVelocity * Mathf.Tan(angle * Mathf.Deg2Rad);

							//							float x = distance.x * factorLeftRight;
							_ball.velocity = _ballParent.TransformDirection(new Vector3(x, distance.y * factorUp, _zVelocity));
							_ball.angularVelocity = new Vector3(0, x, 0f);

							if (EventShoot != null)
							{
								EventShoot();
							}
						}
					}
				}
			}
			else {				// da~ sut' roi`, tuy theo do lech cua touch frame hien tai va truoc do' ma se lam cho banh xoay' trai', phai~, len va xuong' tuong ung'
				if(_canControlBall == true) {	// neu nhac ngon tay len khoi man hinh roi thi ko cho dieu khien banh nua

				    if (_cachedTrans.position.z < -_ballControlLimit)
				    {
				        // neu banh xa hon khung thanh 6m thi moi' cho dieu khien banh xoay, di vo trong khoang cach 6m thi ko cho nua~ de~ lam cho game can bang`

				        distance = _curPos - _prePos;

				        Vector3 speed = _ballParent.InverseTransformDirection(_ball.velocity);
				        speed.y += distance.y*((distance.y > 0) ? factorUp : factorDown);
				        speed.x += distance.x * factorLeftRight * factorLeftRightMultiply;
				        _ball.velocity = _ballParent.TransformDirection(speed);

				        speed = _ball.angularVelocity;
				        speed.y += distance.x*factorLeftRight;
				        _ball.angularVelocity = speed;
				    }
				    else
				    {
				        _canControlBall = false;
				    }
				}
			}
		}
	}

    protected void OnCollisionEnter(Collision other)
    {
		string tag = other.gameObject.tag;
		if (tag.Equals("KickPlayer") || tag.Equals("Drums") || tag.Equals("WoodBarrel") || tag.Equals("Hurdles")|| tag.Equals("Trampoline") || tag.Equals("Player") || tag.Equals("RoadBlock") || tag.Equals("Obstacle") || tag.Equals("Net") || tag.Equals("Wall") || tag.Equals("Bricks") || tag.Equals("BoxesBase") || tag.Equals("Boxes")) {  // banh trung thu mon hoac khung thanh hoac da vao luoi roi thi ko cho banh bay voi van toc nua, luc nay de~ cho physics engine tinh' toan' quy~ dao bay
			_isShooting = false;
			if (tag.Equals("Wall") || tag.Equals("Drums") || tag.Equals("Hurdles"))
			{

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("PlasticHit");

				}
			}
			else if (tag.Equals("Trampoline") || tag.Equals("RoadBlock"))
			{
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("MetalHit");

				}
				TrampolineHitBool = true;
			}
			else if (tag.Equals("Boxes"))
			{

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
			}
			else if (tag.Equals("WoodBarrel"))
			{
				_ball.velocity /= 3f;

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("WoodBarrel");

				}
			}

			else if (tag.Equals("Obstacle"))
			{
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("BallHitBar");
				}
				if (GameConstants.GetMode() == 8)
				{
					Vector3 offset = new Vector3(0, 0.5f, 0);
					var Multiplier = Instantiate(FloatingText, Shoot.share._ball.transform.position, Quaternion.identity);           //floating score text popup 
					Multiplier.GetComponent<TextMesh>().text = 100.ToString();
					Multiplier.transform.position += offset;
				}
			}
			


				if (tag.Equals("Net") || tag.Equals("Bricks") || tag.Equals("Hurdles") || tag.Equals("BoxesBase") )
			{
				_ball.velocity /= 3f;
			}
		}
			EventOnCollisionEnter(other);
    }

    protected void OnTriggerEnter(Collider other)
    {
		//Debug.Log("OnTriggerEnter  ");
		string tag = other.gameObject.tag;
		string name = other.gameObject.name;
		if(GameConstants.GetGameMode()== "RewardShot2" && !GamePlayManager.instance.RewardKickItemPicked) 
        {
			
			if (name.Equals("Coin100"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 100;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;

			}
			else if (name.Equals("Coin150"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 150;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;

			}else if (name.Equals("Coin200"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 200;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;

			}
			else if (name.Equals("Coin250"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 250;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;

			}
			else if (name.Equals("Coin300"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 300;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Coin350"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 350;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Coin400"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 400;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Coin450"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 450;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Coin500"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddCoins(100);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.coin;
				GameConstants.RewardShotPassCoins = 500;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
		
			else if (name.Equals("Diamond10"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//	GenericVariables.AddDiamonds(10);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.diamond;
				GameConstants.RewardShotPassCoins = 10;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Diamond20"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.diamond;
				GameConstants.RewardShotPassCoins = 20;
				//GenericVariables.AddDiamonds(20);
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Diamond30"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;

				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.diamond;
				GameConstants.RewardShotPassCoins = 30;
				//GenericVariables.AddDiamonds(30);
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Diamond40"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddDiamonds(40);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.diamond;
				GameConstants.RewardShotPassCoins = 40;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;

				DartBoardHitBool = true;
			}
			else if (name.Equals("Diamond50"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddDiamonds(50);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.diamond;
				GameConstants.RewardShotPassCoins = 50;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;

				DartBoardHitBool = true;
			}
			else if (name.Equals("Fireball1"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddFire(1);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.fire;
				GameConstants.RewardShotPassCoins = 1;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;

				DartBoardHitBool = true;
			}
			else if (name.Equals("Fireball2"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddFire(2);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.fire;
				GameConstants.RewardShotPassCoins = 2;

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;

				DartBoardHitBool = true;
			}
			else if (name.Equals("Fireball4"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddFire(4);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.fire;
				GameConstants.RewardShotPassCoins = 4;

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;

				DartBoardHitBool = true;
			}
			else if (name.Equals("Fireball6"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
				//GenericVariables.AddFire(6);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.fire;
				GameConstants.RewardShotPassCoins = 6;

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Energy1"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddEnergy(1);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.energy;
				GameConstants.RewardShotPassCoins = 1;

				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Energy2"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddEnergy(2);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.energy;
				GameConstants.RewardShotPassCoins = 2;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Energy3"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddEnergy(3);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.energy;
				GameConstants.RewardShotPassCoins = 3;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Energy4"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddEnergy(4);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.energy;
				GameConstants.RewardShotPassCoins = 4;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
			else if (name.Equals("Energy5"))
			{
				GamePlayManager.instance.RewardKickItemPicked = true;
			//	GenericVariables.AddEnergy(5);
				RewardShotEndMenu.reward = RewardShotEndMenu.RewardState.energy;
				GameConstants.RewardShotPassCoins = 5;
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

				}
				_isShooting = false;
				DartBoardHitBool = true;
			}
		}
        else
        {
			if (tag.Equals("DartBoard"))
			{
				if (SoundManagerNew.Instance != null)
				{
					SoundManagerNew.Instance.playSound("Woodbox");

			}
			_isShooting = false;
			DartBoardHitBool = true;
		}
		else if (tag.Equals("RewardKey"))
		{
			if (SoundManagerNew.Instance != null)
			{
				SoundManagerNew.Instance.playSound("ExtraBall");
			}
			RewardkeyController.instance.KeyCollected();
		}
		else if (tag.Equals("Circles"))
		{
			other.gameObject.SetActive(false);
			CircleMultiplier = CircleMultiplier + 1;

			Vector3 offset = new Vector3(0, 1.2f, 0);
			var Multiplier = Instantiate(FloatingText, other.gameObject.transform.position, Quaternion.identity);           //2x 3x 4x floating text popup 
			Multiplier.GetComponent<TextMesh>().text = CircleMultiplier.ToString() + "x";
			Multiplier.transform.position += offset;
		}
		else if (tag.Equals("GoalCircle"))
		{
			if (SoundManagerNew.Instance != null)
			{
				SoundManagerNew.Instance.playSound("CirclePass");
			}
			other.gameObject.SetActive(false);
			//CircleMultiplier = CircleMultiplier + 1;
			if(GameConstants.GetMode()==32 || GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34)
            {
				GoalCircleNum += 1;
				if(GoalCircleNum==2)		//ball gone through second hoop aswell
				{
					GoalCircle = true;

				}
                else
                {
					GoalCircle = false;

				}
			}
            else
            {
				GoalCircle = true;
			}
			
		}
		else if (tag.Equals("ExtraBall"))
		{
			if (SoundManagerNew.Instance != null)
			{
				SoundManagerNew.Instance.playSound("ExtraBall");

			}
			GameConstants.hitsRemaining++;
			HudManager.instance.UpdateKickRemaining("Increment");

			Vector3 offset = new Vector3(0, 3.5f, 0);
			var Multiplier = Instantiate(FloatingText, Shoot.share._ball.transform.position, Quaternion.identity);           //floating score text popup 
																															 //Multiplier.GetComponent<TextMesh>().fontSize = 150;
			Multiplier.GetComponent<TextMesh>().color = Color.green;
			Multiplier.GetComponent<TextMesh>().text = "EXTRA BALL";
			Multiplier.transform.position = offset;


			Vector3 offset2 = new Vector3(0, 1.2f, 0);
			var ExtraBallNumberText = Instantiate(FloatingText,  gameObject.transform.position, Quaternion.identity);           //floating score text popup 
																																//Multiplier.GetComponent<TextMesh>().fontSize = 150;
			ExtraBallNumberText.GetComponent<TextMesh>().color = Color.green;
			ExtraBallNumberText.GetComponent<TextMesh>().text = "+1";
			ExtraBallNumberText.transform.position += offset2;

		//	ExtraBallNumberText.transform.parent = HudManager.instance.UI_Canvas;

			iTween.MoveTo(ExtraBallNumberText, iTween.Hash("position",new Vector3(15,10,0), "time", 3f, "easetype", iTween.EaseType.linear));

		//	HudManager.instance.KickPanel.GetComponent<Animation>().Play();

				ExtraBallScoreMultiScript.instance.ExtraBallBool = false;
				ExtraBallScoreMultiScript.instance.ExtraBallBoolHit = true;
				Destroy(other.gameObject);
			}

		}

	}

	private void enableEffect() {
		_effect.enabled = true;
		//_effect.time = 1;
		_effect.time = 0.3f;
	}

	/*mode 1= penalties																						//ahmad
        mode 2= simple freekicks
        mode 3= freekicks with dartboard only/bullseye
        mode 4= freekicks with wall
        mode 5= freekicks with wall  and dartboard/bullseye
        mode 6= freekicks with boxes
        mode 7= Goal Spot
        mode 8= Hit the pole 
        mode 9= freekicks with hoops/2x 3x circles
        mode 10= Tournament
        mode 11= freekicks with wall and hoops/2x 3x circles
        mode 12- Freekicks with dummy wall
        mode 13= freekicks with dummy wall and bullseye
        mode 14= freekicks with wood logs
        mode 15: freekicks with RoadblockA
        mode 16: freekicks with RoadblockB and moving defender
        mode 17: freekicks with Hoops
        mode 18: freekicks with Drums
        mode 19: freekicks with two Moving Defender
        mode 20: freekicks with pillars in goal and moving dummy no GK
        mode 21: Freekick directional pillars  and GK
        mode 22: Cones and wooden barrels
        mode 23: Freekick with One Static Dummy
        mode 24: Freekick with One Static Dummy and one moving dummy
        mode 25: Freekick with pillars in goal no GK
        mode 26: Freekick with directional pillars and no GK
        mode 27: Freekick with multiple Dartboards
        mode 28: Freekick with single Dartboard and moving dummy
        mode 29: Freekick with single Goal through Circle  No GK
        mode 30: Freekick with Single Goal through Circle with GK
        mode 31: Freekick with Single Goal through Circle with GK + Moving dummy
        mode 32: Freekick with two Goal through Circle No GK
        mode 33: Freekick with two Goal through Circle + GK 
        mode 34: Freekick with two Goal through Circle + GK + Moving Dummy 
        mode 35: Freekicks with cones variation 
        mode 36: Freekicks with cones variation and moving dummy
        mode 37: Freekicks with cones variation + GK
        mode 38: Freekicks with cones variation and moving dummy + GK
        mode 39: Freekicks with two Human Defenders + GK
        mode 40: Freekicks with Pillars Simple + No GK
        mode 41: Freekicks with DummyDef and Dartboard + No GK
        mode 42: Freekicks with Moving dartboard + GK (BOSS)
        mode 43: Freekicks with Coins + No GK (BONUS LEVEL) (Not Using)
        mode 44: Freekicks with Coins + No GK (BONUS LEVEL)
        mode 45: Freekicks with Trampoline + No GK + no def (BOSS)
        mode 46: Freekicks with Two Moving Rail Defender + GK (BOSS)
        mode 47: Freekicks with Trampoline + No GK + def (BOSS)
		mode 48: Freekicks with Block moving variation 1
		mode 49: Freekicks with Block moving variation 2


	  */
	public virtual void reset() {


		KickAreaController.instance.EnableKickArea();

		BallShot = false;
		TouchonFootball = false; //used in update

		Scene scene = SceneManager.GetActiveScene();
	//	_effect.gameObject.SetActive(true);    //disable trail

		//	Debug.Log(" LevelEndBool = " + GamePlayManager.instance.LevelEndBool);
		//	Debug.Log(" MissionEndBool = " + GamePlayManager.instance.MissionEndBool);
		if (scene.name == GameplaySceneName && (!GamePlayManager.instance.LevelEndBool && !GamePlayManager.instance.MissionEndBool && !GamePlayManager.instance.RewardShotEndBool))
		{
			if (GameConstants.GetGameMode() == "Classic")
			{
				//GoalKeeperLevel.share.setLevel(4);

				//		Debug.Log("shoot.cs gamemode is CLassic");
				BallPositionResetterAccordingToMode();
			}
			else if (GameConstants.GetGameMode() == "RewardShot")
			{
				BallPositionSetterRewardShot();
			}else if (GameConstants.GetGameMode() == "RewardShot2")
			{
				BallPositionSetterRewardShot2();
			}
			else if (GameConstants.GetGameMode() == "Multiplayer2")
			{
				BallPositionSetterMultiplayer2();
			}
            else
			{                       //incase of missions and multiplayer1

				//		Debug.Log("shoot.cs gamemode is Mission");
				//GoalKeeperLevel.share.setLevel(4);

				BallPositionSetterMissionandMultiplayer1();  //mission and multiplayer using this

			}
			GamePlayManager.instance.CelebrationParticles.SetActive(false);
			if (MenuManager.Instance.LoadingScreen.activeSelf == false && !GamePlayManager.instance.LevelEndBool && !GamePlayManager.instance.MissionEndBool && !GamePlayManager.instance.RewardShotEndBool)
			{
			
				PlayerScript.instance.ReloadShootAnim();
			}
			HudManager.instance.UpdateDistance();
		}
		else
        {
			//reset(-Random.Range(_distanceMinX, _distanceMaxX), -Random.Range(_distanceMinZ, _distanceMaxZ));
		}

	}
	public void BallPositionSetterRewardShot()
    {
		 if (GameConstants.GetRewardShotMode() == 1)               //working for Freekick and dartboard + no GK //ahmad
		{
			_distanceMaxZ = 18f;
			_distanceMinZ = 17f;
			_distanceMinX = -5f;
			_distanceMaxX = 5f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			GoalKeeperTurnOff();            //turn off Gk

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdleInstanceDestroyer();

			DartBoardHitBool = false;
			for (int i = 0; i < DartboardArray.Length; i++)
			{
				//	Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}

			HurdleInstance[0] = Instantiate(DartboardArray[Random.Range(0, 6)]);        //no corner dartboard
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
		//	Vector3 offset =new Vector3(0,0.5f,0);
			HurdleInstance[0].transform.localPosition = new Vector3 (HurdleInstance[0].transform.localPosition.x, - 0.5f, HurdleInstance[0].transform.localPosition.z);
			HurdleInstance[0].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
			HurdleInstance[0].SetActive(true);
			//DartboardArray[Random.Range(0, 8)].SetActive(true);
			//GameObject aa= Instantiate(Resources.Load("Target_CircleA (4)")) as GameObject;

			//Debug.Log("inside reset GetMode() 3");

		}
	}

	public void BallPositionSetterRewardShot2()
	{
		
			_distanceMaxZ = 18f;
			_distanceMinZ = 17f;
			_distanceMinX = -1f;
			_distanceMaxX = 1f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			GoalKeeperTurnOff();            //turn off Gk

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdleInstanceDestroyer();

            //DartBoardHitBool = false;
            //for (int i = 0; i < DartboardArray.Length; i++)
            //{
            //    //	Debug.Log("inside reset For Loop");
            //    DartboardArray[i].SetActive(false);
            //}


            HurdleInstance[0] = Instantiate(RewardKickItemPanel);        //no corner dartboard
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			//	Vector3 offset =new Vector3(0,0.5f,0);

			//HurdleInstance[0].transform.localPosition = new Vector3(HurdleInstance[0].transform.localPosition.x, -0.5f, HurdleInstance[0].transform.localPosition.z);
			//HurdleInstance[0].transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
			//
            HurdleInstance[0].SetActive(true);

			//DartboardArray[Random.Range(0, 8)].SetActive(true);
			//GameObject aa= Instantiate(Resources.Load("Target_CircleA (4)")) as GameObject;

			//Debug.Log("inside reset GetMode() 3");

		
	}
	public void BallPositionSetterMissionandMultiplayer1()     //mission and multiplayer using this
	{
		BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
		BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

		reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
	}
	public void BallPositionSetterMultiplayer2()     //mission and multiplayer using this
	{
		_distanceMaxZ = 15f;
		_distanceMinZ = 15f;
		_distanceMinX = -0f;
		_distanceMaxX = 0f;
		BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
		BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

		reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
	}
    public void BallPositionResetterAccordingToMode()
    {
		//StopAllCoroutines();
		if (GamePlayManager.FireBallActive)                      //to restore previous difficulty back on GK after fireball
		{
			GamePlayManager.instance.restoreGKDifficulty();
		}

		if (GameConstants.GetMode() == 1)               //working for first level or penalty level //ahmad
		{
			_distanceMaxZ = 18f;
			_distanceMinZ = 16f;
			_distanceMinX = -2f;
			_distanceMaxX = 2f;

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			//			reset(PenaltySpot.transform.position.x, PenaltySpot.transform.position.z);
			//			BallDistanceFromGoal_Zaxis = -PenaltySpot.transform.position.z;

		}
		else if (GameConstants.GetMode() == 2)   //working for Freekick with keeper only //ahmad
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -10f;
			_distanceMaxX = 10f;
			

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ + _IncrementDistance, _distanceMaxZ + _IncrementDistance);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			if (GoalScoredBool)
			{
				_IncrementDistance += 2;
				_speedMin += 1 ;              //changing for long distance shot
				_speedMax += 1 ;
			}
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			//	Player.transform.position = new Vector3(BallDistanceFromGoal_Xaxis - 1.1f, 0, BallDistanceFromGoal_Zaxis - 5f);


		}
		else if (GameConstants.GetMode() == 3)               //working for Freekick and dartboard + no GK //ahmad
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -10f;
			_distanceMaxX = 10f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			GoalKeeperTurnOff();            //turn off Gk

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdleInstanceDestroyer();

			DartBoardHitBool = false;
			for (int i = 0; i < DartboardArray.Length; i++)
			{
				//	Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}

			HurdleInstance[0] = Instantiate(DartboardArray[Random.Range(0, 6)]);        //no corner dartboard
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);
			//DartboardArray[Random.Range(0, 8)].SetActive(true);
			//GameObject aa= Instantiate(Resources.Load("Target_CircleA (4)")) as GameObject;

			//Debug.Log("inside reset GetMode() 3");

		}
		else if (GameConstants.GetMode() == 4)               //working for Freekick and wall //ahmad
		{
			//	Debug.Log("reset inside shoot.cs  GetMode() == 3");
			Wall.share.IsWall = true;
			//	Debug.Log("Wall.share.IsWall =  " + Wall.share.IsWall + "shoot.cs");

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			if (Wall.share != null)                     // if there is wall in this scene
			{
				//		Debug.Log("Wall.share != null");
				Wall.share.IsWall = true;         // set is wall state
				if (true)                         // if we want wall kick
				{
					//			Debug.Log("Wall.share != null and if (true) ");
					Wall.share.setWall(Shoot.share._ball.transform.position);       // set wall position with respect to ball position
				}
			}

		}
		else if (GameConstants.GetMode() == 5)               //working for Freekick, wall and dartboard //ahmad
		{

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			Wall.share.IsWall = true;
			if (Wall.share != null)                     // if there is wall in this scene
			{
				//	Debug.Log("Wall.share != null");
				Wall.share.IsWall = true;         // set is wall state
				if (true)                         // if we want wall kick
				{
					//		Debug.Log("Wall.share != null and if (true) ");
					Wall.share.setWall(Shoot.share._ball.transform.position);       // set wall position with respect to ball position
				}
			}

			HurdleInstanceDestroyer();

			DartBoardHitBool = false;
			for (int i = 0; i < DartboardArray.Length; i++)
			{
				//	Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}

			HurdleInstance[0] = Instantiate(DartboardArray[Random.Range(0, 10)]);           //all position dartboards
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;

			HurdleInstance[0].SetActive(true);

		}
		else if (GameConstants.GetMode() == 6)     //working for Freekick with boxes
		{
			_distanceMinX = -2f;
			_distanceMaxX = 2f;
			_distanceMinZ = 45f;
			_distanceMaxZ = 50f;
			_speedMin = 33f;              //changing for long distance shot
			_speedMax = 37f;
			//Boxes[0].SetActive(true);
			//Boxes[1].SetActive(true);
			/*	for (int i = 0; i < HurdleInstance.Length; i++)
				{
					if (HurdleInstance[i] != null)
					{
						//HurdleInstance.SetActive(false);
						Destroy(HurdleInstance[i]);
					}
				}*/
			/*if (GamePlayManager.instance.LevelRestart)
			{
				HurdleInstanceDestroyer();
				GamePlayManager.instance.LevelRestart = false;
			}*/
			//if (HurdleInstance[0] == null && HurdleInstance[1] == null)
			if (GamePlayManager.instance.LevelRestart || (HurdleInstance[0] == null && HurdleInstance[1] == null))
			{
				HurdleInstanceDestroyer();
				GamePlayManager.instance.LevelRestart = false;

				HurdleInstance[0] = Instantiate(Boxes[0]);
				HurdleInstance[1] = Instantiate(Boxes[1]);
				HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
				HurdleInstance[1].transform.parent = ParentLevelHurdles.transform;
				HurdleInstance[0].SetActive(true);
				HurdleInstance[1].SetActive(true);
			}
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 7)     //working for Freekick bricks
		{
			_distanceMaxZ = 20f;


			GoalKeeperTurnOff();            //turn off Gk

			HurdleInstanceDestroyer();


			HurdleInstance[0] = Instantiate(BrickwallArray[Random.Range(0, 6)]);                     //BRICKWALL INSTANTIATION
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 8)     //working for Hit the pole
		{
			_distanceMaxZ = 20f;

			GoalKeeperTurnOff();            //turn off Gk

			HurdleInstanceDestroyer();

			GoalPost.SetActive(false);
			GoalDetermineComponents.SetActive(false);

			HurdleInstance[0] = Instantiate(PoleGlow);
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);


			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 9)          //working for circles 2x 3x 4x
		{
			_distanceMaxZ = 30f;
			_distanceMinX = -3f;
			_distanceMaxX = 3f;

			/*for (int i = 0; i < 3; i++)                     //random circles from array
			{
				Circles1[i].SetActive(false);
				Circles2[i].SetActive(false);
				foreach (Transform child in Circles1[i].transform)
				{
					child.gameObject.SetActive(true);
				}
				foreach (Transform child in Circles2[i].transform)
				{
					child.gameObject.SetActive(true);
				}
			}*/

			HurdleInstanceDestroyer();

			//	Circles1[Random.Range(0, 3)].SetActive(true);
			//	Circles2[Random.Range(0, 3)].SetActive(true);

			/*	HurdleInstance = Instantiate(Circles1[Random.Range(0, 3)]);                     //CIRCLES INSTANTIATION
				HurdleInstance2 = Instantiate(Circles2[Random.Range(0, 3)]);
				HurdleInstance.transform.parent = ParentLevelHurdles.transform;
				HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
				HurdleInstance.SetActive(true);
				HurdleInstance2.SetActive(true);
			*/
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 

		}
		else if (GameConstants.GetMode() == 11)         //working for circles 2x 3x 4x with defender wall
		{
			_distanceMaxZ = 30f;
			_distanceMinZ = 25f;
			_distanceMinX = -3f;
			_distanceMaxX = 3f;

			HurdleInstanceDestroyer();

			//	Circles1[Random.Range(0, 3)].SetActive(true);
			//	Circles2[Random.Range(0, 3)].SetActive(true);

			/* 
			HurdleInstance = Instantiate(Circles1[Random.Range(0, 3)]);         //CIRCLES INSTANTIATION
			HurdleInstance2 = Instantiate(Circles2[Random.Range(0, 3)]);
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
			*/

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 

			if (Wall.share != null)                     // if there is wall in this scene
			{

				Wall.share.IsWall = true;         // set is wall state
				if (true)                         // if we want wall kick
				{
					Wall.share.setWall(Shoot.share._ball.transform.position);       // set wall position with respect to ball position
				}
			}

		}
		else if (GameConstants.GetMode() == 12)
		{

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			Wall.share.IsWall = true;
			if (Wall.share != null)                     // if there is wall in this scene
			{
				Wall.share.IsWall = true;         // set is wall state
				if (true)                         // if we want wall kick
				{
					Wall.share.setWall(Shoot.share._ball.transform.position);       // set wall position with respect to ball position
				}
			}

		}
		else if (GameConstants.GetMode() == 13)
		{

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			Wall.share.IsWall = true;
			if (Wall.share != null)                     // if there is wall in this scene
			{
				Wall.share.IsWall = true;         // set is wall state
				if (true)                         // if we want wall kick
				{
					Wall.share.setWall(Shoot.share._ball.transform.position);       // set wall position with respect to ball position
				}
			}

			HurdleInstanceDestroyer();

			DartBoardHitBool = false;
			for (int i = 0; i < DartboardArray.Length; i++)
			{
				Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}

			HurdleInstance[0] = Instantiate(DartboardArray[Random.Range(0, 8)]);
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

		}
		else if (GameConstants.GetMode() == 14 || GameConstants.GetMode() == 15 || GameConstants.GetMode() == 17)
		{
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 18)     //working for Freekick with boxes
		{
			_distanceMinX = -2f;
			_distanceMaxX = 2f;
			_distanceMinZ = 45f;
			_distanceMaxZ = 50f;
			_speedMin = 32f;              //changing for long distance shot
			_speedMax = 35f;
			/*for (int i = 0; i < HurdleInstance.Length; i++)
			{
				if (HurdleInstance[i] != null)
				{
					//HurdleInstance.SetActive(false);
					Destroy(HurdleInstance[i]);
				}
			}*/
			if (GamePlayManager.instance.LevelRestart || (HurdleInstance[0] == null && HurdleInstance[1] == null))
			{
				HurdleInstanceDestroyer();
				GamePlayManager.instance.LevelRestart = false;

				HurdleInstance[0] = Instantiate(DrumSet[0]);
				HurdleInstance[1] = Instantiate(DrumSet[1]);
				HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
				HurdleInstance[1].transform.parent = ParentLevelHurdles.transform;
				HurdleInstance[0].SetActive(true);
				HurdleInstance[1].SetActive(true);
			}
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 19 || GameConstants.GetMode() == 16)     //working for Freekick with moving dummy
		{
			_distanceMinX = -10f;
			_distanceMaxX = 10f;
			_distanceMinZ = 20f;
			_distanceMaxZ = 22f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 

		}
		else if (GameConstants.GetMode() == 20)     //working for Freekick Pillars and moving dummy
		{
			_distanceMinX = -1f;
			_distanceMaxX = 1f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;


			GoalKeeperTurnOff();            //turn off Gk

			HurdleInstanceDestroyer();

			HurdleInstance[0] = Instantiate(PillarsVariations[Random.Range(0, 9)]);                     //Pillars INSTANTIATION
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 21)     //working for Freekick pillar and GK
		{
			_distanceMinX = 0f;
			_distanceMaxX = 0f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;

			HurdleInstanceDestroyer();


			HurdleInstance[0] = Instantiate(PillarsVariations[Random.Range(9, 17)]);                     //Pillars INSTANTIATION
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 22)     //cones and wooden barrels
		{
			_distanceMinX = 0f;
			_distanceMaxX = 0f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 

		}
		else if (GameConstants.GetMode() == 23 || GameConstants.GetMode() == 24)     //Static dummy and moving dummy
		{
			_distanceMinX = -5f;
			_distanceMaxX = 5f;
			_distanceMinZ = 20f;
			_distanceMaxZ = 21f;

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 

		}
		else if (GameConstants.GetMode() == 25)     //working for Freekick Pillars 
		{
			_distanceMinX = 0f;
			_distanceMaxX = 0f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 19f;


			GoalKeeperTurnOff();            //turn off Gk

			HurdleInstanceDestroyer();


			HurdleInstance[0] = Instantiate(PillarsVariations[Random.Range(0, 9)]);                     //Pillars INSTANTIATION
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			//HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 26)     //working for Freekick directional pillar 
		{
			_distanceMinX = 0f;
			_distanceMaxX = 0f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;


			GoalKeeperTurnOff();            //turn off Gk

			HurdleInstanceDestroyer();

			HurdleInstance[0] = Instantiate(PillarsVariations[Random.Range(9, 17)]);                     //Pillars INSTANTIATION
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 27)               //working for Freekick Mutliple dartboard //ahmad
		{
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			GoalKeeperTurnOff();            //turn off Gk

			HurdleInstanceDestroyer();


			DartBoardHitBool = false;
			for (int i = 0; i < 10; i++)
			{
				//	Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}
			for (int i = 0; i < HurdleInstance.Length; i++)
			{
				HurdleInstance[i] = Instantiate(DartboardArray[i]);
				HurdleInstance[i].transform.parent = ParentLevelHurdles.transform;
				HurdleInstance[i].SetActive(true);
			}
			/*HurdleInstance = Instantiate(DartboardArray[Random.Range(0, 2)]);
		HurdleInstance2 = Instantiate(DartboardArray[Random.Range(2, 4)]);
		HurdleInstance3 = Instantiate(DartboardArray[Random.Range(4, 6)]);
		HurdleInstance4 = Instantiate(DartboardArray[Random.Range(6, 8)]);
		HurdleInstance5 = Instantiate(DartboardArray[Random.Range(8, 10)]);
		HurdleInstance.transform.parent = ParentLevelHurdles.transform;
		HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
		HurdleInstance3.transform.parent = ParentLevelHurdles.transform;
		HurdleInstance4.transform.parent = ParentLevelHurdles.transform;
		HurdleInstance5.transform.parent = ParentLevelHurdles.transform;
		HurdleInstance.SetActive(true);
		HurdleInstance2.SetActive(true);
		HurdleInstance3.SetActive(true);
		HurdleInstance4.SetActive(true);
		HurdleInstance5.SetActive(true);*/

			//DartboardArray[Random.Range(0, 8)].SetActive(true);
			//GameObject aa= Instantiate(Resources.Load("Target_CircleA (4)")) as GameObject;

			//Debug.Log("inside reset GetMode() 3");

		}
		else if (GameConstants.GetMode() == 28)               //working for Freekick single dartboard + GK + Moving dummy  //ahmad
		{
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdleInstanceDestroyer();

			DartBoardHitBool = false;
			for (int i = 0; i < 8; i++)
			{
				//	Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}

			HurdleInstance[0] = Instantiate(DartboardArray[Random.Range(0, 10)]);
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 


		}
		else if (GameConstants.GetMode() == 29)         //goal through circle + No GK 
		{
			_distanceMinX = -15f;
			_distanceMaxX = 15f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 19f;
			/*BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ + _IncrementDistance, _distanceMaxZ + _IncrementDistance);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			Debug.Log("GoalScoredBool= " + GoalScoredBool + " GoalCircle" + GoalCircle);
			if (GoalScoredBool && GoalCircle)
			{
				_IncrementDistance += 2;
				_speedMin += 1;              //changing for long distance shot
				_speedMax += 1;
			}*/
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);


			GoalKeeperTurnOff();            //turn off Gk

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 30)         //goal through circle + GK 
		{
			_distanceMinX = -15f;
			_distanceMaxX = 15f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 19f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 31)     //goal through circle + GK  +moving dummy
		{
			_distanceMinX = -10f;
			_distanceMaxX = 10f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 32 || GameConstants.GetMode() == 35)         //goal through circle + No GK //goal through two circle + No GK //goal through cones + No GK
		{
			_distanceMinX = -2f;
			_distanceMaxX = 2f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);


			GoalKeeperTurnOff();            //turn off Gk

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34 || GameConstants.GetMode() == 37 || GameConstants.GetMode() == 38)     //goal through circle + GK   //goal through circle + GK +moving dummy   //cones + GK	//cones + GK + moving dummy 
		{
			_distanceMinX = -2f;
			_distanceMaxX = 2f;
			_distanceMinZ = 18f;
			_distanceMaxZ = 22f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 35 || GameConstants.GetMode() == 36)                         //cones + No GK	//cones + No GK + moving dummy 
		{
			_distanceMinX = -20f;
			_distanceMaxX = 20f;
			_distanceMinZ = 20f;
			_distanceMaxZ = 21f;
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			GoalKeeperTurnOff();            //turn off Gk
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 39)				//Freekicks with two Human Defenders + GK
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -15f;
			_distanceMaxX = 15f;

			HurdleInstanceDestroyer();

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ + _IncrementDistance, _distanceMaxZ + _IncrementDistance);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			if (GoalScoredBool)
			{
				_IncrementDistance += 2;
				_speedMin += 1;              //changing for long distance shot
				_speedMax += 1;
			}

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 40)         //working For Freekicks with simple pillars
		{
			_distanceMaxZ = 25f;
			_distanceMinZ = 24f;
			_distanceMinX = -10f;
			_distanceMaxX = 10f;
			GoalKeeperTurnOff();
			HurdleInstanceDestroyer();

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 41)         //Freekicks with Pillar and Dartboard + No GK
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -20f;
			_distanceMaxX = 20f;
			GoalKeeperTurnOff();
			HurdleInstanceDestroyer();

			DartBoardHitBool = false;
			for (int i = 0; i < DartboardArray.Length; i++)
			{
				//	Debug.Log("inside reset For Loop");
				DartboardArray[i].SetActive(false);
			}

			HurdleInstance[0] = Instantiate(DartboardArray[Random.Range(0, 6)]);        //no corner dartboard
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 42)         //Freekicks with Moving dartboard + GK
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -20f;
			_distanceMaxX = 20f;
		
			HurdleInstanceDestroyer();

			DartBoardHitBool = false;

			int rand=Random.Range(0, 4);
			HurdleInstance[0] = Instantiate(DartBoardMoving);        //no corner dartboard
			HurdleInstance[0].transform.position = DartBoardMovingPoint[rand].transform.position;
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			if(rand==0)
            {
				rand = 1;
            }
			else if(rand == 1)

			{
				rand = 0;
            }
			else if (rand == 2)

			{
				rand = 3;
			}
			else if (rand == 3)

			{
				rand = 2;
			}
			iTween.MoveTo(HurdleInstance[0], iTween.Hash("position", DartBoardMovingPoint[rand].transform.position, "time", 4f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 2f));

		}
		else if (GameConstants.GetMode() == 44)               //working for Bonus level 
		{
			_distanceMaxZ = 25f;
			_distanceMinZ = 24f;
			_distanceMinX = 0f;
			_distanceMaxX = 0f;
			_speedMin = 28f;              //changing for long distance shot
			_speedMax = 30f;
			GoalKeeperTurnOff();
			HurdleInstanceDestroyer();

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 

			//			reset(PenaltySpot.transform.position.x, PenaltySpot.transform.position.z);
			//			BallDistanceFromGoal_Zaxis = -PenaltySpot.transform.position.z;

		}
		else if (GameConstants.GetMode() == 45)         //Freekicks Coins + No GK
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -20f;
			_distanceMaxX = 20f;
			GoalKeeperTurnOff();
			HurdleInstanceDestroyer();

		
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 46)         //Freekicks with Two Moving Rail Defender + GK (BOSS)
		{
			_distanceMaxZ = 22f;
			_distanceMinZ = 18f;
			_distanceMinX = -20f;
			_distanceMaxX = 20f;
			HurdleInstanceDestroyer();

		
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

			HurdlesSetter.instance.SetHurdles(Shoot.share._ball.transform.position);        //set hurdles 
		}
		else if (GameConstants.GetMode() == 48)         //Freekicks with block variation 1
		{
			_distanceMaxZ = 24f;
			_distanceMinZ = 22f;
			_distanceMinX = -1f;
			_distanceMaxX = 1f;
			
			_speedMin = 28f;              //changing for long distance shot
			_speedMax = 33f;
			HurdleInstanceDestroyer();

			HurdleInstance[0] = Instantiate(BlocksArray[0]);        
/*			HurdleInstance[0].transform.position = DartBoardMovingPoint[rand].transform.position;
			HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;*/
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ + _IncrementDistance, _distanceMaxZ+ _IncrementDistance);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			if(GoalScoredBool)
            {
			_IncrementDistance +=2;
				_speedMin += 1;              //changing for long distance shot
				_speedMax += 1;
			}
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		else if (GameConstants.GetMode() == 49)         //Freekicks with block variation 1
		{
			_distanceMaxZ = 24f;
			_distanceMinZ = 22f;
			_distanceMinX = -1f;
			_distanceMaxX = 1f;
			_speedMin = 28f;              //changing for long distance shot
			_speedMax = 33f;
			HurdleInstanceDestroyer();
			HurdleInstance[0] = Instantiate(BlocksArray[1]);
			/*			HurdleInstance[0].transform.position = DartBoardMovingPoint[rand].transform.position;
						HurdleInstance[0].transform.parent = ParentLevelHurdles.transform;*/
			HurdleInstance[0].SetActive(true);

			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ + _IncrementDistance, _distanceMaxZ + _IncrementDistance);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);
			if (GoalScoredBool)
			{
				_IncrementDistance += 2;
				_speedMin += 1;              //changing for long distance shot
				_speedMax += 1;
			}
			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);

		}
		
		else
		{
			//			Debug.Log("reset else inside shoot.cs");
			BallDistanceFromGoal_Zaxis = Random.Range(_distanceMinZ, _distanceMaxZ);
			BallDistanceFromGoal_Xaxis = Random.Range(_distanceMinX, _distanceMaxX);

			reset(-BallDistanceFromGoal_Xaxis, -BallDistanceFromGoal_Zaxis);
		}
	}
    public virtual void reset(float x, float z)
	{
	//   Debug.Log(string.Format("<color=#c3ff55>Reset Ball Pos, x = {0}, z = {1}</color>", x, z));

		_effect.time = 0;
//		_effect.enabled = false;
		Invoke("enableEffect", 0.1f);

		BallPositionX = x;
        EventChangeBallX(x);
		BallPositionZ = z;
	    EventChangeBallZ(z);


		_canControlBall = true;
		_isShoot = false;
		_isShooting = true;
		//enableTouch();

		// reset ball
		_ball.velocity = Vector3.zero;
		_ball.angularVelocity = Vector3.zero;
		_ball.transform.localEulerAngles = Vector3.zero;


		Vector3 pos = new Vector3(BallPositionX, 0f, BallPositionZ);
		Vector3 diff = -pos;
		diff.Normalize();
		float angleRadian = Mathf.Atan2(diff.z, diff.x);		// tinh' goc' lech
		float angle = 90 - angleRadian * Mathf.Rad2Deg;

		_ball.transform.parent.localEulerAngles = new Vector3(0, angle, 0);		// set parent cua ball xoay 1 do theo truc y = goc lech

		_ball.transform.position = new Vector3(BallPositionX, 0.16f, BallPositionZ);

		pos = _ballTarget.position;
		pos.x = 0;
		_ballTarget.position = pos;

        float val = (Mathf.Abs(_ball.transform.localPosition.z) - _distanceMinZ) / (_distanceMaxZ - _distanceMinZ);
		_zVelocity =  Mathf.Lerp(_speedMin, _speedMax, val);

		//Debug.Log("Zvelocity = " + _zVelocity);
	    EventChangeSpeedZ(_zVelocity);

		//    EventDidPrepareNewTurn();

		RewardkeyController.instance.KeyPositionSetter();

		PlayerScript.instance.updatePlayerPosition();

	}

	public void GoalKeeperTurnOff()
    {
		//GoalKeeperObject = GameObject.FindGameObjectWithTag("Player");
		if (GoalKeeperObject != null)                   //No goalkeeper in this mode
		{
			GoalKeeperObject.SetActive(false);
		}

	}
	public void HurdleInstanceDestroyer()
    {
		for (int i = 0; i < HurdleInstance.Length; i++)
		{
			if (HurdleInstance[i] != null)
			{
				//HurdleInstance.SetActive(false);
				Destroy(HurdleInstance[i]);
			}
		}
	}
	public void enableTouch()
    {
        _enableTouch = true;
    }

    public void disableTouch()
    {
        StartCoroutine(_disableTouch());
    }

    private IEnumerator _disableTouch()
    {
        yield return new WaitForEndOfFrame();
        _enableTouch = false;
    }
}
