﻿using UnityEngine;
using System.Collections;


public class AutoResetAfterShootFinish : MonoBehaviour
{

    public static AutoResetAfterShootFinish instance;
    public float resetAfter = 0.5f;//1.5f;  //ahmad
    public float resetAfterGoalMiss = 0.5f;  
    public float resetAfterGoalKeeperSave = 0.5f;  
    public bool RandomNewPos { get; set; }

     void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }
    // Use this for initialization
    void Start ()
	{
	    RandomNewPos = true;
	    GoalDetermine.EventFinishShoot += OnShootFinished;
        if(GameConstants.GetGameMode()=="Multiplayer1"|| GameConstants.GetGameMode() == "Multiplayer2")
        {
            resetAfter =1.2f;//1.5f;  //ahmad
            resetAfterGoalMiss = 1.2f;
            resetAfterGoalKeeperSave = 1.2f;
        }
        else
        {
            resetAfter = 0.5f;//1.5f;  //ahmad
            resetAfterGoalMiss = 0.5f;
            resetAfterGoalKeeperSave = 0.5f;
        }
//	    Shoot.EventDidPrepareNewTurn += OnNewTurn;
	}

    void OnDestroy()
    {
        GoalDetermine.EventFinishShoot -= OnShootFinished;
 //       Shoot.EventDidPrepareNewTurn -= OnNewTurn;
    }

    void OnNewTurn()
    {
        RunAfter.removeTasks(gameObject);
    }

    void OnShootFinished(bool isGoal, Area area)
    {
    //    Debug.Log("is goal is autoresetaftershoot :  " + isGoal);
        RunAfter.runAfter(gameObject, () =>
        {
            DemoShoot.share.Reset(RandomNewPos);
        }, resetAfter);
    }
}
