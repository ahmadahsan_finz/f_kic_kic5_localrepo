﻿using UnityEngine;
using System.Collections;

public class DemoShoot : MonoBehaviour
{
    public static DemoShoot share;

    private bool _isWallKick;

    public static DemoShoot instance;
    public bool IsWallKick
    {
        get { return _isWallKick; }
        set
        {
            _isWallKick = value;
            Wall.share.IsWall = _isWallKick;
            if (_isWallKick)
            {
                Wall.share.setWall(Shoot.share._ball.transform.position);
            }
        }
    }

    [SerializeField] private int initialGKLevel = 0;

    void Awake()
    {
        share = this;
    }

    IEnumerator Start()
    {
        yield return new WaitForEndOfFrame();
   //     GoalKeeperLevel.share.setLevel(initialGKLevel);
    }
 
    public void Reset(bool shouldRandomNewPos)
    {
    //    Debug.Log("inside demoshoot reset");
        // ShootAI reset logic must be called first, to reset new ball poosition, reset() method of other components must come after this
        if(shouldRandomNewPos)
            ShootAI.shareAI.reset();                // used this method to reset new randomised ball's position
        else
            ShootAI.shareAI.reset(ShootAI.shareAI.BallPositionX, ShootAI.shareAI.BallPositionZ);        // call like this to reset new turn with current ball position

        SlowMotion.share.reset();                   // reset the slowmotion logic

        GoalKeeperHorizontalFly.share.reset();      // reset goalkeeperhorizontalfly logic
        if (!GamePlayManager.instance.LevelEndBool && !GamePlayManager.instance.MissionEndBool && !GamePlayManager.instance.RewardShotEndBool)
        {
             GoalKeeper.share.reset();                   // reset goalkeeper logic

        }
        GoalDetermine.share.reset();                // reset goaldetermine logic so that it's ready to detect new goal
        /*
        if (Wall.share != null)                     // if there is wall in this scene                                       //ahmad //committed bcz there is no need to call wall from here
        {
            Debug.Log("Wall.share != null demoshoot.cs");
            Wall.share.IsWall = _isWallKick;         // set is wall state
            Debug.Log("Wall.share.IsWall =  " + Wall.share.IsWall + " demoshoot.cs");
            Debug.Log("IsWallKick =  " + IsWallKick + " demoshoot.cs");
            if (IsWallKick)                         // if we want wall kick
            {
                Debug.Log("IsWallKick demoshoot.cs");
                Wall.share.setWall(Shoot.share._ball.transform.position);       // set wall position with respect to ball position
            }
        }
        */
        if (!GamePlayManager.instance.LevelEndBool && !GamePlayManager.instance.MissionEndBool && !GamePlayManager.instance.RewardShotEndBool)
        { 
            CameraManager.share.reset();                // reset camera position
        }
    }


    public void OnClick_NewTurnRandomPosition()
    {
        Reset(true);
    }

    public void OnClick_NewTurnSamePosition()
    {
        Reset(false);
    }
   
}
