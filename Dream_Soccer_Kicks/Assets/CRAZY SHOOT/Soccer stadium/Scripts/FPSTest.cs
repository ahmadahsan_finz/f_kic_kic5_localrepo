// Attach this to a GUIText to make a frames/second indicator.
//
// It calculates frames/second over each updateInterval,
// so the display does not keep changing wildly.
//
// It is also fairly accurate at very low FPS counts (<10).
// We do this not by simply counting frames per interval, but
// by accumulating FPS for each frame. This way we end up with
// correct overall FPS even if the interval renders something like
// 5.5 frames.

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FPSTest : MonoBehaviour
{

float updateInterval = 0.5f;

private float accum = 0.0f; // FPS accumulated over the interval
private int frames = 0; // Frames drawn over the interval
private float fpsRate = 0.0f;
	float totalTime;

private float timeleft ; // Left time for current interval


void Start()
{
   // Application.targetFrameRate = 60;
    timeleft = updateInterval; 
}
void Update()
{
    timeleft -= Time.deltaTime;
    accum += Time.timeScale/Time.deltaTime;
    ++frames;
   
    // Interval ended - update GUI text and start new interval
    if( timeleft <= 0.0f )
    {
        // display two fractional digits (f2 format)
        fpsRate = (accum/frames);
			GetComponent<Text>().text = "FPS = " + (fpsRate).ToString("f2"); // uncomment it to show FPS on device screen, don't disable this script from scene as it is making calculation of fps for everyplay replay videos at run time. 

			
        timeleft = updateInterval;
        accum = 0.0f;
        frames = 0;
    }
}

	void checkFPSforEveryPlay()//Umair
	{
		
			//Debug.Log("EveryPlay Turn Off due to low fps");
		/*
			if(Everyplay.IsRecording()){
				GameConstants.stopRecording();
			}
			GameConstants.isEveryPlayVideoSupported = false;
			ControllerScript.EveryPlayOnOff (0);
			*/

	}

}