Shader "Custom/MatCapItem"
{
	Properties
	{
	 	_Color ("Main Color", COLOR) = (1,1,1,1)
		_MainTex ("Texture", 2D) = "white" {}
		_ShineRamp ("ShineRamp", 2D) = "white" {}
		_ScrollSpeed ("Shine Scroll Speed", Float) = 10.0
		_EffectStrength  ("Shine Strength", Range (0,5)) = 2
		_Emission  ("Emission", Range (0,1)) = 1
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
		ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			uniform sampler2D _MainTex;
			uniform sampler2D _ShineRamp;
			uniform float _EffectStrength;
			uniform float _Emission;
			uniform float _ScrollSpeed;

			fixed4 _Color;
			float4 _MainTex_ST;
			float4 _ShineRamp_ST;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.uv2 = TRANSFORM_TEX(v.uv2, _ShineRamp) + frac(float2(_ScrollSpeed, _ScrollSpeed) * _Time);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{

				fixed4 tex = tex2D(_MainTex, i.uv);
				fixed4 shine = tex2D(_ShineRamp, i.uv2);
				float4 col = tex * ((shine * _EffectStrength) + _Emission) * _Color;
				return col;
			}
			ENDCG
		}
	}
}

//o.uv2 = TRANSFORM_TEX(v.texcoord.xy,_DetailTex) + frac(float2(_ScrollX, _ScrollY) * _Time);