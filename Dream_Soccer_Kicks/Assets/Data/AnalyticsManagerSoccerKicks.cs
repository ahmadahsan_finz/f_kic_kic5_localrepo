using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

#if USE_FIREBASE
using Firebase.Analytics;
#endif

//using GameAnalyticsSDK;

public class AnalyticsManagerSoccerKicks : GenericSingletonClass<AnalyticsManagerSoccerKicks>
{
	public bool TimeSpentAnalytics = true;

	#region Event_Transitions

	public enum Event_triggers // FireBase Levels Analytics // KD
	{
		MainMenu_Career,
		
		MainMenu_PenaltyButton_NonTutroial,

		MainMenu_FreekickButton_NonTutroial,

		MainMenu_PenaltyButton_Tutorial,

		MainMenu_FreekickButton_Tutorial,

		MainMenu_FreekickButton,
		
		LevelPopUp_Play,

		Cutscene_Skip1,

		Cutscene_Skip2,

		Tutorial_Gameplay_Completed,
		
		Tutorial_Menu_Completed,

		Customization_GameplayButton_Clicked,

		MultplayerPenalty_GameplayButton_Clicked,

		SkipLevel_GameplayButton_Clicked,

		FireballExtra_GameplayButton_Clicked,

	}

	public enum Event_State
    {
		Menu_Events,
		Gameplay_Events
    }
	public void Event_Transitions(Event_triggers Event_triggers, Event_State Event_State) //firebase 
	{
			//	Debug.Log("Episode number is " + episodeno);
			if (PlayerPrefs.GetInt("firebase_" + Event_triggers, 0) == 0)
			{
				//	Debug.Log("started lvl" + levelno);

				PlayerPrefs.SetInt("firebase_" + Event_triggers, 1);
#if USE_FIREBASE
			FirebaseAnalytics.LogEvent(
				  "Event_transitions",
			 new Parameter(Event_State.ToString(), Event_triggers.ToString())
			 );
#endif
			}
	}

/*	public void EventTransition(Event_triggers Event_triggers)			//unity analytics
	{
		Analytics.CustomEvent("Event_Transitions", new Dictionary<string, object>
		{

			{
						"Transitions_"+Application.version ,Event_triggers
			}
		}
		);
	}
	*/

	#endregion

	#region AdOnGamesEnd
	public void MoreGamesQuit()
	{
		//Analytics.CustomEvent ("AdOnGameEnd", new Dictionary<string, object> {
		//	{"Status", "Quited"} } 
		//);
	}
	public void MoreGamesCancel()
	{
		//Analytics.CustomEvent ("AdOnGameEnd", new Dictionary<string, object> {
		//	{"Status", "Cancel"} } 
		//);
	}
	public void MoreGamesMore()
	{
		//Analytics.CustomEvent ("AdOnGameEnd", new Dictionary<string, object> {
		//	{"Status", "MoreGamesList"} } 
		//);
	}

	#endregion



	#region PrivacyPolicy
	public void PrivacyPolicyConsentYes()
	{
		//Analytics.CustomEvent ("PrivacyPolicy", new Dictionary<string, object> {
		//	{"Consent", "Agree"} } 
		//);

	}
	public void PrivacyPolicyConsentNo()
	{
		//Analytics.CustomEvent ("PrivacyPolicy", new Dictionary<string, object> {
		//	{"Consent", "no"} } 
		//);

	}
	public void PrivacyPolicyDocumentReading()
	{
		//Analytics.CustomEvent ("PrivacyPolicy", new Dictionary<string, object> {
		//	{"Consent", "Policy"} } 
		//);

	}
	#endregion


	#region HouseAdsName
	// game name occurances
	public void HouseAdsNameEvent(string name)
	{
		//Analytics.CustomEvent ("HouseAds", new Dictionary<string, object> {
		//	{"GameName",name} } 
		//);
	}
	#endregion


	#region HouseAds_Click
	public void HouseAds_SpecificGameEvent_Click(string name, string input)
	{
		//Analytics.CustomEvent ("InHouseAd", new Dictionary<string, object> {
		//	{ 
		//		name, input

		//	} }
		//);


	}

	#endregion

	public void LevelsAnalytics(int episodeno, int levelno, LevelStates lvlstate) //firebase 
	{
		if (lvlstate == LevelStates.started)
		{
		//	Debug.Log("Episode number is " + episodeno);
			if (episodeno == 1)		//levels
			{
				if (PlayerPrefs.GetInt("firebaselvlstart" + levelno, 0) == 0)
				{
				//	Debug.Log("started lvl" + levelno);

					PlayerPrefs.SetInt("firebaselvlstart" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Levels",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 2)	//missions
			{
				if (PlayerPrefs.GetInt("firebaseMissionstart" + levelno, 0) == 0)
				{
				//	Debug.Log("started Mission" + levelno);

					PlayerPrefs.SetInt("firebaseMissionstart" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Missions",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 3)		//rewardKick
			{
				if (PlayerPrefs.GetInt("firebaseRewardKickstart" + levelno, 0) == 0)
				{
				//	Debug.Log("started RewardKick" + levelno);

					PlayerPrefs.SetInt("firebaseRewardKickstart" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "RewardKick",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}


		}
		else if (lvlstate == LevelStates.ended)
		{
			if (episodeno == 1)
			{
				if (PlayerPrefs.GetInt("firebaselvlend" + levelno, 0) == 0)
				{
				//	Debug.Log("ended lvl" + levelno);

					PlayerPrefs.SetInt("firebaselvlend" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Levels",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMissionend" + levelno, 0) == 0)
				{
				//	Debug.Log("ended Mission" + levelno);

					PlayerPrefs.SetInt("firebaseMissionend" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Missions",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 3)        //rewardKick
			{
				if (PlayerPrefs.GetInt("firebaseRewardKickEnd" + levelno, 0) == 0)
				{
				//	Debug.Log("ended RewardKick" + levelno);

					PlayerPrefs.SetInt("firebaseRewardKickEnd" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "RewardKick",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}

		}
		else if (lvlstate == LevelStates.win)
		{
			if (episodeno == 1)
			{
				if (PlayerPrefs.GetInt("firebaselvlwin" + levelno, 0) == 0)
				{
				//	Debug.Log("win lvl" + levelno);

					PlayerPrefs.SetInt("firebaselvlwin" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Levels",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMissionwin" + levelno, 0) == 0)
				{
				//	Debug.Log("win Mission" + levelno);

					PlayerPrefs.SetInt("firebaseMissionwin" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Missions",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 3)        //rewardKick
			{
				if (PlayerPrefs.GetInt("firebaseRewardKickWin" + levelno, 0) == 0)
				{
				//	Debug.Log("win RewardKick" + levelno);

					PlayerPrefs.SetInt("firebaseRewardKickWin" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "RewardKick",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
		}
		else if (lvlstate == LevelStates.lost)
		{

			if (episodeno == 1)
			{
				if (PlayerPrefs.GetInt("firebaselvllost" + levelno, 0) == 0)
				{
				//	Debug.Log("lost lvl" + levelno);

					PlayerPrefs.SetInt("firebaselvllost" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Levels",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMissionlost" + levelno, 0) == 0)
				{
				//	Debug.Log("lost Mission" + levelno);

					PlayerPrefs.SetInt("firebaseMissionlost" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Missions",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 3)        //rewardKick
			{
				if (PlayerPrefs.GetInt("firebaseRewardKickLost" + levelno, 0) == 0)
				{
				//	Debug.Log("lost RewardKick" + levelno);

					PlayerPrefs.SetInt("firebaseRewardKickLost" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "RewardKick",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}

		}
		else if (lvlstate == LevelStates.restarted)
		{

			if (episodeno == 1)
			{
				if (PlayerPrefs.GetInt("firebaselvlRestart" + levelno, 0) == 0)
				{
				//	Debug.Log("Restart lvl" + levelno);

					PlayerPrefs.SetInt("firebaselvlRestart" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Levels",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			else if (episodeno == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMissionRestart" + levelno, 0) == 0)
				{
				//	Debug.Log("Restart Mission" + levelno);

					PlayerPrefs.SetInt("firebaseMissionRestart" + levelno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Missions",
				 new Parameter(lvlstate.ToString(), levelno.ToString())
				 );
#endif
				}
			}
			

		}

	}
	public void MultiplayerAnalytics(int multiplayerModeNo, int Countno, LevelStates lvlstate) //firebase //ahmad
	{

		if (lvlstate == LevelStates.started)
		{ 
			 if (multiplayerModeNo == 1)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayerStart" + Countno, 0) == 0)
				{
				//	Debug.Log("started Multiplayer " + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayerStart" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}
			 else if (multiplayerModeNo == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayer2Start" + Countno, 0) == 0)
				{
				//	Debug.Log("started Multiplayer2 " + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayer2Start" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer_Penalty",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}

		}
		else if (lvlstate == LevelStates.ended)
		{
			
			 if (multiplayerModeNo == 1)
			{

				if (PlayerPrefs.GetInt("firebaseMultiplayerEnd" + Countno, 0) == 0)
				{
					//Debug.Log("ended Multiplayer " + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayerEnd" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}
			else if (multiplayerModeNo == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayer2End" + Countno, 0) == 0)
				{
					//Debug.Log("ended Multiplayer2 " + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayer2End" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer_Penalty",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}
		}
		else if (lvlstate == LevelStates.win)
		{
		
			 if (multiplayerModeNo == 1)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayerWin" + Countno, 0) == 0)
				{
					//Debug.Log("win Multiplayer " + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayerWin" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}
			else if (multiplayerModeNo == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayer2Win" + Countno, 0) == 0)
				{
				//	Debug.Log("win Multiplayer2 " + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayer2Win" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer_Penalty",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}
		}
		else if (lvlstate == LevelStates.lost)
		{
			 if (multiplayerModeNo == 1)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayerLost" + Countno, 0) == 0)
				{
				//	Debug.Log("lost Multiplayer" + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayerLost" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}
			else if (multiplayerModeNo == 2)
			{
				if (PlayerPrefs.GetInt("firebaseMultiplayer2Lost" + Countno, 0) == 0)
				{
				//	Debug.Log("lost Multiplayer2" + Countno);

					PlayerPrefs.SetInt("firebaseMultiplayer2Lost" + Countno, 1);
#if USE_FIREBASE
					FirebaseAnalytics.LogEvent(
					  "Multiplayer_Penalty",
				 new Parameter(lvlstate.ToString(), Countno.ToString())
				 );
#endif
				}
			}

		}

	}
	/*public void LevelRestartAnalytics(int episodeno, int levelno, LevelStates lvlstate) //firebase //ahmad
	{
		if (episodeno == 1)     //levels
		{
			if (PlayerPrefs.GetInt("firebaselvlRestart" + levelno, 0) == 0)
			{
				Debug.Log("Restarted lvl" + levelno);

				PlayerPrefs.SetInt("firebaselvlRestart" + levelno, 1);
				FirebaseAnalytics.LogEvent(
				  "Levels",
			 new Parameter(lvlstate.ToString(), levelno.ToString())
			 );
			}
		}
		else if (episodeno == 2)    //missions
		{
			if (PlayerPrefs.GetInt("firebaseMissionRestart" + levelno, 0) == 0)
			{
				Debug.Log("Restarted Mission" + levelno);

				PlayerPrefs.SetInt("firebaseMissionRestart" + levelno, 1);
				FirebaseAnalytics.LogEvent(
				  "Missions",
			 new Parameter(lvlstate.ToString(), levelno.ToString())
			 );
			}
		}
	}*/
	public enum LevelStates // FireBase Levels Analytics 
	{
		started,
		ended,
		win,
		lost,
		restarted
	}

	/*
		public void OnClick_MultiplayerPenaltyButton_TutorialAnalytics()
		{
			Analytics.CustomEvent("MultiplayerPenalty_Tutorial_ButtonClick");
		}
		public void OnClick_MultiplayerPenaltyButton_NonTutorialAnalytics()
		{
			Analytics.CustomEvent("MultiplayerPenalty_NonTutorial_ButtonClick");
		}

	*/


	public void otherads(int adcompany)
	{
		//Analytics.CustomEvent ("OtherAds", new Dictionary<string, object> {
		//	{
		//		"Adtype",adcompany
		//	} } 
		//);


	}

	public void adsServed(string type)
	{

		//Analytics.CustomEvent("Ads-Analysis", new Dictionary<string, object> {
		//    {
		//        "Version | v_"+Application.version+" |",type
		//    } }
		//);
	}

	public void currentVersion()
	{
		//if (PlayerPrefs.GetString("Version") != Application.version)
		//{
		//    Analytics.CustomEvent("GameVersion", new Dictionary<string, object> { {
		//        "Version",Application.version
		//    } }
		//);
		//    PlayerPrefs.SetString("Version", Application.version);
		//}

	}

	public void GameStartStatus()
	{
		////        Debug.Log ("ads :"+loc);
		//Analytics.CustomEvent ("GameStart", new Dictionary<string, object> 
		//	{ { "State", "Started"} } );
	}

	public void InternetConnection(bool check)
	{
		////        Debug.Log ("ads :"+loc);
		//Analytics.CustomEvent ("isInternet", new Dictionary<string, object> 
		//	{ { "connected", check} } );
	}

	public enum Customization_Type
    {
		kit,
		skin,
		ball,
		celebration,
		shorts
    }
	public enum Unlock_Type
	{
		By_Coins,
		By_Ads
	}
	public void Customization_Analytics(Customization_Type Customization_Type, int Item_Number, Unlock_Type Unlock_Type) //firebase 
	{
		
			if (PlayerPrefs.GetInt("firebase_Unlock_" + Customization_Type + Item_Number, 0) == 0)
			{
				//	Debug.Log("started lvl" + levelno);

				PlayerPrefs.SetInt("firebase_Unlock_" + Customization_Type + Item_Number, 1);
#if USE_FIREBASE
			FirebaseAnalytics.LogEvent(
				  "Customization_Unlocking",
			 new Parameter(Customization_Type.ToString()+Unlock_Type.ToString(), Item_Number.ToString())
			 );
#endif
			}
		
		//	Debug.Log("Episode number is " + episodeno);
			
	}


	public void characterUnlock(int characterId)
	{
		//Analytics.CustomEvent ("characterUnlock", new Dictionary<string, object> {
		//	{
		//		"Status",characterId
		//	} } 
		//);
		//		Debug.Log ("Status "+characterId);

	}


	public void characterDeath(int levelId, int characterId)
	{
		//Analytics.CustomEvent ("characterDeathRatio", new Dictionary<string, object> {
		//	{
		//		levelId+"",characterId+""
		//	} } 
		//);
	}



	void OnApplicationQuit()
	{
		//userTimeSpent();
	}

	void OnApplicationPause(bool pauseStatus)
	{
		//if (!pauseStatus)
		//{
		//    //	Debug.Log ("delay added to ads for "+AdConstants.timeToDelayAds);
		//    userTimeSpent();
		//}
	}

	public void miniEvents(string events, string status)
	{
		//Analytics.CustomEvent(events, new Dictionary<string, object>
		// { { "status", status } });
		////		Debug.Log ("rateus");
	}

	void userTimeSpent()
	{

		//		int ts = (int)Time.realtimeSinceStartup + PlayerPrefs.GetInt("TimeSpend");
		////		Debug.Log ("ts:"+ts);

		//		string log = "0-100";
		//		if (ts >= 0 && ts < 100) {
		//			log = "0-100";
		//		}
		//		else if (ts >= 100 && ts < 200) 
		//		{
		//			log = "100-200";
		//		}
		//		else if (ts >= 200 && ts < 400) 
		//		{
		//			log = "200-400";
		//		}
		//		else if (ts >= 400 && ts < 800) 
		//		{
		//			log = "400-800";
		//		}
		//		else if (ts >= 800 && ts < 1600) 
		//		{
		//			log = "800-1600";
		//		}
		//		else if (ts >= 1600) 
		//		{
		//			log = "1600>";
		//		}
#if UNITY_IOS
  //      Analytics.CustomEvent ("UserPlayTime", new Dictionary<string, object> {
		//	{
  //              "Time",log
		//	}
		//} 
		//);


#endif
		//      PlayerPrefs.SetInt ("TimeSpend", ts);


		//PlayerPrefs.Save ();
	}

	public enum rewardedstate
	{
		FREE_COINS,
		EXTRA_LIFE,
		POWER_INCREASE,
		HEALTH_INCREASE,
		UNLOCK_LEVEL,
		NO_VIDEO,
		UNLOCK_PLAYER,
		DOUBLE_COINS
	}

	public void rewarded_watch(rewardedstate gamestate, int playerID = -1, int levelno = -1)
	{
		//Analytics.CustomEvent ("rewarded_watch", new Dictionary<string, object> 
		//	{ { "state",gamestate } } );

	}

	#region givingLevelState //umair
	public enum levelState
	{
		LEVEL_STARTED,
		LEVEL_ENDED,
		HIGH_LEVEL_STARTED,
		HIGH_LEVEL_ENDED,
		OTHER_MODES //pass 0 for started, pass 1 for ended for other modes
	}

	public void LevelStatus(levelState levelstate, int levelNumber)
	{
		//	if (levelstate != levelState.OTHER_MODES) {

		//		if (levelstate == levelState.LEVEL_STARTED || levelstate == levelState.HIGH_LEVEL_STARTED) {
		//			if (PlayerPrefs.GetInt ("StageStarted_" + levelNumber, 0) == 0) {
		//				Debug.Log ("Stage Started: " + levelNumber);
		//				Analytics.CustomEvent ("Levels", new Dictionary<string, object> 
		//		{ { levelstate.ToString (),levelNumber } });

		//				PlayerPrefs.SetInt ("StageStarted_" + levelNumber, 1);
		//			}
		//		}
		//		if (levelstate == levelState.LEVEL_ENDED || levelstate == levelState.HIGH_LEVEL_ENDED) {
		//			if (PlayerPrefs.GetInt ("StageEnded_" + levelNumber, 0) == 0) {
		//				Debug.Log ("Stage Ended: " + levelNumber);
		//				Analytics.CustomEvent ("Levels", new Dictionary<string, object> 
		//			{ { levelstate.ToString (),levelNumber } });

		//				PlayerPrefs.SetInt ("StageEnded_" + levelNumber, 1);
		//			}
		//		}
		//	} else {


		//			if (levelNumber == 0) {
		//				Analytics.CustomEvent ("Levels", new Dictionary<string, object> 
		//					{ { levelstate.ToString (),"Started" } });
		//			} else {
		//				Analytics.CustomEvent ("Levels", new Dictionary<string, object> 
		//					{ { levelstate.ToString (),"Ended" } });
		//			}


		////		Analytics.CustomEvent ("Levels", new Dictionary<string, object> 
		////			{ { levelstate.ToString (),levelNumber } });

		//	}



	}
	// You can call this line with different param you want. //umair
	//AnalyticsManagerSoccerKicks.Instance.LevelStatus (AnalyticsManagerSoccerKicks.levelState.LEVEL_STARTED, 1);
	#endregion

	public void rateus()
	{
		//		Analytics.CustomEvent ("RATEUS");
		////		Debug.Log ("rateus");
	}

	public void moregames()
	{
		//		Analytics.CustomEvent ("moregames");

		////		Debug.Log ("moregames");

	}

	public void AdsLoadStatus(string loc)
	{
		////		Debug.Log ("ads :"+loc);
		//		Analytics.CustomEvent ("AdsLoadStatus", new Dictionary<string, object> 
		//			{ {"state_"+Application.version,loc } } );

		//		GameStartStatus ();

	}



	public void InHouseLoadStatus(string loc)
	{
		//		Debug.Log ("house :"+loc);

		//Analytics.CustomEvent ("InHouseLoadStatus", new Dictionary<string, object> 
		//	{ { "state",loc } } );
	}


	public void languageSelect(string lang)
	{

		//Analytics.CustomEvent ("Language", new Dictionary<string, object> 
		//	{ { "name",lang } } );
	}



	#region inapp
	public void inAppPurchaseAnalytics(string itemName, bool status)
	{

		//Analytics.CustomEvent ("InAppPurchase", new Dictionary<string, object> 
		//	{ { itemName, status } } );
	}
	#endregion

	public void AutoFire(string itemName, bool status)
	{

		//Analytics.CustomEvent ("AutoFire", new Dictionary<string, object> 
		//	{ { itemName, status } } );
	}
	public void MatchesWon()
	{

		//Analytics.CustomEvent ("MatchesWon", new Dictionary<string, object>
		//	{ { "WinCount","win"} } );
	}

	public void MatchesLoss()
	{

		//Analytics.CustomEvent ("MatchesLoss", new Dictionary<string, object>
		//	{ { "LossCount","loss" } } );
	}


	public void NeverAskAgainForRemoveAds()
	{
		//xia
		//Analytics.CustomEvent ("IAP-Panel", new Dictionary<string, object> { {
		//		"NeverButton ", "Clicked"
		//	}
		//}
		//);
		//        AnalyticsManagerSoccerKicks.Instance.NeverAskAgainFor ();
	}


	public void crossPromotionAnalysis(string status)
	{
		//Analytics.CustomEvent("CrossPromotion", new Dictionary<string, object> {
		//    {"Status", status} }
		//);

	}



}
