﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Script for flawless 2d crowd (Football) animation (Asad)
public class TextureSwitcher : MonoBehaviour {

    public Texture2D[] crowd;
    public Material CrowdMat;
   
    public float CelebrationSpeed = 0.5f;

    private int index = 0;
    private Coroutine TexChange;

	void Start ()                                                   //Initialization
    {


	}

    private void OnEnable()
    {
        TexChange = StartCoroutine(_SwitchTex(CelebrationSpeed));
        
    }
    private IEnumerator _SwitchTex(float _S)                                  //Coroutine To display 15 frame looping Textures
    {
        while(true)
        {
            yield return new WaitForSeconds(_S);

            if (index < crowd.Length)
            {
                CrowdMat.mainTexture = crowd[index];
                index++;

            }
            else
            {
                index = 0;
                CrowdMat.mainTexture = crowd[index];

            }
        }
        
    }                   
}
