﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadomAnimation : MonoBehaviour {

    public float animationTime;
    public float RepeatRate;

    private Animator ac;
    private int randomindex = 0;
	
    // Use this for initialization
	void Start ()
    {
        ac = GetComponent<Animator>();
        InvokeRepeating("RandomAnim", animationTime, RepeatRate);
		
	}

    void RandomAnim()
    {
        randomindex = (int)Random.Range(0, 2);

       

        if(randomindex == 0 )
        {
            ac.SetTrigger("Clapping");
        }
        else if(randomindex == 1)
        {
            ac.SetTrigger("Rallying");
        }
    }
}
