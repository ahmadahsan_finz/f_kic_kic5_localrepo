﻿Shader "Finz.io/LightmappedPrefab"
{
  Properties
  {
    _MainTex("Base (RGB)", 2D) = "white" {}
    _Lightmap("Lightmap", 2D) = "white" {}
  }

  SubShader
  {
	Tags { "RenderType"="Opaque" "PerformanceChecks"="False" }
    LOD 300
    Tags{ "Queue" = "Geometry+1" }
    Pass
    {
		//Name "FORWARD"
		//Tags { "LightMode" = "ForwardBase" }

      CGPROGRAM
	  //#pragma surface surf Lambert nodynlightmap
	  #include "UnityStandardCoreForward.cginc"
      //#pragma target 3.0
	  // Defining the name of the vertex shader
      #pragma vertex vert

      // Defining the name of the fragment shader
      #pragma fragment frag

	  // make fog work
      #pragma multi_compile_fog

	  //#pragma multi_compile_fwdbase

      // Include some common helper functions,
      // specifically UnityObjectToClipPos and DecodeLightmap.
      #include "UnityCG.cginc"

      // Color Diffuse Map
      //sampler2D _MainTex;
      // Tiling/Offset for _MainTex, used by TRANSFORM_TEX in vertex shader
      //float4 _MainTex_ST;


      // Lightmap (created via Unity Lightbaking)
      sampler2D _Lightmap;
      // Tiling/Offset for _Lightmap, used by TRANSFORM_TEX in vertex shader
      float4 _Lightmap_ST;

      // This is the vertex shader input: position, UV0, UV1, normal
      // UV1 (= second UV channel) needed for the lightmap texture coordinates
      struct appdata
      {
        float4 vertex   : POSITION;
        float2 texcoord : TEXCOORD0;
        float2 texcoord1: TEXCOORD1;
      };

      // This is the data passed from the vertex to fragment shader
      struct v2f
      {
        float4 pos  : SV_POSITION; // position of the pixel
        float2 txuv : TEXCOORD0; // for accessing the diffuse color map
        float2 lmuv : TEXCOORD1; // for accessing the light map
		UNITY_FOG_COORDS(2)
      };

      // This is the vertext shader, doing nothing special at all.
      // Most notably it is calculating the surface normal, because that
      // is needed for the fake specular lighting in the fragment shader.
      v2f vert(appdata v)
      {
        v2f o;
        o.pos = UnityObjectToClipPos(v.vertex);
        o.txuv = TRANSFORM_TEX(v.texcoord.xy, _MainTex); // using _MainTex_ST
        o.lmuv = TRANSFORM_TEX(v.texcoord1.xy, _Lightmap); // using _Lightmap_ST
		UNITY_TRANSFER_FOG(o,o.pos);
        return o;
      }


      // Fragment Shader
      half4 frag(v2f i) : COLOR
      {
        // Reading color directly from the diffuse texture, using first UV channel
        half4 col = tex2D(_MainTex, i.txuv.xy);
        // Reading lightmap value from the lightmap texture
        half4 lm = tex2D(_Lightmap, i.lmuv.xy);

        // Calculating the final color of the pixel by bringing it all together
		//col.rgb = min(half4(1,1,1,1), col.rgb * DecodeLightmap(lm)*2);
		col.rgb = min(half4(1,1,1,1), col.rgb * lm.rgb *2);
		UNITY_APPLY_FOG(i.fogCoord, col);
        return col;
      }
      ENDCG
    }
  }

  Fallback "Diffuse"
}