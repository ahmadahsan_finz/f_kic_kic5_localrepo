﻿Shader "Finz.io/LightmappedPrefabTransparentCutout" {
Properties {
    //_Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
    _LightMap ("Lightmap (RGB)", 2D) = "black" {}
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
}
 
SubShader {
    LOD 200
    Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff
struct Input {
  float2 uv_MainTex;
  float2 uv2_LightMap;
};
sampler2D _MainTex;
sampler2D _LightMap;
//fixed4 _Color;
void surf (Input IN, inout SurfaceOutput o)
{
  //fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
  //o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * _Color;

  fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
  //o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
  half4 lm = tex2D (_LightMap, IN.uv2_LightMap);
  o.Emission = lm.rgb*c.rgb*2;
  o.Alpha = c.a;
}
ENDCG
}
FallBack "Legacy Shaders/Transparent/Cutout/VertexLit"
}
 
