// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Finz.io/LightmappedPrefabTransparentDiffuse" {
Properties {
    //_Color ("Main Color", Color) = (1,1,1,1)
    _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_LightMap ("Lightmap (RGB)", 2D) = "black" {}

}

SubShader {
    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
    LOD 200

CGPROGRAM
#pragma surface surf Lambert alpha:fade

sampler2D _MainTex;
sampler2D _LightMap;
//fixed4 _Color;


struct Input {
    float2 uv_MainTex;
	float2 uv2_LightMap;
};

void surf (Input IN, inout SurfaceOutput o) {
    //fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
	half4 lm = tex2D (_LightMap, IN.uv2_LightMap);
	o.Emission = lm.rgb*c.rgb*2;
    //o.Albedo = c.rgb;
    o.Alpha = c.a;
}
ENDCG
}

Fallback "Legacy Shaders/Transparent/VertexLit"
}
