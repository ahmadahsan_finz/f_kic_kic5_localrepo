﻿Shader "Finz/MatcapStandardLM"
{
    Properties
    {
        _Color("Main Color", Color) = (0.5,0.5,0.5,1)
        _MainTex("Base (RGB)", 2D) = "white" {}
        _MatCap("MatCap", 2D) = "white"
        [Toggle] _PerspectiveCorrection("Use Perspective Correction", Float) = 1.0
        _MatCapStrength ("MatCapStrength" , Float) = 1.0
        //[Toggle(AMBIENT_ON)] _AmbientOn("Ambient Lighting", Float) = 0

    }

        SubShader
        {
            Tags { "RenderType" = "Opaque" }
                
                // Non-lightmapped
            Pass
            {   
			        Tags { "LightMode" = "Vertex" "RenderType" = "Opaque" }
                    
                    Cull Back
                    Lighting On

                    CGPROGRAM

                    #pragma vertex vert
                    #pragma fragment frag
                    #pragma multi_compile_fog
                    //#include "UnityLightingCommon.cginc"
                    #include "UnityCG.cginc"

                    struct v2f
                    {
                        float4 pos : SV_POSITION;
                        float2 uv0  : TEXCOORD0;
                        float2 cap : TEXCOORD4;
                        float3 color : COLOR;
                        UNITY_FOG_COORDS(2)
                    };

                    uniform float4 _Color;
                    uniform sampler2D _MainTex;
                    uniform sampler2D _MatCap;
                    bool _PerspectiveCorrection;
                    uniform float4 _MainTex_ST;
                    uniform float _MatCapStrength;

                    v2f vert(appdata_full v)
                    {
                        v2f o;

                        o.pos = UnityObjectToClipPos(v.vertex);
                        o.uv0 = TRANSFORM_TEX(v.texcoord, _MainTex);
                        o.color = ShadeVertexLights(v.vertex, v.normal);

                        half3 worldNorm = UnityObjectToWorldNormal(v.normal);
                        float3 viewNorm = mul((float3x3)UNITY_MATRIX_V, worldNorm);

                        if (_PerspectiveCorrection)
                        {
                            // get view space position of vertex
                            float3 viewPos = UnityObjectToViewPos(v.vertex);
                            float3 viewDir = normalize(viewPos);

                            // get vector perpendicular to both view direction and view normal
                            float3 viewCross = cross(viewDir, viewNorm);

                            // swizzle perpendicular vector components to create a new perspective corrected view normal
                            viewNorm = float3(-viewCross.y, viewCross.x, 0.0);
                        }
                        o.cap = viewNorm.xy * 0.5 + 0.5;
                        UNITY_TRANSFER_FOG(o, o.pos);
                        return o;
                    }

                    fixed4 frag(v2f i) : COLOR
                    {
                        fixed4 tex = tex2D(_MainTex, i.uv0.xy);
                        float4 col = tex * (tex2D(_MainTex, i.uv0) + (1 - _MatCapStrength) + _MatCapStrength * tex2D(_MatCap, i.cap));
                        col.rgb = col.rgb * i.color; 
                        col = _Color * col * 0.7;

                        UNITY_APPLY_FOG(i.fogCoord, col);
                        return col;
                    }
                    ENDCG
            }
            
                // Lightmapped
            Pass
            {
                Tags{ "LIGHTMODE" = "VertexLM" "RenderType" = "Opaque" }

                CGPROGRAM

                #pragma vertex vert
                #pragma fragment frag
                #pragma target 2.0
                #include "UnityCG.cginc"
                #pragma multi_compile_fog
                //#define USING_FOG (defined(FOG_LINEAR) || defined(FOG_EXP) || defined(FOG_EXP2))

                float4 _MainTex_ST;

                struct appdata
                {
                    float3 pos : POSITION;
                    float3 uv1 : TEXCOORD1;
                    float3 uv0 : TEXCOORD0;

                    UNITY_VERTEX_INPUT_INSTANCE_ID
                };

                struct v2f
                {
                    float2 uv0 : TEXCOORD0;
                    float2 uv1 : TEXCOORD1;
                    float2 cap : TEXCOORD4;
                    UNITY_FOG_COORDS(2)


                //#if USING_FOG
                //    fixed fog : TEXCOORD2;
                //#endif
                    float4 pos : SV_POSITION;

                    UNITY_VERTEX_OUTPUT_STEREO
                };

                bool _PerspectiveCorrection;
                uniform float _MatCapStrength;
                uniform sampler2D _MatCap;
                uniform float4 _Color;

                v2f vert(appdata_full IN)
                {
                    v2f o;
                    UNITY_SETUP_INSTANCE_ID(IN);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                    o.uv1 = IN.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    //o.uv0 = IN.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;
                    o.uv0 = TRANSFORM_TEX(IN.texcoord, _MainTex);
                    half3 worldNorm = UnityObjectToWorldNormal(IN.normal);
                    float3 viewNorm = mul((float3x3)UNITY_MATRIX_V, worldNorm);

                    if (_PerspectiveCorrection)
                    {
                            // get view space position of vertex
                            float3 viewPos = UnityObjectToViewPos(IN.vertex);
                            float3 viewDir = normalize(viewPos);

                            // get vector perpendicular to both view direction and view normal
                            float3 viewCross = cross(viewDir, viewNorm);

                            // swizzle perpendicular vector components to create a new perspective corrected view normal
                            viewNorm = float3(-viewCross.y, viewCross.x, 0.0);
                    }
                    o.cap = viewNorm.xy * 0.5 + 0.5;

                //#if USING_FOG
                //    float3 eyePos = UnityObjectToViewPos(IN.pos);
                //    float fogCoord = length(eyePos.xyz);
                //    UNITY_CALC_FOG_FACTOR_RAW(fogCoord);
                //    o.fog = saturate(unityFogFactor);
                //#endif

                    o.pos = UnityObjectToClipPos(IN.vertex);
                    UNITY_TRANSFER_FOG(o, o.pos);
                    return o;
                }

                sampler2D _MainTex;

                fixed4 frag(v2f IN) : SV_Target
                {
                    //fixed4 col;
                    fixed4 tex = UNITY_SAMPLE_TEX2D(unity_Lightmap, IN.uv1.xy);
                    half3 bakedColor = DecodeLightmap(tex);
                    float4 col = tex * (tex2D(_MainTex, IN.uv0) * (1 - _MatCapStrength) + _MatCapStrength * tex2D(_MatCap, IN.cap));
                    tex = tex2D(_MainTex, IN.uv0.xy);
                    col.rgb = _Color * col ;
                    //tex.rgb = tex.rgb * _Color ;
                    //tex.rgb = (tex + col) / 2.2 ;
                    tex.rgb = (tex + col) * 0.68;
                    col.rgb = tex.rgb * (bakedColor + 0.1);
                    col.a = 1.0f;

                    //#if USING_FOG
                    //col.rgb = lerp(unity_FogColor.rgb, col.rgb, IN.fog);
                    //#endif
                    UNITY_APPLY_FOG(IN.fogCoord, col);
                    return col;
                }

                ENDCG
            }
            
            
            // Pass to render object as a shadow caster
            Pass
            {
                Name "ShadowCaster"
                Tags{ "LightMode" = "ShadowCaster" }

                ZWrite On ZTest LEqual Cull Off

                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma target 2.0
                #pragma multi_compile_shadowcaster
                #include "UnityCG.cginc"

                struct v2f {
                    V2F_SHADOW_CASTER;
                    UNITY_VERTEX_OUTPUT_STEREO
                };

                v2f vert(appdata_base v)
                {
                    v2f o;
                    UNITY_SETUP_INSTANCE_ID(v);
                    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                    TRANSFER_SHADOW_CASTER_NORMALOFFSET(o)
                    return o;
                }

                float4 frag(v2f i) : SV_Target
                {
                    SHADOW_CASTER_FRAGMENT(i)
                }
                ENDCG
             }


        }
        //Fallback "VertexLit"

}