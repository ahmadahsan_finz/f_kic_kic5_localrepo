﻿
using System;
using GoogleMobileAds.Editor;
using UnityEditor;
public class FinzAdmobPatch
{
    [Serializable]
    public class AppIds
    {
        public string appID_Android;
        public string appID_IOS;
    }
    public static void SetAdmobAppID(string androidAppId, string iosAppID)
    {
        GoogleMobileAdsSettings.Instance.GoogleMobileAdsAndroidAppId = androidAppId;
        GoogleMobileAdsSettings.Instance.GoogleMobileAdsIOSAppId = iosAppID;
        GoogleMobileAdsSettings.Instance.DelayAppMeasurementInit = true;
        EditorUtility.DisplayDialog("Success", "Google Mobile Ads App ids is Assigned Successfully\nYou can Check Settings from\nAssets->GoogleMobileAds->Settings", "OK");

    }

    public static void SetAdmobAppIDOnly(string androidAppId, string iosAppID)
    {
        GoogleMobileAdsSettings.Instance.GoogleMobileAdsAndroidAppId = androidAppId;
        GoogleMobileAdsSettings.Instance.GoogleMobileAdsIOSAppId = iosAppID;
        GoogleMobileAdsSettings.Instance.DelayAppMeasurementInit = true;

    }
}
