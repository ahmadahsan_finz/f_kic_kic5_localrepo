﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyAds_TimeController : MonoBehaviour
{
    private float initial_TimeScale = 1;
    private void OnEnable()
    {
#if UNITY_EDITOR
        Debug.Log("--------- Editor Ads Showing ---------");
        initial_TimeScale = Time.timeScale;
#endif
    }

    private void OnDestroy()
    {
#if UNITY_EDITOR
        Debug.Log("--------- Editor Ads Destroyed ---------");
        Time.timeScale = initial_TimeScale;
#endif
    }
}
