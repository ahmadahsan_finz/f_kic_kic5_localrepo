﻿using UnityEngine;
using UnityEditor;
//using CoverShooter;
//using RealShooting;
using UnityEditor.SceneManagement;

public class SoccerMenuItemEditorScript : Editor
{
   
    [MenuItem("SoccerKicks/Clear Pref")]
    public static void ClearPref()
    {
       
        PlayerPrefs.DeleteAll();
    }

    #region Scene
    [MenuItem("SoccerKicks/Scene/PluginScene")]
    public static void openLoading()
    {

        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(EditorBuildSettings.scenes[0].path);
    }
    [MenuItem("SoccerKicks/Scene/MenuScene")]
    public static void openUIScene()
    {

        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(EditorBuildSettings.scenes[1].path);
    }
    [MenuItem("SoccerKicks/Scene/GameplayScene")]
    public static void openGameplay()
    {
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(EditorBuildSettings.scenes[2].path);
    }
    //[MenuItem("SoccerKicks/Scene/Multiplayer")]
    //public static void openMultiplayer()
    //{
    //    EditorSceneManager.SaveOpenScenes();
    //    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
    //    EditorSceneManager.OpenScene(EditorBuildSettings.scenes[3].path);
    //}
    //[MenuItem("SoccerKicks/Scene/Boss")]
    //public static void openBoss()
    //{
    //    EditorSceneManager.SaveOpenScenes();
    //    EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
    //    EditorSceneManager.OpenScene(EditorBuildSettings.scenes[4].path);
    //}
   //[MenuItem("SoccerKicks/Scene/ENV1")]
   // public static void openENV1()
   // {

   //     EditorSceneManager.SaveOpenScenes();
   //     EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
   //     EditorSceneManager.OpenScene(EditorBuildSettings.scenes[5].path);
   // }
   // [MenuItem("SoccerKicks/Scene/ENV2")]
   // public static void openENV2()
   // {
   //     EditorSceneManager.SaveOpenScenes();
   //     EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
   //     EditorSceneManager.OpenScene(EditorBuildSettings.scenes[6].path);
   // }
   // [MenuItem("Real Shooting/Scene/ENV3")]
   // public static void openENV3()
   // {
   //     EditorSceneManager.SaveOpenScenes();
   //     EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
   //     EditorSceneManager.OpenScene(EditorBuildSettings.scenes[7].path);
   // }
   // [MenuItem("Real Shooting/Scene/ENV4")]
   // public static void openENV4()
   // {
   //     EditorSceneManager.SaveOpenScenes();
   //     EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
   //     EditorSceneManager.OpenScene(EditorBuildSettings.scenes[8].path);
   // }
   // [MenuItem("Real Shooting/Scene/ENV5")]
   // public static void openENV5()
   // {
   //     EditorSceneManager.SaveOpenScenes();
   //     EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
   //     EditorSceneManager.OpenScene(EditorBuildSettings.scenes[9].path);
   // }
    #endregion


    #region Gameplay
    //[MenuItem("Real Shooting/Gameplay/Kill Player")]
    //public static void killPlayer()
    //{
    //    if (StoryManager.instance)
    //    {
    //        StoryManager.instance.playerManager.killPlayer();
    //    }
    //    if (BossManager.instance)
    //    {
    //        BossManager.instance.playerManager.killPlayer();
    //    }
    //    if (MultiPlayerManager.instance)
    //    {
    //        MultiPlayerManager.instance.playerManager.killPlayer();
    //    }
    //}
    //[MenuItem("Real Shooting/Gameplay/Kill All Alive Enemies")]
    //public static void killEnemies()
    //{
    //    if (StoryManager.instance)
    //    {
    //        StoryManager.instance.killAllAliveEnemies();
    //    }
    //    if (BossManager.instance)
    //    {
    //        BossManager.instance.killAllAliveEnemies();
    //    }
    //    if (MultiPlayerManager.instance)
    //    {
    //        MultiPlayerManager.instance.killAllAliveEnemies();
    //    }
    //}

    //[MenuItem("Real Shooting/Gameplay/Stop AI")]
    //public static void Endless()
    //{
    //    if (StoryManager.instance)
    //    {
    //        StoryManager.instance.endlessSetup();
    //    }
    //}
    //[MenuItem("Real Shooting/Gameplay/Last 10 seconds")]
    //public static void tenSeconds()
    //{
    //    if (HudManager.instance&&HudManager.instance.countDownTimmer)
    //    {
    //        HudManager.instance.countDownTimmer.lastSeconds();
    //    }
    //}
    #endregion




}
