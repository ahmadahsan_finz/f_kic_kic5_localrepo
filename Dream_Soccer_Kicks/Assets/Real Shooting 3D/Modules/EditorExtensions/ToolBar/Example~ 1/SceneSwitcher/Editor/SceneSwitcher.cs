﻿using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace UnityToolbarExtender.Examples
{
	static class ToolbarStyles
	{
		public static readonly GUIStyle commandButtonStyle;

		static ToolbarStyles()
		{
			commandButtonStyle = new GUIStyle("Command")
			{
				fontSize = 16,
				alignment = TextAnchor.MiddleCenter,
				imagePosition = ImagePosition.ImageAbove,
				fontStyle = FontStyle.Bold
			};
		}
	}

	//[InitializeOnLoad]
	//public class SceneSwitchLeftButton
	//{
	//	static SceneSwitchLeftButton()
	//	{
	//		ToolbarExtender.LeftToolbarGUI.Add(OnToolbarGUI);
			
	//	}

	//	static void OnToolbarGUI()
	//	{
	//		GUILayout.FlexibleSpace();

	//		if (GUILayout.Button(new GUIContent("CP", "CLEAR PREF"), ToolbarStyles.commandButtonStyle))
	//		{
	//			PlayerPrefs.DeleteAll();
	//		}
	//		if (GUILayout.Button(new GUIContent("L", "LOADING"), ToolbarStyles.commandButtonStyle))
	//		{
	//			EditorSceneManager.SaveOpenScenes();
	//			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
	//			EditorSceneManager.OpenScene(EditorBuildSettings.scenes[0].path);
	//		}

	//		if(GUILayout.Button(new GUIContent("UI", "UI"), ToolbarStyles.commandButtonStyle))
	//		{
	//			EditorSceneManager.SaveOpenScenes();
	//			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
	//			EditorSceneManager.OpenScene(EditorBuildSettings.scenes[1].path);
	//		}
			
	//	}
		
	//}

//	[InitializeOnLoad]
	//public class SceneSwitchRightButton
	//{
	//	static SceneSwitchRightButton()
	//	{
			
	//		ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
	//	}

	
	//	static void OnToolbarGUI()
	//	{
	//		GUILayout.FlexibleSpace();

	//		if (GUILayout.Button(new GUIContent("S", "STORY"), ToolbarStyles.commandButtonStyle))
	//		{
	//			EditorSceneManager.SaveOpenScenes();
	//			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
	//			EditorSceneManager.OpenScene(EditorBuildSettings.scenes[2].path);
	//		}
	//		if (GUILayout.Button(new GUIContent("B", "BOSS"), ToolbarStyles.commandButtonStyle))
	//		{
	//			EditorSceneManager.SaveOpenScenes();
	//			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
	//			EditorSceneManager.OpenScene(EditorBuildSettings.scenes[4].path);
	//		}
	//		if (GUILayout.Button(new GUIContent("M", "MULTIPLAYER"), ToolbarStyles.commandButtonStyle))
	//		{
	//			EditorSceneManager.SaveOpenScenes();
	//			EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
	//			EditorSceneManager.OpenScene(EditorBuildSettings.scenes[3].path);
	//		}

	//	}
	//}

	static class SceneHelper
	{
		static string sceneToOpen;

		public static void StartScene(string sceneName)
		{
			if(EditorApplication.isPlaying)
			{
				EditorApplication.isPlaying = false;
			}

			sceneToOpen = sceneName;
			EditorApplication.update += OnUpdate;
		}

		static void OnUpdate()
		{
			if (sceneToOpen == null ||
			    EditorApplication.isPlaying || EditorApplication.isPaused ||
			    EditorApplication.isCompiling || EditorApplication.isPlayingOrWillChangePlaymode)
			{
				return;
			}

			EditorApplication.update -= OnUpdate;

			if(EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
			{
				// need to get scene via search because the path to the scene
				// file contains the package version so it'll change over time
				string[] guids = AssetDatabase.FindAssets("t:scene " + sceneToOpen, null);
				if (guids.Length == 0)
				{
					Debug.LogWarning("Couldn't find scene file");
				}
				else
				{
					string scenePath = AssetDatabase.GUIDToAssetPath(guids[0]);
					EditorSceneManager.OpenScene(scenePath);
					EditorApplication.isPlaying = true;
				}
			}
			sceneToOpen = null;
		}
	}
}