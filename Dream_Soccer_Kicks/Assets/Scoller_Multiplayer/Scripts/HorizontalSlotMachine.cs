﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DanielLochner.Assets.SimpleScrollSnap
{
    //Usama - Things to do:
    //Everything below is done. For sample, tally the working examples.
    //For both generic and horizontal
    //1- Add any image under the content section of view port
    //2- Make sure you add borders for giving it a feel (as shown in the sample images)
    //3- Turn the "Raycast Target" off for every image
    //For Vertical 
    //1- Make sure you give a zoom transforms. They will zoom to the when under range
    //For Horizontal
    //1- Manually and correctly give the "CurrentIndex" according to there position in the content folder(starting from 0)
    //If anything is missing, tally from the working examples.
    public class HorizontalSlotMachine : MonoBehaviour
    {

        [SerializeField] SimpleScrollSnap slot;

        [SerializeField] float speed = 2000f;

        [Tooltip("1/10 of the actual speed")]
        [SerializeField] float slowSpeed = 200f;

        [SerializeField] GameObject image;
        int counter = 0;

        bool canSpin = true;
        private void OnEnable()
        {
            counter = 0;
            canSpin = true;
            image.SetActive(false);
        }
        public void OnSpin()
        {
           
            if (!canSpin) return;
            Debug.Log("Under On Spin");
            GetComponent<Button>().interactable = false;
            canSpin = false;
            image.SetActive(true);

            //Invoke("CanSpin", 3f);

            slot.AddVelocity(Random.Range(3500, 5000) * -Vector2.right);
            //slot.AddVelocity(500 * -Vector2.right);
        }
        void IncreaseSpeed()
        {
            if (counter <= 4)
            {
                counter++;
                slot.AddVelocity(speed * -Vector2.right);
                Invoke("IncreaseSpeed", 0.3f);
            }
            else
            {
                slot.AddVelocity(Random.Range(100, 500) * -Vector2.right);

                counter += 5; //Increases the counter, thus decreasing the lifespan
                Invoke("SlowSpeed", 0.5f);
            }
        }
        void SlowSpeed()
        {
            if (counter >= 0)
            {
                counter--;
                slot.AddVelocity(200 * Vector2.right);
                Invoke("SlowSpeed", 0.2f);
            }
            else
            {
                //StopMovement();
                slot.AddVelocity(50 * Vector2.right);
            }
        }
        void StopMovement()
        {
            slot.GetComponent<ScrollRect>().StopMovement();
        }
        public void CanSpin()
        {
            Debug.Log("can spin to: " + canSpin);
            canSpin = true;
            GetComponent<Button>().interactable = true;
            image.SetActive(false);
        }
    }
}