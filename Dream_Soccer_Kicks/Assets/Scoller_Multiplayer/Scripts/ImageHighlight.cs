﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DanielLochner.Assets.SimpleScrollSnap
{
    public class ImageHighlight : MonoBehaviour
    {
        [SerializeField] SimpleScrollSnap scrollSnap;
        [SerializeField] int currentIndex;
    [SerializeField] RectTransform pointer;
        PointerRotation pointerRotation;
        Animator pointerAnimator;

        RectTransform rectPos;
        [SerializeField] Vector3 ZoomScale = new Vector3(1.5f, 1.5f, 1.5f);

        Vector3 defaultTransform = new Vector3(1, 1, 1);
        Vector3 currentRectPos;

        Image image;
        Color defaultColor;

        float currentPos;
        bool rotate = false;
        float t;

        void Start()
        {
            rectPos = GetComponent<RectTransform>();
            image = GetComponent<Image>();

            defaultColor = image.color;
            Time.timeScale = 0.25f;

            pointerRotation = pointer.GetComponent<PointerRotation>();
            pointerAnimator = pointer.GetComponent<Animator>();
        }

        void Update()
        {
            //if (transform.position.x >= 780 && transform.position.x <= 1160f)
            if (scrollSnap.DetermineNearestPanel() == currentIndex)
            {
                image.color = new Color(255f, 255f, 255f);
                if (!rotate)
                {
                    rotate = true;
                    pointerAnimator.SetBool("rotation", true);
                }
            }
            else
            {
                image.color = defaultColor;
                rotate = false;
            }
        }
    }
}