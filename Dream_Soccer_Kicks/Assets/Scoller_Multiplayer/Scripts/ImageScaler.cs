﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DanielLochner.Assets.SimpleScrollSnap
{
    public class ImageScaler : MonoBehaviour
    {
        [SerializeField] SimpleScrollSnap scrollSnap;
        [SerializeField] int currentIndex;

        [SerializeField] RectTransform target;

        RectTransform rectPos;
        [SerializeField] Vector3 ZoomScale = new Vector3(1.5f, 1.5f, 1.5f);
        Vector3 defaultTransform = new Vector3(1, 1, 1);
        Vector3 currentRectPos;

        float currentPos;
        bool zoomed = false;
        float t;
        // Start is called before the first frame update
        void Start()
        {
            rectPos = GetComponent<RectTransform>();
        }

        // Update is called once per frame
        void Update()
        {
            //if (transform.position.y >= 400 && transform.position.y <= 600)
            if (scrollSnap.DetermineNearestPanel() == currentIndex)
            {
                if (!zoomed)
                {
                    t = 0f;
                    StartCoroutine(IncreaseScale());
                }
                GameConstants.MultiplayerOpponentAvatarNumber = currentIndex;
               // Debug.Log("MultiplayerOpponentAvatarNumber " + GameConstants.MultiplayerOpponentAvatarNumber);
            }
            else
            {
                if (zoomed)
                {
                    t = 0f;
                    currentRectPos = new Vector3(rectPos.localScale.x, rectPos.localScale.y, rectPos.localScale.z);
                    StartCoroutine(DecreaseScale());
                }
       //         Debug.Log("Currentindex is else" + currentIndex);

            }
        }
        IEnumerator IncreaseScale()
        {
            zoomed = true;
            while (t < 60)
            {
                rectPos.localScale = Vector3.Lerp(defaultTransform, ZoomScale, t);
                t += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
        }
        IEnumerator DecreaseScale()
        {
            zoomed = false;
            while (t < 40)
            {
                rectPos.localScale = Vector3.Lerp(currentRectPos, defaultTransform, t);
                t += Time.deltaTime;
                yield return new WaitForFixedUpdate();
            }
        }
    }
}