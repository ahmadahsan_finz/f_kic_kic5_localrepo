﻿using UnityEngine;
using UnityEngine.UI;

namespace DanielLochner.Assets.SimpleScrollSnap
{
    public class SlotMachine : MonoBehaviour
    {
        public static SlotMachine instance;
        [SerializeField] SimpleScrollSnap slot;

        [SerializeField] float speed = 2000f;

        [SerializeField] int counter = 0;

        bool canSpin = true;
        private void Awake()
        {
            if(instance==null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        public void EnableCanSpin()
        {
            canSpin = true;
        }
        public void OnSpin()
        {
            counter = 0; //ahmad added
            if (!canSpin) return;

            canSpin = false;
            GetComponent<Button>().interactable = false;
         
         //   Invoke("IncreaseSpeed", 0.3f);
            Invoke("IncreaseSpeed", 0.3f);

        }
        void IncreaseSpeed()
        {
            if (counter <= 10)
            {
                counter++;
                slot.AddVelocity(speed * Vector2.up);
                Invoke("IncreaseSpeed", 0.3f);
             //   Debug.Log("counter is if " + counter);

            }
            else
            {
               
                slot.AddVelocity(Random.Range(1000, 5000) * Vector2.up);

                counter+=5; //Increases the counter, thus decreasing the lifespan
                Invoke("SlowSpeed", 0.5f);
             //   Debug.Log("counter is else " + counter);
            }
        }
        void SlowSpeed()
        {
            float s = slot.CurrentVelocity().y - 20f;
            slot.AddVelocity(Vector2.up * -s);

        }
        void StopMovement()
        {
            slot.GetComponent<ScrollRect>().StopMovement();
        }
        public void CanSpin()
        {
            //This is the reset button
            canSpin = true;
            GetComponent<Button>().interactable = true;
            counter = 0;
        }
    }
}