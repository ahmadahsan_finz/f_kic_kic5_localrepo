using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AlertDialogManager : MonoBehaviour
{
    [SerializeField] Button GoToShopButton;
    [SerializeField] GameObject CoinImage;
    [SerializeField] Button backButton;
    [SerializeField] Button WatchAdButton;

    private void OnEnable()
    {
        backButton.interactable = true;
        GoToShopButton.interactable = true;
        WatchAdButton.interactable = true;

        GoToShopButton.gameObject.SetActive(false);

        PluginCallBackManager.RewardNotEnoughCoins_ += GiveRewardAdWatched;

      //  Invoke("enableGoToShopButton", 2f);
    }

    public void enableGoToShopButton()
    {
        GoToShopButton.gameObject.SetActive(true);
    }
    public void GotoShopClicked()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            /**//* Time.timeScale = 1;
             MenuManager.Instance.DisableAll();
             this.gameObject.SetActive(false);

             UIAnimations.instance.MainButton(2);
             MenuManager.Instance.Loading(true);
             SceneManager.LoadScene("Menus");*//*
            // AdController.instance.HideBannerAd();
            Debug.Log(" Go to shop clicked....");
            Time.timeScale = 1;
            if (LevelEndCelebrationManager.instance != null)
            {
                LevelEndCelebrationManager.instance.DisableLevelEnd();

            }
            MenuManager.Instance.DisableAll();
            MenuManager.Instance.LevelMenuLoadBool = false;
            //    MenuManager.Instance.UIPlayer.SetActive(true);
            MenuManager.Instance.EnableShopMenu();                         //taking to mission popup menu
            this.gameObject.SetActive(false);
            MenuManager.Instance.Loading(true); //Start Loading Screen
            MenuManager.Instance.sceneChange();                                                  //changing scene to menus
            GamePlayManager.instance.ResetScore();           //reset score //important*/
            Time.timeScale = 0;

            //   MenuManager.Instance.DisableAll();

            this.gameObject.SetActive(false);

            MenuManager.Instance.MainPanels[2].gameObject.SetActive(true);

        }
        else
        {
            Time.timeScale = 1;

            MenuManager.Instance.DisableAll();

            this.gameObject.SetActive(false);

            UIAnimations.instance.MainButton(2);
        }
        // Time.timeScale = 1;

        //        MenuManager.Instance.LevelMenuLoadBool = false;
        //    MenuManager.Instance.UIPlayer.SetActive(true);

        /*     MenuManager.Instance.MainPanels[1].gameObject.SetActive(false);

           MenuManager.Instance.MainPanels[2].gameObject.SetActive(true);
 */
    }
    public void watchAdClicked()
    {

        GenericVariables.RewardAdIndex = 10;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }

    public void GiveRewardAdWatched()
    {
        GenericVariables.RewardAdIndex = -1;
        StartCoroutine(DelayGiveRewardAdWatched());
    }

    IEnumerator DelayGiveRewardAdWatched()
    {
        MenuManager.Instance.moveObject("coins", CoinImage);
        MenuManager.Instance.moveConsumable();

        backButton.interactable = false;
        GoToShopButton.interactable = false;
        WatchAdButton.interactable = false;


        yield return new WaitForSecondsRealtime(2f);

        GenericVariables.AddCoins(100);
        this.gameObject.SetActive(false);

    }

    public void ClickCloseButton()
    {
        this.gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        PluginCallBackManager.RewardNotEnoughCoins_ -= GiveRewardAdWatched;

    }
}