﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomTabPanelScript : MonoBehaviour
{
    public static BottomTabPanelScript instance;
    // Start is called before the first frame update
   public Image[] newNotificationArray;

    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            //Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        EnableNotificationIndicatorOnCustomizationButton();
    }


    public void EnableNotificationIndicatorOnCustomizationButton()
    {
        if (GameConstants.GetNewNotificationCustomizationMenu() == 1)
        {
            newNotificationArray[0].gameObject.SetActive(true);
        }
        else
        {
            newNotificationArray[0].gameObject.SetActive(false);

        }
    }
   /* public void OnClickCharacterCustomization()
    {
        newNotificationArray[0].gameObject.SetActive(false);

    }*/
}
