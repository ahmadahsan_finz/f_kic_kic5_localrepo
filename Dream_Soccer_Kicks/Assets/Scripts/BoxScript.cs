﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class BoxScript : MonoBehaviour
{

    bool hithappenned;
    public GameObject FloatingText;
    [SerializeField] WaitForSeconds wait1Sec = new WaitForSeconds(1f);

    protected void OnCollisionEnter(Collision collision)
    {
        string tag=collision.gameObject.tag;
     //   Debug.Log("OnCollisionEnter Score inside boxscript tag iss   " + tag);
        if (tag.Equals("ground") && !hithappenned)
        {
            hithappenned = true;
            GameConstants.score = GameConstants.score + GamePlayManager.boxhit;
  //          Debug.Log("Score inside boxscript " + GameConstants.score);
            StartCoroutine(BoxesDestroyer());
        }
       
    }
    IEnumerator BoxesDestroyer()          //to destroy the box we hit after some time
    {
     //   Debug.Log("BoxesDestroyer Boxes  ");
     //   yield return new WaitForSeconds(1);
        yield return wait1Sec;
        HudManager.instance.updateStars();
        HudManager.instance.updateScore();
       
        int scoreValue =20;
        Vector3 offset = new Vector3(0, 0.5f, 0);
        var Multiplier = Instantiate(FloatingText, transform.position, Quaternion.identity);           //floating score text popup 
        Multiplier.GetComponent<TextMesh>().text = scoreValue.ToString();
        Multiplier.transform.position += offset;

        GamePlayManager.instance.LevelCompleteCheck();      //check scores after box destroy

        this.gameObject.SetActive(false);

    }
}
