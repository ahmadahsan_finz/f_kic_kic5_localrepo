using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChestRoomMenumanager : MonoBehaviour
{
    [SerializeField] GameObject ContinueButton;
    [SerializeField] GameObject Keys3xButton;
    [SerializeField] GameObject[] KeysArray;
    [SerializeField] GameObject[] BoxArray;
    [SerializeField] GameObject[] RewardUnderBoxArray;
    [SerializeField] Image BestRewardImage;

    [SerializeField] Sprite[] RewardBallImages;
    [SerializeField] Sprite CoinSprite;

    [SerializeField] int keysCollected;
    [SerializeField] int PressedBoxNumber; //initial is 0
    [SerializeField] int CoinsAmountReward; //initial is 0
    [SerializeField] int RewardFootballNumber; //initial is 0
    [SerializeField] int RewardFootballImageNumber;
    [SerializeField] bool Key3XButtonPressed = false;
    [SerializeField] bool SpecialRewardAvailable = false;


    private void OnEnable()
    {
        keysCollected = GameConstants.GetCollectedRewardKeys();
        UpdateCollectedKeys();
        StartCoroutine(EnableDisableContinueButton(false));
        Key3XButtonPressed = false;
        EnableDisbaleKeys3xButton(false);
        EnableAllChestBoxes();

        if (FetchFootballReward())          //fetch football reward to be given at 3rd key usage after watching ad
        {
            Debug.Log("Football will be shown in best reward");
            SpecialRewardAvailable = true;
        }
        else
        {                                                       //if football reward is not available
            Debug.Log("Coins will be shown in best reward");
            SpecialRewardAvailable = false;

            BestRewardImage.sprite = CoinSprite;
        }

        PluginCallBackManager.ChestBoxKeys3xRewardAdComplete_ += give3xKeysReward;
    }

    public void EnableAllChestBoxes()               //enable all chest boxes in menu
    {
        for (int i = 0; i < BoxArray.Length; i++)
        {
            BoxArray[i].SetActive(true);
            BoxArray[i].GetComponent<Button>().interactable = true;
            BoxArray[i].GetComponent<Animator>().Play("ChestBoxAnim3");
        }
    }

    public void disableKeys()
    {
        KeysArray[keysCollected].GetComponent<Animation>().Play();
        KeysArray[keysCollected].SetActive(false);
        GameConstants.SetCollectedRewardKeys(keysCollected);
    }

    public void UpdateCollectedKeys()       //update remaining keys
    {
        for (int a = 0; a < KeysArray.Length; a++)           //disable all keys
        {
            KeysArray[a].SetActive(true);
        }
        /*    for(int a=0;a< KeysArray.Length; a++)           //disable all keys
           {
               KeysArray[a].SetActive(false);
           }    

           for(int i=0;i< keysCollected; i++)          //enable remaining keys
           {
               KeysArray[i].SetActive(true);
           }
           GameConstants.SetCollectedRewardKeys(keysCollected);*/
    }

    public void OnClickChestBox(int boxNumber)          //on click chest box
    {
        PressedBoxNumber = boxNumber;

        if (keysCollected > 1)     //keys are more than 0
        {
            BoxArray[boxNumber].GetComponent<Animator>().Play("ChestBoxAnim");
            BoxArray[boxNumber].GetComponent<Button>().interactable = false;
            //    Invoke("DisableChestBox", 1f);
            DisableChestBox();

            //BoxArray[boxNumber].SetActive(false);
            ShowUnlockedRewardAfterChestPress();
            keysCollected--;
            //   UpdateCollectedKeys();
            disableKeys();

        }
        else if (keysCollected == 1 && !Key3XButtonPressed)       //0 keys remaining
        {
            BoxArray[boxNumber].GetComponent<Animator>().Play("ChestBoxAnim");
            BoxArray[boxNumber].GetComponent<Button>().interactable = false;

            //    Invoke("DisableChestBox", 1f);
            DisableChestBox();
            ShowUnlockedRewardAfterChestPress();
            keysCollected--;
            //  UpdateCollectedKeys();
            disableKeys();
            BoxIdleAnimationPlay();

            StartCoroutine(EnableDisableContinueButton(true));
            EnableDisbaleKeys3xButton(true);
        }
        else if (keysCollected == 1 && Key3XButtonPressed)       //0 keys remaining
        {
            BoxArray[boxNumber].GetComponent<Animator>().Play("ChestBoxAnim");
            BoxArray[boxNumber].GetComponent<Button>().interactable = false;

            //    Invoke("DisableChestBox", 1f);
            DisableChestBox();

            BoxIdleAnimationPlay();

            ShowUnlockedRewardAfterChestPress();
            keysCollected--;
            //  UpdateCollectedKeys();
            disableKeys();

            StartCoroutine(EnableDisableContinueButton(true));
            // EnableDisbaleKeys3xButton(true);
        }

    }

    public void DisableChestBox()
    {
        BoxArray[PressedBoxNumber].SetActive(false);

    }
    public void BoxIdleAnimationPlay()
    {
        for(int i=0;i<BoxArray.Length;i++)
        {
            if (BoxArray[i].activeSelf)
            {
                BoxArray[i].GetComponent<Animator>().Play("ChestBoxAnim2");
            }
        }
    }
    public void BoxScaleUpDownAnimationPlay()
    {
        for (int i = 0; i < BoxArray.Length; i++)
        {
            if (BoxArray[i].activeSelf)
            {
                BoxArray[i].GetComponent<Animator>().Play("ChestBoxAnim3");
            }
        }
    }
    public void ShowUnlockedRewardAfterChestPress()
    {
        if (keysCollected == 3)        //first box open  //giving coins
        {
            Debug.Log("Coins given in RewardBallImages");

            GiveCoinReward();
        }
        else if (keysCollected == 2)        //2nd box open
        {
            Debug.Log("Coins given in RewardBallImages");

            GiveCoinReward();

        }
        else if (keysCollected == 1 && Key3XButtonPressed)        //3rd box open
        {

            if (SpecialRewardAvailable)
            {
                Debug.Log("Football given in RewardBallImages");
                GiveFootballReward();
            }
            else
            {
                Debug.Log("Coins given in RewardBallImages");

                GiveCoinReward();
            }

        }
        else if (keysCollected == 1 && !Key3XButtonPressed)
        {


            GiveCoinReward();

        }
    }

    public bool FetchFootballReward()
    {
        for (int i = 0; i < GameConstants.RewardFootballstoOfferInChest.Count; i++)
        {
            RewardFootballImageNumber = i;

            RewardFootballNumber = GameConstants.RewardFootballstoOfferInChest[RewardFootballImageNumber];
            if (GenericVariables.checkUnlockedFootballWithStatus(RewardFootballNumber) == 0)
            {
                BestRewardImage.sprite = RewardBallImages[RewardFootballImageNumber];
                return true;
            }

        }
        return false;
    }

    public void GiveFootballReward()
    {
        //for (int i = 0; i < GameConstants.RewardFootballstoOfferInChest.Count; i++)
        //{
        //    RewardFootballNumber = GameConstants.RewardFootballstoOfferInChest[i];
        //    if (GenericVariables.checkUnlockedFootballWithStatus(RewardFootballNumber) == 0)
        //    {
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(0).GetComponent<Image>().sprite = RewardBallImages[RewardFootballImageNumber];
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(0).GetComponent<Animation>().Play();
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(1).gameObject.SetActive(false);
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(2).gameObject.SetActive(true);

        GenericVariables.saveUnlockedFootballWithStatus(RewardFootballNumber, 1);

        //     return true; 
        //    }
        //}
        //return false;
    }

    public void GiveCoinReward()
    {
        GenerateRandomCoins();
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(0).GetComponent<Image>().sprite = CoinSprite;
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(0).GetComponent<Animation>().Play();
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(1).gameObject.SetActive(true);
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(1).GetComponent<Text>().text = CoinsAmountReward.ToString();
        RewardUnderBoxArray[PressedBoxNumber].transform.GetChild(2).gameObject.SetActive(false);
        GenericVariables.AddCoins(CoinsAmountReward);
    }
    public void GenerateRandomCoins()
    {
        int rand = Random.Range(0, 4);
        if (rand == 0)
        {
            CoinsAmountReward = 50;
        }
        else if (rand == 1)
        {
            CoinsAmountReward = 100;
        }
        else if (rand == 2)
        {
            CoinsAmountReward = 150;
        }
        else if (rand == 3)
        {
            CoinsAmountReward = 200;
        }
    }

    public IEnumerator EnableDisableContinueButton(bool Value)
    {
        if (Value == true)
        {
            yield return new WaitForSeconds(1f);

        }
        ContinueButton.SetActive(Value);

        ContinueButton.GetComponent<Button>().interactable = Value;

    }

    public void EnableDisbaleKeys3xButton(bool Value)
    {
        Keys3xButton.SetActive(Value);
        Keys3xButton.GetComponent<Button>().interactable = Value;
    }

    public void OnClick3xKeysButton()
    {
        GenericVariables.RewardAdIndex = 8;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
        //StartCoroutine( EnableDisableContinueButton(false));


    }

    public void give3xKeysReward()
    {
      //  EnableAllChestBoxes();
        BoxScaleUpDownAnimationPlay();
        Key3XButtonPressed = true;
        Keys3xButton.GetComponent<Button>().interactable = false;
        StartCoroutine( EnableDisableContinueButton(false));

        keysCollected = 3;
        UpdateCollectedKeys();
        GenericVariables.RewardAdIndex = -1;

    }
    public void OnClickContinueButton()
    {
        ContinueButton.GetComponent<Button>().interactable = false;

        HudManager.instance.KeyCollectionHudReset();

        if (GamePlayManager.instance. LevelStatus == GamePlayManager.LevelCompleteStatus.lose)
        {
            MenuManager.Instance.ActiveLevelFailPanel();

        }
        else if (GamePlayManager.instance.LevelStatus == GamePlayManager.LevelCompleteStatus.win)
        {
            StartCoroutine(MenuManager.Instance.ActiveLevelSuccessPanel());
        }

        this.gameObject.SetActive(false);

    }
}
