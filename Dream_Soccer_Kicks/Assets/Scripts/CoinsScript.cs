﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsScript : MonoBehaviour
{
	public GameObject FloatingText;
	public float speed;
    private void Start()
    {
		iTween.RotateBy(gameObject, iTween.Hash(
				  "z", 3.0f,
				  "time", 5f,
				  "easetype", "linear",
				  "looptype", iTween.LoopType.loop
			  ));
	}
	// Start is called before the first frame update
	public void OnTriggerEnter(Collider other)
	{
		//Debug.Log("OnTriggerEnter  ");
		string tag = other.gameObject.tag;
		if (tag.Equals("Ball"))
		{
			GameConstants.BonusCoinsCount++;

			if (SoundManagerNew.Instance != null)
			{
				SoundManagerNew.Instance.playSound("BallHitGoalExtra");

			}
			GameConstants.BonusCoinsCount++;

			//GenericVariables.AddCoins(1);
		

			/*Vector3 offset = new Vector3(0, 1.2f, 0);
			var CoinText = Instantiate(FloatingText,  gameObject.transform.position, Quaternion.identity);           //floating score text popup 
																														   //Multiplier.GetComponent<TextMesh>().fontSize = 150;
			CoinText.GetComponent<TextMesh>().color = Color.yellow;
			CoinText.GetComponent<TextMesh>().text = "+";
			CoinText.transform.position += offset;

			iTween.MoveTo(CoinText, iTween.Hash("position",new Vector3(-8,15,0), "time", 3f, "easetype", iTween.EaseType.linear));*/
			gameObject.SetActive(false);
		}
	}
  /*  private void Update()
    {
		Quaternion angle = Quaternion.Euler(0f, 180f, 0);
		transform.rotation = Quaternion.RotateTowards(transform.rotation, angle, speed * Time.deltaTime);
	}*/
}
