﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class CustomiazationMenuScript : MonoBehaviour
{
    public static CustomiazationMenuScript instance;
    public GameObject UIPlayer;
    public Image[] NotificationsIcon;
    public GameObject BackButton;
    public int ItemNumber;
    public Category categoryState;
    public enum Category
    {
        kit,
        skin,
        football,
        celebration,
        shorts
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            //Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    private void OnEnable()
    {
        PluginCallBackManager.CustomizationUnlockRewardAdComplete_ += giveCustomizationReward;

        CustomizationScript.instance.onBodyPressed();


        CustomizationScript.instance.KitsLockingUnlocking();        //refersh all the locks and video icons
        CustomizationScript.instance.ShortsLockingUnlocking();        //refersh all the locks and video icons
        CustomizationScript.instance.SkinsLockingUnlocking();
        CustomizationScript.instance.FootballsLockingUnlocking();
        CustomizationScript.instance.CelebrationsLockingUnlocking();

        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            //     Debug.Log("Customization menu on enable");
            AdController.instance.HideBannerAd();
            CustomizationScript.instance.KitsLockingUnlocking();
            EnableDisableBackButton(true);
        }
        else
        {
            CustomizationScript.instance.KitsLockingUnlocking();
            EnableDisableBackButton(false);
        }

        UIPlayer = MenuManager.Instance.SelectedPlayer;
        UIPlayer.transform.SetParent(this.gameObject.transform);
        UIPlayer.transform.localPosition = new Vector3(1876f, -3450.83f, 249.3f);
        UIPlayer.transform.localScale = new Vector3(1.15f, 1.15f, 1.15f);
        UIPlayer.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        UIPlayer.transform.rotation = Quaternion.Euler(0, 180, 0);
        UIPlayer.GetComponent<spinLogic>().enabled = true;

        /*   UIPlayer.GetComponent<Rigidbody>().angularVelocity=new Vector3(0,0,0);
           UIPlayer.transform.rotation = Quaternion.Euler(0, 180, 0);
   */
        CustomizationScript.instance.ApplyOrginalAnimator(); //apply idle animation

        checkNotificationOnIcons();  //check Notifications Pointer icon All Buttons and panels
    }

    public void checkNotificationOnIcons()
    {
        //if (index == 2)
        //{

        switch (GameConstants.GetNewNotificationCustomizationMenu())
        {
            case 1:
                BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(true);

                break;
            default:
                BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

                break;
        }

        switch (GameConstants.GetNewNotification_Shirt_Menu())
        {
            case 1:
                NotificationsIcon[0].gameObject.SetActive(true);

                break;
            default:
                NotificationsIcon[0].gameObject.SetActive(false);

                break;
        }
        switch (GameConstants.GetNewNotification_Shorts_Menu())
        {
            case 1:
                NotificationsIcon[1].gameObject.SetActive(true);

                break;
            default:
                NotificationsIcon[1].gameObject.SetActive(false);

                break;
        }

        switch (GameConstants.GetNewNotificationFootballMenu())
            {
                case 1:
                    NotificationsIcon[2].gameObject.SetActive(true);

                    break;
                default:
                    NotificationsIcon[2].gameObject.SetActive(false);

                    break;
        }
        //  }
        //else if (index == 3)
        //{

        switch (GameConstants.GetNewNotification_Celebration_Menu())
        {
            case 1:
                NotificationsIcon[3].gameObject.SetActive(true);

                break;
            default:
                NotificationsIcon[3].gameObject.SetActive(false);

                break;
        }


        // }
    }
    public void DisableNewNotication(int index)
    {
        if (index == 0)        //shirt Notification
        {

            GameConstants.SetNewNotification_Shirt_Menu(0);
            NotificationsIcon[index].gameObject.SetActive(false);
            //    BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

        }
        if (index == 1)        //short Notification
        {

            GameConstants.SetNewNotification_Shorts_Menu(0);
            NotificationsIcon[index].gameObject.SetActive(false);
            //    BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

        }

        if (index == 2)        //football Notification
        {

            GameConstants.SetNewNotificationFootballMenu(0);
            NotificationsIcon[index].gameObject.SetActive(false);
            //      BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

        }
        if (index == 3)        //celebration Notification
        {

            GameConstants.SetNewNotification_Celebration_Menu(0);
            NotificationsIcon[index].gameObject.SetActive(false);
            //    BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

        }
        if (index == 5)        //customization Button on Bottom panel Notification
        {

            GameConstants.SetNewNotificationCustomizationMenu(0);
            BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);
            //    BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

        }

        if (/*GameConstants.GetNewNotificationCustomizationMenu() == 0
            &&*/ GameConstants.GetNewNotificationFootballMenu() == 0
            && GameConstants.GetNewNotification_Celebration_Menu() == 0
            && GameConstants.GetNewNotification_Shirt_Menu() == 0  )
          //  && GameConstants.GetNewNotification_Shorts_Menu() == 0)  //both tabs are clicked than 
        {
            GameConstants.SetNewNotificationCustomizationMenu(0);

            BottomTabPanelScript.instance.newNotificationArray[0].gameObject.SetActive(false);

        }
    }

    public void EnableDisableBackButton(bool active)
    {
        BackButton.SetActive(active);
    }
    public void BackButtonPressed()
    {
        // AdController.instance.ShowBannerAd();
        Time.timeScale = 1;
        GamePlayManager.instance.SetSelectedPlayerTextures();
        // EnableDisableBackButton(false);
        this.gameObject.SetActive(false);
    }
    public void OnClickGetCustomizationRewardAdButton()
    {
        GenericVariables.RewardAdIndex = 7;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    /*public void OnClickGetSkinButton(int itemNum)
    {
        GenericVariables.RewardAdIndex = 8;
        ItemNumber = itemNum;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }public void OnClickGetFootballButton(int itemNum)
    {
        GenericVariables.RewardAdIndex = 9;
        ItemNumber = itemNum;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }public void OnClickGetCelebrationButton(int itemNum)
    {
        GenericVariables.RewardAdIndex = 10;
        ItemNumber = itemNum;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }*/
    public void giveCustomizationReward()     //costume give away working to be done here
    {
        if (categoryState == Category.kit)
        {
            // Time.timeScale = 1;
            GenericVariables.RewardAdIndex = -1;
            //RewardOfferNumber = GameConstants.RewardBodystoOffer[0];
            GenericVariables.saveUnlockedBodyWithStatus(ItemNumber, 2); //save with status
            GenericVariables.saveBody(ItemNumber);
            CustomizationScript.instance.BodyLockImages[ItemNumber].gameObject.SetActive(false);
            CustomizationScript.instance.ChangeBody(ItemNumber);
            // GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
       //     CustomizationScript.instance.WatchButton.SetActive(false);
            CustomizationScript.instance.KitsLockingUnlocking();        //refersh all the locks and video icons

            AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.kit, ItemNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
        }
        else if (categoryState == Category.shorts)
        {
            // Time.timeScale = 1;
            GenericVariables.RewardAdIndex = -1;
            //RewardOfferNumber = GameConstants.RewardBodystoOffer[0];
            GenericVariables.saveUnlockedShortsWithStatus(ItemNumber, 2); //save with status
            GenericVariables.saveShorts(ItemNumber);
            CustomizationScript.instance.ShortsLockImages[ItemNumber].gameObject.SetActive(false);
            CustomizationScript.instance.ChangeShorts(ItemNumber);
            // GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
       //     CustomizationScript.instance.WatchButton.SetActive(false);
            CustomizationScript.instance.ShortsLockingUnlocking();        //refersh all the locks and video icons

            AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.shorts, ItemNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
        }
        else if (categoryState == Category.skin)
        {
            GenericVariables.RewardAdIndex = -1;
            GenericVariables.saveUnlockedSkinWithStatus(ItemNumber,2);
            GenericVariables.saveSkin(ItemNumber);
            CustomizationScript.instance.SkinLockImages[ItemNumber].gameObject.SetActive(false);
            CustomizationScript.instance.ChangeSkin(ItemNumber);
            //  GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
         //   CustomizationScript.instance.WatchButton.SetActive(false);

            CustomizationScript.instance.SkinsLockingUnlocking();        //refersh all the locks and video icons
            AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.skin, ItemNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
        }
        else if (categoryState == Category.football)
        {
            GenericVariables.RewardAdIndex = -1;

            GenericVariables.saveUnlockedFootballWithStatus(ItemNumber,2);
            GenericVariables.saveFootball(ItemNumber);
            CustomizationScript.instance.FootballLockImages[ItemNumber].gameObject.SetActive(false);
            CustomizationScript.instance.ChangeFootball(ItemNumber);
            //   GameConstants.SetGameOverRewardClaim(ItemNumber);         //setting as claimed
          //  CustomizationScript.instance.WatchButton.SetActive(false);

            CustomizationScript.instance.FootballsLockingUnlocking();        //refersh all the locks and video icons

            AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, ItemNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
        }
        else if (categoryState == Category.celebration)
        {
            GenericVariables.RewardAdIndex = -1;
            GenericVariables.saveUnlockedCelebrationWithStatus(ItemNumber, 2);
            GenericVariables.saveCelebration(ItemNumber);
            CustomizationScript.instance.CelebrationLockImages[ItemNumber].gameObject.SetActive(false);
            CustomizationScript.instance.ChangeCelebration(ItemNumber);
            // GameConstants.SetGameOverRewardClaim(ItemNumber);         //setting as claimed
           // CustomizationScript.instance.WatchButton.SetActive(false);

            CustomizationScript.instance.CelebrationsLockingUnlocking();        //refersh all the locks and video icons

            AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.celebration, ItemNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
        }
    }
      
       

    private void OnDisable()
    {
        
        PluginCallBackManager.CustomizationUnlockRewardAdComplete_ -= giveCustomizationReward;

        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            //  Debug.Log("Customization menu on disable");

            AdController.instance.ShowBannerAd();

            EnableDisableBackButton(false);
        }
    }
    
}
