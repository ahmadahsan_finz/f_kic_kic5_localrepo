﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DartboardDestruction : MonoBehaviour
{
	public GameObject[] Circles;
	[SerializeField] WaitForSeconds waitSec = new WaitForSeconds(0.5f);

	protected void OnTriggerEnter(Collider other)
	{
		//Debug.Log("OnTriggerEnter  ");
		string tag = other.gameObject.tag;
		if (tag.Equals("Ball"))
        {
			
			if (gameObject.name.Contains("Coin") && !GamePlayManager.instance.RewardKickItemPicked)
			{
				Circles[2].transform.position = gameObject.transform.position;
				Vector3 offset = new Vector3(0, 0, 0.5f);
				Circles[2].transform.position += offset;
				//Circles[1].transform.localRotation = Quaternion.Euler(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
				Circles[2].transform.localRotation = gameObject.transform.localRotation;
				Circles[2].transform.localScale = gameObject.transform.localScale;
				Instantiate(Circles[2]);
				//StartCoroutine(DelayInstantiate(1));
				gameObject.SetActive(false);

			}
			else if (gameObject.name.Contains("Diamond") && !GamePlayManager.instance.RewardKickItemPicked)
			{
				Circles[3].transform.position = gameObject.transform.position;
				Vector3 offset = new Vector3(0, 0, 0.5f);
				Circles[3].transform.position += offset;
				//Circles[1].transform.localRotation = Quaternion.Euler(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
				Circles[3].transform.localRotation = gameObject.transform.localRotation;
				Circles[3].transform.localScale = gameObject.transform.localScale;
				Instantiate(Circles[3]);
				//StartCoroutine(DelayInstantiate(1));
				gameObject.SetActive(false);

			}
			else if (gameObject.name.Contains("Fire") && !GamePlayManager.instance.RewardKickItemPicked)
			{
				Circles[4].transform.position = gameObject.transform.position;
				Vector3 offset = new Vector3(0, 0, 0.5f);
				Circles[4].transform.position += offset;
				//Circles[1].transform.localRotation = Quaternion.Euler(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
				Circles[4].transform.localRotation = gameObject.transform.localRotation;
				Circles[4].transform.localScale = gameObject.transform.localScale;
				Instantiate(Circles[4]);
				//StartCoroutine(DelayInstantiate(1));
				gameObject.SetActive(false);

			}
			else if (gameObject.name.Contains("Energy") && !GamePlayManager.instance.RewardKickItemPicked)
			{
				Circles[5].transform.position = gameObject.transform.position;
				Vector3 offset = new Vector3(0, 0, 0.5f);
				Circles[5].transform.position += offset;
				//Circles[1].transform.localRotation = Quaternion.Euler(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
				Circles[5].transform.localRotation = gameObject.transform.localRotation;
				Circles[5].transform.localScale = gameObject.transform.localScale;
				Instantiate(Circles[5]);
				//StartCoroutine(DelayInstantiate(1));
				gameObject.SetActive(false);

			}
			else if (gameObject.name.Contains("Corner"))
            {
				Circles[1].transform.localPosition = gameObject.transform.localPosition;
				Vector3 offset = new Vector3(0, 0, 0.5f);
				Circles[1].transform.localPosition += offset;
				//Circles[1].transform.localRotation = Quaternion.Euler(gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
				Circles[1].transform.localRotation = gameObject.transform.localRotation;
				Circles[1].transform.localScale = gameObject.transform.localScale;
				Instantiate(Circles[1],Shoot.share.ParentLevelHurdles.transform);
				//StartCoroutine(DelayInstantiate(1));
				gameObject.SetActive(false);

			}
			else if(!GamePlayManager.instance.RewardKickItemPicked)
            {
				Circles[0].transform.localPosition = gameObject.transform.localPosition;
				Vector3 offset = new Vector3(0, 0, 0.5f);
				Circles[0].transform.localPosition += offset;
				Circles[0].transform.localRotation = gameObject.transform.localRotation;
				Circles[0].transform.localScale = gameObject.transform.localScale;

				Instantiate(Circles[0], Shoot.share.ParentLevelHurdles.transform);
				gameObject.SetActive(false);

			}
		}
	}
	IEnumerator DelayInstantiate(int index)
    {
		yield return waitSec;
	
		Instantiate(Circles[index]);
	}
}
