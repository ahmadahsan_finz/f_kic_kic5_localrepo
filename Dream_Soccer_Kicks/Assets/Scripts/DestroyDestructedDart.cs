﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyDestructedDart : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] WaitForSeconds waitSec = new WaitForSeconds(0.5f);

    void Start()
    {
        StartCoroutine(DestroyDelay());
    }

    IEnumerator DestroyDelay()
    {
        //yield return new WaitForSeconds(0.5f);
        yield return waitSec;
        Destroy(gameObject);
    }
}
