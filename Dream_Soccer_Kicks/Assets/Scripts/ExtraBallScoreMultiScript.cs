﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExtraBallScoreMultiScript : MonoBehaviour
{
    public static ExtraBallScoreMultiScript instance;
    public GameObject ExtraBallObject;
    public GameObject PointMove1;
    public GameObject PointMove2;
    public bool ExtraBallBool;
    public bool ExtraBallBoolHit;
    int rand=0;
    private void Awake()
    {
        
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        /*  iTween.MoveBy(gameObject, iTween.Hash(
                   "x", 2,
                "time", 0.2f
  ));*/
         rand = 0;
    //    ExtraBallObject.transform.position = PointMove2.transform.position;
    }

/*    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Ball"))//&& this.ExtraBallObject.CompareTag("ExtraBall"))
        {
            Debug.Log("ExtraBall");
            GameConstants.hitsRemaining++;
            ExtraBallBool = false;
            Destroy(this.ExtraBallObject);
        }
       
    }*/

    public void EnableExtraBall()
    {
        //  iTween.MoveTo(gameObject, {“x”:2, “time”:3, “loopType”:”pingPong”, “delay”:1));
        if (!ExtraBallBool)
        {
         //   Debug.Log("EnableExtraBall");

            ExtraBallBool = true;
            //rand = Random.Range(1, 3);
            rand++;
            //   iTween.MoveTo(ExtraBallObject, iTween.Hash("position", PointMove2.transform.position, "easetype", iTween.EaseType.easeInOutSine, "time", 2));
            if(rand % 2 ==0)
            {
                ExtraBallObject.transform.position = PointMove2.transform.position;
                GameObject obj = Instantiate(ExtraBallObject);
                //PointMove2 = Instantiate(ExtraBallObject);

                iTween.MoveTo(obj, iTween.Hash("position", PointMove1.transform.position, "time", 7f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 2f));
            }
            else
            {
                ExtraBallObject.transform.position = PointMove1.transform.position;
                GameObject obj= Instantiate(ExtraBallObject);
                //  PointMove1 = Instantiate(ExtraBallObject);

                iTween.MoveTo(obj, iTween.Hash("position", PointMove2.transform.position, "time", 7f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 2f));
            }
        }



    }

}
