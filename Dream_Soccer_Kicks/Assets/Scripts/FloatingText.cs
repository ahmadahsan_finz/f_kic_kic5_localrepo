﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float timer;
    // Start is called before the first frame update
    void Start()
    {
        DestroyFloatingText(timer);
    }

    public void DestroyFloatingText(float timer)
    {
        Destroy(gameObject, timer);

    }

}
