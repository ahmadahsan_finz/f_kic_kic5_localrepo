﻿//using System.Diagnostics.PerformanceData;
using UnityEngine;
using System.Collections.Generic;
//using NUnit.Framework.Constraints;


public class GameConstants
{
    public static GameConstants instance;

    public static int groundTexture = 1;

    public static float delayTimeAIShoot = 2f;

  
    public static int[] targetScore = { 50, 70, 100, 125 };

    public static float hitsRemaining = 0;
    
    public static float score = 0;

    public static float goalScored = 0;

    public static float timer = 0;

    public static int MissionPassCoins;

    public static int MissionTargetScore;

    public static int CoinsEarned;

    public static int CoinsEarnedBySubscription= 50;

    public static bool CutsceneIsOn;
    
    public static int LevelNumberPlayed;

    public static int BonusCoinsCount=0;


    public static int MultiplayerOpponentKitNumber = 0;

    public static int MultiplayerOpponentShortNumber = 0;

    public static int MultiplayerOpponentSkinNumber = 0;

    public static int MultiplayerOpponentGKBodyNumber = 0;

    public static int MultiplayerOpponentGKSkinNumber = 0;

    public static int MultiplayerPlayerGKBodyNumber = 0;

    public static int MultiplayerPlayerGKSkinNumber = 0;

    public static int MultiplayerOpponentScore = 0;

    public static int MultiplayerPlayerScore = 0;

    public static int MultiplayerOpponentAvatarNumber = 8;

    public static string MultiplayerOpponentName;

    public static int RewardShotPassCoins;

    public static int MultiplayerWonCoins=500;

    public static bool CanShowRateUsBool = false;

    public static int NumberOfRestarts = 0;

    public static int NumberOfTrophies = 17;

    public static List<int> TrophiesLevelList = new List<int> { 2, 12, 24, 48, 70, 94, 135, 159, 180, 192, 222, 246, 259, 268, 280, 291, 300 };
 
    public static int[] BonusLevelList = { 3,5,8,15,21,23,27,30,35,43,54,59,64,73,79,85,90,94,101,103,106,113,119,121,125,128,133,141,152,157,162,171,177,183,188,198,203,206,213,219,221,225,228,
    233,241,252,257,262,271,277,283,288,298};

    public static int TotalGameOverRewards = 12;     //offering number of rewards on game end

    public static List<int> RewardBodystoOffer = new List<int> { 3, 8, 13, 15 };

    public static List<int> RewardSkinstoOffer = new List<int> { 2};

    public static List<int> RewardFootballstoOffer = new List<int> { 2,6,10,14,21 };

    public static List<int> RewardFootballstoOfferInChest = new List<int> { 1, 3, 4, 5, 7,8,9,11,12,13,15,16,17,18,19,20,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39 };

    public static List<int> RewardCelebrationstoOffer = new List<int> { 3,7 };


    public static void unlockItem(string key)
    {
        PlayerPrefs.SetInt(key, 1);
        PlayerPrefs.Save();
    }

    public static bool isItemUnlocked(string key)
    {
              return PlayerPrefs.GetInt(key, 0) == 1;
        
    }

    //Enum
    public enum MultiplayerModePlayedVia
    {
        menus, gameplay
    }

    public static MultiplayerModePlayedVia multiplayerModePlayedViaState;

    public static bool isSoundOn()
    {
        return 1 == PlayerPrefs.GetInt("soundEnabled", 1); //return true if sound is enabled
    }

    #region shop
    public static int[] ballPrice = new int[] { 100, 200, 300, 400 };
    #endregion
    #region UnityAnalytics

    public static int MaximumLevelPlayed;

    public static int initialMaximumLevelPlayed;

    public static void saveMaximumLevelPlayed(int num)
    {
        PlayerPrefs.SetInt("MaximumLevelPlayed", num);
    }

    public static int getMaximumLevelPlayed()
    {
        return PlayerPrefs.GetInt("MaximumLevelPlayed", 0);
    }

    public static void saveinitialMaximumLevelPlayed(int num)
    {
        PlayerPrefs.SetInt("initialMaximumLevelPlayed ", num);
    }

    public static int getinitialMaximumLevelPlayed()
    {
        return PlayerPrefs.GetInt("initialMaximumLevelPlayed ", 0);
    }

    #endregion


    public static int Mode = 0;
 

    public static void SetMode(int i)
    {
        // 1 = penalties
        Mode = i;
    }
    public static int GetMode()
    {
        return Mode;
    }
   

    public static void setSound(int i)
    {
        PlayerPrefs.SetInt("WCKSounds", i);
    }
    public static int GetSound()
    {
        return PlayerPrefs.GetInt("WCKSounds", 1);
    }
    public static int GetLevelNumber()
    {
        return PlayerPrefs.GetInt("LevelNumber");
    }
    public static void SetLevelNumber(int num)
    {
        PlayerPrefs.SetInt("LevelNumber", num);
    }

    public static void setUserFirstTime()
    {
        PlayerPrefs.SetInt("FirstTime", 1);
    }

    public static void setTargetGoals(int Goals)
    {
        PlayerPrefs.SetInt("TargetGoals", Goals);
    }

    public static int GetTargetGoals()
    {
        return PlayerPrefs.GetInt("TargetGoals", 0);
    }
    public static int showCutScene()
    {
        return PlayerPrefs.GetInt("CutScene", 0);
    }
    

    public static void SetTargetScores(int[] TargetsScores)        //ahmad to store 3 score target scores
    {
    /*    for(int i=0;i<3;i++)
        {
            Debug.Log("setting scores " + TargetsScores[i]);
        }
    */
        PlayerPrefs.SetInt("TargetScore1", TargetsScores[0]);
        PlayerPrefs.SetInt("TargetScore2", TargetsScores[1]);
        PlayerPrefs.SetInt("TargetScore3", TargetsScores[2]);
    }
 
    public static int[] GetTargetScores()                                                   //ahmad to get 3 stored target scores
    {
        int[] Targets=new int[3];
        Targets[0]=PlayerPrefs.GetInt("TargetScore1", 50);
        Targets[1]=PlayerPrefs.GetInt("TargetScore2", 100);
        Targets[2]=PlayerPrefs.GetInt("TargetScore3", 150);
        return Targets;

    }
    public static void SetTargetStars(int[] TargetStars)        //ahmad to store 3 target Stars
    {
        /*    for(int i=0;i<3;i++)
            {
                Debug.Log("setting scores " + TargetsScores[i]);
            }
        */
        PlayerPrefs.SetInt("TargetStars1", TargetStars[0]);
        PlayerPrefs.SetInt("TargetStars2", TargetStars[1]);
        PlayerPrefs.SetInt("TargetStars3", TargetStars[2]);
    }
    public static int[] GetTargetStars()                                                   //ahmad to get 3 stored target Stars
    {
        int[] Targets=new int[3];
        Targets[0]=PlayerPrefs.GetInt("TargetStars1", 50);
        Targets[1]=PlayerPrefs.GetInt("TargetStars2", 100);
        Targets[2]=PlayerPrefs.GetInt("TargetStars3", 150);
        return Targets;

    }
    public static void setTournamentRoundName(string pname)
    {
        PlayerPrefs.SetString("TournamentRoundName", pname);
    }
    public static void setCollectedCoins(int CollectedCoins)
    {
        int PreviousCoins = getCollectedCoins();
        PlayerPrefs.SetInt("TotalCollectedCoins", PreviousCoins + CollectedCoins);
    } 
    public static int getCollectedCoins()
    {
       // return PlayerPrefs.GetInt("TotalCollectedCoins", 0);
        return PlayerPrefs.GetInt("Coins", 0);
    }
    public static void setLevelCleared(int LevelsCleared)
    {
        PlayerPrefs.SetInt("LevelsCleared", LevelsCleared);
    }
    public static int getLevelCleared()
    {
        return PlayerPrefs.GetInt("LevelsCleared",0);
    }
    public static void setLevelClearedStars(int LevelNumber, int StarsCollected)
    {
        PlayerPrefs.SetInt("LevelsCleared"+LevelNumber, StarsCollected);
    }
    public static int getLevelClearedStars(int LevelNumber)
    {
        return PlayerPrefs.GetInt("LevelsCleared"+ LevelNumber, 0);
    }
    public static void setNumberOfFireballs(int Fireballs)
    {
        PlayerPrefs.SetInt("Fireballs" , Fireballs);
    }
    public static int getNumberOfFireballs()
    {
        return PlayerPrefs.GetInt("Fireballs", 0);
    }
    public static void setFirstStartofGame(string FirstStart)
    {
        PlayerPrefs.SetString ("FirstStart", FirstStart);
    }    
    public static string getFirstStartofGame()
    {
       return PlayerPrefs.GetString ("FirstStart", "Yes");
    }
    public static void setTotalLevels(int totalLevels)
    {
        PlayerPrefs.SetInt("TotalLevels", totalLevels);
    }

    public static int getTotalLevels()
    {
        return PlayerPrefs.GetInt("TotalLevels", 300);
    }

    public static void SetMissionNumber(int MissionNumber)
    {
        PlayerPrefs.SetInt("MissionNumber", MissionNumber);

    }
    public static int GetMissionNumber()
    {
        return PlayerPrefs.GetInt("MissionNumber", 1);
    }

    public static void SetGameMode(string Mode)
    {
        PlayerPrefs.SetString("GameMode", Mode);
    }
    public static string GetGameMode()
    {
        return PlayerPrefs.GetString("GameMode", "Classic");
    }
    public static void SetMissionMode(int Mode)
    {
        PlayerPrefs.SetInt("MissionMode", Mode);

    }
    public static int GetMissionMode()
    {
        return PlayerPrefs.GetInt("MissionMode",1);
    }

    public static void SetMissionPassed(int missionNumber)
    {
        PlayerPrefs.SetString("MissionComplete" + missionNumber, "Complete");
    }
    public static string GetMissionPassed(int missionNumber)
    {
        return PlayerPrefs.GetString("MissionComplete" + missionNumber, "UnComplete");
    }

    public static void SetMissionClaim(int missionNumber)
    {
        PlayerPrefs.SetString("MissionClaim" + missionNumber, "Claimed");
    }
    public static string GetMissionClaim(int missionNumber)
    {
        return PlayerPrefs.GetString("MissionClaim" + missionNumber, "UnClaimed");
    }
    public static void setFirstStartUITutorial(string FirstStart)
    {
        PlayerPrefs.SetString("FirstStartUITutorial", FirstStart);
    }
    public static string getFirstStartUITutorial()
    {
        return PlayerPrefs.GetString("FirstStartUITutorial", "Yes");
    }
    public static void setLevel2Tutorial(string FirstStart)
    {
        PlayerPrefs.SetString("Level2Tutorial", FirstStart);
    }
    public static string getLevel2Tutorial()
    {
        return PlayerPrefs.GetString("Level2Tutorial", "Yes");
    }
    public static void setStartGamePlayTutorial(string FirstStart)
    {
        PlayerPrefs.SetString("StartGameplayTutorial", FirstStart);
    }
    public static string getStartGamePlayTutorial()
    {
        return PlayerPrefs.GetString("StartGameplayTutorial", "Yes");
    }

    public static void setFortuneWheelShowOnLevel2Availability(string value)        //we will show fortune on level2 only one time
    {
        PlayerPrefs.SetString("FortuneWheelShowOnLevel2Enable", value);
    }
    public static string getFortuneWheelShowOnLevel2Availability()
    {
        return PlayerPrefs.GetString("FortuneWheelShowOnLevel2Enable", "Yes");
    }

    public static void SetRewardProgress(float value)
    {
        PlayerPrefs.SetFloat("RewardProgressBar", value);
    }

    public static float GetRewardProgress()
    {
        return PlayerPrefs.GetFloat("RewardProgressBar", 0);
    }

    public static void SetGameOverRewardNumber(int rewardNumber)
    {
        PlayerPrefs.SetInt("GameOverRewardNumber" , rewardNumber);
    }
    public static int GetGameOverRewardNumber()
    {
        return PlayerPrefs.GetInt("GameOverRewardNumber", 1);
    }
    public static void SetGameOverRewardClaim(int rewardNumber)
    {
        PlayerPrefs.SetString("GameOverRewardClaim" + rewardNumber, "Claimed");
    }
    public static void SetGameOverRewardUnClaim(int rewardNumber)
    {
        PlayerPrefs.SetString("GameOverRewardClaim" + rewardNumber, "UnClaimed");
    }
    public static string GetGameOverRewardClaim(int rewardNumber)
    {
        return PlayerPrefs.GetString("GameOverRewardClaim" + rewardNumber, "UnClaimed");
    }
    public static bool GetGameOverRewardClaim2(int rewardNumber)
    {
       // if( PlayerPrefs.HasKey("GameOverRewardClaim" + rewardNumber+ "UnClaimed"))
        if( PlayerPrefs.GetString("GameOverRewardClaim" + rewardNumber,"UnClaimed")== "UnClaimed")
        {
            return true;
        }
        return false;
    }

    public static void SetMultplayerMode1LevelCountNumber(int count)    //using for logging analytics
    {
        PlayerPrefs.SetInt("MultplayerMode1LevelCountNumber", count);
    }
    public static int GetMultplayerMode1LevelCountNumber()
    {
        return PlayerPrefs.GetInt("MultplayerMode1LevelCountNumber", 0);
    }
    public static void SetMultplayerMode2LevelCountNumber(int count)    //using for logging analytics
    {
        PlayerPrefs.SetInt("MultplayerMode2LevelCountNumber", count);
    }
    public static int GetMultplayerMode2LevelCountNumber()
    {
        return PlayerPrefs.GetInt("MultplayerMode2LevelCountNumber", 0);
    }

    public static void SetRewardShotMode(int RewardShotMode)
    {
        PlayerPrefs.SetInt("RewardShotMode", RewardShotMode);

    }
    public static int GetRewardShotMode()
    {
        return PlayerPrefs.GetInt("RewardShotMode", 1);
    }

    public static void SetRewardKickCountNumber(int count)    //using for logging analytics
    {
        PlayerPrefs.SetInt("RewardKickCountNumber", count);
    }
    public static int GetRewardKickCountNumber()
    {
        return PlayerPrefs.GetInt("RewardKickCountNumber", 0);
    }

    public static void SetNewNotificationCustomizationMenu(int i) //notifcation show on bottom panel on customization
    {
        PlayerPrefs.SetInt("NotificationCustomizationMenu", i);
    }
    public static int GetNewNotificationCustomizationMenu()
    {
        return PlayerPrefs.GetInt("NotificationCustomizationMenu", 1);
    }




    public static void SetNewNotification_Shirt_Menu(int i) //notifcation show on bottom panel on Shirts Button
    {
        PlayerPrefs.SetInt("NotificationShirtMenu", i);
    }
    public static int GetNewNotification_Shirt_Menu()
    {
        return PlayerPrefs.GetInt("NotificationShirtMenu", 1);
    }
    public static void SetNewNotification_Shorts_Menu(int i) //notifcation show on bottom panel on Shorts Button
    {
        PlayerPrefs.SetInt("NotificationShortMenu", i);
    }
    public static int GetNewNotification_Shorts_Menu()
    {
        return PlayerPrefs.GetInt("NotificationShortMenu", 1);
    }
    public static void SetNewNotificationFootballMenu(int i) //notifcation show on bottom panel on Footballs Button
    {
        PlayerPrefs.SetInt("NotificationFootballMenu", i);
    }
    public static int GetNewNotificationFootballMenu()
    {
        return PlayerPrefs.GetInt("NotificationFootballMenu", 1);
    }
    public static void SetNewNotification_Celebration_Menu(int i) //notifcation show on bottom panel on Celebration Button
    {
        PlayerPrefs.SetInt("NotificationCelebrationMenu", i);  
    }
    public static int GetNewNotification_Celebration_Menu()
    {
        return PlayerPrefs.GetInt("NotificationCelebrationMenu", 1);
    }



    public static void SetNewNotification_MultiplayerButton_inGameplay(int i) //notifcation show in gameplay on MultiplayerButton
        {
        PlayerPrefs.SetInt("Notification_MultiplayerButton_inGameplay", i);
    }
    public static int GetNewNotification_MultiplayerButton_inGameplay()
    {
        return PlayerPrefs.GetInt("Notification_MultiplayerButton_inGameplay", 1);
    }
    public static void SetNewNotification_CustomizationButton_InGamePlay(int i)//notifcation show in gameplay on CustomizationButton
    {
        PlayerPrefs.SetInt("Notification_CustomizationButton_inGameplay", i);
    }
    public static int GetNewNotification_CustomizationButton_InGamePlay()
    {
        return PlayerPrefs.GetInt("Notification_CustomizationButton_inGameplay", 1);
    }

    public static void SetlanguageIndex(int i)
    {
        PlayerPrefs.SetInt("Selected_Language_Index", i);

    }
    public static int GetlanguageIndex()
    {
        return PlayerPrefs.GetInt("Selected_Language_Index", 1);
    }

    public static void SetTrophyUnlockedNumber(int TrophyNumber)
    {
        PlayerPrefs.SetInt("TrophyUnlockedNumber", TrophyNumber);
    }
    public static int GetTrophyUnlockedNumber()
    {
        return PlayerPrefs.GetInt("TrophyUnlockedNumber", -1);
    }
  /*  public static void SetSeasonCompleted(int seasonNumber)
    {
        PlayerPrefs.SetString("Season" + seasonNumber, "Completed");
    }
    public static int GetSeasonCompleted()
    {
        return PlayerPrefs.GetInt("SeasonCompleted", 0);
    }*/
    public static void SetSeasonCompleted(string status)
    {
        PlayerPrefs.SetString("SeasonCompleted", status);
    }
    public static string GetSeasonCompleted()
    {
        return PlayerPrefs.GetString("SeasonCompleted", "No");
    }
    public static int GetSelectedPlayer()
    {
        return PlayerPrefs.GetInt("SelectedPlayer",0);
    }
    public static void SetSelectedPlayer(int num)
    {
        PlayerPrefs.SetInt("SelectedPlayer", num);
    }
    public static int GetSelectedGK()
    {
        return PlayerPrefs.GetInt("SelectedGK",0);
    }
    public static void SetSelectedGK(int num)
    {
        PlayerPrefs.SetInt("SelectedGK", num);
    }

    public static int GetCollectedRewardKeys()
    {
        return PlayerPrefs.GetInt("CollectedRewardKeys", 0);
    }
    public static void SetCollectedRewardKeys(int num)
    {
        PlayerPrefs.SetInt("CollectedRewardKeys", num);
    }

    public static void SetSubscriptionStatus(string status)
    {
        PlayerPrefs.SetString("SubscriptionBought", status);
    }

    public static string GetSubscriptionStatus()
    {
        return PlayerPrefs.GetString("SubscriptionBought", "No");
    }
}

