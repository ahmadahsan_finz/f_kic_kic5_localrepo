﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading;
using System.Linq;

public class GamePlayManager : MonoBehaviour
{
    private static GamePlayManager sInstance = null;
    public GameObject LevelCompletePanel;
    public GameObject LevelFailPanel;
    public static int cornerScore = 90, sidePanel = 30, topPanel = 60, midGoal = 20, dartboardHit = 100, boxhit=20, Brickwall=100,PoleHit=100 ;    //ahmad changed
    public float scoreValue;
    public Text currentScore;
    public Text goalsText;
    public Text KickRemaining;
    public GameObject goalsPanal;
    public Text CurrentScoreNew;
    public float hitsRemaining = 0;
    public float score = 0;
    public bool timerbool;
    public int[] TargetsArray = new int[3];
    public int[] TargetsStarsArray = new int[3];
  //  public GameObject[] GroundlinesArray;
    public int StarsCollected=0;
    public int LevelCleared = 0;
    public static int previousGKDifficulty=0;
    public static bool FireBallActive = false;
    public GameObject[] DefendersArray;
    public GameObject[] DummyDefenders;
    public GameObject FireParticle;
    public GameObject FireParticle2;
    public GameObject FloatingText;
    public GameObject FloatingText1;
    public GameObject FloatingText3;

    public GameObject Player;
   // public GameObject Characters;// Gilani
    public GameObject GKPlayer;
    public GameObject GKPlayer_Parent;
    public GameObject PlayerSkinMaterial;
    public GameObject PlayerBodyMaterial;
    public GameObject FootballMaterial;
    public GameObject TutorialScreen;
    public GameObject AreaShoot;
    public Texture[] Pitches;
    public Texture[] PitchAds;
    public GameObject Stadium;
    public Renderer renderer;
    public bool LevelEndBool;
    public bool MissionEndBool;
    public bool RewardShotEndBool;
    public GameObject CelebrationParticles;
    public GameObject GoalPost;
    public Animator GoalPostAnimator;
    public string [] GoalTexts;
    public string[] GoalMissTexts;
    GameObject FloatTextObj;
    public GameObject wall;
    public bool BonusLevel;
    public bool LevelRestart;
    public GameObject GKSkinMaterial;
    public GameObject GKBodyMaterial;
    public Texture[] GKClothes;
    public Texture[] GKSkin;
    public Text[] GoalMissTextsGameplayArray;
    public Text[] LevelDetailShowInGameplay;

    public bool GoalScoredBool;

    public Coroutine DelayLevelCompleteCoroutine;
 //   public static int RateUsNumber = 0;

    public GameObject khabibHat;
    public GameObject HatInstance;

    [SerializeField] WaitForSeconds waitSec1 = new WaitForSeconds(0.3f);
    [SerializeField] WaitForSeconds waitSec2 = new WaitForSeconds(0.5f);
    [SerializeField] WaitForSeconds waitSec3 = new WaitForSeconds(0.8f);
    public float WaitGameWon =3.5f;
    public float WaitGameLost = 3.5f;
    [SerializeField] GameObject[] Environment;
    [SerializeField] Material[] SkyBoxes;
    [SerializeField] GameObject EnvironmentInstance;
    //public GameObject[] CrowdSets;
    public static int EnvironmentNumber;
    public int NumberOfMiss=0;

    //player working
    [Header("player working")]
    public GameObject FootballCore;
    public GameObject TargetBall;


    [Header("Chestbox working")]
    public LevelCompleteStatus LevelStatus;

    [Header("RewardKick working")]
    public bool RewardKickItemPicked = false;

    public enum LevelCompleteStatus
    {
        win,
        lose
    }

    /*mode 1= penalties																						//ahmad
    mode 2= simple freekicks
    mode 3= freekicks with dartboard only/bullseye
    mode 4= freekicks with wall
    mode 5= freekicks with wall  and dartboard/bullseye
    mode 6= freekicks with boxes
    mode 7= Goal Spot
    mode 8= Hit the pole 
    mode 9= freekicks with hoops/2x 3x circles
    mode 10= Tournament
    mode 11= freekicks with wall and hoops/2x 3x circles
    mode 12- Freekicks with dummy wall
    mode 13= freekicks with dummy wall and bullseye
    mode 14= freekicks with wood logs
    mode 15: freekicks with RoadblockA
    mode 16: freekicks with RoadblockB and moving defender
    mode 17: freekicks with Hoops
    mode 18: freekicks with Drums
    mode 19: freekicks with two Moving Defender
    mode 20: freekicks with pillars in goal and moving dummy no GK
    mode 21: Freekick directional pillars  and GK
    mode 22: Cones and wooden barrels
    mode 23: Freekick with One Static Dummy
    mode 24: Freekick with One Static Dummy and one moving dummy
    mode 25: Freekick with pillars in goal no GK
    mode 26: Freekick with directional pillars and no GK
    mode 27: Freekick with multiple Dartboards
    mode 28: Freekick with single Dartboard and moving dummy
    mode 29: Freekick with single Goal through Circle  No GK
    mode 30: Freekick with Single Goal through Circle with GK
    mode 31: Freekick with Single Goal through Circle with GK + Moving dummy
    mode 32: Freekick with two Goal through Circle No GK
    mode 33: Freekick with two Goal through Circle + GK 
    mode 34: Freekick with two Goal through Circle + GK + Moving Dummy 
    mode 35: Freekicks with cones variation 
    mode 36: Freekicks with cones variation and moving dummy
    mode 37: Freekicks with cones variation + GK
    mode 38: Freekicks with cones variation and moving dummy + GK
    mode 39: Freekicks with two Human Defenders + GK
    mode 40: Freekicks with Pillars Simple + No GK
    mode 41: Freekicks with DummyDef and Dartboard + No GK
    mode 42: Freekicks with Moving dartboard + GK (BOSS)
    mode 43: Freekicks with Coins + No GK (BONUS LEVEL) (Not Using)
    mode 44: Freekicks with Coins + No GK (BONUS LEVEL)
    mode 45: Freekicks with Trampoline + No GK + no def (BOSS)
    mode 46: Freekicks with Two Moving Rail Defender + GK (BOSS)
    mode 47: Freekicks with Trampoline + No GK + def (BOSS)
    mode 48: Freekicks with Block moving variation 1
    mode 49: Freekicks with Block moving variation 2


  */

    public static GamePlayManager instance
    {
        get
        {
            return sInstance;
        }
    }

  private void Awake()
    {

        if (sInstance == null)
        {
            sInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        Player = Instantiate(MenuManager.Instance.Players_CharacterArray[GameConstants.GetSelectedPlayer()]);

        GKPlayer = Instantiate(MenuManager.Instance.GK_CharacterArray[GameConstants.GetSelectedGK()]);
        GKPlayer.transform.SetParent(GKPlayer_Parent.transform);
        GKPlayer.transform.localPosition = new Vector3(0, 0, 0);
        GKPlayer.transform.localRotation = Quaternion.Euler(0, 0, 0);
        GKPlayer.transform.localScale = new Vector3(1, 1, 1);

       GKSkinMaterial = GKPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin[0];
       GKBodyMaterial = GKPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody[0];
    }
    
    private void Update()
    {
        if(timerbool)           //if level has timer 
        {
            if (GameConstants.timer >= 0)
            {
              //  Debug.Log(" timer running: timer is:  " + GameConstants.timer);
                GameConstants.timer = GameConstants.timer - Time.deltaTime;
                HudManager.instance.UpdateTimer();
            }
            else
            {
              //  Debug.Log(" timer stopped" );
              if(GameConstants.GetGameMode()=="Classic")
                {
                    LevelCompleteCheck();

                }
              else if(GameConstants.GetGameMode()=="Missions")
                {
                    MissionCompleteCheck();
                }
            }
        }

    }
    void Start()
    {
        Debug.Log("GamePlayManager Start");


        /*        GamePlayManager.instance.CrowdSets[0].SetActive(true);
                GamePlayManager.instance.CrowdSets[2].SetActive(false);
                GamePlayManager.instance.CrowdSets[1].SetActive(false);
                GamePlayManager.instance.CrowdSets[3].SetActive(false);
        */
    
        StartCoroutine(CrowdSound());
       // khabibHat.SetActive(false);
        SoundManagerNew.Instance.OnOffSound(MenuManager.SoundBool);
        SoundManagerNew.Instance.OnOffMusic(MenuManager.MusicBool);

        UITutorialManager.instance.DisableUpperBottomPanelBlackShader();                //disable upper bootom panel UI blackshader

     //   SetPitchTexturesRandom();  //setting random textures of pitch

        GoalPostAnimator = GoalPost.GetComponent<Animator>();

        // Debug.Log("GameplayManager Start");
        // GoalKeeperLevel.share.setLevel(4);

        /*if (GameConstants.GetMode()==1 )                          //to set specific ground lines for penalties
        {
            GroundlinesArray[0].SetActive(false);
            GroundlinesArray[1].SetActive(true);
        }*/

        if (GameConstants.GetMode() == 12 || GameConstants.GetMode() == 13)
        {
            Debug.Log("dummy defenders count " + DefendersArray.Count());
            for (int i = 0; i < DefendersArray.Count(); i++)
            {
                DummyDefenders[i].SetActive(true);
                DefendersArray[i].SetActive(false);
            }

        }

        //   TutorialScreen.SetActive(false);     //no tutorial Screen

        if (GameConstants.GetGameMode() == "Classic")
        {
     
            AdController.instance.ShowBannerAd();
            //      Debug.Log("Mode is Classic");

            /*  
             if (GameConstants.GetMode() != 44)     //if not bonus level
              {
            */
         /*   GamePlayManager.instance.GroundlinesArray[0].SetActive(true);
            GamePlayManager.instance.GroundlinesArray[1].SetActive(false);*/
            LevelDetailsSetter();

            TargetsArray = GameConstants.GetTargetScores();
            TargetsStarsArray = GameConstants.GetTargetStars();

            GoalDetermine.EventFinishShoot += EventShootFinish;
            HudManager.instance.UpdateTargetScores();
            
            if (GameConstants.GetLevelNumber() > 2)   //check to show multiplayer mode dialog
            {
              //  MenuManager.Instance.DisableAll();

              //    MenuManager.Instance.UIMultiplayerOfferDialog.SetActive(true);

                HudManager.instance.EnableDisableMultiplayerButton(true);
                HudManager.instance.EnableDisableCustomizeButton(true);
            }

        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
            AdController.instance.ShowBannerAd();

            //    Debug.Log("Mode is MISSIONS");
         /*   GamePlayManager.instance.GroundlinesArray[0].SetActive(true);
            GamePlayManager.instance.GroundlinesArray[1].SetActive(false);*/

            EnvironmentNumber = 0;
            SetEnvironment(EnvironmentNumber);

            TargetsArray = GameConstants.GetTargetScores();

            GoalDetermine.EventFinishShoot += EventShootFinishMission;

            MenuManager.Instance.MissionHudReseter();
        }
        else if (GameConstants.GetGameMode() == "RewardShot")
        {
            AdController.instance.ShowBannerAd();
            //    Debug.Log("Mode is MISSIONS");
            //   TargetsArray = GameConstants.GetTargetScores();

            EnvironmentNumber = 0;
            SetEnvironment(EnvironmentNumber);
            MenuManager.Instance.RewardShotHudReseter();
            GoalDetermine.EventFinishShoot += EventShootFinishRewardShot;

        //    MenuManager.Instance.MissionHudReseter();
        }
        else if (GameConstants.GetGameMode() == "RewardShot2")
        {
            AdController.instance.ShowBannerAd();
            //    Debug.Log("Mode is MISSIONS");
            //   TargetsArray = GameConstants.GetTargetScores();

            RewardKickItemPicked = false;

            EnvironmentNumber = 1;
            //     EnvironmentNumber = 0;
            SetEnvironment(EnvironmentNumber);
            MenuManager.Instance.RewardShot2HudReseter();

            GoalDetermine.EventFinishShoot += EventShootFinishRewardShot2;

            //    MenuManager.Instance.MissionHudReseter();
        }
        if (timerbool)
        {
            HudManager.instance.TimerPanelEnabler();
        }
        else
        {
            HudManager.instance.KicksPanelEnabler();
        }

        HudManager.instance.UpdateKickRemaining("Increment");


        SetSelectedPlayerTextures();                        //setting selected player skin and clothes textures

        //  GoalKeeper.share.reset();
        // if (BonusLevel)         //if its a bonus level
        // {
        //   SetEnvironment(EnvironmentNumber);
        // }

       /* if (AdConstants.isTestingLicenseAvailable())    //testing
        {
            HudManager.instance.WinLevelButton.SetActive(true);
            HudManager.instance.LoseLevelButton.SetActive(true);
            HudManager. TestingWinButtonPressedBool = false;
            HudManager.TestingLoseButtonPressedBool = false;
        }
        else
        {
            HudManager.instance.WinLevelButton.SetActive(false);
            HudManager.instance.LoseLevelButton.SetActive(false);

            HudManager.TestingWinButtonPressedBool = false;
            HudManager.TestingLoseButtonPressedBool = false;
        }*/

    }
    public IEnumerator CrowdSound()
    {
       // yield return new WaitForSeconds(0.8f);
        yield return waitSec3;
        if (SoundManagerNew.Instance != null)
        {
           //SoundManagerNew.Instance.playMusic("StreetBG");
            if (EnvironmentNumber == 0)
            {
                SoundManagerNew.Instance.playMusic("India");
            }
            else if (EnvironmentNumber == 1)
            {
                SoundManagerNew.Instance.playMusic("Egypt");
            }
            else if (EnvironmentNumber == 2)
            {
                SoundManagerNew.Instance.playMusic("Sydney");
            }
            else if (EnvironmentNumber == 3)
            {
                SoundManagerNew.Instance.playMusic("Paris");
            }
            else if (EnvironmentNumber == 4)
            {
                SoundManagerNew.Instance.playMusic("Statue");
            }
        }
    }

    public void SetPitchTexturesRandom()           //setting random textures of pitch
    {
        renderer = Stadium.GetComponent<Renderer>();

        int random = Random.Range(0, 4);
       /* if(GameConstants.GetLevelNumber()==1)
        {
            random = 4;
        }*/
        renderer.materials[0].SetTexture("_MainTex", PitchAds[random]);
        renderer.materials[1].SetTexture("_MainTex", Pitches[random]);
    }

    public void SetPitchTextures(int Num)           //setting random textures of pitch
    {
        renderer = Stadium.GetComponent<Renderer>();
        
        renderer.materials[0].SetTexture("_MainTex", PitchAds[Num]);
        renderer.materials[1].SetTexture("_MainTex", Pitches[Num]);
    }
    public void SetEnvironment(int EnvNum)
    {
        /* for(int i=0;i<Environment.Length;i++)
         {
             Environment[i].SetActive(false);

         }*/
        // Environment[EnvNum].SetActive(true);
    //    Debug.Log("setting environment" + EnvNum);
      //  Destroy(EnvironmentInstance);
        for(int i=0;i<Environment.Length;i++)
        {
            Environment[i].SetActive(false);
        }
        Environment[EnvNum].SetActive(true);

        if (EnvNum == 3) 
        {
            RenderSettings.skybox = SkyBoxes[1];
                       
        }
        else if (EnvNum == 0 || EnvNum == 1 || EnvNum == 2 || EnvNum == 4)// Aqsa
        {
            RenderSettings.skybox = SkyBoxes[0];

        }
        
        /* 
          EnvironmentInstance = Instantiate(Environment[EnvNum]);
          Debug.Log("setting environment" + EnvNum);
        */

    }

    public void SetSelectedPlayerTextures()              //setting selected player skin and clothes textures
    {
        Texture skin = CustomizationScript.instance.SkinTextures[GenericVariables.getcurrSkin()];
        Texture body = CustomizationScript.instance.BodyTextures[GenericVariables.getcurrBody()];
      //  Texture shorts = CustomizationScript.instance.BodyTextures[GenericVariables.getcurrShorts()];
        //  PlayerSkinMaterial.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        // Player.transform.GetChild(2).GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", skin);
        for (int a = 0; a < Player.GetComponent<PlayerAttributeController>().UIPlayerSkin.Length; a++)
        {
            Player.GetComponent<PlayerAttributeController>().UIPlayerSkin[a].GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        }

        for (int i = 0; i < Player.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            Player.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }

        Player.GetComponent<PlayerAttributeController>().PlayerHeadArray[GenericVariables.getcurrSkin()].SetActive(true);

        // PlayerBodyMaterial.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
        //Player.transform.GetChild(1).GetComponent<Renderer>().sharedMaterial.SetTexture("_MainTex", body);
        for (int i=0; i < Player.GetComponent<PlayerAttributeController>().UIPlayerBody.Length;i++)
        {

          Player.GetComponent<PlayerAttributeController>().UIPlayerBody[i].GetComponent<Renderer>().material.SetTexture("_MainTex", body);

        }
        //for (int b = 0; b < Player.GetComponent<PlayerAttributeController>().UIPlayerShorts.Length; b++)
        //{

        //  Player.GetComponent<PlayerAttributeController>().UIPlayerShorts[b].GetComponent<Renderer>().material.SetTexture("_MainTex", shorts);
        //}

        Texture football = CustomizationScript.instance.FootballTextures[GenericVariables.getcurrFootball()];
      //  Player.GetComponent<PlayerAttributeController>().UIFootball.GetComponent<Renderer>().material.SetTexture("_MainTex", football);
        FootballMaterial.GetComponent<Renderer>().material.SetTexture("_MainTex", football);


        // MenuManager.Instance.ChangeBody(body);
    /*    CustomizationScript.instance.ChangeBody(GenericVariables.getcurrBody());

        CustomizationScript.instance.ChangeSkin(GenericVariables.getcurrSkin());

        CustomizationScript.instance.ChangeFootball(GenericVariables.getcurrFootball());

        CustomizationScript.instance.ChangeCelebration(GenericVariables.getcurrCelebration());*/
    }

    public void ChangeGkOutLook(int ind)
    {

        StartCoroutine(DelayChangeGkOutLook(ind));
    }

    IEnumerator DelayChangeGkOutLook(int ind)
    {
     //   yield return new WaitForSeconds(0.5f);
        yield return waitSec2;
    
        Texture body = GKClothes[ind - 1];
        int rand = Random.Range(0, 7);
        Texture skin = CustomizationScript.instance.SkinTextures[rand];


        for(int i=0;i < GKPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length;i++)  //changing head
        {

            GKPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);

        }
        GKPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[rand].SetActive(true);



        GKBodyMaterial.GetComponent<Renderer>().materials[0].SetTexture("_MainTex", body);
        GKSkinMaterial.GetComponent<Renderer>().materials[0].SetTexture("_MainTex", skin);
     //   GKSkinMaterial.GetComponent<Renderer>().materials[1].SetTexture("_MainTex", skin);

    }
    public void LevelDetailsSetter()                 // for all the information related to levels will set here 
    {
            //     Debug.Log("LevelDetailsSetter");
       if(GameConstants.GetMode()==4 || GameConstants.GetMode() == 5 || GameConstants.GetMode() == 12 || GameConstants.GetMode() == 13)
        {
            wall.SetActive(true);
        }
        else
        {
            //Destroy(wall);
            wall.SetActive(false);

        }

        switch (GameConstants.GetLevelNumber())
        {
            case 0:                                                             //penalties                            
                Debug.Log("level Bonus");
                GameConstants.hitsRemaining = 5f;
                //GameConstants.hitsRemaining = 5f;
                
                SetEnvironment(EnvironmentNumber);
                GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                GamePlayManager.instance.UnlockNextLevel();
                break;

            case 1:                                                             //penalties                            
                Debug.Log("level 1");
                GameConstants.hitsRemaining = 16f;
                //             TutorialScreen.SetActive(true);             //tutorial screen enabled

                //    TutorialManager.instance.TurnOnGameplayTutorial();

                /*  GroundlinesArray[0].SetActive(true);
                 GroundlinesArray[1].SetActive(false);*/
               if(!LevelRestart)
                {

                TimelineCutscene.instance.PlayCutscene();
                }
                else
                {
                    LevelRestart = false;

                }
                //   GoalKeeperLevel.share.setLevel(1);


                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
               // EnvironmentNumber = 5;
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                SoundManagerNew.Instance.playMusic("India");
                //     SetPitchTextures(2);

                break;
            case 2:                                                             //freekicks

                Debug.Log("level 2");
                GameConstants.hitsRemaining = 12f;
                //  GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
               // EnvironmentNumber = 5;
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;

            case 3:                                                              //boxes
                Debug.Log("level 3");
                GameConstants.hitsRemaining = 20f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnableGKHat();

                EnvironmentNumber = 0;
                //  EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
         //       SetPitchTextures(2);

                break;
            case 4:                                                             //freekicks
                Debug.Log("level 4");
                GameConstants.hitsRemaining = 20f;
             //   GoalKeeperLevel.share.setLevel(3);
               StartCoroutine( GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                //EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
           //     SetPitchTextures(4);

                break;
            case 5:                                                              //BullsEye
                Debug.Log("level 5");
                GameConstants.hitsRemaining = 16f;
           //     GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                //EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
             //   EnableGKHat();

                break;
            case 6:                                                              //Penalties
                Debug.Log("level 6");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer //ahmad
                GameConstants.timer = 60f;
             //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
               // EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 7:                                                                //Boxes
                Debug.Log("level 7");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(1));
              //  GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 8:                                                               //Freekicks
                Debug.Log("level 8");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(1));
                // GoalKeeperLevel.share.setLevel(4);
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
         //       EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 9:                                                               //Bullseye
                Debug.Log("level 9");
                GameConstants.hitsRemaining = 14f;
            //    GameConstants.hitsRemaining = 10f;
         //       GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
              //  EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 10:                                                               //Penalties
                Debug.Log("level 10");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
          //      GoalKeeperLevel.share.setLevel(5);
                StartCoroutine(GkDifficultySetter(1));
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                ChangeGkOutLook(1);

                break;
            case 11:                                                               //GoalSpot
                Debug.Log("level 11");
                GameConstants.hitsRemaining = 8f;
                GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 12:                                                               //tournament
                Debug.Log("level 12 Tournament");
             //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 13:                                                               //Boxes
                Debug.Log("level 13");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                ChangeGkOutLook(1);
                StartCoroutine(GkDifficultySetter(1));
                //GoalKeeperLevel.share.setLevel(1);

                break;
            case 14:                                                               //Bullseye with Defenders
                Debug.Log("level 14");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                //GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 15:                                                               //Penalties
                Debug.Log("level 15");
                GameConstants.hitsRemaining = 10f;

                StartCoroutine(GkDifficultySetter(1));
                //GoalKeeperLevel.share.setLevel(5);
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 16:                                                                //Bullseye
                Debug.Log("level 16");
                GameConstants.hitsRemaining = 18f;
                //GameConstants.hitsRemaining = 15f;
               // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(1));
               // StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 17:                                                               //Hit the pole
                Debug.Log("level 17");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 18:                                                                //Circles
                Debug.Log("level 18");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 19:                                                               //Boxes
                Debug.Log("level 19");
                GameConstants.hitsRemaining = 12f;

                StartCoroutine(GkDifficultySetter(1));
                //               GoalKeeperLevel.share.setLevel(2);
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 20:                                                              //GoalSpot
                Debug.Log("level 20");
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 21:                                                            //freekicks with defenders
                Debug.Log("level 21");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
               // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 22:
                Debug.Log("level 22");                                          //freekicks with circles
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
              //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 23:
                Debug.Log("level 23");                                           //hit the pole
                GameConstants.hitsRemaining = 10f;

                //GoalKeeperLevel.share.setLevel(3); 
                StartCoroutine(GkDifficultySetter(2));
              //  EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 24:
                Debug.Log("level 24 Tournament");                                 //tournament
            //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
             //   EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;

            case 25:                                                            //brick wall
                Debug.Log("level 25");
                GameConstants.hitsRemaining = 20f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                //   StartCoroutine(GkDifficultySetter(3));

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 26:
                Debug.Log("level 26");                                            //bulleyes
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
              //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 27:
                Debug.Log("level 27");                                         //freekicks with circles
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                EnableGKHat();
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 28:                                                             //freekicks with boxes
                Debug.Log("level 28");
                GameConstants.hitsRemaining = 12f;
        
           //     GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 29:                                                         //freekick
                Debug.Log("level 29");
                GameConstants.hitsRemaining = 15f;
        
          //      GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 30:                                                        //freekick
                Debug.Log("level 30");
                GameConstants.hitsRemaining = 15f;
                EnableGKHat();
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 31:                                                          //boxes
                Debug.Log("level 31");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 32:                                                          //bulleyes with def
                Debug.Log("level 32");
                GameConstants.hitsRemaining = 15f;
         
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 33:
                Debug.Log("level 33");                                         //penalties
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;
        
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 34:
                Debug.Log("level 34");                                          //freekicks
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
            //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                //          EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 35:                                                            //cirles defenders
                Debug.Log("level 35");
                GameConstants.hitsRemaining = 0f;
          
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;
           
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat(); 
                ChangeGkOutLook(3);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 36:
                Debug.Log("level 36 Tournament");                                 //tournament
                GameConstants.hitsRemaining = 15f;
          
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 37:
                Debug.Log("level 37");                                          //freekicks dummy defenders bulleye
                GameConstants.hitsRemaining = 15f;
        
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 38:
                Debug.Log("level 38");                                       //freekicks wooden 
                GameConstants.hitsRemaining = 8f;
               
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 39:
                Debug.Log("level 39");                                      //road blocks A freekicks
                GameConstants.hitsRemaining = 15f;
             
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 40:                                                        //road blocks B freekicks
                Debug.Log("level 40");
                GameConstants.hitsRemaining = 13f;
             
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 41:                                                       //freekicks with hoops
                Debug.Log("level 41");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 42:                                                
                Debug.Log("level 42");                                      //freekicks with drums
                GameConstants.hitsRemaining = 8f; 
                StartCoroutine(GkDifficultySetter(2));
                //EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 43:
                Debug.Log("level 43");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break; 
            case 44:
                Debug.Log("level 44");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 45:
                Debug.Log("level 45");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 46:
                Debug.Log("level 46");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(3);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
                   
            case 47:
                Debug.Log("level 47");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(3);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 48:
               
                Debug.Log("level 48 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);
               
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 49:
                Debug.Log("level 49");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 50:
                Debug.Log("level 50");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 51:
                Debug.Log("level 51");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 52:
                Debug.Log("level 52");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 6f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 53:
                Debug.Log("level 53");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 54:
                Debug.Log("level 54");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 55:
                Debug.Log("level 55");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(3);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 56:
                Debug.Log("level 56");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 57:
                Debug.Log("level 57");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 58:
                Debug.Log("level 58");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 30;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 59:
                Debug.Log("level 59");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(3);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 60:
                Debug.Log("level 60 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);
                
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 61:                                                              //boxes
                Debug.Log("level 61");
                GameConstants.hitsRemaining = 15f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 62:                                                             //freekicks
                Debug.Log("level 62");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 63:                                                              //BullsEye
                Debug.Log("level 63");
                GameConstants.hitsRemaining = 8f;
                //     GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 64:                                                              //Penalties
                Debug.Log("level 64");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer //ahmad
                GameConstants.timer = 60f;
                //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(1));
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 65:                                                                //Boxes
                Debug.Log("level 65");
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(1));
                //  GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber); break;
            case 66:                                                               //Freekicks
                Debug.Log("level 66");
                GameConstants.hitsRemaining = 16f;
                StartCoroutine(GkDifficultySetter(2));
                // GoalKeeperLevel.share.setLevel(4);
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber); break;
            case 67:                                                               //Bullseye
                Debug.Log("level 67");
                GameConstants.hitsRemaining = 10f;
                //       GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 68:                                                               //Penalties
                Debug.Log("level 68");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //      GoalKeeperLevel.share.setLevel(5);
                StartCoroutine(GkDifficultySetter(1));

                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber); break;
            case 69:                                                               //GoalSpot
                Debug.Log("level 69");
                GameConstants.hitsRemaining = 8f;
                GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 70:                                                               //tournament
                Debug.Log("level 70 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(2);
                EnableGKHat();
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 71:                                                               //Boxes
                Debug.Log("level 71");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                StartCoroutine(GkDifficultySetter(1));
                //GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 72:                                                               //Bullseye with Defenders
                Debug.Log("level 72");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                //GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 73:                                                               //Penalties
                Debug.Log("level 73");
                GameConstants.hitsRemaining = 10f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(2));
                //GoalKeeperLevel.share.setLevel(5);
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 74:                                                                //Bullseye
                Debug.Log("level 74");
                GameConstants.hitsRemaining = 15f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 75:                                                               //Hit the pole
                Debug.Log("level 75");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 76:                                                                //Circles
                Debug.Log("level 76");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 77:                                                               //Boxes
                Debug.Log("level 77");
                GameConstants.hitsRemaining = 12f;

                StartCoroutine(GkDifficultySetter(1));
                //               GoalKeeperLevel.share.setLevel(2);
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 78:                                                              //GoalSpot
                Debug.Log("level 78");
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 79:                                                            //freekicks with defenders
                Debug.Log("level 79");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber); break;
            case 80:
                Debug.Log("level 80");                                          //freekicks with circles
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 81:
                Debug.Log("level 81");                                           //hit the pole
                GameConstants.hitsRemaining = 10f;

                //GoalKeeperLevel.share.setLevel(3); 
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 82:
                Debug.Log("level 82 Tournament");                                 //tournament
                                                                                  //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(3);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;

            case 83:                                                            //brick wall
                Debug.Log("level 83");
                GameConstants.hitsRemaining = 20f;
                StartCoroutine(GkDifficultySetter(1));

                //   StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 84:
                Debug.Log("level 84");                                            //bulleyes
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 85:
                Debug.Log("level 85");                                         //freekicks with circles
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 86:                                                             //freekicks with boxes
                Debug.Log("level 86");
                GameConstants.hitsRemaining = 12f;

                //     GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 87:                                                         //freekick
                Debug.Log("level 87");
                GameConstants.hitsRemaining = 15f;

                //      GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 88:                                                        //freekick
                Debug.Log("level 88");
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 89:                                                          //boxes
                Debug.Log("level 89");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 90:                                                          //bulleyes with def
                Debug.Log("level 90");
                GameConstants.hitsRemaining = 15f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 91:
                Debug.Log("level 91");                                         //penalties
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(1));

                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 92:
                Debug.Log("level 92");                                          //freekicks
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 93:                                                            //cirles defenders
                Debug.Log("level 93");
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(3);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 94:
                Debug.Log("level 94 Tournament");                                 //tournament

                EnableGKHat();

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(3);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 95:
                Debug.Log("level 95");                                          //freekicks dummy defenders bulleye
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 96:
                Debug.Log("level 96");                                       //freekicks wooden 
                GameConstants.hitsRemaining = 8f;

                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 97:
                Debug.Log("level 97");                                      //road blocks A freekicks
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 98:                                                        //road blocks B freekicks
                Debug.Log("level 98");
                GameConstants.hitsRemaining = 13f;

                StartCoroutine(GkDifficultySetter(1));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 99:                                                       //freekicks with hoops
                Debug.Log("level 99");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber); break;
            case 100:
                Debug.Log("level 100");                                      //freekicks with drums
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber); break;

            case 101:                                                              //boxes
                Debug.Log("level 101");
                GameConstants.hitsRemaining = 14f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnableGKHat();
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 102:                                                             //freekicks
                Debug.Log("level 102");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 103:                                                              //BullsEye
                Debug.Log("level 103");
                GameConstants.hitsRemaining = 8f;
                //     GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                //   EnableGKHat();

                break;
            case 104:                                                              //Penalties
                Debug.Log("level 104");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer //ahmad
                GameConstants.timer = 60f;
                //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 105:                                                                //Boxes
                Debug.Log("level 105");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(2));
                //  GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 106:                                                               //Freekicks
                Debug.Log("level 106");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(2));
                // GoalKeeperLevel.share.setLevel(4);
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 107:                                                               //Bullseye
                Debug.Log("level 107");
                GameConstants.hitsRemaining = 10f;
                //       GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 108:                                                               //Penalties
                Debug.Log("level 108");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //      GoalKeeperLevel.share.setLevel(5);
                StartCoroutine(GkDifficultySetter(2));
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                ChangeGkOutLook(1);

                break;
            case 109:                                                               //GoalSpot
                Debug.Log("level 109");
                GameConstants.hitsRemaining = 8f;
                GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 110:                                                               //tournament
                Debug.Log("level 110 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 111:                                                               //Boxes
                Debug.Log("level 111");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                ChangeGkOutLook(1);
                StartCoroutine(GkDifficultySetter(2));
                //GoalKeeperLevel.share.setLevel(1);

                break;
            case 112:                                                               //Bullseye with Defenders
                Debug.Log("level 112");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                //GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 113:                                                               //Penalties
                Debug.Log("level 113");
                GameConstants.hitsRemaining = 10f;

                StartCoroutine(GkDifficultySetter(2));
                //GoalKeeperLevel.share.setLevel(5);
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 114:                                                                //Bullseye
                Debug.Log("level 114");
                GameConstants.hitsRemaining = 15f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 115:                                                               //Hit the pole
                Debug.Log("level 115");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 116:                                                                //Circles
                Debug.Log("level 116");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 117:                                                               //Boxes
                Debug.Log("level 117");
                GameConstants.hitsRemaining = 12f;

                StartCoroutine(GkDifficultySetter(2));
                //               GoalKeeperLevel.share.setLevel(2);
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 118:                                                              //GoalSpot
                Debug.Log("level 118");
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 119:                                                            //freekicks with defenders
                Debug.Log("level 119");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 120:
                Debug.Log("level 120");                                          //freekicks with circles
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 121:
                Debug.Log("level 121");                                           //hit the pole
                GameConstants.hitsRemaining = 10f;

                //GoalKeeperLevel.share.setLevel(3); 
                StartCoroutine(GkDifficultySetter(3));
                //  EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 122:
                Debug.Log("level 122 Tournament");                                 //tournament
                                                                                  //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                //   EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;

            case 123:                                                            //brick wall
                Debug.Log("level 123");
                GameConstants.hitsRemaining = 20f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                //   StartCoroutine(GkDifficultySetter(3));

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 124:
                Debug.Log("level 124");                                            //bulleyes
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 125:
                Debug.Log("level 125");                                         //freekicks with circles
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 126:                                                             //freekicks with boxes
                Debug.Log("level 126");
                GameConstants.hitsRemaining = 12f;

                //     GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 127:                                                         //freekick
                Debug.Log("level 127");
                GameConstants.hitsRemaining = 15f;

                //      GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 128:                                                        //freekick
                Debug.Log("level 128");
                GameConstants.hitsRemaining = 15f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 129:                                                          //boxes
                Debug.Log("level 129");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 130:                                                          //bulleyes with def
                Debug.Log("level 130");
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 131:
                Debug.Log("level 131");                                         //penalties
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 132:
                Debug.Log("level 132");                                          //freekicks
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                //          EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 133:                                                            //cirles defenders
                Debug.Log("level 133");
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 134:
                Debug.Log("level 134 Tournament");                                 //tournament
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 135:
                Debug.Log("level 135");                                          //freekicks dummy defenders bulleye
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 136:
                Debug.Log("level 136");                                       //freekicks wooden 
                GameConstants.hitsRemaining = 8f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 137:
                Debug.Log("level 137");                                      //road blocks A freekicks
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 138:                                                        //road blocks B freekicks
                Debug.Log("level 138");
                GameConstants.hitsRemaining = 13f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 139:                                                       //freekicks with hoops
                Debug.Log("level 139");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 140:
                Debug.Log("level 140");                                      //freekicks with drums
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(3));
                //EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 141:
                Debug.Log("level 141");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 142:
                Debug.Log("level 142");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 143:
                Debug.Log("level 143");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 5f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 144:
                Debug.Log("level 144");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;

            case 145:
                Debug.Log("level 145");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 146:

                Debug.Log("level 146 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 147:
                Debug.Log("level 147");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 148:
                Debug.Log("level 148");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 149:
                Debug.Log("level 149");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 150:
                Debug.Log("level 150");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 6f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 151:
                Debug.Log("level 151");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 152:
                Debug.Log("level 152");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 153:
                Debug.Log("level 153");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(3);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 154:
                Debug.Log("level 154");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 155:
                Debug.Log("level 155");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 156:
                Debug.Log("level 156");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 30;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 157:
                Debug.Log("level 157");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(3);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 158:
                Debug.Log("level 60 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 159:                                                              //boxes
                Debug.Log("level 159");
                GameConstants.hitsRemaining = 15f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 160:                                                             //freekicks
                Debug.Log("level 160");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 161:                                                              //BullsEye
                Debug.Log("level 161");
                GameConstants.hitsRemaining = 8f;
                //     GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(3));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 162:                                                              //Penalties
                Debug.Log("level 162");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer //ahmad
                GameConstants.timer = 60f;
                //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 163:                                                                //Boxes
                Debug.Log("level 163");
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(2));
                //  GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber); break;
            case 164:                                                               //Freekicks
                Debug.Log("level 164");
                GameConstants.hitsRemaining = 16f;
                StartCoroutine(GkDifficultySetter(3));
                // GoalKeeperLevel.share.setLevel(4);
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber); break;
            case 165:                                                               //Bullseye
                Debug.Log("level 165");
                GameConstants.hitsRemaining = 10f;
                //       GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 166:                                                               //Penalties
                Debug.Log("level 166");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //      GoalKeeperLevel.share.setLevel(5);
                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber); break;
            case 167:                                                               //GoalSpot
                Debug.Log("level 167");
                GameConstants.hitsRemaining = 8f;
                GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 168:                                                               //tournament
                Debug.Log("level 168 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnableGKHat();
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 169:                                                               //Boxes
                Debug.Log("level 169");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                StartCoroutine(GkDifficultySetter(2));
                //GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 170:                                                               //Bullseye with Defenders
                Debug.Log("level 170");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                //GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 171:                                                               //Penalties
                Debug.Log("level 171");
                GameConstants.hitsRemaining = 10f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(3));
                //GoalKeeperLevel.share.setLevel(5);
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 172:                                                                //Bullseye
                Debug.Log("level 172");
                GameConstants.hitsRemaining = 15f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(3));

                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 173:                                                               //Hit the pole
                Debug.Log("level 173");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(3));

                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 174:                                                                //Circles
                Debug.Log("level 174");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 175:                                                               //Boxes
                Debug.Log("level 175");
                GameConstants.hitsRemaining = 12f;

                StartCoroutine(GkDifficultySetter(2));
                //               GoalKeeperLevel.share.setLevel(2);
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 176:                                                              //GoalSpot
                Debug.Log("level 176");
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 177:                                                            //freekicks with defenders
                Debug.Log("level 177");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber); break;
            case 178:
                Debug.Log("level 178");                                          //freekicks with circles
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 179:
                Debug.Log("level 179");                                           //hit the pole
                GameConstants.hitsRemaining = 10f;

                //GoalKeeperLevel.share.setLevel(3); 
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 180:
                Debug.Log("level 180 Tournament");                                 //tournament
                                                                                  //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(3);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;

            case 181:                                                            //brick wall
                Debug.Log("level 181");
                GameConstants.hitsRemaining = 20f;
                StartCoroutine(GkDifficultySetter(2));

                //   StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 182:
                Debug.Log("level 182");                                            //bulleyes
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 183:
                Debug.Log("level 183");                                         //freekicks with circles
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 184:                                                             //freekicks with boxes
                Debug.Log("level 184");
                GameConstants.hitsRemaining = 12f;

                //     GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 185:                                                         //freekick
                Debug.Log("level 185");
                GameConstants.hitsRemaining = 15f;

                //      GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));

                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 186:                                                        //freekick
                Debug.Log("level 186");
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(3));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 187:                                                          //boxes
                Debug.Log("level 187");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 188:                                                          //bulleyes with def
                Debug.Log("level 188");
                GameConstants.hitsRemaining = 15f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 189:
                Debug.Log("level 189");                                         //penalties
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(2));

                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 190:
                Debug.Log("level 190");                                          //freekicks
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 191:                                                            //cirles defenders
                Debug.Log("level 191");
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(3);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 192:
                Debug.Log("level 192 Tournament");                                 //tournament

                EnableGKHat();

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(3);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 193:
                Debug.Log("level 193");                                          //freekicks dummy defenders bulleye
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 194:
                Debug.Log("level 194");                                       //freekicks wooden 
                GameConstants.hitsRemaining = 8f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 195:
                Debug.Log("level 195");                                      //road blocks A freekicks
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 196:                                                        //road blocks B freekicks
                Debug.Log("level 196");
                GameConstants.hitsRemaining = 13f;

                StartCoroutine(GkDifficultySetter(2));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 197:                                                       //freekicks with hoops
                Debug.Log("level 197");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber); break;
            case 198:
                Debug.Log("level 198");                                      //freekicks with drums
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(3));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber); break;
            case 199:                                                              //boxes
                Debug.Log("level 199");
                GameConstants.hitsRemaining = 14f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnableGKHat();
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 200:                                                             //freekicks
                Debug.Log("level 200");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;

            case 201:                                                              //boxes
                Debug.Log("level 101");
                GameConstants.hitsRemaining = 14f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnableGKHat();
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 202:                                                             //freekicks
                Debug.Log("level 102");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 203:                                                              //BullsEye
                Debug.Log("level 103");
                GameConstants.hitsRemaining = 8f;
                //     GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                //   EnableGKHat();

                break;
            case 204:                                                              //Penalties
                Debug.Log("level 104");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer //ahmad
                GameConstants.timer = 60f;
                //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 205:                                                                //Boxes
                Debug.Log("level 105");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(4));
                //  GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);

                break;
            case 206:                                                               //Freekicks
                Debug.Log("level 106");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(4));
                // GoalKeeperLevel.share.setLevel(4);
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 207:                                                               //Bullseye
                Debug.Log("level 107");
                GameConstants.hitsRemaining = 10f;
                //       GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 208:                                                               //Penalties
                Debug.Log("level 108");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //      GoalKeeperLevel.share.setLevel(5);
                StartCoroutine(GkDifficultySetter(4));
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                ChangeGkOutLook(1);

                break;
            case 209:                                                               //GoalSpot
                Debug.Log("level 109");
                GameConstants.hitsRemaining = 8f;
                GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 210:                                                               //tournament
                Debug.Log("level 110 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);

                break;
            case 211:                                                               //Boxes
                Debug.Log("level 111");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                ChangeGkOutLook(1);
                StartCoroutine(GkDifficultySetter(4));
                //GoalKeeperLevel.share.setLevel(1);

                break;
            case 212:                                                               //Bullseye with Defenders
                Debug.Log("level 112");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                //GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 213:                                                               //Penalties
                Debug.Log("level 113");
                GameConstants.hitsRemaining = 10f;

                StartCoroutine(GkDifficultySetter(4));
                //GoalKeeperLevel.share.setLevel(5);
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 214:                                                                //Bullseye
                Debug.Log("level 114");
                GameConstants.hitsRemaining = 15f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);

                break;
            case 215:                                                               //Hit the pole
                Debug.Log("level 115");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 216:                                                                //Circles
                Debug.Log("level 116");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 217:                                                               //Boxes
                Debug.Log("level 117");
                GameConstants.hitsRemaining = 12f;

                StartCoroutine(GkDifficultySetter(4));
                //               GoalKeeperLevel.share.setLevel(2);
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 218:                                                              //GoalSpot
                Debug.Log("level 118");
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 219:                                                            //freekicks with defenders
                Debug.Log("level 119");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 220:
                Debug.Log("level 120");                                          //freekicks with circles
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 221:
                Debug.Log("level 121");                                           //hit the pole
                GameConstants.hitsRemaining = 10f;

                //GoalKeeperLevel.share.setLevel(3); 
                StartCoroutine(GkDifficultySetter(4));
                //  EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 222:
                Debug.Log("level 122 Tournament");                                 //tournament
                                                                                   //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                //   EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;

            case 223:                                                            //brick wall
                Debug.Log("level 123");
                GameConstants.hitsRemaining = 20f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                //   StartCoroutine(GkDifficultySetter(3));

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 224:
                Debug.Log("level 124");                                            //bulleyes
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 225:
                Debug.Log("level 125");                                         //freekicks with circles
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 226:                                                             //freekicks with boxes
                Debug.Log("level 126");
                GameConstants.hitsRemaining = 12f;

                //     GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 227:                                                         //freekick
                Debug.Log("level 127");
                GameConstants.hitsRemaining = 15f;

                //      GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 228:                                                        //freekick
                Debug.Log("level 128");
                GameConstants.hitsRemaining = 15f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 229:                                                          //boxes
                Debug.Log("level 129");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 230:                                                          //bulleyes with def
                Debug.Log("level 130");
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 231:
                Debug.Log("level 131");                                         //penalties
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 232:
                Debug.Log("level 132");                                          //freekicks
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                //          EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 233:                                                            //cirles defenders
                Debug.Log("level 133");
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 234:
                Debug.Log("level 134 Tournament");                                 //tournament
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(3);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 235:
                Debug.Log("level 135");                                          //freekicks dummy defenders bulleye
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 236:
                Debug.Log("level 136");                                       //freekicks wooden 
                GameConstants.hitsRemaining = 8f;

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 237:
                Debug.Log("level 137");                                      //road blocks A freekicks
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 238:                                                        //road blocks B freekicks
                Debug.Log("level 138");
                GameConstants.hitsRemaining = 13f;

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 239:                                                       //freekicks with hoops
                Debug.Log("level 139");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 240:
                Debug.Log("level 140");                                      //freekicks with drums
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(5));
                //EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 241:
                Debug.Log("level 141");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 242:
                Debug.Log("level 142");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 243:
                Debug.Log("level 143");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 5f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;
            case 244:
                Debug.Log("level 144");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);

                break;

            case 245:
                Debug.Log("level 145");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(3);
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 246:

                Debug.Log("level 146 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 247:
                Debug.Log("level 147");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 248:
                Debug.Log("level 148");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 249:
                Debug.Log("level 149");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 250:
                Debug.Log("level 150");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 6f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 251:
                Debug.Log("level 151");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 252:
                Debug.Log("level 152");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 253:
                Debug.Log("level 153");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(6));
                ChangeGkOutLook(3);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 254:
                Debug.Log("level 154");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 255:
                Debug.Log("level 155");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 15f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 256:
                Debug.Log("level 156");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 30;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 257:
                Debug.Log("level 157");                                      //freekicks with moving dummy 
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(3);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 258:
                Debug.Log("level 60 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(6));
                EnableGKHat();
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 259:                                                              //boxes
                Debug.Log("level 159");
                GameConstants.hitsRemaining = 15f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 260:                                                             //freekicks
                Debug.Log("level 160");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 261:                                                              //BullsEye
                Debug.Log("level 161");
                GameConstants.hitsRemaining = 8f;
                //     GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(5));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 262:                                                              //Penalties
                Debug.Log("level 162");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer //ahmad
                GameConstants.timer = 60f;
                //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(4));
                EnableGKHat();
                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 263:                                                                //Boxes
                Debug.Log("level 163");
                GameConstants.hitsRemaining = 10f;
                StartCoroutine(GkDifficultySetter(4));
                //  GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber); break;
            case 264:                                                               //Freekicks
                Debug.Log("level 164");
                GameConstants.hitsRemaining = 16f;
                StartCoroutine(GkDifficultySetter(5));
                // GoalKeeperLevel.share.setLevel(4);
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber); break;
            case 265:                                                               //Bullseye
                Debug.Log("level 165");
                GameConstants.hitsRemaining = 10f;
                //       GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 266:                                                               //Penalties
                Debug.Log("level 166");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //      GoalKeeperLevel.share.setLevel(5);
                StartCoroutine(GkDifficultySetter(4));

                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber); break;
            case 267:                                                               //GoalSpot
                Debug.Log("level 167");
                GameConstants.hitsRemaining = 8f;
                GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 268:                                                               //tournament
                Debug.Log("level 168 Tournament");
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(6));
                ChangeGkOutLook(2);
                EnableGKHat();
                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 269:                                                               //Boxes
                Debug.Log("level 169");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                StartCoroutine(GkDifficultySetter(4));
                //GoalKeeperLevel.share.setLevel(1);
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 270:                                                               //Bullseye with Defenders
                Debug.Log("level 170");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                //GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 271:                                                               //Penalties
                Debug.Log("level 171");
                GameConstants.hitsRemaining = 10f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(5));
                //GoalKeeperLevel.share.setLevel(5);
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 272:                                                                //Bullseye
                Debug.Log("level 172");
                GameConstants.hitsRemaining = 15f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(5));

                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 273:                                                               //Hit the pole
                Debug.Log("level 173");
                GameConstants.hitsRemaining = 12f;
                StartCoroutine(GkDifficultySetter(5));

                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 274:                                                                //Circles
                Debug.Log("level 174");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 275:                                                               //Boxes
                Debug.Log("level 175");
                GameConstants.hitsRemaining = 12f;

                StartCoroutine(GkDifficultySetter(5));
                //               GoalKeeperLevel.share.setLevel(2);
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 276:                                                              //GoalSpot
                Debug.Log("level 176");
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 277:                                                            //freekicks with defenders
                Debug.Log("level 177");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                // GoalKeeperLevel.share.setLevel(2);
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber); break;
            case 278:
                Debug.Log("level 178");                                          //freekicks with circles
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(1);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 279:
                Debug.Log("level 179");                                           //hit the pole
                GameConstants.hitsRemaining = 10f;

                //GoalKeeperLevel.share.setLevel(3); 
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;
            case 280:
                Debug.Log("level 180 Tournament");                                 //tournament
                                                                                   //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(3);

                EnvironmentNumber = 0;
                SetEnvironment(EnvironmentNumber);
                break;

            case 281:                                                            //brick wall
                Debug.Log("level 181");
                GameConstants.hitsRemaining = 20f;
                StartCoroutine(GkDifficultySetter(4));

                //   StartCoroutine(GkDifficultySetter(3));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 282:
                Debug.Log("level 182");                                            //bulleyes
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
                //  GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 283:
                Debug.Log("level 183");                                         //freekicks with circles
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 284:                                                             //freekicks with boxes
                Debug.Log("level 184");
                GameConstants.hitsRemaining = 12f;

                //     GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(1);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 285:                                                         //freekick
                Debug.Log("level 185");
                GameConstants.hitsRemaining = 15f;

                //      GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));

                ChangeGkOutLook(2);
                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                break;
            case 286:                                                        //freekick
                Debug.Log("level 186");
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));

                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 287:                                                          //boxes
                Debug.Log("level 187");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 288:                                                          //bulleyes with def
                Debug.Log("level 188");
                GameConstants.hitsRemaining = 15f;
                EnableGKHat();

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 289:
                Debug.Log("level 189");                                         //penalties
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(4));

                ChangeGkOutLook(1);
                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 290:
                Debug.Log("level 190");                                          //freekicks
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                //    GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 2;
                SetEnvironment(EnvironmentNumber);
                break;
            case 291:                                                            //cirles defenders
                Debug.Log("level 191");
                GameConstants.hitsRemaining = 0f;

                timerbool = true;                   //level has timer 
                GameConstants.timer = 50f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(3);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 292:
                Debug.Log("level 192 Tournament");                                 //tournament

                EnableGKHat();

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(3);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 293:
                Debug.Log("level 193");                                          //freekicks dummy defenders bulleye
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 294:
                Debug.Log("level 194");                                       //freekicks wooden 
                GameConstants.hitsRemaining = 8f;

                StartCoroutine(GkDifficultySetter(4));
                ChangeGkOutLook(1);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 295:
                Debug.Log("level 195");                                      //road blocks A freekicks
                GameConstants.hitsRemaining = 15f;

                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);

                EnvironmentNumber = 3;
                SetEnvironment(EnvironmentNumber);
                break;
            case 296:                                                        //road blocks B freekicks
                Debug.Log("level 196");
                GameConstants.hitsRemaining = 13f;

                StartCoroutine(GkDifficultySetter(6));
                ChangeGkOutLook(1);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;
            case 297:                                                       //freekicks with hoops
                Debug.Log("level 197");
                GameConstants.hitsRemaining = 0f;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                StartCoroutine(GkDifficultySetter(5));
                ChangeGkOutLook(2);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber); break;
            case 298:
                Debug.Log("level 198");                                      //freekicks with drums
                GameConstants.hitsRemaining = 8f;
                StartCoroutine(GkDifficultySetter(5));
                EnableGKHat();
                ChangeGkOutLook(2);

                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber); break;
            case 299:                                                              //boxes
                Debug.Log("level 199");
                GameConstants.hitsRemaining = 14f;
                //   GoalKeeperLevel.share.setLevel(1);
                StartCoroutine(GkDifficultySetter(6));
                ChangeGkOutLook(1);
                EnableGKHat();
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);

                break;
            case 300:                                                             //freekicks
                Debug.Log("level 200");
                GameConstants.hitsRemaining = 12f;
                //   GoalKeeperLevel.share.setLevel(3);
                StartCoroutine(GkDifficultySetter(6));
                ChangeGkOutLook(1);
                EnvironmentNumber = 4;
                SetEnvironment(EnvironmentNumber);
                break;

        }
    }
    
    public void EnableGKHat()
    {
        if(HatInstance!=null)
        {
            HatInstance = null;
        }
        HatInstance = Instantiate(khabibHat);
        HatInstance.transform.parent = GKPlayer.GetComponent<PlayerAttributeController>().PlayerHead.transform;
        HatInstance.transform.localPosition = new Vector3(0, 0.0046f, -0.00126f);
        HatInstance.transform.localRotation = Quaternion.Euler(-90, 0, 0);
        HatInstance.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
    }
    public void GoalNetAnimationCaller(Area areaHit)
    {
        if (areaHit == Area.CornerLeft || areaHit == Area.Left)
        {

            GoalPostAnimator.Play("LeftNetAnim", 0, 0.25f);
        }
        else if ( areaHit == Area.CornerRight || areaHit == Area.Right)
        {
            GoalPostAnimator.Play("RightNetAnim", 0, 0.25f);
            
        }
        else if (areaHit == Area.Top)
        {
            GoalPostAnimator.Play("MiddleNetAnim", 0, 0.25f);

        }
        else //mid
        {
            GoalPostAnimator.Play("MiddleNetAnim", 0, 0.25f);

        }
    }
    void EventShootFinish(bool goal, Area area)
    {
        AreaShoot.SetActive(false);
      //  Debug.Log("EventShootFinish");
        if (goal && GameConstants.GetMode()!=10)  //mode 10 is tournament 
        {

            //     Debug.Log("goal");
            GoalNetAnimationCaller(area);
            ManageGoal(area);
        }
        else if(GameConstants.GetMode() != 10)//if (isGoalLevel)  //mode 10 is tournament 
        {
            // Debug.Log("isGoalLevel");
        //    Debug.Log("Goalmissed");
            ManageMiss();
        }
    }
    public void FloatingScoreTextShow()
    {
        Vector3 offset = new Vector3(0, 0.5f, 0);
        var Multiplier = Instantiate(FloatingText, Shoot.share._ball.transform.position, Quaternion.identity);           //floating score text popup 
        Multiplier.GetComponent<TextMesh>().text = scoreValue.ToString();
        Multiplier.transform.position += offset;
    }

    /*  mode 1= penalties																						//ahmad
          mode 2= simple freekicks
          mode 3= freekicks with dartboard only/bullseye
          mode 4= freekicks with wall
          mode 5= freekicks with wall  and dartboard/bullseye
          mode 6= freekicks with boxes
          mode 7= Goal Spot
          mode 8= Hit the pole 
          mode 9= freekicks with hoops/2x 3x circles
          mode 10= Tournament
          mode 11= freekicks with wall and hoops/2x 3x circles
          mode 12- Freekicks with dummy wall
          mode 13= freekicks with dummy wall and bullseye
          mode 14= freekicks with wood logs
          mode 15: freekicks with RoadblockA
          mode 16: freekicks with RoadblockB and moving defender
          mode 17: freekicks with Hoops
          mode 18: freekicks with Drums
          mode 19: freekicks with two Moving Defender
          mode 20: freekicks with pillars in goal and moving dummy no GK
          mode 21: Freekick directional pillars  and GK
          mode 22: Cones and wooden barrels
          mode 23: Freekick with One Static Dummy
          mode 24: Freekick with One Static Dummy and one moving dummy
          mode 25: Freekick with pillars in goal no GK
          mode 26: Freekick with directional pillars and no GK
          mode 27: Freekick with multiple Dartboards
          mode 28: Freekick with single Dartboard and moving dummy
          mode 29: Freekick with single Goal through Circle  No GK
          mode 30: Freekick with Single Goal through Circle with GK
          mode 31: Freekick with Single Goal through Circle with GK + Moving dummy
          mode 32: Freekick with two Goal through Circle No GK
          mode 33: Freekick with two Goal through Circle + GK 
          mode 34: Freekick with two Goal through Circle + GK + Moving Dummy 
          mode 35: Freekicks with cones variation 
          mode 36: Freekicks with cones variation and moving dummy
          mode 37: Freekicks with cones variation + GK
          mode 38: Freekicks with cones variation and moving dummy + GK
          mode 39: Freekicks with two Human Defenders + GK
          mode 40: Freekicks with Pillars Simple + No GK
          mode 41: Freekicks with DummyDef and Dartboard + No GK
          mode 42: Freekicks with Moving dartboard + GK (BOSS)
          mode 43: Freekicks with Coins + No GK (BONUS LEVEL) (Not Using)
          mode 44: Freekicks with Coins + No GK (BONUS LEVEL)
          mode 45: Freekicks with Trampoline + No GK (BOSS)
          mode 46: Freekicks with Two Moving Rail Defender + GK (BOSS)

      */
    void ManageGoal(Area areaHit)
    {
        CelebrationParticles.SetActive(true);
        /*  CamShake camShake = Camera.main.GetComponent<CamShake>();
          if (camShake != null) camShake.Shake(0.2f);             */
 //       Debug.Log("MANAGE GOAL  0");

        if (GameConstants.GetLevelNumber() == 1)                //FOR First level tutorials
        {
            if(TutorialManager.instance.tutorialNumber<5)           
            {

        //        Debug.Log("tutorial decider tut num + " + TutorialManager.instance.tutorialNumber);
                    TutorialManager.instance.tutorialNumber++;
         //       Debug.Log("tutorial decider tut num + " + TutorialManager.instance.tutorialNumber);

            }
            TutorialManager.instance.TutorialDecider();
  //          Debug.Log("MANAGE GOAL  1");

        }
        else if (GameConstants.GetLevelNumber() == 2 && TutorialManager.instance.FireTutorialBool && GameConstants.getStartGamePlayTutorial() == "Yes")      //assigning extra ball tutorial for level 2
        {
            TutorialManager.instance.tutorialNumber = 6;
            TutorialManager.instance.TutorialDecider();
        }
        else if (GameConstants.GetLevelNumber() == 2 && !TutorialManager.instance.FireTutorialBool && GameConstants.getStartGamePlayTutorial() == "Yes")      //assigning extra ball tutorial for level 2
        {
            if(TutorialManager.instance.ExtraBallObject!= null)
            {
     
                TutorialManager.instance.tutorialNumber=7;
                TutorialManager.instance.TutorialDecider();
            }
            else
            {
    
                TutorialManager.instance.tutorialNumber++;
                TutorialManager.instance.TutorialDecider();
            }
        }
 //       Debug.Log("MANAGE GOAL  2");

        if (!timerbool)              //if timer is on 
        {
            GameConstants.hitsRemaining--;
              if (GameConstants.GetMode() != 8 && GameConstants.GetMode() != 44 && GameConstants.GetLevelNumber() != 0 && GameConstants.GetLevelNumber()!=1 && GameConstants.GetLevelNumber() != 2)
              {
                if (GameConstants.hitsRemaining == 2 || GameConstants.hitsRemaining == 5 || GameConstants.hitsRemaining == 7)
                {
                   // Debug.Log("EnableExtraBall inside ManageGoal");

                    ExtraBallScoreMultiScript.instance.EnableExtraBall();

                }

            }
   //         Debug.Log("MANAGE GOAL  3");

        }
        //   Debug.Log("remaining hit are............... " + GameConstants.hitsRemaining);
        if (GameConstants.GetMode() == 5 || GameConstants.GetMode() == 3 || GameConstants.GetMode() == 13 || GameConstants.GetMode() == 27 || GameConstants.GetMode() == 28 || GameConstants.GetMode() == 41 || GameConstants.GetMode() == 42) //mode 3 and 5 is dartboard mode //ahmad
        {
         //   Debug.Log("Shoot.DartBoardHitBool is " + Shoot.DartBoardHitBool);
            if (Shoot.DartBoardHitBool)
            {
                GoalScoreFloatingTextShow();
                //     Debug.Log(" if(Shoot.DartBoardHitBool) ");
                scoreValue = dartboardHit;
                FloatingScoreTextShow();
                GameConstants.score = GameConstants.score + scoreValue;
          //      Debug.Log("score is " + GameConstants.score);
            }
            else
            {
                GoalMissFloatingTextShow();
            }
        }
        else if (GameConstants.GetMode() == 7)
        {
            GoalScoreFloatingTextShow();

            scoreValue = Brickwall;
            FloatingScoreTextShow();

            GameConstants.score = GameConstants.score + scoreValue;
      //      Debug.Log("score is " + GameConstants.score);
        }
        else if (GameConstants.GetMode() == 8)
        {
            if (GoalDetermine.share._poleArea != Area.None)            
            {
                GoalScoreFloatingTextShow();

                //          Debug.Log("AREA HIT IS POLE ");
                scoreValue = PoleHit;
                GoalDetermine.share._poleArea = Area.None;
            //    FloatingScoreTextShowPoleLevel(scoreValue);

                //scoreValue = Brickwall;
                GameConstants.score = GameConstants.score + scoreValue;
           //     Debug.Log("score is " + GameConstants.score);
            }
            else
            {
                GoalMissFloatingTextShow();
            }
        }
        else
        {
   //         Debug.Log("MANAGE GOAL  4");

            Vector3 offset = new Vector3(0, 0.5f, 0);
            if (areaHit == Area.CornerLeft || areaHit == Area.CornerRight)
            {
                scoreValue = cornerScore;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17)
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;
                 //   Debug.Log("SCore issssss after CircleMultiplier =" + GameConstants.score);
                    Shoot.CircleMultiplier = 1;
                }

                else if( GameConstants.GetMode() == 29 || GameConstants.GetMode() == 30 || GameConstants.GetMode() == 31 || GameConstants.GetMode() == 32 || GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34)              //score goal through circle
                {
                    if(!Shoot.GoalCircle)
                    {
                        scoreValue = 0;
                        GoalMissFloatingTextShow();

                    }
                    else    //if true
                    {
                        Shoot.GoalCircle = false;
                        Shoot.GoalCircleNum = 0;
                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                
                else if (GameConstants.GetMode() == 45)  //trampoline
                {
                    if(!Shoot.TrampolineHitBool)
                    {
                        scoreValue = 0;
                        GoalMissFloatingTextShow();

                    }
                    else    //if true
                    {
                        Shoot.TrampolineHitBool = false;
                        
                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else
                {
     //               Debug.Log("MANAGE GOAL  5");

                    GoalScoreFloatingTextShow();
                    FloatingScoreTextShow();
                }

              
                GameConstants.score = GameConstants.score + scoreValue;
         //       Debug.Log("score is " + GameConstants.score);
             
            }
            else if (areaHit == Area.Left || areaHit == Area.Right)
            {
                scoreValue = sidePanel;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 )
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;
                 //   Debug.Log("SCore issssss after CircleMultiplier =" + GameConstants.score);
                    Shoot.CircleMultiplier = 1;
                }
                else if (GameConstants.GetMode() == 29 || GameConstants.GetMode() == 30 || GameConstants.GetMode() == 31 || GameConstants.GetMode() == 32 || GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34)              //score goal through circle
                {
                    if (!Shoot.GoalCircle)
                    {
                        scoreValue = 0;
                            GoalMissFloatingTextShow();
                    }
                    else    //if true
                    {
                        Shoot.GoalCircle = false;
                        Shoot.GoalCircleNum = 0;
                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else  if (GameConstants.GetMode() == 45)  //trampoline
                {
                    if (!Shoot.TrampolineHitBool)
                    {
                        scoreValue = 0;
                        GoalMissFloatingTextShow();

                    }
                    else    //if true
                    {
                        Shoot.TrampolineHitBool = false;

                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else
                {
  //                  Debug.Log("MANAGE GOAL  6");

                    GoalScoreFloatingTextShow();
                    FloatingScoreTextShow();
                }

                GameConstants.score = GameConstants.score + scoreValue;
         //       Debug.Log("score is " + GameConstants.score);
 
            }
            else if (areaHit == Area.Top)
            {
                scoreValue = topPanel;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 )
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;
                  //  Debug.Log("SCore issssss after CircleMultiplier =" + GameConstants.score);
                    Shoot.CircleMultiplier = 1;
                }

                else if (GameConstants.GetMode() == 29 || GameConstants.GetMode() == 30 || GameConstants.GetMode() == 31 || GameConstants.GetMode() == 32 || GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34)              //score goal through circle
                {
                    if (!Shoot.GoalCircle)
                    {
                        scoreValue = 0;
                            GoalMissFloatingTextShow();
                    }
                    else    //if true
                    {
                        Shoot.GoalCircle = false;
                        Shoot.GoalCircleNum = 0;

                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else if (GameConstants.GetMode() == 45)  //trampoline
                {
                    if (!Shoot.TrampolineHitBool)
                    {
                        scoreValue = 0;
                        GoalMissFloatingTextShow();

                    }
                    else    //if true
                    {
                        Shoot.TrampolineHitBool = false;

                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else
                {
   //                 Debug.Log("MANAGE GOAL  7");
                    GoalScoreFloatingTextShow();
                    FloatingScoreTextShow();
                }

                GameConstants.score = GameConstants.score + scoreValue;
               // Debug.Log("score is " + GameConstants.score);
              
            }
            else
            {
                scoreValue = midGoal;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 )
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;
                //    Debug.Log("SCore issssss after CircleMultiplier =" + GameConstants.score);
                    Shoot.CircleMultiplier = 1;
                }
                else if(GameConstants.GetMode() == 29 || GameConstants.GetMode() == 30 || GameConstants.GetMode() == 31 || GameConstants.GetMode() == 32 || GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34)              //score goal through circle
                {
                    if (!Shoot.GoalCircle)
                    {
                        scoreValue = 0;
                            GoalMissFloatingTextShow();
                    }
                    else    //if true
                    {
                        Shoot.GoalCircle = false;
                        Shoot.GoalCircleNum = 0;

                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else if (GameConstants.GetMode() == 45)          //trampoline
                {
                    if (!Shoot.TrampolineHitBool)
                    {
                        scoreValue = 0;
                        GoalMissFloatingTextShow();

                    }
                    else    //if true
                    {
                        Shoot.TrampolineHitBool = false;

                        GoalScoreFloatingTextShow();
                        FloatingScoreTextShow();

                    }
                }
                else
                {
 //                   Debug.Log("MANAGE GOAL  8");
                    GoalScoreFloatingTextShow();
                    FloatingScoreTextShow();
                }


                GameConstants.score = GameConstants.score + scoreValue;

            }
        }
            HudManager.instance.updateScore();
            HudManager.instance.updateStars();
//        Debug.Log("MANAGE GOAL  9");

        HudManager.instance.UpdateKickRemaining("Decrement");
       /* Debug.Log("LEVEL COMPLETE BOOL : " + LevelEndBool);*/
//        Debug.Log("MANAGE GOAL  10");
      
            LevelCompleteCheck();                          //check if level succeed or failed
//        Debug.Log("MANAGE GOAL  11");

    }
    public void LevelCompleteCheck()
    {
        //  Debug.Log("LEVEL COMPLETE BOOL : " + LevelEndBool);
 //       Debug.Log("LevelCompleteCheck 0");

     //   int[] TargetScoresInt = GameConstants.GetTargetScores();

        //      Debug.Log("LevelCompleteCheck 1");

        if (GameConstants.score >= TargetsArray[2] && !LevelEndBool)
        {
            //  Debug.Log("Scoreeeeeeeee issssss " + GameConstants.score);
         //   LevelEndCelebrationManager.instance.GameWon();
            LevelComplete(WaitGameWon);
 //           Debug.Log("LevelCompleteCheck 2");

        }
        else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.score >= TargetsArray[0] && !LevelEndBool)            //no more kicks and score more than target 1 //level cleared
        {
            //   Debug.Log("level failed 1 timerbool false ");
          //  LevelEndCelebrationManager.instance.GameWon();
            LevelComplete(WaitGameWon);
        }
        else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.score < TargetsArray[0] && !LevelEndBool)         //no more kicks and score less than target 1 //level failed
        {
            //  Debug.Log("level failed 1 timerbool false ");
            //  LevelEndCelebrationManager.instance.GameLost();
            LevelFailed(WaitGameLost);
        }
        else if(timerbool && GameConstants.timer<=0 && GameConstants.score >= TargetsArray[0] && !LevelEndBool)            // timer is 0 and score is more than target 1 //level success
        {
            //  Debug.Log("level failed 2 timerbool true  ");
        //    LevelEndCelebrationManager.instance.GameWon();
            LevelComplete(WaitGameWon);
            timerbool = false;
        }
        else if (timerbool && GameConstants.timer <= 0 && GameConstants.score < TargetsArray[0] && !LevelEndBool)            //timer is 0 and score is less than target 1 //level failed
        {
            //  Debug.Log("level failed 2 timerbool true  else if last check");
          //  LevelEndCelebrationManager.instance.GameLost();
            LevelFailed(WaitGameLost);
       //     timerbool = false;
        }
    //    Debug.Log("LevelCompleteCheck 3");

    }
    void ManageMiss()
    {
        NumberOfMiss++;
     //   Debug.Log("Number of restart is = " + GameConstants.NumberOfRestarts + "Number of miss is = " + NumberOfMiss);

        if (GameConstants.NumberOfRestarts == 1 && NumberOfMiss==3)
        {
        //    Debug.Log("Number of restart is = " + GameConstants.NumberOfRestarts + "Number of miss is = " +NumberOfMiss);
            GameConstants.NumberOfRestarts = 0;
            NumberOfMiss = 0;

            if (GameConstants.getStartGamePlayTutorial() != "Yes")  //if tutorial has completed
            {
                HudManager.instance.EnableDisableSkipLevelButton(true);
            }
        }

        if (GameConstants.GetLevelNumber() == 1)
        {
            TutorialManager.instance.TutorialDecider();
        }
        else if (GameConstants.GetLevelNumber() == 2 && TutorialManager.instance.FireTutorialBool && GameConstants.getStartGamePlayTutorial() == "Yes")      //assigning extra ball tutorial for level 2
        {
     
            TutorialManager.instance.tutorialNumber = 6;
            TutorialManager.instance.TutorialDecider();
        }
        else if (GameConstants.GetLevelNumber() == 2 && !TutorialManager.instance.FireTutorialBool && GameConstants.getStartGamePlayTutorial() == "Yes")      //assigning extra ball tutorial for level 2
        {
            if (TutorialManager.instance.ExtraBallObject != null)
            {
          
                TutorialManager.instance.tutorialNumber = 7;
                TutorialManager.instance.TutorialDecider();
            }
            else
            {
           
                TutorialManager.instance.tutorialNumber++;
                TutorialManager.instance.TutorialDecider();
            }
        }
        else if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17)         //if miss in 2x 3x 4x circle levels
        {
            Shoot.CircleMultiplier = 1;
        }

        else if (GameConstants.GetMode() == 29 || GameConstants.GetMode() == 30 || GameConstants.GetMode() == 31 || GameConstants.GetMode() == 32 || GameConstants.GetMode() == 33 || GameConstants.GetMode() == 34)              //score goal through circle
        {
            Shoot.GoalCircle = false;
            Shoot.GoalCircleNum = 0;
        }
        else if (GameConstants.GetMode() == 45)
        {

            Shoot.TrampolineHitBool = false;
        }

        if (GameConstants.GetMode() == 8)
        {
            if (GoalDetermine.share._poleArea != Area.None)
            {
                CelebrationParticles.SetActive(true);
                GoalScoreFloatingTextShow();

                //     Debug.Log("AREA HIT IS POLE ");
                scoreValue = PoleHit;
                GoalDetermine.share._poleArea = Area.None;
                GameConstants.score = GameConstants.score + scoreValue;
                //    Debug.Log("score is " + GameConstants.score);
            }
        }
        else if(GameConstants.GetMode() != 18 && GameConstants.GetMode() != 6)  //SHOW miss text unless it is boxes or drums levels
        {
            GoalMissFloatingTextShow();
        }


        if (!timerbool)
        {

            GameConstants.hitsRemaining--;
           
            if (GameConstants.GetMode() != 8 && GameConstants.GetMode() != 44 && GameConstants.GetLevelNumber() != 0 && GameConstants.GetLevelNumber() != 1 && GameConstants.GetLevelNumber() != 2)
                {
                    
                if (GameConstants.hitsRemaining == 2 || GameConstants.hitsRemaining == 5 || GameConstants.hitsRemaining == 7)
                {
                //    Debug.Log("EnableExtraBall inside ManageGoal");
                    ExtraBallScoreMultiScript.instance.EnableExtraBall();

                }

            }
            HudManager.instance.UpdateKickRemaining("Decrement");
        }
        HudManager.instance.updateScore();
        HudManager.instance.updateStars();

        LevelCompleteCheck();

    }
 
    public void StopDelayLevelWonCoroutine()
    {
       if(DelayLevelCompleteCoroutine!=null)
            StopCoroutine(DelayLevelCompleteCoroutine);
        
    }
    public void LevelComplete(float timer)
    {
        //  Debug.Log("LEVEL COMPLETE");
        LevelStatus = LevelCompleteStatus.win;
          LevelEndBool = true;
        if(GameConstants.TrophiesLevelList.Contains(GameConstants.GetLevelNumber()) && GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed()) //if level is trophy and played first time
        {
            LevelEndCelebrationManager.instance.TrophyWon();
            DelayLevelCompleteCoroutine = StartCoroutine(DelayLevelComplete(13f));

        }
        else
        {
            LevelEndCelebrationManager.instance.GameWon();
            DelayLevelCompleteCoroutine = StartCoroutine(DelayLevelComplete(timer));
            StartCoroutine(ShowRateUs(3f));
        }
       if((GameConstants.GetLevelNumber()==2 || GameConstants.GetLevelNumber() > 2) && GameConstants.getStartGamePlayTutorial()=="Yes")  //turns off gameplay tutorial
        {
            TutorialManager.instance.TurnOffLevelTwoTutorial();

        }
    }
    public IEnumerator DelayLevelComplete(float timer)
    {
        
        yield return new WaitForSeconds(timer);
        
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("LevelWin");

        }
        MenuManager. GameOverBoolVideoAd = true;

         CoinsCollectedSummaryMenuCalling();

    /*    BonusLevel = false;
        for (int i = 0; i < GameConstants.BonusLevelList.Length; i++)           //Bonus Level Check
        {
            if (GameConstants.GetLevelNumber() == GameConstants.BonusLevelList[i])
            {
                BonusLevel = true;
            }
        }
        if(!BonusLevel)             //if there is no bonus level on this level than unlock next level
        {
             UnlockNextLevel();
        }

        if (timerbool)
        {
            timerbool = false;
        }*/
        MenuManager.Instance.AnalyticsCallingEndWin();

    }
    public void UnlockNextLevel()
    {
        GameConstants.setLevelCleared(GameConstants.GetLevelNumber());
        if (GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed() && GameConstants.GetLevelNumber() < GameConstants.getTotalLevels())       //to check if you are playing the latest unlocked level
        {
            GenericVariables.incrementLevel();          //unlocking next level
      //      Debug.Log("LevelPassed inside LEVELCOMPLETE()");
        }
    }

    public void LevelFailed(float timer)
    {
        LevelStatus = LevelCompleteStatus.lose;

        LevelEndBool = true;
        LevelEndCelebrationManager.instance.GameLost();
        DelayLevelCompleteCoroutine = StartCoroutine(DelayLevelFailed(timer));
    }
    public IEnumerator DelayLevelFailed(float timer)
    {

        yield return new WaitForSeconds(timer);
        if(SoundManagerNew.Instance!=null)
        {
            SoundManagerNew.Instance.playSound("LevelFail");

        }
        MenuManager. GameOverBoolVideoAd = true;

        if (GameConstants.GetCollectedRewardKeys() == 3)             //show chest box
        {
            MenuManager.Instance.ChestBoxMenu.SetActive(true);
        }
        else
        {
            MenuManager.Instance.ActiveLevelFailPanel();

        }
        MenuManager.Instance.AnalyticsCallingEndLose();

    }

    public void mainMenuClicked()
    {
        GameConstants.score = 0f;
        SceneManager.LoadScene("Menus");
    }

    public void CoinsStarsCollectedOnTournamentWon()
    {
        StarsCollected = 3;
      //  TargetsStarsArray = GameConstants.GetTargetStars();
        GameConstants.CoinsEarned = TargetsStarsArray[0];
        // GenericVariables.AddCoins(1500);

        GenericVariables.setRating(GameConstants.GetLevelNumber(), StarsCollected);

         DelayLevelCompleteCoroutine= StartCoroutine(MenuManager.Instance.ActiveLevelSuccessPanel());
    }
    public void CoinsCollectedSummaryMenuCalling()        //Coins and stars collected on level end
    {
       // TargetsArray = GameConstants.GetTargetScores();
    //    TargetsStarsArray = GameConstants.GetTargetStars();
        if (GameConstants.score >= (float)TargetsArray[0] && GameConstants.score < (float)TargetsArray[1])
        {
            StarsCollected=1;
            //Stars Target 1
            //      GenericVariables.AddCoins(TargetsStarsArray[0]);
            GameConstants.CoinsEarned = TargetsStarsArray[0] + GameConstants.BonusCoinsCount;
            GameConstants.BonusCoinsCount = 0;

            /*StartCoroutine(HudManager.instance.ActiveLevelSuccessPanel(GameConstants.CoinsEarned));
            //  HudManager.instance.ActiveLevelSuccessPanel(TargetsStarsArray[0]);
            GenericVariables.setRating(GameConstants.GetLevelNumber(), StarsCollected);           //setting rating or number of stars collected
          */  //     MenuManager.Instance.CoinsToBeAddedOnClaimHit(TargetsStarsArray[0]);

          
        }
        if (GameConstants.score >= (float)TargetsArray[1] && GameConstants.score < (float)TargetsArray[2])
        {
            //      GenericVariables.AddCoins(TargetsStarsArray[1]);
         

            //Stars Target 2
            StarsCollected = 2;
            GameConstants.CoinsEarned = TargetsStarsArray[1] + GameConstants.BonusCoinsCount;
            GameConstants.BonusCoinsCount = 0;

            //  HudManager.instance.ActiveLevelSuccessPanel(TargetsStarsArray[1]);

           

         /*   StartCoroutine(HudManager.instance.ActiveLevelSuccessPanel(GameConstants.CoinsEarned));
            GenericVariables.setRating(GameConstants.GetLevelNumber(), StarsCollected);           //setting rating or number of stars collected
          */
            //    MenuManager.Instance.CoinsToBeAddedOnClaimHit(TargetsStarsArray[1]);

          
        }
        if (GameConstants.score >= (float)TargetsArray[2])
        {
            //       GenericVariables.AddCoins(TargetsStarsArray[2]);
            //Stars Target 3
      
            StarsCollected = 3;
            GameConstants.CoinsEarned = TargetsStarsArray[2] + GameConstants.BonusCoinsCount; 
            GameConstants.BonusCoinsCount=0;
            //    HudManager.instance.ActiveLevelSuccessPanel(TargetsStarsArray[2]);
           /* StartCoroutine(HudManager.instance.ActiveLevelSuccessPanel(GameConstants.CoinsEarned));
            GenericVariables.setRating(GameConstants.GetLevelNumber(), StarsCollected);             //setting rating or number of stars collected
*/
            //  MenuManager.Instance.CoinsToBeAddedOnClaimHit(TargetsStarsArray[2]);
        
        }
        if(HudManager.TestingWinButtonPressedBool)  //for testing button
        {
            HudManager.TestingWinButtonPressedBool = false;
            StarsCollected = 3;
            GameConstants.CoinsEarned = TargetsStarsArray[2] + GameConstants.BonusCoinsCount;
            GameConstants.BonusCoinsCount = 0;
            
            /*StartCoroutine(HudManager.instance.ActiveLevelSuccessPanel(GameConstants.CoinsEarned));
            GenericVariables.setRating(GameConstants.GetLevelNumber(), StarsCollected);             //setting rating or number of stars collected
*/
            //    HudManager.instance.ActiveLevelSuccessPanel(TargetsStarsArray[2]);

        }
        if (GameConstants.GetCollectedRewardKeys() == 3)             //show chest box
        {
            MenuManager.Instance.ChestBoxMenu.SetActive(true);
        }
        else
        {

            StartCoroutine(MenuManager.Instance.ActiveLevelSuccessPanel());
        }
            GenericVariables.setRating(GameConstants.GetLevelNumber(), StarsCollected);             //setting rating or number of stars collected

        HeaderMenuManager.instance.UpdateStarsOnGameover();
    }

    public void GoalScoreFloatingTextShow()
    {

        if (SoundManagerNew.Instance!=null)
        {
            SoundManagerNew.Instance.playSound("GoalScoreHuman");
        }
   /*    
    *    Vector3 offset1 = new Vector3(0, 2.8f, 0);
        //FloatTextObj.transform.position = offset1;
        var GoalText = Instantiate(FloatingText1, Shoot.share._ball.transform.position, Quaternion.identity);           //floating score text popup                                                                                                             //Multiplier.GetComponent<TextMesh>().fontSize = 150;
        GoalText.GetComponent<TextMesh>().color = Color.Lerp(Color.white,Color.green,0.6f);
        //int rand = Random.Range(0, GoalTexts.Length);
        int rand = Random.Range(0,3);
        GoalText.GetComponent<TextMesh>().text = GoalTexts[rand];
        GoalText.transform.position = offset1;*/
    }
    public void GoalMissFloatingTextShow()
    {
        Vector3 offset = new Vector3(0, 2.8f, 0);
        var GOALTEXT = Instantiate(FloatingText1, Shoot.share._ball.transform.position, Quaternion.identity);           //floating score text popup 
                                                                                                                        //Multiplier.GetComponent<TextMesh>().fontSize = 150;
        GOALTEXT.GetComponent<TextMesh>().color = Color.white;
    //    int rand = Random.Range(0, GoalMissTexts.Length);
        int rand = Random.Range(0, 2);
       // GOALTEXT.GetComponent<TextMesh>().text = GoalMissTexts[rand];
        GOALTEXT.GetComponent<TextMesh>().text = GoalMissTextsGameplayArray[rand].text;
        GOALTEXT.transform.position = offset;
    }

    public void LevelDetailTextShowInGameplay()  //example :HIT THE POLE
    {
        Vector3 offset = new Vector3(0, 2.8f, 0);
        var DetailText = Instantiate(FloatingText3, Shoot.share._ball.transform.position, Quaternion.identity);           //floating score text popup 
                                                                                                                         //Multiplier.GetComponent<TextMesh>().fontSize = 150;
        DetailText.GetComponent<TextMesh>().color = Color.white;

        DetailText.GetComponent<TextMesh>().text = LevelDetailShowInGameplay[0].text;
        DetailText.transform.position = offset;
    }
    public void onFireballClick()               //enabling fireball
    {

        if(TutorialManager.instance.FireTutorialBool && GameConstants.GetLevelNumber()==2)      //to disable tutorial screen on firepress in level 2
        {
            TutorialManager.instance.TurnOffGameplayTutorial();
            TutorialManager.instance.AreaShoot.SetActive(true);
            TutorialManager.instance.FireTutorialBool = false;

        }

        if (!FireBallActive && !Shoot.share.BallShot)
        {
           if( GenericVariables.Removefire(1))
            {
                FireBallActive = true;

                previousGKDifficulty = GoalKeeperLevel.share._level;
                Debug.Log("previousGKDifficulty   " + previousGKDifficulty);
                GoalKeeperLevel.share.setLevel(0);

                Shoot.share._effect.enabled = false;
                //disable trail
                FireParticle.SetActive(true);                       //Fire particles turned on  
                FireParticle2.SetActive(true);                       //Fire particles turned on  
                
                HudManager.instance.UpdateFireballs();

                if (GameConstants.getStartGamePlayTutorial() == "Yes")
                {
                    HudManager.instance.UpdateFireballsForTutorial();
                }
            }
            else
            {
                return;
            }
        }

    }

    public void restoreGKDifficulty()           //restoring GK difficulty after fireball
    {
        Debug.Log("restoreGKDifficulty   " );

        GoalKeeperLevel.share.setLevel(previousGKDifficulty);
        FireBallActive = false;
    
        FireParticle.SetActive(false);                          //Fire particles turned on 
        
        FireParticle2.SetActive(false);                       //Fire particles turned on  

    }

    public void ResetScore()
    {
        LevelEndBool = false;
        MissionEndBool = false;
        RewardShotEndBool = false;
        GameConstants.BonusCoinsCount = 0;
        GameConstants.score = 0;
        GameConstants.goalScored = 0;
        GameConstants.CoinsEarned = 0;
        NumberOfMiss = 0;
       /* LevelCompleteMenu.Watched2xRewardAd = false;
        LevelFailManager.SkipLevelAdWatched = false;*/
    }
    public IEnumerator GkDifficultySetter(int DiffLevel)
    {
      //  yield return new WaitForSeconds(0.0f);
        yield return waitSec1;
       // GoalKeeperLevel.share._previousLevel= DiffLevel;
       // GoalKeeperLevel.share._level = DiffLevel;
        GoalKeeperLevel.share.setLevel(DiffLevel);
    }
  
    #region MissionsWorking

    public void MissionDetailsSetter()
    {
        switch (GameConstants.GetMissionNumber())
        {
            case 1:
                Debug.Log("Mission 1");
                GameConstants.hitsRemaining = 10f;
                GameConstants.setTargetGoals(3);                //set target goals to achieve
               // GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                EnableGKHat();
                ChangeGkOutLook(2);
                GameConstants.MissionPassCoins= 200; //coins


                break;
            case 2:
                Debug.Log("Mission 2");
                GameConstants.hitsRemaining = 10f;

                GameConstants.MissionTargetScore = 300;                  //set target score to achieve

           //     GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                GameConstants.SetTargetScores(TargetsArray);
                EnableGKHat();
                ChangeGkOutLook(2);
                GameConstants.MissionPassCoins = 200;

                break;
            case 3:
                Debug.Log("Mission 3");
                GameConstants.hitsRemaining = 10f;
                GameConstants.setTargetGoals(5);                //set target goals to achieve
             //   GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.MissionPassCoins = 300;
                EnableGKHat();
                ChangeGkOutLook(3);

                break;
            case 4:
                Debug.Log("Mission 4");
                GameConstants.hitsRemaining = 10f;
                GameConstants.MissionTargetScore = 450;                  //set target score to achieve
         //       GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                GameConstants.SetTargetScores(TargetsArray);
                timerbool = true;                   //level has timer 
                GameConstants.timer = 45f;
                GameConstants.MissionPassCoins = 300;
                EnableGKHat();
                ChangeGkOutLook(3);

                break;
            case 5:
                Debug.Log("Mission 5");
                GameConstants.hitsRemaining = 1f;
                GameConstants.setTargetGoals(5);                //set target goals to achieve
            //    GoalKeeperLevel.share.setLevel(4);
                StartCoroutine(GkDifficultySetter(2));
                GameConstants.MissionPassCoins = 500;
                EnableGKHat();
                ChangeGkOutLook(3);
                break;
        }
    }
    void EventShootFinishMission(bool goal, Area area)
    {
        AreaShoot.SetActive(false);

        if (goal)
        {
            ManageGoalMissions(area);
        }
        else
        {
            ManageGoalMissMissions();

        }
    }
    void ManageGoalMissions(Area areaHit)
    {
        CelebrationParticles.SetActive(true);

        if (GameConstants.GetMissionMode() == 5)
        {
            //if goal is scored in this mode than we will not deduct kicks remaining //do not miss mode
        }
        else if (!timerbool)              //if timer is on 
        {
            GameConstants.hitsRemaining--;
        }

        if (GameConstants.GetMissionMode()==2 || GameConstants.GetMissionMode() == 4)       //missions with score target
        {
           // Vector3 offset = new Vector3(0, 0.5f, 0);
            if (areaHit == Area.CornerLeft || areaHit == Area.CornerRight)
            {
                scoreValue = cornerScore;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 || GameConstants.GetMode() == 29)
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;
                    Shoot.CircleMultiplier = 1;
                }
                FloatingScoreTextShow();
                GameConstants.score = GameConstants.score + scoreValue;

            }
            else if (areaHit == Area.Left || areaHit == Area.Right)
            {
                scoreValue = sidePanel;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 || GameConstants.GetMode() == 29)
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;

                    Shoot.CircleMultiplier = 1;
                }
                FloatingScoreTextShow();
                GameConstants.score = GameConstants.score + scoreValue;

            }
            else if (areaHit == Area.Top)
            {
                scoreValue = topPanel;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 || GameConstants.GetMode() == 29)
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;
                    Shoot.CircleMultiplier = 1;
                }
                FloatingScoreTextShow();
                GameConstants.score = GameConstants.score + scoreValue;

            }
            else
            {
                scoreValue = midGoal;
                if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11 || GameConstants.GetMode() == 17 || GameConstants.GetMode() == 29)
                {
                    scoreValue = scoreValue * Shoot.CircleMultiplier;

                    Shoot.CircleMultiplier = 1;
                }
                FloatingScoreTextShow();

                GameConstants.score = GameConstants.score + scoreValue;

            }
            
            HudManager.instance.UpdateMissionTargetScore();

        }
        else                                                       //missions with goals target
        {
            GameConstants.goalScored++;
            HudManager.instance.UpdateMissionTargetGoals();
            HudManager.instance.UpdateMissionGoalScored();
        }

        GoalScoreFloatingTextShow();
        HudManager.instance.updateScore();
        HudManager.instance.updateStars();

        

        HudManager.instance.UpdateKickRemaining("Decrement");
        MissionCompleteCheck();
    }
    void ManageGoalMissMissions()
    {
        if (!timerbool)              //if timer is on 
        {
            GameConstants.hitsRemaining--;
            HudManager.instance.UpdateKickRemaining("Decrement");

        }
        MissionCompleteCheck();
    }
    public void MissionCompleteCheck()
    {
        Debug.Log("LEVEL COMPLETE BOOL : " + LevelEndBool);
        if(GameConstants.GetMissionMode()== 1 || GameConstants.GetMissionMode() == 3 || GameConstants.GetMissionMode() == 5)
        {
          //  Debug.Log("goal scored " + GameConstants.goalScored);
          //  Debug.Log("goal target " + GameConstants.GetTargetGoals());

         
            if(GameConstants.goalScored>=GameConstants.GetTargetGoals() && !MissionEndBool)
            {
                MissionEndBool = true;
                LevelEndCelebrationManager.instance.GameWon();
                DelayLevelCompleteCoroutine=StartCoroutine(DelayMissionComplete(WaitGameWon));
            }
            
            else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.goalScored < GameConstants.GetTargetGoals() && !MissionEndBool)
            {
            //    Debug.Log("level should fail");
                LevelEndCelebrationManager.instance.GameLost();

                DelayLevelCompleteCoroutine = StartCoroutine(DelayMissionFailed(WaitGameLost));

            }
            else if(timerbool && GameConstants.timer <= 0 && GameConstants.goalScored < GameConstants.GetTargetGoals() && !MissionEndBool)
            {
                LevelEndCelebrationManager.instance.GameLost();

                DelayLevelCompleteCoroutine = StartCoroutine(DelayMissionFailed(WaitGameLost));
            }
        }
        else 
        {
               //int[] TargetScoresInt = GameConstants.GetTargetScores();

            if (GameConstants.score >= GameConstants.MissionTargetScore && !MissionEndBool)
            {
           //     Debug.Log("Scoreeeeeeeee issssss " + GameConstants.score);
                MissionEndBool = true;

                LevelEndCelebrationManager.instance.GameWon();
                DelayLevelCompleteCoroutine = StartCoroutine(DelayMissionComplete(WaitGameWon));
            }
            /*else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.score >= TargetScoresInt[2])            //no more kicks and score more than target 3 //level cleared
            {

                MissionComplete(1);
            }*/
            else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.score < GameConstants.MissionTargetScore && !MissionEndBool)                        //no more kicks score less than target 3
            {
                LevelEndCelebrationManager.instance.GameLost();
                DelayLevelCompleteCoroutine = StartCoroutine(DelayMissionFailed(WaitGameLost));
            }
            else if (timerbool && GameConstants.timer <= 0 && GameConstants.score < GameConstants.MissionTargetScore && !MissionEndBool)            //timer is 0 and score is less than target 3 //level failed
            {
                LevelEndCelebrationManager.instance.GameLost();
                DelayLevelCompleteCoroutine = StartCoroutine(DelayMissionFailed(WaitGameLost));
                timerbool = false;
            }
          
        }
            
    }


  /*  public void MissionComplete(float timer)
    {
        Debug.Log("MISSION COMPLETE");
        LevelEndBool = true;
        StartCoroutine(DelayMissionComplete(timer));
    }*/
     public IEnumerator DelayMissionComplete(float timer)
    {
    //    Debug.Log("MISSION FAILED");
       
        yield return new WaitForSeconds(timer);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("LevelWin");

        }
        MenuManager.GameOverBoolVideoAd = true;

        MissionsCoinsCollected();
        MarkMissionComplete();
        if (timerbool)
        {
            timerbool = false;
        }
        MenuManager.Instance.AnalyticsCallingEndWin();

    }
    public void MarkMissionComplete()
    {
        GameConstants.SetMissionPassed(GameConstants.GetMissionNumber());
    }
   public  IEnumerator DelayMissionFailed(float timer)
    {
        MissionEndBool = true;
        MenuManager.GameOverBoolVideoAd = true;

        yield return new WaitForSeconds(timer);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("LevelFail");

        }
        MenuManager.Instance.ActiveMissionFailPanel();
        MenuManager.Instance.AnalyticsCallingEndLose();

    }
    public void MissionsCoinsCollected()        //Coins and stars collected on level end
    {

        //  GenericVariables.AddCoins(GameConstants.MissionPassCoins);


        MenuManager.Instance.ActiveMissionSuccessPanel(GameConstants.MissionPassCoins);
        
    }

    #endregion

    #region RewardShot
    public void RewardShotDetailsSetter()
    {

        switch (GameConstants.GetRewardShotMode())
        {
            case 1:
           //     Debug.Log("RewardShotMode 1");
                GameConstants.hitsRemaining = 3f;
                GameConstants.setTargetGoals(3);                //set target goals to achieve
                                                                // GoalKeeperLevel.share.setLevel(4);
                                                                //   StartCoroutine(GkDifficultySetter(2));
                                                                //    khabibHat.SetActive(true);
                                                                //   ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                GameConstants.RewardShotPassCoins = 500; //coins
                break;
        }
    }

    void EventShootFinishRewardShot(bool goal, Area area)
    {
        if (AreaShoot != null)
        {
            AreaShoot.SetActive(false);

        }

        if (goal)
        {
            ManageGoalRewardShot(area);
        }
        else
        {
            ManageGoalMissRewardShot();
        }
    }

    void ManageGoalRewardShot(Area areaHit)
    {
        if(CelebrationParticles!=null)
        {

        CelebrationParticles.SetActive(true);
        }
        GameConstants.hitsRemaining--;

        if (GameConstants.GetRewardShotMode() == 1 && Shoot.DartBoardHitBool)               //working for Freekick and dartboard + no GK //ahmad
        {
             GameConstants.goalScored++;
              Shoot.DartBoardHitBool = false;

        }
            /*   if (GameConstants.GetMissionMode() == 5)
               {
                   //if goal is scored in this mode than we will not deduct kicks remaining //do not miss mode
               }
               else if (!timerbool)              //if timer is on 
               {
                   GameConstants.hitsRemaining--;
               }*/
        /*
                if (GameConstants.GetMissionMode() == 2 || GameConstants.GetMissionMode() == 4)       //missions with score target
                {*/
        // Vector3 offset = new Vector3(0, 0.5f, 0);
        /*        if (areaHit == Area.CornerLeft || areaHit == Area.CornerRight)
                    {
                        scoreValue = cornerScore;
                        FloatingScoreTextShow();
                        GameConstants.score = GameConstants.score + scoreValue;

                    }
                    else if (areaHit == Area.Left || areaHit == Area.Right)
                    {
                        scoreValue = sidePanel;

                        FloatingScoreTextShow();
                        GameConstants.score = GameConstants.score + scoreValue;

                    }
                    else if (areaHit == Area.Top)
                    {
                        scoreValue = topPanel;

                        FloatingScoreTextShow();
                        GameConstants.score = GameConstants.score + scoreValue;

                    }
                    else
                    {
                        scoreValue = midGoal;

                        FloatingScoreTextShow();

                        GameConstants.score = GameConstants.score + scoreValue;

                    }*/

        //     HudManager.instance.UpdateMissionTargetScore();

        /*  }
          else                                                       //missions with goals target
          {
              GameConstants.goalScored++;
              HudManager.instance.UpdateMissionTargetGoals();
              HudManager.instance.UpdateMissionGoalScored();
          }*/
       /* scoreValue = 1;
        FloatingScoreTextShow();*/
        GoalScoreFloatingTextShow();
        //  HudManager.instance.updateScore();
        //  HudManager.instance.updateStars();

        HudManager.instance.UpdateMissionTargetGoals();
        HudManager.instance.UpdateMissionGoalScored();

        HudManager.instance.UpdateKickRemaining("Decrement");
        RewardShotCompleteCheck();
    }
    void ManageGoalMissRewardShot()
    {
        if (!timerbool)              //if timer is on 
        {
            GameConstants.hitsRemaining--;
            HudManager.instance.UpdateKickRemaining("Decrement");

        }
        RewardShotCompleteCheck();
    }
    public void RewardShotCompleteCheck()
    {

        if (GameConstants.goalScored >= GameConstants.GetTargetGoals() && !RewardShotEndBool)
        {
            RewardShotEndBool = true;
            LevelEndCelebrationManager.instance.GameWon();
            DelayLevelCompleteCoroutine = StartCoroutine(DelayRewardShotComplete(WaitGameWon));
        }

        else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.goalScored < GameConstants.GetTargetGoals() && !RewardShotEndBool)
        {
        //    Debug.Log("RewardShot should fail");
            RewardShotEndBool = true;

            //    LevelEndCelebrationManager.instance.GameLost();

            LevelEndCelebrationManager.instance.GameWon();      //we have to show player celebrating in lucky shot


            DelayLevelCompleteCoroutine = StartCoroutine(DelayRewardShotFailed(WaitGameLost));

        }
        else if (timerbool && GameConstants.timer <= 0 && GameConstants.goalScored < GameConstants.GetTargetGoals() && !RewardShotEndBool)
        {
            RewardShotEndBool = true;

            // LevelEndCelebrationManager.instance.GameLost();

            LevelEndCelebrationManager.instance.GameWon();      //we have to show player celebrating in lucky shot

            DelayLevelCompleteCoroutine = StartCoroutine(DelayRewardShotFailed(WaitGameLost));
        }
    }

    public IEnumerator DelayRewardShotComplete(float timer)
    {
      //  Debug.Log("RewardShot FAILED");
        RewardShotEndBool = true;

        yield return new WaitForSeconds(timer);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("LevelWin");

        }
        MenuManager.GameOverBoolVideoAd = true;

        RewardShotCoinsCollected();
      //  MarkMissionComplete();
        if (timerbool)
        {
            timerbool = false;
        }
        MenuManager.Instance.AnalyticsCallingEndWin();

    }

    public IEnumerator DelayRewardShotFailed(float timer)
    {
        RewardShotEndBool = true;
        MenuManager.GameOverBoolVideoAd = true;

        yield return new WaitForSeconds(timer);
        if (SoundManagerNew.Instance != null)
        {
            // SoundManagerNew.Instance.playSound("LevelFail");
            SoundManagerNew.Instance.playSound("LevelWin");


        }
        GameConstants.RewardShotPassCoins = GameConstants.RewardShotPassCoins / 2;
        MenuManager.Instance.ActiveRewardKickCompletePanel(GameConstants.RewardShotPassCoins);
        MenuManager.Instance.AnalyticsCallingEndLose();

    }
    public void RewardShotCoinsCollected()        //Coins and stars collected on level end
    {

        //  GenericVariables.AddCoins(GameConstants.MissionPassCoins);
        MenuManager.Instance.ActiveRewardKickCompletePanel(GameConstants.RewardShotPassCoins);

    }

    #endregion

    #region RewardShot2
    public void RewardShot2DetailsSetter()
    {

                GameConstants.hitsRemaining = 1f;
                GameConstants.setTargetGoals(1);                //set target goals to achieve
                                                                // GoalKeeperLevel.share.setLevel(4);
                                                                //   StartCoroutine(GkDifficultySetter(2));
                                                                //    khabibHat.SetActive(true);
                                                                //   ChangeGkOutLook(2);

                EnvironmentNumber = 1;
                SetEnvironment(EnvironmentNumber);
                GameConstants.RewardShotPassCoins = 50; //coin
    }

    void EventShootFinishRewardShot2(bool goal, Area area)
    {
        if (AreaShoot != null)
        {
            AreaShoot.SetActive(false);

        }

        if (goal)
        {
            ManageGoalRewardShot2(area);
        }
        else
        {
            ManageGoalMissRewardShot2();
        }
    }

    void ManageGoalRewardShot2(Area areaHit)
    {
        if (CelebrationParticles != null)
        {

            CelebrationParticles.SetActive(true);
        }
        GameConstants.hitsRemaining--;

        if (GameConstants.GetRewardShotMode() == 1 && Shoot.DartBoardHitBool)               //working for Freekick and dartboard + no GK //ahmad
        {
            GameConstants.goalScored++;
            Shoot.DartBoardHitBool = false;

        }
      
        GoalScoreFloatingTextShow();
        //  HudManager.instance.updateScore();
        //  HudManager.instance.updateStars();

        HudManager.instance.UpdateMissionTargetGoals();
        HudManager.instance.UpdateMissionGoalScored();

        HudManager.instance.UpdateKickRemaining("Decrement");
        RewardShotCompleteCheck2();
    }
    void ManageGoalMissRewardShot2()
    {
        if (!timerbool)              //if timer is on 
        {
            GameConstants.hitsRemaining--;
            HudManager.instance.UpdateKickRemaining("Decrement");

        }
        RewardShotCompleteCheck2();
    }
    public void RewardShotCompleteCheck2()
    {

        if (GameConstants.goalScored >= GameConstants.GetTargetGoals() && !RewardShotEndBool)
        {
            RewardShotEndBool = true;
            LevelEndCelebrationManager.instance.GameWon();
            DelayLevelCompleteCoroutine = StartCoroutine(DelayRewardShotComplete2(WaitGameWon));
        }

        else if (!timerbool && GameConstants.hitsRemaining == 0 && GameConstants.goalScored < GameConstants.GetTargetGoals() && !RewardShotEndBool)
        {
            //    Debug.Log("RewardShot should fail");
            RewardShotEndBool = true;

            //    LevelEndCelebrationManager.instance.GameLost();

            LevelEndCelebrationManager.instance.GameWon();      //we have to show player celebrating in lucky shot


            DelayLevelCompleteCoroutine = StartCoroutine(DelayRewardShotFailed2(WaitGameLost));

        }
        else if (timerbool && GameConstants.timer <= 0 && GameConstants.goalScored < GameConstants.GetTargetGoals() && !RewardShotEndBool)
        {
            RewardShotEndBool = true;

            // LevelEndCelebrationManager.instance.GameLost();

            LevelEndCelebrationManager.instance.GameWon();      //we have to show player celebrating in lucky shot

            DelayLevelCompleteCoroutine = StartCoroutine(DelayRewardShotFailed2(WaitGameLost));
        }
    }

    public IEnumerator DelayRewardShotComplete2(float timer)
    {
        //  Debug.Log("RewardShot FAILED");
        RewardShotEndBool = true;

        yield return new WaitForSeconds(timer);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("LevelWin");

        }
        MenuManager.GameOverBoolVideoAd = true;

        RewardShotCoinsCollected2();
        //  MarkMissionComplete();
        if (timerbool)
        {
            timerbool = false;
        }
        MenuManager.Instance.AnalyticsCallingEndWin();

    }

    public IEnumerator DelayRewardShotFailed2(float timer)
    {
        RewardShotEndBool = true;
        MenuManager.GameOverBoolVideoAd = true;

        yield return new WaitForSeconds(timer);
        if (SoundManagerNew.Instance != null)
        {
            // SoundManagerNew.Instance.playSound("LevelFail");
            SoundManagerNew.Instance.playSound("LevelWin");


        }
        GameConstants.RewardShotPassCoins = 25;//GameConstants.RewardShotPassCoins / 2;
        RewardShotEndMenu.reward= RewardShotEndMenu.RewardState.coin;
        MenuManager.Instance.ActiveRewardKickCompletePanel(GameConstants.RewardShotPassCoins);
        MenuManager.Instance.AnalyticsCallingEndLose();

    }
    public void RewardShotCoinsCollected2()        //Coins and stars collected on level end
    {

        //  GenericVariables.AddCoins(GameConstants.MissionPassCoins);
        MenuManager.Instance.ActiveRewardKickCompletePanel(GameConstants.RewardShotPassCoins);

    }

    #endregion
    #region rate us
    public IEnumerator ShowRateUs(float timer)
    {
        yield return new WaitForSeconds(timer);
        //  GamePlayManager.RateUsNumber++;
        // if (GamePlayManager.RateUsNumber % 4 == 0)
        if (GameConstants.GetLevelNumber() % 3 == 0)  //previously it was 4
        {
            Debug.Log("RateUs");
            // AdController.instance.PromptRateMenu();
            // GameConstants.CanShowRateUsBool = true; 
            /*if (GameConstants.getStartGamePlayTutorial() != "Yes" && GameConstants.GetGameMode() == "Classic" && GameConstants.CanShowRateUsBool)
            {*/
            AdController.instance.PromptRateMenu();                  //SHow rate US
                                                                     //     GameConstants.CanShowRateUsBool = false;
                                                                     //    }
                                                                     //   GamePlayManager.RateUsNumber = 0;
        }
        else if (GameConstants.TrophiesLevelList.Contains(GameConstants.GetLevelNumber())) //will be used when called from trophy end cutscene
        {
            Debug.Log("RateUs");

            AdController.instance.PromptRateMenu();                  //SHow rate US

        }
        /*  else
          {

              GameConstants.CanShowRateUsBool = false;
          }*/
    }
    #endregion


    private void OnDestroy()
    {
        if (GameConstants.GetGameMode() == "Classic")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinish;

        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinishMission;

        }else if (GameConstants.GetGameMode() == "RewardShot")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinishRewardShot;

        }else if (GameConstants.GetGameMode() == "RewardShot2")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinishRewardShot2;

        }
    }
    public void EventsUnlink()
    {
        if (GameConstants.GetGameMode() == "Classic")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinish;

        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinishMission;

        }
        else if (GameConstants.GetGameMode() == "RewardShot")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinishRewardShot;

        }
        else if (GameConstants.GetGameMode() == "RewardShot2")
        {
            GoalDetermine.EventFinishShoot -= EventShootFinishRewardShot2;

        }
    }
}
