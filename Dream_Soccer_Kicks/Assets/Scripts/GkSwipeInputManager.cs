﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GkSwipeInputManager : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 startPos, endPos;
   [SerializeField] float threshold =0.5f;
    void Start()
    {
        startPos= endPos;
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_EDITOR 
        if( !MultiplayerMatchLogic.instance.MultiplayerLevelEndBool)
        {
            MoveInput();

        }
#elif UNITY_ANDROID || UNITY_IOS
        if( !MultiplayerMatchLogic.instance.MultiplayerLevelEndBool)
        {
            TouchInput();

        }
#endif

    }
    void MoveInput()
    {
        if(Input.GetMouseButton(0))
        {
            if(Input.GetMouseButtonDown(0))
            {
                startPos = Input.mousePosition;
                
            }
            endPos = Input.mousePosition;
            DecideDirection();

        }
       /* if (Input.GetMouseButtonUp(0))
        {
            *//*if (DecideDirection()!= Vector3.zero)
            {*//*
          //  DecideDirection();
           //}
        }*/
    }
    void TouchInput()
    {
        if (Input.touchCount>0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase==TouchPhase.Began)
            {
                startPos = touch.position;

            }
            if (touch.phase == TouchPhase.Moved)
            {
                endPos = touch.position;

            }
        
            DecideDirection();

        }
        
    }
    void DecideDirection()
    {
       // Vector3 direction = Vector3.zero;
        if(Mathf.Abs(endPos.x- startPos.x)> threshold)
        {
             if(endPos.x> startPos.x)
            {
              //  direction = Vector3.right;
                GoalKeeperHorizontalFly.share.MoveGKToRight();
            }
            else
            {
                //  direction = Vector3.left;
                GoalKeeperHorizontalFly.share.MoveGKToLeft();
            }
        }
       //return direction;
    }
}
