﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalKeeperLookAtBall : MonoBehaviour
{
    public GameObject KeeperHead;

    void Update()
    {
        //ahmad //for GK head movement
        if (!GamePlayManager.instance.LevelEndBool && !GamePlayManager.instance.MissionEndBool && !MultiplayerMatchLogic.instance.MultiplayerLevelEndBool && !GamePlayManager.instance.RewardShotEndBool)
        {
            var direction = new Vector3(Shoot.share._cachedTrans.transform.position.x, Shoot.share._cachedTrans.transform.position.y, Shoot.share._cachedTrans.transform.position.z) - KeeperHead.transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            rotation.eulerAngles = new Vector3(Mathf.Clamp(rotation.eulerAngles.x, -500, 500), Mathf.Clamp(rotation.eulerAngles.y, 110, 250), rotation.eulerAngles.z);
            KeeperHead.transform.rotation = rotation;

        }
        /*if (Shoot.share._canControlBall)
        {
			KeeperHead.transform.LookAt(Shoot.share._ball.transform);

		}*/
    }
}
