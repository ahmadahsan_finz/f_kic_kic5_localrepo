﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : MonoBehaviour
{
    protected void OnCollisionEnter(Collision other)
    {
        string tag = other.gameObject.tag;
        Debug.Log("OnCollisionEnter ground script  ");
        if (tag.Equals("Boxes")|| tag.Equals("Drums"))
        {
            Debug.Log("BoxesDestroyer Boxes  ");
            StartCoroutine(BoxesDestroyer(other.gameObject));
        }

    }
    IEnumerator BoxesDestroyer(GameObject box)			//to destroy the box we hit after some time
    {
        Debug.Log("BoxesDestroyer Boxes  ");
        yield return new WaitForSeconds(2);
        box.SetActive(false);
           
    }
}
