using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HitTag : MonoBehaviour
{

    public GameObject ball;
    Ray ray;
    RaycastHit hit;
    public GameObject pin;
    public bool hitSomething = false;
    public Text Textfield;
    public void SetText(string text)
    {
        Textfield.text = text;
       // text = "Europe";
    }
    
    void Update()
    {
        //hit and find tag with fixed position and rotation of pin and ball Aqsa Gilani
        
        // pin.transform.localPosition = new Vector3(0.35f, 0.51f, -1243.42f);
        pin.transform.localPosition = new Vector3(-0.2f, 0.69f, -1243.42f);
        // ball.transform.localPosition = new Vector3(1.22f, -9.104356f, -1234.62f);

        ray = new Ray(transform.position, transform.forward);


        Vector3 forward = transform.TransformDirection(Vector3.forward * 5f);
        Debug.DrawRay(transform.position, forward, Color.green, 0.0f, true);


        if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Africa")
        {

            ball.transform.rotation = Quaternion.Euler(0, 208, 0);
            pin.transform.position = hit.point;
            //hit.transform.GetComponent<Renderer>().material.color = Color.red;
            hitSomething = true;
            SetText("Africa");
            Debug.Log("Hit Africa");
        }
        else
        {
            pin.transform.rotation = Quaternion.Euler(0, 208, 0);
            pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
            // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
            hitSomething = false;
        }

        if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Antarctica")
        {

            ball.transform.rotation = Quaternion.Euler(16.746f, 271.472f, -94.555f);
            pin.transform.position = hit.point;
            // hit.transform.GetComponent<Renderer>().material.color = Color.red;
            hitSomething = true;
            SetText("Antarctica");
            Debug.Log("Hit Antarctica");
        }
        else
        {
            pin.transform.rotation = Quaternion.Euler(16.746f, 271.472f, -94.555f);
            pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
            // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
            hitSomething = false;
        }
           if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Asia")
           {

               ball.transform.rotation = Quaternion.Euler(36.118f, 247.205f, 9.435f);
               pin.transform.position = hit.point;
             //  hit.transform.GetComponent<Renderer>().material.color = Color.red;
               hitSomething = true;
            SetText("Asia");
            Debug.Log("Hit Asia");
           }
           else
           {
               pin.transform.rotation = Quaternion.Euler(36.118f, 247.205f, 9.435f);
               pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
               // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
               hitSomething = false;
              
           }
         if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Australia")
         {

            //ball.transform.rotation = Quaternion.Euler(8.099f, 289.118f, -17.51f);
            ball.transform.rotation = Quaternion.Euler(-134.878f, 244.933f, -1.315002f);
            pin.transform.position = hit.point;
         //    hit.transform.GetComponent<Renderer>().material.color = Color.red;
             hitSomething = true;
            SetText("Australia");
            Debug.Log("Hit Australia");
         }
         else
         {
            pin.transform.rotation = Quaternion.Euler(-134.878f, 244.933f, -1.315002f);
            pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
             // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
             hitSomething = false;
         }
         if (Physics.Raycast(ray, out hit) && hit.collider.tag == "Europe")
         {

            // ball.transform.rotation = Quaternion.Euler(35.284f, 206.851f, 0.315f);  
            // ball.transform.rotation = Quaternion.Euler(20.563f, 235.071f, 56.314f);
            ball.transform.rotation = Quaternion.Euler(20.563f, 150.332f, 30.585f);
            pin.transform.position = hit.point;
            // hit.transform.GetComponent<Renderer>().material.color = Color.red;
             hitSomething = true;
             SetText("Europe");
             Debug.Log("Hit Europe");
         }
         else
         {
            //pin.transform.rotation = Quaternion.Euler(35.284f,206.851f, 0.315f);
            //pin.transform.rotation = Quaternion.Euler(20.563f, 235.071f, 56.314f);
            pin.transform.rotation = Quaternion.Euler(20.563f, 150.332f, 30.585f);
            pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
             // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
             hitSomething = false;
            
         }
         if (Physics.Raycast(ray, out hit) && hit.collider.tag == "NorthAmerica")
         {

             ball.transform.rotation = Quaternion.Euler(12.81f, 91.17f, -27.919f);
             pin.transform.position = hit.point;
           //  hit.transform.GetComponent<Renderer>().material.color = Color.red;
             hitSomething = true;
            SetText("NorthAmerica");
            Debug.Log("Hit NorthAmerica");
         }
         else
         {
             pin.transform.rotation = Quaternion.Euler(12.81f, 91.17f, -27.919f);
             pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
             // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
             hitSomething = false;
          
         }
         if (Physics.Raycast(ray, out hit) && hit.collider.tag == "SouthAmerica")
         {

             ball.transform.rotation = Quaternion.Euler(-16.429f, 121.497f, 13.205f);
             pin.transform.position = hit.point;
            // hit.transform.GetComponent<Renderer>().material.color = Color.red;
             hitSomething = true;
            SetText("SouthAmerica");
            Debug.Log("Hit SouthAmerica");
         }
         else
         {
             pin.transform.rotation = Quaternion.Euler(-16.429f, 121.497f, 13.205f);
             pin.transform.rotation = Quaternion.Euler(360, 0, 0f);
             // ball.transform.rotation = Quaternion.Euler(0, 198.08f, 0);
             hitSomething = false;
         }
    }

}

