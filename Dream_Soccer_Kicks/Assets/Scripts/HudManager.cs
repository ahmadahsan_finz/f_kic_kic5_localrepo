﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManager : MonoBehaviour
{
    
    public static HudManager instance;
    public GameObject UI_Canvas;
    public Text CurrentScore, DistanceText, KickRemaining,LevelCompleteText,TimerText;
    public static int NumberOfTargetScores = 3;
    public Text[] TargetScoreText = new Text[NumberOfTargetScores];
    public int[] TargetScoresInt = new int[NumberOfTargetScores];
    public GameObject LevelSuccessPanel,LevelFailPanel;
    public Image ProgressSlider1;
    public Image ProgressSlider2;
    public Image ProgressSlider3;
    public GameObject[] StarImageArray;
    public Text FireBallsText;
    public Text FireBallsText2;
    public Text MultiplierText;
  //  public GameObject FloatingText;
    public GameObject TimerPanel;
    public GameObject KickPanel;
    int FireballCount;

    //for tournament
    [Header("tournament Component")]
    public Text textCountGoalPlayer;
    public Text textCountGoalAI;
    public Text textCountPenalties;
    public Text textTournamentRoundResult;
    public Text[] textTournamentRoundName;
    public GameObject TournamentPanel;
    public GameObject TargetScoringPanel;
    public GameObject[] GoalsPlayer;
    public GameObject[] GoalsMissPlayer;
    public GameObject[] GoalsAI;
    public GameObject[] GoalsMissAI;


    //for Missions
    [Header("Missions Component")]
    public GameObject MissionsHud;
    public GameObject StarsPanel;
    public GameObject GoalScorePanel;
    public GameObject TargetGoalPanel;
    public GameObject ScorePanel;
    public GameObject FireballObject;
    public Text GoalScored;
    public Text TargetGoals;

    [Header("Onscreen Buttons Component")]
    public GameObject MultiplayerButton;
    public GameObject CustomizeButton;
    public GameObject SkipLevelButton;
    public GameObject Fireball_AdButton;
    public GameObject MultiplayerButton_Notificaiton;
    public GameObject CustomizeButton_Notificaiton;

    [Header("Reward Key Component")]
    public GameObject KeysPanel;
    public GameObject[] KeysCollectedArray;


    [Header("Onscreen Testing Buttons")]
    public GameObject WinLevelButton;
    public GameObject LoseLevelButton;
    public static bool TestingWinButtonPressedBool=false;
    public static bool TestingLoseButtonPressedBool=false;

    [Header("Onscreen Level details")]
    public GameObject LevelDetailsTextParent;
    public GameObject HitThePoletext;

    private void OnEnable()
    {
        PluginCallBackManager.SkipLevelRewardAdCompleteInGameplay_ += giveRewardSkipLevel;
        PluginCallBackManager.FireballRewardAdComplete_ += giveRewardFireball;

    }
    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    //MissionMode 1: Kicks with Goals
    //MissionMode 2: Kicks with Scores
    //MissionMode 3: Timer with Goals
    //MissionMode 4: Timer with Scores
    //MissionMode 5: Kicks with Bulleyes
    //MissionMode 6: Timer with Bulleyes
    //MissionMode 7: Kicks with do not miss
    //MissionMode 8: Timer with do not miss

    void Start()
    {
        if (GameConstants.GetGameMode() == "Missions")
        {
            MissionsHud.SetActive(true);
            TournamentPanel.SetActive(false);
            TargetScoringPanel.SetActive(true);
            StarsPanel.SetActive(false);
            FireballObject.SetActive(false);
            KeysPanel.SetActive(false);
            
            if (GameConstants.GetMissionMode() == 1 || GameConstants.GetMissionMode() == 3 || GameConstants.GetMissionMode() == 5)     //goals mode
            {
                GoalScorePanel.SetActive(true);
                TargetGoalPanel.SetActive(true);
                ScorePanel.SetActive(false);
            }
            else                                                //score mode
            {
                GoalScorePanel.SetActive(false);
                TargetGoalPanel.SetActive(true);
                ScorePanel.SetActive(true);

            }
        }
        else if (GameConstants.GetGameMode() == "Classic")                                                    //is mode is classic
        {
           

            if (GenericVariables.getFireRemaining() == 0)
            {
                EnableDisableFireball_AdButton(true);
                EnableDisableFireBallButton(false);
            }

            MissionsHud.SetActive(false);

            if (GameConstants.GetMode() == 10)
            {
                KeysPanel.SetActive(true);
                TournamentHudEnabler();
            }
            else //if(GameConstants.CutsceneIsOn!=true)
            {
                TournamentPanel.SetActive(false);
                TargetScoringPanel.SetActive(true);
                KeysPanel.SetActive(true);
                StarsPanel.SetActive(true);
             //   ScorePanel.SetActive(true);
            }
            if(GameConstants.GetMode()==8)
            {

                EnableLevelDetailOnscreenTexts();

            }

            UpdateFireballs();                              //fireballs count display

            if (GameConstants.getStartGamePlayTutorial() == "Yes")
            {
                UpdateFireballsForTutorial();
            }
        }
        else if (GameConstants.GetGameMode() == "RewardShot")
        {
            MissionsHud.SetActive(true);
            TournamentPanel.SetActive(false);
            TargetScoringPanel.SetActive(true);
            StarsPanel.SetActive(false);
            FireballObject.SetActive(false);
            KeysPanel.SetActive(false);

            GoalScorePanel.SetActive(true);
            TargetGoalPanel.SetActive(true);
            ScorePanel.SetActive(false);
        }
        else if (GameConstants.GetGameMode() == "RewardShot2")
        {
            MissionsHud.SetActive(true);
            TournamentPanel.SetActive(false);
            TargetScoringPanel.SetActive(true);
            StarsPanel.SetActive(false);
            FireballObject.SetActive(false);
            KeysPanel.SetActive(false);

            GoalScorePanel.SetActive(false);
            TargetGoalPanel.SetActive(false);
            ScorePanel.SetActive(false);
        }

        if (AdConstants.isTestingLicenseAvailable())    //testing
        {
            WinLevelButton.SetActive(true);
            LoseLevelButton.SetActive(true);
            TestingWinButtonPressedBool = false;
            TestingLoseButtonPressedBool = false;
        }
        else
        {
            WinLevelButton.SetActive(false);
            LoseLevelButton.SetActive(false);

            TestingWinButtonPressedBool = false;
            TestingLoseButtonPressedBool = false;
        }
    }
    public void EnableLevelDetailOnscreenTexts()            //show HIT THE POLE TEXT
    {
        GamePlayManager.instance.LevelDetailTextShowInGameplay();
        //LevelDetailsTextParent.SetActive(true);
        //HitThePoletext.SetActive(true);
        //Invoke("DisableLevelDetailOnscreenTexts",4f);
    }
    public void DisableLevelDetailOnscreenTexts()
    {
        LevelDetailsTextParent.SetActive(false);
        HitThePoletext.SetActive(false);
    }

    public void TournamentHudEnabler()
    {
        MissionsHud.SetActive(false);

        TargetScoringPanel.SetActive(false);
        TournamentPanel.SetActive(true);
    }
    public void updateStars()
    {
        if (GameConstants.score >= (float)TargetScoresInt[0])
        {
            StarImageArray[0].SetActive(true);

        }
        if (GameConstants.score >= (float)TargetScoresInt[1])
        {
            StarImageArray[0].SetActive(true);
            StarImageArray[1].SetActive(true);
        }
        if (GameConstants.score >= (float)TargetScoresInt[2])
        {
            StarImageArray[0].SetActive(true);
            StarImageArray[1].SetActive(true);
            StarImageArray[2].SetActive(true);
        }

    }
    public void StarsHudDisplayReset()
    {
        StarImageArray[0].SetActive(false);
        StarImageArray[1].SetActive(false);
        StarImageArray[2].SetActive(false);
    }
    public void updateScore()
    {
        CurrentScore.text = GameConstants.score.ToString();
        //    TargetScoresInt[2];
        // Debug.Log("TArget final is " + TargetScoresInt[2]);
        if (GameConstants.score <= (float)TargetScoresInt[0])
        {
            ProgressSlider1.fillAmount = (GameConstants.score / (float)TargetScoresInt[0]);
        }
        else if (GameConstants.score <= (float)TargetScoresInt[1])
        {
            ProgressSlider1.fillAmount = 1;
            ProgressSlider2.fillAmount = (GameConstants.score / (float)TargetScoresInt[1]);
            
        }
        else if (GameConstants.score <= (float)TargetScoresInt[2]) {
            ProgressSlider1.fillAmount = 1;
            ProgressSlider2.fillAmount = 1;
            ProgressSlider3.fillAmount = (GameConstants.score / (float)TargetScoresInt[2]);
        }
        else if (GameConstants.score >= (float)TargetScoresInt[2])
        {
            ProgressSlider1.fillAmount = 1;
            ProgressSlider2.fillAmount = 1;
            ProgressSlider3.fillAmount = 1;
        }

    }
    public void UpdateMissionTargetScore()
    {
        TargetGoals.text = GameConstants.MissionTargetScore.ToString();
    }
    public void UpdateMissionTargetGoals()
    {
        TargetGoals.text = GameConstants.GetTargetGoals().ToString();
    }
    public void UpdateMissionGoalScored()
    {
        GoalScored.text =GameConstants.goalScored.ToString();
    }

    public void UpdateKickRemaining(string status)
    {
        if(status=="Increment")
        {
           instance.KickPanel.GetComponent<Animation>().Play("KickRemainingUIpanel");
            ExtraBallScoreMultiScript.instance.ExtraBallBoolHit = false;                            
        }
			
        else if(status == "Decrement" && ExtraBallScoreMultiScript.instance.ExtraBallBoolHit != true)       //if ball have not extra ball
        {
            KickPanel.GetComponent<Animation>().Play("KickRemainingUIpanelDeduction");
        }
        KickRemaining.text = GameConstants.hitsRemaining.ToString();
    }

    public void UpdateDistance()
    {
        DistanceText.text = ((int)Shoot.share.BallDistanceFromGoal_Zaxis).ToString() + "m";
    }

    public void UpdateTargetScores()
    {
        TargetScoresInt = GameConstants.GetTargetScores();
        for (int i=0; i< NumberOfTargetScores; i++)
        {
            TargetScoreText[i].text = TargetScoresInt[i].ToString();
        }   
    }

    public void UpdateLevelComplete()
    {
        LevelCompleteText.text = "Level Completed";
    }


   
   
    public void UpdateTimer()
    {
        TimerText.text = ((int)GameConstants.timer).ToString();
    }
    public void updateTournamentScores()
    {
        textCountGoalPlayer.text=TournamentHandler.instance.PlayerScore.ToString();
        textCountGoalAI.text = TournamentHandler.instance.AI_Score.ToString();
        textCountPenalties.text = TournamentHandler.instance.Count_penalties.ToString();
    }

    public void updateTournamanentRoundName()
    {
       if( TournamentHandler.instance.QuarterFinal)
        {
         //   textTournamentRoundName.text = "QUARTER FINAL";
            textTournamentRoundName[0].gameObject.SetActive(true);
            textTournamentRoundName[1].gameObject.SetActive(false);
            textTournamentRoundName[2].gameObject.SetActive(false);

        }
        else if (TournamentHandler.instance.SemiFinal)
        {
           // textTournamentRoundName.text = "SEMI FINAL";
            textTournamentRoundName[0].gameObject.SetActive(false);
            textTournamentRoundName[1].gameObject.SetActive(true);
            textTournamentRoundName[2].gameObject.SetActive(false);
        }
        else if (TournamentHandler.instance.Final)
        {
          //  textTournamentRoundName.text = "FINAL"; 
            textTournamentRoundName[0].gameObject.SetActive(false);
            textTournamentRoundName[1].gameObject.SetActive(false);
            textTournamentRoundName[2].gameObject.SetActive(true);
        }
    }

    public void UpdateFireballs()                                 //fireballs count display
    {
        //    Debug.Log("update fireballs Hudmanager");
        //  FireBallsText.text = GameConstants.getNumberOfFireballs().ToString();
        FireballCount = GenericVariables.getFireRemaining();
        FireBallsText.text = FireballCount.ToString();
        if (FireballCount == 0)
        {
            EnableDisableFireball_AdButton(true);
            EnableDisableFireBallButton(false);
        }
        else
        {
            EnableDisableFireball_AdButton(false);
            EnableDisableFireBallButton(true);
        }
    
    } 
    public void UpdateFireballsForTutorial()                                 //fireballs count display
    {
    //    Debug.Log("update fireballs Hudmanager");
      //  FireBallsText.text = GameConstants.getNumberOfFireballs().ToString();
        FireBallsText2.text = GenericVariables.getFireRemaining().ToString();

    }

    public void TimerPanelEnabler()
    {
        TimerPanel.SetActive(true);
        KickPanel.SetActive(false);
    }

    public void KicksPanelEnabler()
    {
        TimerPanel.SetActive(false);
        KickPanel.SetActive(true);
    }

    public void PlayerGoalTournamentUpdate(int index)                 //tournament goaling scoring and displaying in hud
    {
        GoalsPlayer[index].SetActive(true);
    }

    public void AIGoalTournamentUpdate(int index)                   //tournament goaling scoring and displaying in hud
    {
        GoalsAI[index].SetActive(true);

    } 

    public void PlayerGoalMissTournamentUpdate(int index)                 //tournament goaling scoring and displaying in hud
    {
        GoalsMissPlayer[index].SetActive(true);
    }

    public void AIGoalMissTournamentUpdate(int index)                   //tournament goaling scoring and displaying in hud
    {
        GoalsMissAI[index].SetActive(true);

    }

    public void GoalTournamentReset()                             //tournament goaling scoring and displaying in hud reset
    {
        for(int i=0;i< GoalsAI.Length;i++)
        {
            GoalsAI[i].SetActive(false);
            GoalsPlayer[i].SetActive(false);
            
            GoalsMissAI[i].SetActive(false);
            GoalsMissPlayer[i].SetActive(false);
        }
        
    }
    public void DisableExtraObjectFromScreen()
    {
        DistanceText.gameObject.SetActive(false);
        FireballObject.gameObject.SetActive(false);
        TournamentPanel.gameObject.SetActive(false);
        TargetScoringPanel.gameObject.SetActive(false);

        CustomizeButton.gameObject.SetActive(false);
        MultiplayerButton.gameObject.SetActive(false);
        SkipLevelButton.gameObject.SetActive(false);
        Fireball_AdButton.gameObject.SetActive(false);
        KeysPanel.SetActive(false);

        WinLevelButton.SetActive(false);
        LoseLevelButton.SetActive(false);

        if (GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2")
        {
            HudManagerMultiplayer.instance.EnableDisableMultiplayerHud(false);
            HudManagerMultiplayer.instance.EnableDisableGKLeftRightButtons(false);
        }
    }

    public void EnableDisableFireBallButton(bool active)
    {
        FireballObject.gameObject.SetActive(active);

    }
    public void FireButtonEnableDisableInGamePlay()
    {
        if (GenericVariables.getFireRemaining() == 0)    //firebutton ad enable
        {
           EnableDisableFireball_AdButton(true);
           EnableDisableFireBallButton(false);
        }
        else if (GameConstants.getStartGamePlayTutorial() == "Yes" && GameConstants.GetLevelNumber() == 1)
        {
            EnableDisableFireBallButton(false);

        }
        else
        {
            EnableDisableFireBallButton(true);

        }
    }

    #region GameplayButtonsWorking

    public void EnableDisableMultiplayerButton(bool active)
    {
        StartCoroutine(DelayEnableDisableMultiplayerButton(active));
    }
    IEnumerator DelayEnableDisableMultiplayerButton(bool active)
    {
        if (active == true)
        {
            yield return new WaitForSeconds(2f);
            MultiplayerButton.SetActive(active);
            if(GameConstants.GetNewNotification_MultiplayerButton_inGameplay()==1)
            {
                MultiplayerButton_Notificaiton.SetActive(true);

            }
            else
            {
                MultiplayerButton_Notificaiton.SetActive(false);
            }
        }
        else
        {
            MultiplayerButton.SetActive(active);
        }
    }
    public void OnClickMultiplayerButton()
    {
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MultplayerPenalty_GameplayButton_Clicked, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);

        Time.timeScale = 0;
        MenuManager.Instance.UIMultiplayerOfferDialog.SetActive(true);
        MenuManager.Instance.MainPanels[1].gameObject.SetActive(false);     //active false incase we press both button together

        if (GameConstants.GetNewNotification_MultiplayerButton_inGameplay() == 1)
        {
              GameConstants.SetNewNotification_MultiplayerButton_inGameplay(0);
            MultiplayerButton_Notificaiton.SetActive(false);

        }
        AdConstants.currentState = AdConstants.States.OnPause;
        AdController.instance.ChangeState();
    }

    public void EnableDisableCustomizeButton(bool active)
    {
        StartCoroutine(DelayEnableDisableCustomizeButton(active));
    }
    IEnumerator DelayEnableDisableCustomizeButton(bool active)
    {
        if (active == true)
        {
            yield return new WaitForSeconds(1f);
            CustomizeButton.SetActive(active);
            if(GameConstants.GetNewNotification_CustomizationButton_InGamePlay()==1)
            {
                CustomizeButton_Notificaiton.SetActive(true);
            }
            else
            {
                CustomizeButton_Notificaiton.SetActive(false);

            }
        }
        else
        {
            CustomizeButton.SetActive(active);
        }
    }
    public void OnClickCustomizeButton()
    {
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.Customization_GameplayButton_Clicked, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);

        Time.timeScale = 0;

        MenuManager.Instance.MainPanels[1].gameObject.SetActive(true);

        MenuManager.Instance.UIMultiplayerOfferDialog.SetActive(false);     //active false incase we press both button together

       
        if (GameConstants.GetNewNotification_CustomizationButton_InGamePlay() == 1)
        {
            GameConstants.SetNewNotification_CustomizationButton_InGamePlay(0);
            CustomizeButton_Notificaiton.SetActive(false);
        }

        AdConstants.currentState = AdConstants.States.OnPause;
        AdController.instance.ChangeState();
        //  UIAnimations.instance.MainButton(1);
    }
    public void EnableDisableSkipLevelButton(bool active)
    {
        StartCoroutine(DelayEnableDisableSkipLevelButton(active));
    }
    IEnumerator DelayEnableDisableSkipLevelButton(bool active)
    {
        if (active == true)
        {
            yield return new WaitForSeconds(1f);
            SkipLevelButton.SetActive(active);
        }
        else
        {
            SkipLevelButton.SetActive(active);
        }
    }

    public void OnClickSkipLevelButton()
    {
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.SkipLevel_GameplayButton_Clicked, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);

        GenericVariables.RewardAdIndex = 5;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    public void giveRewardSkipLevel()
    {
        StartCoroutine(DelayGiveRewardSkipLevel());
    }
    public IEnumerator DelayGiveRewardSkipLevel()
    {
        Time.timeScale = 1;
        GenericVariables.RewardAdIndex = -1;
        LevelFailManager.SkipLevelAdWatched = true;
        yield return new WaitForSeconds(1);
        /*     MenuManager.Instance.moveObject("coins", SkipLevelButton.gameObject);
             MenuManager.Instance.moveConsumable();
             yield return new WaitForSeconds(3.0f);
             GenericVariables.AddCoins(GameConstants.CoinsEarned);
             yield return new WaitForSeconds(0.5f);
        */

        if (GameConstants.TrophiesLevelList.Contains(GameConstants.GetLevelNumber()) && GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed())  //check if level is skipped
        {
            GameConstants.SetTrophyUnlockedNumber(GameConstants.GetTrophyUnlockedNumber() + 1);   //trophyunlock initial number starts from -1
            GameConstants.SetSeasonCompleted("Yes");
        }

        GamePlayManager.instance.UnlockNextLevel();

        MenuManager.Instance.levelSuccessTakeToLevelMenu();

    }

    public void EnableDisableFireball_AdButton(bool active)
    {
        StartCoroutine(DelayOnClickFireballButton(active));
    }
    IEnumerator DelayOnClickFireballButton(bool active)
    {
        if (active == true && GameConstants.GetLevelNumber() != 1)
        {
            yield return new WaitForSeconds(1f);
            Fireball_AdButton.SetActive(active);
        }
        else if (active == false)
        {
            Fireball_AdButton.SetActive(active);
        }
    }

    public void OnClickFireballButton()
    {
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.FireballExtra_GameplayButton_Clicked, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);

        GenericVariables.RewardAdIndex = 6;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    public void giveRewardFireball()
    {
        StartCoroutine(DelayGiveRewardFireball());
    }
    public IEnumerator DelayGiveRewardFireball()
    {
        Time.timeScale = 1;
        GenericVariables.RewardAdIndex = -1;
       // LevelFailManager.SkipLevelAdWatched = true;
        yield return new WaitForSeconds(1);
        /*     MenuManager.Instance.moveObject("coins", SkipLevelButton.gameObject);
             MenuManager.Instance.moveConsumable();
             yield return new WaitForSeconds(3.0f);
             GenericVariables.AddCoins(GameConstants.CoinsEarned);
             yield return new WaitForSeconds(0.5f);
        */
        GenericVariables.AddFire(1);
        //GamePlayManager.instance.UnlockNextLevel();
        UpdateFireballs();                              //fireballs count display

        //   MenuManager.Instance.levelSuccessTakeToLevelMenu();

    }

    public void UpdateKeyCollection()
    {
        for(int i=0; i < GameConstants.GetCollectedRewardKeys(); i++ )
        {
            KeysCollectedArray[i].SetActive(true);
        }
    }

    public void KeyCollectionHudReset()
    {
        for (int i = 0; i < KeysCollectedArray.Length; i++)
        {
            KeysCollectedArray[i].SetActive(false);
        }
    }

    #endregion

    #region Testing Buttons

    public void OnClickWinLevelTestButton()
    {
        TestingWinButtonPressedBool = true;
        GamePlayManager.instance.LevelComplete(GamePlayManager.instance.WaitGameWon);

    }
    public void OnClickLoseLevelTestButton()
    {
       // TestingLoseButtonPressedBool = true;
        GamePlayManager.instance.LevelFailed(GamePlayManager.instance.WaitGameLost);

    }

    #endregion

    private void OnDisable()
    {
        PluginCallBackManager.SkipLevelRewardAdCompleteInGameplay_ -= giveRewardSkipLevel;
        PluginCallBackManager.FireballRewardAdComplete_ -= giveRewardFireball;

    }
}
