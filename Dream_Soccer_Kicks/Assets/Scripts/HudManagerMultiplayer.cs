﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudManagerMultiplayer : MonoBehaviour
{
    public static HudManagerMultiplayer instance;
    [SerializeField] private GameObject[] GoalsPlayer;
    [SerializeField] private GameObject[] GoalsMissPlayer;
    [SerializeField] private GameObject[] GoalsAI;
    [SerializeField] private GameObject[] GoalsMissAI;
    [SerializeField] private GameObject MultiplayerHud;
    [SerializeField] private GameObject[] GKLeftRightButtons;
    [SerializeField] private GameObject Area_GK_Control;
    [SerializeField] private Text opponentName;
    [SerializeField] private Text PlayerScore;
    [SerializeField] private Text OpponentScore;
    [SerializeField] Image PlayerTimerSlider;
    [SerializeField] Image OpponentTimerSlider;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void EnableDisableMultiplayerHud(bool value)
    {
        MultiplayerHud.SetActive(value);
        if(value)
        {
            OpponentNameSetter();
        }
    }

    public void UpdateOpponentScore()
    {
        OpponentScore.text = GameConstants.MultiplayerOpponentScore.ToString();
    }
    public void UpdatePlayerScore()
    {
        PlayerScore.text = GameConstants.MultiplayerPlayerScore.ToString();
    }
    public void OpponentNameSetter()
    {
        opponentName.text = GameConstants.MultiplayerOpponentName;
    }
    public void EnableDisableGKLeftRightButtons(bool value)
    {
        /*foreach (GameObject obj in GKLeftRightButtons)
        {
             obj.SetActive(value);
         }*/
        Area_GK_Control.SetActive(value);
    }
    public void PlayerGoalMultiplayerUpdate(int index)                 //tournament goaling scoring and displaying in hud
    {
        GoalsPlayer[index].SetActive(true);
    }
    public void AIGoalMultiplayerUpdate(int index)                   //tournament goaling scoring and displaying in hud
    {
        GoalsAI[index].SetActive(true);

    }
    public void PlayerGoalMissMultiplayerUpdate(int index)                 //tournament goaling scoring and displaying in hud
    {
        GoalsMissPlayer[index].SetActive(true);
    }
    public void AIGoalMissMultiplayerUpdate(int index)                   //tournament goaling scoring and displaying in hud
    {
        GoalsMissAI[index].SetActive(true);

    }
    public void GoalMultiplayerReset()                             //tournament goaling scoring and displaying in hud reset
    {
        for (int i = 0; i < GoalsAI.Length; i++)
        {
            GoalsAI[i].SetActive(false);
            GoalsPlayer[i].SetActive(false);

            GoalsMissAI[i].SetActive(false);
            GoalsMissPlayer[i].SetActive(false);
        }

    }

    public void UpdatePlayerTimerSlider(float timer)
    {
        PlayerTimerSlider.fillAmount = timer / 10;
    }
    public void UpdateOpponentTimerSlider(float timer)
    {
        OpponentTimerSlider.fillAmount = timer / 10;
    }
}
