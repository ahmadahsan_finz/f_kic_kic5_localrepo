﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurdlesSetter : MonoBehaviour
{
	public static HurdlesSetter instance; 
	public Transform _poleLeft;
	public Transform _poleRight;
	public GameObject[] HurdleInstanceArray;

	public GameObject HurdleInstance;
	public GameObject HurdleInstance2;
	public GameObject HurdleInstance3;
	public GameObject HurdleInstance4;
	public GameObject MoveTowardsObjForDummy1;
	public GameObject MoveTowardsObjForDummy2;
	public GameObject ParentLevelHurdles;

	public GameObject CoinBonusObject;
	public GameObject WoodLogs;
	public GameObject WallTwoDef;
	public GameObject WallThreeDef;
	public GameObject RoadBlockA;
	public GameObject RoadBlockB;
	public GameObject Hoop;
	public GameObject GoalHoop;
	public GameObject DummyDefenderMoving;
	public GameObject DummyDefenderMoving2;
	public GameObject DummyDefenderStatic;
	public GameObject DummyDefenderRailStatic;
	public GameObject DummyDefenderRailMoving1;
	public GameObject DummyDefenderRailMoving2;
	public GameObject[] PillarSimpleVariations;
	public GameObject ConesVariation1;
	public GameObject WoodenBarrelVariation1;
	public GameObject MoveTowards;
	public GameObject DartboardMoving;
	public GameObject TrampolineObj;

	public GameObject[] BrickVariations;

	public GameObject[] Circles1;
	public GameObject[] Circles2;
	public GameObject[] ConesVariations;
	public GameObject[] CoinsVariations;
	public GameObject[] DummyRailMovingVariation;

	float OrginalX;
	float ModifiedX;
	float OrginalZ;
	float ModifiedZ;

	public GameObject OrginalInterectionCalculated;
	// Start is called before the first frame update
	private void Awake()
    {
        if(instance==null)
        {
			instance = this;
        }
        else
        {
			Destroy(gameObject);
        }
    }

	/*  mode 1= penalties																						//ahmad
		  mode 2= simple freekicks
		  mode 3= freekicks with dartboard only/bullseye
		  mode 4= freekicks with wall
		  mode 5= freekicks with wall  and dartboard/bullseye
		  mode 6= freekicks with boxes
		  mode 7= Goal Spot
		  mode 8= Hit the pole 
		  mode 9= freekicks with hoops/2x 3x circles
		  mode 10= Tournament
		  mode 11= freekicks with wall and hoops/2x 3x circles
		  mode 12- Freekicks with dummy wall
		  mode 13= freekicks with dummy wall and bullseye
		  mode 14= freekicks with wood logs
		  mode 15: freekicks with RoadblockA
		  mode 16: freekicks with RoadblockB and moving defender
		  mode 17: freekicks with Hoops
		  mode 18: freekicks with Drums
		  mode 19: freekicks with two Moving Defender
		  mode 20: freekicks with pillars in goal and moving dummy no GK
		  mode 21: Freekick directional pillars  and GK
		  mode 22: Cones and wooden barrels
		  mode 23: Freekick with One Static Dummy
		  mode 24: Freekick with One Static Dummy and one moving dummy
		  mode 25: Freekick with pillars in goal no GK
		  mode 26: Freekick with directional pillars and no GK
		  mode 27: Freekick with multiple Dartboards
		  mode 28: Freekick with single Dartboard and moving dummy
		  mode 29: Freekick with single Goal through Circle  No GK
		  mode 30: Freekick with Single Goal through Circle with GK
		  mode 31: Freekick with Single Goal through Circle with GK + Moving dummy
		  mode 32: Freekick with two Goal through Circle No GK
		  mode 33: Freekick with two Goal through Circle + GK 
		  mode 34: Freekick with two Goal through Circle + GK + Moving Dummy 
		  mode 35: Freekicks with cones variation 
		  mode 36: Freekicks with cones variation and moving dummy
		  mode 37: Freekicks with cones variation + GK
		  mode 38: Freekicks with cones variation and moving dummy + GK
		  mode 39: Freekicks with two Human Defenders + GK
		  mode 40: Freekicks with Pillars Simple + No GK
		  mode 41: Freekicks with DummyDef and Dartboard + No GK
		  mode 42: Freekicks with Moving dartboard + GK (BOSS)
		  mode 43: Freekicks with Coins + No GK (BONUS LEVEL) (Not Using)
		  mode 44: Freekicks with Coins + No GK (BONUS LEVEL)
		  mode 45: Freekicks with Trampoline + No GK (BOSS)
		  mode 46: Freekicks with Two Moving Rail Defender + GK (BOSS)
		mode 100: Lucky Shots with DartBoard	

	  */
	public void SetHurdles(Vector3 ballPosition)					// this will set hurdles according to angle and range
	{
		HurdleInstanceDestroyer();
		Vector3 pos = -ballPosition;
		pos.Normalize();
		float angleRadian = Mathf.Atan2(pos.z, pos.x);      // tinh' goc' lech
		float angle = 90 - angleRadian * Mathf.Rad2Deg;     // angle in degree
		float angleAbs = Mathf.Abs(angle);

		int count;      // bao nhieu nguoi dung hang rao

		if (angleAbs < 10)
		{
			count = 6;
		}
		else if (angleAbs < 20)
		{
			count = 5;
		}
		else if (angleAbs < 30)
		{
			count = 4;
		}
		else if (angleAbs < 40)
		{
			count = 3;
		}
		else
		{
			count = 2;
		}



		float z = ballPosition.z + 11f;     // z cua hang rao, hang rao se dat o~ vi tri' z, * 0.55f co' nghia la hang rao se gan trai banh hon la gan khung thanh
		float x = ballPosition.x;


		

		//AHMAD
		//BEST for multiple hurdles/Moving hurdles bcz we would like them to place in center of goal
		//place hurdles not according to the angles, so now intersection point will be the center of goal straight to ball position and Gk standing position 


		Vector3 lineVector;

		if ( GameConstants.GetMode() == 16   || GameConstants.GetMode() == 17 || GameConstants.GetMode() == 22 || GameConstants.GetMode() == 35 || GameConstants.GetMode() == 36 || GameConstants.GetMode() == 37 || GameConstants.GetMode() == 38 )          //if you want no angle dependency
		{

			 lineVector = (ballPosition).normalized;       //  vector chi~ phuong tu` trai' banh den' cot doc
        }
        else    //For single hurdles which has to placed according to angles covering one side of the goal //pole position will be used in it 
        {
			Vector3 posPole;
			if (angle > 0)
			{  // hang rao` se~ lay' cot doc ben trai lam chuan, nhin` tu` banh den' goal
				posPole = _poleLeft.position;
			}
			else
			{           // hang rao` se~ lay' cot doc ben fai lam chuan,  nhin` tu` banh den' goal
				posPole = _poleRight.position;
			}

			 lineVector = (posPole - ballPosition).normalized;       //  vector chi~ phuong tu` trai' banh den' cot doc	
        }
		
		Vector3 intersection;       // giao diem cua~ duong thang tu trai banh den' cot doc cat' mat phang co' z = z vua tim duoc o~ tren, cau thu thu' 2 se dat o giao diem nay
		Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

		//intersection.y = 0.5f;
		intersection.y = 0;
		OrginalInterectionCalculated.transform.position = intersection;
		OrginalInterectionCalculated.transform.LookAt(-getPos());
		float sign = Mathf.Sign(angle);

		//intersection.x -= (sign * 0.5f);        // cau thu dau` tien se~ bi dat lech 0.5f, tuy luc' do' la ben trai' hay fai ma` cau thu se bi lech trai hay lech fai~

		//HurldeInstantiator();
		HurdleInstanceDestroyer2();


		if (GameConstants.GetMode() == 14)                  //wood logs
		{
			HurdleInstance = Instantiate(WoodLogs);
			intersection.z += 2f;           //making it closer to goal post
			HurdleInstance.transform.position = intersection;
			//	HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
		}
		else if (GameConstants.GetMode() == 15)             //road block A
		{
			HurdleInstance = Instantiate(RoadBlockA);
			HurdleInstance2 = Instantiate(RoadBlockA);

			//intersection.z -= 2f;
			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			intersection.x -= (sign * 0.5f);
			HurdleInstance.transform.position = intersection;
			intersection.x -= (sign * 3f);
			HurdleInstance2.transform.position = intersection;

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
		}
		else if (GameConstants.GetMode() == 16)             //road block B	with moving dummy
		{
			HurdleInstance = Instantiate(RoadBlockB);
			HurdleInstance2 = Instantiate(RoadBlockB);
			HurdleInstance3 = Instantiate(RoadBlockB);
			HurdleInstance4 = Instantiate(DummyDefenderMoving);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

			//	intersection.z -= 5f;
			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			HurdleInstance3.transform.LookAt(-getPos());
			HurdleInstance4.transform.LookAt(-getPos());
			MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			//intersection.x -= (sign * 0.5f);
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			 OrginalZ = intersection.z - 5f;
			 ModifiedZ = OrginalZ;
			intersection.x = ModifiedX;   //- (sign * 0.5f);
			intersection.z = ModifiedZ;   //- (sign * 0.5f);
			HurdleInstance.transform.localPosition = intersection;
			ModifiedX = OrginalX;
			intersection.x = ModifiedX - 3f;
			//intersection.x -= (sign * 3f);
			//intersection.z -= (sign * 3f);
			intersection.z = ModifiedZ + 3f;
			HurdleInstance2.transform.localPosition = intersection;
			ModifiedX = OrginalX;

			intersection.x = ModifiedX + 3f;
			ModifiedZ = OrginalZ;
			//intersection.x -= (sign * 3f);
			intersection.z = ModifiedZ + 3f;
			//intersection.z -= (sign * 3f);
			HurdleInstance3.transform.localPosition = intersection;

			ModifiedX = OrginalX;

			intersection.x = ModifiedX -1f;
			ModifiedZ = OrginalZ;
			//intersection.x -= (sign * 3f);
			intersection.z = ModifiedZ + 6f;
			//intersection.z -= (sign * 3f);
			HurdleInstance4.transform.localPosition = intersection;
			ModifiedX = OrginalX;
			
			if (angle > 0)
			{
				intersection.x = ModifiedX + 3f;

			}
			else
			{

				intersection.x = ModifiedX - 3f;
			}
			//intersection.x -= (sign * 3f);
			ModifiedZ = OrginalZ;
			intersection.z = ModifiedZ + 6f;
			//intersection.z -= (sign * 3f);
			MoveTowardsObjForDummy1.transform.localPosition = intersection;

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance3.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance4.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
			HurdleInstance3.SetActive(true);
			HurdleInstance4.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);
			iTween.MoveTo(HurdleInstance4, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

		}
		else if (GameConstants.GetMode() == 11 || GameConstants.GetMode() == 9)             //Circle 2x 3x etc
		{
			intersection.y = 2.5f;
			int rand1 = Random.Range(0, 3);
			int rand2 = Random.Range(0, 3);
			HurdleInstance = Instantiate(Circles1[rand1]);         //CIRCLES INSTANTIATION
			HurdleInstance.transform.LookAt(-getPos());

			HurdleInstance2 = Instantiate(Circles2[rand2]);
			HurdleInstance2.transform.LookAt(-getPos());

			Vector3 poleRightPos = _poleRight.position;
			poleRightPos.x += 1;
			poleRightPos.y += 0.5f;
			poleRightPos.z -= 1.5f;

			HurdleInstance.transform.position = poleRightPos;
			
			Vector3 poleLeftPos = _poleLeft.position;
			poleLeftPos.x -= 1;
			poleLeftPos.y += 0.5f;
			poleLeftPos.z -= 1.5f;
			HurdleInstance2.transform.position = poleLeftPos;

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
		}
		else if (GameConstants.GetMode() == 17)		//hoops circles
		{
			HurdleInstance = Instantiate(Hoop);
			HurdleInstance2 = Instantiate(Hoop);

			//intersection.z -= 2f;
			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			intersection.x -= (sign * 0.5f);
			HurdleInstance.transform.position = intersection;
			intersection.z += 4f;
			intersection.x -= (sign * 1f);
			HurdleInstance2.transform.position = intersection;

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
		}
		else if (GameConstants.GetMode() == 19)				//two moving dummy
		{
			HurdleInstance = Instantiate(DummyDefenderMoving);
			HurdleInstance2 = Instantiate(DummyDefenderMoving2);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);
			MoveTowardsObjForDummy2 = Instantiate(MoveTowards);

			z = ballPosition.z + 7f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			
			/*OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			
			OrginalX = intersection.x;
			 ModifiedX = OrginalX;

			intersection.x= ModifiedX;
			if (angle > 0)
			{
				intersection.x = ModifiedX -3f;

			}
			else
			{

				intersection.x = ModifiedX +3f;
			}

			HurdleInstance.transform.position = intersection;
		
			ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX-1.4f;

			}
			else
			{

				intersection.x = ModifiedX+ 1.4f;
			}
			MoveTowardsObjForDummy1.transform.position = intersection;          //move towards position

			z = ballPosition.z + 12f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));
			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			//intersection.z += 4f;
			intersection.y = 0;

			if (angle > 0)
			{
				intersection.x = ModifiedX;

			}
			else
			{

				intersection.x = ModifiedX;
			}

			HurdleInstance2.transform.position = intersection;

			ModifiedX = OrginalX;
			intersection.x = ModifiedX;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 1.6f;

			}
			else
			{

				intersection.x = ModifiedX	+ 1.6f;
			}
			MoveTowardsObjForDummy2.transform.position = intersection;			//move towards position

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			MoveTowardsObjForDummy2.transform.LookAt(-getPos());

            HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);
			MoveTowardsObjForDummy2.SetActive(true);
			
			iTween.MoveTo(HurdleInstance, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 2f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

			iTween.MoveTo(HurdleInstance2, iTween.Hash("position", MoveTowardsObjForDummy2.transform.position, "time", 2f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));
		
		}
		else if (GameConstants.GetMode() == 20 )                //moving dummy with pillars
		{
			HurdleInstance = Instantiate(DummyDefenderMoving);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);
		

			HurdleInstance.transform.LookAt(-getPos());
		
			
			//HurdleInstance.transform.position = intersection;
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			ModifiedX = OrginalX;
		//	intersection.z += 4f;

			intersection.x = ModifiedX;
			if (angle > 0)
			{
				intersection.x = ModifiedX;

			}
			else
			{

				intersection.x = ModifiedX;
			}
			HurdleInstance.transform.position = intersection;

			ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 4f;

			}
			else
			{

				intersection.x = ModifiedX + 4f;
			}

			MoveTowardsObjForDummy1.transform.position = intersection;          //move towards position
			
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;
		
			HurdleInstance.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);

			iTween.MoveTo(HurdleInstance, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));


		}
		else if (GameConstants.GetMode() == 22)             //Cones and Wooden Barrels
		{
			HurdleInstance = Instantiate(ConesVariation1);
			HurdleInstance2 = Instantiate(WoodenBarrelVariation1);

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;

			intersection.x = ModifiedX -2f;
			HurdleInstance.transform.position = intersection;

			ModifiedX = OrginalX;
			intersection.z += 6f;
			intersection.x = ModifiedX + 2f;
			HurdleInstance2.transform.position = intersection;

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
		}
		else if (GameConstants.GetMode() == 23 || GameConstants.GetMode() == 41)             //One Static dummy
		{
			HurdleInstance = Instantiate(DummyDefenderRailStatic);
	//		MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

			//	MoveTowardsObjForDummy1.transform.LookAt(-getPos());

			z = ballPosition.z + 6.5f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
		/*	OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());
*/
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			intersection.x = ModifiedX ;
			//intersection.z -= 7.5f;
			if (angle > 0)
			{
				intersection.x = ModifiedX -1.5f;

			}
			else
			{

				intersection.x = ModifiedX + 1.5f;
			}
			HurdleInstance.transform.position = intersection;

			HurdleInstance.transform.LookAt(-getPos());
		/*	ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 4f;

			}
			else
			{

				intersection.x = ModifiedX + 4f;
			}
*/
		//	MoveTowardsObjForDummy1.transform.position = intersection;          //move towards position

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
		//	MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);
	//		MoveTowardsObjForDummy1.SetActive(true);

		//	iTween.MoveTo(HurdleInstance, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));
			
			
		}
        else if (GameConstants.GetMode() == 24)             //One Static dummy and one moving defender
		{
			HurdleInstance = Instantiate(DummyDefenderRailStatic);
			HurdleInstance2 = Instantiate(DummyDefenderMoving2);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			z = ballPosition.z + 4f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			/*OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			 OrginalZ = intersection.z;
			 ModifiedZ = OrginalZ;
		//	intersection.x = ModifiedX;
		//	intersection.z = ModifiedZ - 7.5f;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 1.3f;
				//intersection.x = ModifiedX - 2.8f;
			}
			else
			{
				intersection.x = ModifiedX + 1.3f;

		//		intersection.x = ModifiedX + 2.8f;
			}
			HurdleInstance.transform.position = intersection;

			ModifiedX = OrginalX;
			ModifiedZ = OrginalZ;
			intersection.z = ModifiedZ +7f;
			if (angle > 0)
			{
				intersection.x = ModifiedX+2f;
			}
			else
			{
				intersection.x = ModifiedX - 2f;
			}
			//intersection.x = ModifiedX;
			HurdleInstance2.transform.position = intersection;
		
			ModifiedX = OrginalX;
			intersection.x = ModifiedX  ;
/*			if(angle>0)
            {
			//intersection.x = ModifiedX +2f ;
			intersection.x = ModifiedX  ;

            }
            else
            {

			//intersection.x = ModifiedX -2f ;
			intersection.x = ModifiedX ;
            }*/
			MoveTowardsObjForDummy1.transform.position = intersection;		//giving move towards a position according to angle

			
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);

			iTween.MoveTo(HurdleInstance2, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

		}
		else if (GameConstants.GetMode() == 28)             //moving dummy with dartboard
		{
			HurdleInstance = Instantiate(DummyDefenderMoving);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);


			HurdleInstance.transform.LookAt(-getPos());


			//HurdleInstance.transform.position = intersection;
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			ModifiedX = OrginalX;
			//	intersection.z += 4f;

			intersection.x = ModifiedX;
			if (angle > 0)
			{
				intersection.x = ModifiedX;

			}
			else
			{

				intersection.x = ModifiedX;
			}
			HurdleInstance.transform.position = intersection;

			ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 4f;

			}
			else
			{

				intersection.x = ModifiedX + 4f;
			}

			MoveTowardsObjForDummy1.transform.position = intersection;          //move towards position

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);

			iTween.MoveTo(HurdleInstance, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));


		}
		else if (GameConstants.GetMode() == 29 || GameConstants.GetMode() == 30)     //single goal through circles
		{
			

			HurdleInstance = Instantiate(GoalHoop);
			
			z = ballPosition.z + 10f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			/*			OrginalInterectionCalculated.transform.position = intersection;
						OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			sign = Mathf.Sign(angle);
			//	intersection.x = ModifiedX; //-= (sign * 0.5f);

			//intersection.z -= 5f;
			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
				intersection.x = ModifiedX - 3f;


			}
			else
			{

				intersection.x = ModifiedX +3f;


			}

			HurdleInstance.transform.position = intersection;
			HurdleInstance.transform.LookAt(-getPos());

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);
		}
		else if (GameConstants.GetMode() == 31)             //One goal through circle and one moving defender
		{
			HurdleInstance = Instantiate(GoalHoop);
			HurdleInstance2 = Instantiate(DummyDefenderMoving2);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);
		//	HurdleInstance = Instantiate(GoalHoop);

			z = ballPosition.z + 10f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			/*			OrginalInterectionCalculated.transform.position = intersection;
						OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;

			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
				intersection.x = ModifiedX - 0.5f;


			}
			else
			{

				intersection.x = ModifiedX + 0.5f;


			}
			HurdleInstance.transform.position = intersection;
			z = z + 3f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			OrginalX = intersection.x;
			ModifiedX = OrginalX;
		//	intersection.z = z+ 3f;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 2f;
			}
			else
			{
				intersection.x = ModifiedX + 2f;
			}
			//intersection.x = ModifiedX;
			HurdleInstance2.transform.position = intersection;

			ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX + 0.5f;

			}
			else
			{

				intersection.x = ModifiedX - 0.5f;
			}
			MoveTowardsObjForDummy1.transform.position = intersection;      //giving move towards a position according to angle

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			MoveTowardsObjForDummy1.transform.LookAt(-getPos());

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);

			iTween.MoveTo(HurdleInstance2, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 2f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

		}
		else if (GameConstants.GetMode() == 32 || GameConstants.GetMode() == 33)             // goal through two circle 
		{
			HurdleInstance = Instantiate(GoalHoop);
			HurdleInstance2 = Instantiate(GoalHoop);

			z = ballPosition.z + 10f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			/*			OrginalInterectionCalculated.transform.position = intersection;
						OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			OrginalX = intersection.x;
			 ModifiedX = OrginalX;

			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
				intersection.x = ModifiedX - 0.5f;


			}
			else
			{

				intersection.x = ModifiedX + 0.5f;


			}
			HurdleInstance.transform.position = intersection;

			ModifiedX = OrginalX;
			intersection.z = z+ 3f;
			if (angle > 0)
			{
				intersection.x = ModifiedX + 0.2f;
			}
			else
			{
				intersection.x = ModifiedX - 0.2f;
			}
			HurdleInstance2.transform.position = intersection;

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
	
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);

		}
		else if (GameConstants.GetMode() == 34)             // goal through two circle and one moving defender
		{
			HurdleInstance = Instantiate(GoalHoop);
			HurdleInstance2 = Instantiate(GoalHoop);

			HurdleInstance3 = Instantiate(DummyDefenderMoving2);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance2.transform.LookAt(-getPos());
			HurdleInstance3.transform.LookAt(-getPos());
			MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			 OrginalZ = intersection.z;
			 ModifiedZ = OrginalZ;
			intersection.x = ModifiedX;
			HurdleInstance.transform.position = intersection;
		
			ModifiedX = OrginalX;
			ModifiedZ += 3f;
			intersection.z = ModifiedZ;
			if (angle > 0)
			{
				intersection.x = ModifiedX+0.5f ;
			}
			else
			{
				intersection.x = ModifiedX- 0.5f;
			}
			//intersection.x = ModifiedX;
			HurdleInstance2.transform.position = intersection;

			ModifiedX = OrginalX;
			ModifiedZ = OrginalZ;
			ModifiedZ += 4f;
			intersection.z = ModifiedZ;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 3f;
			}
			else
			{
				intersection.x = ModifiedX + 3f;
			}
			//intersection.x = ModifiedX;
			HurdleInstance3.transform.position = intersection;

			ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX + 3f;

			}
			else
			{

				intersection.x = ModifiedX - 3f;
			}
			MoveTowardsObjForDummy1.transform.position = intersection;      //giving move towards a position according to angle


			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance3.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);
			HurdleInstance3.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);

			iTween.MoveTo(HurdleInstance3, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

		}
		else if (GameConstants.GetMode() == 35 || GameConstants.GetMode() == 37)             // goal through cones 
		{
			int rand = Random.Range(0, 9);
			HurdleInstance = Instantiate(ConesVariations[rand]);
			//HurdleInstance2 = Instantiate(GoalHoop);

			HurdleInstance.transform.LookAt(-getPos());
		//	HurdleInstance2.transform.LookAt(-getPos());

			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;

			intersection.x = ModifiedX;
			HurdleInstance.transform.position = intersection;

		//	ModifiedX = OrginalX;
		//	intersection.z += 3f;
			/*if (angle > 0)
			{
				intersection.x = ModifiedX - 0.5f;
			}
			else
			{
				intersection.x = ModifiedX + 0.5f;
			}*/
			//intersection.x = ModifiedX;
			//HurdleInstance2.transform.position = intersection;

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			//HurdleInstance2.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);
			//HurdleInstance2.SetActive(true);


		}
		else if (GameConstants.GetMode() == 36 || GameConstants.GetMode() == 38)             // cones variations and one moving dummy
		{
			int rand = Random.Range(0, 9);
			HurdleInstance = Instantiate(ConesVariations[rand]);
			//HurdleInstance = Instantiate(GoalHoop);
			//HurdleInstance2 = Instantiate(GoalHoop);

			HurdleInstance3 = Instantiate(DummyDefenderMoving2);
			MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

			HurdleInstance.transform.LookAt(-getPos());
		//	HurdleInstance2.transform.LookAt(-getPos());
			HurdleInstance3.transform.LookAt(-getPos());
			MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			 OrginalZ = intersection.z;
			 ModifiedZ = OrginalZ;
			intersection.x = ModifiedX;
			HurdleInstance.transform.position = intersection;

		/*	ModifiedX = OrginalX;
			ModifiedZ += 3f;
			intersection.z = ModifiedZ;
			if (angle > 0)
			{
				intersection.x = ModifiedX + 0.5f;
			}
			else
			{
				intersection.x = ModifiedX - 0.5f;
			}*/
			//intersection.x = ModifiedX;
		//	HurdleInstance2.transform.position = intersection;

			ModifiedX = OrginalX;
			ModifiedZ = OrginalZ;
			ModifiedZ += 4f;
			intersection.z = ModifiedZ;
			if (angle > 0)
			{
				intersection.x = ModifiedX - 3f;
			}
			else
			{
				intersection.x = ModifiedX + 3f;
			}
			//intersection.x = ModifiedX;
			HurdleInstance3.transform.position = intersection;

			ModifiedX = OrginalX;
			if (angle > 0)
			{
				intersection.x = ModifiedX + 3f;

			}
			else
			{

				intersection.x = ModifiedX - 3f;
			}
			MoveTowardsObjForDummy1.transform.position = intersection;      //giving move towards a position according to angle


			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			//HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance3.transform.parent = ParentLevelHurdles.transform;
			MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
		//	HurdleInstance2.SetActive(true);
			HurdleInstance3.SetActive(true);
			MoveTowardsObjForDummy1.SetActive(true);

			iTween.MoveTo(HurdleInstance3, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

		}
		else if (GameConstants.GetMode() == 39)             //Two Defender wall 
		{
			HurdleInstance = Instantiate(WallTwoDef);
			//		MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

			//	MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			z = ballPosition.z + 5f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
/*			OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			 OrginalX = intersection.x;
			 ModifiedX = OrginalX;
			sign = Mathf.Sign(angle);
		//	intersection.x = ModifiedX; //-= (sign * 0.5f);
		
			//intersection.z -= 5f;
			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
					intersection.x = ModifiedX - 1.5f;
				

			}
			else
			{

					intersection.x = ModifiedX + 1.5f;
				

			}

			HurdleInstance.transform.position = intersection;
			HurdleInstance.transform.LookAt(-getPos());

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);
		
		}
		else if (GameConstants.GetMode() == 40)             //Pillars Simple + No GK
		{
			int rand = Random.Range(0, PillarSimpleVariations.Length);
			HurdleInstance = Instantiate(PillarSimpleVariations[rand]);

			z = ballPosition.z + 7.5f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

	
			intersection.y = 0;

			OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());

			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			sign = Mathf.Sign(angle);
			//	intersection.x = ModifiedX; //-= (sign * 0.5f);

			//intersection.z -= 5f;
			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
				intersection.x = ModifiedX - 0.5f;


			}
			else
			{

				intersection.x = ModifiedX + 0.5f;


			}

			HurdleInstance.transform.position = intersection;

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);

		}
		else if (GameConstants.GetMode() == 43)             // goal with coins Bonus Level (not used)
		{	
			z = ballPosition.z + 7f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 1;
			/*			OrginalInterectionCalculated.transform.position = intersection;
						OrginalInterectionCalculated.transform.LookAt(-getPos());*/

			OrginalX = intersection.x;
			ModifiedX = OrginalX;

			OrginalZ = z;
			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
				intersection.x = ModifiedX - 0.5f;

			}
			else
			{

				intersection.x = ModifiedX + 0.5f;

			}
			HurdleInstanceArray = new GameObject[30];
			for(int i=0;i< HurdleInstanceArray.Length; i++)
            {
				HurdleInstanceArray[i]=Instantiate(CoinBonusObject);
				intersection.z = z + 0.5f;
				z = intersection.z;
				if( i==10)
                {
					z = OrginalZ;
						ModifiedX = OrginalX;
					if (angle > 0)
					{
						//intersection.x -= (sign * 0.5f);
						intersection.x = ModifiedX - 2f;


					}
					else
					{

						intersection.x = ModifiedX + 2f;
					}
				}
				else if (i == 20)
				{
					z = OrginalZ;
					ModifiedX = OrginalX;
					if (angle > 0)
					{
						//intersection.x -= (sign * 0.5f);
						intersection.x = ModifiedX + 1f;


					}
					else
					{

						intersection.x = ModifiedX - 1f;
					}
				}
				HurdleInstanceArray[i].transform.position = intersection;
				HurdleInstanceArray[i].transform.parent = ParentLevelHurdles.transform;
				HurdleInstanceArray[i].transform.LookAt(-getPos());
				HurdleInstanceArray[i].SetActive(true);
			}


			/*ModifiedX = OrginalX;
			intersection.z = z + 3f;
			if (angle > 0)
			{
				intersection.x = ModifiedX + 0.2f;
			}
			else
			{
				intersection.x = ModifiedX - 0.2f;
			}
			HurdleInstance2.transform.position = intersection;

			HurdleInstance.transform.LookAt(-getPos());*/
		/*	HurdleInstance2.transform.LookAt(-getPos());
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
*/
		/*	HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);*/

		}
		else if (GameConstants.GetMode() == 44)             //CoinsVariations + No GK Bonus Level
		{
			int rand = Random.Range(0, CoinsVariations.Length);
			HurdleInstance = Instantiate(CoinsVariations[rand]);

			lineVector = (ballPosition).normalized;
			z = ballPosition.z + 8.5f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));


			intersection.y = 0;

			/*OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());*/

			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			sign = Mathf.Sign(angle);
			//	intersection.x = ModifiedX; //-= (sign * 0.5f);

			//intersection.z -= 5f;
			if (angle > 0)
			{
				//intersection.x -= (sign * 0.5f);
				intersection.x = ModifiedX ;


			}
			else
			{

				intersection.x = ModifiedX ;


			}

			HurdleInstance.transform.position = intersection;

			HurdleInstance.transform.LookAt(-getPos());
			HurdleInstance.transform.parent = ParentLevelHurdles.transform;

			HurdleInstance.SetActive(true);

		}
		else if (GameConstants.GetMode() == 45)             // Trampoline freekicks
		{
			HurdleInstance = Instantiate(TrampolineObj);
		//	HurdleInstance2 = Instantiate(BrickVariations[0]);
		//	HurdleInstance2 = Instantiate(WallThreeDef);

			z = ballPosition.z + 17f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;

			/*OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());*/

			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			
			intersection.x = ModifiedX;
			if (angle > 0)
			{
				
				intersection.x = ModifiedX + 2f;

			}
			else
			{
				
				intersection.x = ModifiedX - 2f;
			}

			HurdleInstance.transform.position = intersection;


			z = ballPosition.z + 6f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));
			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			//intersection.z += 4f;
			intersection.y = 0f;
		/*	OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());*/
			if (angle > 0)
			{
				intersection.x = ModifiedX-0.75f;

			}
			else
			{

				intersection.x = ModifiedX+ 0.75f;
			}

			//HurdleInstance2.transform.position = intersection;

			z = ballPosition.z + 16.5f	;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));
			intersection.y = 0f;

			HurdleInstance.transform.LookAt(intersection);
		//	HurdleInstance2.transform.LookAt(-getPos());
		

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			//HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
		//	HurdleInstance2.SetActive(true);
		
		}
	/*	else if (GameConstants.GetMode() == 46)              //two moving Rail dummy
		{
			int rand = Random.Range(0, 4);
			HurdleInstance = Instantiate(DummyRailMovingVariation[rand]);
			
			z = ballPosition.z + 4f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;
			*//*	OrginalInterectionCalculated.transform.position = intersection;
				OrginalInterectionCalculated.transform.LookAt(-getPos());
	*//*
			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			intersection.x = ModifiedX;
			//intersection.z -= 7.5f;
			if (angle > 0)
			{
				intersection.x = ModifiedX-0.5f ;

			}
			else
			{

				intersection.x = ModifiedX + 0.5f;
			}
			HurdleInstance.transform.position = intersection;

			HurdleInstance.transform.LookAt(-getPos());
	

			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
		

			HurdleInstance.SetActive(true);
		

		}*/
			else if (GameConstants.GetMode() == 46)             //two moving Rail dummy		(not using)
			{
				HurdleInstance = Instantiate(DummyDefenderRailMoving1);
				HurdleInstance2 = Instantiate(DummyDefenderRailMoving2);
		/*		MoveTowardsObjForDummy1 = Instantiate(MoveTowards);
				MoveTowardsObjForDummy2 = Instantiate(MoveTowards);*/

				z = ballPosition.z + 6f;
				Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));

				//intersection.y = 0.5f;
				intersection.y = 0;

				/*OrginalInterectionCalculated.transform.position = intersection;
				OrginalInterectionCalculated.transform.LookAt(-getPos());*/

				OrginalX = intersection.x;
				ModifiedX = OrginalX;

		//		intersection.x = ModifiedX;
				if (angle > 0)
				{
					intersection.x = ModifiedX-0.1f;

				}
				else
				{

					intersection.x = ModifiedX+ 0.1f;
				}

				HurdleInstance.transform.position = intersection;

			//	ModifiedX = OrginalX;
/*				if (angle > 0)
				{
					intersection.x = ModifiedX - 1f;

				}
				else
				{

					intersection.x = ModifiedX + 1f;
				}
				MoveTowardsObjForDummy1.transform.position = intersection;          //move towards position*/

				z = ballPosition.z + 11f;
				Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(0, 0, z));
				OrginalX = intersection.x;
				ModifiedX = OrginalX;
				//intersection.z += 4f;
				intersection.y = 0;

				if (angle > 0)
				{
					intersection.x = ModifiedX - 3f; 
		
				}
				else
				{

					intersection.x = ModifiedX+ 3f;
				}

				HurdleInstance2.transform.position = intersection;

				//ModifiedX = OrginalX;
			//	intersection.x = ModifiedX;
/*				if (angle > 0)
				{
					intersection.x = ModifiedX - 1.6f;

				}
				else
				{

					intersection.x = ModifiedX + 1.6f;
				}
				MoveTowardsObjForDummy2.transform.position = intersection;          //move towards position*/

				HurdleInstance.transform.LookAt(-getPos());
				HurdleInstance2.transform.LookAt(-getPos());
//MoveTowardsObjForDummy1.transform.LookAt(-getPos());
			//	MoveTowardsObjForDummy2.transform.LookAt(-getPos());

				HurdleInstance.transform.parent = ParentLevelHurdles.transform;
				HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
				HurdleInstance.SetActive(true);
				HurdleInstance2.SetActive(true);
			//	MoveTowardsObjForDummy1.SetActive(true);
			//	MoveTowardsObjForDummy2.SetActive(true);

		//		iTween.MoveTo(HurdleInstance.transform.GetChild(0).gameObject, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 2f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

			//	iTween.MoveTo(HurdleInstance2.transform.GetChild(0).gameObject, iTween.Hash("position", MoveTowardsObjForDummy2.transform.position, "time", 2f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));

		}
		
		else if (GameConstants.GetMode() == 47)             // Trampoline freekicks with defender
		{
			HurdleInstance = Instantiate(TrampolineObj);
			//	HurdleInstance2 = Instantiate(BrickVariations[0]);
			HurdleInstance2 = Instantiate(WallThreeDef);

			z = ballPosition.z + 17f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));

			//intersection.y = 0.5f;
			intersection.y = 0;

			OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());

			OrginalX = intersection.x;
			ModifiedX = OrginalX;

			intersection.x = ModifiedX;
			if (angle > 0)
			{

				intersection.x = ModifiedX + 2f;

			}
			else
			{

				intersection.x = ModifiedX - 2f;
			}

			HurdleInstance.transform.position = intersection;


			z = ballPosition.z + 6f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));
			OrginalX = intersection.x;
			ModifiedX = OrginalX;
			//intersection.z += 4f;
			intersection.y = 0f;
			OrginalInterectionCalculated.transform.position = intersection;
			OrginalInterectionCalculated.transform.LookAt(-getPos());
			if (angle > 0)
			{
				intersection.x = ModifiedX - 0.75f;

			}
			else
			{

				intersection.x = ModifiedX + 0.75f;
			}

			HurdleInstance2.transform.position = intersection;

			z = ballPosition.z + 16.5f;
			Math3d.LinePlaneIntersection(out intersection, ballPosition, lineVector, Vector3.forward, new Vector3(x, 0, z));
			intersection.y = 0f;

			HurdleInstance.transform.LookAt(intersection);
			HurdleInstance2.transform.LookAt(-getPos());


			HurdleInstance.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance2.transform.parent = ParentLevelHurdles.transform;
			HurdleInstance.SetActive(true);
			HurdleInstance2.SetActive(true);

		}


		/*
			 else if (GameConstants.GetMode() == 23)             //One Moving dummy
            {
                HurdleInstance = Instantiate(DummyDefenderRailStatic);
                MoveTowardsObjForDummy1 = Instantiate(MoveTowards);

                HurdleInstance.transform.LookAt(-getPos());
                MoveTowardsObjForDummy1.transform.LookAt(-getPos());
                float OrginalX = intersection.x;
                float ModifiedX = OrginalX;
                intersection.x = ModifiedX;

                if (angle > 0)
                {
                    intersection.x = ModifiedX;

                }
                else
                {

                    intersection.x = ModifiedX;
                }
                HurdleInstance.transform.position = intersection;

                    ModifiedX = OrginalX;
                    if (angle > 0)
                    {
                        intersection.x = ModifiedX - 4f;

                    }
                    else
                    {

                        intersection.x = ModifiedX + 4f;
                    }

                    MoveTowardsObjForDummy1.transform.position = intersection;          //move towards position

                HurdleInstance.transform.parent = ParentLevelHurdles.transform;
                MoveTowardsObjForDummy1.transform.parent = ParentLevelHurdles.transform;

                HurdleInstance.SetActive(true);
                MoveTowardsObjForDummy1.SetActive(true);

                    iTween.MoveTo(HurdleInstance, iTween.Hash("position", MoveTowardsObjForDummy1.transform.position, "time", 3f, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.5f));


            }
		*/
	}
	public void HurldeInstantiator()
	{
		if (HurdleInstance != null)
		{
			//HurdleInstance.SetActive(false);
			Destroy(HurdleInstance);
		}

		HurdleInstance = Instantiate(WoodLogs);
		HurdleInstance.transform.parent = ParentLevelHurdles.transform;
		HurdleInstance.SetActive(true);
	}
	public Vector3 getPos()                         //ahmad
	{
		if (GameObject.FindGameObjectWithTag("Ball") != null)
		{
			Vector3 Pos = GameObject.FindGameObjectWithTag("Ball").transform.position;
			return Pos;
		}
		return Vector3.zero;
	}

	public void HurdleInstanceDestroyer2()
    {
		if (HurdleInstance != null)
		{
			Destroy(HurdleInstance);
		}
		if (HurdleInstance2 != null)
		{
			Destroy(HurdleInstance2);
		}
		if (HurdleInstance3 != null)
		{
			Destroy(HurdleInstance3);
		}
		if (HurdleInstance4 != null)
		{
			Destroy(HurdleInstance4);
		}
		if (MoveTowardsObjForDummy1 != null)
		{
			Destroy(MoveTowardsObjForDummy1);
		}
		if (MoveTowardsObjForDummy2 != null)
		{
			Destroy(MoveTowardsObjForDummy2);
		}
	}
	
	public void HurdleInstanceDestroyer()
	{
		for (int i = 0; i < HurdleInstanceArray.Length; i++)
		{
			if (HurdleInstanceArray[i] != null)
			{
				//HurdleInstance.SetActive(false);
				Destroy(HurdleInstanceArray[i]);
			}
		}
	}
}