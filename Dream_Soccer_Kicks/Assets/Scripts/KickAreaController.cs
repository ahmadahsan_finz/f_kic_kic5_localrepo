using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickAreaController : MonoBehaviour
{
    //put this gameobject with sphere collider inside your ball and adjust position
    public static KickAreaController instance;
    public BoxCollider KickAreaObject;
    private void Awake()
    {
      
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            //  Destroy(gameObject);
        }
    }
    private void Start()
    {
        Shoot.EventShoot += DisableKickArea;
    }
    //Enable sphereCollider of kick area (Area range from which player can shoot)
    public void EnableKickArea()
    {
        KickAreaObject.enabled = true;
     //   Shoot.EventShoot -= DisableKickArea;
    }

    //Disable kick area
    public void DisableKickArea()
    {
        KickAreaObject.enabled = false;
        Debug.Log("DisableKickArea CALLED");
    }

    private void OnDestroy()
    {
        Shoot.EventShoot -= DisableKickArea;

    }
}