using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LanguagePanelScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Button[] languageButton;
    public GameObject[] languageButtonTicks;

    private void OnEnable()
    {
        SelectedlanguageButtonTick();
    }

    public void SelectedlanguageButtonTick()
    {
        /*var colors = languageButton[GameConstants.GetlanguageIndex()-1].colors;
        colors.normalColor = colors.selectedColor;
        languageButton[GameConstants.GetlanguageIndex()-1].colors=colors;*/
        languageButtonTicks[GameConstants.GetlanguageIndex() - 1].SetActive(true);
    }
    public void OnSelectLanguage(int i)
    {

        for(int a=0;a< languageButtonTicks.Length;a++)
        {
            /*  var colors = languageButton[a].colors;
              colors.normalColor = colors.highlightedColor;
              languageButton[a].colors = colors;*/
            languageButtonTicks[a].SetActive(false);

        }
        GameConstants.SetlanguageIndex(i);
        languageButtonTicks[i-1].SetActive(true);
        if(SettingsPanelScript.instance)
        {
             SettingsPanelScript.instance.LanguageTextSetter();
        }
        
    }

   public void OnClickCloseButton()
    {
        this.gameObject.SetActive(false);
    }
}
