﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelCompleteMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public static LevelCompleteMenu Instance;
    public GameObject[] StarsImage;
    public Image ProgressImage;
    public Image RewardProgressImage;
    public Text CoinsText;
    public Text CoinsTextSubscriptionBonus;
    public Text [] LevelNumberTexts;
    public Button ClaimButton;
    public Button ClaimButtonForTutorialLevels;
    public Button ClaimButton2x;
    public Text ClaimButtonText;
    public GameObject CoinImage;
    public GameObject CoinsEarnedViaSubscriptionGameObject;

    [SerializeField] float ProgressRewardBy = 0.2f;
    int stars = 1;
    public GameObject RewardCostumeMenu;

    public static bool Watched2xRewardAd;

  //  [SerializeField] Sprite[] RewardItemsImagesList;
    [SerializeField] Image RewardItemImage;
    [SerializeField] Image RewardItemImageBase;
    public Image RewardItemImageWhite;
    [SerializeField] int RewardNumber;
    [SerializeField] int PreviousRewardNumber;
    [SerializeField] Text RewardProgressText;
    [SerializeField] GameObject RewardProgressPanel;


    int coinsEarned;
    int coinsEarnedviaSubscription;
    public Button RewardIconButton;
    private void Awake()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        // RewardIconButton.interactable = false;
        AdController.instance.HideBannerAd();
        AdController.instance.showNativeBannerAd();
     //   RewardItemImageWhite.gameObject.SetActive(true);

        RewardProgress(); //Game End reward progress
        SelectRewardToOffer();
        if (GameConstants.GetLevelNumber()==1|| GameConstants.GetLevelNumber() == 2)
        {
            //   ClaimButtonText.text = "Continue";
            ClaimButtonForTutorialLevels.gameObject.SetActive(true);
 

            ClaimButton.gameObject.SetActive(false);
            ClaimButton2x.gameObject.SetActive(false);
        }
        else if (RewardProgressImage.fillAmount == 1 && GameConstants.GetLevelNumber()==GenericVariables.getTotalLevelsPassed() && GameConstants.GetGameOverRewardNumber() != (GameConstants.TotalGameOverRewards+1) ) //if reward is available and level loaded is latest one
        {
            ClaimButtonForTutorialLevels.gameObject.SetActive(false);

            ClaimButton.gameObject.SetActive(false);

            ClaimButton2x.gameObject.SetActive(false);


        }
        else
        {
            ClaimButtonForTutorialLevels.gameObject.SetActive(false);

            ClaimButton.gameObject.SetActive(false);
            StartCoroutine(DelayClaimButtonEnable());
            ClaimButton2x.gameObject.SetActive(true);
           
        }

        if (GameConstants.GetMode()!=44)             //if not bonus level //setting level number
        {
            //  LevelNumber.text = "LEVEL " + GameConstants.GetLevelNumber().ToString();
            LevelNumberTexts[0].gameObject.SetActive(true);
            LevelNumberTexts[1].gameObject.SetActive(true);
            LevelNumberTexts[2].gameObject.SetActive(true);
          //  LevelNumberTexts[2].gameObject.SetActive(false);

            LevelNumberTexts[1].text =  GameConstants.GetLevelNumber().ToString();

        }
        else
        {
            LevelNumberTexts[0].gameObject.SetActive(false);
            LevelNumberTexts[1].gameObject.SetActive(false);
            LevelNumberTexts[2].gameObject.SetActive(true);
           // LevelNumber.text = "BONUS LEVEL";
        }
        ClaimButton.interactable = true;
        ClaimButton2x.interactable = true;
        ClaimButtonForTutorialLevels.interactable = true;
        ResetStarImages();
        StartCoroutine(StarsFiller());
        /* RateUsNumber++;
         if (RateUsNumber % 3==0)
         {
         AdController.instance.PromptRateMenu();

         }*/
        /*
                if (GameConstants.GetLevelNumber() == 5 )
                {
                    RewardCostumeMenu.SetActive(true);
                }
                   */

        CoinsText.text = GameConstants.CoinsEarned.ToString();

        if (GameConstants.GetSubscriptionStatus() == "Yes")
        {
            CoinsEarnedViaSubscriptionGameObject.gameObject.SetActive(true);
            
          //  CoinsTextSubscriptionBonus.gameObject.SetActive(true);

            coinsEarnedviaSubscription = (GameConstants.CoinsEarned * 20) / 100; // GameConstants.CoinsEarnedBySubscription;

            CoinsTextSubscriptionBonus.text = coinsEarnedviaSubscription.ToString();


        }
        else
        {
            CoinsEarnedViaSubscriptionGameObject.gameObject.SetActive(false);

            //CoinsTextSubscriptionBonus.gameObject.SetActive(false);

        }


        PluginCallBackManager.Score2xRewardAdComplete_ += giveRewardScore2x;

    }

    public IEnumerator DelayClaimButtonEnable()
    {
    //    ClaimButtonText.text = "No Thanks";
        yield return new WaitForSeconds(2);
        ClaimButton.gameObject.SetActive(true);

    }

    public void EnableClaimButtonsAfterRewardNoThanks()
    {
        ClaimButton.gameObject.SetActive(true);
      //  StartCoroutine(DelayClaimButtonEnable());
        ClaimButton2x.gameObject.SetActive(true);
    }
    public void EnableClaimButtonsAfterRewardTake()
    {
        ClaimButtonForTutorialLevels.gameObject.SetActive(true);
        ClaimButton2x.gameObject.SetActive(false);
        ClaimButton.gameObject.SetActive(false);

        //  StartCoroutine(DelayClaimButtonEnable());
        // ClaimButton2x.gameObject.SetActive(true);
    }
    public void ResetStarImages()
    {
      //  ProgressImage.fillAmount = (float)0;
        for (int i = 0; i < 3; i++)
        {
            StarsImage[i].SetActive(false);
        }
    }
    public IEnumerator StarsFiller()
    {
        yield return new WaitForSeconds(1f);
        stars = GenericVariables.getRating(GameConstants.GetLevelNumber());         //get the stars earned in this level

     //   Debug.Log("Stars after match end in SUmaary menu " + stars);
        if (stars == 1)
        {
            Debug.Log("stars == 1 SUmaary menu ");
            yield return new WaitForSeconds(0.2f);

         //   ProgressImage.fillAmount = (float) 1 / 3;

            StarsImage[0].SetActive(true);
        }
        else if (stars == 2)
        {
            Debug.Log("stars == 2 SUmaary menu ");
            yield return new WaitForSeconds(0.2f);

        //    ProgressImage.fillAmount = (float)2 / 3;
            StarsImage[0].SetActive(true);
            yield return new WaitForSeconds(0.3f);

            StarsImage[1].SetActive(true);

        }
        else if (stars == 3)
        {
       //     Debug.Log("stars == 3 SUmaary menu ");

            yield return new WaitForSeconds(0.2f);
     //       ProgressImage.fillAmount = (float)3 / 3;
            StarsImage[0].SetActive(true);
            yield return new WaitForSeconds(0.3f);
            StarsImage[1].SetActive(true);
            yield return new WaitForSeconds(0.3f);
            StarsImage[2].SetActive(true);

        }
    }
    public void LevelSuccessNextButton()                    //to go back to level menu from gameplay scene call this
    {
        // MenuManager.Instance.DisablePopUps();
        StartCoroutine(LevelSuccessClaimButtonClick());

    }

    public IEnumerator LevelSuccessClaimButtonClick()
    {
        ClaimButton.GetComponent<Button>().interactable = false;
        ClaimButton2x.GetComponent<Button>().interactable = false;
        ClaimButtonForTutorialLevels.GetComponent<Button>().interactable = false;
        PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
        if (GameConstants.getFortuneWheelShowOnLevel2Availability() == "Yes" && GameConstants.GetLevelNumber() < 3)           //if its first time than take back to main menu to continue tutorial
        {
            Time.timeScale = 1;
            Debug.Log("getStartGamePlayTutorial is YES");
            MenuManager.Instance.moveObject("coins", CoinImage);
            MenuManager.Instance.moveConsumable();
            yield return new WaitForSeconds(2f);

            if (GameConstants.GetSubscriptionStatus() == "Yes")
            {

                coinsEarned = coinsEarned + coinsEarnedviaSubscription;

            }
            GenericVariables.AddCoins(GameConstants.CoinsEarned);
           // yield return new WaitForSeconds(0.2f);
            yield return new WaitForSeconds(0.8f);

            if (GameConstants.GetLevelNumber() == 1)
            {
              //  Debug.Log("GetLevelNumber is 1");
                GameConstants.SetLevelNumber(2);

                PopUpLevelDetails.LevelDetailsSetter();
                MenuManager.Instance.DisableAll();
                GamePlayManager.instance.ResetScore();
                //     ClaimButton.GetComponent<Button>().interactable = true;
                //    ClaimButtonForTutorialLevels.GetComponent<Button>().interactable = true;

                    MenuManager.Instance.LoadLevel();
                //MenuManager.Instance.levelSuccessTakeToLevelMenu();

            }
            else if (GameConstants.GetLevelNumber() == 2)
            {
          //      Debug.Log("GetLevelNumber is 2");
                GameConstants.SetLevelNumber(3);
                PopUpLevelDetails.LevelDetailsSetter();

                MenuManager.Instance.DisableAll();

                GamePlayManager.instance.ResetScore();
          //      ClaimButtonForTutorialLevels.GetComponent<Button>().interactable = true;
           //     ClaimButton.GetComponent<Button>().interactable = true;
           //     ClaimButton2x.GetComponent<Button>().interactable = true;

                UIAnimations.instance.MainButton(4);   //call spin wheel

                //  LoadLevel();

            }
         /*   else if (GameConstants.GetLevelNumber() == 3)
            {
         //       ClaimButton.GetComponent<Button>().interactable = true;
        //        ClaimButton2x.GetComponent<Button>().interactable = true;
          //      ClaimButtonForTutorialLevels.GetComponent<Button>().interactable = true;

                //      levelSuccessTakeToLevelMenu();

            }*/

            // levelSuccessTakeToMainMenu();
        }
        else if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            Time.timeScale = 1;                                               //changing scene to menus
                                                                              //  ButtonFiller button = new ButtonFiller();
                                                                              //  button.FillButtons(GenericVariables.getTotalLevelsPassed());
            MenuManager.Instance.moveObject("coins", CoinImage);
            MenuManager.Instance.moveConsumable();
            yield return new WaitForSeconds(2f);
            if (GameConstants.GetSubscriptionStatus() == "Yes")
            {

                coinsEarned = coinsEarned + coinsEarnedviaSubscription;

            }
            GenericVariables.AddCoins(GameConstants.CoinsEarned);
            yield return new WaitForSeconds(0.8f);

            /*Debug.Log("GetLevelNumber = " + GameConstants.GetLevelNumber());
            Debug.Log("getTotalLevelsPassed = " + GenericVariables.getTotalLevelsPassed());
*/
            if (GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed())      //check if level is being played first time //we dont want repeated bonus levels on same level
            {
                if (GamePlayManager.instance.BonusLevel)         //if its a bonus level
                {
                 
                    //Debug.Log("level complete nexttttttttttttttttt if bonus level ");
                    GameConstants.LevelNumberPlayed = GameConstants.GetLevelNumber();
                    

                    GameConstants.SetLevelNumber(0);            //BONUS LEVEL

                    PopUpLevelDetails.LevelDetailsSetter();
                    MenuManager.Instance.DisableAll();
                  //  GamePlayManager.instance.LevelDetailsSetter();

                    GamePlayManager.instance.ResetScore();
                    //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;


                    MenuManager.Instance.LoadLevel();


                }
                else  //this check is not working bcz GetLevelNumber == getTotalLevelsPassed happens only when next level is bonus and if next level is bonus it will be caught up in above IF statement
                      //where as getTotalLevelsPassed will always be 1+ bcz of UnlockNextLevel() being called on level success before setlevenumber ++;
                {
                  //  Debug.Log("level complete nexttttttttttttttttt bonus level else");
                    // levelSuccessTakeToLevelMenu();
                    if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
                    {
                        GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed + 1);

                        PopUpLevelDetails.LevelDetailsSetter();
                   //     GamePlayManager.instance.LevelDetailsSetter();
                        
                            MenuManager.Instance.DisableAll();
                        GamePlayManager.instance.ResetScore();
                        //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                        //   MenuManager.Instance.LoadLevel();
                        MenuManager.Instance.levelSuccessTakeToLevelMenu();

                        //   GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                    }
                    else
                    {
                        MenuManager.Instance.levelSuccessTakeToLevelMenu();

                    }
                }
            }
            else  //calls after level end and bonus level end 
            {
                //   levelSuccessTakeToLevelMenu();

                if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
                {
                //    Debug.Log("level complete nexttttttttttttttttt");
                    GameConstants.SetLevelNumber(GameConstants.GetLevelNumber() + 1);
                    PopUpLevelDetails.LevelDetailsSetter();
                //    MenuManager.Instance.DisableAll();
            //        GamePlayManager.instance.LevelDetailsSetter();
                    GamePlayManager.instance.ResetScore();
                    //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                    //   Debug.Log("Level number is ................................. " + GameConstants.GetLevelNumber());

                    /*          if (GameConstants.GetLevelNumber() == 4)   //check to show multiplayer mode dialog
                              {
                             //     Debug.Log("Level number is ..............................2... " + GameConstants.GetLevelNumber());

                                  MenuManager.Instance.DisableAll();

                                  MenuManager.Instance.UIMultiplayerOfferDialog.SetActive(true);
                              }
                              else
                              {
                            //      Debug.Log("Level number is ..............................3... " + GameConstants.GetLevelNumber());


                              }
                    */
                    MenuManager.Instance.DisableAll();

                    //MenuManager.Instance.LoadLevel();
                    MenuManager.Instance.levelSuccessTakeToLevelMenu();

                    //   GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                }
                else
                {
                    MenuManager.Instance.levelSuccessTakeToLevelMenu();

                }
            }

        }
        Time.timeScale = 1f;
       
    }
    public void onClickClaim2xCoinButton()
    {
        GenericVariables.RewardAdIndex = 1;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    public void giveRewardScore2x()
    {
        StartCoroutine(DelayGiveRewardScore2x());
    }
    IEnumerator DelayGiveRewardScore2x()
    {
        Time.timeScale = 1;
        Watched2xRewardAd = true;
        ClaimButton.interactable = false;
        ClaimButton2x.interactable = false;
      //  RewardIconButton.interactable = false;
        PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
        GenericVariables.RewardAdIndex = -1;
        coinsEarned = GameConstants.CoinsEarned * 2;
        CoinsText.text = coinsEarned.ToString();

        if (GameConstants.GetSubscriptionStatus() == "Yes")
        {

            coinsEarned = coinsEarned + coinsEarnedviaSubscription;

        }

        MenuManager.Instance.moveObject("coins", CoinImage);
        MenuManager.Instance.moveConsumable();
        yield return new WaitForSeconds(2f);
        GenericVariables.AddCoins(coinsEarned);
        yield return new WaitForSeconds(0.2f);

        //  MenuManager.Instance.levelSuccessTakeToLevelMenu();

        if (GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed())      //check if level is being played first time //we dont want repeated bonus levels on same level
        {
            if (GamePlayManager.instance.BonusLevel)         //if its a bonus level
            {
                GameConstants.LevelNumberPlayed = GameConstants.GetLevelNumber();
                GameConstants.SetLevelNumber(0);            //BONUS LEVEL

                /*
                     ClaimButton.interactable = true;
                     ClaimButton2x.interactable = true;
                */

                MenuManager.Instance.DisableAll();
                PopUpLevelDetails.LevelDetailsSetter();
             //   GamePlayManager.instance.LevelDetailsSetter();
                GamePlayManager.instance.ResetScore();
                //  MenuManager.Instance.ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;
                MenuManager.Instance.LoadLevel();
                /*GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                GamePlayManager.instance.UnlockNextLevel();*/
            }
            else
            {
                if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
                {
                    GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed + 1);
                    MenuManager.Instance.DisableAll();
                    PopUpLevelDetails.LevelDetailsSetter();
                  //  GamePlayManager.instance.LevelDetailsSetter();

                    GamePlayManager.instance.ResetScore();
                    //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                    //   MenuManager.Instance.LoadLevel();
                    MenuManager.Instance.levelSuccessTakeToLevelMenu();

                    //   GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                }
                else
                {
                    MenuManager.Instance.levelSuccessTakeToLevelMenu();


                }

            }
            /*  if (GameConstants.getFirstStartUITutorial() == "Yes")           //if its first time than take back to main menu to continue tutorial
              {
                  MenuManager.Instance.levelSuccessTakeToMainMenu();
              }
              else
              {
                  MenuManager.Instance.levelSuccessTakeToLevelMenu();
              }*/
        }
        else
        {

            if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
            {
                GameConstants.SetLevelNumber(GameConstants.GetLevelNumber() + 1);
                MenuManager.Instance.DisableAll();
                PopUpLevelDetails.LevelDetailsSetter();
         //       GamePlayManager.instance.LevelDetailsSetter();

                GamePlayManager.instance.ResetScore();
                //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                //MenuManager.Instance.LoadLevel();
                MenuManager.Instance.levelSuccessTakeToLevelMenu();

                //   GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
            }
            else
            {
                MenuManager.Instance.levelSuccessTakeToLevelMenu();


            }

        }
        
    }

    public void RewardProgress() //progress bar updation and reward calling at level end
    {
        float ProgressValue= GameConstants.GetRewardProgress();
        Debug.Log("GameConstants.GetLevelNumber() + " +GameConstants.GetLevelNumber());
        Debug.Log("GenericVariables.getTotalLevelsPassed() +    "+ GenericVariables.getTotalLevelsPassed());

        if (GameConstants.GetLevelNumber() != GenericVariables.getTotalLevelsPassed() && GameConstants.GetMode() != 44)     //if level is being played again or Mode is bonus
        {
            //if playing old level
            //    Debug.Log("RewardProgress if");
            Debug.Log("NO PROGRESSSSSS");
            RewardProgressImage.fillAmount = ProgressValue;
            RewardItemImage.fillAmount = ProgressValue;
          //  RewardItemImageWhite.fillAmount = ProgressValue;
            RewardProgressText.text = (ProgressValue * 100f).ToString() + "%";
        }
        else //if playing new level
        {
            //   Debug.Log("RewardProgress else");
            Debug.Log("YES PROGRESSSSSS");

            if (ProgressValue!=1)
            {
                ProgressValue += ProgressRewardBy;

            }
            RewardProgressImage.fillAmount = ProgressValue;
            RewardItemImage.fillAmount = ProgressValue;
         //   RewardItemImageWhite.fillAmount = ProgressValue;
            RewardProgressText.text = (ProgressValue * 100f).ToString() + "%";

            GameConstants.SetRewardProgress(ProgressValue);

            if (RewardProgressImage.fillAmount == 1 && GameConstants.GetGameOverRewardNumber()<= GameConstants.TotalGameOverRewards)    //progress bar is full
            {
                RewardMenuCall(3f);
              //  RewardIconButton.interactable = true;

            }
            else if(GameConstants.GetGameOverRewardNumber() > GameConstants.TotalGameOverRewards)
            {
                GameConstants.SetRewardProgress(0);
            }
        }
    }
    public void RewardMenuCall(float time)
    {
        StartCoroutine(DelayRewardMenuCall(time));
    }
    IEnumerator DelayRewardMenuCall(float time)
    {
        yield return new WaitForSeconds(time);
        
        RewardCostumeMenu.SetActive(true);
    }
    public void SelectRewardToOffer()
    {
        RewardNumber = GameConstants.GetGameOverRewardNumber(); //by default 1
      //  Debug.Log("Reward Number is " + RewardNumber);
        switch (RewardNumber)
        { //if (RewardNumber==1)
            case 1:                 //ball
                {
                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedFootballWithStatus(GameConstants.RewardFootballstoOffer[0]) != 2)
                    {

                      /*  RewardItemImage.sprite = RewardItemsImagesList[0];
                        RewardItemImageBase.sprite = RewardItemsImagesList[0];  */
                        
                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[0];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[0];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[0];
                    } 
                    else       //if reward is claimed or bought
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim
                        IncrementRewardNumber();
                    }
                }
                break;
            //if (RewardNumber == 2)
            case 2:                 //kit

                {
                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedBodyWithStatus(GameConstants.RewardBodystoOffer[0])!=2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[1];
                         RewardItemImageBase.sprite = RewardItemsImagesList[1];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[1];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[1];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[1];
                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }

                   
                }
                break;
            case 3:                 //celebration

                {
                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedCelebrationWithStatus(GameConstants.RewardCelebrationstoOffer[0]) != 2)
                    {

                        /*  RewardItemImage.sprite = RewardItemsImagesList[2];
                          RewardItemImageBase.sprite = RewardItemsImagesList[2];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[2];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[2];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[2];


                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }

                }
                break;
            // if(RewardNumber == 3)
            case 4:                     //ball
                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedFootballWithStatus(GameConstants.RewardFootballstoOffer[1]) != 2)
                    {

                        /*RewardItemImage.sprite = RewardItemsImagesList[3];
                        RewardItemImageBase.sprite = RewardItemsImagesList[3];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[3];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[3];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[3];

                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }

                break;

            //if(RewardNumber == 4 )
            case 5:                 //kit

                {
                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedBodyWithStatus(GameConstants.RewardBodystoOffer[0]) != 2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[4];
                         RewardItemImageBase.sprite = RewardItemsImagesList[4];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[4];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[4];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[4];
                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }


                }

                break;

            //if(RewardNumber == 5)
            case 6:                     //skin

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedSkinWithStatus(GameConstants.RewardSkinstoOffer[0])!=2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[5];
                         RewardItemImageBase.sprite = RewardItemsImagesList[5];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[5];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[5];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[5];


                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break;
            case 7:                 //celebration

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedCelebrationWithStatus(GameConstants.RewardCelebrationstoOffer[1]) != 2)
                    {

                        /*RewardItemImage.sprite = RewardItemsImagesList[6];
                        RewardItemImageBase.sprite = RewardItemsImagesList[6];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[6];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[6];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[6];


                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break; 
            case 8:                  //kit

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedBodyWithStatus(GameConstants.RewardBodystoOffer[0]) != 2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[7];
                         RewardItemImageBase.sprite = RewardItemsImagesList[7];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[7];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[7];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[7];
                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break;
            case 9:                  //ball

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedFootballWithStatus(GameConstants.RewardFootballstoOffer[2]) != 2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[8];
                         RewardItemImageBase.sprite = RewardItemsImagesList[8];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[8];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[8];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[8];

                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break;
            case 10:                 //kit

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedBodyWithStatus(GameConstants.RewardBodystoOffer[0]) != 2)
                    {

                        /*RewardItemImage.sprite = RewardItemsImagesList[9];
                        RewardItemImageBase.sprite = RewardItemsImagesList[9];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[9];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[9];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[9];
                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break;
            case 11:                  //ball

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedFootballWithStatus(GameConstants.RewardFootballstoOffer[3]) != 2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[10];
                         RewardItemImageBase.sprite = RewardItemsImagesList[10];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[10];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[10];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[10];

                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break; 
            case 12:                  //ball

                {

                    if (GameConstants.GetGameOverRewardClaim(RewardNumber) != "Claimed" && GenericVariables.checkUnlockedFootballWithStatus(GameConstants.RewardFootballstoOffer[4]) != 2)
                    {

                        /* RewardItemImage.sprite = RewardItemsImagesList[11];
                         RewardItemImageBase.sprite = RewardItemsImagesList[11];*/

                        //RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[11];
                        //RewardItemImageWhite.sprite = CustomizationScript.instance.RewardItemsImagesList[11];

                        //RewardItemImageBase.sprite = CustomizationScript.instance.RewardItemsImagesList[11];

                    }
                    else
                    {
                        GameConstants.SetGameOverRewardClaim(RewardNumber); //if reward is claimed or bought //mark as claim

                        IncrementRewardNumber();

                    }
                }
                break;
            case 13:
                //  if (RewardNumber == 6) //no reward available
                {
                   /* RewardItemImage.gameObject.SetActive(false);
                    RewardItemImageBase.gameObject.SetActive(false);*/
                    RewardProgressPanel.SetActive(false);
                }
                break;
        }
        
    }
    public void IncrementRewardNumber() 
    {
       /* Debug.Log("IncrementRewardNumber");
        PreviousRewardNumber = RewardNumber;
        for (int i = RewardNumber; i < GameConstants.TotalGameOverRewards; i++)
        {
            if(i== GameConstants.TotalGameOverRewards)      //if i reached end
            {
                if(PreviousRewardNumber!=1)         //when function started Reward was not 1 //addinf this so it not keep looping
                {
                    PreviousRewardNumber = 1;
                    i = 0;                          //added so we check the whole array once if any unclaimed reward is present

                }else           //if previous reward number was 1 that means array has been checked already and on reward is present
                {
                    GameConstants.SetGameOverRewardNumber(GameConstants.TotalGameOverRewards + 1);      //set limit + 1 so no image is loaded and no reward is available

                    break;
                }
            }
            if (GameConstants.GetGameOverRewardClaim(i+1) != "Claimed")       //check if unclaimed reward is here
            {
               
                GameConstants.SetGameOverRewardNumber(i+1);

                    break;
            }
        }
        SelectRewardToOffer();*/

          bool FoundAnyInQueue=false;
          if (RewardNumber < GameConstants.TotalGameOverRewards )
           {
          //   Debug.Log("IncrementRewardNumber ++");
               GameConstants.SetGameOverRewardNumber(RewardNumber + 1);
               SelectRewardToOffer();
           }
           else
           {
             //   Debug.Log("IncrementRewardNumber +" + GameConstants.GetGameOverRewardNumber());

                if (GameConstants.GetGameOverRewardNumber() <= GameConstants.TotalGameOverRewards)
                {
                    for (int i = 1; i <= GameConstants.TotalGameOverRewards; i++)
                    {
                     //   Debug.Log("IncrementRewardNumber ++");
                        // if( GameConstants.GetGameOverRewardClaim(i) != "Claimed")
                        if (GameConstants.GetGameOverRewardClaim2(i))
                        {
                       //     Debug.Log("IncrementRewardNumber else ++" + i);

                            GameConstants.SetGameOverRewardNumber(i);
                            FoundAnyInQueue = true;
                      //      Debug.Log("FoundAnyInQueue  ++" + FoundAnyInQueue);

                            break;
                        }

                    }
                }
                 //   Debug.Log("FoundAnyInQueue  ++" + FoundAnyInQueue);

                if (!FoundAnyInQueue)
                      {
                          GameConstants.SetGameOverRewardNumber(GameConstants.TotalGameOverRewards + 1);        //all rewards aare taken

                      }

        
                      SelectRewardToOffer();

          }

    }
    private void OnDisable()
    {
        AdController.instance.hideNativeBannerAd();

        PluginCallBackManager.Score2xRewardAdComplete_ -= giveRewardScore2x;

    }
}
