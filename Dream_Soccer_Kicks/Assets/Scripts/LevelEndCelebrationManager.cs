﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using Cinemachine;

public class LevelEndCelebrationManager : MonoBehaviour
{
    public static LevelEndCelebrationManager instance;
    public GameObject Camera;
    public GameObject Camera2;
    public GameObject PlayerObj;
    public GameObject GoalkeeperObj;
    public GameObject GoalkeeperObj_Parent;
    public GameObject PlayerStandPos;
    public GameObject GKStandPos;
    public GameObject Screen;
    public GameObject LookAtObject;
    public GameObject Hurdles;
    public GameObject LookAtObject2;
    public GameObject Ball;
    public GameObject Wall;
    public GameObject SkipButton;

    bool LevelWon;
    Animator PlayerAnimator;
    Animator GoalKeeperAnimator;
    public AnimationClip[] AnimationList;   
    public AnimatorOverrideController animatorOverrideController;
    public AnimatorOverrideController animatorOverrideControllerOrginal;
    // public static int AnimControllerNum = 4;

    [Header("Trophy Cutscene")]
    public GameObject LevelEndTimelineObject;
    public GameObject TimelineMainCamera;
    public GameObject SkipButtonTrophyCutscene;

    public AnimationClip[] AnimationTrophyWonList;
    public PlayableDirector playableDirector;
    public CinemachineVirtualCamera[] VirtualCameraArray;
  //  public CinemachineVirtualCamera VirtualCamera2;
    private void OnEnable()
    {

        playableDirector.stopped += DisableCutscene;

    }
    public void DisableCutscene(PlayableDirector aDirector)
    {
        if (playableDirector == aDirector)
        {
            //     Debug.Log("DisableCutscene");
            // AdController.instance.HideBannerAd();

            /*GamePlayManager.instance.CrowdSets[0].SetActive(true);
            GamePlayManager.instance.CrowdSets[1].SetActive(false);
            GamePlayManager.instance.CrowdSets[2].SetActive(false);
            GamePlayManager.instance.CrowdSets[3].SetActive(false);
            MenuManager.Instance.Loading(true);
           */
            DisableObjectOnCutsceneEnd();
        }
    }

    public void DisableObjectOnCutsceneEnd()
    {
        SkipButtonTrophyCutscene.SetActive(false);
        PlayerObj.transform.parent = GamePlayManager.instance.FootballCore.transform.parent;
        GoalkeeperObj.transform.parent = GamePlayManager.instance.GKPlayer_Parent.transform;
        PlayerObj.SetActive(true);
        GoalkeeperObj.SetActive(true);
        playableDirector.gameObject.SetActive(false);

        for (int i = 0; i < VirtualCameraArray.Length; i++)
        {
            VirtualCameraArray[i].gameObject.SetActive(false);


        }
        LevelEndTimelineObject.SetActive(false);
        TimelineMainCamera.SetActive(false);
        //  PlayerObj.transform.parent = GamePlayManager.instance.FootballCore.transform;
        StartCoroutine(PlayerAnimatorSetterAfterTrophyTimelineComplete());

    }
    public void SkipButtonTrophyTimeline()
    {
       
            if (GameConstants.GetMode() == 10)
            {
              GamePlayManager.instance.StopDelayLevelWonCoroutine();
                TournamentHandler.instance.StopDelayTournamentCoroutine();

            StartCoroutine(TournamentHandler.instance.delayTournamentWon(0));

            }
            else
            {
                GamePlayManager.instance.StopDelayLevelWonCoroutine();

                StartCoroutine(GamePlayManager.instance.DelayLevelComplete(0));

            }
            LevelWon = false;

        DisableObjectOnCutsceneEnd();
    }
    public void PlayCutscene_TrophyWon()
    {
        playableDirector.gameObject.SetActive(true);
        playableDirector.Play();
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    
    void Start()
    {
        
        Camera.SetActive(false);
        Camera2.SetActive(false);
        Screen.SetActive(false);

        PlayerObj = GamePlayManager.instance.Player;
        GoalkeeperObj = GamePlayManager.instance.GKPlayer;
        PlayerAnimator = PlayerObj.GetComponent<Animator>();
        GoalKeeperAnimator = GamePlayManager.instance.GKPlayer_Parent.GetComponent<Animator>();
       // GoalKeeperAnimator = GoalkeeperObj.GetComponent<Animator>();

       // StartCoroutine( PlayerAnimatorSetter());

    }
    public IEnumerator PlayerAnimatorSetter()  // animator overiden for win animation play
    {
        yield return new WaitForSeconds(0f);
      //  PlayerAnimator = CustomUIPlayer.GetComponent<Animator>();

        animatorOverrideControllerOrginal = new AnimatorOverrideController(PlayerAnimator.runtimeAnimatorController)
        {
            name = "OrginalAnimator"
        };

        animatorOverrideController = new AnimatorOverrideController(PlayerAnimator.runtimeAnimatorController)
        {
            name = "CelebrationAnimator"
           
        };
        
        
    } 
    public IEnumerator PlayerAnimatorSetterAfterTrophyTimelineComplete()  // animator overiden for win animation play
    {
        yield return new WaitForSeconds(0f);
      //  PlayerAnimator = CustomUIPlayer.GetComponent<Animator>();

        animatorOverrideControllerOrginal = new AnimatorOverrideController(PlayerAnimator.runtimeAnimatorController)
        {
            name = "OrginalAnimator"
        };

        animatorOverrideController = new AnimatorOverrideController(PlayerAnimator.runtimeAnimatorController)
        {
            name = "CelebrationAnimator"
           
        };

        PlayerScript.instance.LevelEndStopAnim();

        //    Debug.Log("Animation number is... " + GenericVariables.getcurrCelebration());

        animatorOverrideController["Celebrate"] = AnimationList[GenericVariables.getcurrCelebration()];           //assign required animation on "Celebrate" animation

        PlayerAnimator.runtimeAnimatorController = animatorOverrideController;

        PlayerAnimator.Play("Celebration");

        //  yield return new WaitForSeconds(0.01f);

      //  PlayerObj.transform.position = PlayerStandPos.transform.position;
       // PlayerObj.transform.LookAt(LookAtObject.transform);

        //GoalkeeperObj.transform.position = GKStandPos.transform.position;
      //  GoalkeeperObj.transform.LookAt(LookAtObject.transform);

        GoalKeeperAnimator.Play("Lose");

        Camera2.SetActive(true);


    }
    public void ApplyOrginalAnimator()      //orginal animator applied
    {
        
        PlayerAnimator.runtimeAnimatorController = animatorOverrideControllerOrginal;
        //    animatorOverrideController["Take 001"] = animationOrginal;
    }

    public void GameWon()
    {
        AdController.instance.HideBannerAd();
        SkipButton.SetActive(false);
        Screen.SetActive(true);
        MenuManager.Instance.DisableAll();
  /*      GamePlayManager.instance.CrowdSets[0].SetActive(true);
        GamePlayManager.instance.CrowdSets[1].SetActive(true);
        GamePlayManager.instance.CrowdSets[2].SetActive(false);
        GamePlayManager.instance.CrowdSets[3].SetActive(false);*/
        StartCoroutine(DelayGameWon());
    }
    IEnumerator DelayGameWon()
    {
        /* PlayerScript.instance.anim.runtimeAnimatorController = Resources.Load("Player" +AnimControllerNum) as RuntimeAnimatorController;    //load random animator for player kicks
          if (AnimControllerNum < 9)
          {
              AnimControllerNum++;
          }
          else
          {
              AnimControllerNum = 4;
          }*/

        StartCoroutine(PlayerAnimatorSetter());
        yield return new WaitForSeconds(1.2f);

        
        MenuManager.Instance.setSettingsButton(false);
        HudManager.instance.DisableExtraObjectFromScreen();    //disables extra screen buttons when in celebration
        LevelWon = true;
      //  SkipButton.SetActive(true);
       
        StartCoroutine(EnableSkipButton());                         //enable skip button after sometime
        StartCoroutine(DisableSkipButton());                            //disable skip button after sometime

        GamePlayManager.instance.CelebrationParticles.SetActive(true);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playMusic("Celebration");
        }
        TutorialManager.instance.TurnOffGameplayTutorial();
      //  Hurdles.SetActive(false);
        if(Wall!=null)
        {
           Wall.SetActive(false);
        }

        //Ball.transform.position = LookAtObject2.transform.position;

        PlayerScript.instance.LevelEndStopAnim();
       
    //    Debug.Log("Animation number is... " + GenericVariables.getcurrCelebration());
       
        animatorOverrideController["Celebrate"] = AnimationList[GenericVariables.getcurrCelebration()];           //assign required animation on "Celebrate" animation
      
        PlayerAnimator.runtimeAnimatorController = animatorOverrideController;
        
        PlayerAnimator.Play("Celebration");
        
        //  yield return new WaitForSeconds(0.01f);
       
        PlayerObj.transform.position = PlayerStandPos.transform.position;
        PlayerObj.transform.LookAt(LookAtObject.transform);

        GoalkeeperObj.transform.position = GKStandPos.transform.position;
        GoalkeeperObj.transform.LookAt(LookAtObject.transform);
        GoalKeeperAnimator.Play("Lose");
        Camera.SetActive(true);
    }
    public void TrophyWon()
    {
        AdController.instance.HideBannerAd();
        SkipButton.SetActive(false);

        StartCoroutine(enableSkipTrophyCutsceneButton());

        //SetCharacter("GKPlayer", GoalKeeperObject);
        Screen.SetActive(true);
        MenuManager.Instance.DisableAll();
      /*  GamePlayManager.instance.CrowdSets[0].SetActive(true);
        GamePlayManager.instance.CrowdSets[1].SetActive(true);
        GamePlayManager.instance.CrowdSets[2].SetActive(false);
        GamePlayManager.instance.CrowdSets[3].SetActive(false);*/

        GameConstants.SetTrophyUnlockedNumber(GameConstants.GetTrophyUnlockedNumber() + 1);   //trophyunlock initial number starts from -1
        GameConstants.SetSeasonCompleted("Yes");       //Season completed check
        //  GameConstants.SetSeasonCompleted(GameConstants.GetTrophyUnlockedNumber() + 1);

        StartCoroutine(DelayTrophyWon());
    }

    IEnumerator enableSkipTrophyCutsceneButton()
    {

        yield return new WaitForSeconds(6f);
        SkipButtonTrophyCutscene.SetActive(true);

    }
    IEnumerator DelayTrophyWon()
    {
        yield return new WaitForSeconds(1.2f);

        HurdlesSetter.instance.HurdleInstanceDestroyer2();          //destroy hurdles before cutscene
        HurdlesSetter.instance.HurdleInstanceDestroyer();
        Shoot.share.HurdleInstanceDestroyer();

        LevelEndTimelineObject.SetActive(true);
        TimelineMainCamera.SetActive(true);
        //   PlayerObjForTimeline = PlayerObj;
        PlayerObj.transform.parent = LevelEndTimelineObject.transform;
        GoalkeeperObj.transform.parent = LevelEndTimelineObject.transform;
        VirtualCameraArray[1].LookAt = PlayerObj.transform;
       // PlayerObj.transform.position = new Vector3(0, 0, -5);
    //    PlayerObj.transform.rotation = Quaternion.Euler(0, 0, 0);
        SetCharacter("KickPlayer", PlayerObj);
        SetCharacter("GKPlayer", GoalkeeperObj);

        // StartCoroutine(PlayerAnimatorSetter());
        PlayCutscene_TrophyWon();

        MenuManager.Instance.setSettingsButton(false);

        HudManager.instance.DisableExtraObjectFromScreen();    //disables extra screen buttons when in celebration
        LevelWon = true;

     /*   for (int i = 0; i < VirtualCameraArray.Length; i++)
        {
            VirtualCameraArray[i].gameObject.SetActive(true);
        }*/

        //  SkipButton.SetActive(true);

        /* StartCoroutine(EnableSkipButton());                         //enable skip button after sometime
         StartCoroutine(DisableSkipButton());                            //disable skip button after sometime
 */
        GamePlayManager.instance.CelebrationParticles.SetActive(true);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playMusic("Celebration");
        }
        TutorialManager.instance.TurnOffGameplayTutorial();
        //  Hurdles.SetActive(false);
        if (Wall != null)
        {
            Wall.SetActive(false);
        }
    }

    public void SetCharacter(string characterType, GameObject character)
    {
        foreach (var playableAssetOutput in playableDirector.playableAsset.outputs)
        {
            //Debug.Log(playableAssetOutput.streamName);
            if (playableAssetOutput.streamName.StartsWith(characterType))
            {
                //Debug.Log(character.name);
                //Debug.Log(temp.name);
                /*                character.transform.localPosition = temp.localPosition;
                                character.transform.localRotation = temp.localRotation;
                                character.transform.localScale = temp.localScale;
                                character.GetComponent<Animator>().applyRootMotion = false;*/

                playableDirector.SetGenericBinding(playableAssetOutput.sourceObject, character);
                //     Destroy(temp.gameObject);
            }

            //Debug.Log(playableAssetOutput.sourceObject);
        }
    }
    public void GameLost()
    {
        AdController.instance.HideBannerAd();
        SkipButton.SetActive(false);

        Screen.SetActive(true);
        MenuManager.Instance.DisableAll();
/*        GamePlayManager.instance.CrowdSets[0].SetActive(true);
        GamePlayManager.instance.CrowdSets[1].SetActive(true);
        GamePlayManager.instance.CrowdSets[2].SetActive(false);
        GamePlayManager.instance.CrowdSets[3].SetActive(false);*/
        StartCoroutine(DelayGameLost());

    }
    IEnumerator DelayGameLost()
    {

        yield return new WaitForSeconds(1.2f);

        MenuManager.Instance.setSettingsButton(false);

        HudManager.instance.DisableExtraObjectFromScreen();

      
        StartCoroutine(EnableSkipButton());                         //enable skip button after sometime
        StartCoroutine(DisableSkipButton());                         //disable skip button after sometime

        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("Crowd_Out");

        }
        TutorialManager.instance.TurnOffGameplayTutorial();

      //  Hurdles.SetActive(false);
        if(Wall != null)
        {
              Wall.SetActive(false);
        }
      //  Ball.transform.position = LookAtObject2.transform.position;
        PlayerObj.transform.position = PlayerStandPos.transform.position;
        PlayerObj.transform.LookAt(LookAtObject.transform);
        PlayerScript.instance.LevelEndStopAnim();
        PlayerAnimator.Play("Lose");
        GoalkeeperObj.transform.position = GKStandPos.transform.position;
        GoalkeeperObj.transform.LookAt(LookAtObject.transform);
        GoalKeeperAnimator.Play("Win");
        Camera.SetActive(true);
    }
    public void DisableLevelEnd()
    {
        Camera.SetActive(false);
        Camera2.SetActive(false);
        Screen.SetActive(false);
        //    Hurdles.SetActive(true);
        /*if(Wall!=null)
        {

        Wall.SetActive(true);
        }*/
        ApplyOrginalAnimator();             //apply orginal animator

        SkipButton.SetActive(false);
        if (GoalkeeperObj_Parent.activeSelf)
        {
       //     Debug.Log("INSIDE DISABLE LEVEL END ");
            GoalkeeperObj_Parent.transform.position = new Vector3(0, 0, 0);
            GoalkeeperObj_Parent.transform.rotation = Quaternion.Euler(0, 180, 0);

            GoalkeeperObj.transform.position = new Vector3(0, 0, 0);
            GoalkeeperObj.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
    public void SkipButtonPressed()                         
    {
        SkipButton.SetActive(false);
        if(LevelWon && GameConstants.GetGameMode()=="Classic")
        {
            if(GameConstants.GetMode() == 10)
            {
                TournamentHandler.instance.StopDelayTournamentCoroutine();
                StartCoroutine(TournamentHandler.instance.delayTournamentWon(0));

            }
            else
            {
                GamePlayManager.instance.StopDelayLevelWonCoroutine();

                StartCoroutine(GamePlayManager.instance.DelayLevelComplete(0));
    
            }
            LevelWon = false;
        }
        else if(!LevelWon && GameConstants.GetGameMode() == "Classic")
        {
            if (GameConstants.GetMode() == 10)
            {
                TournamentHandler.instance.StopDelayTournamentCoroutine();

                StartCoroutine(TournamentHandler.instance.delayTournamentFailed(0));

            }
            else
            {
                GamePlayManager.instance.StopDelayLevelWonCoroutine();

                StartCoroutine(GamePlayManager.instance.DelayLevelFailed(0));
            }
        }
        else if (LevelWon && GameConstants.GetGameMode() == "Missions")
        {
            GamePlayManager.instance.StopDelayLevelWonCoroutine();

            StartCoroutine(GamePlayManager.instance.DelayMissionComplete(0));
            LevelWon = false;
        }
        else if (!LevelWon && GameConstants.GetGameMode() == "Missions")
        {
            GamePlayManager.instance.StopDelayLevelWonCoroutine();

            StartCoroutine(GamePlayManager.instance.DelayMissionFailed(0));
        }
        else if (LevelWon && (GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2"))
        {
            //  GamePlayManager.instance.StopDelayLevelWonCoroutine();

            //  StartCoroutine(GamePlayManager.instance.DelayMissionComplete(0));
            MultiplayerGameOverManager.instance.StopDelayMultiplayerCoroutine(); //stop already running coroutine for end menu
            StartCoroutine( MultiplayerGameOverManager.instance.MultiplayerWonScreen(0));
            LevelWon = false;
        }
        else if (!LevelWon && (GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2"))
        {
            //  GamePlayManager.instance.StopDelayLevelWonCoroutine();
            MultiplayerGameOverManager.instance.StopDelayMultiplayerCoroutine(); //stop already running coroutine for end menu

            //    StartCoroutine(GamePlayManager.instance.DelayMissionFailed(0));
            StartCoroutine( MultiplayerGameOverManager.instance.MultiplayerFailedScreen(0));
        }
        else if (LevelWon && GameConstants.GetGameMode() == "RewardShot2")
        {
            GamePlayManager.instance.StopDelayLevelWonCoroutine();

            StartCoroutine(GamePlayManager.instance.DelayRewardShotComplete(0));
            LevelWon = false;
        }
        else if (!LevelWon && GameConstants.GetGameMode() == "RewardShot2")
        {
            GamePlayManager.instance.StopDelayLevelWonCoroutine();

            StartCoroutine(GamePlayManager.instance.DelayRewardShotFailed(0));
        }

    }

    IEnumerator EnableSkipButton()
    {
        yield return new WaitForSeconds(0.1f);
        SkipButton.SetActive(true);

    }
    IEnumerator DisableSkipButton()
    {
        yield return new WaitForSeconds(1.5f);
        SkipButton.SetActive(false);
    }
    void OnDisable()
    {
        playableDirector.stopped -= DisableCutscene;
    }
}