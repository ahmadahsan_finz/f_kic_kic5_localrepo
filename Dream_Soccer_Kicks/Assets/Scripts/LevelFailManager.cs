﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelFailManager : MonoBehaviour
{
    public static LevelFailManager instance;
    public Text LevelNumberText;
    public Button SkipLevelButton;
    public Button RestartButton;
    public Button ContinueButton;
    public Button HomeButton;
    public Button GoToShopButton;
    public Image ExtraBallSlider;
    public Text ContinueText;
    public GameObject FootballImage;
    public static bool ContinueAdWatched;
    public static bool SkipLevelAdWatched;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        AdController.instance.HideBannerAd();

        AdController.instance.showNativeBannerAd();

        PluginCallBackManager.SkipLevelRewardAdComplete_ += giveRewardSkipLevel;
        PluginCallBackManager.ContinueRewardAdComplete_ += giveRewardContinueLevel;

        LevelNumberText.text = GameConstants.GetLevelNumber().ToString(); //set level number

        if (ContinueAdWatched == true)                 //if continue ad has been watched once //we will not offer continue ad again we will offer skip level with ad button
        {
            ExtraBallSlider.fillAmount = 0;
            ContinueButton.gameObject.SetActive(false);
            if (AdController.instance.IsRewardedAdAvailable() &&  GameConstants.getStartGamePlayTutorial() != "Yes")  //if reward video is available //if tutorial has completed
            {
                SkipLevelButton.gameObject.SetActive(true);     //enable skip level button
                HomeButton.gameObject.SetActive(false);
            }
            else
            {
                HomeButton.gameObject.SetActive(true);          //enable home button
                SkipLevelButton.gameObject.SetActive(false);

            }
            ContinueText.gameObject.SetActive(false);
            FootballImage.SetActive(false);
        }
        else                                                                                        //if continue ad has not been watched before
        {
            if (AdController.instance.IsRewardedAdAvailable() && GameConstants.GetMode() != 10)    //if reward video is available and mode is not tournament
            {
                ExtraBallSlider.fillAmount = 1;
                SkipLevelButton.gameObject.SetActive(false);                    //disable skip level button offer continue level offer
                HomeButton.gameObject.SetActive(false);
                ContinueButton.gameObject.SetActive(true);
                FootballImage.SetActive(true);

                /*  if (GamePlayManager.instance.timerbool == true)
                  {
                      ContinueText.text = "Get +20 Extra Time";

                  }
                  else
                  {
                      ContinueText.text = "Get +3 Extra Kicks";

                  }*/
                ContinueText.gameObject.SetActive(true);
            }
            else if (AdController.instance.IsRewardedAdAvailable() && GameConstants.GetMode() == 10)  //if reward video is available and mode is tournament
            {
                ExtraBallSlider.fillAmount = 0;
                ContinueButton.gameObject.SetActive(false);
                SkipLevelButton.gameObject.SetActive(true);         //offer skip level button
                HomeButton.gameObject.SetActive(false);
                ContinueText.gameObject.SetActive(false);
                FootballImage.SetActive(false);
            }
            else               //if reward video is not available enable home button
            {
                ExtraBallSlider.fillAmount = 0;
                ContinueButton.gameObject.SetActive(false);
                SkipLevelButton.gameObject.SetActive(false);
                HomeButton.gameObject.SetActive(true);
                ContinueText.gameObject.SetActive(false);
                FootballImage.SetActive(false);

            }
        }
        if (GenericVariables.GetEnergy() == 0)  //if energy is 0 than offer go to shop button instead if restart
        {
            RestartButton.gameObject.SetActive(false);
          //  GoToShopButton.gameObject.SetActive(true);

        }
        else
        {
            RestartButton.gameObject.SetActive(true);
          //  GoToShopButton.gameObject.SetActive(false);
        }

    }

    private void Update()
    {
        if (FootballImage.activeSelf)        //if football image is true than update the progress bar of continue level button
        {

            if (ExtraBallSlider.fillAmount != 0)
            {
                // Debug.Log("levelfail managfer update");
                ExtraBallSlider.fillAmount = ExtraBallSlider.fillAmount - 0.2f * Time.deltaTime;
            }
            else if (ExtraBallSlider.fillAmount <= 0 && SkipLevelButton.gameObject.activeSelf == false)
            {
                if (GameConstants.getStartGamePlayTutorial() != "Yes")  //if tutorial has completed offer skip level button
                {

                    SkipLevelButton.gameObject.SetActive(true);
                    HomeButton.gameObject.SetActive(false);

                }
                else                                     //if tutorial has not completed offer home button
                {
                    HomeButton.gameObject.SetActive(true);
                    SkipLevelButton.gameObject.SetActive(false);

                }
                ContinueButton.gameObject.SetActive(false);
                ContinueText.gameObject.SetActive(false);
                FootballImage.SetActive(false);
            }
        }

    }
    public void onClickSkipLevelButton()
    {
        GenericVariables.RewardAdIndex = 2;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }

    public void OnClickContinueLevelButton()
    {
        GenericVariables.RewardAdIndex = 3;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    public void giveRewardSkipLevel()
    {
        StartCoroutine(DelayGiveRewardSkipLevel());
    }
    public IEnumerator DelayGiveRewardSkipLevel()
    {
        Time.timeScale = 1;
        GenericVariables.RewardAdIndex = -1;
        SkipLevelAdWatched = true;
        yield return new WaitForSeconds(1);
        /*     MenuManager.Instance.moveObject("coins", SkipLevelButton.gameObject);
             MenuManager.Instance.moveConsumable();
             yield return new WaitForSeconds(3.0f);
             GenericVariables.AddCoins(GameConstants.CoinsEarned);
             yield return new WaitForSeconds(0.5f);
        */

        if (GameConstants.TrophiesLevelList.Contains(GameConstants.GetLevelNumber()) && GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed())  //check if level is skipped
        {
            GameConstants.SetTrophyUnlockedNumber(GameConstants.GetTrophyUnlockedNumber() + 1);   //trophyunlock initial number starts from -1
            GameConstants.SetSeasonCompleted("Yes");
        }

        GamePlayManager.instance.UnlockNextLevel();

        MenuManager.Instance.levelSuccessTakeToLevelMenu();
    }

    public void HomeButtonClicked()
    {
        Time.timeScale = 1;
        MenuManager.Instance.levelSuccessTakeToLevelMenu();
    }

    private void OnDisable()
    {
        AdController.instance.hideNativeBannerAd();

        PluginCallBackManager.ContinueRewardAdComplete_ -= giveRewardContinueLevel;
        PluginCallBackManager.SkipLevelRewardAdComplete_ -= giveRewardSkipLevel;

    }

    public void GoToShopClicked()
    {

        if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            // AdController.instance.HideBannerAd();

            Time.timeScale = 1;
            if (LevelEndCelebrationManager.instance != null)
            {
                LevelEndCelebrationManager.instance.DisableLevelEnd();

            }
            MenuManager.Instance.DisableAll();
            MenuManager.Instance.LevelMenuLoadBool = false;
            //    MenuManager.Instance.UIPlayer.SetActive(true);
            MenuManager.Instance.EnableShopMenu();                         //taking to mission popup menu
            MenuManager.Instance.Loading(true); //Start Loading Screen
            MenuManager.Instance.sceneChange();                                                  //changing scene to menus
            GamePlayManager.instance.ResetScore();           //reset score //important

        }
    }
    public void giveRewardContinueLevel()
    {
        ContinueAdWatched = true;
        LevelEndCelebrationManager.instance.DisableLevelEnd();
        MenuManager.Instance.DisableAll();
        Time.timeScale = 1;
        GenericVariables.RewardAdIndex = -1;

        GamePlayManager.instance.LevelEndBool = false;
        //    GamePlayManager.instance.LevelDetailsSetter();
        if (GamePlayManager.instance.timerbool == true)                   //level has timer 
        {
            GameConstants.timer = 20f;
            HudManager.instance.UpdateTimer();

        }
        else
        {
            GameConstants.hitsRemaining = 3f;
            HudManager.instance.UpdateKickRemaining("Increment");
        }
        // GamePlayManager.instance.ResetScore();
        HudManager.instance.updateScore();

        HudManager.instance.TargetScoringPanel.SetActive(true);
        HudManager.instance.KeysPanel.SetActive(true);


        HudManager.instance.FireButtonEnableDisableInGamePlay();  //according to remaining count of fireballs

        HudManager.instance.DistanceText.gameObject.SetActive(true);
        // HudManager.instance.StarsHudDisplayReset();
        MenuManager.Instance.Loading(true); //Start Loading Screen
        Shoot.share.reset();
        CameraManager.share.reset();
        GoalKeeper.share.reset();
    }

}

