﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuScript : MonoBehaviour
{
    public static MainMenuScript instance;
    public GameObject UIPlayer;
    [SerializeField] GameObject removeAdsButton;

    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {

        }
       
    }

    private void OnEnable()
    {
        AdController.instance.HideBannerAd();

        UIPlayer = MenuManager.Instance.SelectedPlayer;
        UIPlayer.transform.SetParent(this.gameObject.transform);
        UIPlayer.transform.localPosition = new Vector3(1875.95f, -3450.699f, 249f);
        UIPlayer.transform.localScale = new Vector3(1, 1, 1);

        UIPlayerPositionReset_SpinLogicEnableDisable(true);

#if UNITY_IPHONE || UNITY_EDITOR 

        //if (AdConstants.shouldDisplayAds())
        //    removeAdsButton.SetActive(true);
        //else removeAdsButton.SetActive(false);

        //if (GameConstants.GetSubscriptionStatus() != "Yes" && AdConstants.shouldDisplayAds())
        //{
        //    removeAdsButton.SetActive(true);
        //}
        //else
        //{
        //    removeAdsButton.SetActive(false);

        //}
        removeAdsButton.SetActive(false);

#elif UNITY_ANDROID
        removeAdsButton.SetActive(false);

#endif

       
            Invoke("showRewardKickDialog", 0.7f);
        


      
        //UIPlayer.transform.rotation = Quaternion.Euler(0, 160f, 0);
        // Invoke("PlayerRotationRester", 4f);
    }

    public void showRewardKickDialog()
    {
        if (GameConstants.getStartGamePlayTutorial() != "Yes" && (RewardOnGameStartMenu.RewardKickCloseState != RewardOnGameStartMenu.RewardKickDialogClose.closed) && RewardedTimeManager.instance.CheckTimerRewardKickForExternalUser())    //check if reward kick timer is up and show reward kick dialog
        {
             if (!UITutorialManager.instance.BlackShaderMultiplayerVersusScreen.activeSelf && !MenuManager.Instance.popUps[1].activeSelf) //missions pop up and multiplayer tutorial is off
            {
               // Debug.Log("RewardKickDialog show ");
          
                Debug.Log("showRewardKickDialog");
              MenuManager.Instance.RewardKickPopUpShow();
            }

        }

        //UIPlayer.transform.rotation = Quaternion.Euler(0, 160f, 0);
        // Invoke("PlayerRotationRester", 4f);
    }

    public void UIPlayerPositionReset_SpinLogicEnableDisable(bool value)
    {
        UIPlayer = MenuManager.Instance.SelectedPlayer;
        UIPlayer.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        UIPlayer.transform.rotation = Quaternion.Euler(0, 160f, 0);
        UIPlayer.GetComponent<spinLogic>().enabled = value;
    }

    public void removeAdButtonClicked()
    {
        ShopScript.instance.RemoveAds();
    }

    void disableAds()
    {
        AdConstants.disableAds();
        removeAdsButton.SetActive(false);
    }
}
