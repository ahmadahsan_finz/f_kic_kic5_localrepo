﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MissionFailedManager : MonoBehaviour
{
    public Button GoToShopButton;
    public Button RestartButton;
    public Button HomeButton;

    // Start is called before the first frame update
    private void OnEnable()
    {
        
        AdController.instance.showNativeBannerAd();
        
        HomeButton.interactable = true;
        GoToShopButton.interactable = true;
        RestartButton.interactable = true;

        if (GenericVariables.GetEnergy() == 0)
        {
            RestartButton.gameObject.SetActive(false);
           // GoToShopButton.gameObject.SetActive(true);

        }
        else
        {
            RestartButton.gameObject.SetActive(true);
           // GoToShopButton.gameObject.SetActive(false);
        }
    }
   public void OnClickHomeButton()
    {
        Time.timeScale = 1;
     //   AdController.instance.HideBannerAd();

        HomeButton.interactable = false;
        RestartButton.interactable = false;
        GoToShopButton.interactable = false;
        StartCoroutine(DelayOnClickHomeButton());
    }

    IEnumerator DelayOnClickHomeButton()
    {
        yield return new WaitForSeconds(0.5f);
        {
            //  MenuManager.Instance.DisablePopUps();
            MenuManager.Instance.DisableAll();
            LevelEndCelebrationManager.instance.DisableLevelEnd();

            //   setHUD();
            //      UIPlayer.SetActive(true);
            //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            MenuManager.Instance.setMain(true);
            //  SettingsBackButton.gameObject.SetActive(false);
            //   GameConstants.score = 0;
            GamePlayManager.instance.ResetScore();
            MenuManager.Instance.LevelMenuLoadBool = false;

            MenuManager.Instance.Loading(true); //Start Loading Screen
            MenuManager.Instance.sceneChange();
        }
    }
    public void GoToShopClicked()
    {
        HomeButton.interactable = false;
        GoToShopButton.interactable = false;
        if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            Time.timeScale = 1;
            LevelEndCelebrationManager.instance.DisableLevelEnd();
            MenuManager.Instance.DisableAll();
            MenuManager.Instance.LevelMenuLoadBool = false;
            //    MenuManager.Instance.UIPlayer.SetActive(true);
            MenuManager.Instance.EnableShopMenu();                         //taking to mission popup menu
            MenuManager.Instance.Loading(true); //Start Loading Screen
            MenuManager.Instance.sceneChange();                                                  //changing scene to menus
            GamePlayManager.instance.ResetScore();           //reset score //important

        }
    }
    private void OnDisable()
    {
        AdController.instance.hideNativeBannerAd();
    }
}
