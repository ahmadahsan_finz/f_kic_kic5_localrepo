﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MissionMenuScript : MonoBehaviour
{
	//MissionMode 1: Kicks with Goals
	//MissionMode 2: Kicks with Scores
	//MissionMode 3: Timer with Goals
	//MissionMode 4: Timer with Scores
	//MissionMode 5: Kicks with do not miss
	//MissionMode 6: Kicks with Bulleyes
	//MissionMode 7: Timer with Bulleyes
	//MissionMode 8: Timer with do not miss


	public int[] MissionCoins;
	public Text[] MissionCoinsTexts;
	public GameObject[] playButtons;
	public GameObject[] claimButtons;
	public GameObject UIPlayer;
	

	private void OnEnable()
    {
		//	ShowAdsGameover.instance.CallGameoverAd(0.1f);   //calling it for missions
		//	MenuManager.Instance.UIPlayer.SetActive(false);

		MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(false); //disable spin on Ui player

		for (int i = 0; i < MissionCoins.Length; i++)
		{
			MissionCoinsTexts[i].text = MissionCoins[i].ToString();

			if (GameConstants.GetMissionPassed(i + 1) == "Complete")
			{
				playButtons[i].SetActive(false);

				if (GameConstants.GetMissionClaim(i + 1) == "Claimed")
				{
					claimButtons[i].SetActive(false);

				}


			}

		}
	}


    public void SetMission(int i)
	{
		GameConstants.SetMissionNumber(i);

		switch (GameConstants.GetMissionNumber())
		{
			case 1:
				GameConstants.SetMissionMode(1);
				break;
			case 2:
				GameConstants.SetMissionMode(2);
				break;
			case 3:
				GameConstants.SetMissionMode(3);
				break;
			case 4:
				GameConstants.SetMissionMode(4);
				break;
			case 5:
				GameConstants.SetMissionMode(5);
				break;

		}
		GameConstants.SetGameMode("Missions");

		MenuManager.Instance.LoadLevel();
		
		MissionCloseClicked();

	}

	public void ClaimButtonClicked(int missionNumber)
    {
		//StartCoroutine(DelayClaimButtonClick(missionNumber));
		GenericVariables.AddCoins(MissionCoins[missionNumber - 1]);
		GameConstants.SetMissionClaim(missionNumber);
		claimButtons[missionNumber - 1].SetActive(false);
	}

	IEnumerator DelayClaimButtonClick(int missionNumber)
    {
		MenuManager.Instance.moveObject("coins", claimButtons[missionNumber - 1]);
		MenuManager.Instance.moveConsumable();
		claimButtons[missionNumber - 1].GetComponent<Button>().interactable = false;
		yield return new WaitForSeconds(2);
		GenericVariables.AddCoins(MissionCoins[missionNumber - 1]);
		GameConstants.SetMissionClaim(missionNumber);
		claimButtons[missionNumber - 1].SetActive(false);
	}
	public void MissionCloseClicked()
	{
		//MenuManager.Instance.UIPlayer.SetActive(true);

		MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(true); //enable spin on Ui player

		this.gameObject.SetActive(false);

	}
}
