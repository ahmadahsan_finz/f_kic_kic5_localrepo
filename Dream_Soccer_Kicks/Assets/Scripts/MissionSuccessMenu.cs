﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MissionSuccessMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public static MissionSuccessMenu instance;
    public Text CoinsText;
    [SerializeField] Button nextButton;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        AdController.instance.showNativeBannerAd();

        nextButton.interactable = true;
    }
    // Update is called once per frame
    public void MissionSuccessNextButton()                    //to go back to level menu from gameplay scene call this
    {
        if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
          //  AdController.instance.HideBannerAd();

            nextButton.interactable = false;
            Time.timeScale = 1;

            StartCoroutine(DelayMissionSuccessNextButton());
        }


    }

    IEnumerator DelayMissionSuccessNextButton()
    {
        yield return new WaitForSeconds(1f); //putting delay to load game over ad
        LevelEndCelebrationManager.instance.DisableLevelEnd();
        MenuManager.Instance.DisableAll();
        MenuManager.Instance.LevelMenuLoadBool = false;
        //    MenuManager.Instance.UIPlayer.SetActive(true);
        MenuManager.Instance.EnableMissionPopUpMenu();                         //taking to mission popup menu
        MenuManager.Instance.Loading(true); //Start Loading Screen
        MenuManager.Instance.sceneChange();                                                  //changing scene to menus
        GamePlayManager.instance.ResetScore();           //reset score //important
    }

    private void OnDisable()
    {
        AdController.instance.hideNativeBannerAd();
    }
    }

