﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingRailDummy : MonoBehaviour
{
    public GameObject MoveTowards;
    float rand;
    // Start is called before the first frame update
    void Start()
    {
    //    rand =(float) Random.Range(1, 4);
        rand = 2;
	    iTween.MoveTo(gameObject, iTween.Hash("position", MoveTowards.transform.position, "time", rand, "easetype", iTween.EaseType.linear, "loopType", "pingPong", "delay", 0.2f));

    }

}
