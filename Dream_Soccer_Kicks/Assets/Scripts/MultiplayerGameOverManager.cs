﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiplayerGameOverManager : MonoBehaviour
{
    public static MultiplayerGameOverManager instance;
    Coroutine DelayMultiplayerCompleteCoroutine;
    public bool MultiplayerWonByUser;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            //Destroy(gameObject);
        }
    }
   /*  void OnEnable()
    {
        PlaceTexturesMaterialsOnPlayers();
    }*/

  /*  public void PlaceTexturesMaterialsOnPlayers()
    {
        UIOpponentSkin.GetComponent<Renderer>().material =MutliplayerVersusScreen.instance. OpponentSkinMaterials[GameConstants.MultiplayerOpponentNumber];
        UIOpponentBody.GetComponent<Renderer>().material = MutliplayerVersusScreen.instance.OpponentBodyMaterials[GameConstants.MultiplayerOpponentNumber];

        Texture skin =CustomizationScript.instance. SkinTextures[GenericVariables.getcurrSkin()];
        Texture body = CustomizationScript.instance.BodyTextures[GenericVariables.getcurrBody()];
        Texture football = CustomizationScript.instance.FootballTextures[GenericVariables.getcurrFootball()];
        UIPlayerMultiplayerGameoverSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        UIPlayerMultiplayerGameoverBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
        UIMultiplayerGameoverFootball.GetComponent<Renderer>().material.SetTexture("_MainTex", football);
        
    }*/
    public void MultiplayerWon()
    {
      //  GamePlayManager.instance.LevelEndBool = true;
        LevelEndCelebrationManager.instance.GameWon();
        MultiplayerWonByUser = true;
        DelayMultiplayerCompleteCoroutine = StartCoroutine(MultiplayerWonScreen(6));
    }
    public void MultiplayerLost()
    {
        LevelEndCelebrationManager.instance.GameLost();
        MultiplayerWonByUser = false;

        DelayMultiplayerCompleteCoroutine = StartCoroutine(MultiplayerFailedScreen(6));
    }
    public void StopDelayMultiplayerCoroutine()
    {
        StopCoroutine(DelayMultiplayerCompleteCoroutine);
    }
    public IEnumerator MultiplayerWonScreen(int delay)
    {
        yield return new WaitForSeconds(delay);
        MenuManager.GameOverBoolVideoAd = true;

        MenuManager.Instance.AnalyticsCallingEndWin();

        MenuManager.Instance.MultiplayerPassedScreen.SetActive(true);

    }

    public IEnumerator MultiplayerFailedScreen(int delay)
    {
        yield return new WaitForSeconds(delay);
        MenuManager.GameOverBoolVideoAd = true;

        MenuManager.Instance.AnalyticsCallingEndLose();

        MenuManager.Instance.MultiplayerFailedScreen.SetActive(true);

    }
   /* public void ContinueButtonPressed()
    {
        Debug.Log("ContinueButtonPressed");
        //   popUps[3].GetComponent<Animator>().SetBool("BackPressed", true);
        //   Debug.Log("SettingsMainButton");

        //  MenuManager.Instance.DisablePopUps();
        //   DisableAll();
        LevelEndCelebrationManager.instance.DisableLevelEnd();
        MenuManager.Instance.settingBackbutton();
        MenuManager.Instance.setHUD();
        MenuManager.Instance.UIPlayer.SetActive(true);
        //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        MenuManager.Instance.setMain(true);
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
        GamePlayManager.instance.ResetScore();
        MenuManager.Instance.LevelMenuLoadBool = false;
        Time.timeScale = 1;
        MenuManager.Instance.Loading(true); //Start Loading Screen

        this.gameObject.SetActive(false);

        MenuManager.Instance.sceneChange();                                                  //changing scene to menus
                                                                                             //  StartCoroutine(DelaySceneChange());


    }*/

}