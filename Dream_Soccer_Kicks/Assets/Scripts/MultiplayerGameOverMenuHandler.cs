﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MultiplayerGameOverMenuHandler : MonoBehaviour
{
    public static MultiplayerGameOverMenuHandler instance;
    public GameObject UIOpponentSkin;
    public GameObject UIOpponentBody;
    public GameObject UIPlayerMultiplayerGameoverSkin;
    public GameObject UIPlayerMultiplayerGameoverBody;

    public GameObject UIMultiplayerGameoverFootball;

    public GameObject UIPlayer;
    public GameObject UIPlayerGK;
    public GameObject UIOpponentPlayer;
    public GameObject UIOpponentGK;
    public GameObject UIPlayerGKSkin;
    public GameObject UIPlayerGKBody;
    public GameObject UIOpponentGKSkin;
    public GameObject UIOpponentGKBody;

    [SerializeField] Text PlayerScore;
    [SerializeField] Text OpponentScoreScore;
    [SerializeField] Text OpponentPlayerNameScore;
    [SerializeField] Text[] PlayerChatText;
    [SerializeField] Text[] OpponentChatText;
    [SerializeField] GameObject PlayerChat;
    [SerializeField] GameObject OpponentPlayerChat;
    [SerializeField] GameObject RematchButton;
    [SerializeField] GameObject ContinueButton;
    [SerializeField] GameObject HandTutorial;
    [SerializeField] GameObject[] OpponentAvatarArray;
    List<GameObject> tempList;
    Coroutine OpponentLeaveCoroutine;
    Coroutine OpponentWantsRematchCoroutine;
    int RematchAvailable;
    PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();

    [Header ("GameEnd Coins")]
    [SerializeField] GameObject CoinImage;
    [SerializeField] GameObject PlayerImageForCoinsDrag;
    [SerializeField] GameObject OpponentImageForCoinsDrag;
    [SerializeField] GameObject CoinsPanel;
    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void OnEnable()
    {
        UIPlayer = MenuManager.Instance.SelectedPlayer;
        UIPlayer.transform.SetParent(this.gameObject.transform);
        UIPlayer.transform.localPosition = new Vector3(2540.7f, -2118.37f, -1416f);
        UIPlayer.transform.localScale = new Vector3(10, 10, 10);
        UIPlayer.transform.rotation = Quaternion.Euler(0, 150f, 0);
        UIPlayer.GetComponent<spinLogic>().enabled = false;

        MenuManager.Instance.SelectedGK.SetActive(true);
        UIPlayerGK = MenuManager.Instance.SelectedGK;
        UIPlayerGK.transform.SetParent(this.gameObject.transform);
        UIPlayerGK.transform.localPosition = new Vector3(2542.4f, -2117.43f, -1406.96f);
        UIPlayerGK.transform.localScale = new Vector3(10, 10, 10);
        UIPlayerGK.transform.rotation = Quaternion.Euler(0, 170f, 0);

        PlaceTexturesMaterialsOnPlayers();
        SetNameScoreDetails();
        SetOpponentAvatar();
        RematchButton.GetComponent<Button>().interactable = true;
        ContinueButton.GetComponent<Button>().interactable = true;
        CoinsPanel.SetActive(true);
        HandTutorial.SetActive(false);

        if (MultiplayerGameOverManager.instance.MultiplayerWonByUser)          //if player won than show coins going on its avatar
            {
                StartCoroutine(UserCoinsCollectShow());
            }
        else
        {
            StartCoroutine(OpponentCoinsCollectShow());                        //if opponent won than show coins going on its avatar
            

        }

        RematchAvailable = Random.Range(1, 3);

        if(RematchAvailable==2) //rematch is available
        {
            // OpponentChatText.text = "Let's Play Again!";
            OpponentChatText[0].gameObject.SetActive(true);            
            OpponentChatText[1].gameObject.SetActive(false);            
            OpponentWantsRematchCoroutine = StartCoroutine(OpponentWantsRematch(2));
         

        }
        else            //not available
        {
           
            OpponentLeaveCoroutine = StartCoroutine(OpponentleftAfterWait(5));                        //Opponent waited and left game

        }
    }

    public IEnumerator UserCoinsCollectShow()                       //if player won than show coins going on its avatar
    {
        yield return new WaitForSeconds(0.5f);
        MenuManager.Instance.moveObject("coins", CoinImage);
        moveConsumable();
        yield return new WaitForSeconds(2f);
        GenericVariables.AddCoins(GameConstants.MultiplayerWonCoins);
       // yield return new WaitForSeconds(0.2f);

    }
    public IEnumerator OpponentCoinsCollectShow()             //if opponent won than show coins going on its avatar
    {
        yield return new WaitForSeconds(0.5f);

        MenuManager.Instance.moveObject("coins", CoinImage);
        moveConsumable();
        yield return new WaitForSeconds(0f);
       // GenericVariables.AddCoins(GameConstants.MultiplayerWonCoins);
        // yield return new WaitForSeconds(0.2f);

    }
    public void moveConsumable()
    {

        InvokeRepeating("moveFunc", 0, 0.4f);
        Invoke("stopMove", 1.0f);

    }

    private void moveFunc()
    {
        GameObject g = null;
        g = Instantiate(MenuManager.Instance.coinsPrefab, MenuManager.Instance.Pin, false);
        if (MultiplayerGameOverManager.instance.MultiplayerWonByUser)       //coins drag to user panel
        {
          
            iTween.MoveTo(g, iTween.Hash("position", PlayerImageForCoinsDrag.GetComponent<RectTransform>().position, "time", MenuManager.Instance.moveTime, "easetype", MenuManager.Instance.easeType, "oncomplete", "shakeText", "oncompletetarget", this.gameObject));
        }
        else                 //coins drag to opponent panel
        {
            iTween.MoveTo(g, iTween.Hash("position", OpponentImageForCoinsDrag.GetComponent<RectTransform>().position, "time", MenuManager.Instance.moveTime, "easetype", MenuManager.Instance.easeType, "oncomplete", "shakeText", "oncompletetarget", this.gameObject));
        }

        if (tempList == null)
        {
           tempList = new List<GameObject>();
        }
        tempList.Add(g);

    }
    private void stopMove()
    {
        MultiplayerGameOverManager.instance.MultiplayerWonByUser = false;

        CancelInvoke("moveFunc");
        Invoke("DestroyObjects",1);
    }
    private void DestroyObjects()
    {
        for (int i = 0; i < tempList.Count; i++)
        {
            Destroy(tempList[i].gameObject);
        }
    }
    public void SetNameScoreDetails()       //setting names and other details 
    {
        PlayerChat.SetActive(false);
        OpponentPlayerChat.SetActive(false);

        PlayerScore.text = GameConstants.MultiplayerPlayerScore.ToString();
        OpponentScoreScore.text = GameConstants.MultiplayerOpponentScore.ToString();
        OpponentPlayerNameScore.text = GameConstants.MultiplayerOpponentName.ToString();
    }
    public void SetOpponentAvatar()          //setting names and other details for opponent
    {
        OpponentAvatarArray[GameConstants.MultiplayerOpponentAvatarNumber].SetActive(true);
    }
    public void PlaceTexturesMaterialsOnPlayers()   //Textures setup on characters
    {
        Debug.Log("PlaceTexturesMaterialsOnPlayers");
        //==============================================FOR OUR PLAYER and GOALKEEPER=======================================//

        //-------------------------- FOR OUR PLAYER------------------------------//

        Texture skin = CustomizationScript.instance.SkinTextures[GenericVariables.getcurrSkin()];
        Texture body = CustomizationScript.instance.BodyTextures[GenericVariables.getcurrBody()];
        // football = CustomizationScript.instance.FootballTextures[GenericVariables.getcurrFootball()];


        for (int i = 0; i < UIPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody.Length; i++)
        {
            UIPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody[i].GetComponent<Renderer>().material.SetTexture("_MainTex", body);

        }
        for (int a = 0; a < UIPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin.Length; a++)
        {
            UIPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin[a].GetComponent<Renderer>().material.SetTexture("_MainTex", skin);

        }
        for (int b = 0; b < UIPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; b++)
        {
            UIPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[b].SetActive(false);

        }
        UIPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[GenericVariables.getcurrSkin()].SetActive(true);

        //-------------------------- FOR OUR GK------------------------------//

        body = CustomizationScript.instance.GKBodyTextures[GameConstants.MultiplayerPlayerGKBodyNumber];
        skin = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerPlayerGKSkinNumber];

        UIPlayerGK.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", body);
        UIPlayerGK.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", skin);

        for (int i = 0; i < UIPlayerGK.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            UIPlayerGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }
        UIPlayerGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerPlayerGKSkinNumber].SetActive(true);



        //==============================================FOR OPPONENT PLAYER and GOALKEEPER=======================================//

        //===========================FOR OPPONENT PLAYERS===========//

         body = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentKitNumber];
         skin = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentSkinNumber];
      //   Texture shorts = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentShortNumber];

        for (int i = 0; i < UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody.Length; i++)
        {
            UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody[i].GetComponent<Renderer>().material.SetTexture("_MainTex", body);

        }
        //for (int i = 0; i < UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts.Length; i++)
        //{
        //    UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts[i].GetComponent<Renderer>().material.SetTexture("_MainTex", shorts);

        //}
        for (int a = 0; a < UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin.Length; a++)
        {
            UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin[a].GetComponent<Renderer>().material.SetTexture("_MainTex", skin);

        }
        for (int b = 0; b < UIOpponentPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; b++)
        {
            UIOpponentPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[b].SetActive(false);

        }
        UIOpponentPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerOpponentSkinNumber].SetActive(true);


        //===========================FOR OPPONENT GOALKEEPER===========//
    
    
        skin = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentGKSkinNumber];
        body = CustomizationScript.instance.GKBodyTextures[GameConstants.MultiplayerOpponentGKBodyNumber];

        
         UIOpponentGK.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", body);
         UIOpponentGK.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", skin);

        

        for (int i = 0; i < UIOpponentGK.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            UIOpponentGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }
        UIOpponentGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerOpponentGKSkinNumber].SetActive(true);


      

    }
    public void ContinueButtonPressed() //onclick continue button
    {
        Debug.Log("ContinueButtonPressed");

        StartCoroutine(DelayContinueButtonPressed());
    }
    IEnumerator DelayContinueButtonPressed()  //continue to the main menu
    {

        ContinueButton.GetComponent<Button>().interactable = false;
        HandTutorial.SetActive(false);

        //PlayerChatText.text = "Sorry, Can't Play Right\nNow!";

        PlayerChatText[0].gameObject.SetActive(false);
        PlayerChatText[1].gameObject.SetActive(true);

        PlayerChat.SetActive(true);
        RematchButton.GetComponent<Button>().interactable = false;
        yield return new WaitForSeconds(2f);


        MultiplayerGameOverManager.instance.MultiplayerWonByUser = false;

        LevelEndCelebrationManager.instance.DisableLevelEnd();
        MenuManager.Instance.settingBackbutton();
        //  MenuManager.Instance.setHUD();
        //  MenuManager.Instance.UIPlayer.SetActive(true);
        //  MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        MenuManager.Instance.setMain(true);
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
        GamePlayManager.instance.ResetScore();
        MenuManager.Instance.LevelMenuLoadBool = false;
        Time.timeScale = 1;


        OpponentAvatarArray[GameConstants.MultiplayerOpponentAvatarNumber].SetActive(false);


        MenuManager.Instance.Loading(true); //Start Loading Screen

        this.gameObject.SetActive(false);
        if (GameConstants.multiplayerModePlayedViaState == GameConstants.MultiplayerModePlayedVia.menus)
        {
            MenuManager.Instance.sceneChange();                                                  //changing scene to menus

        }
        else
        {
            GamePlayManager.instance.EventsUnlink();
            GameConstants.SetGameMode("Classic");

            //if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
            //{
            Debug.Log("inside DelayContinueButtonClicked RewardKick2 scene is 2 ____ 2 ");

            //    Debug.Log("level complete nexttttttttttttttttt");
            GameConstants.SetLevelNumber(GameConstants.GetLevelNumber());
            PopUpLevelDetails.LevelDetailsSetter();
           
            GamePlayManager.instance.ResetScore();

            Debug.Log("Level number is ................................. " + GameConstants.GetLevelNumber());


            //  MenuManager.Instance.DisableAll();

            MenuManager.Instance.LoadLevel();

            //}
        }
        //  StartCoroutine(DelaySceneChange());
    }

    //IEnumerator DelayContinueButtonPressed()  //continue to the main menu
    //{
    //    ContinueButton.GetComponent<Button>().interactable=false;
    //    HandTutorial.SetActive(false);

    //    //PlayerChatText.text = "Sorry, Can't Play Right\nNow!";

    //    PlayerChatText[0].gameObject.SetActive(false);
    //    PlayerChatText[1].gameObject.SetActive(true);

    //    PlayerChat.SetActive(true);
    //    RematchButton.GetComponent<Button>().interactable = false;
    //    yield return new WaitForSeconds(2f);


    //    MultiplayerGameOverManager.instance.MultiplayerWonByUser = false;

    //    LevelEndCelebrationManager.instance.DisableLevelEnd();
    //    MenuManager.Instance.settingBackbutton();

    //    //  MenuManager.Instance.setHUD();
    //    //  MenuManager.Instance.UIPlayer.SetActive(true);
    //    //  MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;

    //    // MenuManager.Instance.setMain(true);   //commented because we have disabled MainMenu //aqsa gilani

    //    GameConstants.SetLevelNumber(GenericVariables.getTotalLevelsPassed());//aqsa added 
    //    PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
    //    PopUpLevelDetails.LevelDetailsSetter();

    //    int selectedlevel = GameConstants.GetLevelNumber();

    //    GameConstants.SetGameMode("Classic");
    //    if (GameConstants.GetMode() == 10)
    //    {
    //        MenuManager.Instance.ShowTournamentPanelPopup(selectedlevel);
    //    }
    //    else
    //    {
    //        MenuManager.Instance.ShowPenaltyPanel(selectedlevel);
    //    }
    //  //  MenuManager.Instance.setLevelMenu(true); // we have to go LevelSelectMenu directly after Multiplayer gameplay  //aqsa gilani added
        

    //    MenuManager.Instance.LoadLevel();// back to current level after play multiplyer gameplay mode 

    //    MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
    //    GamePlayManager.instance.ResetScore();
    //    MenuManager.Instance.LevelMenuLoadBool = false;
    //    Time.timeScale = 1;


    //    OpponentAvatarArray[GameConstants.MultiplayerOpponentAvatarNumber].SetActive(false);
    //    MenuManager.Instance.Loading(true); //Start Loading Screen

    //    this.gameObject.SetActive(false);

    //    MenuManager.Instance.sceneChange();                                                  //changing scene to menus
    //                                                                                         //  StartCoroutine(DelaySceneChange());
    //}

    public void RematchButtonClick()            //onclick rematch
    {
        if(RematchAvailable==2) //rematch is available
        {
            if(LevelEndCelebrationManager.instance!=null)
            {
                LevelEndCelebrationManager.instance.DisableLevelEnd();
            }
            if (OpponentWantsRematchCoroutine != null)
            { 
                StopCoroutine(OpponentWantsRematchCoroutine);   //stop coroutines
            }
            if (OpponentLeaveCoroutine != null)
            {
                StopCoroutine(OpponentLeaveCoroutine);   //stop coroutines
            }
           
            StartCoroutine(DelayRematchButtonClickedRematchIsAvailable());
           
        }

        else
        {
            RematchButton.GetComponent<Button>().interactable = false;
            if (OpponentLeaveCoroutine != null)
            {
                StopCoroutine(OpponentLeaveCoroutine);   //stop coroutines
            }
            StartCoroutine(DelayRematchButtonClickedRematchIsNotAvailable());
        }
    }
    
    IEnumerator DelayRematchButtonClickedRematchIsNotAvailable() // rematch button is clicked and opponent doesnt wants a rematch 
    {
       // PlayerChatText.text = "Let's Play Again!";
        PlayerChatText[0].gameObject.SetActive(true);
        PlayerChatText[1].gameObject.SetActive(false);

        PlayerChat.SetActive(true);
        yield return new WaitForSeconds(2f);
        // OpponentChatText.text = "Sorry, Can't Play Right\nNow!";
        OpponentChatText[0].gameObject.SetActive(false);
        OpponentChatText[1].gameObject.SetActive(true);

        OpponentPlayerChat.SetActive(true);
        yield return new WaitForSeconds(1f);
        CoinsPanel.GetComponent<Animator>().Play("MultiplayerEndCoinBaseAnim2");
        yield return new WaitForSeconds(2f); 
        CoinsPanel.SetActive(false);
        HandTutorial.SetActive(true);


    }
    IEnumerator DelayRematchButtonClickedRematchIsAvailable()               // rematch button is clicked and opponent wants a rematch as well
    {
        ContinueButton.GetComponent<Button>().interactable = false;

        RematchButton.GetComponent<Button>().interactable = false;

      //  PlayerChatText.text = "Let's Play Again!";
        PlayerChatText[0].gameObject.SetActive(true);
        PlayerChatText[1].gameObject.SetActive(false);

        PlayerChat.SetActive(true);
        yield return new WaitForSeconds(1f);
       // OpponentChatText.text = "Let's Play Again!";
        OpponentChatText[0].gameObject.SetActive(true);
        OpponentChatText[1].gameObject.SetActive(false);
        OpponentPlayerChat.SetActive(true);
        yield return new WaitForSeconds(2f);

        MultiplayerMatchLogic.instance.OnStartMultiplayerMode();
        MenuManager.Instance.Loading(true); //Start Loading Screen
        Shoot.share.reset();
        // CameraManager.share.reset();
        GoalKeeper.share.reset();
        this.gameObject.SetActive(false);

        /*   yield return new WaitForSeconds(1f);
           CoinsPanel.GetComponent<Animator>().Play("MultiplayerEndCoinBaseAnim2");
           yield return new WaitForSeconds(2f);
           CoinsPanel.SetActive(false);
   */
    }
    IEnumerator OpponentleftAfterWait(int delay)                //opponent waited and left game
    {
        yield return new WaitForSeconds(delay);
      //  OpponentChatText.text = "Sorry, Can't Play Right\nNow!";
        OpponentChatText[0].gameObject.SetActive(false);
        OpponentChatText[1].gameObject.SetActive(true);
        OpponentPlayerChat.SetActive(true);
        RematchButton.GetComponent<Button>().interactable = false;
        HandTutorial.SetActive(true);

    }
    IEnumerator OpponentWantsRematch(int delay)                 //opponent offered a rematch
    {
        yield return new WaitForSeconds(delay);
        OpponentPlayerChat.SetActive(true);
        OpponentLeaveCoroutine = StartCoroutine(OpponentleftAfterWait(10));                        //Opponent will wait for 10 secs
                                                                                              
        //  RematchButton.SetActive(false);

    }


}
