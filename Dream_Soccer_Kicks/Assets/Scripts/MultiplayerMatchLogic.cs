﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MultiplayerMatchLogic : MonoBehaviour
{
    //  [SerializeField] private Text textCountGoalMe;
    //   [SerializeField] private Text textCountGoalAI;
    //   [SerializeField] private Text textLevelShootAI;
    //   [SerializeField] private Text textCountTurn;
    //    [SerializeField] private Text textResult;
    //   [SerializeField] private GameObject panelResult;
    public static MultiplayerMatchLogic instance;
    int AiShootNumber;
    int PlayerShootnumber;
    float resetShootAfter = 1.2f;

    public int maxTurn = 5;

    private int _count;
    private float _curveLevel;
    private float _difficultyShootLevel;

    private float[] _curveLevels = new float[] { 0f, 0.4f, 0.8f, 1f };
    private float[] _difficultyShootLevels = new float[] { 0f, 0.4f, 0.8f, 1f };

    private int _countGoalMe;

    public bool MultiplayerLevelEndBool;
    /*    [SerializeField] GameObject OpponentGK;
        [SerializeField] GameObject OpponentPlayer;*/
    [SerializeField] GameObject PlayerObject;
    [SerializeField] GameObject GoalKeeperObject;

    /* [SerializeField] GameObject OpponentPlayerSkin;
     [SerializeField] GameObject OpponentPlayerBody;*/
    // [SerializeField] GameObject OpponentGKBody;
    [SerializeField] Material GKBodyMaterial;
    [SerializeField] Material GKSkinMaterial;
    [SerializeField] GameObject[] PlayerBodyMaterial;
    [SerializeField] GameObject[] PlayerShortMaterial;
    [SerializeField] GameObject[] PlayerSkinMaterial;

    /*    [SerializeField] Material PlayerBodyMaterial;
        [SerializeField] Material PlayerSkinMaterial;
        [SerializeField] Material OpponentBodyMaterial;
        [SerializeField] Material OpponentSkinMaterial;

        [SerializeField] Material PlayerGkBodyMaterial;
        [SerializeField] Material PlayerGkSkinMaterial;
        [SerializeField] Material OpponentGkBodyMaterial;
        [SerializeField] Material OpponentGkSkinMaterial;*/

    Material GoalNetOrginalMaterial;
    [SerializeField] Material GoalNetTransparentMaterial;
    [SerializeField] GameObject Area_GK_Control;
    [SerializeField] GameObject GoalNet;
    [SerializeField] GameObject BackCamera;

    [SerializeField] Texture PlayerBodyTexture;
    [SerializeField] Texture PlayerShortTexture;
    [SerializeField] Texture PlayerSkinTexture;
    [SerializeField] Texture PlayerGKBodyTexture;
    [SerializeField] Texture PlayerGKSkinTexture;
    [SerializeField] Texture OpponentGKBodyTexture;
    [SerializeField] Texture OpponentGKSkinTexture;
    [SerializeField] Texture OpponentBodyTexture;
    [SerializeField] Texture OpponentShortTexture;
    [SerializeField] Texture OpponentSkinTexture;

    float timer = 10;
    bool timerRunning = false;
    public bool FirstTimeBeingGoalKeeper;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Update()
    {
        if (_count % 2 == 0 && !MultiplayerLevelEndBool && timerRunning)       // even turn is my turn to shoot
        {
            timer -= Time.deltaTime;
            //    Debug.Log("timer is " + timer);
            HudManagerMultiplayer.instance.UpdatePlayerTimerSlider(timer);
            if (timer <= 0f && !Shoot.share.BallShot)
            {
                EventShootFinish(false, Area.None);
                //  timer = 0;
            }
        }
        else if (_count % 2 != 0 && !MultiplayerLevelEndBool && timerRunning)       // odd turn is opponent turn to shoot
        {
            timer -= Time.deltaTime;
            //    Debug.Log("timer is " + timer);
            HudManagerMultiplayer.instance.UpdateOpponentTimerSlider(timer);
            /*   if (timer <= 0f && !Shoot.share.BallShot)
               {

                  // EventShootFinish(false, Area.None);
                   //  timer = 0;
               }*/
        }
    }
    public int CountGoalMe
    {
        get { return _countGoalMe; }
        set
        {
            _countGoalMe = value;
            //      textCountGoalMe.text = "" + _countGoalMe;
        }
    }


    public int CountGoalAi
    {
        get { return _countGoalAI; }
        set
        {
            _countGoalAI = value;
            //    textCountGoalAI.text = "" + _countGoalAI;
        }
    }

    private int _countGoalAI;

    private void Start()
    {
        FirstTimeBeingGoalKeeper = true;
        OnStartMultiplayerMode();
        if (GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2")
        {
            //GamePlayManager.EnvironmentNumber = 0;
            GamePlayManager.EnvironmentNumber = Random.Range(0,5);
            GamePlayManager.instance.SetEnvironment(GamePlayManager.EnvironmentNumber);



            GoalDetermine.EventFinishShoot += EventShootFinish;
        }
    }

    public void OnStartMultiplayerMode()
    {

        StartCoroutine(GamePlayManager.instance.CrowdSound());         //to set stadium sound after each match //issue was winning sound kept playing in BG after rematch

        if (GameConstants.GetGameMode() == "Multiplayer1")
        {
            
            AdController.instance.ShowBannerAd(); //Commented Banner 

            getPlayerandGKmaterials();

            Shoot.share._speedMin = 28f;              //changing for long distance shot
            Shoot.share._speedMax = 35f;

            /*   GamePlayManager.instance.GroundlinesArray[1].SetActive(false);
               GamePlayManager.instance.GroundlinesArray[0].SetActive(true);*/
            MenuManager.Instance.setSettingsButton(true);
            // HudManager.instance.DistanceText.gameObject.SetActive(false);
            HudManager.instance.FireballObject.gameObject.SetActive(false);
            HudManagerMultiplayer.instance.EnableDisableMultiplayerHud(true);

            getPlayerandOpponentTextures();

            _curveLevel = _curveLevels[0];
            _difficultyShootLevel = _difficultyShootLevels[0];


            //    GoalKeeperLevel.share.setLevel(0);
            OnChange_ShootAILevel(0f);

            OnClick_RestartGame();

            MenuManager.Instance.AnalyticsCallingLevelStart();
        }

        else if (GameConstants.GetGameMode() == "Multiplayer2")
        {
            AdController.instance.ShowBannerAd();

            getPlayerandGKmaterials();  

            BackCamera.transform.position = new Vector3(BackCamera.transform.position.x, 2, BackCamera.transform.position.z); //topping camera for penalty shoot for better visibilty of ball
         /*   GamePlayManager.instance.GroundlinesArray[1].SetActive(true);
            GamePlayManager.instance.GroundlinesArray[0].SetActive(false);*/
            MenuManager.Instance.setSettingsButton(true);
            // HudManager.instance.DistanceText.gameObject.SetActive(false);
            HudManager.instance.FireballObject.gameObject.SetActive(false);
            HudManagerMultiplayer.instance.EnableDisableMultiplayerHud(true);

            getPlayerandOpponentTextures();

            _curveLevel = _curveLevels[0];
            _difficultyShootLevel = _difficultyShootLevels[0];

            //    GoalKeeperLevel.share.setLevel(0);
            OnChange_ShootAILevel(0f);

            OnClick_RestartGame();

            MenuManager.Instance.AnalyticsCallingLevelStart();
        }

    }

    public void getPlayerandGKmaterials()   //get material of player and goalkeeper
    {
        PlayerObject = GamePlayManager.instance.Player;
        PlayerBodyMaterial = PlayerObject.GetComponent<PlayerAttributeController>().UIPlayerBody;
    //    PlayerShortMaterial = PlayerObject.GetComponent<PlayerAttributeController>().UIPlayerShorts;
        PlayerSkinMaterial = PlayerObject.GetComponent<PlayerAttributeController>().UIPlayerSkin;


        GoalKeeperObject = GamePlayManager.instance.GKPlayer;
        GKBodyMaterial = GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0];
        GKSkinMaterial = GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0];
     //   GKSkinMaterial = GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[1];
    }
    public void getPlayerandOpponentTextures()
    {
        GoalNetOrginalMaterial= GoalNet.GetComponent<Renderer>().material;

        //=====================================================FOR PLAYER=========================================//

        PlayerBodyTexture = CustomizationScript.instance.BodyTextures[GenericVariables.getcurrBody()];
       // PlayerShortTexture = CustomizationScript.instance.BodyTextures[GenericVariables.getcurrShorts()];
        PlayerSkinTexture = CustomizationScript.instance.SkinTextures[GenericVariables.getcurrSkin()];
    

        //=====================================================FOR PLAYER GK=========================================//

        PlayerGKBodyTexture = CustomizationScript.instance.GKBodyTextures[GameConstants.MultiplayerPlayerGKBodyNumber];
        PlayerGKSkinTexture = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerPlayerGKSkinNumber];


        //=====================================================FOR OpponentPlayer=========================================//

        OpponentBodyTexture = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentKitNumber];
        OpponentShortTexture = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentShortNumber];
        OpponentSkinTexture = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentSkinNumber];


        //=====================================================FOR Opponent GK=========================================//

        OpponentGKBodyTexture = CustomizationScript.instance.GKBodyTextures[GameConstants.MultiplayerOpponentGKBodyNumber];
        OpponentGKSkinTexture = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentGKSkinNumber];


      
    }
    private void OnDestroy()
    {
        GoalDetermine.EventFinishShoot -= EventShootFinish;
    }

    void NextTurn()
    {
        /*if (_count / 2 >= maxTurn)
        {
            Finalize();    
            
        }
        else
        {*/
        //    textCountTurn.text = (_count / 2 + 1).ToString();
            Reset();
            if (_count % 2 == 0)       // even turn is my turn to shoot
            {
                GoalKeeperHorizontalFly.share.IsAIControl = true;
                SwitchCamera.share.IsFront = true;
                PlayerTurnToShoot();
        }
            else        //  odd turn is AI turn to shoot
            {
                GoalKeeperHorizontalFly.share.IsAIControl = false;
                SwitchCamera.share.IsFront = false;
                OpponentTurnToShoot();
                PlayerScript.instance.ReloadShootAnim();
                RunAfter.runAfter(gameObject, DoShootAI, resetShootAfter);
            }
     //   }
    }

    public void PlayerTurnToShoot()
    {
      
        GoalNet.GetComponent<Renderer>().material = GoalNetOrginalMaterial;

        for(int i=0;i< PlayerBodyMaterial.Length;i++)
        {

             PlayerBodyMaterial[i].GetComponent<Renderer>().material.mainTexture = PlayerBodyTexture;
        } 
        //for(int i=0;i< PlayerShortMaterial.Length;i++)
        //{

        //    PlayerShortMaterial[i].GetComponent<Renderer>().material.mainTexture = PlayerShortTexture;
        //} 
        for(int i=0;i< PlayerSkinMaterial.Length;i++)
        {

            PlayerSkinMaterial[i].GetComponent<Renderer>().material.mainTexture = PlayerSkinTexture;
        }
        for (int b = 0; b < PlayerObject.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; b++)
        {
            PlayerObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[b].SetActive(false);

        }
        PlayerObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[GenericVariables.getcurrSkin()].SetActive(true);

        
        GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", OpponentGKBodyTexture);
        GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", OpponentGKSkinTexture);
//        GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[1].SetTexture("_MainTex", OpponentGKSkinTexture);


        for (int i = 0; i < GoalKeeperObject.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            GoalKeeperObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }
        GoalKeeperObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerOpponentGKSkinNumber].SetActive(true);


     /*   GamePlayManager.instance.CrowdSets[0].SetActive(true);
        GamePlayManager.instance.CrowdSets[1].SetActive(false);
        GamePlayManager.instance.CrowdSets[2].SetActive(false);
        GamePlayManager.instance.CrowdSets[3].SetActive(false);*/
    }
    public void OpponentTurnToShoot()
    {
     
        if(FirstTimeBeingGoalKeeper)
        {
            StartCoroutine(TutorialManager.instance.TutorialGoalkeeperEnabler(true));
        }

        GoalNet.GetComponent<Renderer>().material = GoalNetTransparentMaterial;

       
        for (int i = 0; i < PlayerBodyMaterial.Length; i++)
        {

            PlayerBodyMaterial[i].GetComponent<Renderer>().material.mainTexture = OpponentBodyTexture;
        }
        //for (int i = 0; i < PlayerShortMaterial.Length; i++)
        //{

        //    PlayerShortMaterial[i].GetComponent<Renderer>().material.mainTexture = OpponentShortTexture;
        //}
        for (int i = 0; i < PlayerSkinMaterial.Length; i++)
        {

            PlayerSkinMaterial[i].GetComponent<Renderer>().material.mainTexture = OpponentSkinTexture;
        }
        for (int b = 0; b < PlayerObject.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; b++)
        {
            PlayerObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[b].SetActive(false);

        }
        PlayerObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerOpponentSkinNumber].SetActive(true);


       
        GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", PlayerGKBodyTexture);

        GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", PlayerGKSkinTexture);
      //  GoalKeeperObject.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[1].SetTexture("_MainTex", PlayerGKSkinTexture);

        

        for (int i = 0; i < GoalKeeperObject.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            GoalKeeperObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }
        GoalKeeperObject.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerPlayerGKSkinNumber].SetActive(true);


       /* GamePlayManager.instance.CrowdSets[0].SetActive(true);
        GamePlayManager.instance.CrowdSets[1].SetActive(true);
        GamePlayManager.instance.CrowdSets[2].SetActive(true);
        GamePlayManager.instance.CrowdSets[3].SetActive(true);*/
    }

    



    IEnumerator Finalize()
    {
        //   textResult.text = "You Won";
        if (_countGoalMe > _countGoalAI)
        {
            MultiplayerLevelEndBool = true;
            PlayerScript.instance.LevelEndStopAnim();
            MultiplayerGameOverManager.instance.MultiplayerWon();
            yield return new WaitForSeconds(1.2f);
            PlayerTurnToShoot();
        }
        else if (_countGoalMe < _countGoalAI)
        {
           // PlayerTurnToShoot();
            MultiplayerLevelEndBool = true;
            PlayerScript.instance.LevelEndStopAnim();
            MultiplayerGameOverManager.instance.MultiplayerLost();
            //textResult.text = "You Lose";    
            yield return new WaitForSeconds(1.2f);
            PlayerTurnToShoot();
        }
        else if (_countGoalMe == _countGoalAI)
        {
            yield return new WaitForSeconds(1f);

            OnClick_RestartGame();

    //        textResult.text = "Duel";
        }

        //     panelResult.SetActive(true);
        //   textResult.enabled = true;
    }

    void DoShootAI()
    {
        PlayerScript.instance.ApplyShootAnim();
        ShootAI.shareAI.shoot(Direction.Both, _curveLevel, _difficultyShootLevel);
    }


    /// <summary>
    /// How to reset:
    /// 1/ Reset ball position
    /// 2/ Reset Wall position, this can be optional if we don't want to have wall kick in this turn
    /// 3/ Reset GoalKeeperHorizontalFly 
    /// 4/ Reset GoalKeeper
    /// 5/ Reset GoalDetermine
    /// 6/ Reset CameraManager
    /// 7/ Reset SlowMotion (optional)
    /// </summary>
    void Reset()
    {
        timer = 10;
        timerRunning = true;

        HudManagerMultiplayer.instance.UpdatePlayerTimerSlider(timer);
        HudManagerMultiplayer.instance.UpdateOpponentTimerSlider(timer);

        if (_count % 2 == 1)        // even means same turn
        {
            Shoot.share.reset(Shoot.share.BallPositionX, Shoot.share.BallPositionZ);        // reset with current ball position
        }
        else    // even means new turn, so will random new ball position
        {
            Shoot.share.reset();        // reset with new random ball position
        }

      /*  Wall.share.IsWall = ((_count / 2) % 2 != 0) ? true : false;     // each odd turn will be a wall kick
        if (Wall.share.IsWall)  // if wall kick
        {
            Wall.share.setWall(Shoot.share._ball.transform.position);   
        }
*/
        /// must have reset call
        GoalKeeperHorizontalFly.share.reset();
        GoalKeeper.share.reset();
        GoalDetermine.share.reset();
        CameraManager.share.reset();

        /// optianal reset call
        SlowMotion.share.reset();
    }

    private void EventShootFinish(bool isGoal, Area area)
    {
        /* if (isGoal)
         {
             if (_count%2 == 0)
             {
                 ++CountGoalMe;
                 HudManagerMultiplayer.instance.PlayerGoalMultiplayerUpdate(CountGoalMe);          //penalty scored display goal icon

             }
             else
             {
                 ++CountGoalAi;
                 HudManagerMultiplayer.instance.AIGoalMultiplayerUpdate(CountGoalAi);              //penalty scored display goal icon

             }
         }*/
        timerRunning = false;
        if (_count % 2 == 0)
        {
            if (isGoal)
            {
               GamePlayManager.instance. GoalNetAnimationCaller(area);
                ++CountGoalMe;
                GameConstants.MultiplayerPlayerScore = CountGoalMe;
                HudManagerMultiplayer.instance.UpdatePlayerScore();
                HudManagerMultiplayer.instance.PlayerGoalMultiplayerUpdate(PlayerShootnumber);          //penalty scored display goal icon
                GamePlayManager.instance.GoalScoreFloatingTextShow();

            }
            else
            {
                

                HudManagerMultiplayer.instance.PlayerGoalMissMultiplayerUpdate(PlayerShootnumber);          //penalty scored display goal icon

            }
            
            PlayerShootnumber++;
        }
        else
        {
            Area_GK_Control.SetActive(false);
            if (isGoal)
            {
                GamePlayManager.instance.GoalNetAnimationCaller(area);

                ++CountGoalAi;
                GameConstants.MultiplayerOpponentScore = CountGoalAi;
                HudManagerMultiplayer.instance.UpdateOpponentScore();
                HudManagerMultiplayer.instance.AIGoalMultiplayerUpdate(AiShootNumber);          //penalty scored display goal icon

            }
            else
            {
                HudManagerMultiplayer.instance.AIGoalMissMultiplayerUpdate(AiShootNumber);          //penalty scored display goal icon

            }
            AiShootNumber++;
        }
        ++_count;

        if (_count / 2 >= maxTurn)
        {
            StartCoroutine( Finalize());

        }
        else
        {
         RunAfter.runAfter(gameObject, NextTurn, resetShootAfter);

        }
    }

    public void OnChange_ShootAILevel(float val)
    {
        int level = (int)Mathf.Lerp(0, 3, val);
     //   textLevelShootAI.text = "" + level;

        _curveLevel = _curveLevels[level];
        _difficultyShootLevel = _difficultyShootLevels[level];
    }

    public void OnClick_RestartGame()
    {
        _count = 0;
        CountGoalMe = 0;
        CountGoalAi = 0;
        AiShootNumber = 0;
        PlayerShootnumber = 0;
        GameConstants.MultiplayerPlayerScore = CountGoalMe;
        GameConstants.MultiplayerOpponentScore = CountGoalAi;
        HudManagerMultiplayer.instance.UpdatePlayerScore();
        HudManagerMultiplayer.instance.UpdateOpponentScore();

        //    panelResult.SetActive(false);
        RunAfter.removeTasks(gameObject);
        NextTurn();
        MultiplayerLevelEndBool = false;
        HudManagerMultiplayer.instance.GoalMultiplayerReset();
    }
}
