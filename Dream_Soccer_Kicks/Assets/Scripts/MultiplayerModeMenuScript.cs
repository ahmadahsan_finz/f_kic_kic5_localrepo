using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiplayerModeMenuScript : MonoBehaviour
{
    [SerializeField] Scrollbar MultiplayerScrollBar;
    [SerializeField] ScrollRect MultiplayerScrollRect;
    [SerializeField] Animator MultiplayerModeMenuAnimator;
    [SerializeField] GameObject UIPlayer;

    private void OnEnable()
    {
        //   MultiplayerScrollBar.value = 1;
        //  MultiplayerScrollRect.normalizedPosition = new Vector2(1, 0);
        //  MultiplayerScrollRect.horizontalNormalizedPosition = 1;
        //MultiplayerScrollBar.value = 1;

        MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(false); //disable spin on Ui player


        MultiplayerModeMenuAnimator.enabled = true;
        Invoke("ResetScroll", 1f);
    }
    public void ResetScroll()
    {
      
       /* while (MultiplayerScrollBar.value > -0.2f)
        {
            // MultiplayerScrollRect.horizontalNormalizedPosition -= 0.05f;
            MultiplayerScrollBar.value -= 0.05f;
        }*/
        MultiplayerModeMenuAnimator.enabled = false;
      
    }
    public void OnClickMultiplayerModeCloseButton()
    {
        MenuManager.Instance.SelectedPlayer.GetComponent<spinLogic>().enabled = true;
        this.gameObject.SetActive(false);
    }
    public void OnClickMultiplayerMode(int multiplayerMode)
    {
        //multiplayer1 is freekicks
        //multiplayer2 is Penalty
        MenuManager.Instance.setPanels();
        GameConstants.SetGameMode("Multiplayer" + multiplayerMode);
       // GameConstants.SetGameMode("Multiplayer" + 1);
        /*111 if (UITutorialManager.instance.BlackShaderMultiplayerPenaltyTutorial.activeSelf)
        {
             UITutorialManager.instance.MultiplayerPenaltyTutorialEnabler(false);111*/

        //   AnalyticsManagerSoccerKicks.Instance. OnClick_MultiplayerPenaltyButton_TutorialAnalytics();             //unity analytics
        //  AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_PenaltyButton_Tutorial);

        /*  if (UITutorialManager.instance.BlackShaderMultiplayerModeMenu.activeSelf)
          {
               UITutorialManager.instance.MultiplayerModeMenuEnabler(false);
              //1 UITutorialManager.instance.MultiplayerVersusScreenEnabler(false);*/
        if (UITutorialManager.instance.BlackShaderMultiplayerVersusScreen.activeSelf)
        {
            UITutorialManager.instance.MultiplayerVersusScreenEnabler(false);

            if (multiplayerMode==1)
            {
                AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_FreekickButton_Tutorial, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);

            }
            else if(multiplayerMode == 2)
            {
            AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_PenaltyButton_Tutorial, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);

            }

        }
        else
        {
            if (multiplayerMode == 1)
            {
                AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_FreekickButton_NonTutroial, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);

            }
            else if (multiplayerMode == 2)
            {
                AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_PenaltyButton_NonTutroial, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);

            }
        }
        // AnalyticsManagerSoccerKicks.Instance.OnClick_MultiplayerPenaltyButton_NonTutorialAnalytics();       //unity analytics

        //  AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_PenaltyButton_NonTutroial);
       
        MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(true); //enable spin on Ui player
        MenuManager.Instance.MultiplayerVersusScreen.SetActive(true);
        this.gameObject.SetActive(false);
    }
}
