﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MultiplayerOfferDialog : MonoBehaviour
{
    // Start is called before the first frame update

    int multiplayerMode;
    /* public void PlayButtonClicked()
     {
         Time.timeScale = 1;
         //111 UITutorialManager.instance.MultiplayerPenaltyTutorialEnabler(true);
         UITutorialManager.instance.MultiplayerVersusScreenEnabler(true);    
            // MenuManager.Instance.setPanels();
             GameConstants.SetGameMode("Multiplayer" + 1);
             AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_FreekickButton_Tutorial, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);


         MenuManager.Instance.levelSuccessTakeToMainMenu();
         this.gameObject.SetActive(false);
         MenuManager.Instance.DisableAll();
     }
     public void CloseButtonPressed()
     {
         // MenuManager.Instance.DisableAll();
         //  MenuManager.Instance.LoadLevel();
         Time.timeScale = 1;

         this.gameObject.SetActive(false);
     }*/
    public void onCloseClick()
    {
        this.gameObject.SetActive(false);
        Time.timeScale = 1;
    }
    public void PlayButtonClicked()
    {
        Time.timeScale = 1;
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            Debug.Log("inside OnClickPlay MultiplayerOfferDialog scene is 2 ");

            GameConstants.multiplayerModePlayedViaState = GameConstants.MultiplayerModePlayedVia.gameplay;

            GamePlayManager.instance.EventsUnlink();
            MenuManager.Instance.Loading(true);
            //   MenuManager.Instance.MultiplayerVersusScreen.SetActive(true);
            GameConstants.SetGameMode("Multiplayer" + 1);
            AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_FreekickButton_NonTutroial, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);
            Debug.Log("inside OnClickPlay MultiplayerOfferDialog scene is 2 -------3  ");


            //  MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(true); //enable spin on Ui player
            MenuManager.Instance.MultiplayerVersusScreen.SetActive(true);
            //StartCoroutine(DelayLoadMultiplayerFromGameplay());

        }
        else
        {
            GameConstants.multiplayerModePlayedViaState = GameConstants.MultiplayerModePlayedVia.menus;
            //  UITutorialManager.instance.MultiplayerPenaltyTutorialEnabler(true);
            UITutorialManager.instance.MultiplayerVersusScreenEnabler(true);
            MenuManager.Instance.levelSuccessTakeToMainMenu();

        }
        this.gameObject.SetActive(false);
        MenuManager.Instance.DisableAll();

    }

   

}
