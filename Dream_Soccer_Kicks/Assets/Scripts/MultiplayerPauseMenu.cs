﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerPauseMenu : MonoBehaviour
{
    public void OnCloseButtonClick()
    {
      //  this.gameObject.SetActive(false);
        MenuManager.Instance.DisableAll();

    }
    

    public void OnHomeButtonClick()
    {
        AdController.instance.HideBannerAd();

        LevelEndCelebrationManager.instance.DisableLevelEnd();
        MenuManager.Instance.settingBackbutton();
    //    MenuManager.Instance.setHUD();
    //    MenuManager.Instance.UIPlayer.SetActive(true);
        //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        MenuManager.Instance.setMain(true);
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
        GamePlayManager.instance.ResetScore();
        MenuManager.Instance.LevelMenuLoadBool = false;
        Time.timeScale = 1;
        MenuManager.Instance.Loading(true); //Start Loading Screen
        this.gameObject.SetActive(false);
        MenuManager.Instance.sceneChange();

    }
}
