﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DanielLochner.Assets.SimpleScrollSnap;
public class MutliplayerVersusScreen : MonoBehaviour
{
    public static MutliplayerVersusScreen instance;
    public GameObject UIPlayerSkin;
    public GameObject UIPlayerBody;
    public GameObject UIFootball;
    public GameObject UIPlayer;
    public GameObject UIPlayerGK;
    public GameObject UIOpponentPlayer;
    public GameObject UIOpponentGK;

    public GameObject[] UIOpponentSkin;
    public GameObject[] UIOpponentBody;
    public Material[] OpponentBodyMaterials;
    public Material[] OpponentSkinMaterials;


    public GameObject UIPlayerGKBody;
    public GameObject UIPlayerGKSkin;
    public GameObject UIOpponentGKBody;
    public GameObject UIOpponentGKSkin;
    public Material[] GKBodyMaterials;
    public Material[] GKSkinMaterials;

    [SerializeField] Text [] ConnectionText;
    [SerializeField] Text OpponentNameText;
    [SerializeField] GameObject OpponentPlayerShadow;
    [SerializeField] GameObject OpponentGKShadow;
    [SerializeField] GameObject CoinsPanel;
    [SerializeField] Text[] TitleText;

    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        //  GameConstants.SetGameMode("Multiplayer1");
        UIPlayer = MenuManager.Instance.SelectedPlayer;
        UIPlayer.transform.SetParent(this.gameObject.transform);
        UIPlayer.transform.localPosition = new Vector3(2540.7f, -2118.37f, -1416f);
        UIPlayer.transform.localScale = new Vector3(10, 10, 10);
        UIPlayer.transform.rotation = Quaternion.Euler(0, 150f, 0);
        UIPlayer.GetComponent<spinLogic>().enabled = false;

        MenuManager.Instance.SelectedGK.SetActive(true);
        UIPlayerGK = MenuManager.Instance.SelectedGK;
        UIPlayerGK.transform.SetParent(this.gameObject.transform);
        UIPlayerGK.transform.localPosition = new Vector3(2542.4f, -2117.43f, -1406.96f);
        UIPlayerGK.transform.localScale = new Vector3(10, 10, 10);
        UIPlayerGK.transform.rotation = Quaternion.Euler(0, -170f, 0);



        if (GameConstants.GetGameMode()== "Multiplayer1")
        {
           // TitleText.text = "Free Kicks";
            TitleText[0].gameObject.SetActive(true);
            TitleText[1].gameObject.SetActive(false);
        }
        else if (GameConstants.GetGameMode() == "Multiplayer2")
        {
            //TitleText.text = "Penalty Shoots";
            TitleText[0].gameObject.SetActive(false);
            TitleText[1].gameObject.SetActive(true);
        }

        ConnectionText[0].gameObject.SetActive(false);
        ConnectionText[1].gameObject.SetActive(false);
        ConnectionText[2].gameObject.SetActive(false);

        UIOpponentPlayer.SetActive(false);
        UIOpponentGK.SetActive(false);
        OpponentPlayerShadow.SetActive(false);
        OpponentGKShadow.SetActive(false);
        OpponentNameText.text = GameConstants.MultiplayerOpponentName = " ";
        enablePlayerGK();

        StartCoroutine(DelayVerticalScroll());
        StartCoroutine(EnableOpponentPlayer());
        ChangeConnectionText(1);
        CoinsPanel.SetActive(false);

    }
  
    public void enablePlayerGK()
    {
        GameConstants.MultiplayerPlayerGKBodyNumber = Random.Range(0, 3);
        GameConstants.MultiplayerPlayerGKSkinNumber = Random.Range(0, 9);

        Texture body = CustomizationScript.instance.GKBodyTextures[GameConstants.MultiplayerPlayerGKBodyNumber];
        Texture skin = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerPlayerGKSkinNumber];

       
        UIPlayerGK.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", body);
        UIPlayerGK.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", skin);
    //    UIPlayerGK.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[1].SetTexture("_MainTex", skin);

        for (int i = 0; i < UIPlayerGK.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            UIPlayerGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }
        UIPlayerGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerPlayerGKSkinNumber].SetActive(true);
    }
    public void OpponentNameSetter()
    {
        int rand = Random.Range(200000, 400000);
        OpponentNameText.text = GameConstants.MultiplayerOpponentName = "Guest_" + rand;
      //  OpponentNameText.text = GameConstants.MultiplayerOpponentName;
    }
    IEnumerator DelayVerticalScroll()
    {
        yield return new WaitForSeconds(1f);
        SlotMachine.instance.EnableCanSpin();
        SlotMachine.instance.OnSpin();
    }
    IEnumerator DelayLoadMultiplayer()
    {
      //  MenuManager.Instance.AnalyticsCallingLevelStart();
        yield return new WaitForSeconds(3f);

        MenuManager.Instance.setPanels(); //Turn all Panels off
                                          // MenuManager.Instance.setHUDActive(); //Turn HUD on
        MenuManager.Instance.Loading(true); //Start Loading Screen
    //    MenuManager.Instance.UIPlayer.SetActive(false);

        MenuManager.Instance.setMain(false); //Turn off Main Tabs
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(true); //Back Button must be active only in gameplay
                                                                            //  SceneManager.LoadScene(LevelButtonsController.selectedlevel); //Load Selected Level
        this.gameObject.SetActive(false);
        SceneManager.LoadScene("Gameplay"); //Load Selected Level

    }

    public IEnumerator EnableOpponentPlayer()
    {
        //==============================================FOR OPPONENT PLAYER========================================//

        GameConstants.MultiplayerOpponentKitNumber = Random.Range(0, 18);
        GameConstants.MultiplayerOpponentShortNumber = Random.Range(0, 18);
        GameConstants.MultiplayerOpponentSkinNumber = Random.Range(0, 6);
        Texture body = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentKitNumber];
      //  Texture shorts = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentShortNumber];
        Texture skin = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentSkinNumber];

        for (int i = 0; i < UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody.Length; i++)
        {
            UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody[i].GetComponent<Renderer>().material.SetTexture("_MainTex", body);

        }
        //for (int i = 0; i < UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts.Length; i++)
        //{
        //    UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts[i].GetComponent<Renderer>().material.SetTexture("_MainTex", shorts);

        //}
        for (int a = 0; a < UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin.Length; a++)
        {
            UIOpponentPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin[a].GetComponent<Renderer>().material.SetTexture("_MainTex", skin);

        }
        for (int b = 0; b < UIOpponentPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; b++)
        {
            UIOpponentPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[b].SetActive(false);

        }
        UIOpponentPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerOpponentSkinNumber].SetActive(true);


        /*       for(int i=0;i< UIOpponentBody.Length;i++)
               {

                   UIOpponentBody[i].GetComponent<Renderer>().material.mainTexture = CustomizationScript.instance.BodyTextures[GameConstants.MultiplayerOpponentKitNumber];
               }
               for (int a = 0; a < UIOpponentSkin.Length; a++)
               {
                   UIOpponentSkin[a].GetComponent<Renderer>().material.mainTexture = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentSkinNumber];
               }*/

        //========================================================FOR OPPONENT GK==========================================//

        GameConstants.MultiplayerOpponentGKBodyNumber = Random.Range(0, 3);
        GameConstants.MultiplayerOpponentGKSkinNumber = Random.Range(0, 7);

         skin = CustomizationScript.instance.SkinTextures[GameConstants.MultiplayerOpponentGKSkinNumber];
         body = CustomizationScript.instance.GKBodyTextures[GameConstants.MultiplayerOpponentGKBodyNumber];

         UIOpponentGK.GetComponent<PlayerAttributeController>().UIPlayerBody[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", body);
         UIOpponentGK.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[0].SetTexture("_MainTex", skin);
      //   UIOpponentGK.GetComponent<PlayerAttributeController>().UIPlayerSkin[0].GetComponent<Renderer>().materials[1].SetTexture("_MainTex", skin);

        for (int i = 0; i < UIOpponentGK.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            UIOpponentGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }
        UIOpponentGK.GetComponent<PlayerAttributeController>().PlayerHeadArray[GameConstants.MultiplayerOpponentGKSkinNumber].SetActive(true);

        yield return new WaitForSeconds(5f);
        OpponentNameSetter();

        yield return new WaitForSeconds(1f);
        ChangeConnectionText(2);
        yield return new WaitForSeconds(1.5f);
        UIOpponentPlayer.SetActive(true);
        UIOpponentGK.SetActive(true);

        OpponentPlayerShadow.SetActive(true);
        OpponentGKShadow.SetActive(true);

        CoinsPanel.SetActive(true);             //show coins

        ChangeConnectionText(3);

        StartCoroutine(DelayLoadMultiplayer());

    }

    void ChangeConnectionText(int ConnectionTextNumber)
    {
        if (ConnectionTextNumber == 1)
        {
            // ConnectionText.text = "Finding Opponent...";
            ConnectionText[0].gameObject.SetActive(true);
        }
        else if (ConnectionTextNumber == 2)
        {
            //  ConnectionText.text = "Fetching Data...";
            ConnectionText[0].gameObject.SetActive(false);
            ConnectionText[1].gameObject.SetActive(true);

        }
        else if (ConnectionTextNumber == 3)
        {
            ConnectionText[1].gameObject.SetActive(false);
            ConnectionText[2].gameObject.SetActive(true);
            //   ConnectionText.text = "Transitioning to game...";

        }



    }
}
