﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public static PlayerScript instance;
    public GameObject TargetBall;
    public GameObject Target;
    //public static int AnimControllerNum = 4;
    public Animator anim;
    bool shootDone;

    // Start is called before the first frame update
    private void Awake()
    {
        
        if(instance==null)
        {
            instance = this;
        }
        else
        {
          //  Destroy(gameObject);
        }
    }
    void Start()
    {
       
        anim = GetComponent<Animator>();
        int rand = Random.Range(1, 4);      //random animator picker
        

        anim.runtimeAnimatorController = Resources.Load("Player" + rand) as RuntimeAnimatorController;    //load random animator for player kicks
        //anim.runtimeAnimatorController = Resources.Load("Characters" + rand) as RuntimeAnimatorController; //Gilani for characters
        /* if(AnimControllerNum<9)
           {
               AnimControllerNum++;
           }
           else
           {
               AnimControllerNum = 4;
           }
        */
        // this.transform.position = CameraManager.share._cameraMain.transform.position;
        if (TargetBall!=null)
        {
            this.transform.position = GamePlayManager.instance.TargetBall.transform.position;
            this.transform.rotation = GamePlayManager.instance.TargetBall.transform.rotation;
        }
       //transform.position = new Vector3(this.transform.position.x - 1.2f, transform.position.y, transform.position.z + 5f);
    }

    // Update is called once per frame
   public void updatePlayerPosition()
    {
        //  this.transform.position = CameraManager.share._cameraMain.transform.position;
         this.transform.position = GamePlayManager.instance.TargetBall.transform.position;
         this.transform.rotation = GamePlayManager.instance.TargetBall.transform.rotation;
         // int rand = Random.Range(1, 4);
         int rand = Random.Range(1, 4);   //random animator picker

         anim.runtimeAnimatorController = Resources.Load("Player" + rand) as RuntimeAnimatorController;    //load random animator for player kicks
        // anim.runtimeAnimatorController = Resources.Load("Characters" + rand) as RuntimeAnimatorController; //Gilani
         /* 
         if (AnimControllerNum < 9)
         {
             AnimControllerNum++;
         }
         else
         {
             AnimControllerNum = 4;
         }
         */

        // Shoot.share.enableTouch();
        // Quaternion rotationLook = Quaternion.LookRotation(Target.transform.);
        // transform.position = new Vector3(this.transform.position.x - 1.2f, transform.position.y, transform.position.z - 3f);
        // transform.position = new Vector3(this.transform.position.x - 1.2f, transform.position.y, transform.position.z + 5f);

    }

    public void ApplyShootAnim()
    {
        if (!shootDone)
        {
            shootDone = true;
            anim.SetBool("Shoot", true);
            anim.SetBool("NextTurn", false);

        }
    } 
    public void LevelEndStopAnim()
    {
     
        shootDone = false;
        anim.SetBool("NextTurn", false);
    } 
 
    public void ReloadShootAnim()
    {
        shootDone = false;

        anim.SetBool("Shoot", false);
        anim.SetBool("NextTurn", true);
    }
}
