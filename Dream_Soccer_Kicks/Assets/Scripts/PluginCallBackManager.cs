﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Unity.Notifications;
public class PluginCallBackManager : MonoBehaviour
{   //personalized 
	public delegate void fireRewardAdComplete();
	public delegate void Score2xRewardAdComplete();
	public delegate void SkipLevelRewardAdComplete();
	public delegate void ContinueRewardAdComplete();
	public delegate void ConstumeRewardAdComplete();
	public delegate void SkipLevelRewardAdCompleteInGameplay();
	public delegate void FireballRewardAdComplete();
	public delegate void CustomizationUnlockRewardAdComplete();
	public delegate void ChestBoxKeys3xRewardAdComplete();
	public delegate void RewardKickRewardAdComplete();
	public delegate void RewardNotEnoughCoins();
	public delegate void FortuneWheelRewardAdComplete();

	public static event fireRewardAdComplete fireRewardAdComplete_;
	public static event Score2xRewardAdComplete Score2xRewardAdComplete_;
	public static event SkipLevelRewardAdComplete SkipLevelRewardAdComplete_;
	public static event ContinueRewardAdComplete ContinueRewardAdComplete_;
	public static event ConstumeRewardAdComplete CostumeRewardAdComplete_;
	public static event SkipLevelRewardAdCompleteInGameplay SkipLevelRewardAdCompleteInGameplay_;
	public static event FireballRewardAdComplete FireballRewardAdComplete_;
	public static event CustomizationUnlockRewardAdComplete CustomizationUnlockRewardAdComplete_;
	public static event ChestBoxKeys3xRewardAdComplete ChestBoxKeys3xRewardAdComplete_;
	public static event RewardKickRewardAdComplete RewardKickRewardAdComplete_;
	public static event RewardNotEnoughCoins RewardNotEnoughCoins_;
	public static event FortuneWheelRewardAdComplete FortuneWheelRewardAdComplete_;


	// Use this for initialization

	public static PluginCallBackManager instance;

    //public GameObject noVideoDialog;
    private void Awake()
    {
		DontDestroyOnLoad(this);

		if (instance==null)
        {
			instance = this;
        }
        else
        {
			//Destroy(gameObject);
        }
    }

    int val = 20;
	// Code ----------------------------------------
	public string[] NotificationTexts =
	{
		"Play the New Multiplayer and show the World you are the best!",
		"Harder Than you think!",
		"Can you bend it to the top corner?",
		"Be the best Scorer!",
		"Your Reward Kick is Ready, Claim now!",
		"Spin Wheel is Ready, Spin and Win now!",
		"A Energy Bottle received! Continue your Career.",
	};
	private void Start()
	{

		string temp = NotificationTexts[Random.Range(0, 4)];
		//Daily Notifications
		int minutes = 1440; // minutes in day are 1440
		GleyNotifications.Initialize();
		GleyNotifications.SendNotification("Soccer Kicks Strike", temp, new System.TimeSpan(0, minutes, 0), null, null, "Opened from Gley Notification");

	}
	void OnEnable()
	{

		AdController.gaveRewardMethod += GaveReward;
		AdController.noRewardedVideoMethod += NoRewardedVideo; //AdController.noVideoDialogMethod += noVideoDialog;
		AdController.loadNextScene += LoadScene;
		AdController.reviewDialogMethod += promptReviewDialog;

	}

	void OnDisable()
	{
		AdController.gaveRewardMethod -= GaveReward;
		AdController.noRewardedVideoMethod -= NoRewardedVideo;  //    AdController.noVideoDialogMethod -= noVideoDialog;
		AdController.loadNextScene -= LoadScene;
		AdController.reviewDialogMethod -= promptReviewDialog;

	}

	#region Reward
	// Dev Side Workings: Add Logic




	public void GaveReward()
	{
		// will ad gave reward logic here.

		Debug.Log("Gave Rewards");

		if(GenericVariables.RewardAdIndex==0)
        {
			fireRewardAdComplete_();

		}
		else if(GenericVariables.RewardAdIndex==1)
        {
			Score2xRewardAdComplete_();

		}
		else if(GenericVariables.RewardAdIndex==2)
        {
			SkipLevelRewardAdComplete_();

		}
		else if(GenericVariables.RewardAdIndex==3)
        {
			ContinueRewardAdComplete_();

		}
		else if(GenericVariables.RewardAdIndex==4)
        {
			CostumeRewardAdComplete_();

		}
		else if(GenericVariables.RewardAdIndex==5)
        {
			SkipLevelRewardAdCompleteInGameplay_();

		}
		else if(GenericVariables.RewardAdIndex==6)
        {
			FireballRewardAdComplete_();

		}
		else if(GenericVariables.RewardAdIndex==7)
        {
			CustomizationUnlockRewardAdComplete_();

		}
		else if (GenericVariables.RewardAdIndex == 8)
		{
			ChestBoxKeys3xRewardAdComplete_();

		}
		else if (GenericVariables.RewardAdIndex == 9)
		{
			RewardKickRewardAdComplete_();

		}
		else if (GenericVariables.RewardAdIndex == 10)
		{
			RewardNotEnoughCoins_();

		}
		else if (GenericVariables.RewardAdIndex == 11)
		{
			FortuneWheelRewardAdComplete_();

		}
		//val += 20;

		//reward.text = "reward: " + val;

		//if (AdConstants.sawRewardedAd)
		//{

		//	AdConstants.sawRewardedAd = false;

		//}
	}
	public void promptReviewDialog()
	{
		Debug.Log("Rate event called");
		Instantiate(Resources.Load("NativeAndroidRateDialogMenu"));

	}
	public void NoRewardedVideo()
	{
		// will ad gave reward logic here.

		Debug.Log("No Rewards");
		MenuManager.Instance.NoRewardAdDialog.SetActive(true);

	}

	public void LoadScene()
	{
		//Debug.Log("Scene Loading");
		SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);
	}

	#endregion
	// Code ----------------------------------------

}
