﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpLevelDetailsSetter : MonoBehaviour
{
    private static PopUpLevelDetailsSetter sInstance = null;
    public int[] TargetsArray = new int[3];
    public int[] CoinsArray = new int[3];
    public bool timerbool;
  /*  public int[,] LevelScores = new int[,] { 
        { 1, 1, 1 },
        { 2, 2, 4 },
        { 5, 0, 9 } };*/

    string LevelName;
    public static PopUpLevelDetailsSetter instance
    {
        get
        {
            return sInstance;
        }
    }

    private void Awake()
    {

        if (sInstance == null)
        {
            sInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    /*  mode 1= penalties																						//ahmad
        mode 2= simple freekicks
        mode 3= freekicks with dartboard only/bullseye
        mode 4= freekicks with wall
        mode 5= freekicks with wall  and dartboard/bullseye
        mode 6= freekicks with boxes
        mode 7= Goal Spot
        mode 8= Hit the pole 
        mode 9= freekicks with hoops/2x 3x circles
        mode 10= Tournament
        mode 11= freekicks with wall and hoops/2x 3x circles
        mode 12- Freekicks with dummy wall
        mode 13= freekicks with dummy wall and bullseye
        mode 14= freekicks with wood logs
        mode 15: freekicks with RoadblockA
        mode 16: freekicks with RoadblockB and moving defender
        mode 17: freekicks with Hoops
        mode 18: freekicks with Drums
        mode 19: freekicks with two Moving Defender
        mode 20: freekicks with pillars in goal and moving dummy no GK
        mode 21: Freekick directional pillars  and GK
        mode 22: Cones and wooden barrels
        mode 23: Freekick with One Static Dummy
        mode 24: Freekick with One Static Dummy and one moving dummy
        mode 25: Freekick with pillars in goal no GK
        mode 26: Freekick with directional pillars and no GK
        mode 27: Freekick with multiple Dartboards
        mode 28: Freekick with single Dartboard and moving dummy
        mode 29: Freekick with single Goal through Circle  No GK
        mode 30: Freekick with Single Goal through Circle with GK
        mode 31: Freekick with Single Goal through Circle with GK + Moving dummy
        mode 32: Freekick with two Goal through Circle No GK
        mode 33: Freekick with two Goal through Circle + GK 
        mode 34: Freekick with two Goal through Circle + GK + Moving Dummy 
        mode 35: Freekicks with cones variation 
        mode 36: Freekicks with cones variation and moving dummy
        mode 37: Freekicks with cones variation + GK
        mode 38: Freekicks with cones variation and moving dummy + GK
        mode 39: Freekicks with two Human Defenders + GK
        mode 40: Freekicks with Pillars Simple + No GK
        mode 41: Freekicks with DummyDef and Dartboard + No GK
        mode 42: Freekicks with Moving dartboard + GK (BOSS)
        mode 43: Freekicks with Coins + No GK (BONUS LEVEL) (Not Using)
        mode 44: Freekicks with Coins + No GK (BONUS LEVEL)
        mode 45: Freekicks with Trampoline + No GK + no def (BOSS)
        mode 46: Freekicks with Two Moving Rail Defender + GK (BOSS)
        mode 47: Freekicks with Trampoline + No GK + def (BOSS)

    */
    public string LevelTypeName()
    {
       
        if (GameConstants.GetMode() == 1)
            LevelName = "PENALTY";
        else if (GameConstants.GetMode() == 2 || GameConstants.GetMode() == 4)
            LevelName = "FREEKICKS";
        else if (GameConstants.GetMode() == 3|| GameConstants.GetMode() == 5 || GameConstants.GetMode() == 13)
            LevelName = "BULLSEYE";
        else if (GameConstants.GetMode() == 6)
            LevelName = "HIT THE BOXES";
        else if (GameConstants.GetMode() == 7)
            LevelName = "GOALSPOT";
        else if (GameConstants.GetMode() == 8)
            LevelName = "HIT THE POLE";
        else if (GameConstants.GetMode() == 9 || GameConstants.GetMode() == 11)
            LevelName = "CIRCLES";
        else if (GameConstants.GetMode() == 10)
            LevelName = "TOURNAMENT";
        else if (GameConstants.GetMode() == 12|| GameConstants.GetMode() == 14|| GameConstants.GetMode() == 15|| GameConstants.GetMode() == 16)
            LevelName = "HURDLES";
        else if (GameConstants.GetMode() == 17)
            LevelName = "HOOPS";
        else if (GameConstants.GetMode() == 18)
            LevelName = "HIT THE DRUMS";
  
        return LevelName;

    }
    public void LevelDetailsSetter()                 // for all the information related to levels will set here 
    {
        switch (GameConstants.GetLevelNumber())
        {
            case 0:                                                             //penalties      //SwipeTutorial                      
                Debug.Log("level 0 Bonus");
                GameConstants.SetMode(44);
                TargetsArray[0] = 20;               //scores
                TargetsArray[1] = 125;
             //   TargetsArray[2] = 180;
                TargetsArray[2] = 200;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 200;          //coins
                CoinsArray[1] = 250;
                CoinsArray[2] = 300;
                /*CoinsArray[0] = 100;          //coins
                CoinsArray[1] = 110;
                CoinsArray[2] = 120;*/
                GameConstants.SetTargetStars(CoinsArray);
             
                break;

            case 1:                                                             //penalties      //SwipeTutorial                      
                Debug.Log("level 1");
				GameConstants.SetMode(1);
                TargetsArray[0] = 75;               //scores
                TargetsArray[1] = 125;
                TargetsArray[2] = 200;
             //   TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 30;          //coins
                CoinsArray[1] = 40;
                CoinsArray[2] = 50;
                GameConstants.SetTargetStars(CoinsArray);
                break;
   
            case 2:                                                             //freekicks     //FireTutorial       
                Debug.Log("level 2");
             
				GameConstants.SetMode(2);
                TargetsArray[0] = 50;
                TargetsArray[1] = 125;
                TargetsArray[2] = 150; 

               /* TargetsArray[0] = 50;
                TargetsArray[1] = 125;
                TargetsArray[2] = 225;*/
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 50;
                CoinsArray[1] = 60;
                CoinsArray[2] = 70; 
                
             /*   CoinsArray[0] = LevelScores[0,0];
                CoinsArray[1] = LevelScores[0, 1];
                CoinsArray[2] = LevelScores[0, 2];*/
                GameConstants.SetTargetStars(CoinsArray);

                break;

            case 3:                                                              //Freekick with One Static Dummy
                Debug.Log("level 3");
              
				GameConstants.SetMode(39);
                /*TargetsArray[0] = 40;
                TargetsArray[1] = 80;
                TargetsArray[2] = 150;*/
               
                
                TargetsArray[0] = 75;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
             
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 80;
                CoinsArray[1] = 90;
                CoinsArray[2] = 100;
                GameConstants.SetTargetStars(CoinsArray);
          

                break;
            case 4:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 4");
               
                GameConstants.SetMode(24);
           /*     TargetsArray[0] = 60;
                TargetsArray[1] = 90;
                TargetsArray[2] = 120;*/
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 200;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 110;
                CoinsArray[1] = 120;
                CoinsArray[2] = 130;
                GameConstants.SetTargetStars(CoinsArray); 
                
                break;
       
            case 5:                                                              //Freekick with one moving dummy
                Debug.Log("level 5");
             //   GameConstants.SetMode(1);
                GameConstants.SetMode(49);
                TargetsArray[0] = 100;
                TargetsArray[1] = 175;
                TargetsArray[2] = 250;
                /*  TargetsArray[0] = 125;
                TargetsArray[1] = 225;
                TargetsArray[2] = 325;*/

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 140;
                CoinsArray[1] = 150;
                CoinsArray[2] = 160;
                GameConstants.SetTargetStars(CoinsArray);

        
                break;
            case 6:                                                              //Freekick with pillars in goal
                Debug.Log("level 6");
               
				GameConstants.SetMode(29);
			//	GameConstants.SetMode(39);
		
                TargetsArray[0] = 90;
                TargetsArray[1] = 150;
                TargetsArray[2] = 280;
          /*      TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
          */
                GameConstants.SetTargetScores(TargetsArray);
              
                CoinsArray[0] = 170;
                CoinsArray[1] = 180;
                CoinsArray[2] = 190;
                GameConstants.SetTargetStars(CoinsArray);
                
                break;
            case 7:                                                                //freekicks with pillars in goal and moving dummy
                Debug.Log("level 7");

				GameConstants.SetMode(41);
				//GameConstants.SetMode(40);
                TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                /*      TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;*/
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 200;
                CoinsArray[1] = 210;
                CoinsArray[2] = 220;
                GameConstants.SetTargetStars(CoinsArray);
        
                break;
            case 8:                                                               //Freekick with directional pillars 
                Debug.Log("level 8");

                GameConstants.SetMode(48);
                //   GameConstants.SetMode(42);
                TargetsArray[0] = 125;
                TargetsArray[1] = 180;
                TargetsArray[2] = 280;
                /*  TargetsArray[0] = 175;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;*/
                GameConstants.SetTargetScores(TargetsArray);
            
                CoinsArray[0] = 230;
                CoinsArray[1] = 240;
                CoinsArray[2] = 250;
                GameConstants.SetTargetStars(CoinsArray);
                
                
                break;
            case 9:                                                               //Freekick directional pillars  and GK
                Debug.Log("level 9");
                GameConstants.SetMode(42);

              //    GameConstants.SetMode(25);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
                /*  TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;*/
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 260;
                CoinsArray[1] = 270;
                CoinsArray[2] = 280;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 10:                                                               // Freekick with multiple Dartboards
                Debug.Log("level 10");

                GameConstants.SetMode(45);
             //   GameConstants.SetMode(42);
        
                TargetsArray[0] = 80;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                
                /*  TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;*/
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);
             
                CoinsArray[0] = 290;
                CoinsArray[1] = 300;
                CoinsArray[2] = 310;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 11:                                                               //Freekicks with single dartboard and GK
                Debug.Log("level 11");
      
               GameConstants.SetMode(8);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);
             
                CoinsArray[0] = 320;
                CoinsArray[1] = 330;
                CoinsArray[2] = 340;
                GameConstants.SetTargetStars(CoinsArray);
              
                break;
            case 12:                                                               //tournament
                Debug.Log("level 12 Tournament");
				GameConstants.SetMode(10);
                CoinsArray[0] = 500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 13:                                                               //Freekick with single Dartboard and moving dummy
                Debug.Log("level 13");
             
                GameConstants.SetMode(7);
           //     GameConstants.SetMode(45);
         
                   TargetsArray[0] = 100;
                   TargetsArray[1] = 150;
                   TargetsArray[2] = 300;
                   //TargetsArray[2] = 300;
                   timerbool = true;                   //level has timer 
                   GameConstants.timer = 60f;
                   GameConstants.SetTargetScores(TargetsArray);

                   CoinsArray[0] = 350;
                   CoinsArray[1] = 360;
                   CoinsArray[2] = 370;
                   GameConstants.SetTargetStars(CoinsArray);
              
                break;
            case 14:                                                               //Bullseye with dummy Defenders
                Debug.Log("level 14");
               
				GameConstants.SetMode(6);
			//	GameConstants.SetMode(30);
		
                TargetsArray[0] = 250;
                TargetsArray[1] = 400;
                TargetsArray[2] = 550;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
             
                CoinsArray[0] = 380;
                CoinsArray[1] = 390;
                CoinsArray[2] = 400;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 15:                                                               //Goal through Circles
                Debug.Log("level 15");
                GameConstants.SetMode(46);
             //   GameConstants.SetMode(19);
     
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 225;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 410;
                CoinsArray[1] = 420;
                CoinsArray[2] = 430;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 16:                                                                //Goal through Circles + GK
                Debug.Log("level 16");
               
				GameConstants.SetMode(30);
			//	GameConstants.SetMode(46);
			    TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);
          
                CoinsArray[0] = 440;
                CoinsArray[1] = 450;
                CoinsArray[2] = 460;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 17:                                                               //Goal through Circles + GK + moving dummy
                Debug.Log("level 17");
               
				GameConstants.SetMode(4);
		//		GameConstants.SetMode(7);
		        TargetsArray[0] = 125;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 470;
                CoinsArray[1] = 480;
                CoinsArray[2] = 490;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 18:                                                                ///Goal through  two Circles
                Debug.Log("level 18");

                GameConstants.SetMode(40);  
            //    GameConstants.SetMode(25);  
           //     GameConstants.SetMode(32);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);
        
                CoinsArray[0] = 500;
                CoinsArray[1] = 510;
                CoinsArray[2] = 520;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 19:                                                               //Goal through  two Circles +GK
                Debug.Log("level 19");

				GameConstants.SetMode(32);
			//	GameConstants.SetMode(33);
                TargetsArray[0] = 50;
                TargetsArray[1] = 100;
                TargetsArray[2] = 125;
                GameConstants.SetTargetScores(TargetsArray);
           
                CoinsArray[0] = 530;    
                CoinsArray[1] = 540;
                CoinsArray[2] = 550;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 20:                                                               //Goal through  two Circles + GK + moving dummy
                Debug.Log("level 20");
                
				GameConstants.SetMode(9);
		//		GameConstants.SetMode(6);

                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 450;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 560;
                CoinsArray[1] = 570;
                CoinsArray[2] = 580;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 21:
                Debug.Log("level 21");                                       // cones  

			
				GameConstants.SetMode(19);
		//		GameConstants.SetMode(46);
	//			GameConstants.SetMode(9);

                TargetsArray[0] = 200;
                TargetsArray[1] = 300;
                TargetsArray[2] = 450;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 590;
                CoinsArray[1] = 600;
                CoinsArray[2] = 610;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 22:
                Debug.Log("level 22");
                
			//	GameConstants.SetMode(36);                          // cones + moving dummy
				GameConstants.SetMode(35);                        

                //	GameConstants.SetMode(9);
                TargetsArray[0] = 275;
                TargetsArray[1] = 375;
                TargetsArray[2] = 475;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 37f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 620;
                CoinsArray[1] = 630;
                CoinsArray[2] = 640;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 23:
                Debug.Log("level 23");
            
				GameConstants.SetMode(28);                       
		//		GameConstants.SetMode(41);                       
		
                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 650;
                CoinsArray[1] = 660;
                CoinsArray[2] = 670;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 24:
                Debug.Log("level 24 Tournament");
				GameConstants.SetMode(10);
                CoinsArray[0] = 1000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 25:
                Debug.Log("level 25");
               
				GameConstants.SetMode(36);                 
			//	GameConstants.SetMode(38);                  // cones + GK + moving dummy
               //GameConstants.SetMode(7);
                TargetsArray[0] = 275;
                TargetsArray[1] = 350;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 680;
                CoinsArray[1] = 690;
                CoinsArray[2] = 700;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 26:
                Debug.Log("level 26");
               
				GameConstants.SetMode(23);
			//	GameConstants.SetMode(5);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;
          
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 710;
                CoinsArray[1] = 720;
                CoinsArray[2] = 730;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 27:
                Debug.Log("level 27");
               
				GameConstants.SetMode(31);
		//		GameConstants.SetMode(12);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 740;
                CoinsArray[1] = 750;
                CoinsArray[2] = 760;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 28:
                Debug.Log("level 28");
             
				GameConstants.SetMode(18);
			//	GameConstants.SetMode(6);
                TargetsArray[0] = 250;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1000;
                //   timerbool = true;                   //level has timer 
                //  GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 770;
                CoinsArray[1] = 780;
                CoinsArray[2] = 790;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 29:
                Debug.Log("level 29");
               
				GameConstants.SetMode(33);
			//	GameConstants.SetMode(14);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 800;
                CoinsArray[1] = 810;
                CoinsArray[2] = 820;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 30:
                Debug.Log("level 30");
              
				GameConstants.SetMode(47);
			
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 830;
                CoinsArray[1] = 840;
                CoinsArray[2] = 850;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 31:
                Debug.Log("level 31");
            
				GameConstants.SetMode(11);
			//	GameConstants.SetMode(27);
                TargetsArray[0] = 200;
                TargetsArray[1] = 350;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 860;
                CoinsArray[1] = 870;
                CoinsArray[2] = 880;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 32:
                Debug.Log("level 32");
                
				GameConstants.SetMode(34);
	//			GameConstants.SetMode(18);
                TargetsArray[0] = 250;
                TargetsArray[1] = 325;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
           
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 890;
                CoinsArray[1] = 900;
                CoinsArray[2] = 910;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 33:
                Debug.Log("level 33");
               
				GameConstants.SetMode(13);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;
        
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 920;
                CoinsArray[1] = 930;
                CoinsArray[2] = 940;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 34:
                Debug.Log("level 34");
              
				GameConstants.SetMode(37);
			//	GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 300;
                TargetsArray[2] = 375;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
            
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 950;
                CoinsArray[1] = 960;
                CoinsArray[2] = 970;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 35:
                Debug.Log("level 35");
              
				GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
             
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 980;
                CoinsArray[1] = 990;
                CoinsArray[2] = 1000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 36:
                Debug.Log("level 36 Tournament");
            
				GameConstants.SetMode(10);
                CoinsArray[0] = 1500;
                GameConstants.SetTargetStars(CoinsArray);
                //	GameConstants.SetMode(12);
                /*   TargetsArray[0] = 225;
                   TargetsArray[1] = 275;
                   TargetsArray[2] = 325;
                   //timerbool = true;                   //level has timer 
                   // GameConstants.timer = 23f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 50;
                   CoinsArray[1] = 100;
                   CoinsArray[2] = 150;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 37:
                Debug.Log("level 37");
             
				GameConstants.SetMode(38);
		//		GameConstants.SetMode(13);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 400;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;
             
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1010;
                CoinsArray[1] = 1020;
                CoinsArray[2] = 1030;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 38:
                Debug.Log("level 38");
				GameConstants.SetMode(27);
			//	GameConstants.SetMode(8);
			//	GameConstants.SetMode(14);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 450;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;
          
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1040;
                CoinsArray[1] = 1050;
                CoinsArray[2] = 1060;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 39:
                Debug.Log("level 39");

				GameConstants.SetMode(15);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;
             
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1070;
                CoinsArray[1] = 1080;
                CoinsArray[2] = 1090;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 40:
                Debug.Log("level 40");
      
				GameConstants.SetMode(20);
//				GameConstants.SetMode(16);
                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;
         
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1100;
                CoinsArray[1] = 1110;
                CoinsArray[2] = 1120;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 41:
                Debug.Log("level 41");
               
				GameConstants.SetMode(21);
			//	GameConstants.SetMode(17);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;
            
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1130;
                CoinsArray[1] = 1140;
                CoinsArray[2] = 1150;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 42:
                Debug.Log("level 42");
       
				GameConstants.SetMode(18);
                TargetsArray[0] = 225;
                TargetsArray[1] = 350;
                TargetsArray[2] = 600;
                
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1160;
                CoinsArray[1] = 1170;
                CoinsArray[2] = 1180;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 43:
                Debug.Log("level 43");
       
				GameConstants.SetMode(19);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 450;
                
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1190;
                CoinsArray[1] = 1200;
                CoinsArray[2] = 1210;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 44:
                Debug.Log("level 44");

                GameConstants.SetMode(7);
                TargetsArray[0] = 400;
                TargetsArray[1] = 550;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1220;
                CoinsArray[1] = 1230;
                CoinsArray[2] = 1240;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 45:
                Debug.Log("level 45");

            //    GameConstants.SetMode(7);
                GameConstants.SetMode(8);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1250;
                CoinsArray[1] = 1260;
                CoinsArray[2] = 1270;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 46:
                Debug.Log("level 46");

                GameConstants.SetMode(9);
                TargetsArray[0] = 250;
                TargetsArray[1] = 400;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1280;
                CoinsArray[1] = 1290;
                CoinsArray[2] = 1300;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 47:
                Debug.Log("level 47");

                GameConstants.SetMode(12);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1310;
                CoinsArray[1] = 1320;
                CoinsArray[2] = 1330;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 48:
                Debug.Log("level 48 Tournament");

                GameConstants.SetMode(10);
                CoinsArray[0] = 2000;
                GameConstants.SetTargetStars(CoinsArray);

             /*   //       GameConstants.SetMode(4);
                TargetsArray[0] = 150;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1340;
                CoinsArray[1] = 1350;
                CoinsArray[2] = 1360;
                GameConstants.SetTargetStars(CoinsArray);*/
                break;

            case 49:
                Debug.Log("level 49");

                GameConstants.SetMode(5);
                TargetsArray[0] = 125;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1370;
                CoinsArray[1] = 1380;
                CoinsArray[2] = 1390;
                GameConstants.SetTargetStars(CoinsArray);
                break;

            case 50:
                Debug.Log("level 50");

                GameConstants.SetMode(15);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1400;
                CoinsArray[1] = 1410;
                CoinsArray[2] = 1420;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 51:
                Debug.Log("level 51");

                GameConstants.SetMode(16);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1430;
                CoinsArray[1] = 1440;
                CoinsArray[2] = 1450;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 52:
                Debug.Log("level 52");

                GameConstants.SetMode(3);
            //    GameConstants.SetMode(19);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1460;
                CoinsArray[1] = 1470;
                CoinsArray[2] = 1480;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 53:
                Debug.Log("level 53");

                GameConstants.SetMode(19);
                TargetsArray[0] = 250;
                TargetsArray[1] = 350;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1490;
                CoinsArray[1] = 1500;
                CoinsArray[2] = 1510;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 54:
                Debug.Log("level 54");

                GameConstants.SetMode(1);
                TargetsArray[0] = 125;
                TargetsArray[1] = 225;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1520;
                CoinsArray[1] = 1530;
                CoinsArray[2] = 1540;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 55:
                Debug.Log("level 55");

                GameConstants.SetMode(22);
                TargetsArray[0] = 200;
                TargetsArray[1] = 310;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1550;
                CoinsArray[1] = 1560;
                CoinsArray[2] = 1570;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 56:
                Debug.Log("level 56");

                GameConstants.SetMode(38);
        //        GameConstants.SetMode(23);
                TargetsArray[0] = 250;
                TargetsArray[1] = 350;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1580;
                CoinsArray[1] = 1590;
                CoinsArray[2] = 1600;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 57:
                Debug.Log("level 57");

                GameConstants.SetMode(21);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1610;
                CoinsArray[1] = 1620;
                CoinsArray[2] = 1630;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 58:
                Debug.Log("level 58");

                GameConstants.SetMode(39);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 500;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1640;
                CoinsArray[1] = 1650;
                CoinsArray[2] = 1670;
                GameConstants.SetTargetStars(CoinsArray);
                break; 
            case 59:
                Debug.Log("level 59");

                GameConstants.SetMode(31);
                TargetsArray[0] = 200;
                TargetsArray[1] = 280;
                TargetsArray[2] = 380;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1680;
                CoinsArray[1] = 1690;
                CoinsArray[2] = 1700;
                GameConstants.SetTargetStars(CoinsArray);
                break; 
            case 60:
                Debug.Log("level 60 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 2500;
                GameConstants.SetTargetStars(CoinsArray);

               /* GameConstants.SetMode(34);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1710;
                CoinsArray[1] = 1720;
                CoinsArray[2] = 1730;
                GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 61:                                                              //Freekick with One Static Dummy
                Debug.Log("level 61");

                GameConstants.SetMode(16);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 200;
                TargetsArray[1] = 400;
                TargetsArray[2] = 600;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1740;
                CoinsArray[1] = 1750;
                CoinsArray[2] = 1760;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 62:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 62");

                GameConstants.SetMode(24);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1770;
                CoinsArray[1] = 1780;
                CoinsArray[2] = 1790;
                GameConstants.SetTargetStars(CoinsArray);

                break;

            case 63:                                                              //Freekick with one moving dummy
                Debug.Log("level 63");
                GameConstants.SetMode(1);
                //         GameConstants.SetMode(3);
                TargetsArray[0] = 200;
                TargetsArray[1] = 300;
                TargetsArray[2] = 400;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 1800;
                CoinsArray[1] = 1810;
                CoinsArray[2] = 1820;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 64:                                                              //Freekick with pillars in goal
                Debug.Log("level 64");

                GameConstants.SetMode(29);
                //	GameConstants.SetMode(39);

                TargetsArray[0] = 150;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;

                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1830;
                CoinsArray[1] = 1840;
                CoinsArray[2] = 1850;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 65:                                                                //freekicks with pillars in goal and moving dummy
                Debug.Log("level 65");

                GameConstants.SetMode(41);
                //GameConstants.SetMode(40);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1860;
                CoinsArray[1] = 1870;
                CoinsArray[2] = 1880;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 66:                                                               //Freekick with directional pillars 
                Debug.Log("level 66");

                GameConstants.SetMode(42);
                //	GameConstants.SetMode(41);
                TargetsArray[0] = 175;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1890;
                CoinsArray[1] = 1900;
                CoinsArray[2] = 1910;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 67:                                                               //Freekick directional pillars  and GK
                Debug.Log("level 67");
                GameConstants.SetMode(25);
                //     GameConstants.SetMode(29);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1920;
                CoinsArray[1] = 1930;
                CoinsArray[2] = 1940;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 68:                                                               // Freekick with multiple Dartboards
                Debug.Log("level 68");

                GameConstants.SetMode(47);
                //   GameConstants.SetMode(42);

                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1950;
                CoinsArray[1] = 1960;
                CoinsArray[2] = 1970;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 69:                                                               //Freekicks with single dartboard and GK
                Debug.Log("level 69");

                GameConstants.SetMode(8);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 1980;
                CoinsArray[1] = 1990;
                CoinsArray[2] = 2000;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 70:                                                               //tournament
                Debug.Log("level 70 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 3000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 71:                                                               //Freekick with single Dartboard and moving dummy
                Debug.Log("level 71");

                GameConstants.SetMode(7);
                //     GameConstants.SetMode(45);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2010;
                CoinsArray[1] = 2020;
                CoinsArray[2] = 2030;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 72:                                                               //Bullseye with dummy Defenders
                Debug.Log("level 72");

                GameConstants.SetMode(6);
                //	GameConstants.SetMode(30);

                TargetsArray[0] = 400;
                TargetsArray[1] = 500;
                TargetsArray[2] = 700;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2040;
                CoinsArray[1] = 2050;
                CoinsArray[2] = 2060;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 73:                                                               //Goal through Circles
                Debug.Log("level 73");
                GameConstants.SetMode(46);
                //   GameConstants.SetMode(19);

                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 225;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2070;
                CoinsArray[1] = 2080;
                CoinsArray[2] = 2090;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 74:                                                                //Goal through Circles + GK
                Debug.Log("level 74");

                GameConstants.SetMode(30);
                //	GameConstants.SetMode(46);
                TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2100;
                CoinsArray[1] = 2110;
                CoinsArray[2] = 2120;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 75:                                                               //Goal through Circles + GK + moving dummy
                Debug.Log("level 75");

                GameConstants.SetMode(4);
                //		GameConstants.SetMode(7);
                TargetsArray[0] = 125;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2130;
                CoinsArray[1] = 2140;
                CoinsArray[2] = 2150;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 76:                                                                ///Goal through  two Circles
                Debug.Log("level 76");

                GameConstants.SetMode(40);
                //    GameConstants.SetMode(25);  
                //     GameConstants.SetMode(32);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2160;
                CoinsArray[1] = 2170;
                CoinsArray[2] = 2180;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 77:                                                               //Goal through  two Circles +GK
                Debug.Log("level 77");

                GameConstants.SetMode(32);
                //	GameConstants.SetMode(33);
                TargetsArray[0] = 50;
                TargetsArray[1] = 100;
                TargetsArray[2] = 125;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2190;
                CoinsArray[1] = 2200;
                CoinsArray[2] = 2210;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 78:                                                               //Goal through  two Circles + GK + moving dummy
                Debug.Log("level 78");

                GameConstants.SetMode(9);
                //		GameConstants.SetMode(6);

                TargetsArray[0] = 350;
                TargetsArray[1] = 600;
                TargetsArray[2] = 825;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2220;
                CoinsArray[1] = 2230;
                CoinsArray[2] = 2240;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 79:
                Debug.Log("level 79");                                       // cones  


                GameConstants.SetMode(19);
                //		GameConstants.SetMode(46);
                //			GameConstants.SetMode(9);

                TargetsArray[0] = 400;
                TargetsArray[1] = 600;
                TargetsArray[2] = 900;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2250;
                CoinsArray[1] = 2260;
                CoinsArray[2] = 2270;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 80:
                Debug.Log("level 80");

                //	GameConstants.SetMode(36);                          // cones + moving dummy
                GameConstants.SetMode(35);

                //	GameConstants.SetMode(9);
                TargetsArray[0] = 275;
                TargetsArray[1] = 375;
                TargetsArray[2] = 475;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 37f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2280;
                CoinsArray[1] = 2290;
                CoinsArray[2] = 2300;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 81:
                Debug.Log("level 81");

                GameConstants.SetMode(28);
                //		GameConstants.SetMode(41);                       

                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2310;
                CoinsArray[1] = 2320;
                CoinsArray[2] = 2330;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 82:
                Debug.Log("level 82 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 3500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 83:
                Debug.Log("level 83");

                GameConstants.SetMode(36);
                //	GameConstants.SetMode(38);                  // cones + GK + moving dummy
                //GameConstants.SetMode(7);
                TargetsArray[0] = 275;
                TargetsArray[1] = 350;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2340;
                CoinsArray[1] = 2350;
                CoinsArray[2] = 2360;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 84:
                Debug.Log("level 84");

                GameConstants.SetMode(23);
                //	GameConstants.SetMode(5);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2370;
                CoinsArray[1] = 2380;
                CoinsArray[2] = 2390;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 85:
                Debug.Log("level 85");

                GameConstants.SetMode(31);
                //		GameConstants.SetMode(12);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2400;
                CoinsArray[1] = 2410;
                CoinsArray[2] = 2420;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 86:
                Debug.Log("level 86");

                GameConstants.SetMode(18);
                //	GameConstants.SetMode(6);
                TargetsArray[0] = 450;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1000;
                //   timerbool = true;                   //level has timer 
                //  GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2430;
                CoinsArray[1] = 2440;
                CoinsArray[2] = 2450;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 87:
                Debug.Log("level 87");

                GameConstants.SetMode(33);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2460;
                CoinsArray[1] = 2470;
                CoinsArray[2] = 2480;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 88:
                Debug.Log("level 88");

                GameConstants.SetMode(16);
                //	GameConstants.SetMode(15);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2490;
                CoinsArray[1] = 2500;
                CoinsArray[2] = 2510;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 89:
                Debug.Log("level 89");

                GameConstants.SetMode(12);
                //	GameConstants.SetMode(27);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2520;
                CoinsArray[1] = 2530;
                CoinsArray[2] = 2540;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 90:
                Debug.Log("level 90");

                GameConstants.SetMode(34);
                //			GameConstants.SetMode(18);
                TargetsArray[0] = 250;
                TargetsArray[1] = 325;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2550;
                CoinsArray[1] = 2560;
                CoinsArray[2] = 2570;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 91:
                Debug.Log("level 91");

                GameConstants.SetMode(13);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2580;
                CoinsArray[1] = 2590;
                CoinsArray[2] = 2600;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 92:
                Debug.Log("level 92");

                GameConstants.SetMode(37);
                //	GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 300;
                TargetsArray[2] = 375;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2610;
                CoinsArray[1] = 2620;
                CoinsArray[2] = 2630;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 93:
                Debug.Log("level 93");

                GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2640;
                CoinsArray[1] = 2650;
                CoinsArray[2] = 2660;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 94:
                Debug.Log("level 94 Tournament");

                GameConstants.SetMode(10);
                CoinsArray[0] = 4000;
                GameConstants.SetTargetStars(CoinsArray);
                //	GameConstants.SetMode(12);
                /*   TargetsArray[0] = 225;
                   TargetsArray[1] = 275;
                   TargetsArray[2] = 325;
                   //timerbool = true;                   //level has timer 
                   // GameConstants.timer = 23f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 50;
                   CoinsArray[1] = 100;
                   CoinsArray[2] = 150;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 95:
                Debug.Log("level 95");

                GameConstants.SetMode(38);
                //		GameConstants.SetMode(13);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2670;
                CoinsArray[1] = 2680;
                CoinsArray[2] = 2690;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 96:
                Debug.Log("level 96");
                GameConstants.SetMode(27);
                //	GameConstants.SetMode(8);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2700;
                CoinsArray[1] = 2710;
                CoinsArray[2] = 2720;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 97:
                Debug.Log("level 97");

                GameConstants.SetMode(15);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2730;
                CoinsArray[1] = 2740;
                CoinsArray[2] = 2750;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 98:
                Debug.Log("level 98");

                GameConstants.SetMode(21);
                //				GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2760;
                CoinsArray[1] = 2770;
                CoinsArray[2] = 2780;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 99:
                Debug.Log("level 99");

                GameConstants.SetMode(20);
                //	GameConstants.SetMode(17);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2790;
                CoinsArray[1] = 2800;
                CoinsArray[2] = 2810;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 100:
                Debug.Log("level 100");

                GameConstants.SetMode(18);
                TargetsArray[0] = 500;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1050;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2820;
                CoinsArray[1] = 2830;
                CoinsArray[2] = 2840;
                GameConstants.SetTargetStars(CoinsArray);
                break;

            case 101:                                                              //Freekick with One Static Dummy
                Debug.Log("level 3");

                GameConstants.SetMode(39);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 75;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2850;
                CoinsArray[1] = 2860;
                CoinsArray[2] = 2870;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 102:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 4");

                GameConstants.SetMode(24);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 200;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2880;
                CoinsArray[1] = 2890;
                CoinsArray[2] = 2900;
                GameConstants.SetTargetStars(CoinsArray);

                break;

            case 103:                                                              //Freekick with one moving dummy
                Debug.Log("level 5");
                GameConstants.SetMode(1);
                //         GameConstants.SetMode(3);
                TargetsArray[0] = 125;
                TargetsArray[1] = 225;
                TargetsArray[2] = 325;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 2910;
                CoinsArray[1] = 2920;
                CoinsArray[2] = 2930;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 104:                                                              //Freekick with pillars in goal
                Debug.Log("level 6");

                GameConstants.SetMode(29);
                //	GameConstants.SetMode(39);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;

                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2940;
                CoinsArray[1] = 2950;
                CoinsArray[2] = 2960;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 105:                                                                //freekicks with pillars in goal and moving dummy
                Debug.Log("level 7");

                GameConstants.SetMode(41);
                //GameConstants.SetMode(40);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 2970;
                CoinsArray[1] = 2980;
                CoinsArray[2] = 2990;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 106:                                                               //Freekick with directional pillars 
                Debug.Log("level 8");

                GameConstants.SetMode(42);
                //	GameConstants.SetMode(41);
                TargetsArray[0] = 175;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3000;
                CoinsArray[1] = 3010;
                CoinsArray[2] = 3020;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 107:                                                               //Freekick directional pillars  and GK
                Debug.Log("level 9");
                GameConstants.SetMode(25);
                //     GameConstants.SetMode(29);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3030;
                CoinsArray[1] = 3040;
                CoinsArray[2] = 3050;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 108:                                                               // Freekick with multiple Dartboards
                Debug.Log("level 10");

                GameConstants.SetMode(45);
                //   GameConstants.SetMode(42);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3060;
                CoinsArray[1] = 3070;
                CoinsArray[2] = 3080;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 109:                                                               //Freekicks with single dartboard and GK
                Debug.Log("level 11");

                GameConstants.SetMode(8);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3090;
                CoinsArray[1] = 3100;
                CoinsArray[2] = 3110;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 110:                                                               //tournament
                Debug.Log("level 12 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 4500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 111:                                                               //Freekick with single Dartboard and moving dummy
                Debug.Log("level 13");

                GameConstants.SetMode(7);
                //     GameConstants.SetMode(45);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3120;
                CoinsArray[1] = 3130;
                CoinsArray[2] = 3140;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 112:                                                               //Bullseye with dummy Defenders
                Debug.Log("level 14");

                GameConstants.SetMode(6);
                //	GameConstants.SetMode(30);

                TargetsArray[0] = 250;
                TargetsArray[1] = 400;
                TargetsArray[2] = 550;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3150;
                CoinsArray[1] = 3160;
                CoinsArray[2] = 3170;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 113:                                                               //Goal through Circles
                Debug.Log("level 15");
                GameConstants.SetMode(46);
                //   GameConstants.SetMode(19);

                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 225;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3180;
                CoinsArray[1] = 3190;
                CoinsArray[2] = 3200;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 114:                                                                //Goal through Circles + GK
                Debug.Log("level 16");

                GameConstants.SetMode(30);
                //	GameConstants.SetMode(46);
                TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3210;
                CoinsArray[1] = 3220;
                CoinsArray[2] = 3230;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 115:                                                               //Goal through Circles + GK + moving dummy
                Debug.Log("level 17");

                GameConstants.SetMode(4);
                //		GameConstants.SetMode(7);
                TargetsArray[0] = 125;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3240;
                CoinsArray[1] = 3250;
                CoinsArray[2] = 3260;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 116:                                                                ///Goal through  two Circles
                Debug.Log("level 18");

                GameConstants.SetMode(40);
                //    GameConstants.SetMode(25);  
                //     GameConstants.SetMode(32);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3270;
                CoinsArray[1] = 3280;
                CoinsArray[2] = 3290;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 117:                                                               //Goal through  two Circles +GK
                Debug.Log("level 19");

                GameConstants.SetMode(32);
                //	GameConstants.SetMode(33);
                TargetsArray[0] = 50;
                TargetsArray[1] = 100;
                TargetsArray[2] = 125;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 3300;
                CoinsArray[1] = 3310;
                CoinsArray[2] = 3320;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 118:                                                               //Goal through  two Circles + GK + moving dummy
                Debug.Log("level 20");

                GameConstants.SetMode(9);
                //		GameConstants.SetMode(6);

                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 450;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3330;
                CoinsArray[1] = 3340;
                CoinsArray[2] = 3350;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 119:
                Debug.Log("level 21");                                       // cones  


                GameConstants.SetMode(19);
                //		GameConstants.SetMode(46);
                //			GameConstants.SetMode(9);

                TargetsArray[0] = 200;
                TargetsArray[1] = 300;
                TargetsArray[2] = 450;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3360;
                CoinsArray[1] = 3370;
                CoinsArray[2] = 3380;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 120:
                Debug.Log("level 22");

                //	GameConstants.SetMode(36);                          // cones + moving dummy
                GameConstants.SetMode(35);

                //	GameConstants.SetMode(9);
                TargetsArray[0] = 275;
                TargetsArray[1] = 375;
                TargetsArray[2] = 475;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 37f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3370;
                CoinsArray[1] = 3380;
                CoinsArray[2] = 3390;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 121:
                Debug.Log("level 23");

                GameConstants.SetMode(28);
                //		GameConstants.SetMode(41);                       

                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3400;
                CoinsArray[1] = 3410;
                CoinsArray[2] = 3420;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 122:
                Debug.Log("level 24 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 5000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 123:
                Debug.Log("level 25");

                GameConstants.SetMode(36);
                //	GameConstants.SetMode(38);                  // cones + GK + moving dummy
                //GameConstants.SetMode(7);
                TargetsArray[0] = 275;
                TargetsArray[1] = 350;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3430;
                CoinsArray[1] = 3440;
                CoinsArray[2] = 3450;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 124:
                Debug.Log("level 26");

                GameConstants.SetMode(23);
                //	GameConstants.SetMode(5);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3460;
                CoinsArray[1] = 3470;
                CoinsArray[2] = 3480;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 125:
                Debug.Log("level 27");

                GameConstants.SetMode(31);
                //		GameConstants.SetMode(12);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3490;
                CoinsArray[1] = 3500;
                CoinsArray[2] = 3510;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 126:
                Debug.Log("level 28");

                GameConstants.SetMode(18);
                //	GameConstants.SetMode(6);
                TargetsArray[0] = 250;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1000;
                //   timerbool = true;                   //level has timer 
                //  GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3520;
                CoinsArray[1] = 3530;
                CoinsArray[2] = 3540;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 127:
                Debug.Log("level 29");

                GameConstants.SetMode(33);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3550;
                CoinsArray[1] = 3560;
                CoinsArray[2] = 3570;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 128:
                Debug.Log("level 30");

                GameConstants.SetMode(47);

                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3580;
                CoinsArray[1] = 3590;
                CoinsArray[2] = 3600;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 129:
                Debug.Log("level 31");

                GameConstants.SetMode(11);
                //	GameConstants.SetMode(27);
                TargetsArray[0] = 200;
                TargetsArray[1] = 350;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3610;
                CoinsArray[1] = 3620;
                CoinsArray[2] = 3630;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 130:
                Debug.Log("level 32");

                GameConstants.SetMode(34);
                //			GameConstants.SetMode(18);
                TargetsArray[0] = 250;
                TargetsArray[1] = 325;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3640;
                CoinsArray[1] = 3650;
                CoinsArray[2] = 3660;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 131:
                Debug.Log("level 33");

                GameConstants.SetMode(13);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3670;
                CoinsArray[1] = 3680;
                CoinsArray[2] = 3690;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 132:
                Debug.Log("level 34");

                GameConstants.SetMode(37);
                //	GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 300;
                TargetsArray[2] = 375;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3700;
                CoinsArray[1] = 3710;
                CoinsArray[2] = 3720;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 133:
                Debug.Log("level 35");

                GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3730;
                CoinsArray[1] = 3740;
                CoinsArray[2] = 3750;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 134:
                Debug.Log("level 36");

                GameConstants.SetMode(10);
                CoinsArray[0] = 5500;
                GameConstants.SetTargetStars(CoinsArray);
                //	GameConstants.SetMode(12);
                /*   TargetsArray[0] = 225;
                   TargetsArray[1] = 275;
                   TargetsArray[2] = 325;
                   //timerbool = true;                   //level has timer 
                   // GameConstants.timer = 23f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 50;
                   CoinsArray[1] = 100;
                   CoinsArray[2] = 150;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 135:
                Debug.Log("level 37");

                GameConstants.SetMode(38);
                //		GameConstants.SetMode(13);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 400;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3760;
                CoinsArray[1] = 3770;
                CoinsArray[2] = 3780;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 136:
                Debug.Log("level 38");
                GameConstants.SetMode(27);
                //	GameConstants.SetMode(8);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 450;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3790;
                CoinsArray[1] = 3800;
                CoinsArray[2] = 3810;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 137:
                Debug.Log("level 39");

                GameConstants.SetMode(15);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3820;
                CoinsArray[1] = 3830;
                CoinsArray[2] = 3840;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 138:
                Debug.Log("level 40");

                GameConstants.SetMode(20);
                //				GameConstants.SetMode(16);
                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3850;
                CoinsArray[1] = 3860;
                CoinsArray[2] = 3870;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 139:
                Debug.Log("level 41");

                GameConstants.SetMode(21);
                //	GameConstants.SetMode(17);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3880;
                CoinsArray[1] = 3890;
                CoinsArray[2] = 3900;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 140:
                Debug.Log("level 42");

                GameConstants.SetMode(18);
                TargetsArray[0] = 225;
                TargetsArray[1] = 350;
                TargetsArray[2] = 600;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3910;
                CoinsArray[1] = 3920;
                CoinsArray[2] = 3930;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 141:
                Debug.Log("level 43");

                GameConstants.SetMode(19);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 450;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3940;
                CoinsArray[1] = 3950;
                CoinsArray[2] = 3960;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 142:
                Debug.Log("level 44");

                GameConstants.SetMode(7);
                TargetsArray[0] = 400;
                TargetsArray[1] = 550;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 3970;
                CoinsArray[1] = 3980;
                CoinsArray[2] = 3990;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 143:
                Debug.Log("level 45");

                //    GameConstants.SetMode(7);
                GameConstants.SetMode(8);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4000;
                CoinsArray[1] = 4010;
                CoinsArray[2] = 4020;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 144:
                Debug.Log("level 46");

                GameConstants.SetMode(9);
                TargetsArray[0] = 250;
                TargetsArray[1] = 400;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4030;
                CoinsArray[1] = 4040;
                CoinsArray[2] = 4050;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 145:
                Debug.Log("level 47");

                GameConstants.SetMode(12);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4060;
                CoinsArray[1] = 4070;
                CoinsArray[2] = 4080;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 146:
                Debug.Log("level 48 Tournament");

                GameConstants.SetMode(10);
                CoinsArray[0] = 6000;
                GameConstants.SetTargetStars(CoinsArray);

                /*   //       GameConstants.SetMode(4);
                   TargetsArray[0] = 150;
                   TargetsArray[1] = 225;
                   TargetsArray[2] = 300;
                   //   timerbool = true;                   //level has timer 
                   //   GameConstants.timer = 90f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 1340;
                   CoinsArray[1] = 1350;
                   CoinsArray[2] = 1360;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;

            case 147:
                Debug.Log("level 49");

                GameConstants.SetMode(5);
                TargetsArray[0] = 125;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4090;
                CoinsArray[1] = 4100;
                CoinsArray[2] = 4110;
                GameConstants.SetTargetStars(CoinsArray);
                break;

            case 148:
                Debug.Log("level 50");

                GameConstants.SetMode(15);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4120;
                CoinsArray[1] = 4130;
                CoinsArray[2] = 4140;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 149:
                Debug.Log("level 51");

                GameConstants.SetMode(16);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4150;
                CoinsArray[1] = 4160;
                CoinsArray[2] = 4170;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 150:
                Debug.Log("level 52");

                GameConstants.SetMode(3);
                //    GameConstants.SetMode(19);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4180;
                CoinsArray[1] = 4190;
                CoinsArray[2] = 4200;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 151:
                Debug.Log("level 53");

                GameConstants.SetMode(19);
                TargetsArray[0] = 250;
                TargetsArray[1] = 350;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4210;
                CoinsArray[1] = 4220;
                CoinsArray[2] = 4230;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 152:
                Debug.Log("level 54");

                GameConstants.SetMode(1);
                TargetsArray[0] = 125;
                TargetsArray[1] = 225;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4240;
                CoinsArray[1] = 4250;
                CoinsArray[2] = 4260;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 153:
                Debug.Log("level 55");

                GameConstants.SetMode(22);
                TargetsArray[0] = 200;
                TargetsArray[1] = 310;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4270;
                CoinsArray[1] = 4280;
                CoinsArray[2] = 4290;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 154:
                Debug.Log("level 56");

                GameConstants.SetMode(38);
                //        GameConstants.SetMode(23);
                TargetsArray[0] = 250;
                TargetsArray[1] = 350;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4300;
                CoinsArray[1] = 4310;
                CoinsArray[2] = 4320;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 155:
                Debug.Log("level 57");

                GameConstants.SetMode(21);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4330;
                CoinsArray[1] = 4340;
                CoinsArray[2] = 4350;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 156:
                Debug.Log("level 58");

                GameConstants.SetMode(39);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 500;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4360;
                CoinsArray[1] = 4370;
                CoinsArray[2] = 4380;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 157:
                Debug.Log("level 59");

                GameConstants.SetMode(31);
                TargetsArray[0] = 200;
                TargetsArray[1] = 280;
                TargetsArray[2] = 380;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4390;
                CoinsArray[1] = 4400;
                CoinsArray[2] = 4410;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 158:
                Debug.Log("level 60 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 6500;
                GameConstants.SetTargetStars(CoinsArray);

                /* GameConstants.SetMode(34);
                 TargetsArray[0] = 125;
                 TargetsArray[1] = 175;
                 TargetsArray[2] = 200;
                 //   timerbool = true;                   //level has timer 
                 //   GameConstants.timer = 90f;

                 GameConstants.SetTargetScores(TargetsArray);
                 CoinsArray[0] = 1710;
                 CoinsArray[1] = 1720;
                 CoinsArray[2] = 1730;
                 GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 159:                                                              //Freekick with One Static Dummy
                Debug.Log("level 61");

                GameConstants.SetMode(16);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 200;
                TargetsArray[1] = 400;
                TargetsArray[2] = 600;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4420;
                CoinsArray[1] = 4430;
                CoinsArray[2] = 4440;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 160:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 62");

                GameConstants.SetMode(24);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4450;
                CoinsArray[1] = 4460;
                CoinsArray[2] = 4470;
                GameConstants.SetTargetStars(CoinsArray);

                break;

            case 161:                                                              //Freekick with one moving dummy
                Debug.Log("level 63");
                GameConstants.SetMode(1);
                //         GameConstants.SetMode(3);
                TargetsArray[0] = 200;
                TargetsArray[1] = 300;
                TargetsArray[2] = 400;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4480;
                CoinsArray[1] = 4490;
                CoinsArray[2] = 4500;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 162:                                                              //Freekick with pillars in goal
                Debug.Log("level 64");

                GameConstants.SetMode(29);
                //	GameConstants.SetMode(39);

                TargetsArray[0] = 150;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;

                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4510;
                CoinsArray[1] = 4520;
                CoinsArray[2] = 4530;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 163:                                                                //freekicks with pillars in goal and moving dummy
                Debug.Log("level 65");

                GameConstants.SetMode(41);
                //GameConstants.SetMode(40);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4540;
                CoinsArray[1] = 4550;
                CoinsArray[2] = 4560;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 164:                                                               //Freekick with directional pillars 
                Debug.Log("level 66");

                GameConstants.SetMode(42);
                //	GameConstants.SetMode(41);
                TargetsArray[0] = 175;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4570;
                CoinsArray[1] = 4580;
                CoinsArray[2] = 4590;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 165:                                                               //Freekick directional pillars  and GK
                Debug.Log("level 67");
                GameConstants.SetMode(25);
                //     GameConstants.SetMode(29);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4600;
                CoinsArray[1] = 4610;
                CoinsArray[2] = 4620;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 166:                                                               // Freekick with multiple Dartboards
                Debug.Log("level 68");

                GameConstants.SetMode(47);
                //   GameConstants.SetMode(42);

                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4630;
                CoinsArray[1] = 4640;
                CoinsArray[2] = 4650;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 167:                                                               //Freekicks with single dartboard and GK
                Debug.Log("level 69");

                GameConstants.SetMode(8);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4660;
                CoinsArray[1] = 4670;
                CoinsArray[2] = 4680;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 168:                                                               //tournament
                Debug.Log("level 70 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 7000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 169:                                                               //Freekick with single Dartboard and moving dummy
                Debug.Log("level 71");

                GameConstants.SetMode(7);
                //     GameConstants.SetMode(45);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4690;
                CoinsArray[1] = 4700;
                CoinsArray[2] = 4710;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 170:                                                               //Bullseye with dummy Defenders
                Debug.Log("level 72");

                GameConstants.SetMode(6);
                //	GameConstants.SetMode(30);

                TargetsArray[0] = 400;
                TargetsArray[1] = 500;
                TargetsArray[2] = 700;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4720;
                CoinsArray[1] = 4730;
                CoinsArray[2] = 4740;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 171:                                                               //Goal through Circles
                Debug.Log("level 73");
                GameConstants.SetMode(46);
                //   GameConstants.SetMode(19);

                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 225;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4750;
                CoinsArray[1] = 4760;
                CoinsArray[2] = 4770;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 172:                                                                //Goal through Circles + GK
                Debug.Log("level 74");

                GameConstants.SetMode(30);
                //	GameConstants.SetMode(46);
                TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4780;
                CoinsArray[1] = 4790;
                CoinsArray[2] = 4800;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 173:                                                               //Goal through Circles + GK + moving dummy
                Debug.Log("level 75");

                GameConstants.SetMode(4);
                //		GameConstants.SetMode(7);
                TargetsArray[0] = 125;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4810;
                CoinsArray[1] = 4820;
                CoinsArray[2] = 4830;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 174:                                                                ///Goal through  two Circles
                Debug.Log("level 76");

                GameConstants.SetMode(40);
                //    GameConstants.SetMode(25);  
                //     GameConstants.SetMode(32);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4840;
                CoinsArray[1] = 4850;
                CoinsArray[2] = 4860;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 175:                                                               //Goal through  two Circles +GK
                Debug.Log("level 77");

                GameConstants.SetMode(32);
                //	GameConstants.SetMode(33);
                TargetsArray[0] = 50;
                TargetsArray[1] = 100;
                TargetsArray[2] = 125;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 4870;
                CoinsArray[1] = 4880;
                CoinsArray[2] = 4890;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 176:                                                               //Goal through  two Circles + GK + moving dummy
                Debug.Log("level 78");

                GameConstants.SetMode(9);
                //		GameConstants.SetMode(6);

                TargetsArray[0] = 350;
                TargetsArray[1] = 600;
                TargetsArray[2] = 825;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4900;
                CoinsArray[1] = 4910;
                CoinsArray[2] = 4920;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 177:
                Debug.Log("level 79");                                       // cones  


                GameConstants.SetMode(19);
                //		GameConstants.SetMode(46);
                //			GameConstants.SetMode(9);

                TargetsArray[0] = 400;
                TargetsArray[1] = 600;
                TargetsArray[2] = 900;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4930;
                CoinsArray[1] = 4940;
                CoinsArray[2] = 4950;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 178:
                Debug.Log("level 80");

                //	GameConstants.SetMode(36);                          // cones + moving dummy
                GameConstants.SetMode(35);

                //	GameConstants.SetMode(9);
                TargetsArray[0] = 275;
                TargetsArray[1] = 375;
                TargetsArray[2] = 475;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 37f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4960;
                CoinsArray[1] = 4970;
                CoinsArray[2] = 4980;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 179:
                Debug.Log("level 81");

                GameConstants.SetMode(28);
                //		GameConstants.SetMode(41);                       

                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 4990;
                CoinsArray[1] = 5000;
                CoinsArray[2] = 5010;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 180:
                Debug.Log("level 82 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 7500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 181:
                Debug.Log("level 83");

                GameConstants.SetMode(36);
                //	GameConstants.SetMode(38);                  // cones + GK + moving dummy
                //GameConstants.SetMode(7);
                TargetsArray[0] = 275;
                TargetsArray[1] = 350;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5020;
                CoinsArray[1] = 5030;
                CoinsArray[2] = 5040;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 182:
                Debug.Log("level 84");

                GameConstants.SetMode(23);
                //	GameConstants.SetMode(5);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5050;
                CoinsArray[1] = 5060;
                CoinsArray[2] = 5070;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 183:
                Debug.Log("level 85");

                GameConstants.SetMode(31);
                //		GameConstants.SetMode(12);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5080;
                CoinsArray[1] = 5090;
                CoinsArray[2] = 5100;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 184:
                Debug.Log("level 86");

                GameConstants.SetMode(18);
                //	GameConstants.SetMode(6);
                TargetsArray[0] = 450;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1000;
                //   timerbool = true;                   //level has timer 
                //  GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5110;
                CoinsArray[1] = 5120;
                CoinsArray[2] = 5130;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 185:
                Debug.Log("level 87");

                GameConstants.SetMode(33);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5140;
                CoinsArray[1] = 5150;
                CoinsArray[2] = 5160;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 186:
                Debug.Log("level 88");

                GameConstants.SetMode(16);
                //	GameConstants.SetMode(15);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5170;
                CoinsArray[1] = 5180;
                CoinsArray[2] = 5190;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 187:
                Debug.Log("level 89");

                GameConstants.SetMode(12);
                //	GameConstants.SetMode(27);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5200;
                CoinsArray[1] = 5210;
                CoinsArray[2] = 5220;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 188:
                Debug.Log("level 90");

                GameConstants.SetMode(34);
                //			GameConstants.SetMode(18);
                TargetsArray[0] = 250;
                TargetsArray[1] = 325;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5230;
                CoinsArray[1] = 5240;
                CoinsArray[2] = 5250;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 189:
                Debug.Log("level 91");

                GameConstants.SetMode(13);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5260;
                CoinsArray[1] = 5270;
                CoinsArray[2] = 5280;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 190:
                Debug.Log("level 92");

                GameConstants.SetMode(37);
                //	GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 300;
                TargetsArray[2] = 375;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5290;
                CoinsArray[1] = 5300;
                CoinsArray[2] = 5310;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 191:
                Debug.Log("level 93");

                GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5320;
                CoinsArray[1] = 5330;
                CoinsArray[2] = 5340;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 192:
                Debug.Log("level 94 Tournament");

                GameConstants.SetMode(10);
                CoinsArray[0] = 8000;
                GameConstants.SetTargetStars(CoinsArray);
                //	GameConstants.SetMode(12);
                /*   TargetsArray[0] = 225;
                   TargetsArray[1] = 275;
                   TargetsArray[2] = 325;
                   //timerbool = true;                   //level has timer 
                   // GameConstants.timer = 23f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 50;
                   CoinsArray[1] = 100;
                   CoinsArray[2] = 150;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 193:
                Debug.Log("level 95");

                GameConstants.SetMode(38);
                //		GameConstants.SetMode(13);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5350;
                CoinsArray[1] = 5360;
                CoinsArray[2] = 5370;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 194:
                Debug.Log("level 96");
                GameConstants.SetMode(27);
                //	GameConstants.SetMode(8);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5380;
                CoinsArray[1] = 5390;
                CoinsArray[2] = 5400;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 195:
                Debug.Log("level 97");

                GameConstants.SetMode(15);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5410;
                CoinsArray[1] = 5420;
                CoinsArray[2] = 5430;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 196:
                Debug.Log("level 98");

                GameConstants.SetMode(21);
                //				GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5440;
                CoinsArray[1] = 5450;
                CoinsArray[2] = 5460;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 197:
                Debug.Log("level 99");

                GameConstants.SetMode(20);
                //	GameConstants.SetMode(17);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5470;
                CoinsArray[1] = 5480;
                CoinsArray[2] = 5490;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 198:
                Debug.Log("level 100");

                GameConstants.SetMode(18);
                TargetsArray[0] = 500;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1050;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5500;
                CoinsArray[1] = 5510;
                CoinsArray[2] = 5520;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 199:                                                              //Freekick with One Static Dummy
                Debug.Log("level 3");

                GameConstants.SetMode(39);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 75;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5530;
                CoinsArray[1] = 5540;
                CoinsArray[2] = 5550;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 200:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 4");

                GameConstants.SetMode(24);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 200;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5560;
                CoinsArray[1] = 5570;
                CoinsArray[2] = 5580;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 201:                                                              //Freekick with One Static Dummy
                Debug.Log("level 3");

                GameConstants.SetMode(39);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 75;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5590;
                CoinsArray[1] = 5600;
                CoinsArray[2] = 5610;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 202:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 4");

                GameConstants.SetMode(24);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 200;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5620;
                CoinsArray[1] = 5630;
                CoinsArray[2] = 5640;
                GameConstants.SetTargetStars(CoinsArray);

                break;

            case 203:                                                              //Freekick with one moving dummy
                Debug.Log("level 5");
                GameConstants.SetMode(1);
                //         GameConstants.SetMode(3);
                TargetsArray[0] = 125;
                TargetsArray[1] = 225;
                TargetsArray[2] = 325;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5650;
                CoinsArray[1] = 5660;
                CoinsArray[2] = 5670;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 204:                                                              //Freekick with pillars in goal
                Debug.Log("level 6");

                GameConstants.SetMode(29);
                //	GameConstants.SetMode(39);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;

                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5680;
                CoinsArray[1] = 5690;
                CoinsArray[2] = 5700;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 205:                                                                //freekicks with pillars in goal and moving dummy
                Debug.Log("level 7");

                GameConstants.SetMode(41);
                //GameConstants.SetMode(40);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5710;
                CoinsArray[1] = 5720;
                CoinsArray[2] = 5730;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 206:                                                               //Freekick with directional pillars 
                Debug.Log("level 8");

                GameConstants.SetMode(42);
                //	GameConstants.SetMode(41);
                TargetsArray[0] = 175;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5740;
                CoinsArray[1] = 5750;
                CoinsArray[2] = 5760;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 207:                                                               //Freekick directional pillars  and GK
                Debug.Log("level 9");
                GameConstants.SetMode(25);
                //     GameConstants.SetMode(29);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5770;
                CoinsArray[1] = 5780;
                CoinsArray[2] = 5790;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 208:                                                               // Freekick with multiple Dartboards
                Debug.Log("level 10");

                GameConstants.SetMode(45);
                //   GameConstants.SetMode(42);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5800;
                CoinsArray[1] = 5810;
                CoinsArray[2] = 5820;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 209:                                                               //Freekicks with single dartboard and GK
                Debug.Log("level 11");

                GameConstants.SetMode(8);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5830;
                CoinsArray[1] = 5840;
                CoinsArray[2] = 5850;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 210:                                                               //tournament
                Debug.Log("level 12 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 8500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 211:                                                               //Freekick with single Dartboard and moving dummy
                Debug.Log("level 13");

                GameConstants.SetMode(7);
                //     GameConstants.SetMode(45);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5860;
                CoinsArray[1] = 5870;
                CoinsArray[2] = 5880;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 212:                                                               //Bullseye with dummy Defenders
                Debug.Log("level 14");

                GameConstants.SetMode(6);
                //	GameConstants.SetMode(30);

                TargetsArray[0] = 250;
                TargetsArray[1] = 400;
                TargetsArray[2] = 550;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5890;
                CoinsArray[1] = 5900;
                CoinsArray[2] = 5910;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 213:                                                               //Goal through Circles
                Debug.Log("level 15");
                GameConstants.SetMode(46);
                //   GameConstants.SetMode(19);

                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 225;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5920;
                CoinsArray[1] = 5930;
                CoinsArray[2] = 5940;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 214:                                                                //Goal through Circles + GK
                Debug.Log("level 16");

                GameConstants.SetMode(30);
                //	GameConstants.SetMode(46);
                TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 5950;
                CoinsArray[1] = 5960;
                CoinsArray[2] = 5970;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 215:                                                               //Goal through Circles + GK + moving dummy
                Debug.Log("level 17");

                GameConstants.SetMode(4);
                //		GameConstants.SetMode(7);
                TargetsArray[0] = 125;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 5980;
                CoinsArray[1] = 5990;
                CoinsArray[2] = 6000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 216:                                                                ///Goal through  two Circles
                Debug.Log("level 18");

                GameConstants.SetMode(40);
                //    GameConstants.SetMode(25);  
                //     GameConstants.SetMode(32);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 6010;
                CoinsArray[1] = 6020;
                CoinsArray[2] = 6030;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 217:                                                               //Goal through  two Circles +GK
                Debug.Log("level 19");

                GameConstants.SetMode(32);
                //	GameConstants.SetMode(33);
                TargetsArray[0] = 50;
                TargetsArray[1] = 100;
                TargetsArray[2] = 125;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 6040;
                CoinsArray[1] = 6050;
                CoinsArray[2] = 6060;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 218:                                                               //Goal through  two Circles + GK + moving dummy
                Debug.Log("level 20");

                GameConstants.SetMode(9);
                //		GameConstants.SetMode(6);

                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 450;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6070;
                CoinsArray[1] = 6080;
                CoinsArray[2] = 6090;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 219:
                Debug.Log("level 21");                                       // cones  


                GameConstants.SetMode(19);
                //		GameConstants.SetMode(46);
                //			GameConstants.SetMode(9);

                TargetsArray[0] = 200;
                TargetsArray[1] = 300;
                TargetsArray[2] = 450;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6100;
                CoinsArray[1] = 6110;
                CoinsArray[2] = 6120;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 220:
                Debug.Log("level 22");

                //	GameConstants.SetMode(36);                          // cones + moving dummy
                GameConstants.SetMode(35);

                //	GameConstants.SetMode(9);
                TargetsArray[0] = 275;
                TargetsArray[1] = 375;
                TargetsArray[2] = 475;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 37f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6130;
                CoinsArray[1] = 6140;
                CoinsArray[2] = 6150;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 221:
                Debug.Log("level 23");

                GameConstants.SetMode(28);
                //		GameConstants.SetMode(41);                       

                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6160;
                CoinsArray[2] = 6170;
                CoinsArray[1] = 6180;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 222:
                Debug.Log("level 24 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 9000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 223:
                Debug.Log("level 25");

                GameConstants.SetMode(36);
                //	GameConstants.SetMode(38);                  // cones + GK + moving dummy
                //GameConstants.SetMode(7);
                TargetsArray[0] = 275;
                TargetsArray[1] = 350;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6190;
                CoinsArray[1] = 6200;
                CoinsArray[2] = 6210;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 224:
                Debug.Log("level 26");

                GameConstants.SetMode(23);
                //	GameConstants.SetMode(5);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6220;
                CoinsArray[1] = 6230;
                CoinsArray[2] = 6240;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 225:
                Debug.Log("level 27");

                GameConstants.SetMode(31);
                //		GameConstants.SetMode(12);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6250;
                CoinsArray[1] = 6260;
                CoinsArray[2] = 6270;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 226:
                Debug.Log("level 28");

                GameConstants.SetMode(18);
                //	GameConstants.SetMode(6);
                TargetsArray[0] = 250;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1000;
                //   timerbool = true;                   //level has timer 
                //  GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6280;
                CoinsArray[1] = 6290;
                CoinsArray[2] = 6300;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 227:
                Debug.Log("level 29");

                GameConstants.SetMode(33);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6310;
                CoinsArray[1] = 6320;
                CoinsArray[2] = 6330;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 228:
                Debug.Log("level 30");

                GameConstants.SetMode(47);

                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6340;
                CoinsArray[1] = 6350;
                CoinsArray[2] = 6360;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 229:
                Debug.Log("level 31");

                GameConstants.SetMode(11);
                //	GameConstants.SetMode(27);
                TargetsArray[0] = 200;
                TargetsArray[1] = 350;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6370;
                CoinsArray[1] = 6380;
                CoinsArray[2] = 6390;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 230:
                Debug.Log("level 32");

                GameConstants.SetMode(34);
                //			GameConstants.SetMode(18);
                TargetsArray[0] = 250;
                TargetsArray[1] = 325;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6400;
                CoinsArray[1] = 6410;
                CoinsArray[2] = 6420;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 231:
                Debug.Log("level 33");

                GameConstants.SetMode(13);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6430;
                CoinsArray[1] = 6440;
                CoinsArray[2] = 6450;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 232:
                Debug.Log("level 34");

                GameConstants.SetMode(37);
                //	GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 300;
                TargetsArray[2] = 375;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6460;
                CoinsArray[1] = 6470;
                CoinsArray[2] = 6480;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 233:
                Debug.Log("level 35");

                GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6490;
                CoinsArray[1] = 6500;
                CoinsArray[2] = 6510;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 234:
                Debug.Log("level 36");

                GameConstants.SetMode(10);
                CoinsArray[0] = 9500;
                GameConstants.SetTargetStars(CoinsArray);
                //	GameConstants.SetMode(12);
                /*   TargetsArray[0] = 225;
                   TargetsArray[1] = 275;
                   TargetsArray[2] = 325;
                   //timerbool = true;                   //level has timer 
                   // GameConstants.timer = 23f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 50;
                   CoinsArray[1] = 100;
                   CoinsArray[2] = 150;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 235:
                Debug.Log("level 37");

                GameConstants.SetMode(38);
                //		GameConstants.SetMode(13);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 400;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6520;
                CoinsArray[1] = 6530;
                CoinsArray[2] = 6540;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 236:
                Debug.Log("level 38");
                GameConstants.SetMode(27);
                //	GameConstants.SetMode(8);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 450;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6550;
                CoinsArray[1] = 6560;
                CoinsArray[2] = 6570;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 237:
                Debug.Log("level 39");

                GameConstants.SetMode(15);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6580;
                CoinsArray[1] = 6590;
                CoinsArray[2] = 6600;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 238:
                Debug.Log("level 40");

                GameConstants.SetMode(20);
                //				GameConstants.SetMode(16);
                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6610;
                CoinsArray[1] = 6620;
                CoinsArray[2] = 6630;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 239:
                Debug.Log("level 41");

                GameConstants.SetMode(21);
                //	GameConstants.SetMode(17);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6640;
                CoinsArray[1] = 6650;
                CoinsArray[2] = 6660;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 240:
                Debug.Log("level 42");

                GameConstants.SetMode(18);
                TargetsArray[0] = 225;
                TargetsArray[1] = 350;
                TargetsArray[2] = 600;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6670;
                CoinsArray[1] = 6680;
                CoinsArray[2] = 6690;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 241:
                Debug.Log("level 43");

                GameConstants.SetMode(19);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 450;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6700;
                CoinsArray[1] = 6710;
                CoinsArray[2] = 6720;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 242:
                Debug.Log("level 44");

                GameConstants.SetMode(7);
                TargetsArray[0] = 400;
                TargetsArray[1] = 550;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6730;
                CoinsArray[1] = 6740;
                CoinsArray[2] = 6750;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 243:
                Debug.Log("level 45");

                //    GameConstants.SetMode(7);
                GameConstants.SetMode(8);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6760;
                CoinsArray[1] = 6770;
                CoinsArray[2] = 6780;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 244:
                Debug.Log("level 46");

                GameConstants.SetMode(9);
                TargetsArray[0] = 250;
                TargetsArray[1] = 400;
                TargetsArray[2] = 600;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6790;
                CoinsArray[1] = 6800;
                CoinsArray[2] = 6810;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 245:
                Debug.Log("level 47");

                GameConstants.SetMode(12);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6820;
                CoinsArray[1] = 6830;
                CoinsArray[2] = 6840;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 246:
                Debug.Log("level 48 Tournament");

                GameConstants.SetMode(10);
                CoinsArray[0] = 10000;
                GameConstants.SetTargetStars(CoinsArray);

                /*   //       GameConstants.SetMode(4);
                   TargetsArray[0] = 150;
                   TargetsArray[1] = 225;
                   TargetsArray[2] = 300;
                   //   timerbool = true;                   //level has timer 
                   //   GameConstants.timer = 90f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 1340;
                   CoinsArray[1] = 1350;
                   CoinsArray[2] = 1360;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;

            case 247:
                Debug.Log("level 49");

                GameConstants.SetMode(5);
                TargetsArray[0] = 125;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6850;
                CoinsArray[1] = 6860;
                CoinsArray[2] = 6870;
                GameConstants.SetTargetStars(CoinsArray);
                break;

            case 248:
                Debug.Log("level 50");

                GameConstants.SetMode(15);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6880;
                CoinsArray[1] = 6890;
                CoinsArray[2] = 6900;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 249:
                Debug.Log("level 51");

                GameConstants.SetMode(16);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6910;
                CoinsArray[1] = 6920;
                CoinsArray[2] = 6930;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 250:
                Debug.Log("level 52");

                GameConstants.SetMode(3);
                //    GameConstants.SetMode(19);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 700;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6940;
                CoinsArray[1] = 6950;
                CoinsArray[2] = 6960;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 251:
                Debug.Log("level 53");

                GameConstants.SetMode(19);
                TargetsArray[0] = 250;
                TargetsArray[1] = 350;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 6970;
                CoinsArray[1] = 6980;
                CoinsArray[2] = 6990;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 252:
                Debug.Log("level 54");

                GameConstants.SetMode(1);
                TargetsArray[0] = 125;
                TargetsArray[1] = 225;
                TargetsArray[2] = 350;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7000;
                CoinsArray[1] = 7010;
                CoinsArray[2] = 7020;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 253:
                Debug.Log("level 55");

                GameConstants.SetMode(22);
                TargetsArray[0] = 200;
                TargetsArray[1] = 310;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7030;
                CoinsArray[1] = 7040;
                CoinsArray[2] = 7050;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 254:
                Debug.Log("level 56");

                GameConstants.SetMode(38);
                //        GameConstants.SetMode(23);
                TargetsArray[0] = 250;
                TargetsArray[1] = 350;
                TargetsArray[2] = 450;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7060;
                CoinsArray[1] = 7070;
                CoinsArray[2] = 7080;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 255:
                Debug.Log("level 57");

                GameConstants.SetMode(21);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7090;
                CoinsArray[1] = 7100;
                CoinsArray[2] = 7110;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 256:
                Debug.Log("level 58");

                GameConstants.SetMode(39);
                TargetsArray[0] = 300;
                TargetsArray[1] = 450;
                TargetsArray[2] = 500;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7120;
                CoinsArray[1] = 7130;
                CoinsArray[2] = 7140;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 257:
                Debug.Log("level 59");

                GameConstants.SetMode(31);
                TargetsArray[0] = 200;
                TargetsArray[1] = 280;
                TargetsArray[2] = 380;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7150;
                CoinsArray[1] = 7160;
                CoinsArray[2] = 7170;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 258:
                Debug.Log("level 60 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 10500;
                GameConstants.SetTargetStars(CoinsArray);

                /* GameConstants.SetMode(34);
                 TargetsArray[0] = 125;
                 TargetsArray[1] = 175;
                 TargetsArray[2] = 200;
                 //   timerbool = true;                   //level has timer 
                 //   GameConstants.timer = 90f;

                 GameConstants.SetTargetScores(TargetsArray);
                 CoinsArray[0] = 1710;
                 CoinsArray[1] = 1720;
                 CoinsArray[2] = 1730;
                 GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 259:                                                              //Freekick with One Static Dummy
                Debug.Log("level 61");

                GameConstants.SetMode(16);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 200;
                TargetsArray[1] = 400;
                TargetsArray[2] = 600;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7180;
                CoinsArray[1] = 7190;
                CoinsArray[2] = 7200;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 260:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 62");

                GameConstants.SetMode(24);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7210;
                CoinsArray[1] = 7220;
                CoinsArray[2] = 7230;
                GameConstants.SetTargetStars(CoinsArray);

                break;

            case 261:                                                              //Freekick with one moving dummy
                Debug.Log("level 63");
                GameConstants.SetMode(1);
                //         GameConstants.SetMode(3);
                TargetsArray[0] = 200;
                TargetsArray[1] = 300;
                TargetsArray[2] = 400;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7240;
                CoinsArray[1] = 7250;
                CoinsArray[2] = 7260;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 262:                                                              //Freekick with pillars in goal
                Debug.Log("level 64");

                GameConstants.SetMode(29);
                //	GameConstants.SetMode(39);

                TargetsArray[0] = 150;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;

                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7270;
                CoinsArray[1] = 7280;
                CoinsArray[2] = 7290;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 263:                                                                //freekicks with pillars in goal and moving dummy
                Debug.Log("level 65");

                GameConstants.SetMode(41);
                //GameConstants.SetMode(40);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7300;
                CoinsArray[1] = 7310;
                CoinsArray[2] = 7320;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 264:                                                               //Freekick with directional pillars 
                Debug.Log("level 66");

                GameConstants.SetMode(42);
                //	GameConstants.SetMode(41);
                TargetsArray[0] = 175;
                TargetsArray[1] = 225;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7330;
                CoinsArray[1] = 7340;
                CoinsArray[2] = 7350;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 265:                                                               //Freekick directional pillars  and GK
                Debug.Log("level 67");
                GameConstants.SetMode(25);
                //     GameConstants.SetMode(29);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7360;
                CoinsArray[1] = 7370;
                CoinsArray[2] = 7380;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 266:                                                               // Freekick with multiple Dartboards
                Debug.Log("level 68");

                GameConstants.SetMode(47);
                //   GameConstants.SetMode(42);

                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7390;
                CoinsArray[1] = 7400;
                CoinsArray[2] = 7410;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 267:                                                               //Freekicks with single dartboard and GK
                Debug.Log("level 69");

                GameConstants.SetMode(8);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7420;
                CoinsArray[1] = 7430;
                CoinsArray[2] = 7440;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 268:                                                               //tournament
                Debug.Log("level 70 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 11000;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 269:                                                               //Freekick with single Dartboard and moving dummy
                Debug.Log("level 71");

                GameConstants.SetMode(7);
                //     GameConstants.SetMode(45);

                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7450;
                CoinsArray[1] = 7460;
                CoinsArray[2] = 7470;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 270:                                                               //Bullseye with dummy Defenders
                Debug.Log("level 72");

                GameConstants.SetMode(6);
                //	GameConstants.SetMode(30);

                TargetsArray[0] = 400;
                TargetsArray[1] = 500;
                TargetsArray[2] = 700;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7480;
                CoinsArray[1] = 7490;
                CoinsArray[2] = 7500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 271:                                                               //Goal through Circles
                Debug.Log("level 73");
                GameConstants.SetMode(46);
                //   GameConstants.SetMode(19);

                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 225;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7510;
                CoinsArray[1] = 7520;
                CoinsArray[2] = 7530;
                GameConstants.SetTargetStars(CoinsArray);

                break;
            case 272:                                                                //Goal through Circles + GK
                Debug.Log("level 74");

                GameConstants.SetMode(30);
                //	GameConstants.SetMode(46);
                TargetsArray[0] = 100;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7540;
                CoinsArray[1] = 7550;
                CoinsArray[2] = 7560;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 273:                                                               //Goal through Circles + GK + moving dummy
                Debug.Log("level 75");

                GameConstants.SetMode(4);
                //		GameConstants.SetMode(7);
                TargetsArray[0] = 125;
                TargetsArray[1] = 250;
                TargetsArray[2] = 400;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7570;
                CoinsArray[1] = 7580;
                CoinsArray[2] = 7590;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 274:                                                                ///Goal through  two Circles
                Debug.Log("level 76");

                GameConstants.SetMode(40);
                //    GameConstants.SetMode(25);  
                //     GameConstants.SetMode(32);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 250;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7600;
                CoinsArray[1] = 7610;
                CoinsArray[2] = 7620;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 275:                                                               //Goal through  two Circles +GK
                Debug.Log("level 77");

                GameConstants.SetMode(32);
                //	GameConstants.SetMode(33);
                TargetsArray[0] = 50;
                TargetsArray[1] = 100;
                TargetsArray[2] = 125;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 7630;
                CoinsArray[1] = 7640;
                CoinsArray[2] = 7650;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 276:                                                               //Goal through  two Circles + GK + moving dummy
                Debug.Log("level 78");

                GameConstants.SetMode(9);
                //		GameConstants.SetMode(6);

                TargetsArray[0] = 350;
                TargetsArray[1] = 600;
                TargetsArray[2] = 825;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7660;
                CoinsArray[1] = 7670;
                CoinsArray[2] = 7680;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 277:
                Debug.Log("level 79");                                       // cones  


                GameConstants.SetMode(19);
                //		GameConstants.SetMode(46);
                //			GameConstants.SetMode(9);

                TargetsArray[0] = 400;
                TargetsArray[1] = 600;
                TargetsArray[2] = 900;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7690;
                CoinsArray[1] = 7700;
                CoinsArray[2] = 7710;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 278:
                Debug.Log("level 80");

                //	GameConstants.SetMode(36);                          // cones + moving dummy
                GameConstants.SetMode(35);

                //	GameConstants.SetMode(9);
                TargetsArray[0] = 275;
                TargetsArray[1] = 375;
                TargetsArray[2] = 475;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 37f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7720;
                CoinsArray[1] = 7730;
                CoinsArray[2] = 7740;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 279:
                Debug.Log("level 81");

                GameConstants.SetMode(28);
                //		GameConstants.SetMode(41);                       

                TargetsArray[0] = 200;
                TargetsArray[1] = 250;
                TargetsArray[2] = 300;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7750;
                CoinsArray[1] = 7760;
                CoinsArray[2] = 7770;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 280:
                Debug.Log("level 82 Tournament");
                GameConstants.SetMode(10);
                CoinsArray[0] = 11500;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 281:
                Debug.Log("level 83");

                GameConstants.SetMode(36);
                //	GameConstants.SetMode(38);                  // cones + GK + moving dummy
                //GameConstants.SetMode(7);
                TargetsArray[0] = 275;
                TargetsArray[1] = 350;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;
                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7780;
                CoinsArray[1] = 7790;
                CoinsArray[2] = 7800;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 282:
                Debug.Log("level 84");

                GameConstants.SetMode(23);
                //	GameConstants.SetMode(5);
                TargetsArray[0] = 200;
                TargetsArray[1] = 275;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 40f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7810;
                CoinsArray[1] = 7820;
                CoinsArray[2] = 7830;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 283:
                Debug.Log("level 85");

                GameConstants.SetMode(31);
                //		GameConstants.SetMode(12);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 350;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7840;
                CoinsArray[1] = 7850;
                CoinsArray[2] = 7860;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 284:
                Debug.Log("level 86");

                GameConstants.SetMode(18);
                //	GameConstants.SetMode(6);
                TargetsArray[0] = 450;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1000;
                //   timerbool = true;                   //level has timer 
                //  GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7870;
                CoinsArray[1] = 7880;
                CoinsArray[2] = 7890;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 285:
                Debug.Log("level 87");

                GameConstants.SetMode(33);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 125;
                TargetsArray[1] = 175;
                TargetsArray[2] = 200;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7900;
                CoinsArray[1] = 7910;
                CoinsArray[2] = 7920;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 286:
                Debug.Log("level 88");

                GameConstants.SetMode(16);
                //	GameConstants.SetMode(15);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7930;
                CoinsArray[1] = 7940;
                CoinsArray[2] = 7950;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 287:
                Debug.Log("level 89");

                GameConstants.SetMode(12);
                //	GameConstants.SetMode(27);
                TargetsArray[0] = 150;
                TargetsArray[1] = 250;
                TargetsArray[2] = 325;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7960;
                CoinsArray[1] = 7970;
                CoinsArray[2] = 7980;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 288:
                Debug.Log("level 90");

                GameConstants.SetMode(34);
                //			GameConstants.SetMode(18);
                TargetsArray[0] = 250;
                TargetsArray[1] = 325;
                TargetsArray[2] = 400;
                //   timerbool = true;                   //level has timer 
                //   GameConstants.timer = 90f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 7990;
                CoinsArray[1] = 8000;
                CoinsArray[2] = 8010;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 289:
                Debug.Log("level 91");

                GameConstants.SetMode(13);
                TargetsArray[0] = 150;
                TargetsArray[1] = 200;
                TargetsArray[2] = 300;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 25f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8020;
                CoinsArray[1] = 8030;
                CoinsArray[2] = 8040;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 290:
                Debug.Log("level 92");

                GameConstants.SetMode(37);
                //	GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 300;
                TargetsArray[2] = 375;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8050;
                CoinsArray[1] = 8060;
                CoinsArray[2] = 8070;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 291:
                Debug.Log("level 93");

                GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                timerbool = true;                   //level has timer 
                GameConstants.timer = 60f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8080;
                CoinsArray[1] = 8090;
                CoinsArray[2] = 8100;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 292:
                Debug.Log("level 94 Tournament");

                GameConstants.SetMode(10);
                CoinsArray[0] = 12000;
                GameConstants.SetTargetStars(CoinsArray);
                //	GameConstants.SetMode(12);
                /*   TargetsArray[0] = 225;
                   TargetsArray[1] = 275;
                   TargetsArray[2] = 325;
                   //timerbool = true;                   //level has timer 
                   // GameConstants.timer = 23f;

                   GameConstants.SetTargetScores(TargetsArray);
                   CoinsArray[0] = 50;
                   CoinsArray[1] = 100;
                   CoinsArray[2] = 150;
                   GameConstants.SetTargetStars(CoinsArray);*/
                break;
            case 293:
                Debug.Log("level 95");

                GameConstants.SetMode(38);
                //		GameConstants.SetMode(13);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8110;
                CoinsArray[1] = 8120;
                CoinsArray[2] = 8130;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 294:
                Debug.Log("level 96");
                GameConstants.SetMode(27);
                //	GameConstants.SetMode(8);
                //	GameConstants.SetMode(14);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8140;
                CoinsArray[1] = 8150;
                CoinsArray[2] = 8160;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 295:
                Debug.Log("level 97");

                GameConstants.SetMode(15);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8170;
                CoinsArray[1] = 8180;
                CoinsArray[2] = 8190;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 296:
                Debug.Log("level 98");

                GameConstants.SetMode(21);
                //				GameConstants.SetMode(16);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8200;
                CoinsArray[1] = 8210;
                CoinsArray[2] = 8220;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 297:
                Debug.Log("level 99");

                GameConstants.SetMode(20);
                //	GameConstants.SetMode(17);
                TargetsArray[0] = 225;
                TargetsArray[1] = 275;
                TargetsArray[2] = 325;
                //timerbool = true;                   //level has timer 
                // GameConstants.timer = 23f;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8230;
                CoinsArray[1] = 8240;
                CoinsArray[2] = 8250;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 298:
                Debug.Log("level 100");

                GameConstants.SetMode(18);
                TargetsArray[0] = 500;
                TargetsArray[1] = 700;
                TargetsArray[2] = 1050;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8260;
                CoinsArray[1] = 8270;
                CoinsArray[2] = 8280;
                GameConstants.SetTargetStars(CoinsArray);
                break;
            case 299:                                                              //Freekick with One Static Dummy
                Debug.Log("level 3");

                GameConstants.SetMode(39);
                //	GameConstants.SetMode(23);
                TargetsArray[0] = 75;
                TargetsArray[1] = 150;
                TargetsArray[2] = 225;
                GameConstants.SetTargetScores(TargetsArray);

                CoinsArray[0] = 8290;
                CoinsArray[1] = 8300;
                CoinsArray[2] = 8310;
                GameConstants.SetTargetStars(CoinsArray);


                break;
            case 300:                                                             //Freekick with One Static Dummy and one moving dummy
                Debug.Log("level 4");

                GameConstants.SetMode(24);
                TargetsArray[0] = 100;
                TargetsArray[1] = 150;
                TargetsArray[2] = 200;

                GameConstants.SetTargetScores(TargetsArray);
                CoinsArray[0] = 8320;
                CoinsArray[1] = 8330;
                CoinsArray[2] = 8340;
                GameConstants.SetTargetStars(CoinsArray);

                break;

        }

        if (GameConstants.GetLevelNumber()!=GenericVariables.getTotalLevelsPassed() && GameConstants.GetMode()!=44)     //if level is being played again and Mode is not bonus
        {
            CoinsArray[0] = 10;          //coins
            CoinsArray[1] = 15;
            CoinsArray[2] = 20;
            GameConstants.SetTargetStars(CoinsArray);
    //        GamePlayManager.instance.SetEnvironment(GamePlayManager.EnvironmentNumber);
        }
    }
}
