﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardCostumeManager : MonoBehaviour
{
    // [SerializeField] Sprite[] RewardItemsImagesList;
    [SerializeField] Image RewardItemImage;
    [SerializeField] int RewardNumber;
    [SerializeField] Text[] RewardNameTextArray;

    int RewardOfferNumber;

    /*          
                case 1://football

                case 2: //kit

                case 3: //celebration

                case 4: //football

                case 5: //kit

                case 6: //face

                case 7: //celebration

                case 8: //kit

                case 9: //football

                case 10: //kit

                case 11: //football

                case 12: //football
    */

    private void OnEnable()
    {
        AdController.instance.hideNativeBannerAd();

        PluginCallBackManager.CostumeRewardAdComplete_ += giveRewardCostume;
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("RewardWon");
        }
        SelectRewardImageToShow();
    }
    public enum RewardName
    {
        kit,
        skin,
        football,
        celebration
    }
    public void EnableNewItemNameText( RewardName rewardName)
    {
        for(int i=0; i<RewardNameTextArray.Length;i++)
        {
            RewardNameTextArray[i].gameObject.SetActive(false);

        }
        switch (rewardName)
        {
            case RewardName.kit:
                RewardNameTextArray[0].gameObject.SetActive(true);
                break;

            case RewardName.skin:
                RewardNameTextArray[1].gameObject.SetActive(true);

                break;
            
            case RewardName.football:

                RewardNameTextArray[2].gameObject.SetActive(true);
                break;

            case RewardName.celebration:

                RewardNameTextArray[3].gameObject.SetActive(true);
                break;

        }

    }

    public void SelectRewardImageToShow()
    {
        RewardNumber = GameConstants.GetGameOverRewardNumber(); //by default 1
      
        switch (RewardNumber)
        {
            case 1://football 
                   // RewardItemImage.sprite = RewardItemsImagesList[0];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[0];
                EnableNewItemNameText(RewardName.football);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 2: //kit
                    //   RewardItemImage.sprite = RewardItemsImagesList[1];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[1];
                EnableNewItemNameText(RewardName.kit);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 3: //celebration
                    //   RewardItemImage.sprite = RewardItemsImagesList[2];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[2];
                EnableNewItemNameText(RewardName.celebration);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 4: //football
                    // RewardItemImage.sprite = RewardItemsImagesList[3];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[3];
                EnableNewItemNameText(RewardName.football);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 5: //kit
                    //   RewardItemImage.sprite = RewardItemsImagesList[4];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[4];
                EnableNewItemNameText(RewardName.kit);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 6: //face
                    //     RewardItemImage.sprite = RewardItemsImagesList[5];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[5];
                EnableNewItemNameText(RewardName.skin);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 7: //celebration
                    //  RewardItemImage.sprite = RewardItemsImagesList[6];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[6];
                EnableNewItemNameText(RewardName.celebration);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 8: //kit
                    // RewardItemImage.sprite = RewardItemsImagesList[7];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[7];
                EnableNewItemNameText(RewardName.kit);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 9: //football
                    //  RewardItemImage.sprite = RewardItemsImagesList[8];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[8];
                EnableNewItemNameText(RewardName.football);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 10: //kit
                     //     RewardItemImage.sprite = RewardItemsImagesList[9];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[9];
                EnableNewItemNameText(RewardName.kit);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 11: //football
                     //   RewardItemImage.sprite = RewardItemsImagesList[10];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[10];
                EnableNewItemNameText(RewardName.football);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;
            case 12: //football
                     //   RewardItemImage.sprite = RewardItemsImagesList[11];
                RewardItemImage.sprite = CustomizationScript.instance.RewardItemsImagesList[11];
                EnableNewItemNameText(RewardName.football);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                break;

        }
  

    }
    public void IncrementRewardNumber() //next reward if no thanks or claimed
    {
        if (RewardNumber < GameConstants.TotalGameOverRewards)
        {
            GameConstants.SetGameOverRewardNumber(RewardNumber + 1);
        }
        else
        {
            GameConstants.SetGameOverRewardNumber(1);
        }
    }

    public void CloseButtonClicked() //no thanks
    {
        GameConstants.SetGameOverRewardUnClaim(RewardNumber);         //setting as unclaimed
        IncrementRewardNumber();
        LevelCompleteMenu.Instance.EnableClaimButtonsAfterRewardNoThanks();
     //   LevelCompleteMenu.Instance.RewardIconButton.interactable = true;
        GameConstants.SetRewardProgress(0);

        switch (RewardNumber)
        {
            case 1: //football
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[0];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 1);
                break;
          
            case 2: //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[0];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 1); //save with status
                break;
          
            case 3: //celebration
                RewardOfferNumber = GameConstants.RewardCelebrationstoOffer[0];
                GenericVariables.saveUnlockedCelebrationWithStatus(RewardOfferNumber, 1);
                break;
         
            case 4: //football
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[1];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 1);
                break;

            case 5: //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[1];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 1); //save with status
                break;

            case 6://face
                RewardOfferNumber = GameConstants.RewardSkinstoOffer[0];
                GenericVariables.saveUnlockedSkinWithStatus(RewardOfferNumber, 1);
                break;
         
            case 7:     //celebration
                RewardOfferNumber = GameConstants.RewardCelebrationstoOffer[1];
                GenericVariables.saveUnlockedCelebrationWithStatus(RewardOfferNumber, 1);
                break;
        
            case 8:     //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[2];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 1); //save with status
                break;

            case 9: //ball
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[2];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 1);
                break;
         
            case 10: //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[3];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 1); //save with status
                break;
      
            case 11: //ball
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[3];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 1);
                break;
         
            case 12: //ball
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[4];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 1);
                break;

        }

        this.gameObject.SetActive(false);
    }
    public void OnClickGetCostumeButton()
    {
        GenericVariables.RewardAdIndex = 4;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    public void giveRewardCostume()     //costume give away working to be done here
    {
        Time.timeScale = 1;
        GenericVariables.RewardAdIndex = -1;

        switch (RewardNumber)
        {
            case 1: //football
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[0];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber,2);
                GenericVariables.saveFootball(RewardOfferNumber);
                CustomizationScript.instance.FootballLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeFootball(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);

                break;
            case 2: //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[0];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber,2); //save with status
                GenericVariables.saveBody(RewardOfferNumber);
                CustomizationScript.instance.BodyLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeBody(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.kit, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
                break;
            case 3: //celebration
                RewardOfferNumber = GameConstants.RewardCelebrationstoOffer[0];
                GenericVariables.saveUnlockedCelebrationWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveCelebration(RewardOfferNumber);
                CustomizationScript.instance.CelebrationLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeCelebration(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.celebration, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);

                break;
            case 4: //football
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[1];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveFootball(RewardOfferNumber);
                CustomizationScript.instance.FootballLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeFootball(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);

                break;
            case 5: //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[1];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 2); //save with status
                GenericVariables.saveBody(RewardOfferNumber);
                CustomizationScript.instance.BodyLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeBody(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.kit, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);


                break;
            case 6://face
                RewardOfferNumber = GameConstants.RewardSkinstoOffer[0];
                GenericVariables.saveUnlockedSkinWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveSkin(RewardOfferNumber);
                CustomizationScript.instance.SkinLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeSkin(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.skin, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);


                break; 
            case 7:     //celebration
                RewardOfferNumber = GameConstants.RewardCelebrationstoOffer[1];
                GenericVariables.saveUnlockedCelebrationWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveCelebration(RewardOfferNumber);
                CustomizationScript.instance.CelebrationLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeCelebration(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.celebration, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);

                break; 
            case 8:     //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[2];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 2); //save with status
                GenericVariables.saveBody(RewardOfferNumber);
                CustomizationScript.instance.BodyLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeBody(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.kit, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
                break;

            case 9: //ball
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[2];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveFootball(RewardOfferNumber);
                CustomizationScript.instance.FootballLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeFootball(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
                break;
            case 10: //kit
                RewardOfferNumber = GameConstants.RewardBodystoOffer[3];
                GenericVariables.saveUnlockedBodyWithStatus(RewardOfferNumber, 2); //save with status
                GenericVariables.saveBody(RewardOfferNumber);
                CustomizationScript.instance.BodyLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeBody(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.kit, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
                break;
            case 11: //ball
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[3];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveFootball(RewardOfferNumber);
                CustomizationScript.instance.FootballLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeFootball(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
                break;
            case 12: //ball
                RewardOfferNumber = GameConstants.RewardFootballstoOffer[4];
                GenericVariables.saveUnlockedFootballWithStatus(RewardOfferNumber, 2);
                GenericVariables.saveFootball(RewardOfferNumber);
                CustomizationScript.instance.FootballLockImages[RewardOfferNumber].gameObject.SetActive(false);
                CustomizationScript.instance.ChangeFootball(RewardOfferNumber);
                GameConstants.SetGameOverRewardClaim(RewardNumber);         //setting as claimed
                CustomizationScript.instance.buyButton.SetActive(false);
                AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, RewardOfferNumber, AnalyticsManagerSoccerKicks.Unlock_Type.By_Ads);
                break;

        }

        /*    GenericVariables.saveBoughtBody(5);
            GenericVariables.saveBody(5);

    *//*        MenuManager.Instance.  BodyLockImages[5].gameObject.SetActive(false);
            MenuManager.Instance.ChangeBody(5);
            MenuManager.Instance.buyButton.SetActive(false);*//*

            CustomizationScript.instance.BodyLockImages[5].gameObject.SetActive(false);
            CustomizationScript.instance.ChangeBody(5);
            CustomizationScript.instance.buyButton.SetActive(false);
    */

        //  IncrementRewardNumber();

        LevelCompleteMenu.Instance.EnableClaimButtonsAfterRewardTake();
      //  LevelCompleteMenu.Instance.RewardIconButton.interactable = false;
        GameConstants.SetRewardProgress(0);

        this.gameObject.SetActive(false);

    }
    private void OnDisable()
    {
        //AdController.instance.ShowBannerAd();
        LevelCompleteMenu.Instance.RewardItemImageWhite.gameObject.SetActive(false);
        PluginCallBackManager.CostumeRewardAdComplete_ -= giveRewardCostume;

    }
}
