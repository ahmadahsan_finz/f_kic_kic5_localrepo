using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardKickItemsManager : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] RewardItemsConsumbables;
    public GameObject[] RewardItemsCustomizations;
    public GameObject[] PositionsPoints;
    int randomInt;
    List<int> numbersToChooseFrom = new List<int>(new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 });

    private void OnEnable()
    {
        disbaleAllItems();
        SetPositionOfRewards();
    }

    public void disbaleAllItems()
    {
        for (int i = 0; i < RewardItemsConsumbables.Length; i++)
        {
            RewardItemsConsumbables[i].gameObject.SetActive(false);

        }
    }
    public int RandomRanger()
    {
         randomInt = numbersToChooseFrom[Random.Range(0, numbersToChooseFrom.Count)];

        numbersToChooseFrom.Remove(randomInt);

        return randomInt;
    }

    public void SetPositionOfRewards()
    {
        for(int i=0; i< RewardItemsConsumbables.Length;i++)
        {
            RewardItemsConsumbables[i].gameObject.SetActive(true);
            RewardItemsConsumbables[i].transform.position = PositionsPoints[RandomRanger()].transform.position;

        }
    }

    //public void OnTriggerEnter(Collider other)
    //{
    //    if(other.CompareTag("Coin100"))
    //    {
    //        GenericVariables.AddCoins(100);
    //    }
    //    else if (other.CompareTag("Coin150"))
    //    {
    //        GenericVariables.AddCoins(100);


    //    }
    //    else if (other.CompareTag("Coin250"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Coin300"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Coin350"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Coin400"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Coin450"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Coin500"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Coin500"))
    //    {
    //        GenericVariables.AddCoins(100);

    //    }
    //    else if (other.CompareTag("Diamond10"))
    //    {
    //        GenericVariables.AddDiamonds(10);

    //    }
    //    else if (other.CompareTag("Diamond20"))
    //    {
    //        GenericVariables.AddDiamonds(20);

    //    }
    //    else if (other.CompareTag("Diamond30"))
    //    {

    //        GenericVariables.AddDiamonds(30);
    //    }
    //    else if (other.CompareTag("Diamond40"))
    //    {
    //        GenericVariables.AddDiamonds(40);

    //    }
    //    else if (other.CompareTag("Fireball1"))
    //    {
    //        GenericVariables.AddFire(1);

    //    }
    //    else if (other.CompareTag("Fireball2"))
    //    {
    //        GenericVariables.AddFire(2);

    //    }
    //    else if (other.CompareTag("Fireball4"))
    //    {
    //        GenericVariables.AddFire(4);

    //    }
    //    else if (other.CompareTag("Fireball6"))
    //    {
    //        GenericVariables.AddFire(6);

    //    }
    //    else if (other.CompareTag("Energy1"))
    //    {
    //        GenericVariables.AddEnergy(1);

    //    }
    //    else if (other.CompareTag("Energy2"))
    //    {
    //        GenericVariables.AddEnergy(2);

    //    }
    //    else if (other.CompareTag("Energy3"))
    //    {
    //        GenericVariables.AddEnergy(3);

    //    }
    //    else if (other.CompareTag("Energy4"))
    //    {
    //        GenericVariables.AddEnergy(4);

    //    }
    //    else if (other.CompareTag("Energy5"))
    //    {
    //        GenericVariables.AddEnergy(5);

    //    }
    //}
}
