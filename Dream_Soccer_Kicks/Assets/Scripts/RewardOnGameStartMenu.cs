﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardOnGameStartMenu : MonoBehaviour
{
	public static RewardOnGameStartMenu instance; 
    public enum RewardKickDialogClose
	{
		 unclosed,closed
	}
	public static RewardKickDialogClose RewardKickCloseState;

	private void Awake()
    {
        if(instance==null)
        {
			instance = this;
        }
        else
        {
//			Destroy(gameObject);
        }
    }

	private void OnEnable()
    {
		MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(false); //enable spin on Ui player

	}
	public void OnClickPlay()
	{
		GameConstants.SetRewardShotMode(1);

		GameConstants.SetGameMode("RewardShot2");
	//	GameConstants.SetGameMode("RewardShot");

		RewardedTimeManager.instance.GetRewardKick(); //reset timer and turn off claim button
		
		MenuManager.Instance.LoadLevel();

		RewardCloseClicked();

	}
	public void RewardCloseClicked()
	{
		MainMenuScript.instance.UIPlayerPositionReset_SpinLogicEnableDisable(true); //enable spin on Ui player
		RewardKickCloseState = RewardKickDialogClose.closed;
		//	MenuManager.Instance.UIPlayer.SetActive(true);
		this.gameObject.SetActive(false);

	}
}
