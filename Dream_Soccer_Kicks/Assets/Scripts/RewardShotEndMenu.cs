﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RewardShotEndMenu : MonoBehaviour
{
    public static RewardShotEndMenu instance;
    public Text CoinsText;
    [SerializeField] GameObject RewardImage;
    [SerializeField] Sprite[] RewardImageSprites;
    [SerializeField] Button ContinueButton;
    [SerializeField] Button WatchAdButton;

    public enum WatchAdState
    {
        notwatched, watched
    }
    public enum RewardState
    {
        coin, diamond, fire, energy
    }
    public static RewardState reward;
    public static WatchAdState adstate;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        AdController.instance.showNativeBannerAd();

        PluginCallBackManager.RewardKickRewardAdComplete_ += RewardkickAfterAdWatch;

        if (reward==RewardState.coin)
        {
              RewardImage.GetComponent<Image>().sprite = RewardImageSprites[0];
        }
        else if(reward==RewardState.diamond)
        {
              RewardImage.GetComponent<Image>().sprite = RewardImageSprites[1];
        }
        else if(reward==RewardState.energy)
        {
              RewardImage.GetComponent<Image>().sprite = RewardImageSprites[2];
        }
        else if(reward==RewardState.fire)
        {
              RewardImage.GetComponent<Image>().sprite = RewardImageSprites[3];
        }
        //        ContinueButton.interactable = true;

        RewardShotComplete();
        WatchAdButtonEnabler();

    }
    public void WatchAdButtonEnabler()
    {
        if (adstate == WatchAdState.notwatched)
        {
            WatchAdButton.gameObject.SetActive(true);
        }
        else
        {
            WatchAdButton.gameObject.SetActive(false);

        }

    }

    // Update is called once per frame
    public void RewardShotComplete()                    //to go back to level menu from gameplay scene call this
    {
        ContinueButton.gameObject.SetActive(false);
        StartCoroutine(DelayRewardShotComplete());


    }
    IEnumerator DelayRewardShotComplete()
    {
        if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            Time.timeScale = 1;
            if (reward == RewardState.coin)
            {
                MenuManager.Instance.moveObject("coins", RewardImage);
                MenuManager.Instance.moveConsumable();

                yield return new WaitForSeconds(2f);

                GenericVariables.AddCoins(GameConstants.RewardShotPassCoins);
            }
            else if (reward == RewardState.diamond)
            {
                MenuManager.Instance.moveObject("diamond", RewardImage);
                MenuManager.Instance.moveConsumable();

                yield return new WaitForSeconds(2f);

                GenericVariables.AddDiamonds(GameConstants.RewardShotPassCoins);
            }
            else if (reward == RewardState.energy)
            {
                MenuManager.Instance.moveObject("energy", RewardImage);
                MenuManager.Instance.moveConsumable();

                yield return new WaitForSeconds(2f);

                GenericVariables.AddEnergy(GameConstants.RewardShotPassCoins);
            }
            else if (reward == RewardState.fire)
            {
                MenuManager.Instance.moveObject("fire", RewardImage);
                MenuManager.Instance.moveConsumable();

                yield return new WaitForSeconds(2f);
                GenericVariables.AddFire(GameConstants.RewardShotPassCoins);
            }

            //MenuManager.Instance.moveObject("coins", RewardImage);
            //MenuManager.Instance.moveConsumable();
          
            //yield return new WaitForSeconds(2f);
          
            //GenericVariables.AddCoins(GameConstants.RewardShotPassCoins);
          
            yield return new WaitForSeconds(0.8f);

            ContinueButton.gameObject.SetActive(true);


        }
    }
    public void watchAdClicked()
    {

        GenericVariables.RewardAdIndex = 9;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }

    public void RewardkickAfterAdWatch()
    {
        adstate = WatchAdState.watched;
        WatchAdButtonEnabler();
        GenericVariables.RewardAdIndex = -1;

        GameConstants.SetRewardShotMode(1);

        GameConstants.SetGameMode("RewardShot2");
        //	GameConstants.SetGameMode("RewardShot");

        // RewardedTimeManager.instance.GetRewardKick(); //reset timer and turn off claim button

        LevelEndCelebrationManager.instance.DisableLevelEnd();

     //   this.gameObject.SetActive(false);

        MenuManager.Instance.DisableAll();

        MenuManager.Instance.Loading(true); //Start Loading Screen

        GamePlayManager.instance.ResetScore();           //reset score //important

        MenuManager.Instance.LoadLevel(); 
        this.gameObject.SetActive(false);

    }

    public void ContinueButtonClicked()
    { 
        StartCoroutine(DelayContinueButtonClicked());
    }

    public IEnumerator DelayContinueButtonClicked()
    {
        adstate = WatchAdState.notwatched;
        LevelEndCelebrationManager.instance.DisableLevelEnd();

      //  MenuManager.Instance.DisableAll();                      //disables all menus with delay //including this one

        MenuManager.Instance.settingBackbutton();

        MenuManager.Instance.LevelMenuLoadBool = false;
        //       MenuManager.Instance.UIPlayer.SetActive(true);
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
        MenuManager.Instance.setMain(true);
        //        MenuManager.Instance.setHUD();`
        MenuManager.Instance.Loading(true); //Start Loading Screen
        MenuManager.Instance.sceneChange();                                                  //changing scene to menus
        GamePlayManager.instance.ResetScore();           //reset score //important

        yield return new WaitForSeconds(0.3f);

        this.gameObject.SetActive(false);

    }

    private void OnDisable()
    {

        AdController.instance.hideNativeBannerAd();
        PluginCallBackManager.RewardKickRewardAdComplete_ -= RewardkickAfterAdWatch;

    }

}
