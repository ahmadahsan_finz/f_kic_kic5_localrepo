using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RewardkeyController : MonoBehaviour
{
    public static RewardkeyController instance;
    [Header("Reward Chest box")]
    public GameObject[] PositionPoints;
    public GameObject Rewardkey;
    public GameObject RewardkeyInstance;
    public KeyCollectState chestBoxState;
    public int KeysCollected;
    void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            //Destroy(gameObject);
        }
    }
    public enum KeyCollectState
    {
        uncollected,
        collected
    }

    void Start()
    {
        if (GameConstants.GetLevelNumber() > 2 && GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed() && GameConstants.GetGameMode() == "Classic" && GameConstants.GetCollectedRewardKeys()!=3 || GameConstants.GetLevelNumber()==0 && GameConstants.GetGameMode() == "Classic" && GameConstants.GetCollectedRewardKeys() != 3)   //if playing new level  
        {
            if(GameConstants.GetLevelNumber()% 2 ==0)
            {
                HudManager.instance.UpdateKeyCollection();
                chestBoxState = KeyCollectState.uncollected;
                RewardkeyInstance = Instantiate(Rewardkey);
                KeyPositionSetter();
            }
        }
        else if (GameConstants.GetGameMode() == "Classic")
        {
            HudManager.instance.UpdateKeyCollection();

        }
    }

    public void KeyPositionSetter()
    {
        if (chestBoxState != KeyCollectState.collected && RewardkeyInstance!=null)         //if key isnt collected
        {
            int rand = Random.Range(0, PositionPoints.Length);
           // RewardkeyInstance.transform.localPosition = PositionPoints[rand].transform.localPosition;
            RewardkeyInstance.transform.localPosition = new Vector3(PositionPoints[rand].transform.localPosition.x, PositionPoints[rand].transform.localPosition.y, -0.5f);
        }
    }

    public void KeyCollected()
    {
        KeysCollected = GameConstants.GetCollectedRewardKeys();
        GameConstants.SetCollectedRewardKeys(KeysCollected + 1);
       // KeysCollected = GameConstants.GetCollectedRewardKeys();
        //Debug.Log("KeyCollected = " + KeysCollected);
        chestBoxState = KeyCollectState.collected;
        /* if(GameConstants.GetCollectedRewardKeys()==3)
         {
         }
        */
        HudManager.instance.UpdateKeyCollection(); //update collected keys in hud
        Destroy(RewardkeyInstance);
    }
}
