﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPanelScript : MonoBehaviour
{
    public static SettingsPanelScript instance;
    [SerializeField] Text LanguageText;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
         //   Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        LanguageTextSetter();
    }

    public void LanguageTextSetter()
    {
        switch (GameConstants.GetlanguageIndex())
        {
            case 1:
                LanguageText.text = "English";
                break;
            case 2:
                LanguageText.text = "Español";

                break;
            case 3:
                LanguageText.text = "Italiano";

                break;
            case 4:
                LanguageText.text = "Français";

                break;
            case 5:
                LanguageText.text = "ﯽﺑﺮﻋ";

                break;
            case 6:
                LanguageText.text = "Português";

                break;
            case 7:
                LanguageText.text = "中文";
                break;
            case 8:
                LanguageText.text = "Deutsche";

                break;
            case 9:
                LanguageText.text = "हिन्दी";

                break;

        }
    }
}
