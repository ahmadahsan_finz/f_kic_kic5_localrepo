using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShereLevelRotation : MonoBehaviour
{

    public float PcSpeed = 2000f;
    void Update()
    {

        float rotX = Input.GetAxis("Mouse X") * PcSpeed;
        float rotY = Input.GetAxis("Mouse Y") * PcSpeed;

        Rigidbody rb = GetComponent<Rigidbody>();


        if (Input.GetMouseButton(0))
        {

            rb.AddTorque(Vector3.down * -rotX * Time.deltaTime);
            rb.AddTorque(Vector3.up * rotX * Time.deltaTime);
            rb.AddTorque(Vector3.left * -rotY * Time.deltaTime);
            rb.AddTorque(Vector3.right * rotY * Time.deltaTime);
            rb.angularDrag = 1f;
        }
    }
}
   