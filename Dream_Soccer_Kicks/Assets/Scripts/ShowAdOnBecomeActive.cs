﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAdOnBecomeActive : MonoBehaviour
{
    public static bool FirstOnBecomeActive;
    private void OnEnable()
    {
        /*  if(!FirstOnBecomeActive && GameConstants.getFirstStartUITutorial() != "Yes")
          {
              FirstOnBecomeActive = true;
              StartCoroutine(delayOnBecomeActive());

          }*/

        if (AdConstants.isfirstTime || GameConstants.getFirstStartUITutorial() == "Yes")
        {
            AdConstants.isfirstTime = false;
            AdConstants.currentState = AdConstants.States.OnBecomeActive;
            AdController.instance.ChangeState();
        }
        else
        {
        //    AdConstants.currentState = AdConstants.States.OnMainMenu;
        }
    }
    IEnumerator delayOnBecomeActive()
    {
        yield return new WaitForSeconds(0.5f);
        AdConstants.currentState = AdConstants.States.OnBecomeActive;
        AdController.instance.ChangeState();
    }


}

