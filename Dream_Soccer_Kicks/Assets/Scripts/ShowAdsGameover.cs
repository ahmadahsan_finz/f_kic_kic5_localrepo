﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowAdsGameover : MonoBehaviour
{
    // Start is called before the first frame update
    public static ShowAdsGameover instance;
    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            //Destroy(gameObject);
        }
    }
    public void CallGameoverAd(float delay)
    {
        if (MenuManager.GameOverBoolVideoAd && !LevelCompleteMenu.Watched2xRewardAd && !LevelFailManager.SkipLevelAdWatched)
        {
          //  Debug.Log("ShowAdsGameover OnEnable()");
            StartCoroutine(delayGameOverAd(delay));
            LevelFailManager.ContinueAdWatched = false;             //reseting continue ad enabling

        }
        else
        {       //reseting ads watch
            LevelCompleteMenu.Watched2xRewardAd = false;
            LevelFailManager.ContinueAdWatched = false;             //reseting continue ad enabling
            LevelFailManager.SkipLevelAdWatched = false;

        }
    }
    IEnumerator delayGameOverAd(float delay)
    {
        MenuManager.GameOverBoolVideoAd = false;
     //   Debug.Log("ShowAdsGameover android 1");

        // yield return new WaitForSeconds(2.25f);
        yield return new WaitForSeconds(delay);
      //  Debug.Log("ShowAdsGameover android 2");

        if (/*GameConstants.getStartGamePlayTutorial() != "Yes" && */ GameConstants.GetGameMode()=="Classic" /* && !GameConstants.CanShowRateUsBool && GameConstants.GetLevelNumber()!=4*/)  //not showing add on multiplayer offer dialog         //if its first time than take back to main menu to continue tutorial
        {
//#if UNITY_IOS
          /*  Debug.Log("ShowAdsGameover iphone");
            if(GameConstants.GetLevelNumber() % 2==0)
            {
                if(GameConstants.GetLevelNumber() != 2)
                {
                AdConstants.currentState = AdConstants.States.OnGameOver;
                AdController.instance.ChangeState();

                }
            }
            else //if(GameConstants.GetLevelNumber() % 3 == 0)
            {
                AdConstants.currentState = AdConstants.States.OnHalfSummaryMenu;
                AdController.instance.ChangeState();
            }*/
      
//#elif UNITY_ANDROID || UNITY_EDITOR
          //  Debug.Log("ShowAdsGameover android");
            if (GameConstants.GetLevelNumber() % 2 != 0) //ad at every odd level
            {
              //   Debug.Log("ShowAdsGameover android 3");
                /*if (GameConstants.GetLevelNumber() != 2)
                */
                AdConstants.currentState = AdConstants.States.OnGameOver;
                    AdController.instance.ChangeState();

                    
            }
            else //if(GameConstants.GetLevelNumber() % 3 == 0)
            {
                AdConstants.currentState = AdConstants.States.OnHalfSummaryMenu;
                AdController.instance.ChangeState();
            }
           
            //#endif
        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
            Debug.Log("ShowAdsGameover Missions delayGameOverAd()");
        ////    if (GameConstants.GetMissionNumber() % 2 == 0)
         //   {

                AdConstants.currentState = AdConstants.States.OnGameOver;
                AdController.instance.ChangeState();
        //    }
        /*    else //if(GameConstants.GetLevelNumber() % 3 == 0)
            {
                AdConstants.currentState = AdConstants.States.OnHalfSummaryMenu;
                AdController.instance.ChangeState();
            }*/
        }
        else if(GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2")
        {
            Debug.Log("ShowAdsGameover Multiplayer1 delayGameOverAd()");
          //  if (GameConstants.GetMultplayerMode1LevelCountNumber() % 2 == 0)
        //    {

                AdConstants.currentState = AdConstants.States.OnGameOver;
                AdController.instance.ChangeState();
           // }
          /*  else //if(GameConstants.GetLevelNumber() % 3 == 0)
            {
                AdConstants.currentState = AdConstants.States.OnHalfSummaryMenu;
                AdController.instance.ChangeState();
            }*/
        }
	    else if (GameConstants.GetGameMode() == "RewardShot2")
        {
            Debug.Log("ShowAdsGameover RewardShot delayGameOverAd()");
            /*if (GameConstants.GetRewardKickCountNumber() % 2 == 0)
            {*/

                AdConstants.currentState = AdConstants.States.OnGameOver;
                AdController.instance.ChangeState();
           // }
            /*else //if(GameConstants.GetLevelNumber() % 3 == 0)
            {
                AdConstants.currentState = AdConstants.States.OnHalfSummaryMenu;
                AdController.instance.ChangeState();
            }*/
        }
        /* if (GameConstants.getFirstStartUITutorial() != "Yes")           //if its first time than take back to main menu to continue tutorial
        {
            Debug.Log("ShowAdsGameover delayGameOverAd()");

            AdConstants.currentState = AdConstants.States.OnGameOver;
            AdController.instance.ChangeState();
        }*/
    }
}
