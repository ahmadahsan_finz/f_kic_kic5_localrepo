﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SphereRotate : MonoBehaviour
{
    float PcSpeed = 5f;
    /*Gilani void OnMouseDrag()
  {
      float rotX = Input.GetAxis("Mouse X") * PcSpeed;
      float rotY = Input.GetAxis("Mouse Y") * PcSpeed;


      transform.Rotate(Vector3.down, -rotX);
      transform.Rotate(Vector3.up, rotX);
      transform.Rotate(Vector3.left, -rotY);
      transform.Rotate(Vector3.right, rotY);

  }*/

    void Update()
    {
        float rotX = Input.GetAxis("Mouse X") * PcSpeed;
        float rotY = Input.GetAxis("Mouse Y") * PcSpeed;

        Rigidbody rb = GetComponent<Rigidbody>();


        if (Input.GetMouseButton(0))
        {

            rb.AddTorque(Vector3.down * -rotX * Time.deltaTime);
            rb.AddTorque(Vector3.up * rotX * Time.deltaTime);
            rb.AddTorque(Vector3.left * -rotY * Time.deltaTime);
            rb.AddTorque(Vector3.right * rotY * Time.deltaTime);
            rb.angularDrag = 1f;
        }
    }
}