﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class SplashScreenPlay : MonoBehaviour {

    public GameObject logo ,loadingMenu;
    public RawImage rawImage;
    public VideoPlayer videoPlayer;
    public AudioSource audioSource;

 //   public GameObject ads;
    private void Awake()
    {
     //   ads.SetActive(false);
       // logo.SetActive(true);
    }
    // Use this for initialization
    void Start()
    {
        StartCoroutine(SplashPlayer());
	}
	
	IEnumerator SplashPlayer()
    {
        videoPlayer.Prepare();
        
        while(!videoPlayer.isPrepared)
        {
            yield return new WaitForSeconds(2f);
            break;
        }

        rawImage.texture = videoPlayer.texture;
     //   logo.SetActive(false);
        videoPlayer.Play();

		//if(ControllerScript.isSoundOn())
        audioSource.Play();

        Invoke("bye", 5f);
    }
    public void bye() {
//        UnityEngine.Analytics.Analytics.CustomEvent("Game Start", new System.Collections.Generic.Dictionary<string, object> {
//            {"Start", 1} }
//      );
       // ads.SetActive(true);
		Instantiate(loadingMenu);
		SceneManager.LoadSceneAsync(1);
        Destroy(this.gameObject);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
