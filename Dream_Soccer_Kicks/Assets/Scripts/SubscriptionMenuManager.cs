using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SubscriptionMenuManager : MonoBehaviour
{
    public GameObject BackButton;

    private void OnEnable()
    {
        BackButton.SetActive(false);

        Invoke("EnableBackButton", 3f);
    }

    public void EnableBackButton()
    {
        BackButton.SetActive(true);
    }
    public void OnClickSubscriptionButton()
    {
        ShopScript.ProductId = Purchaser.Instance.InAppIds_Subscription;
        Purchaser.Instance.BuyProductID(ShopScript.ProductId, PurchaseSubscription);
    }
    public void PurchaseSubscription()
    {
        if (ShopScript.ProductId == Purchaser.Instance.InAppIds_Subscription.ToString()) // "")
        {
            GenericVariables.AddCoins(500);
            GenericVariables.AddDiamonds(500);
            GenericVariables.AddFire(5);

            int rand = UnityEngine.Random.Range(0, 3);

            switch (rand)
            {
                case 0:
                    CustomizationScript.instance.UnlockBallsForSubscription();
                    GameConstants.SetNewNotificationFootballMenu(1);
                    GameConstants.SetNewNotificationCustomizationMenu(1);
                    //        CustomiazationMenuScript.instance.checkNotificationOnIcons();
                    BottomTabPanelScript.instance.EnableNotificationIndicatorOnCustomizationButton();


                    break;
                case 1:
                    CustomizationScript.instance.UnlockKitShirtForSubscription();
                    GameConstants.SetNewNotificationFootballMenu(1);
                    GameConstants.SetNewNotificationCustomizationMenu(1);
                    //    CustomiazationMenuScript.instance.checkNotificationOnIcons();
                    BottomTabPanelScript.instance.EnableNotificationIndicatorOnCustomizationButton();


                    break;

                case 2:
                    CustomizationScript.instance.UnlockKitShortsForSubscription();
                    GameConstants.SetNewNotificationFootballMenu(1);
                    GameConstants.SetNewNotificationCustomizationMenu(1);
                    //  CustomiazationMenuScript.instance.checkNotificationOnIcons();
                    BottomTabPanelScript.instance.EnableNotificationIndicatorOnCustomizationButton();


                    break;
            }

          // ShopScript.instance.RemoveAds();
          //  Purchaser.Instance.BuyProductID(Purchaser.Instance.InAppIds_NonConsumable[0], AdConstants.disableAds);
            AdConstants.disableAds();

            Purchaser.Instance.GenerateProductSubscriptionReceipt(ShopScript.ProductId);
            PlayerPrefs.SetString("COINPERDAY", DateTime.Now.ToString());

            GameConstants.SetSubscriptionStatus("Yes");

            if (ShopScript.instance != null)
            {
              ShopScript.instance.EnableDisableSubscriptionButton(false);
            }

            this.gameObject.SetActive(false);

            Invoke("LoadSubscriptionRewardShowMenu", 1.5f);  //show what reward he got

        }
    }
    public void LoadSubscriptionRewardShowMenu()
    {

            MenuManager.Instance.SubscriptionRewardShowMenu.SetActive(true); //show what reward he got

    }
    public void OnClickTermConditionsButton()
    {
        Application.OpenURL("https://drive.google.com/file/d/1Pu2CqBKbx3h2HpYk47iXLiKdvpJ_Ov2b/view");
    }
    public void OnClickPrivacyPolicyButton()
    {
        Application.OpenURL("https://drive.google.com/file/d/1Pwk6bhl7SzbrwnHWjd74gDAFxVH54oNl/view");

    }
    public void OnClickCloseButton()
    {
        this.gameObject.SetActive(false);
    }
    public void OnClickRestoreButton()
    {
        Purchaser.Instance.RestorePurchases();
    }

}
