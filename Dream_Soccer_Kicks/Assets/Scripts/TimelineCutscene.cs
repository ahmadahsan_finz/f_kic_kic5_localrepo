﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class TimelineCutscene : MonoBehaviour
{
    public static TimelineCutscene instance;
    public PlayableDirector playableDirector;
    public GameObject BallReal;
    public GameObject BallCutscene;
    public GameObject CutscenePanel;
    public GameObject[] CutsceneCams;
    public GameObject[] HudPanels;
    public GameObject SkipButtonObj1;
    public GameObject SkipButtonObj2;
   // public GameObject TimeLineObjects; Gilani
    public GameObject YellowCardTL;
    public GameObject MainCameraBrain;
    public GameObject MainCamera;
    public GameObject PlayerObject;
    public GameObject CharactersObject;//Gilani
    public GameObject GoalKeeperObject;
    Coroutine DisableSkipButton1Coroutine;

    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void OnEnable()
    {
       
        playableDirector.stopped += DisableCutscene;

    }


   
    // Start is called before the first frame update
    void Start()
    {

        if (GameConstants.GetLevelNumber() == 1 && GameConstants.GetGameMode()=="Classic")
        {

            //PlayerObject = Instantiate(MenuManager.Instance.Players_CharacterArray[GameConstants.GetSelectedPlayer()]);
            PlayerObject = GamePlayManager.instance.Player;
            PlayerObject.transform.localPosition = new Vector3(-0.48f, 0.01268f, -19.11f);
            PlayerObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
            PlayerObject.transform.localScale = new Vector3(1.15f, 1.15f, 1.15f);

          /*  CharactersObject = GamePlayManager.instance.Player;//GamePlayManager Gilani
            CharactersObject.transform.localPosition = new Vector3(-0.19f, 0.01168f, 9.999999f);
            CharactersObject.transform.localRotation = Quaternion.Euler(0, 0, 0);
            CharactersObject.transform.localScale = new Vector3(1.15f, 1.15f, 1.15f); *///Gilani

            GoalKeeperObject = GamePlayManager.instance.GKPlayer;
            PlayCutscene();

            SetCharacter("KickPlayer", PlayerObject);
           // SetCharacter("CharacterPlayer", CharactersObject); //Gilani
            SetCharacter("GKPlayer", GoalKeeperObject);


            //      StartCoroutine(Disableskined meshpButton1());
            //    StartCoroutine("EnableSkipButton2", 22f);
            //    AdController.instance.ShowBannerAd();
         /*   GamePlayManager.instance.CrowdSets[0].SetActive(true);
            GamePlayManager.instance.CrowdSets[1].SetActive(true);
            GamePlayManager.instance.CrowdSets[2].SetActive(true);
            GamePlayManager.instance.CrowdSets[3].SetActive(true);*/
        }
        else
        {
            Debug.Log("NoCutsceneLevel");
            NoCutsceneLevel();
       /*     GamePlayManager.instance.CrowdSets[0].SetActive(true);
            GamePlayManager.instance.CrowdSets[1].SetActive(false);
            GamePlayManager.instance.CrowdSets[2].SetActive(false);
            GamePlayManager.instance.CrowdSets[3].SetActive(false);*/

        }

    }
    public void SetCharacter(string characterType, GameObject character)
    {
        foreach (var playableAssetOutput in playableDirector.playableAsset.outputs)
        {
            //Debug.Log(playableAssetOutput.streamName);
            if (playableAssetOutput.streamName.StartsWith(characterType))
            {
                //Debug.Log(character.name);
                //Debug.Log(temp.name);
/*                character.transform.localPosition = temp.localPosition;
                character.transform.localRotation = temp.localRotation;
                character.transform.localScale = temp.localScale;
                character.GetComponent<Animator>().applyRootMotion = false;*/

                playableDirector.SetGenericBinding(playableAssetOutput.sourceObject, character);
           //     Destroy(temp.gameObject);
            }

            //Debug.Log(playableAssetOutput.sourceObject);
        }
    }
        public void PlayCutscene()
    {
        SkipButtonObj1.SetActive(false);
        SkipButtonObj2.SetActive(false);
        StartCoroutine(EnableSkipButton1());
        Debug.Log("PlayCutscene 1");
        MainCameraBrain.SetActive(true);
        MainCamera.SetActive(false);
        MenuManager.Instance.setSettingsButton(false);


        //Aqsa Gilani
        // TimeLineObjects.SetActive(true);
        YellowCardTL.SetActive(true);

        GameConstants.CutsceneIsOn = true;
        playableDirector.gameObject.SetActive(true);

        for (int i = 0; i < CutsceneCams.Length; i++)
        {
            CutsceneCams[i].SetActive(true);
         
        }
        Shoot.share._enableTouch = false;
        BallReal.SetActive(false);
        BallCutscene.SetActive(true);
        CutscenePanel.SetActive(true);
        for (int a = 0; a < HudPanels.Length; a++)
        {
       //     Debug.Log("HudPanels disabling");
            HudPanels[a].SetActive(false);


        }
        Debug.Log("PlayCutscene 2");

        playableDirector.Play();
    }

    public void SkipButton()
    {

        MainCamera.SetActive(true);

        Shoot.share._enableTouch = true;
        GameConstants.CutsceneIsOn = false;
        BallReal.SetActive(true);
        BallCutscene.SetActive(false);
        CutscenePanel.SetActive(false);

        for (int i = 0; i < CutsceneCams.Length; i++)
        {
            CutsceneCams[i].SetActive(false);
           

        }
        for (int a = 0; a < HudPanels.Length; a++)
        {
            HudPanels[a].SetActive(true);


        }
        //  playableDirector.Stop();
        CameraManager.share.reset();
        GoalKeeperObject.transform.position = new Vector3(0, 0, 0);
        GoalKeeperObject.transform.rotation = Quaternion.Euler (0, 180, 0);
        GoalKeeper.share.reset();
        Shoot.share.reset();
        playableDirector.gameObject.SetActive(false);

        /* GamePlayManager.instance. GroundlinesArray[0].SetActive(false);
         GamePlayManager.instance.GroundlinesArray[1].SetActive(true);*/


        //    Debug.Log("skipbutton");

        // TimeLineObjects.SetActive(false);    //Aqsa Gilani
        YellowCardTL.SetActive(false);
       // SkipToTutorial();

    }
    IEnumerator EnableSkipButton1()
    {
        yield return new WaitForSeconds(1);
        SkipButtonObj1.SetActive(true);
        DisableSkipButton1Coroutine = StartCoroutine(DisableSkipButton1());

    }
    IEnumerator DisableSkipButton1()
    {
        yield return new WaitForSeconds(18);
        SkipButtonObj1.SetActive(false);
       StartCoroutine(EnableSkipButton2());


    }
    IEnumerator EnableSkipButton2()
    {
        yield return new WaitForSeconds(4);
        SkipButtonObj2.SetActive(true);
        StartCoroutine(DisableSkipButton2());
    } 
    IEnumerator DisableSkipButton2()
    {
        yield return new WaitForSeconds(5f);
        SkipButtonObj2.SetActive(false);
    }
    
    public void OnClickSkipButton1()
    {
      //  AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.Cutscene_Skip1);
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.Cutscene_Skip1, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);

        playableDirector.time = 24f;
        SkipButtonObj1.SetActive(false);
        StopCoroutine(DisableSkipButton1Coroutine);

        StartCoroutine(EnableSkipButton2());

    }
    public void OnClickSkipButton2()
    {
       // AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.Cutscene_Skip2);
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.Cutscene_Skip2, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);
    
        playableDirector.time = 36f;
       

        SkipButtonObj2.SetActive(false);

    }
    public void DisableCutscene(PlayableDirector aDirector)
    {
        if (playableDirector == aDirector)
        {
            //     Debug.Log("DisableCutscene");
          // AdController.instance.HideBannerAd();
/*
            GamePlayManager.instance.CrowdSets[0].SetActive(true);
            GamePlayManager.instance.CrowdSets[1].SetActive(false);
            GamePlayManager.instance.CrowdSets[2].SetActive(false);
            GamePlayManager.instance.CrowdSets[3].SetActive(false);*/
            MenuManager.Instance.Loading(true);

            AdConstants.currentState = AdConstants.States.OnGameOver;   //intersitial ad on cutscene end
            AdController.instance.ChangeState();

            SkipButton();
        }
    }
    public void NoCutsceneLevel()
    {
        GameConstants.CutsceneIsOn = false;
        MainCamera.SetActive(true);

        BallReal.SetActive(true);
        BallCutscene.SetActive(false);
        CutscenePanel.SetActive(false);
        //Aqsa Gilani
        //  TimeLineObjects.SetActive(false);
        YellowCardTL.SetActive(false);
        if (GameConstants.getStartGamePlayTutorial() != "Yes" || GameConstants.GetLevelNumber() > 2)
        {
            MenuManager.Instance.setSettingsButton(true);

        }

    }
    public void SkipToTutorial()
    {

     //   GamePlayManager.instance.GroundlinesArray[0].SetActive(true);
       // GamePlayManager.instance.GroundlinesArray[1].SetActive(false);

        TutorialManager.instance.TurnOnGameplayTutorial();
        if (GameConstants.getStartGamePlayTutorial() != "Yes" || GameConstants.GetLevelNumber() > 2)
        {
            MenuManager.Instance.setSettingsButton(true);

        }
    }

    void OnDisable()
    {
       // Destroy(PlayerObject);
      //  GamePlayManager.instance.Player.SetActive(true);

        playableDirector.stopped -= DisableCutscene;
    }
}
  
