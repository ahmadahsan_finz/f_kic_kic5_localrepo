﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TournamentHandler : MonoBehaviour
{

    public static TournamentHandler instance;
    public int maxTurn = 4;
    public int Count_penalties = 0;
    public int PlayerScore = 0;
    public int AI_Score = 0;
    public bool QuarterFinal;
    public bool SemiFinal;
    public bool Final;
    public float RandomRange;
    public bool boolPlayerWon, boolAiWon, boolDraw;
    public GameObject GameplayManager;
    public GameObject[] GoalsPlayer;
    public GameObject[] GoalsAI;
    public GameObject[] TournamentRoundScreens;
    public GameObject TournamentLevelPassScreen;
    public GameObject TournamentLevelFailScreen;
    public Text[] TournamentStageText;
    public Coroutine TournamentCompleteCoroutine;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        //GameplayManager.SetActive(false);
        if (GameConstants.GetMode() == 10 && GameConstants.GetGameMode()=="Classic")
        {
            OnTournamentStart();
            GoalDetermine.EventFinishShoot += EventShootFinish;

           
        }


    }
    public void OnTournamentStart()
    {
        HudManager.instance.GoalTournamentReset();                          //disables goal sprites //reset goals missed and scored images from panel

        //      HudManager.instance.updateTournamentScores();
        QuarterFinal = true;
        HudManager.instance.updateTournamanentRoundName();
        //MenuManager.Instance.ShowPopUp(10);     
        MenuManager.Instance.ShowTournamentsPopUp(10);
        //  TournamentStageText = GameObject.FindGameObjectWithTag("TournamentStage");
    }
    private void OnDestroy()
    {
        GoalDetermine.EventFinishShoot -= EventShootFinish;
    }

    private void EventShootFinish(bool isGoal, Area area)
    {

        if (isGoal)
        {
            // Count_penalties++;
          GamePlayManager.instance.GoalScoreFloatingTextShow();

            PlayerScore++;
            GamePlayManager.instance.GoalNetAnimationCaller(area);
            GamePlayManager.instance.CelebrationParticles.SetActive(true);

            //       GoalsPlayer[Count_penalties].SetActive(true);
            HudManager.instance.PlayerGoalTournamentUpdate(Count_penalties);          //penalty scored display goal icon
        }
        else
        {
            GamePlayManager.instance.GoalMissFloatingTextShow();

            HudManager.instance.PlayerGoalMissTournamentUpdate(Count_penalties);        //penalty missed display goal icon

        }
        AiShoot();
    }

    public void AiShoot()
    {
        if (QuarterFinal)
        {
            //        Debug.Log("inside quarterfinal");

            RandomRange = Random.Range(0, 4);
            if (RandomRange == 1)
            {

                AI_Score++;

                HudManager.instance.AIGoalTournamentUpdate(Count_penalties);              //penalty scored display goal icon

            }
            else
            {
                HudManager.instance.AIGoalMissTournamentUpdate(Count_penalties);         //penalty missed display goal icon

            }
        }
        else if (SemiFinal)
        {

            RandomRange = Random.Range(0, 3);
            if (RandomRange == 1)
            {

                AI_Score++;

                HudManager.instance.AIGoalTournamentUpdate(Count_penalties);           //penalty scored display goal icon

            }
            else
            {
                HudManager.instance.AIGoalMissTournamentUpdate(Count_penalties);         //penalty missed display goal icon

            }

        }
        else if (Final)
        {
            RandomRange = Random.Range(0, 2);
            if (RandomRange == 1)
            {
                AI_Score++;
                HudManager.instance.AIGoalTournamentUpdate(Count_penalties);            //penalty scored display goal icon
            }
            else
            {
                HudManager.instance.AIGoalMissTournamentUpdate(Count_penalties);          //penalty missed display goal icon
            }

        }

        NextTurn();
    }
    public void NextTurn()
    {
        Count_penalties++;
        if (Count_penalties > maxTurn)
        {
            if (Final)          //if was playing final
            {
                RoundEnd();
            }
            else
            {
                ShowTournamentLevelPassFailScreen();            //if was playing any other final
            }
        }

    //    HudManager.instance.updateTournamentScores();

    }
    public void Reset()
    {
        Shoot.share.reset();
    }
    public void ShowTournamentLevelPassFailScreen()
    {
        //  TournamentLevelPassScreen.SetActive(true);
        //     MenuManager.Instance.ShowPopUp(13);


        if (PlayerScore > AI_Score)
        {
            LevelEndCelebrationManager.instance.Screen.SetActive(true);
            StartCoroutine(delayTournamentLevelPassedPopUp());
        }
        else if (PlayerScore == AI_Score)
        {
            DrawHappened();             //draw happenned dont show any dialog

        }
        else
        {
            LevelEndCelebrationManager.instance.Screen.SetActive(true);
            GamePlayManager.instance.LevelEndBool = true;

            // StartCoroutine(delayTournamentFailed());
            TournamentFailed();
           // MenuManager.Instance.ShowTournamentsPopUp(14);       //tournament failed pop up

        }
    }


    public void RoundEnd()
    {
      //  HudManager.instance.GoalTournamentReset();                  //reset goals missed and scored images from panel


        if (PlayerScore > AI_Score)
        {
            LevelEndCelebrationManager.instance.Screen.SetActive(true);

            //Player won
            if (QuarterFinal)
            {
                Debug.Log("delaySemiFinalPopUpShow");
                StartCoroutine(delaySemiFinalPopUpShow());

            }
            else if (SemiFinal)
            {
                Debug.Log("delayFinalPopUpShow");
                StartCoroutine(delayFinalPopUpShow());

            }
            else if (Final)
            {
                Debug.Log("Final");
                GamePlayManager.instance.LevelEndBool = true;
                //  StartCoroutine(delayTournamentWon());
                TournamentWon();
            }
        }
        else if (AI_Score > PlayerScore)
        {
            //AI won
            GamePlayManager.instance.LevelEndBool = true;
            // StartCoroutine(delayTournamentFailed());
            TournamentFailed();

        }
        else if (PlayerScore == AI_Score)
        {
            //draw
            DrawHappened();
        }
    }

    public void DrawHappened()
    {
        StartCoroutine(delayDrawHappened());
    }

    public IEnumerator delayDrawHappened()
    {
        yield return new WaitForSeconds(1);
        boolDraw = true;
        HudManager.instance.GoalTournamentReset();              //reset goals missed and scored images from panel
        Count_penalties = 0;
        PlayerScore = 0;
        AI_Score = 0;
    }

    IEnumerator delayTournamentLevelPassedPopUp()
    {
        Shoot.share._enableTouch = false;
        yield return new WaitForSeconds(0.5f);

        MenuManager.Instance.ShowTournamentsPopUp(13);

        if (QuarterFinal)
        {
            //GameObject TournamentStageText= GameObject.FindGameObjectWithTag("TournamentStage") as GameObject;


         //   GameObject.FindGameObjectWithTag("TournamentStage").GetComponent<Text>().text = "SEMIFINAL";
            TournamentLevelPassedMenu.instance. TournamentStageText[0].gameObject.SetActive(true);
            TournamentLevelPassedMenu.instance.TournamentStageText[1].gameObject.SetActive(false);
            TournamentLevelPassedMenu.instance.TournamentStageText[2].gameObject.SetActive(false);

        }
        else if (SemiFinal)
        {
           // GameObject.FindGameObjectWithTag("TournamentStage").GetComponent<Text>().text = "FINAL";
            TournamentLevelPassedMenu.instance.TournamentStageText[0].gameObject.SetActive(false);
            TournamentLevelPassedMenu.instance.TournamentStageText[1].gameObject.SetActive(true);
            TournamentLevelPassedMenu.instance.TournamentStageText[2].gameObject.SetActive(false);
        }
        //tournament level pass pop up
    }
    public void TournamentFailed()
    {

        MenuManager.Instance.AnalyticsCallingEndLose();

        Shoot.share._enableTouch = false;

        LevelEndCelebrationManager.instance.GameLost();

        TournamentCompleteCoroutine = StartCoroutine(delayTournamentFailed(3.5f));

    }
    public IEnumerator delayTournamentFailed(float timer)
    {
      //  LevelEndCelebrationManager.instance.GameLost();
        yield return new WaitForSeconds(timer);
        //yield return new WaitForSeconds(0.5f);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("LevelFail");

        }
        //  MenuManager.Instance.ShowTournamentsPopUp(14);               //tournament failed pop up
          MenuManager.Instance.ShowTournamentsPopUp(5);               //level failed pop up

        boolAiWon = true;
      //  HudManager.instance.GoalTournamentReset();                  //reset goals missed and scored images from panel

    }
    public void TournamentWon()
    {
        MenuManager.Instance.AnalyticsCallingEndWin();

        Shoot.share._enableTouch = false;
        if (GameConstants.TrophiesLevelList.Contains(GameConstants.GetLevelNumber()) && GameConstants.GetLevelNumber() == GenericVariables.getTotalLevelsPassed()) //if level is trophy and played first time
        {
            LevelEndCelebrationManager.instance.TrophyWon();
            TournamentCompleteCoroutine = StartCoroutine(delayTournamentWon(13f));
        }
        else
        {
            LevelEndCelebrationManager.instance.GameWon();
            TournamentCompleteCoroutine = StartCoroutine(delayTournamentWon(3.5f));

        }
    }
    public IEnumerator delayTournamentWon(float timer )
    {
        /*MenuManager.Instance.AnalyticsCallingEndWin();

        Shoot.share._enableTouch = false;
       */
        yield return new WaitForSeconds(timer);
        //yield return new WaitForSeconds(0.5f);
        if (SoundManagerNew.Instance != null)
        {
            //SoundManagerNew.Instance.playSound("TournamentWon");
            SoundManagerNew.Instance.playSound("LevelWin");

        }
        Debug.Log("TOurnament won");
       
        //MenuManager.Instance.ShowTournamentsPopUp(15);                        //Tournament won pop up show

        Final = false;
        //tounamnet won
        boolPlayerWon = true;
        Count_penalties = 0;
        PlayerScore = 0;
        AI_Score = 0;

  //      HudManager.instance.GoalTournamentReset();                  //reset goals missed and scored images from panel
        GamePlayManager.instance.CoinsStarsCollectedOnTournamentWon();
        
        GamePlayManager.instance.UnlockNextLevel();

    }

    IEnumerator delayFinalPopUpShow()
    {
        Shoot.share._enableTouch = false;

     //   yield return new WaitForSeconds(5f);
        yield return new WaitForSeconds(0.1f);
        //      MenuManager.Instance.ShowPopUp(12);

        MenuManager.Instance.ShowTournamentsPopUp(12);                       //Final pop up show

        SemiFinal = false;
        Final = true;
        Count_penalties = 0;
        PlayerScore = 0;
        AI_Score = 0;
        HudManager.instance.GoalTournamentReset();                  //reset goals missed and scored images from panel

        HudManager.instance.updateTournamanentRoundName();

    }

    IEnumerator delaySemiFinalPopUpShow()
    {
        Shoot.share._enableTouch = false;

        yield return new WaitForSeconds(0.1f);

        //        MenuManager.Instance.ShowPopUp(11);
        MenuManager.Instance.ShowTournamentsPopUp(11);           //SemiFinal pop up show

        QuarterFinal = false;
        SemiFinal = true;
        Count_penalties = 0;
        PlayerScore = 0;
        AI_Score = 0;
        HudManager.instance.GoalTournamentReset();                  //reset goals missed and scored images from panel

        HudManager.instance.updateTournamanentRoundName();
    }

    public void StopDelayTournamentCoroutine()
    {
       if(TournamentCompleteCoroutine!=null)
            StopCoroutine(TournamentCompleteCoroutine);

        
    }
}
