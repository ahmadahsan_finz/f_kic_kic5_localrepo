using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TournamentLevelPassedMenu : MonoBehaviour
{
    public static TournamentLevelPassedMenu instance;
    public Text[] TournamentStageText;

    private void Awake()
    {
        if (instance == null)
        {

            instance = this;

        }
        else
        {
            //Destroy(gameObject);
        }

    }
}
