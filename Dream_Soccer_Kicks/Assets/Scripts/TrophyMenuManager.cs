using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophyMenuManager : MonoBehaviour
{
    public GameObject[] TrophyImageArray;
    int UnlockedTrophies;
    public ScrollRect scrollRect;
    public GameObject Target;
    [SerializeField] Text[] AccomplishedTextDetails;
    public Text TrophyAchievedNumberText;
    public GameObject DetailsText;
    public GameObject DetailsTextForLvl2;
 //   public Text SeasonNumberText;

    private void OnEnable()
    {

        EnablerDisableAccomplishmentDetailsText(true);   //enable accomplishment text
        EnableDisableDetailsText(false);
        EnableDisableDetailsTextForLevel2(false);
        UnlockedTrophies = GameConstants.GetTrophyUnlockedNumber();
        if (UnlockedTrophies >= 0)
        {
            for (int i = 0; i <= UnlockedTrophies; i++)
            {
                TrophyImageArray[i].SetActive(true);
            }
        }

        // for scroll snap
        if (UnlockedTrophies < 0)  //initailly trophies are -1
        {
            UnlockedTrophies = 0;
            TrophyAchievedNumberText.text = UnlockedTrophies.ToString();        //number of trophies unlocked

        }
        else
        {

            TrophyAchievedNumberText.text = (UnlockedTrophies + 1).ToString();        //number of trophies unlocked
        }

        Target = TrophyImageArray[UnlockedTrophies];
        SetScrollPosition(Target);
    }
    private void SetScrollPosition(GameObject target)
    {
        //Canvas.ForceUpdateCanvases();
        scrollRect.content.anchoredPosition = Vector2.up;
        Vector2 pos = (Vector2)scrollRect.content.transform.InverseTransformPoint(scrollRect.content.position)
                        - (Vector2)scrollRect.transform.InverseTransformPoint(target.GetComponent<RectTransform>().position);
        scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, pos.y);
    }
    public void BackButtonPressed()
    {
        this.gameObject.SetActive(false);
    }

    public void OnTrophyClick(int TrophyNumber)
    {
        if(TrophyNumber==-1)
        {
            EnablerDisableAccomplishmentDetailsText(false);
            EnableDisableDetailsText(false);
            EnableDisableDetailsTextForLevel2(true);

            // SeasonNumberText.text = (TrophyNumber + 1).ToString();
        }
        else
        {
            EnablerDisableAccomplishmentDetailsText(false);
            EnableDisableDetailsText(true);
            EnableDisableDetailsTextForLevel2(false);

            // DetailsTextArray[TrophyNumber].SetActive(true);
           
            //SeasonNumberText.text = (TrophyNumber + 1).ToString();

        }
    }
    void EnableDisableDetailsText(bool value) //disable all details text
    {
        
        //    SeasonNumberText.gameObject.SetActive(value);
            DetailsText.SetActive(value);
       
    } 
    void EnableDisableDetailsTextForLevel2(bool value) //disable all details text
    {
        
          //  SeasonNumberText.gameObject.SetActive(value);
            DetailsTextForLvl2.SetActive(value);
       
    }
    void EnablerDisableAccomplishmentDetailsText(bool Value) //disable all details text
    {
        for (int i = 0; i < AccomplishedTextDetails.Length; i++)
        {
            AccomplishedTextDetails[i].gameObject.SetActive(Value);

        }
    }
}
