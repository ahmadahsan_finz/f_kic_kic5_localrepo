using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TrophyUnlockMenu : MonoBehaviour
{
    [SerializeField] Image TrophyImagePreview;
    [SerializeField] Sprite[] TrophyImageArray;
    int TrophyUnlockedNumber;
    private void OnEnable()
    {
        TrophyUnlockedNumber = GameConstants.GetTrophyUnlockedNumber();
        if(TrophyUnlockedNumber<0)
        {
            TrophyUnlockedNumber = 0;
        }
        TrophyImagePreview.sprite = TrophyImageArray[TrophyUnlockedNumber];    //assigning unlocked trophy on unlocked menu 
            
    }

    public void OnClickGoToTrophy()
    {
        this.gameObject.SetActive(false);
        MenuManager.Instance.TrophyMenu.SetActive(true);
    }
    public void OnClickNotNow()
    {
        this.gameObject.SetActive(false);
    }
}
