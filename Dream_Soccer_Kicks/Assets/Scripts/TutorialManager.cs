﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static TutorialManager instance;
    public GameObject TutorialScreen;
    public GameObject BlackScreen;
    public GameObject HandObject;
    public GameObject FireObject;
    public GameObject FireObject2;
    public GameObject ExtraBallObject;
    public GameObject AreaShoot;
    public Text TutorialText; 
    public Animator HandAnimation;
    public Animator HandAnimation2;
    public int tutorialNumber = 2;
    public bool FireTutorialBool;
 //   public Renderer[] scoreCards;
    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        if (GameConstants.getFirstStartUITutorial() == "Yes")
        {
            GameConstants.setFirstStartUITutorial("No");        //disable UI Tutorial on gameplay reach
        //    AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.Tutorial_Menu_Completed);
            AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.Tutorial_Menu_Completed, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);
            UITutorialManager.instance.DisableBlackShader();                //disable UI tutorial
        }
       

        // HandAnimation = HandObject.GetComponent<Animator>();
        FireObject2.SetActive(false);
        if(GenericVariables.getFireRemaining()>1 && GameConstants.GetLevelNumber()==2 && GameConstants.getStartGamePlayTutorial()=="Yes")
        {
            TurnOnFireButtonTutorial();
       //     MenuManager.Instance.settingsButton.SetActive(false);
        }
    }
    public void TutorialDecider()
    {                                   // tutorialNumber is being incremented from ManageGoal() in GameplayManager.cs after goal scoring //ahmad
          if(tutorialNumber == 2)
        {
            Debug.Log("tutorialNumber is " + tutorialNumber);
            Invoke( "TurnOnGameplayTutorial", 0.8f);

        }
      /*  else if(tutorialNumber == 2)
        {
            Debug.Log("tutorialNumber is " + tutorialNumber);
            Invoke("GameplayTutorialSwing", 0.8f);
          //  GameplayTutorialSwing();
        }*/
        else if(tutorialNumber == 3)
        {
           // Debug.Log("tutorialNumber is " + tutorialNumber);

            Invoke("GameplayTutorialScoreCardsShow", 0.8f);

      //      GameplayTutorialScoreCardsShow();
        }
        else if(tutorialNumber == 4)
        {
      //      Debug.Log("tutorialNumber is " + tutorialNumber);

            Invoke("GameplayTutorialScoreCardsDisable", 0.8f);
           // Invoke("CompleteLevelText", 0.5f);
            
       //     GameplayTutorialScoreCardsDisable();
      //      CompleteLevelText();

        }
        else if (tutorialNumber == 6 )
        {
         //   Debug.Log("tutorialNumber is " + tutorialNumber);

            //TurnOnFireButtonTutorial();
            Invoke("TurnOnFireButtonTutorial", 0.5f);

        }
        /*  else if (tutorialNumber == 7 )
          {
              TurnOnExtraBallTutorial();

          }*/

    }
    public void TurnOnGameplayTutorial()
    {
        //   tutorialNumber = 1;
        //   MenuManager.Instance.settingsButton.SetActive(false);
      //  BlackScreen.SetActive(true);
        TutorialScreen.SetActive(true);
        FireObject.SetActive(false);
        FireObject2.SetActive(false);
//        HandAnimation.Play("TutorialHandAnim");
        HandAnimation.Play("TutorialHandAnim3");

        //  TutorialText.text = "Swipe to kick";
    }
    public void TurnOffGameplayTutorial()               //called after each shoot from shoot.cs     
    {
        TutorialScreen.SetActive(false);
        BlackScreen.SetActive(false);

    }

    public void GameplayTutorialSwing()
    {
        //tutorialNumber = 2;
        TutorialScreen.SetActive(true);
        FireObject.SetActive(false);
        FireObject2.SetActive(false);
        HandAnimation.Play("TutorialHandAnim3");
  //      TutorialText.text = "Swipe to kick and drag to swing";
    }

    public void GameplayTutorialScoreCardsShow()
    {
        TutorialScreen.SetActive(true);
        HandAnimation.Play("TutorialHandAnim");
  //      TutorialText.text = "Hit different areas and get different points";
        /*   for(int i=0; i<scoreCards.Length;i++)
           {
                scoreCards[i].enabled = true;
           }*/
        //     GoalDetermine.share.flashingHighScoreAreas();
        StartCoroutine(delayGameplayTutorialScoreCardsShow());
    }
    IEnumerator delayGameplayTutorialScoreCardsShow()
    {
        yield return new WaitForSeconds(0.01f);
      /*  GoalDetermine.share._effectCornerLeft.enabled = true;
        GoalDetermine.share._effectCornerRight.enabled = true;
        GoalDetermine.share._effectLeft.enabled = true;
        GoalDetermine.share._effectRight.enabled = true;
        GoalDetermine.share._effectTop.enabled = true;*/
        //added checks to enable only one at a time //ahmad
        //_effectTop.enabled = true;
        GoalDetermine.share.effectTop.SetActive(true);
        GoalDetermine.share.effectLeft.SetActive(true);
        GoalDetermine.share.effectRight.SetActive(true);
        GoalDetermine.share.effectCornerLeft.SetActive(true);
        GoalDetermine.share.effectCornerRight.SetActive(true);
    }
    public void GameplayTutorialScoreCardsDisable()
    {
      //  Debug.Log("GameplayTutorialScoreCardsDisable");
        /*  for (int i = 0; i < scoreCards.Length; i++)
          {
              scoreCards[i].enabled = false;
          }*/
        GoalDetermine.share.completeFlashing();
      //  TutorialScreen.SetActive(false);
        /*      
               GoalDetermine.share._effectCornerLeft.enabled = false;
                GoalDetermine.share._effectCornerRight.enabled = false;
                GoalDetermine.share._effectLeft.enabled = false;
                GoalDetermine.share._effectRight.enabled = false;
                GoalDetermine.share._effectTop.enabled = false;
        */
    }
    public void CompleteLevelText()
    {
        TutorialScreen.SetActive(true);
        TutorialText.text = "Well done, Now score goals and complete the level";
    }

    public void TurnOnFireButtonTutorial()
    {
        AreaShoot.SetActive(false);
        FireTutorialBool = true;
        FireObject2.SetActive(true);
        TutorialScreen.SetActive(true);
        TutorialText.gameObject.SetActive(false);
        // LevelEndCelebrationManager.instance.Screen.SetActive(true);
        HandAnimation.gameObject.SetActive(false);
        HandAnimation2.Play("TutorialHandAnim4");
    }

    public void TurnOnExtraBallTutorial()
    {
        FireTutorialBool = false;
        TutorialScreen.SetActive(true);
        HandAnimation.gameObject.SetActive(true);

        HandAnimation.Play("TutorialHandAnim5");
        ExtraBallObject.SetActive(true);
      //  GameConstants.setFirstStartUITutorial("No");
    }
    public void TurnOffLevelTwoTutorial()
    {
        //GameConstants.setLevel2Tutorial("No");


        GameConstants.setStartGamePlayTutorial("No");
        MenuManager.Instance.setSettingsButton(true);

        FireTutorialBool = false;
        TutorialScreen.SetActive(false);
       // BlackScreen.SetActive(false);

        if (ExtraBallObject != null)
        {
            ExtraBallObject.SetActive(false);
        }
        TutorialText.gameObject.SetActive(false);

    //    AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.Tutorial_Gameplay_Completed);
        AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.Tutorial_Gameplay_Completed, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);

    }
    public IEnumerator TutorialGoalkeeperEnabler(bool active)
    {
        if (active)
        {
          //  Time.timeScale = 0.1f;
            yield return new WaitForSeconds(0f);

        }
        else
        {
            yield return new WaitForSeconds(1f);
       //     Time.timeScale = 1f;

        }

        MultiplayerMatchLogic.instance.FirstTimeBeingGoalKeeper = false;

        TutorialManager.instance.TutorialScreen.SetActive(active);

        TutorialManager.instance.HandObject.SetActive(active);

        if (active)
        {
            TutorialManager.instance.HandAnimation.Play("TutorialHandAnimGoalKeeperTutorial");
        }

        StartCoroutine(TutorialGoalkeeperEnabler(false));
    }
}
