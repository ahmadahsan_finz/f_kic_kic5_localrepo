﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITutorialManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static UITutorialManager instance;
    public GameObject ClaimButton;
    public GameObject StatPanel;
    public GameObject BlackShaderMainMenu;
    public GameObject BlackShaderMainMenu2;
    public GameObject BlackShaderLevelMenu;
    public GameObject BlackShaderLevelPopUp;
    public GameObject BlackShaderUpperPanel;
    public GameObject BlackShaderBottomPanel;
    public GameObject HandFortuneWheel;
    public GameObject HandFortuneWheel2 ;
    public GameObject BlackShaderMultiplayerModeMenu;
    public GameObject BlackShaderMultiplayerVersusScreen;
    //111public GameObject BlackShaderMultiplayerPenaltyTutorial;
   
    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        if(GameConstants.getFirstStartUITutorial()=="Yes")
        {
            EnableBlackShader();
        }
    }

    public void EnableBlackShader()
    {

        BlackShaderMainMenu.SetActive(true);
        BlackShaderLevelMenu.SetActive(true);
        BlackShaderLevelPopUp.SetActive(true);
        BlackShaderUpperPanel.SetActive(true);
        BlackShaderBottomPanel.SetActive(true);
    }
    public void FortuneWheelClaimTutorial()
    {
    //    BlackShaderMainMenu.SetActive(false);
        HandFortuneWheel.SetActive(true);
     //   HandFortuneWheel2.SetActive(true);
    //    BlackShaderMainMenu2.SetActive(true);
   //     ClaimButton.transform.SetParent(BlackShaderMainMenu2.transform);
 
        //     Instantiate(ClaimButton, BlackShaderMainMenu2.transform);

    }

    /* public void MultiplayerPenaltyTutorialEnabler(bool active)
     {
         BlackShaderMultiplayerPenaltyTutorial.SetActive(active);
     }*/
   
    /*public void MultiplayerModeMenuEnabler(bool active)
    {
        BlackShaderMultiplayerModeMenu.SetActive(active);
    }*/

    public void MultiplayerVersusScreenEnabler(bool active)
    {
        BlackShaderMultiplayerVersusScreen.SetActive(active);
    }


    public void TurnOffFortuneWheelHand()
    {
        HandFortuneWheel.SetActive(false);

    }
    public void EnableUpperBottomPanelBlackShader()
    {

        BlackShaderUpperPanel.SetActive(true);
        BlackShaderBottomPanel.SetActive(true);
    }
    public void DisableUpperBottomPanelBlackShader()
    {
        BlackShaderUpperPanel.SetActive(false);
        BlackShaderBottomPanel.SetActive(false);
    }
    public void DisableBlackShader()
    {
        BlackShaderMainMenu.SetActive(false);
        BlackShaderMainMenu2.SetActive(false);
        BlackShaderLevelMenu.SetActive(false);
        BlackShaderLevelPopUp.SetActive(false);
        BlackShaderUpperPanel.SetActive(false);
        BlackShaderBottomPanel.SetActive(false);
        TurnOffFortuneWheelHand();
        ClaimButton.transform.SetParent(StatPanel.transform);

        GameConstants.setFirstStartUITutorial("No");
    }

    
}
