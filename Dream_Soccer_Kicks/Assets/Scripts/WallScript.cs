﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallScript : MonoBehaviour
{
    public static WallScript instance;
    
    public GameObject DefenderHeads;
    public float minX=-500;
    public float maxX =500;

    private void Start()
    {
        if (this.gameObject.name.Contains("1") )//== '1'.ToString())
        {
            minX = 140;
            maxX = 220;
        }
        else if (this.gameObject.name.Contains("2"))
        {
            minX =150;
            maxX = 210;
        }
        else if (this.gameObject.name.Contains("3"))
        {
            minX = 160;
            maxX = 200;
        }
        else if (this.gameObject.name.Contains("4"))
        {
            minX = 170;
            maxX = 190;
        }
        else if (this.gameObject.name.Contains("5"))
        {
            minX = 180;
            maxX = 180;
        } 
        else if (this.gameObject.name.Contains("6"))
        {
            minX = 130;
            maxX = 230;
        }

    }
    void Update()
    {
        if (DefenderHeads != null)              //defender head movement according to ball
        {
            
            var direction = new Vector3(Shoot.share._ball.transform.position.x, Shoot.share._ball.transform.position.y, Shoot.share._ball.transform.position.z) - DefenderHeads.transform.position;
            Quaternion rotation = Quaternion.LookRotation(direction);
            rotation.eulerAngles = new Vector3(Mathf.Clamp(rotation.eulerAngles.x, -500, 500), Mathf.Clamp(rotation.eulerAngles.y, minX, maxX), rotation.eulerAngles.z);
            DefenderHeads.transform.rotation = rotation;
        }
        
    }

   
}
