﻿using UnityEngine;
using UnityEngine.EventSystems;

public class spinLogic : MonoBehaviour
{
    public float zoomSpeed;
    float direction;
    Vector3 startPos, endPos;
    float touchZeroCurrPos; float touchZeroPrevPos; float Diff;
    Vector2 touchDeltaPosition;
    Touch touchZero;

    void Update()
    {
#if UNITY_EDITOR 
        
            MoveInput();

        
#elif UNITY_ANDROID || UNITY_IOS
       
            TouchInput();

        
#endif

        
    }
    void MoveInput()        //ahmad
    {
            if (Input.GetMouseButton(0))
            {

                if (Input.GetMouseButtonDown(0))
                {
              //      Debug.Log("GetMouseButtonDown");
                // Get movement of the finger since last frame
                     startPos = Input.mousePosition;
                }
                    //actual touch
              //      Debug.Log("GetMouseButtonUp");

                    endPos = Input.mousePosition;
                    // throw anker, stop rotating slowly   //current touch x position
                    touchZeroCurrPos = endPos.x;
                    //previous touch x position
                    touchZeroPrevPos = startPos.x;
                    //difference between previous and current finger position
                    Diff = touchZeroPrevPos - touchZeroCurrPos;

               // Debug.Log("touchZeroCurrPos  =" + touchZeroCurrPos);
              //  Debug.Log("touchZeroPrevPos  =" + touchZeroPrevPos);
              //  Debug.Log("GetMouseButtonUp diff ="+Diff);

                if (touchZeroCurrPos < touchZeroPrevPos)
                    {
                  //      Debug.Log("right");
                        direction = -1;
                        //    transform.Rotate(Vector3.up, Diff * Time.deltaTime * zoomSpeed);
                       gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.up * Diff * Time.deltaTime * zoomSpeed); //for realistic rotation //ahmad
              //  gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 1f, 0); ;
            }
                    if (touchZeroCurrPos > touchZeroPrevPos)
                    {
                      //  Debug.Log("left");
                        direction = 1;
                        //  transform.Rotate(Vector3.down, -Diff * Time.deltaTime * zoomSpeed);
                      gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.down * -Diff * Time.deltaTime * zoomSpeed);
              //  gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, -1f, 0); ;
            }
            gameObject.GetComponent<Rigidbody>().angularDrag = 1f;

            /* else if (Input.GetMouseButtonUp(0))
                     {

                         gameObject.GetComponent<Rigidbody>().angularDrag = 0.2f;
                     }*/


            /*
                            if(Input.GetMouseButtonUp(0))
                        {
                            Debug.Log("GetMouseButtonUp");
                            if (touchZeroCurrPos < touchZeroPrevPos)
                            {
                                //     Debug.Log("right");
                                direction = -1;
                                transform.Rotate(Vector3.up, Diff * Time.deltaTime * zoomSpeed);
                            }
                            if (touchZeroCurrPos > touchZeroPrevPos)
                            {
                                //     Debug.Log("left");
                                direction = 1;
                                transform.Rotate(Vector3.down, -Diff * Time.deltaTime * zoomSpeed);
                            }
                        }*/

        }



    //    gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0.1f, 0); ;
        /* if (Input.GetMouseButtonUp(0))
         {
             *//*if (DecideDirection()!= Vector3.zero)
             {*//*
           //  DecideDirection();
            //}
         }*/
    }
   void TouchInput()
    {

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touchDeltaPosition = Input.GetTouch(0).position;
        //    touchDeltaPosition = Input.GetTouch(0).deltaPosition;

        }
        // else
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {

            // Get movement of the finger since last frame
            //    Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            //actual touch
             touchZero = Input.GetTouch(0);
            //current touch x position
             touchZeroCurrPos = touchZero.position.x;
            //previous touch x position
             touchZeroPrevPos =  touchDeltaPosition.x;     //changed //ahmad
          //  float touchZeroPrevPos = touchZero.position.x - touchDeltaPosition.x;
            //difference between previous and current finger position
             Diff = touchZeroPrevPos - touchZeroCurrPos;


            if (touchZeroCurrPos < touchZeroPrevPos)
            {
                //    Debug.Log("right");
                direction = -1;
               // transform.Rotate(Vector3.up, Diff * Time.deltaTime * zoomSpeed);
                    gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.up *  Diff * Time.deltaTime * zoomSpeed);  //added for relistic rotation //ahmad
            }
            if (touchZeroCurrPos > touchZeroPrevPos)
            {
                   //  Debug.Log("left");
                direction = 1;
           //     transform.Rotate(Vector3.down, -Diff * Time.deltaTime * zoomSpeed);
                    gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.down * -Diff * Time.deltaTime * zoomSpeed);
            }

        }
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            // throw anker, stop rotating slowly
            gameObject.GetComponent<Rigidbody>().angularDrag = 1f;
            /*if (touchZeroCurrPos < touchZeroPrevPos)
            {
                gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, -0.02f, 0); ;

            }
            else if (touchZeroCurrPos > touchZeroPrevPos)
            {
                  gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0.02f, 0); ;

            }*/


        }
    /*    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Canceled || Input.GetTouch(0).phase==TouchPhase.Stationary)
        {
            if(gameObject.transform.rotation.y!=0f)
            {
                gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 2f, 0); 

            }
           else if(gameObject.transform.rotation.y == 0f)
            {
                gameObject.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);

            }
        }*/
    }

}