﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SettingScript : MonoBehaviour
{
    public GameObject settingsPanel;
    public GameObject overlay;
    // Start is called before the first frame update
    public void HomeClicked()
    {
        if (settingsPanel != null) {
            settingsPanel.SetActive(false);
            overlay.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(false);
        }
   //     UIAnimations.instance.showAgain();
        //GamePlayManager.instance.score = 0;
        GameConstants.score = 0;
        SceneManager.LoadScene(0);
        //Application.LoadLevelAsync("Menus");

        //  GameObject.Find("MainTab").SetActive(true);
        //GameObject.Find("MainTab").SetActive(true);
        // Load Home Again
    }
    public void SettingsClicked()
    {
        settingsPanel.SetActive(true);
    }
}
