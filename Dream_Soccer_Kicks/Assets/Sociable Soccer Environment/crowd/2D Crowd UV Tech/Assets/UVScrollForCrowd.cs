﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//UV Step for Crowd 2D , By Asad

public class UVScrollForCrowd : MonoBehaviour {


    public Material spriteSheetMaterial;
    public int spriteCount;
    public int uvAnimationTileX;
    public int uvAnimationTileY;
    public int framesPerSecond;

    private float startTime;
    private float uIndex;
    private float vIndex;
    private Vector2 offset;
    private Vector2 size;

	void Start(){

	//	StartCoroutine("PlayAniamtion");
		
	}
  
	
	// Update is called once per frame
	void Update ()
    {
		TextureSwitch ();
       


    }

	IEnumerator PlayAniamtion()
	{
		while(true)
		{
			yield return new WaitForSeconds(2);
			StartCoroutine("TextureSwitch");
		}
	}

	void TextureSwitch(){

		// Calculate index

		float index = (Time.time - startTime) * framesPerSecond;


		if (index >= spriteCount)
		{
			startTime = Time.time;
			index = 0;

		}

		if (index <= spriteCount)
		{

			// repeat when exhausting all frames
			index = index % (uvAnimationTileX * uvAnimationTileY);

			// Size of every tile
			size = new Vector2(1.0f / uvAnimationTileX, 1.0f / uvAnimationTileY);


			// split into horizontal and vertical index
			uIndex = Mathf.Floor(index % uvAnimationTileX);
			vIndex = Mathf.Floor(index / uvAnimationTileX);

			// build offset
			offset = new Vector2(uIndex * size.x, 1.0f - size.y - vIndex * size.y);

			spriteSheetMaterial.SetTextureOffset("_MainTex", offset);
			spriteSheetMaterial.SetTextureScale("_MainTex", size);

		}

		
	}
}
