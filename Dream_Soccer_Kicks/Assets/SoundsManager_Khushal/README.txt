-----------------------------------------------------------------
--------------------------Sound Manager--------------------------
-----------------------------------------------------------------

1. Drag Prefab SoundManager from resources folder in scene 

2. Pass All possible Audiotracks in SoundManager prefab

3. Use instance of soundmanager whereever you wanna use sound in your script 
 i.e SoundManager soundmanager = SoundManager.Instance;

4. Use Following functions in your code
	1. Music On Off (bg Music)
		soundmanager.OnOffMusic(true);
	2. Sound On Off (Sound like on buttons)
		soundmanager.OnOffSound(true);
	3. Play Music (i.e bg Sound)
		soundmanager.PlayMusic();
	4. Play Sound (i.e Button sound)
		soundmanager.PlaySound();


--------www.finz.io--------www.finz.io--------www.finz.io--------
-----------------------------------------------------------------
-----------------------------The End-----------------------------
