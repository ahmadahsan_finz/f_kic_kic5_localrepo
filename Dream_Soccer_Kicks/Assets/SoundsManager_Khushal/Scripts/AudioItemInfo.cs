﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioItemInfo  {

        public string name;
        public float volume = 1f;
        public AudioClip[] clip;
}
