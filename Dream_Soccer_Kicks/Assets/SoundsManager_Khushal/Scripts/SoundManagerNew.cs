﻿using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class SoundManagerNew : MonoBehaviour
{
    private static SoundManagerNew instance;
    bool isDeviceHighEnd;
    public static SoundManagerNew Instance { get { return instance; } }
    private void Start()
    {
        checkDeviceSpecs();                   //check if device is high end or low end

        Shoot.EventShoot += OnShoot;
        GoalKeeper.EventBallHitGK += OnBallHitGK;
        GoalDetermine.EventFinishShoot += OnGoal;
    }

    void OnDestroy()
    {
        Shoot.EventShoot -= OnShoot;
        GoalKeeper.EventBallHitGK -= OnBallHitGK;
        GoalDetermine.EventFinishShoot -= OnGoal;

    }
    public void checkDeviceSpecs()
    {
        Application.targetFrameRate = 60;

        //Debug.Log("SystemInfo.operatingSystemFamily" + SystemInfo.operatingSystemFamily);
        //Debug.Log("SystemInfo.operatingSystem" + SystemInfo.operatingSystem);
        //Debug.Log("SystemInfo.graphicsDeviceType" + SystemInfo.graphicsDeviceType);
        //Debug.Log("SystemInfo.maxTextureSize" + SystemInfo.maxTextureSize);
        //Debug.Log("graphicsMemorySize" + SystemInfo.graphicsMemorySize);
        //Debug.LogError("systemMemorySize" + SystemInfo.systemMemorySize);
        //Debug.LogError("maxTextureSize" + SystemInfo.maxTextureSize);
        //Debug.LogError("operatingSystem" + SystemInfo.operatingSystem);
        //Debug.LogError("processorCount" + SystemInfo.processorCount);

        //isDeviceHighEnd = true;

        /*#if UNITY_IOS || UNITY_IPHONE
                if (SystemInfo.deviceModel.ToLower().Trim().Contains("iphone6")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("iphone5")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("iphone7")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("iphone4")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("ipod")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("ipod")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("ipad1")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("ipad2")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("ipad3")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("iphone1")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("iphone2")
                    || SystemInfo.deviceModel.ToLower().Trim().Contains("iphone3")
                    )
                {
                    QualitySettings.SetQualityLevel(6);
                    isDeviceHighEnd = false;

                }
                else
                {
                    QualitySettings.SetQualityLevel(2);
                    isDeviceHighEnd = true;
                }


                if (UnityEngine.iOS.Device.generation >= UnityEngine.iOS.DeviceGeneration.iPhone7)
                {
                    QualitySettings.SetQualityLevel(2);
                    isDeviceHighEnd = true;
                }

        #endif*/

        /*#if UNITY_ANDROID
                if (SystemInfo.systemMemorySize < 1024 // ram
                    || (SystemInfo.batteryLevel > 0 && SystemInfo.batteryLevel < 0.2f) // battery 20%
                    || SystemInfo.graphicsMemorySize < 1024 // graphic memory 
                  ) // Texture Quality 
                {
                 //   QualitySettings.SetQualityLevel(6);
                    isDeviceHighEnd = false;
                }
                else
                {
                   // QualitySettings.SetQualityLevel(2);
                    isDeviceHighEnd = true;
                }
                //QualitySettings.SetQualityLevel(6);
                //isDeviceHighEnd = false;
        #endif
                if (isDeviceHighEnd)
                {
                    *//* Analytics.CustomEvent("DEVICE", new Dictionary<string, object> {
                     {  "QUALITY".ToUpper(), "HIGH"}

                 });*//*
                    Debug.Log("Device is High End");

                    Application.targetFrameRate = 60;
                }
                else
                {
                    Debug.Log("Device is Low End");
                    *//*#if UNITY_IOS || UNITY_IPHONE
                                Application.targetFrameRate = 40;
                    #endif*/
        /*  Analytics.CustomEvent("DEVICE", new Dictionary<string, object> {
          {  "QUALITY".ToUpper(), "LOW"}
        });*//*

        Application.targetFrameRate = 40;


    }*/
        //      Debug.Log(SystemInfo.deviceModel.ToLower().Trim() + "ishighend" + isDeviceHighEnd.ToString());
        //SpecialAnalytics.instance.deviceNameAnalytics();
    }
    void OnShoot()
    {
        playSound("Shoot");
    }

    void OnBallHitGK()
    {
        playSound("goalkeeper_catch");
    }

    void OnGoal(bool isGoal, Area area)
    {
        if (isGoal)
        {
            if (area == Area.None || area == Area.Normal)
            {
               playSound("BallHitGoal");
               // SoundManager.share.playSoundSFX(SOUND_NAME.Ball_Hit_Goal);
            }
            else
            {
                playSound("BallHitGoalExtra");
               // SoundManager.share.playSoundSFX(SOUND_NAME.Ball_Hit_Goal_Extra);
            }
            playSound("Crowd_Goal");
           // SoundManager.share.playSoundSFX(SOUND_NAME.Crowd_Goal);
        }
        else
        {
            playSound("Crowd_Out");
          //  SoundManager.share.playSoundSFX(SOUND_NAME.Crowd_Out);
        }
    }
    public AudioItemInfo[] AudioList; //list of audios in game
        private AudioSource source;
        private float musicVolume = 1f; //background music volume
        private float sfxVolume = 1f; //sound volume 

        void Awake()
        {
        //if(Instance == null && Instance != this)
        //{
        //    //Destroy(this.gameObject);
        //    Instance = this;

        //}
        //DontDestroyOnLoad(this.gameObject);



        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }


        source = GetComponent<AudioSource>();
            //set settings
            GameSettings settings = Resources.Load("GameSettings", typeof(GameSettings)) as GameSettings;
            if (settings != null)
            {
                musicVolume = settings.MusicVolume; //music volume get from GameSettings
                sfxVolume = settings.SFXVolume; //music volume get from GameSettings
            }
        }

        //play sound 
        public void playSound(string name,  bool loop=false)
        {
            bool SFXFound = false;
            foreach (AudioItemInfo audioItem in AudioList)
            {
                if (audioItem.name == name)
                {

                //pick a random number (not same twice)
                int rand = Random.Range(0, audioItem.clip.Length);
                    source.PlayOneShot(audioItem.clip[rand]);
                    source.volume = audioItem.volume * sfxVolume;
                    source.loop = loop;
                    
                    SFXFound = true;
                }
            }
            if (!SFXFound) Debug.Log("no sfx found with name: " + name);
        }

        //play music like gameplay background music
        public void playMusic(string name, bool loop=true)
        {
      //  Debug.Log("In play music function");
            GameObject music = GameObject.Find("Music");
            AudioSource audioSource;
            if (music == null)
            { //create a separate gameobject designated for playing music
                music = new GameObject();
                music.name = "Music";
                audioSource = music.AddComponent<AudioSource>();
            }
            else
            audioSource = music.GetComponent<AudioSource>();

        //get music track from trackList
        foreach (AudioItemInfo audioItem in AudioList)
        {
       //     Debug.Log(audioItem.name);

            if (audioItem.name == name)
                {
      //          Debug.Log("PlayingMusic");
                     if(audioSource!=null)
                    {

                        audioSource.clip = audioItem.clip[0];
                        audioSource.loop = loop;
                        audioSource.volume = audioItem.volume * musicVolume;
                        audioSource.Play();
                    }
                }
            }
        }

    //On Off bg music. Pass true to turn on and false to turn off
    public void OnOffMusic(bool On)
    {
         GameObject music = GameObject.Find("Music");
         AudioSource audioSource;
         if (music == null)
         { //create a separate gameobject designated for playing music
         music = new GameObject();
             music.name = "Music";
             audioSource = music.AddComponent<AudioSource>();
         }
         else
             audioSource = music.GetComponent<AudioSource>();

         if(On==true)
             audioSource.mute = false;
         else
                audioSource.mute = true;
     }

        //On Off bg sound. Pass true to turn on and false to turn off
     public void OnOffSound(bool On)
     {
        if (On == true)
        {
     //       Debug.Log("on is trueeeeee");
            source.mute = false;
        }
        else
        {
           
            source.mute = true;
        }
     }

    public void OnOffSound2()
    {
        if (source.mute == false)
            source.mute = true;
        else
            source.mute = false;
    }


    public void ClickSound()
    {
      //  if(GameConstants.GetSound() == 1)
       // {
            playSound("Click");
       // }
    }


    public void playGoalSound()
    {
        if (GameConstants.GetSound() ==1)
        {
            playSound("PlayGoalSound");
        }
    }

}
