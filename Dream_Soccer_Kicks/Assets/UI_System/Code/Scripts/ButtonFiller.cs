﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ButtonFiller : MonoBehaviour //Fills button in layout 
{
    // Start is called before the first frame update
    public static ButtonFiller instance;
     List<LevelButtonsController> buttons;

    public GameObject buttonToBeFilled;
    public int NumberOfLevels;
    public static int initialPos;
    public Transform parent;
    public ScrollRect scrollRect;

    public GameObject btnGrp;
    public GameObject btngrp1;
    public int[] GiftLevels; // Set size in inspector for level gifts
    public int[] BossLevels; // Set size in inspector for level gifts
    public List<int> cupLevels;
    public static int currFound;
    public int currLevel;
    public GameObject Hand;
    public GameObject CurrentLevelIndicator;
    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
        //    Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        //Debug.Log("OnEnable ButtonFiller");

        Hand.SetActive(false);

        currLevel = GameConstants.GetLevelNumber();
        int levelPassed = GenericVariables.getTotalLevelsPassed(); //60;// 
   //     Debug.Log("Levels Passed :  " + levelPassed);
        GenericVariables.setTotalLevelsPassed(levelPassed);
        currFound = 0;

       /* buttons = new List<LevelButtonsController >();
        FillButtons(NumberOfLevels);
       */ //    PopupDetailMenuShow();
        if (GameConstants.getFirstStartUITutorial() != "Yes")
        {
            TutorialHandTurnOnOFF(false);

           // Invoke("PopupDetailMenuShow", 0.2f);  //disabling popup
        }
        else
        {
            TutorialHandTurnOnOFF(true);
        }
        //    refreshButtons();
        ///NumberOfLevels = NumberOfLevels - (NumberOfLevels / 2);
        //    Debug.Log("inside on enable Button Filler");
        StartCoroutine(DelayScrollSnap());

    }

    public void PopupDetailMenuShow()
    {
        //MenuManager.Instance.setMain(false);
        GameConstants.SetLevelNumber(GenericVariables.getTotalLevelsPassed());
        PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
        PopUpLevelDetails.LevelDetailsSetter();

        //selectedlevel = levelno;
        int selectedlevel = GameConstants.GetLevelNumber();

        GameConstants.SetGameMode("Classic");
        //	Debug.Log("Game mode issssssssssssssss " + GameConstants.GetGameMode());

        // MenuManager.Instance.ShowPenaltyPanel(levelno);
        if (GameConstants.GetMode() == 10)
        {
            MenuManager.Instance.ShowTournamentPanelPopup(selectedlevel);
        }
        else
        {
            MenuManager.Instance.ShowPenaltyPanel(selectedlevel);
        }
        //Your level number comes here
    }

    private void Start()
    {
        //buttons = new List<LevelButtonsController>();
        //FillButtons(NumberOfLevels)
    }
    private void refreshButtons()
    {
        if (buttons != null)
        {
            if (currLevel == GenericVariables.getTotalLevelsPassed())
            {
                if(NumberOfLevels!= currLevel) //if curr level is not equal to max level
                {

                  //  buttons[currLevel].setStars(GenericVariables.getRating(currLevel));
                    buttons[currLevel].setInteractable(true);
                }
            }
        }
    }
    public void FillButtons(int numberOfLevels)
    {
        foreach (Transform child in parent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        int stop = 1;
        for (int i = 1; i <= numberOfLevels && stop <= numberOfLevels; i++)
        {
            if (i % 2 != 0)
            {
                GameObject g = Instantiate(buttonToBeFilled, parent, false); //Instrantiate single button on even pos
                g.transform.SetAsFirstSibling();
                buttons.Add(g.GetComponent<LevelButtonsController>());
                stop++;
            }
            else
            {
                GameObject g = Instantiate(btnGrp, parent, false); //Instantiate Two buttons on odd pos
                g.transform.SetAsFirstSibling(); //Reverses Objects
                LevelButtonsController[] childButtons = g.GetComponentsInChildren<LevelButtonsController>();
                for (int j = 0; j < 2; j++)
                {
                    if (stop <= numberOfLevels)
                    {
                        buttons.Add(childButtons[j]);
                    }
                    else
                    {
                        //buttons.Add(childButtons[j]);
                        childButtons[j].gameObject.SetActive(false); //Generate only one button at end for specific number
                    }
                    stop++;
                }
            }
        }

        for (int i = 0; i < buttons.Count; i++)
        {
         //   buttons[i].setLevelButton((i + 1).ToString());           // Sets each level button's text and relevant listener
        }

        /*   for (int i = 0; i < buttons.Count; i++) //Snap into view
           {
               if (!buttons[i].GetComponentInChildren<Button>().interactable)
               {
                   Debug.Log(i);
                   setInitialPos(buttons[i].gameObject);
                   break;
               } 
           }
        */
       // Debug.Log("total levls passed  " + GenericVariables.getTotalLevelsPassed());
   //     Debug.Log("total levls passed  " + buttons[GenericVariables.getTotalLevelsPassed()].GetComponent<Text>().text);
        //   buttons[GenericVariables.getTotalLevelsPassed()].GetComponent<Text>().text

        StartCoroutine(DelayScrollSnap());
        // setInitialPos(buttons[GenericVariables.getTotalLevelsPassed()].gameObject);

        setGifts();
        setCups();
        SetBoss();
    }

    IEnumerator DelayScrollSnap()
    {
        yield return new WaitForSeconds(0);
      //  Debug.Log("total levls passed  " + buttons[GenericVariables.getTotalLevelsPassed()].GetComponent<Text>().text);
     //  buttons[GenericVariables.getTotalLevelsPassed()].showText();
    
     //   buttons[GenericVariables.getTotalLevelsPassed() - 1].setNewUnlockedLevel();
     
        //   Debug.Log("DelayScrollSnap " + GenericVariables.getTotalLevelsPassed());
     //   Debug.Log("DelayScrollSnap " + (GenericVariables.getTotalLevelsPassed() - 1));
       
        //setInitialPos(buttons[GenericVariables.getTotalLevelsPassed()-1].gameObject);
        
        setInitialPos(LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed()-1].gameObject);


        //***setting current level indicator***//
        CurrentLevelIndicator.transform.parent = LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed()].transform; 
        CurrentLevelIndicator.transform.SetSiblingIndex(8);
        CurrentLevelIndicator.transform.localPosition = new Vector3(0f, 15f, 0f);
        CurrentLevelIndicator.transform.GetChild(1).GetComponent<Text>().text = GenericVariables.getTotalLevelsPassed().ToString(); //setting current level number on indicator
    }

    public void TutorialHandTurnOnOFF(bool active)
    {
        // buttons[GenericVariables.getTotalLevelsPassed() - 1].TutorialHandTurnOff();
        //   LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed()].transform.GetChild()
        Hand.SetActive(active);
    }
    private void setInitialPos(GameObject target)
    {
        Canvas.ForceUpdateCanvases();
        scrollRect.content.anchoredPosition = Vector2.zero;
        Vector2 pos = (Vector2)scrollRect.content.transform.InverseTransformPoint(scrollRect.content.position)
                        -   (Vector2)scrollRect.transform.InverseTransformPoint(target.GetComponent<RectTransform>().position);
        scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, pos.y);
    }


    private void setGifts() // Set gift for each level until
    {
        for (int i = 0; i < GiftLevels.Length; i++)
        {
            if (GiftLevels[i] < NumberOfLevels)
            {
                buttons[GiftLevels[i] - 1].setGift();
            }//Fills gift for each provided level
        }
    }
    private void SetBoss() // Set gift for each level until
    {
        for (int i = 0; i < BossLevels.Length; i++)
        {
            if (BossLevels[i] < NumberOfLevels)
            {
                buttons[BossLevels[i] - 1].setBossLevelImage();
            }//Fills gift for each provided level
        }
    }

    private void setCups()
    {
        if (cupLevels.Count > 0)
        {
            for (int i = 0; i < cupLevels.Count; i++)
            {
                if (cupLevels[i] <= NumberOfLevels)
                {
                    buttons[cupLevels[i] - 1].setCup();
                }
                // fills cups for each provided level
            }
        }
        else
        {
            //Must specify cup level
        }

    }


}