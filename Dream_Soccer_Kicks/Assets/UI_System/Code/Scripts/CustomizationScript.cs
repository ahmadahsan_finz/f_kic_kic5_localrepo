﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CustomizationScript : MonoBehaviour
{
    public static CustomizationScript instance;
    static int selectedIndex;

    [Header("Customization Panel")]
    public GameObject UIPlayer;
    public GameObject CustomUIPlayer;
    public GameObject UIPlayerSkin;
    public GameObject UIPlayerBody;
    public GameObject UIFootball;
    [SerializeField] GameObject EmoteScrollView;
    [SerializeField] GameObject BodyScrollView;
    [SerializeField] GameObject FaceScrollView;
    [SerializeField] GameObject FootballScrollView;
    [SerializeField] GameObject ShortsScrollView;
    public GameObject buyButton;
    public GameObject WatchButton;
    public Text priceText;

    [Header("Player Skins")]
    public Texture[] SkinTextures;
    public Texture[] BodyTextures;
    public Texture[] ShortTextures;
    public Texture[] FootballTextures;
    public Image[] BodyLockImages;
    public Image[] BodyVideoImages;
    public Image[] BodyTickImages;
    public Image[] SkinLockImages;
    public Image[] SkinVideoImages;
    public Image[] SkinTickImages;
    public Image[] FootballLockImages;
    public Image[] FootballVideoImages;
    public Image[] FootballTickImages;
    public Image[] ShortsVideoImages;
    public Image[] ShortsLockImages;
    public Image[] ShortTickImages;
    public int[] SkinPrices;
    public int[] BodyPrices;
    public int[] ShortsPrices;
    public int[] FootballPrices;

    [Header("GK Textures")]
    public Texture[] GKSkinTextures;
    public Texture[] GKBodyTextures;

    [Header("Player Animation")]
    public AnimationClip[] animationClips;
    public AnimationClip animationOrginal;
    public Animator animator;
    public AnimatorOverrideController animatorOverrideController;
    public AnimatorOverrideController animatorOverrideControllerOrginal;
    public Image[] CelebrationLockImages;
    public Image[] CelebrationVideoImages;
    public Image[] CelebrationTickImages;
    public int[] CelebrationPrices;
    public bool NewCelebrationBought;

    [Header("Reward Gameover")]
    public Sprite[] RewardItemsImagesList;


    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }


    private void Start()
    {


        StartCoroutine(PlayerAnimatorSetter());
        UnlockFirstItemOfAllCustomization();

        ApplySkinTextures();
        ApplyBodyTextures();
   //     ApplyShortsTextures();
        ApplyFootballTextures();

        SkinTickImages[GenericVariables.getcurrSkin()].gameObject.SetActive(true);
        BodyTickImages[GenericVariables.getcurrBody()].gameObject.SetActive(true);
       // ShortTickImages[GenericVariables.getcurrShorts()].gameObject.SetActive(true);
        FootballTickImages[GenericVariables.getcurrFootball()].gameObject.SetActive(true);
        CelebrationTickImages[GenericVariables.getcurrCelebration()].gameObject.SetActive(true);

        KitsLockingUnlocking();
        ShortsLockingUnlocking();
        SkinsLockingUnlocking();
        FootballsLockingUnlocking();
        CelebrationsLockingUnlocking();


        BodyLockImages[0].gameObject.SetActive(false);
        ShortsLockImages[0].gameObject.SetActive(false);
        SkinLockImages[0].gameObject.SetActive(false);
        FootballLockImages[0].gameObject.SetActive(false);
        CelebrationLockImages[0].gameObject.SetActive(false);
    }

    public void ApplySkinTextures()
    {
        Texture skin = SkinTextures[GenericVariables.getcurrSkin()];

        for (int a = 0; a < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin.Length; a++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin[a].GetComponent<Renderer>().material.SetTexture("_MainTex", skin);

        }
        for (int i = 0; i < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length; i++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);

        }
        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[GenericVariables.getcurrSkin()].SetActive(true);


    }

    public void ApplyBodyTextures()
    {
        Texture body = BodyTextures[GenericVariables.getcurrBody()];
        for (int i = 0; i < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody.Length; i++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody[i].GetComponent<Renderer>().material.SetTexture("_MainTex", body);
        }
    }
    public void ApplyShortsTextures()
    {
        Texture shorts = BodyTextures[GenericVariables.getcurrShorts()];
        for (int i = 0; i < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts.Length; i++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts[i].GetComponent<Renderer>().material.SetTexture("_MainTex", shorts);
        }
    }
    public void ApplyFootballTextures()
    {
        Texture football = FootballTextures[GenericVariables.getcurrFootball()];
        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIFootball.GetComponent<Renderer>().material.SetTexture("_MainTex", football);

    }

    public void UnlockFirstItemOfAllCustomization()
    {
        GenericVariables.saveUnlockedBodyWithStatus(0, 2);
        GenericVariables.saveUnlockedShortsWithStatus(0, 2);
        GenericVariables.saveUnlockedCelebrationWithStatus(0, 2);
        GenericVariables.saveUnlockedFootballWithStatus(0, 2);
        GenericVariables.saveUnlockedSkinWithStatus(0, 2);
    }

    public void KitsLockingUnlocking()
    {
        for (int i = 0; i < BodyLockImages.Length; i++)
        {
            if (GenericVariables.checkUnlockedBodyWithStatus(i) == 0)
            {
                BodyLockImages[i].gameObject.SetActive(true);
                BodyVideoImages[i].gameObject.SetActive(false);

            }
            else if (GenericVariables.checkUnlockedBodyWithStatus(i) == 1)
            {
                BodyLockImages[i].gameObject.SetActive(false);
                BodyVideoImages[i].gameObject.SetActive(true);
            }
            else if (GenericVariables.checkUnlockedBodyWithStatus(i) == 2)
            {
                BodyLockImages[i].gameObject.SetActive(false);
                BodyVideoImages[i].gameObject.SetActive(false);
            }

            WatchButton.SetActive(false);
            buyButton.SetActive(false);
            /*   else if (GenericVariables.checkBoughtBody(i).Item1 && GenericVariables.checkBoughtBody(i).Item2 != 1)
               {
                   BodyLockImages[i].gameObject.SetActive(false);

                   BodyVideoImages[i].gameObject.SetActive(false);
               }*/

        }
    }
    public void ShortsLockingUnlocking()
    {
        for (int i = 0; i < ShortsLockImages.Length; i++)
        {
            if (GenericVariables.checkUnlockedShortWithStatus(i) == 0)
            {
                ShortsLockImages[i].gameObject.SetActive(true);
                ShortsVideoImages[i].gameObject.SetActive(false);

            }
            else if (GenericVariables.checkUnlockedShortWithStatus(i) == 1)
            {
                ShortsLockImages[i].gameObject.SetActive(false);
                ShortsVideoImages[i].gameObject.SetActive(true);
            }
            else if (GenericVariables.checkUnlockedShortWithStatus(i) == 2)
            {
                ShortsLockImages[i].gameObject.SetActive(false);
                ShortsVideoImages[i].gameObject.SetActive(false);
            }

            WatchButton.SetActive(false);
            buyButton.SetActive(false);
        }
    }
    public void SkinsLockingUnlocking()
    {
        for (int i = 0; i < SkinLockImages.Length; i++)
        {
            if (GenericVariables.checkUnlockedSkinWithStatus(i) == 0)
            {
                SkinLockImages[i].gameObject.SetActive(true);
                SkinVideoImages[i].gameObject.SetActive(false);

            }
            else if (GenericVariables.checkUnlockedSkinWithStatus(i) == 1)
            {
                SkinLockImages[i].gameObject.SetActive(false);
                SkinVideoImages[i].gameObject.SetActive(true);
            }
            else if (GenericVariables.checkUnlockedSkinWithStatus(i) == 2)
            {
                SkinLockImages[i].gameObject.SetActive(false);
                SkinVideoImages[i].gameObject.SetActive(false);
            }
            /*   else if (GenericVariables.checkBoughtBody(i).Item1 && GenericVariables.checkBoughtBody(i).Item2 != 1)
               {
                   BodyLockImages[i].gameObject.SetActive(false);

                   BodyVideoImages[i].gameObject.SetActive(false);
               }*/
            WatchButton.SetActive(false);
            buyButton.SetActive(false);
        }
    }
    public void FootballsLockingUnlocking()
    {
        for (int i = 0; i < FootballLockImages.Length; i++)
        {
            if (GenericVariables.checkUnlockedFootballWithStatus(i) == 0)
            {
                FootballLockImages[i].gameObject.SetActive(true);
                FootballVideoImages[i].gameObject.SetActive(false);

            }
            else if (GenericVariables.checkUnlockedFootballWithStatus(i) == 1)
            {
                FootballLockImages[i].gameObject.SetActive(false);
                FootballVideoImages[i].gameObject.SetActive(true);
            }
            else if (GenericVariables.checkUnlockedFootballWithStatus(i) == 2)
            {
                FootballLockImages[i].gameObject.SetActive(false);
                FootballVideoImages[i].gameObject.SetActive(false);
            }
            /*   else if (GenericVariables.checkBoughtBody(i).Item1 && GenericVariables.checkBoughtBody(i).Item2 != 1)
               {
                   BodyLockImages[i].gameObject.SetActive(false);

                   BodyVideoImages[i].gameObject.SetActive(false);
            }*/
            WatchButton.SetActive(false);
            buyButton.SetActive(false);
        }
    }
    public void CelebrationsLockingUnlocking()
    {
        for (int i = 0; i < CelebrationLockImages.Length; i++)
        {
            if (GenericVariables.checkUnlockedCelebrationWithStatus(i) == 0)
            {
                CelebrationLockImages[i].gameObject.SetActive(true);
                CelebrationVideoImages[i].gameObject.SetActive(false);

            }
            else if (GenericVariables.checkUnlockedCelebrationWithStatus(i) == 1)
            {
                CelebrationLockImages[i].gameObject.SetActive(false);
                CelebrationVideoImages[i].gameObject.SetActive(true);
            }
            else if (GenericVariables.checkUnlockedCelebrationWithStatus(i) == 2)
            {
                CelebrationLockImages[i].gameObject.SetActive(false);
                CelebrationVideoImages[i].gameObject.SetActive(false);
            }
            /*   else if (GenericVariables.checkBoughtBody(i).Item1 && GenericVariables.checkBoughtBody(i).Item2 != 1)
               {
                   BodyLockImages[i].gameObject.SetActive(false);

                   BodyVideoImages[i].gameObject.SetActive(false);
               }*/
            WatchButton.SetActive(false);
            buyButton.SetActive(false);
        }
    }
    public IEnumerator PlayerAnimatorSetter()
    {
        yield return new WaitForSeconds(1f);

        animator = MenuManager.Instance.SelectedPlayer.GetComponent<Animator>();
      //  animator = CustomUIPlayer.GetComponent<Animator>();

        animatorOverrideControllerOrginal = new AnimatorOverrideController(animator.runtimeAnimatorController)
        {
            name = "OrginalAnimator"
        };

        animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController)
        {
            name = "CelebrationAnimator"
        };

    }
    public void ApplyOrginalAnimator()
    {
    //    UIFootball.SetActive(true);
        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIFootball.SetActive(true);
        animator.runtimeAnimatorController = animatorOverrideControllerOrginal;
        animator.Play("Idle_WithFootball");

        //    animatorOverrideController["Take 001"] = animationOrginal;
    }
    public void ShowBuyButton(bool value)
    {
        buyButton.SetActive(value);
    }
    public void ShowWatchButton(bool value)
    {
        WatchButton.SetActive(value);
    }

    public void onFacePressed() 
    {

        ApplyOrginalAnimator();
        ShowBuyButton(false);
        ShowWatchButton(false);
        //    int body = GenericVariables.getcurrBody();
        //    MenuManager.Instance.ChangeBody(body); //List is present in menu manager with relevant indices
        Customize(0); // 0 for face scroll view
    }
    public void onBodyPressed()
    {
        ApplyOrginalAnimator();
        ShowBuyButton(false);
        ShowWatchButton(false);
        CustomiazationMenuScript.instance.DisableNewNotication(0);

        //   int skin = GenericVariables.getcurrSkin();
        //   MenuManager.Instance.ChangeSkin(skin);
        Customize(1); // 1 for body scroll view
    }
    public void onShortsPressed()
    {
        ApplyOrginalAnimator();
        ShowBuyButton(false);
        ShowWatchButton(false);
        CustomiazationMenuScript.instance.DisableNewNotication(1);

        //   int skin = GenericVariables.getcurrSkin();
        //   MenuManager.Instance.ChangeSkin(skin);
        Customize(4); // 1 for body scroll view
    }
   

    public void onFootballPressed()
    {

        CustomiazationMenuScript.instance.DisableNewNotication(2);

        ApplyOrginalAnimator();
        ShowBuyButton(false);
        ShowWatchButton(false);

        Customize(2); // 2 for Football scroll view
    }
    public void onEmotePressed()
    {

        CustomiazationMenuScript.instance.DisableNewNotication(3);

        ApplyOrginalAnimator();
        ShowBuyButton(false);
        ShowWatchButton(false);

        Customize(3); // 3 for Emote scroll view
    }
    public void CelebrationSelected(int imageIndex) //Which celebration Image is selected
    {
        selectedIndex = imageIndex;
        CustomiazationMenuScript.instance.categoryState = CustomiazationMenuScript.Category.celebration;
        CustomiazationMenuScript.instance.ItemNumber= imageIndex;//assigning the item number

        if (GenericVariables.checkUnlockedCelebrationWithStatus(selectedIndex) == 0)
        {
            ShowBuyButton(true);
            ShowWatchButton(false);

        }
        else if (GenericVariables.checkUnlockedCelebrationWithStatus(selectedIndex) == 1)
        {
            ShowBuyButton(false);
            ShowWatchButton(true);

        }
        else if (GenericVariables.checkUnlockedCelebrationWithStatus(selectedIndex) == 2)
        {
            GenericVariables.saveCelebration(imageIndex);
            CelebrationTickImagesReseter();
            CelebrationTickImages[imageIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
            ShowWatchButton(false);
        }
        ChangeCelebration(imageIndex);

        Debug.Log("celebration Selected " + selectedIndex);

/*        if (CelebrationLockImages[imageIndex].gameObject.activeSelf)
        {
            ShowBuyButton(true);
        }
        else
        {
            GenericVariables.saveCelebration(imageIndex);
            CelebrationTickImagesReseter();
            CelebrationTickImages[imageIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
        }
        Debug.Log("Face Selected " + imageIndex);
        ChangeCelebration(imageIndex);*/
    }


    public void FaceSelected(int imageIndex) //Which face Image is selected
    {
        selectedIndex = imageIndex;
        CustomiazationMenuScript.instance.categoryState = CustomiazationMenuScript.Category.skin;
        CustomiazationMenuScript.instance.ItemNumber= selectedIndex;//assigning the item number
        if (GenericVariables.checkUnlockedSkinWithStatus(selectedIndex) == 0)
        {
            ShowBuyButton(true);
            ShowWatchButton(false);

        }
        else if (GenericVariables.checkUnlockedSkinWithStatus(selectedIndex) == 1)
        {
            ShowBuyButton(false);
            ShowWatchButton(true);

        }
        else if (GenericVariables.checkUnlockedSkinWithStatus(selectedIndex) == 2)
        {
            GenericVariables.saveSkin(selectedIndex);
            SkinTickImagesReseter();
            SkinTickImages[selectedIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
            ShowWatchButton(false);
        }
        ChangeSkin(selectedIndex); 

        Debug.Log("Face Selected " + selectedIndex);

        /*if (SkinLockImages[imageIndex].gameObject.activeSelf)
        {
           ShowBuyButton(true);
        }
        else
        {
            GenericVariables.saveSkin(imageIndex);
            SkinTickImagesReseter();
            SkinTickImages[imageIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
        }
        Debug.Log("Face Selected " + imageIndex);
       ChangeSkin(imageIndex);*/
    }

    public void BodySelected(int imageIndex) //Which Image is selected
    {
        selectedIndex = imageIndex;
        CustomiazationMenuScript.instance.categoryState = CustomiazationMenuScript.Category.kit;
        CustomiazationMenuScript.instance.ItemNumber= selectedIndex; //assigning the item number
        
        if(GenericVariables.checkUnlockedBodyWithStatus(selectedIndex) ==0) //locked
        {
            ShowBuyButton(true);
            ShowWatchButton(false);

        }
        else if (GenericVariables.checkUnlockedBodyWithStatus(selectedIndex) == 1) //watch ad
        {
            ShowBuyButton(false);
            ShowWatchButton(true);

        }
        else if (GenericVariables.checkUnlockedBodyWithStatus(selectedIndex) == 2) //unlocked
        {
            GenericVariables.saveBody(selectedIndex);
            BodyTickImagesReseter();
            BodyTickImages[selectedIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
            ShowWatchButton(false);

        }

        ChangeBody(selectedIndex); //List is present in menu manager with relevant indices
        Debug.Log("kit Selected " + selectedIndex);

        /*   
           if (BodyLockImages[imageIndex].gameObject.activeSelf)
           {
               ShowBuyButton(true);
           }
           else
           {
               GenericVariables.saveBody(imageIndex);
               BodyTickImagesReseter();
               BodyTickImages[imageIndex].gameObject.SetActive(true);
               ShowBuyButton(false);
           }
           ChangeBody(imageIndex); //List is present in menu manager with relevant indices

        */
    } 
    public void ShortsSelected(int imageIndex) //Which Image is selected
    {
        selectedIndex = imageIndex;
        CustomiazationMenuScript.instance.categoryState = CustomiazationMenuScript.Category.shorts;
        CustomiazationMenuScript.instance.ItemNumber= selectedIndex; //assigning the item number
        
        if(GenericVariables.checkUnlockedShortWithStatus(selectedIndex) ==0) //locked
        {
            ShowBuyButton(true);
            ShowWatchButton(false);

        }
        else if (GenericVariables.checkUnlockedShortWithStatus(selectedIndex) == 1) //watch ad
        {
            ShowBuyButton(false);
            ShowWatchButton(true);

        }
        else if (GenericVariables.checkUnlockedShortWithStatus(selectedIndex) == 2) //unlocked
        {
            GenericVariables.saveShorts(selectedIndex);
            ShortsTickImagesReseter();
            ShortTickImages[selectedIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
            ShowWatchButton(false);

        }

        ChangeShorts(selectedIndex); //List is present in menu manager with relevant indices
        Debug.Log("Shorts Selected " + selectedIndex);
    }
    public void FootballSelected(int imageIndex) //Which football Image is selected
    {
        selectedIndex = imageIndex;
        CustomiazationMenuScript.instance.categoryState = CustomiazationMenuScript.Category.football;
        CustomiazationMenuScript.instance.ItemNumber = selectedIndex;//assigning the item number
        if(GenericVariables.checkUnlockedFootballWithStatus(selectedIndex) == 0)
        {
            ShowBuyButton(false);
            ShowWatchButton(false);

        }
        else if(GenericVariables.checkUnlockedFootballWithStatus(selectedIndex) == 1)
        {
            ShowBuyButton(false);
            ShowWatchButton(true);
            ChangeFootball(selectedIndex);

        }
        else if(GenericVariables.checkUnlockedFootballWithStatus(selectedIndex) == 2)
        {
            GenericVariables.saveFootball(selectedIndex);
            FootballTickImagesReseter();
            FootballTickImages[selectedIndex].gameObject.SetActive(true);
            ShowBuyButton(false);
            ShowWatchButton(false);
            ChangeFootball(selectedIndex);
        }
        Debug.Log("Football Selected " + selectedIndex);

        /*    if (FootballLockImages[imageIndex].gameObject.activeSelf)
            {
                ShowBuyButton(true);
            }
            else
            {
                GenericVariables.saveFootball(imageIndex);
                FootballTickImagesReseter();
                FootballTickImages[imageIndex].gameObject.SetActive(true);
                ShowBuyButton(false);
            }
        */
     //   Debug.Log("Football Selected " + selectedIndex);
      //  ChangeFootball(imageIndex);
    }

    public void BuyButtonClicked()
    {
        if (BodyScrollView.activeSelf)
        {
            CustomizationBought(selectedIndex , "body"); //Send with relevant image index for pricing
        }
        else if(FaceScrollView.activeSelf)
        {
           CustomizationBought(selectedIndex, "skin");
        }
        else if (FootballScrollView.activeSelf)
        {
            CustomizationBought(selectedIndex, "football");
        }
        else if (EmoteScrollView.activeSelf)
        {
            CustomizationBought(selectedIndex, "emote");
        } 
        else if (ShortsScrollView.activeSelf)
        {
            CustomizationBought(selectedIndex, "shorts");
        }
    }
    public void Customize(int ind)
    {
        EmoteScrollView.SetActive(false);
        BodyScrollView.SetActive(false);
        FaceScrollView.SetActive(false);
        FootballScrollView.SetActive(false);
        ShortsScrollView.SetActive(false);
        if (ind == 0)
        {
            FaceScrollView.SetActive(true);
        }
        else if (ind == 1)
        {
            BodyScrollView.SetActive(true);
        }
        else if (ind == 2)
        {
            FootballScrollView.SetActive(true);
        }
        else if (ind == 3)
        {
            EmoteScrollView.SetActive(true);
        } 
        else if (ind == 4)
        {
            ShortsScrollView.SetActive(true);
        }
    }
    
    public bool CustomizationBought(int ind, string type)
    {
        switch (type)
        {
            case "body":
                if (GenericVariables.checkUnlockedBodyWithStatus(ind)!=2) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(BodyPrices[ind]))
                    {

                        GenericVariables.saveUnlockedBodyWithStatus(ind,2);
                        GenericVariables.saveBody(ind);
                        BodyLockImages[ind].gameObject.SetActive(false);
                        ChangeBody(ind);
                        /*if(GameConstants.RewardBodystoOffer.Contains(ind))
                        {
                            GameConstants.SetGameOverRewardClaim(2);
                        }*/
                        buyButton.SetActive(false);

                        KitsLockingUnlocking();
                      

                        AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.kit, ind, AnalyticsManagerSoccerKicks.Unlock_Type.By_Coins);
                    }
                    else
                    {
                        //Not Enough Coins 
                        MenuManager.Instance.AlertDialogMenu.SetActive(true);
                    }

                }
                else
                {
                    //already bought
                    
                    return false;
                }
                break; 
            case "shorts":
                if (GenericVariables.checkUnlockedShortWithStatus(ind)!=2) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(ShortsPrices[ind]))
                    {

                        GenericVariables.saveUnlockedShortsWithStatus(ind,2);
                        GenericVariables.saveShorts(ind);
                        ShortsLockImages[ind].gameObject.SetActive(false);
                        ChangeShorts(ind);
                        /*if(GameConstants.RewardBodystoOffer.Contains(ind))
                        {
                            GameConstants.SetGameOverRewardClaim(2);
                        }*/
                        buyButton.SetActive(false);

                        ShortsLockingUnlocking();
                      

                        AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.shorts, ind, AnalyticsManagerSoccerKicks.Unlock_Type.By_Coins);
                    }
                    else
                    {
                        //Not Enough Coins 
                        MenuManager.Instance.AlertDialogMenu.SetActive(true);
                    }

                }
                else
                {
                    //already bought
                    
                    return false;
                }
                break;
            case "skin":
                if (GenericVariables.checkUnlockedSkinWithStatus(ind)!=2) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(SkinPrices[ind]))
                    {
                        GenericVariables.saveUnlockedSkinWithStatus(ind,2);
                        GenericVariables.saveSkin(ind);
                        Debug.Log("Skin issss     " + ind);
                        SkinLockImages[ind].gameObject.SetActive(false);
                        buyButton.SetActive(false);
                        ChangeSkin(ind);

                        SkinsLockingUnlocking();
                     
                        AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.skin, ind, AnalyticsManagerSoccerKicks.Unlock_Type.By_Coins);

                    }
                    else
                    {
                        //Not Enough Coins 
                        MenuManager.Instance.AlertDialogMenu.SetActive(true);
                    }
                }
                else
                {
                    //already bought

                    return false;
                }
                break;
            case "football":
                if (GenericVariables.checkUnlockedFootballWithStatus(ind)!=2) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(FootballPrices[ind]))
                    {
                        GenericVariables.saveUnlockedFootballWithStatus(ind,2);
                        GenericVariables.saveFootball(ind);
                        Debug.Log("football issss     " + ind);
                        FootballLockImages[ind].gameObject.SetActive(false);
                        buyButton.SetActive(false);
                        ChangeFootball(ind);

                      
                        FootballsLockingUnlocking();

                        AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.ball, ind, AnalyticsManagerSoccerKicks.Unlock_Type.By_Coins);

                    }
                    else
                    {
                        //Not Enough Coins 
                        MenuManager.Instance.AlertDialogMenu.SetActive(true);
                    }
                }
                else
                {
                    //already bought

                    return false;
                }
                break;
            case "emote":
                if (GenericVariables.checkUnlockedCelebrationWithStatus(ind)!=2) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(CelebrationPrices[ind]))
                    {
                        GenericVariables.saveUnlockedCelebrationWithStatus(ind,2);
                        GenericVariables.saveCelebration(ind);
                        Debug.Log("Celebration issss     " + ind);
                        CelebrationLockImages[ind].gameObject.SetActive(false);
                        buyButton.SetActive(false);
                        ChangeCelebration(ind);
                        NewCelebrationBought = true;


                        CelebrationsLockingUnlocking();
                        AnalyticsManagerSoccerKicks.Instance.Customization_Analytics(AnalyticsManagerSoccerKicks.Customization_Type.celebration, ind, AnalyticsManagerSoccerKicks.Unlock_Type.By_Coins);

                    }
                    else
                    {
                        //Not Enough Coins 
                        MenuManager.Instance.AlertDialogMenu.SetActive(true);
                    }
                }
                else
                {
                    //already bought

                    return false;
                }
                break;
            default:
                break;

        }
        return true;
        //Unlock at specific index
    }
    public void ChangeSkin(int ind)
    {
        Texture skin = SkinTextures[ind];
        //UIPlayerSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        //MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        for(int a=0;a < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin.Length;a++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerSkin[a].GetComponent<Renderer>().material.SetTexture("_MainTex", skin);

        }
        
        for(int i=0; i < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray.Length;i++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[i].SetActive(false);
        }

        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().PlayerHeadArray[ind].SetActive(true);
        SkinTickImagesReseter();

        if (GenericVariables.checkUnlockedSkinWithStatus(ind) == 2)
        {
            SkinTickImages[ind].gameObject.SetActive(true);
        }

        /*if (!SkinLockImages[ind].gameObject.activeSelf)
        {
            SkinTickImages[ind].gameObject.SetActive(true);


          //  MenuManager.Instance.UIPlayerMainSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
           // MenuManager.Instance.UIPlayerMultiplayerSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);

        }*/
        priceText.text = SkinPrices[ind].ToString();

    }
    
    public void ChangeBody(int ind)
    {
        Texture body = BodyTextures[ind];
        //UIPlayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
        for(int  i=0; i < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody.Length;i++)
        {
            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerBody[i].GetComponent<Renderer>().material.SetTexture("_MainTex", body);

        }
        BodyTickImagesReseter();

        if (GenericVariables.checkUnlockedBodyWithStatus(ind) == 2)
        {
            BodyTickImages[ind].gameObject.SetActive(true);

        }
        /*
        if (!BodyLockImages[ind].gameObject.activeSelf)
        {
            BodyTickImages[ind].gameObject.SetActive(true);


           // MenuManager.Instance.UIPlayerMainBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
            //MenuManager.Instance.UIPlayerMultiplayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
        }*/
        priceText.text = BodyPrices[ind].ToString(); //Change price according to body selected
    }
    public void ChangeShorts(int ind)
    {
        Texture Short = ShortTextures[ind];

        //UIPlayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);

        for(int a=0;a < MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts.Length;a++)
        {

            MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIPlayerShorts[a].GetComponent<Renderer>().material.SetTexture("_MainTex", Short);
        }
           
        ShortsTickImagesReseter();

        if (GenericVariables.checkUnlockedShortWithStatus(ind) == 2)
        {
            ShortTickImages[ind].gameObject.SetActive(true);

        }
        /*
        if (!BodyLockImages[ind].gameObject.activeSelf)
        {
            BodyTickImages[ind].gameObject.SetActive(true);


           // MenuManager.Instance.UIPlayerMainBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
            //MenuManager.Instance.UIPlayerMultiplayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", body);
        }*/
        priceText.text = ShortsPrices[ind].ToString(); //Change price according to body selected
    }
    public void ChangeFootball(int ind)
    {
        Texture footbaltexture = FootballTextures[ind];
       // UIFootball.GetComponent<Renderer>().material.SetTexture("_MainTex", footbaltexture);
        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIFootball.GetComponent<Renderer>().material.SetTexture("_MainTex", footbaltexture);

        FootballTickImagesReseter();
        if (GenericVariables.checkUnlockedFootballWithStatus(ind) == 2)
        {
            FootballTickImages[ind].gameObject.SetActive(true);

        }
       /* if (!FootballLockImages[ind].gameObject.activeSelf)
        {
            FootballTickImages[ind].gameObject.SetActive(true);

          //  MenuManager.Instance.UIFootballMainMenu.GetComponent<Renderer>().material.SetTexture("_MainTex", footbaltexture);
           // MenuManager.Instance.UIMultiplayerFootball.GetComponent<Renderer>().material.SetTexture("_MainTex", footbaltexture);
        }*/
        priceText.text = FootballPrices[ind].ToString();
    }

    public void ChangeCelebration(int ind)
    {

        // animationOrginal = animatorOverrideController["Take 001"];

     //   UIFootball.SetActive(false);
        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIFootball.SetActive(false);

        animatorOverrideController["Celebrate"] = animationClips[ind];           //assign required animation on "Take 001" animation

        animator.runtimeAnimatorController = animatorOverrideController;

        //  animator.runtimeAnimatorController.name = "CelebrationAnimator";

        // animator.SetTrigger("PlayCustom");

        animator.Play("Celebration", -1, 0f);

      //  animator.Play("Celebration");

        CelebrationTickImagesReseter();

        if (GenericVariables.checkUnlockedCelebrationWithStatus(ind) == 2)
        {
            CelebrationTickImages[ind].gameObject.SetActive(true);

        }
        
        /*
        if (!CelebrationLockImages[ind].gameObject.activeSelf)
        {
            CelebrationTickImages[ind].gameObject.SetActive(true);
        }*/
        priceText.text = CelebrationPrices[ind].ToString();

    }

    public void ChangeCelebrationWithoutPlaying(int ind)
    {
        MenuManager.Instance.SelectedPlayer.GetComponent<PlayerAttributeController>().UIFootball.SetActive(true);

        animatorOverrideController["Celebrate"] = animationClips[ind];           //assign required animation on "Take 001" animation

        animator.runtimeAnimatorController = animatorOverrideController;
        animator.Play("Idle_WithFootball", -1, 0f);

        CelebrationTickImagesReseter();

        if (GenericVariables.checkUnlockedCelebrationWithStatus(ind) == 2)
        {
            CelebrationTickImages[ind].gameObject.SetActive(true);

        }

        priceText.text = CelebrationPrices[ind].ToString();

    }


    public void UnlockKitShirtForSubscription()
    {
        for(int i=BodyTickImages.Length-1; i>=0; i--)
        {
            if (GenericVariables.checkUnlockedBodyWithStatus(i) != 2)
            {
                    GenericVariables.saveUnlockedBodyWithStatus(i, 2);
                break;
            }
        }
    }
    public void UnlockKitShortsForSubscription()
    {
        for (int i = ShortTickImages.Length - 1; i >= 0; i--)
        {
            if (GenericVariables.checkUnlockedShortWithStatus(i) != 2)
            {
                GenericVariables.saveUnlockedShortsWithStatus(i, 2);
                break;
            }
        }
    }
    public void UnlockBallsForSubscription()
    {
        for (int i = FootballTickImages.Length - 1; i >= 0; i--)
        {
            if (GenericVariables.checkUnlockedFootballWithStatus(i) != 2)
            {
                GenericVariables.saveUnlockedFootballWithStatus(i, 2);
                break;
            }
        }
    }



    public void BodyTickImagesReseter()
    {
        for(int i=0;i < BodyTickImages.Length;i++)
        {
            BodyTickImages[i].gameObject.SetActive(false);
        }
    } 
    public void ShortsTickImagesReseter()
    {
        for(int i=0;i < ShortTickImages.Length;i++)
        {
            ShortTickImages[i].gameObject.SetActive(false);
        }
    }
    public void SkinTickImagesReseter()
    {
        for(int i=0;i < SkinTickImages.Length;i++)
        {
            SkinTickImages[i].gameObject.SetActive(false);
        }
    }
    public void FootballTickImagesReseter()
    {
        for(int i=0;i < FootballTickImages.Length;i++)
        {
            FootballTickImages[i].gameObject.SetActive(false);
        }
    }
    public void CelebrationTickImagesReseter()
    {
        for (int i = 0; i < CelebrationTickImages.Length; i++)
        {
            CelebrationTickImages[i].gameObject.SetActive(false);
        }
    }
}
