﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEditor;

public class FortuneWheelManager : MonoBehaviour
{
    public static FortuneWheelManager Instance;
    private bool _isStarted;
    private float[] _sectorsAngles;
    private float _finalAngle;
    private float _startAngle = 0;
    private float _currentLerpRotationTime;

    [SerializeField] WaitForSeconds wait3Sec = new WaitForSeconds(3f);

    public Button TurnButton;
    public GameObject Pin;
    public GameObject Circle; 			// Rotatable Object with rewards
    public Text CoinsDeltaText; 		// Pop-up text with wasted or rewarded coins amount
    public Text CurrentCoinsText; 		// Pop-up text with wasted or rewarded coins amount
    public int TurnCost = 300;			// How much coins user waste when turn whe wheel
    public int CurrentCoinsAmount = 1000;	// Started coins amount. In your project it can be set up from CoinsManager or from PlayerPrefs and so on
    public int PreviousCoinsAmount;		// For wasted coins animation
    public Text Timer;
    public GameObject Backbutton;

    [SerializeField] GameObject WatchAdButton;

    public enum WatchAdState
    {
        notWatched, Watched
    }
    public WatchAdState watchAdState_;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Fortune wheel ELSE on AWAKE");
            Destroy(gameObject);
        }
    }
    private void OnEnable()
    {
        PluginCallBackManager.FortuneWheelRewardAdComplete_ += GiveRewardAdWatched;

        UITutorialManager.instance.HandFortuneWheel2.SetActive(false);

        watchAdState_ = WatchAdState.notWatched;
        WatchAdButton.SetActive(false);
        Backbutton.SetActive(false);
        MenuManager.Instance.headers[0].SetActive(true);
      //  TurnButton.enabled = true;
        TurnButton.gameObject.SetActive(true);

        Timer.gameObject.SetActive(false);
    }
    public void TurnWheel()
    {
        if (GameConstants.getFortuneWheelShowOnLevel2Availability() == "Yes")  //check to for fortune wheel to show after level 2
        {
            GameConstants.setFortuneWheelShowOnLevel2Availability("No");
        }

        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playSound("Spinwheel");
        }
        UITutorialManager.instance.TurnOffFortuneWheelHand();
      //  TurnButton.enabled = false;
        TurnButton.gameObject.SetActive(false);

        // Player has enough money to turn the wheel
        if (CurrentCoinsAmount >= TurnCost)
        {
            _currentLerpRotationTime = 0f;

            // Fill the necessary angles (for example if you want to have 12 sectors you need to fill the angles with 30 degrees step)
        //    _sectorsAngles = new float[] { 36, 72, 108, 144, 180, 216, 252, 288, 324, 360 };
            _sectorsAngles = new float[] { 19, 55, 91, 127, 163, 199, 235, 271, 307, 343 };
            int fullCircles = 5;
            float randomFinalAngle = _sectorsAngles[UnityEngine.Random.Range(0, _sectorsAngles.Length)];

            // Here we set up how many circles our wheel should rotate before stop
            _finalAngle = -(fullCircles * 360 + randomFinalAngle);
            _isStarted = true;

        }
    }

    private IEnumerator GiveAwardByAngle()
    {
        // Here you can set up rewards for every sector of wheel
        switch ((int)_startAngle)
        {

            case -343:
                MenuManager.Instance.moveObject("coins", Pin); //set Object to be moved and parent to start at
                MenuManager.Instance.moveConsumable(); //Start moving
                                                       // yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddCoins(600);
                break;
            case -307:
                MenuManager.Instance.moveObject("diamond", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddDiamonds(50);
                break;
            case -271:
                MenuManager.Instance.moveObject("diamond", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddDiamonds(5);
                break;
            case -235:
                MenuManager.Instance.moveObject("coins", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddCoins(400);
                break;
            case -199:
                MenuManager.Instance.moveObject("energy", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddEnergy(5);
                break;
            case -163:
                MenuManager.Instance.moveObject("coins", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddCoins(350);
                break;
            case -127:
                MenuManager.Instance.moveObject("coins", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddCoins(100);
                break;
            case -91:
                MenuManager.Instance.moveObject("energy", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddEnergy(10);
                break;
            case -55:
                MenuManager.Instance.moveObject("diamond", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddDiamonds(10);
                break;
            case -19:
                MenuManager.Instance.moveObject("coins", Pin);
                MenuManager.Instance.moveConsumable();
                //yield return new WaitForSeconds(3.0f);
                yield return wait3Sec;
                GenericVariables.AddCoins(800);
                break;
            default:
                break;
        }

        if(watchAdState_==WatchAdState.notWatched && SceneManager.GetActiveScene().buildIndex!=2)
        {
            WatchAdButton.SetActive(true);
            Timer.gameObject.SetActive(true);


            Invoke("EnableGoBackButton", 2f);
        }
        else
        {
            Timer.gameObject.SetActive(true);

            Invoke("EnableGoBackButton", 0.1f);
            Invoke("EnableBackButtonHand", 0.2f);

        }
        RewardedTimeManager.instance.GetReward();



    } //Gives award with angle specified

    void EnableBackButtonHand()
    {
        UITutorialManager.instance.HandFortuneWheel2.SetActive(true);

    } //Gives award with angle specified

    public void EnableGoBackButton()
    {
        Backbutton.SetActive(true);
    }
    void Update()
    {
        // Make turn button non interactable if user has not enough money for the turn
        if (_isStarted || CurrentCoinsAmount < TurnCost)
        {
            //  TurnButton.interactable = false;
            TurnButton.gameObject.SetActive(false);

            //TurnButton.GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            //TurnButton.interactable = true;
            //TurnButton.GetComponent<Image>().color = new Color(255, 255, 255, 1);
        }

        if (!_isStarted)
            return;

        float maxLerpRotationTime = 4f;

        // increment timer once per frame
        _currentLerpRotationTime += Time.deltaTime;
        if (_currentLerpRotationTime > maxLerpRotationTime || Circle.transform.eulerAngles.z == _finalAngle)
        {
            _currentLerpRotationTime = maxLerpRotationTime;
            _isStarted = false;
            _startAngle = _finalAngle % 360;
            StartCoroutine(GiveAwardByAngle());
            //StartCoroutine(HideCoinsDelta ());
        }

        // Calculate current position using linear interpolation
        float t = _currentLerpRotationTime / maxLerpRotationTime;

        // This formulae allows to speed up at start and speed down at the end of rotation.
        // Try to change this values to customize the speed
        t = t * t * t * (t * (6f * t - 15f) + 10f);

        float angle = Mathf.Lerp(_startAngle, _finalAngle, t);
        Circle.transform.eulerAngles = new Vector3(0, 0, angle);
    }
    public void OnClickBackButton()
    {
        if(SceneManager.GetActiveScene().buildIndex>1)              
        {
            if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
            {
                MenuManager.Instance.setFireAddPanel(false);

                //    Debug.Log("level complete nexttttttttttttttttt");
                PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();

                PopUpLevelDetails.LevelDetailsSetter();
    
                GamePlayManager.instance.ResetScore();
            
                MenuManager.Instance.DisableAll();

                MenuManager.Instance.LoadLevel();
                //   MenuManager.Instance.levelSuccessTakeToLevelMenu();

            }

        }
        else
        {
            UIAnimations.instance.MainButton(6);
        }
    }
    public void watchAdClicked()
    {

        GenericVariables.RewardAdIndex = 11;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }

    public void GiveRewardAdWatched()
    {
        watchAdState_ = WatchAdState.Watched;
        GenericVariables.RewardAdIndex = -1;
        WatchAdButton.SetActive(false);

        CancelInvoke("EnableGoBackButton");
        Backbutton.SetActive(false);

        Timer.gameObject.SetActive(false);

        MenuManager.Instance.headers[0].SetActive(true);
        UITutorialManager.instance.FortuneWheelClaimTutorial();
       // TurnButton. enabled = true;
        TurnButton.gameObject.SetActive(true);

        //  StartCoroutine(DelayGiveRewardAdWatched());
    }

    private void OnDisable()
    {
        PluginCallBackManager.FortuneWheelRewardAdComplete_ -= GiveRewardAdWatched;

    }

}
