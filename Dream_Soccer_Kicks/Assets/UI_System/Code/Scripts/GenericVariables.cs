﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using UnityEditor;
using UnityEngine;

public static class GenericVariables
{
    public static int RewardAdIndex = -1;            //ahmad added

    #region Main Initializations
    public static void CreateKeys() //Function to create required keys in Player Prefs only run at start of game
    {
        if (!PlayerPrefs.HasKey(TotalLevelsPassed))
        {
            PlayerPrefs.SetInt(TotalLevelsPassed, 0);
        }
        if (!PlayerPrefs.HasKey(levelstring))
        {
            PlayerPrefs.SetInt(levelstring, 0);
        }
        if (!PlayerPrefs.HasKey(skin))
        {
            PlayerPrefs.SetInt(skin, 0);
        }
        if (!PlayerPrefs.HasKey(body))
        {
            PlayerPrefs.SetInt(body, 0);
        }
        if (!PlayerPrefs.HasKey(coinStr))
        {
            PlayerPrefs.SetInt(coinStr, 0);
        }
        if (!PlayerPrefs.HasKey(diamondStr))
        {
            PlayerPrefs.SetInt(diamondStr, 0);
        }
        if (!PlayerPrefs.HasKey(energyStr))
        {
            PlayerPrefs.SetInt(energyStr, 0);
        }
        if (!PlayerPrefs.HasKey(fireStr))
        {
            PlayerPrefs.SetInt(fireStr, 0);
        }
        if (!PlayerPrefs.HasKey(fireStr))
        {
            PlayerPrefs.SetInt(fireStr, 0);
        }
    }
    public static void ResetValues()
    {
        PlayerPrefs.SetInt(TotalLevelsPassed, 0);
        PlayerPrefs.SetInt(levelstring, 0);
        PlayerPrefs.SetInt(coinStr, 0);
        PlayerPrefs.SetInt(diamondStr, 0);
        PlayerPrefs.SetInt(energyStr, 0);
        PlayerPrefs.SetInt(fireStr, 0);
        MenuManager.Instance.UpdateCoins(0);
        MenuManager.Instance.UpdateEnergy(0);
        MenuManager.Instance.UpdateDiamonds(0);
        MenuManager.Instance.UpdateFire(0);
    } //Resets all Keys use with caution
    #endregion

    #region Level Manager
    // Start is called before the first frame update
    public static int levelsPassed;
    public static string TotalLevelsPassed = "LevelsPassed";
    public static string levelstring = "Level"; //Numbers concatenated further

  /*  public static void setTotalLevelsPassed()
    {
        PlayerPrefs.SetInt(TotalLevelsPassed, levelsPassed);
    }*/
    public static void setTotalLevelsPassed(int lev)
    {
        PlayerPrefs.SetInt(TotalLevelsPassed, lev);
    }
    //public static void setTotalLevelsPassed(int totalLevels)
    //{
    //    levelsPassed = totalLevels;
    //    PlayerPrefs.SetInt(TotalLevelsPassed, totalLevels);
    //}
    public static void incrementLevel()
    {
        levelsPassed++;
        PlayerPrefs.SetInt(TotalLevelsPassed, levelsPassed);
       /* if (GameConstants.TrophiesLevelList.Contains(levelsPassed) && (levelsPassed == GenericVariables.getTotalLevelsPassed())) //if level is trophy and played first time
        {
            
        }*/
     
    } //Cal this when level is completed
    public static int getTotalLevelsPassed()
    {
        levelsPassed = PlayerPrefs.GetInt(TotalLevelsPassed, 1);
        return levelsPassed;
    } //Returns total levels passed

    public static int getRating(int levelNo) //Returns rating at levelNo
    {
        return PlayerPrefs.GetInt(levelstring + (levelNo.ToString()));
    }

    public static void setRating(int levelNo, int noOfStars) //Sets Rating at levelNo
    {
        PlayerPrefs.SetInt(levelstring + (levelNo.ToString()), noOfStars);
    }

    #endregion

    #region Player Customization Manager
    public static string skin = "Skin";
    public static string body = "Body";
    public static string shorts = "Shorts";
    public static string football = "Football";
    public static string celebration = "Celebration";
    //To change Player skins and body go to main menu manager script and add textures at the end
    //Remember to add textures in same sequence as avatar buttons
    public static void saveSkin(int i)
    {
        PlayerPrefs.SetInt(skin, i);
    }
    public static void saveBody(int i)
    {
        PlayerPrefs.SetInt(body, i);
    } public static void saveShorts(int i)
    {
        PlayerPrefs.SetInt(shorts, i);
    }
    public static void saveFootball(int i)
    {
        PlayerPrefs.SetInt(football, i);
    }
    public static void saveCelebration(int i)
    {
        PlayerPrefs.SetInt(celebration, i);
    }

    public static void saveUnlockedSkinWithStatus(int skinNumber,int Status)
    {
        //PlayerPrefs.SetInt(skin + i, i);
        PlayerPrefs.SetInt(skin + skinNumber, Status);

    }
    public static void saveUnlockedBodyWithStatus(int bodyNumber,int Status)
    {   //0 status means locked
        //1 status means watch ad
        //2 status means unlocked

        PlayerPrefs.SetInt(body + bodyNumber, Status);
    } 
    public static void saveUnlockedShortsWithStatus(int shortNumber,int Status)
    {   //0 status means locked
        //1 status means watch ad
        //2 status means unlocked
        PlayerPrefs.SetInt(shorts + shortNumber, Status);
    }
    public static void saveUnlockedFootballWithStatus(int footballNumber, int Status)
    {
        //0 status means locked
        //1 status means watch ad
        //2 status means unlocked
        PlayerPrefs.SetInt(football + footballNumber, Status);
    } 
    public static void saveUnlockedCelebrationWithStatus(int celebrationNumber, int Status)
    {
        //0 status means locked
        //1 status means watch ad
        //2 status means unlocked
        PlayerPrefs.SetInt(celebration + celebrationNumber, Status);
    }

    public static int checkUnlockedSkinWithStatus(int i)     //0 means locked, 1 means watch ad, 2 means unlocked
    {
      
        return PlayerPrefs.GetInt(skin + i,0);

    }
    public static int checkUnlockedBodyWithStatus(int i)    //0 means locked, 1 means watch ad, 2 means unlocked
    {
        //if (PlayerPrefs.HasKey(body + i))
        //  {
        /* if(PlayerPrefs.GetInt(body + i)==1)   //means you have unlocked just watch video
         {

         }*/
        //     return (true, PlayerPrefs.GetInt(body + i));
        //  }

        return PlayerPrefs.GetInt(body + i,0);
    } 
    public static int checkUnlockedShortWithStatus(int i)    //0 means locked, 1 means watch ad, 2 means unlocked
    {
        //if (PlayerPrefs.HasKey(body + i))
        //  {
        /* if(PlayerPrefs.GetInt(body + i)==1)   //means you have unlocked just watch video
         {

         }*/
        //     return (true, PlayerPrefs.GetInt(body + i));
        //  }

        return PlayerPrefs.GetInt(shorts + i,0);
    }
    public static int checkUnlockedFootballWithStatus(int i)    //0 means locked, 1 means watch ad, 2 means unlocked
    {
        return PlayerPrefs.GetInt(football + i,0);
    }

    public static int checkUnlockedCelebrationWithStatus(int i)     //0 means locked, 1 means watch ad, 2 means unlocked
    {

        return PlayerPrefs.GetInt(celebration + i,0);

    }

    /* public static bool checkBoughtSkin(int i)
    {
        if(PlayerPrefs.HasKey(skin+i))
        {
            return true;
        }
        return false;
    }*/


    /*public static bool checkBoughtFootball(int i)
    {
        if (PlayerPrefs.HasKey(football + i))
        {
            return true;
        }
        return false;
    }*/

    /*public static bool checkBoughtCelebration(int i)
    {
        if (PlayerPrefs.HasKey(celebration + i))
        {
            return true;
        }
        return false;
    }*/

    public static int getcurrSkin()
    {
        return PlayerPrefs.GetInt(skin);
    }

    public static int getcurrBody()
    {
        return PlayerPrefs.GetInt(body);
    }

    public static int getcurrShorts()
    {
        return PlayerPrefs.GetInt(shorts);
    }

     public static int getcurrFootball()
    {
        return PlayerPrefs.GetInt(football);
    }

     public static int getcurrCelebration()
    {
        return PlayerPrefs.GetInt(celebration);
    }


    #endregion

    #region Shop Manager
    public static string coinStr = "Coins";
    public static string diamondStr = "Diamonds";
    public static string energyStr = "Energy";
    public static string fireStr = "Fire";

    public static void AddCoins(int amount) //Adds coins and update text
    {
        int count = PlayerPrefs.GetInt(coinStr);
        count = count + amount;
        PlayerPrefs.SetInt(coinStr, count);
        MenuManager.Instance.UpdateCoins(count);
    }
    
    public static bool RemoveCoins(int amount) //Remove Coins returns false when coin are less
    {
        int count = PlayerPrefs.GetInt(coinStr);
        count = count - amount;
        if (count < 0)
        {
            return false;
        }
        PlayerPrefs.SetInt(coinStr, count);
        MenuManager.Instance.UpdateCoins(count);
        return true;
    }
    public static void AddDiamonds(int amount)//Adds Diamonds to current amount ad updates text
    {
        int count = PlayerPrefs.GetInt(diamondStr);
        count = count + amount;
        PlayerPrefs.SetInt(diamondStr, count);
        MenuManager.Instance.UpdateDiamonds(count);
    } 
    public static bool RemoveDiamonds(int amount)//Remove diamonds returns false when diamonds are less
    {
        int count = PlayerPrefs.GetInt(diamondStr);
        count = count - amount;
        if(count < 0)
        {
            return false;
        }
        PlayerPrefs.SetInt(diamondStr, count);
        MenuManager.Instance.UpdateDiamonds(count);
        return true;
    } 
    public static void AddEnergy(int amount)//Adds Energy to current amount and updates text
    {
        int count = PlayerPrefs.GetInt(energyStr);
        count = count + amount;
        PlayerPrefs.SetInt(energyStr, count);
        MenuManager.Instance.UpdateEnergy(count);
    } 
    public static bool RemoveEnergy(int amount) //Removes Energy by amount specified returns false when energy is less
    {
        int count = PlayerPrefs.GetInt(energyStr);
        count = count - amount;
        if (count < 0)
        {
            return false;
        }
        PlayerPrefs.SetInt(energyStr, count);
        MenuManager.Instance.UpdateEnergy(count);
        return true;
    }
    public static int GetEnergy()
    {
        return PlayerPrefs.GetInt(energyStr);
    }

    public static void AddFire(int amount)
    {
        int count = PlayerPrefs.GetInt(fireStr);
        count = count + amount;
        PlayerPrefs.SetInt(fireStr, count);
        MenuManager.Instance.UpdateFire(count);
    }// Add fire by amount specified
    public static bool Removefire(int amount) //Removes fire by amount returns false if not enough energy
    {
        int count = PlayerPrefs.GetInt(fireStr);
        count = count - amount;
        if (count < 0)
        {
            return false;
        }
        PlayerPrefs.SetInt(fireStr, count);
        MenuManager.Instance.UpdateFire(count);
        return true;
    }

    public static int getFireRemaining()
    {
       return PlayerPrefs.GetInt(fireStr, 0);

    }
    public static void LoadConsumables() //Loads and updates texts with latest number of coins ,energy and diamonds
    {
        int count = PlayerPrefs.GetInt(energyStr);
        MenuManager.Instance.UpdateEnergy(count);
        count = PlayerPrefs.GetInt(diamondStr);
        MenuManager.Instance.UpdateDiamonds(count);
        count = PlayerPrefs.GetInt(coinStr);
        MenuManager.Instance.UpdateCoins(count);

    }


    #endregion

}
