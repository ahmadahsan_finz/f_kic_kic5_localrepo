﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelButtonsController : MonoBehaviour //Script attached on each button
{
    public static LevelButtonsController instance;
    Button levelSelect; //Button object
    public UnityEngine.UI.Text levelText;
    Image im;
    public GameObject gift; //Gift Image
    public GameObject BossImage; //Boss Image
    public GameObject cup; //Cup Imaage
    public Sprite selected;
    public Sprite released;
    public GameObject NewUnlocked;
    public static int selectedlevel;
    public GameObject penaltyPopUp;
    public Text penaltyText;
	public GameObject handTutorial;
    public GameObject [] stars;
    public GameObject [] LevelButtons;

    //taken from ButtonFiller.cs
    public static int currFound;
    public int currLevel;
    public Transform parent;
    public ScrollRect scrollRect;
    public GameObject Hand;
    public GameObject CurrentLevelIndicator;

    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {

        }

        levelSelect = GetComponentInChildren<Button>();
        levelText = GetComponentInChildren<Text>();
        im = GetComponentInChildren<Image>();
        im.sprite = released;
    }
       
    public void OnEnable()    
    {
            Hand.SetActive(false);
        currLevel = GameConstants.GetLevelNumber();
        int levelPassed = GenericVariables.getTotalLevelsPassed(); //60;// 
        GenericVariables.setTotalLevelsPassed(levelPassed);
        currFound = 0;

        if (GameConstants.TrophiesLevelList.Contains( GenericVariables.getTotalLevelsPassed()-1) && GameConstants.GetSeasonCompleted()=="Yes")  //check for if season has completed adn show trophy dilaog
        {
          MenuManager.Instance.TrophyUnlockMenu.SetActive(true);
          GameConstants.SetSeasonCompleted("No");
        }

        //else if (GameConstants.getStartGamePlayTutorial() != "Yes" && (RewardOnGameStartMenu.RewardKickCloseState!=RewardOnGameStartMenu.RewardKickDialogClose.closed) && RewardedTimeManager.instance.CheckTimerRewardKickForExternalUser())    //check if reward kick timer is up and show reward kick dialog
        //{
        //    Debug.Log("RewardKickDialog show ");

        //    MenuManager.Instance.RewardKickPopUpShow();
        //}

        //     Debug.Log("Levels Passed :  " + levelPassed);

        if (GameConstants.getFirstStartUITutorial() != "Yes")
       {
           TutorialHandTurnOnOFF(false);

                // Invoke("PopupDetailMenuShow", 0.2f);  //disabling popup
            }
            else
            {
                TutorialHandTurnOnOFF(true);
            }

            StartCoroutine(DelayScrollSnap());
            SetLevelButtonsDetails();      
    }
     
    
        
    public void SetLevelButtonsDetails() 
    {
            for (int levelNo = 1; levelNo <=GameConstants.getTotalLevels(); levelNo++)
            {
                if (levelNo <= GenericVariables.getTotalLevelsPassed())
                {
                    int NumberOfStars = GenericVariables.getRating(levelNo);

                    LevelButtons[levelNo].transform.GetChild(0).GetComponent<Button>().interactable = true;

                    //   LevelButtons[i]..GetChild(0)
                    SetStars(levelNo, NumberOfStars);
                }
                else
                {
                  //  ButtonFiller.currFound = int.Parse(levelNo);
                    LevelButtons[levelNo].transform.GetChild(0).GetComponent<Button>().interactable = false;
                }

                LevelButtons[levelNo].transform.GetChild(1).GetComponent<Text>().text = levelNo.ToString();

            }

        }
    IEnumerator DelayScrollSnap()           //set scroller to current level
    {
        yield return new WaitForSeconds(0);
        //  Debug.Log("total levls passed  " + buttons[GenericVariables.getTotalLevelsPassed()].GetComponent<Text>().text);
        //  buttons[GenericVariables.getTotalLevelsPassed()].showText();

        //   buttons[GenericVariables.getTotalLevelsPassed() - 1].setNewUnlockedLevel();

        //   Debug.Log("DelayScrollSnap " + GenericVariables.getTotalLevelsPassed());
        //   Debug.Log("DelayScrollSnap " + (GenericVariables.getTotalLevelsPassed() - 1));

        //setInitialPos(buttons[GenericVariables.getTotalLevelsPassed()-1].gameObject);

//        setInitialPos(LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed() - 1].gameObject);
 
         setInitialPos(LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed()].gameObject);


        //***setting current level indicator***//
        if(GenericVariables.getTotalLevelsPassed()==GameConstants.getTotalLevels())  //if 300 levels are passed tha disble indicator
        {
            CurrentLevelIndicator.SetActive(false);
        }
        else
        {
            CurrentLevelIndicator.SetActive(true);
            CurrentLevelIndicator.transform.parent = LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed()].transform;
            CurrentLevelIndicator.transform.SetSiblingIndex(8);
            CurrentLevelIndicator.transform.localPosition = new Vector3(0f, 15f, 0f);
            CurrentLevelIndicator.transform.GetChild(1).GetComponent<Text>().text = GenericVariables.getTotalLevelsPassed().ToString(); //setting current level number on indicator

        }
    }
    public void TutorialHandTurnOnOFF(bool active)
    {
        // buttons[GenericVariables.getTotalLevelsPassed() - 1].TutorialHandTurnOff();
        //   LevelButtonsController.instance.LevelButtons[GenericVariables.getTotalLevelsPassed()].transform.GetChild()
        Hand.SetActive(active);
    }
    private void setInitialPos(GameObject target)
    {
        //Canvas.ForceUpdateCanvases();
        scrollRect.content.anchoredPosition = Vector2.zero;
        Vector2 pos = (Vector2)scrollRect.content.transform.InverseTransformPoint(scrollRect.content.position)
                        - (Vector2)scrollRect.transform.InverseTransformPoint(target.GetComponent<RectTransform>().position);
        scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, pos.y);
    }

    public void PopupDetailMenuShow()
    {
        //MenuManager.Instance.setMain(false);
        GameConstants.SetLevelNumber(GenericVariables.getTotalLevelsPassed());
        PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
        PopUpLevelDetails.LevelDetailsSetter();

        //selectedlevel = levelno;
        int selectedlevel = GameConstants.GetLevelNumber();

        GameConstants.SetGameMode("Classic");
        //	Debug.Log("Game mode issssssssssssssss " + GameConstants.GetGameMode());

        // MenuManager.Instance.ShowPenaltyPanel(levelno);
        if (GameConstants.GetMode() == 10)
        {
            MenuManager.Instance.ShowTournamentPanelPopup(selectedlevel);
        }
        else
        {
            MenuManager.Instance.ShowPenaltyPanel(selectedlevel);
        }
        //Your level number comes here
    }
    public void SetStars(int levelNo,int noOfStars)
    {
/*        if (noOfStars > 3)
        {
            return;
        }*/
     /*   for (int a = 0; a < 3; a++)
        {
            //	Debug.Log("Star no. which is being off " + a);
            // stars[a].SetActive(false);
            LevelButtons[levelNo].transform.GetChild(5).gameObject.SetActive(false);
            LevelButtons[levelNo].transform.GetChild(6).gameObject.SetActive(false);
            LevelButtons[levelNo].transform.GetChild(7).gameObject.SetActive(false);
           // LevelButtons[levelNo].transform.GetChild(2).GetChild(a).GetChild(0).gameObject.SetActive(false);
        }*/
        if(noOfStars==0)
        {
            LevelButtons[levelNo].transform.GetChild(5).gameObject.SetActive(false);
            LevelButtons[levelNo].transform.GetChild(6).gameObject.SetActive(false);
            LevelButtons[levelNo].transform.GetChild(7).gameObject.SetActive(false);
        }
        else if (noOfStars == 1)
        {
            LevelButtons[levelNo].transform.GetChild(5).gameObject.SetActive(true);
            LevelButtons[levelNo].transform.GetChild(6).gameObject.SetActive(false);
            LevelButtons[levelNo].transform.GetChild(7).gameObject.SetActive(false);
        }  
        else if (noOfStars == 2)
        {
            LevelButtons[levelNo].transform.GetChild(5).gameObject.SetActive(true);
            LevelButtons[levelNo].transform.GetChild(6).gameObject.SetActive(true);
            LevelButtons[levelNo].transform.GetChild(7).gameObject.SetActive(false);
        } 
        else if (noOfStars == 3)
        {
            LevelButtons[levelNo].transform.GetChild(5).gameObject.SetActive(true);
            LevelButtons[levelNo].transform.GetChild(6).gameObject.SetActive(true);
            LevelButtons[levelNo].transform.GetChild(7).gameObject.SetActive(true);
        }
        /* for (int i = 0; i < noOfStars; i++)
         {
             //	Debug.Log("Star no. which is being on " + i);
             //stars[i].SetActive(true);
             LevelButtons[levelNo].transform.GetChild(2).GetChild(i).GetChild(0).gameObject.SetActive(true);

         }*/
    }
/*    public void setLevelButton(string levelNo)
    {
        if (int.Parse(levelNo) <= GenericVariables.getTotalLevelsPassed()) //Set Stars until level passed
        {           
           
            int n = GenericVariables.getRating(int.Parse(levelNo));
		//	Debug.Log("Level is sssss   " + n);
            setStars(n);
        }
        else //Set other non interactable
        {
            ButtonFiller.currFound = int.Parse(levelNo);
            this.GetComponentInChildren<Button>().interactable = false;
        }
      //  levelSelect.onClick.AddListener(delegate { OnClick(int.Parse(levelNo)); }); //Add listner t load relevant level
        levelText.text = levelNo;
    }*/

    public void setGift()
    {
        gift.SetActive(true); // Set current level's gift
    }   
    public void setBossLevelImage()
    {
        BossImage.SetActive(true); // Set current level's gift
    }
	public void setNewUnlockedLevel()           //highlights Current Level
	{
        if(NewUnlocked!=null)
        {

	    	NewUnlocked.SetActive(true); // Set current level's gift
        }

		if (GameConstants.getFirstStartUITutorial() == "Yes")
		{
			handTutorial.SetActive(true);
		}
	}
    public void TutorialHandTurnOff()
    {
        handTutorial.SetActive(false);

    }
    public void setCup()
    {
        cup.SetActive(true);
    }

  /*  public void setStars(int noOfStars) //Set Rating
    {

        if(noOfStars > 3)
        {
            return;
        }
		for(int a = 0; a < 3; a++)
        {
		//	Debug.Log("Star no. which is being off " + a);
			stars[a].SetActive(false);
            
        }
        for(int i = 0; i < noOfStars; i++)
        {
		//	Debug.Log("Star no. which is being on " + i);
			stars[i].SetActive(true);
        }
    }*/

	public void showText()
    {
		Debug.Log("	levelText.text =" + levelText.text);


	}

	public void setInteractable(bool active)
    {
		levelSelect.interactable = active;

	}
	/* mode 1= penalties																						//ahmad
        mode 2= simple freekicks
        mode 3= freekicks with dartboard only/bullseye
        mode 4= freekicks with wall
        mode 5= freekicks with wall  and dartboard/bullseye
        mode 6= freekicks with boxes
        mode 7= Goal Spot
        mode 8= Hit the pole 
        mode 9= freekicks with hoops/2x 3x circles
        mode 10= Tournament
        mode 11= freekicks with wall and hoops/2x 3x circles
        mode 12- Freekicks with dummy wall
        mode 13= freekicks with dummy wall and bullseye
        mode 14= freekicks with wood logs
        mode 15: freekicks with RoadblockA
        mode 16: freekicks with RoadblockB and moving defender
        mode 17: freekicks with Hoops
        mode 18: freekicks with Drums
        mode 19: freekicks with two Moving Defender
        mode 20: freekicks with pillars in goal and moving dummy no GK
        mode 21: Freekick directional pillars  and GK
        mode 22: Cones and wooden barrels
        mode 23: Freekick with One Static Dummy
        mode 24: Freekick with One Static Dummy and one moving dummy
        mode 25: Freekick with pillars in goal no GK
        mode 26: Freekick with directional pillars and no GK
        mode 27: Freekick with multiple Dartboards
        mode 28: Freekick with single Dartboard and moving dummy
        mode 29: Freekick with single Goal through Circle  No GK
        mode 30: Freekick with Single Goal through Circle with GK
        mode 31: Freekick with Single Goal through Circle with GK + Moving dummy
        mode 32: Freekick with two Goal through Circle No GK
        mode 33: Freekick with two Goal through Circle + GK 
        mode 34: Freekick with two Goal through Circle + GK + Moving Dummy 
        mode 35: Freekicks with cones variation 
        mode 36: Freekicks with cones variation and moving dummy
        mode 37: Freekicks with cones variation + GK
        mode 38: Freekicks with cones variation and moving dummy + GK
        mode 39: Freekicks with two Human Defenders + GK
        mode 40: Freekicks with Pillars Simple + No GK
        mode 41: Freekicks with DummyDef and Dartboard + No GK
        mode 42: Freekicks with Moving dartboard + GK (BOSS)
        mode 43: Freekicks with Coins + No GK (BONUS LEVEL) (Not Using)
        mode 44: Freekicks with Coins + No GK (BONUS LEVEL)
        mode 45: Freekicks with Trampoline + No GK (BOSS)
        mode 46: Freekicks with Two Moving Rail Defender + GK (BOSS)

		*/
	public void OnClickLevelButton(int levelno)
    {
		//MenuManager.Instance.setMain(false);
		GameConstants.SetLevelNumber(levelno);
		PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
		  PopUpLevelDetails.LevelDetailsSetter();
	
		//selectedlevel = levelno;
		selectedlevel = GameConstants.GetLevelNumber();

		GameConstants.SetGameMode("Classic");
        //	Debug.Log("Game mode issssssssssssssss " + GameConstants.GetGameMode());

        // MenuManager.Instance.ShowPenaltyPanel(levelno);


        /*if (GameConstants.GetMode() == 10)            //commented because we have disabled pop up menu //ahmad
		{
			MenuManager.Instance.ShowTournamentPanelPopup(selectedlevel);
		}
		else 
		{
			MenuManager.Instance.ShowPenaltyPanel(selectedlevel);
		}*/

        MenuManager.Instance.LoadLevel();                             //load level directly


		//Your level number comes here
	}

    public void OnClickUnlockNextLevelButton()
    {
        if(GenericVariables.getTotalLevelsPassed() < GameConstants.getTotalLevels())
        {
            int levelPassed = GenericVariables.getTotalLevelsPassed() + 1; //60;// 
                                                                   //     Debug.Log("Levels Passed :  " + levelPassed);
            GenericVariables.setTotalLevelsPassed(levelPassed);
            StartCoroutine(DelayScrollSnap());
            SetLevelButtonsDetails();
        }

        }
    

}
