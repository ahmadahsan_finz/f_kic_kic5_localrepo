﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
public class MenuManager : GenericSingletonClass<MenuManager>
{
    [Header("Main Canvas Camera")]
    public Camera cam;
    [Header("Main Canvas")]
    public Canvas mainCanvas;
    [Header("Main Panels")]
    public List<RectTransform> MainPanels; // Main Menu Panels

    [Header("Pop Ups")]
    public List<GameObject> popUps; // Popups like settings ,level failed or pass popUp etc.
    public GameObject LoadingScreen;

    [Header("Main Tabs")]
    public List<GameObject> headers; //Upper and Lower Tab

    [Header("LeaderBoard Tab")]
    public Button claimButton;
    public GameObject timerPanel;
    public Text timer;

    [Header("RewardKick Tab")]
    public Text RewardKickTimerText;
    public GameObject RewardKickTimer;
    public GameObject RewardKickClaimButton;
    public GameObject RewardKick_Start_Menu;
    public GameObject RewardKick_End_Menu;


    [Header("Upper Tab Texts")]
    public Text coinText;
    public Text diamondText;
    public Text energyText;
    public Text fireText;

    [Header("Upper Tab Panels")]
    public GameObject EnergyBarPanel;
    public GameObject diamondBarPanel;
    public GameObject FireAddPanel;
    public GameObject settingsButton;
    public GameObject coinBar;
    public Text EnergyReloadTimer;

    [Header("Main BackGround")]
    public GameObject mainBack; //Main Background

    [Header("Texts")]
    public Text penaltyText; //Text to show on Penalty Screen
 //   public Text TournamentPopuplevelText; //Text to show on Tournament pop up Screen
    public Text settingsText; //Settings Main Button Text

    [Header("Settings Buttons")]
    public Button SettingsBackButton; //Back Button on Settings Panel
    public GameObject privacyPolicyButton;
    public GameObject SettingsHomeButton;

    [Header("Main Menu Panels")]
    public GameObject UIPlayerMainSkin;
    public GameObject UIPlayerMainBody;
    public GameObject UIFootballMainMenu;
    public GameObject UIPlayer;

    [Header("Settings Panels")]
    public GameObject CoinPanel;
    public GameObject EnergyPanel;
    public GameObject DiamondPanel;

    [Header("Customization Panel")]
    public GameObject CustomUIPlayer;

/*    public GameObject UIPlayerSkin;
    public GameObject UIPlayerBody;*/
  /*  public GameObject FaceScrollView;
    public GameObject BodyScrollView;
    public GameObject EmoteScrollView;
    public GameObject buyButton;
    public Text priceText;
*/
    [Header("Spin Wheel Panel")]
    public Button turnButton;
    public Transform Pin;
    public GameObject coinsPrefab;
    public GameObject energyPrefab;
    public GameObject diamondPrefab;
    public GameObject firePrefab;


/*    [Header("Player Skins")]
    public List<Texture> SkinTextures;
    public List<Texture> BodyTextures;
    public List<Image> BodyLockImages;
    public List<Image> SkinLockImages;
    public List<int> SkinPrices;
    public List<int> BodyPrices;*/

    public bool coinsMove;
    public bool energyMove;
    public bool diamondMove;
    public bool fireMove;

    private List<GameObject> tempList;
    private int counter = 0;
    private int counter1 = 0;

    public float destructionTime;
    public float moveTime;
    public float scaleFactor;
    public iTween.EaseType easeType;
    public iTween.LoopType loop;
    public iTween.EaseType ScaleEaseType;

    public Text[] TargetScores;             //addition by ahmad
    public Text[] TargetScoresCoins;
    public Text TargetCoinsTournamentText;
    public Text LevelName;
    public bool LevelMenuLoadBool;
    private int currInd;
    public string FirstStart;

    public static bool SoundBool =true;
    public static bool MusicBool =true;
    public GameObject MusicOnImage;
    public GameObject MusicOffImage;
    public GameObject SoundOnImage;
    public GameObject SoundOffImage;

    public GameObject mainCamera;
    public GameObject NoRewardAdDialog;

    [Header("Level Success Panel")]
    public GameObject ClaimButtonLevelSuccess;
    public GameObject ClaimButton2XLevelSuccess;
    int CoinsEarned;
    public Text levelSuccessNumber;

    [Header("Level Fail Panel")]
    public Text levelFailNumber;

    [Header("Level End Ads")]
    public static bool GameOverBoolVideoAd=false;

    [Header("Mulitplayer Related")]
    public GameObject MultiplayerVersusScreen;
    public GameObject MultiplayerModeMenu;
    public GameObject MultiplayerPassedScreen;
    public GameObject MultiplayerFailedScreen;
/*    public GameObject UIPlayerMultiplayer;
    public GameObject UIPlayerMultiplayerSkin;
    public GameObject UIPlayerMultiplayerBody;
    public GameObject UIMultiplayerFootball;*/
    public GameObject UIPlayerMultiplayerGameoverSkin;
    public GameObject UIPlayerMultiplayerGameoverBody;
    public GameObject UIMultiplayerGameoverFootball;
    public GameObject UIMultiplayerPauseMenu;
    public GameObject UIMultiplayerOfferDialog;


    [Header("Alert Dialog Related")]
    public GameObject AlertDialogMenu;

    [Header("Language Panel")]
    public GameObject LanguagePanel;
    //[SerializeField] Text LanguageText;

    [Header("Trophy Menu")]
    public GameObject TrophyMenu;
    public GameObject TrophyUnlockMenu;

    [Header("Characters")]
    public GameObject[] Players_CharacterArray;
    public GameObject[] Players_CharacterArray_Menus;
    public GameObject[] GK_CharacterArray;
    public GameObject[] GK_CharacterArray_Menus;
    public GameObject SelectedPlayer;
    public GameObject SelectedGK;
    
    [Header("ChestBox Menu")]
    public GameObject ChestBoxMenu;

    [Header("Subscription Menu")]
    public GameObject SubscriptionMenu;
    public GameObject SubscriptionRewardShowMenu;


    [Header("Testing Buttons")]
    public GameObject CoinsButtons;
    public GameObject UnlockNextLevelButton;

    public bool BonusLevel;
    //  public bool MissionMenuLoadBool;
  /*  private void Awake()
    {
        SelectedPlayer = Instantiate(Players_CharacterArray_Menus[GameConstants.GetSelectedPlayer()]);
    }*/
    private void OnEnable()
    {
        Debug.Log("ONENABLE CALLED MENUMANAGER");
        GameConstants.SetSelectedPlayer(0);
        GameConstants.SetSelectedGK(0);
        SelectedPlayer = Instantiate(Players_CharacterArray_Menus[GameConstants.GetSelectedPlayer()]);
        SelectedGK = Instantiate(GK_CharacterArray_Menus[GameConstants.GetSelectedGK()]);
        SelectedGK.transform.SetParent( this.gameObject.transform);
        SelectedGK.gameObject.SetActive(false);

        coinsMove = false;
        energyMove = false;
        diamondMove = false;
        initializeScreen();
        GenericVariables.AddCoins(0);
    }

    private void Start()
    {
        
        //  Application.targetFrameRate = 60;   
         GenericVariables.AddCoins(0);
         GenericVariables.AddEnergy(0);
         GenericVariables.AddDiamonds(0);
         GenericVariables.AddFire(0);
/*        GenericVariables.AddFire(200);
        GenericVariables.AddCoins(50000);
        GenericVariables.AddEnergy(500);
        GenericVariables.AddDiamonds(1000);*/
        //CameraChange();
        FirstStart = GameConstants.getFirstStartofGame();
        //  Debug.Log("First Start and Fires are  " + FirstStart);

        if (FirstStart == "Yes")
        {
            //  GameConstants.setNumberOfFireballs(5);


            GenericVariables.AddFire(2);
            //  GenericVariables.AddCoins(25000);
            GenericVariables.AddEnergy(10);
            //  GenericVariables.AddDiamonds(0);
            /*   GenericVariables.AddFire(49);
               GenericVariables.AddCoins(459999);
               GenericVariables.AddEnergy(299);
               GenericVariables.AddDiamonds(499);*/
            Debug.Log("First Start and Fires are  " + GenericVariables.getFireRemaining());
            FirstStart = "No";
            GameConstants.setFirstStartofGame(FirstStart);

            //  LanguageButtonClick();   //set language panel on 

        }
        else
        {
            Debug.Log("rateus");

            AdController.instance.PromptRateMenu();   //call rateus on second game start
        }

                                                      //else if(RewardedTimeManager.instance.CheckTimerRewardKickForExternalUser()) //if its true 
                                                      //{
                                                      //    RewardKickPopUpShow();
                                                      //}


            if (AdConstants.isTestingLicenseAvailable())
        {
            CoinsButtons.SetActive(true);
            UnlockNextLevelButton.SetActive(true);
        }
        else
        {
            CoinsButtons.SetActive(false);
            UnlockNextLevelButton.SetActive(false);

        }


    //    Invoke("EnableSubscription_Offer_Menu", 0.5f);  //show subscription menu
    }

    public void EnableSubscription_Offer_Menu()
    {
        if(FirstStart != "Yes" && GameConstants.getFirstStartUITutorial() != "Yes")
        {
            if (GameConstants.GetSubscriptionStatus() == "No")
            {
                SubscriptionMenu.SetActive(true);
            }
        }
    }
    public void setMain(bool active)             //when loading Main menu coming back from gameplay scene       //moazan added
    {
        headers[1].gameObject.SetActive(active); //Sets Main Tab active or inactive
        mainBack.SetActive(active);
    }
   
    public void setLevelMenu(bool active)                   //when loading level menu coming back from gameplay scene       //ahmad added
    {
        headers[1].gameObject.SetActive(active); //Sets Main Tab active or inactive
        mainBack.SetActive(active);
        MainPanels[3].gameObject.SetActive(active);
    }
   
    public void ShowPopUp(int index) //Shows specific popUp
    {
        // overlay.SetActive(true);
        //currInd = index;
        DisablePopUps();
        //if(popUps[index].GetComponent<Animator>())
        //{
        //    popUps[index].GetComponent<Animator>().SetBool("BackPressed", false);

        //}
        popUps[index].SetActive(true);
      //  UIPlayer.SetActive(false);
      //  CustomUIPlayer.SetActive(false);
      //  c.renderMode = RenderMode.ScreenSpaceOverlay;
    }
    public void DisablePopUps() //Disable all PopUps
    {
        //mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
    //    UIPlayer.SetActive(true);
    //    CustomUIPlayer.SetActive(true);
        foreach (GameObject pop in popUps)
        {
            pop.SetActive(false); //Makes all popUps inActive
        }
      
    }
    public void setPanels() //Set al Panels InActive
    {
        foreach (RectTransform panels in MainPanels)
        {
            panels.gameObject.SetActive(false);
        }
    }

  

    /*public void setHUD() //Sets HUD inActive
    {
        MainPanels[4].gameObject.SetActive(false);
    }*/
    public void ShowPenaltyPanel(int levelNo) //Shows Up after level Selection
    {
        /*if(levelNo==1)
        {
            ButtonFiller.instance.Hand.SetActive(false);
        }*/

        if (GameConstants.getFirstStartUITutorial() == "Yes")
        {
            ButtonFiller.instance.TutorialHandTurnOnOFF(false);
        }
        
        //popUps[6].transform.SetSiblingIndex(1);
        penaltyText.text = levelNo.ToString(); //Set level text to be selected text
      //  PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
      //  PopUpLevelDetails.LevelDetailsSetter();
      //  string levelName = PopUpLevelDetails.LevelTypeName();
       
      //  LevelName.text = levelName;
        int[] TargetScoresInt =GameConstants.GetTargetScores();
        int[] TargetCoinsInt= GameConstants.GetTargetStars();

        for(int i=0;i<3;i++)
        {
            TargetScores[i].text = TargetScoresInt[i].ToString();
            TargetScoresCoins[i].text = TargetCoinsInt[i].ToString();

        }
        //setPanels();// Set All Main Panels false
        popUps[6].GetComponent<Animator>().SetBool("BackPressed", false);
        popUps[6].SetActive(true); //Show pwenalty panel
        popUps[0].SetActive(true);//Show overlay
        

    }
    public void ShowTournamentPanelPopup(int levelNo) //Shows Up after tournament level Selection          //ahmad added for tournament pop up
    {
        //   penaltyText.text = "Level " + levelNo; //Set level text to be selected text
        int[] TargetCoinsInt = GameConstants.GetTargetStars();
        TargetCoinsTournamentText.text = TargetCoinsInt[0].ToString();
        popUps[9].SetActive(true); //Show tournament panel
        popUps[0].SetActive(true);//Show overlay
    }

    /* public void ShowBuyButton(bool v)
     {
         buyButton.SetActive(v);
     }*/
    public void showSettings()
    {
        //mainCanvas.renderMode = RenderMode.ScreenSpaceOverlay;
        if (SceneManager.GetActiveScene().buildIndex == 2) // If we are at Gameplay than set text as "HOME" otherwise false
        {
            //  if ((GameConstants.GetGameMode() == "Classic" && !GameConstants.CutsceneIsOn && GameConstants.getFirstStartUITutorial() != "Yes" && !GamePlayManager.instance.LevelEndBool) || (GameConstants.GetGameMode()=="Missions" && GameConstants.getFirstStartUITutorial() != "Yes" && !GamePlayManager.instance.MissionEndBool))           //if tutorial is enabled 

            if ((GameConstants.GetGameMode() == "Classic" && GameConstants.GetLevelNumber() > 2 && !GamePlayManager.instance.LevelEndBool) || (GameConstants.GetGameMode() == "Classic" && GameConstants.GetLevelNumber() < 3 && !GameConstants.CutsceneIsOn && GameConstants.getStartGamePlayTutorial() != "Yes" && !GamePlayManager.instance.LevelEndBool) || (GameConstants.GetGameMode() == "Missions" && !GamePlayManager.instance.MissionEndBool) || (GameConstants.GetGameMode() == "RewardShot2" && !GamePlayManager.instance.RewardShotEndBool))
            {
                //     AdController.instance.ShowBannerAd();
                Time.timeScale = 0;                             //game pause
                                                                //  UIPlayer.SetActive(true);
                                                                //  CustomUIPlayer.SetActive(true);
                setPrivacyPolicyButton(false);
                SettingsHomeButton.SetActive(true);
                //   settingsText.text = "HOME";
                if (Shoot.share != null)
                {
                    Shoot.share._enableTouch = false;
                }

                setPanels(); //disable any main menu enabled(did to disable customization menu for now)

                popUps[0].SetActive(true);// Show Overlay

                popUps[3].GetComponent<Animator>().SetBool("BackPressed", false);
                Debug.Log("parameter is  " + popUps[3].GetComponent<Animator>().GetBool("BackPressed"));
                popUps[3].SetActive(true);// Show settings

                AdConstants.currentState = AdConstants.States.OnPause;
                AdController.instance.ChangeState();
            }
            else if (GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2")
            {
                //  Time.timeScale = 0;
                //   AdController.instance.ShowBannerAd();
                setPrivacyPolicyButton(false);
                SettingsHomeButton.SetActive(true);
                //   settingsText.text = "HOME";
                if (Shoot.share != null)
                {
                    Shoot.share._enableTouch = false;
                }
                popUps[0].SetActive(true);// Show Overlay

                popUps[3].GetComponent<Animator>().SetBool("BackPressed", false);
                Debug.Log("parameter is  " + popUps[3].GetComponent<Animator>().GetBool("BackPressed"));
                popUps[3].SetActive(true);// Show settings
            }
        }
        //if ((GameConstants.GetGameMode() == "Classic" && !GameConstants.CutsceneIsOn && GameConstants.getStartGamePlayTutorial() != "Yes" && !GamePlayManager.instance.LevelEndBool) || (GameConstants.GetGameMode()=="Missions" && !GamePlayManager.instance.MissionEndBool))           //if tutorial is enabled 

        else
        {
            //   UIPlayer.SetActive(false);
            //  CustomUIPlayer.SetActive(false);
            //setPrivacyPolicyButton(true);
            //  settingsText.text = "MORE GAMES"; // Change settings text for menus scene
            SettingsHomeButton.SetActive(false);
            popUps[0].SetActive(true);// Show Overlay

            popUps[3].GetComponent<Animator>().SetBool("BackPressed", false);
            Debug.Log("parameter is  " + popUps[3].GetComponent<Animator>().GetBool("BackPressed"));
            popUps[3].SetActive(true);// Show settings

            AdConstants.currentState = AdConstants.States.OnPause;
            AdController.instance.ChangeState();
        }




        // UIAnimations anim = new UIAnimations();
        // anim.FadeIn();
    }
    public void Loading(bool active)
    {
        //popUps[7].SetActive(active); // Show Loading
        
        headers[0].SetActive(true);

        LoadingScreen.SetActive(active); // Show Loading
   //     Debug.Log("inside Loading ");
        if (GameConstants.getStartGamePlayTutorial() == "Yes" && GameConstants.GetLevelNumber() < 3 && SceneManager.GetActiveScene().buildIndex == 1)       //when going from menu to gameplay
        {
     //       Debug.Log("inside Loading Tutorial on");

            setSettingsButton(false);

        }
        else if (GameConstants.getStartGamePlayTutorial() != "Yes")
        {
     //       Debug.Log("inside Loading Tutorial off");
           setSettingsButton(true);

        }
        StartCoroutine(StopLoad()); // Close after 1 seconds
        //Invoke("StopLoad", 0.7f); 
    }

    public IEnumerator StopLoad()
    {
        yield return new WaitForSeconds(0.8f);
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            PlayerScript.instance.ReloadShootAnim();

        }
        LoadingScreen.SetActive(false); // Show Loading
        
       // popUps[7].SetActive(false); //Stop Loading
    }
   
    public void setCamera(bool active)
    {
        cam.gameObject.SetActive(active); //change cam settings
    }

    public void setShopPanel(bool active)
    {
        MainPanels[2].gameObject.SetActive(active);
    }
  /*  public void setHUDActive()
    {
        MainPanels[4].gameObject.SetActive(true);
    }
    */

 /*   public void ChangeSkin(int ind)
    {
        Texture skin = SkinTextures[ind];
        UIPlayerSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        if (!SkinLockImages[ind].gameObject.activeInHierarchy)
        {
            UIPlayerMainSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        }        
        priceText.text = SkinPrices[ind].ToString();

    }

    public void ChangeBody(int ind)
    {
        Texture skin = BodyTextures[ind];
        UIPlayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        if(!BodyLockImages[ind].gameObject.activeInHierarchy)
        {
            UIPlayerMainBody.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        }
        priceText.text = BodyPrices[ind].ToString(); //Change price according to body selected
    }
    public void ChangeFootball(int ind)
    {
        Texture skin = SkinTextures[ind];
        UIPlayerSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        if (!SkinLockImages[ind].gameObject.activeInHierarchy)
        {
            UIPlayerMainSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        }
        priceText.text = SkinPrices[ind].ToString();

    }
 */
    public void initializeScreen()
    {
       /* Texture skin = SkinTextures[GenericVariables.getcurrSkin()];
        UIPlayerMainSkin.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        UIPlayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        skin = BodyTextures[GenericVariables.getcurrBody()];
        UIPlayerMainBody.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);
        UIPlayerBody.GetComponent<Renderer>().material.SetTexture("_MainTex", skin);*/

       /* for (int i = 0; i < BodyLockImages.Count; i++)
        {
            if(GenericVariables.checkBoughtBody(i))
            {
                BodyLockImages[i].gameObject.SetActive(false);
            }
            
        }
        for (int i = 0; i < SkinLockImages.Count; i++)
        {
            if (GenericVariables.checkBoughtSkin(i))
            {
                SkinLockImages[i].gameObject.SetActive(false);
            }
        }*/

/*        BodyLockImages[0].gameObject.SetActive(false);
        SkinLockImages[0].gameObject.SetActive(false);*/

        GenericVariables.LoadConsumables(); //Refreshes texts and consumable like enrgy, coin etc. values in player prefs

    }

    public void UpdateCoins(int amount)
    {
        coinText.text = amount.ToString();
        //coinText.text= GameConstants.getCollectedCoins().ToString();
    } 
    
    public void UpdateDiamonds(int amount)
    {
        diamondText.text = amount.ToString();
    }
    public void UpdateEnergy(int amount)
    {
        energyText.text = amount.ToString();
    }

    public void UpdateFire(int amount)
    {
        fireText.text = amount.ToString();
    }
    public void EnergyBought(int price)
    {
        if (GenericVariables.RemoveDiamonds(price))
        {
            GenericVariables.AddEnergy(10); //Add 10 energy    
        }
        else
        {
            //Not enough diamonds
            return;
        }
      //  GenericVariables.AddEnergy(10); //Add 10 energy        
    }

    public void FireBought(int price , int amount)
    {
        //if(GenericVariables.RemoveCoins(price))
        if (GenericVariables.RemoveDiamonds(price))
        {
            GenericVariables.AddFire(amount); //Add fire    
        }
        else
        {
            //Not enough diamonds
            return;
        }
      //  GenericVariables.AddFire(amount);      
        
    }

/*    public bool CustomizationBought(int ind , string type)
    {
        switch (type)
        {
            case "body":
                if (!GenericVariables.checkBoughtBody(ind)) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(BodyPrices[ind]))
                    {
                        GenericVariables.saveBoughtBody(ind);
                        GenericVariables.saveBody(ind);
                        BodyLockImages[ind].gameObject.SetActive(false);
                        ChangeBody(ind);                       
                        buyButton.SetActive(false);
                    }
                    
                }
                else
                {
                    //Not Enough Coins or already bought
                    return false;
                }
                break;
            case "skin":
                if (!GenericVariables.checkBoughtSkin(ind)) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(SkinPrices[ind]))
                    {
                        GenericVariables.saveBoughtSkin(ind);
                        GenericVariables.saveSkin(ind);
                        Debug.Log("Skin issss     " + ind);
                        SkinLockImages[ind].gameObject.SetActive(false);
                        buyButton.SetActive(false);
                        ChangeSkin(ind);
                    }
                }
                else
                {
                    //Not enough coins or already bought
                    return false;
                }
                break;
            case "football":
                if (!GenericVariables.checkBoughtSkin(ind)) // Place prices at relevant indices
                {
                    if (GenericVariables.RemoveCoins(SkinPrices[ind]))
                    {
                        GenericVariables.saveBoughtSkin(ind);
                        GenericVariables.saveSkin(ind);
                        Debug.Log("Skin issss     " + ind);
                        SkinLockImages[ind].gameObject.SetActive(false);
                        buyButton.SetActive(false);
                        ChangeSkin(ind);
                    }
                }
                else
                {
                    //Not enough coins or already bought
                    return false;
                }
                break;
            case "emote":
                break;
            default:
                break;

            }
        return true;
        //Unlock at specific index
    }*/
    public void setTime(string time)
    {
        timer.text = time;
        if (FortuneWheelManager.Instance!=null)
        {
         FortuneWheelManager.Instance.Timer.text = time;

        }
    }

    public void shakeClaimButton(bool active)
    {
        claimButton.gameObject.SetActive(active);
        timerPanel.gameObject.SetActive(false);
       /// turnButton.interactable = true;

     /*   if (active)
        {
            InvokeRepeating("shake", 0, 0.5f);
        }*/
    }
  
    public void shake()
    {       
        iTween.PunchScale(claimButton.gameObject, new Vector3(0.2f, 0.2f, 0.2f), 1.0f);
    }

    public void setEnergyBar(bool active)
    {
        EnergyBarPanel.gameObject.SetActive(active);
    }

    public void setFireAddPanel(bool active)
    {
        FireAddPanel.gameObject.SetActive(active);
    }

    public void setSettingsButton(bool active)
    {
        settingsButton.gameObject.SetActive(active);
    }

    public void setPrivacyPolicyButton(bool active)
    {
        privacyPolicyButton.SetActive(active);
    }

    public void moveConsumable()
    {
       
        InvokeRepeating("moveFunc", 0, 0.4f);
        Invoke("stopMove", 1.0f);

    }

    private void moveFunc()
    {
        GameObject g = null;
        if (coinsMove)
        {
            g = Instantiate(coinsPrefab, Pin, false);
            iTween.MoveTo(g, iTween.Hash("position", coinBar.GetComponent<RectTransform>().position, "time", moveTime, "easetype", easeType, "oncomplete", "shakeText", "oncompletetarget", this.gameObject, "ignoretimescale", true));            
        }
        else if(energyMove)
        {
            g = Instantiate(energyPrefab, Pin, false);
            iTween.MoveTo(g, iTween.Hash("position", EnergyBarPanel.GetComponent<RectTransform>().position, "time", moveTime,"easetype" , easeType, "oncomplete", "shakeText", "oncompletetarget", this.gameObject, "ignoretimescale", true));
        }
        else if(diamondMove)
        {
            g = Instantiate(diamondPrefab, Pin, false);
            iTween.MoveTo(g, iTween.Hash("position", diamondBarPanel .GetComponent<RectTransform>().position, "time", moveTime, "easetype", easeType, "oncomplete", "shakeText", "oncompletetarget", this.gameObject, "ignoretimescale", true));
        }
        else if (fireMove)
        {
            g = Instantiate(firePrefab, Pin, false);
            iTween.MoveTo(g, iTween.Hash("position", FireAddPanel.GetComponent<RectTransform>().position, "time", moveTime, "easetype", easeType, "oncomplete", "shakeText", "oncompletetarget", this.gameObject, "ignoretimescale",true));
        }
        //     iTween.ScaleTo(g, iTween.Hash("scale"  , g.transform.localScale * scaleFactor , "time" , moveTime / 2, "oncomplete", "ScaleBack","oncompletetarget", this.gameObject, "easetype", ScaleEaseType, "looptype" , loop));
        //Invoke("ScaleBack", 1.0f);
        if (tempList == null)
        {
            tempList = new List<GameObject>();
        }
        tempList.Add(g);

    }


    private void stopMove()
    {
        CancelInvoke("moveFunc");
        Invoke("DestroyObjects", destructionTime);
    }
    private void DestroyObjects()
    {
        for (int i = 0; i < tempList.Count; i++)
        {
            Destroy(tempList[i].gameObject);
        }
    }

    private void ScaleBack()
    {
        if (counter1 < tempList.Count)
        {
            iTween.ScaleTo(tempList[counter1], iTween.Hash("scale", Vector3.one, "time", moveTime / 2, "easetype", ScaleEaseType, "looptype", loop));
            counter1++;
        }
    }
    
    private void shakeText()
    {
        Destroy(tempList[counter]);
        counter++;       
        if (coinsMove)
        {
            iTween.PunchScale(coinText.gameObject, new Vector3(2.0f, 2.0f, 2.0f), 0.5f);
        }
        else if (diamondMove)
        {
            iTween.PunchScale(diamondText.gameObject, new Vector3(2.0f, 2.0f, 2.0f), 0.5f);
        }
        else if (energyMove)
        {
            iTween.PunchScale(energyText.gameObject, new Vector3(2.0f, 2.0f, 2.0f), 0.5f);
        }
        else if (fireMove)
        {
            iTween.PunchScale(fireText.gameObject, new Vector3(2.0f, 2.0f, 2.0f), 0.5f);
        }
    }

    public void moveObject(string obj ,GameObject parent)
    {
        Pin = parent.transform;
        coinsMove = false;
        diamondMove = false;
        energyMove = false;
        switch (obj)
        {
           
            case "coins":
                coinsMove = true;
                break;

            case "diamond":
                diamondMove = true;
                break;
            case "energy":
                energyMove = true;
                break;
            case "fire":
                fireMove = true;
                break;
            default:
                break;
        }

    }

    #region Pop Up Controller Functions
    public void CameraChange()
    {
        //         CameraRef cam2 = new CameraRef();
        //   cam = cam2.GetComponent<Camera>();
        //     MenuManager.Instance.mainCanvas.worldCamera = cam; //Change camer space to hide player
        // MenuManager.Instance.mainCanvas.worldCamera = mainCamera; //Change camer space to hide player
        //  mainCamera = Camera.main;
        // MenuManager.Instance.mainCanvas.worldCamera = mainCamera.GetComponent<Camera>(); //Change camer space to hide player
     
        mainCamera = CameraRef.instance.MainCamera;
     
        mainCanvas.worldCamera = mainCamera.GetComponent<Camera>(); //Change camer space to hide player
    }

    public void ShowTournamentsPopUp(int index) //Shows specific popUp
    {
        //currInd = index;
        if (popUps[index].GetComponent<Animator>())
        {
            popUps[index].GetComponent<Animator>().SetBool("BackPressed", false);

        }
        popUps[index].SetActive(true);
      
        //c.renderMode = RenderMode.ScreenSpaceOverlay;
        //UIPlayer.SetActive(false);
    }
  
    public void LoadLevel() //Loads current selected level on button click
    {

        //DisableAll();

     /*   if (popUps[6].activeSelf)             //commented because we have disabled popup menu //ahmad
        {
           // AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.LevelPopUp_Play);
            AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.LevelPopUp_Play, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);

            penaltyPopUpBackbutton();

        }
        else if(popUps[9].activeSelf)
        {
        //    AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.LevelPopUp_Play);
            AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.LevelPopUp_Play, AnalyticsManagerSoccerKicks.Event_State.Menu_Events);

            TournamentPopUpBackbutton();
            Debug.Log("if(popUps[9])");


        }
*/
        //c.renderMode = RenderMode.ScreenSpaceOverlay;
        
        StartCoroutine(delayLoadLevel());
        
    }
 
    public IEnumerator delayLoadLevel()
    {

        MenuManager.Instance.AnalyticsCallingLevelStart();
        yield return new WaitForSeconds(0f);

        MenuManager.Instance.setPanels(); //Turn all Panels off
                                          // MenuManager.Instance.setHUDActive(); //Turn HUD on
        MenuManager.Instance.Loading(true); //Start Loading Screen
    //    UIPlayer.SetActive(false);

        MenuManager.Instance.setMain(false); //Turn off Main Tabs
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(true); //Back Button must be active only in gameplay
                                                                            //  SceneManager.LoadScene(LevelButtonsController.selectedlevel); //Load Selected Level
        SceneManager.LoadScene("Gameplay"); //Load Selected Level


    }
    public void DisableAll()
    {
        StartCoroutine(DelayDisableAll());
    }

    IEnumerator DelayDisableAll()
    {
     //   Debug.Log("saasdadasdadasdasdadadad");
        yield return new WaitForSecondsRealtime(0.5f);
        if(Time.timeScale ==0)
        {
            Time.timeScale = 1;

        }
        DisablePopUps();// Disable all active pop ups
    }
    public void SettingsMainButton()
    {
        popUps[3].GetComponent<Animator>().SetBool("BackPressed", true);

        if (SceneManager.GetActiveScene().buildIndex > 0 && GameConstants.GetGameMode() != "Multiplayer1" && GameConstants.GetGameMode() != "Multiplayer2") //If we are at a level then go back to main
        {
            Debug.Log("SettingsMainButton");
            AdController.instance.HideBannerAd();

            //  MenuManager.Instance.DisablePopUps();
            //   DisableAll();
            if (LevelEndCelebrationManager.instance != null)
            {
                LevelEndCelebrationManager.instance.DisableLevelEnd();

            }
            settingBackbutton();
            //setHUD();
        //    UIPlayer.SetActive(true);
            //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            setMain(true);
            SettingsBackButton.gameObject.SetActive(false);
            GamePlayManager.instance.ResetScore();
            LevelMenuLoadBool = false;
            Time.timeScale = 1;
            Loading(true); //Start Loading Screen
            sceneChange();                                                  //changing scene to menus
                                                                            //  StartCoroutine(DelaySceneChange());

        }
        else if (SceneManager.GetActiveScene().buildIndex > 0 && (GameConstants.GetGameMode() == "Multiplayer1" || GameConstants.GetGameMode() == "Multiplayer2")) //If we are at a level then go back to main
        {
         //   AdController.instance.HideBannerAd();

            popUps[3].SetActive(false);
            UIMultiplayerPauseMenu.SetActive(true);
        }
        else
        {
            //If we are not at level then more games button pressed
        }
    }
    public void settingBackbutton()
    {
        Animator anim = popUps[3].GetComponent<Animator>();
        anim.SetBool("BackPressed", true);
       // AdController.instance.HideBannerAd();

        DisableAll();
    }
    public void penaltyPopUpBackbutton()
    {
        Animator anim = popUps[6].GetComponent<Animator>();
        anim.SetBool("BackPressed", true);

       /* if (GameConstants.getFirstStartUITutorial() == "Yes")
        {
         ButtonFiller.instance.TutorialHandTurnOnOFF(true);
        }
        else
        {
            ButtonFiller.instance.TutorialHandTurnOnOFF(false);
        }*/

        DisableAll();
    }
    public void TournamentPopUpBackbutton()
    {
        Animator anim = popUps[9].GetComponent<Animator>();
        anim.SetBool("BackPressed", true);
        DisableAll();
    }
    
    public void ShopBackPressed() //Back button in shop pressed
    {
        MenuManager.Instance.setShopPanel(false);
    }

 /*   public void LevelSuccessNextButton()                    //to go back to level menu from gameplay scene call this
    {
            // MenuManager.Instance.DisablePopUps();
            StartCoroutine(LevelSuccessClaimButtonClick());

    }

    public IEnumerator LevelSuccessClaimButtonClick()
    {
        ClaimButtonLevelSuccess.GetComponent<Button>().interactable = false;
        ClaimButton2XLevelSuccess.GetComponent<Button>().interactable = false;
        PopUpLevelDetailsSetter PopUpLevelDetails = new PopUpLevelDetailsSetter();
        if (GameConstants.getStartGamePlayTutorial() == "Yes" && GameConstants.GetLevelNumber() < 3)           //if its first time than take back to main menu to continue tutorial
        {
            Time.timeScale = 1;
            Debug.Log("getStartGamePlayTutorial is YES");
            MenuManager.Instance.moveObject("coins", ClaimButtonLevelSuccess);
            MenuManager.Instance.moveConsumable();
            yield return new WaitForSeconds(2.5f);
            GenericVariables.AddCoins(GameConstants.CoinsEarned);
            yield return new WaitForSeconds(0.3f);

            if (GameConstants.GetLevelNumber() == 1)
            {
                Debug.Log("GetLevelNumber is 1");
                GameConstants.SetLevelNumber(2);

                PopUpLevelDetails.LevelDetailsSetter();
                DisableAll();
                GamePlayManager.instance.ResetScore();
                ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                LoadLevel();

            }
            else if (GameConstants.GetLevelNumber() == 2)
            {
                Debug.Log("GetLevelNumber is 2");
                GameConstants.SetLevelNumber(3);
                PopUpLevelDetails.LevelDetailsSetter();

                DisableAll();
                GamePlayManager.instance.ResetScore();
                ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;
                ClaimButton2XLevelSuccess.GetComponent<Button>().interactable = true;

                UIAnimations.instance.MainButton(5);

                //  LoadLevel();

            }
            else if (GameConstants.GetLevelNumber() == 3)
            {
                ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;
                ClaimButton2XLevelSuccess.GetComponent<Button>().interactable = true;

          //      levelSuccessTakeToLevelMenu();

            }

            // levelSuccessTakeToMainMenu();
        }
        else if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            Time.timeScale = 1;                                               //changing scene to menus
                                                                              //  ButtonFiller button = new ButtonFiller();
                                                                              //  button.FillButtons(GenericVariables.getTotalLevelsPassed());
            MenuManager.Instance.moveObject("coins", ClaimButtonLevelSuccess);
            MenuManager.Instance.moveConsumable();
            yield return new WaitForSeconds(2.5f);
            GenericVariables.AddCoins(GameConstants.CoinsEarned);
            yield return new WaitForSeconds(0.3f);

            *//*Debug.Log("GetLevelNumber = " + GameConstants.GetLevelNumber());
            Debug.Log("getTotalLevelsPassed = " + GenericVariables.getTotalLevelsPassed());
            Debug.Log("BonusLevelList.lengh = " + GameConstants.BonusLevelList.Length);*//*

            if (GameConstants.GetLevelNumber()  == GenericVariables.getTotalLevelsPassed())      //check if level is being played first time //we dont want repeated bonus levels on same level
            {
                if(GamePlayManager.instance.BonusLevel)         //if its a bonus level
                { 
                   GameConstants.LevelNumberPlayed = GameConstants.GetLevelNumber();
               *//* for (int i = 0; i < GameConstants.BonusLevelList.Length; i++)       //check for bonus level
                {
                    Debug.Log("BonusLevelList = " + i);
                    if (GameConstants.GetLevelNumber() == GameConstants.BonusLevelList[i])
                    {*//*

                     //   BonusLevel = true;

                        *//* if (GameConstants.GetLevelNumber() == 3 || GameConstants.GetLevelNumber() == 7 || GameConstants.GetLevelNumber() == 10)
                         {*/

                        /*ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;
                        ClaimButton2XLevelSuccess.GetComponent<Button>().interactable = true;*//*

                        GameConstants.SetLevelNumber(0);            //BONUS LEVEL

                        PopUpLevelDetails.LevelDetailsSetter();
                       GamePlayManager.instance.LevelDetailsSetter();
                        DisableAll();
                        GamePlayManager.instance.ResetScore();
                  //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                        LoadLevel();
                        GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                        GamePlayManager.instance.UnlockNextLevel();
                   
                }
                //if(!BonusLevel)         //
                
                else
                {
                    // levelSuccessTakeToLevelMenu();
                    if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
                    {
                        GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed + 1);
                        PopUpLevelDetails.LevelDetailsSetter();
                        GamePlayManager.instance.LevelDetailsSetter();
                        DisableAll();
                        GamePlayManager.instance.ResetScore();
                        //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                        LoadLevel();
                     //   GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                    }
                    else
                    {
                         levelSuccessTakeToLevelMenu();

                    }
                }
            }
            else
            {
                //   levelSuccessTakeToLevelMenu();

                if (GameConstants.GetLevelNumber() != GameConstants.getTotalLevels())
                {
                    GameConstants.SetLevelNumber(GameConstants.GetLevelNumber() + 1);
                    PopUpLevelDetails.LevelDetailsSetter();
                    GamePlayManager.instance.LevelDetailsSetter();
                    DisableAll();
                    GamePlayManager.instance.ResetScore();
                    //      ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;

                    LoadLevel();
                 //   GameConstants.SetLevelNumber(GameConstants.LevelNumberPlayed);
                }
                else
                {
                    levelSuccessTakeToLevelMenu();

                }
            }

        }
        else
        {
            UIPlayer.SetActive(false);
            //If we are not at level then more games button pressed
        }
        Time.timeScale = 1f;
    }
*/
    public void levelSuccessTakeToMainMenu()
    {
        LevelEndCelebrationManager.instance.DisableLevelEnd();
    //    setHUD();
     //   UIPlayer.SetActive(true);
        //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
        setMain(true);
        SettingsBackButton.gameObject.SetActive(false);
        GamePlayManager.instance.ResetScore();
        LevelMenuLoadBool = false;
        Loading(true); //Start Loading Screen
        DisableAll();
        UITutorialManager.instance.EnableUpperBottomPanelBlackShader();
        UITutorialManager.instance.FortuneWheelClaimTutorial();
        ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;
        ClaimButton2XLevelSuccess.GetComponent<Button>().interactable = true;
        sceneChange();

    }
    public void levelSuccessTakeToLevelMenu()
    {
        LevelEndCelebrationManager.instance.DisableLevelEnd();

        DisableAll();
    //    setHUD();
    //    UIPlayer.SetActive(true);
        setPanels();
        setLevelMenu(true);                         //taking to level menu
        SettingsBackButton.gameObject.SetActive(false);

        LevelMenuLoadBool = true;                                           //if level is being loaded
        GamePlayManager.instance.ResetScore();           //reset score //important
        Loading(true); //Start Loading Screen
        ClaimButtonLevelSuccess.GetComponent<Button>().interactable = true;
        ClaimButton2XLevelSuccess.GetComponent<Button>().interactable = true;
        sceneChange();
    }
    public void SetLevelSuccessNumber()
    {
        levelSuccessNumber.text = GameConstants.GetLevelNumber().ToString();
    } 
    public void SetLevelFailNumber()
    {
        levelFailNumber.text = GameConstants.GetLevelNumber().ToString();
    } 
        public void sceneChange()
    {  
        SceneManager.LoadScene("Menus");
     //   SceneManager.LoadScene(1); //Load scene for Main Menu
        //CameraChange();
    }

    IEnumerator DelaySceneChange()
    {
        yield return new WaitForSeconds(0.5f);
        //SceneManager.LoadScene(1); //Load scene for Main Menu
        SceneManager.LoadScene("Menus");

    }
    public void NextButtonTournamentTreePanel()
    {
        //  this.gameObject.SetActive(false);
        if (popUps[10] != null)
        {
            if (popUps[10].GetComponent<Animator>())
            {
                Debug.Log("if (popUps[10].GetComponent<Animator>())");
                popUps[10].GetComponent<Animator>().SetBool("BackPressed", true);

            }
            //popUps[10].SetActive(false);
          StartCoroutine(delayTournamentPopUpDisable(10));

        }
        if (popUps[11] != null)
        {
            if (popUps[11].GetComponent<Animator>())
            {
                popUps[11].GetComponent<Animator>().SetBool("BackPressed", true);

            }
            // popUps[11].SetActive(false);
            StartCoroutine(delayTournamentPopUpDisable(11));


        }
        if (popUps[12] != null)
        {
            if (popUps[12].GetComponent<Animator>())
            {
                popUps[12].GetComponent<Animator>().SetBool("BackPressed", true);

            }
        //    popUps[12].SetActive(false);
            StartCoroutine(delayTournamentPopUpDisable(12));


        }
        LevelEndCelebrationManager.instance.Screen.SetActive(false);

    }

    IEnumerator delayTournamentPopUpDisable(int index)
    {
        yield return new WaitForSeconds(0.1f);
        popUps[index].SetActive(false);
    }

    public void NextClickTournamentLevelPassScreen()
    {
        //DisablePopUps();
        if (popUps[13].GetComponent<Animator>())
        {
            popUps[13].GetComponent<Animator>().SetBool("BackPressed", true);

        }
       // DisableAll();
        popUps[13].SetActive(false);

        //  LevelEndCelebrationManager.instance.Screen.SetActive(false);

        TournamentHandler.instance.RoundEnd();
    }
    public void LevelFailedRestartGame()            //restart game on level failed
    {
        /* if (GenericVariables.GetEnergy() > 1)
         {*/
        // MenuManager.Instance.DisablePopUps();
        
        GameConstants.NumberOfRestarts++;
        
        if (GenericVariables.RemoveEnergy(1))
        {
            GamePlayManager.instance.LevelRestart = true;
            LevelEndCelebrationManager.instance.DisableLevelEnd();
            DisableAll();
            Time.timeScale = 1;
            GamePlayManager.instance.LevelEndBool = false;
            if(GameConstants.GetLevelNumber() > 2)              //enabling in gameplay buttons
            {
                HudManager.instance.EnableDisableCustomizeButton(true);
                HudManager.instance.EnableDisableMultiplayerButton(true);
            }

            if (GameConstants.GetMode() == 44)         //if bonus level mode
            {
                GameConstants.SetLevelNumber(0);
            }

            GamePlayManager.instance.LevelDetailsSetter();
            GamePlayManager.instance.ResetScore();

            if(GameConstants.GetMode() == 8)
            {
                HudManager.instance.EnableLevelDetailOnscreenTexts();   //show HIT THE POLE TEXT
            }

            if (GameConstants.GetMode() == 10)         //if tournament level mode
            {
                TournamentHandler.instance.OnTournamentStart();
                TournamentHandler.instance.DrawHappened();
                HudManager.instance.KeysPanel.SetActive(true);

                HudManager.instance.TournamentHudEnabler();
            }
            else
            {
                HudManager.instance.updateScore();
                HudManager.instance.StarsHudDisplayReset();
                HudManager.instance.TargetScoringPanel.SetActive(true);
                HudManager.instance.KeysPanel.SetActive(true);
                HudManager.instance.UpdateKickRemaining("Increment");
            }
            /*
                        if (GameConstants.getStartGamePlayTutorial() == "Yes" && GameConstants.GetLevelNumber() == 1)
                        {
                            HudManager.instance.FireballObject.SetActive(false);
                        }
                        else
                        {
                            HudManager.instance.FireballObject.SetActive(true);
                        }*/

            HudManager.instance.FireButtonEnableDisableInGamePlay();  //according to remaining count of fireballs


            HudManager.instance.DistanceText.gameObject.SetActive(true);
            Loading(true); //Start Loading Screen
            Shoot.share.reset();
            CameraManager.share.reset();
            GoalKeeper.share.reset();

            if (AdConstants.isTestingLicenseAvailable())    //testing
            {
                HudManager.instance.WinLevelButton.SetActive(true);
                HudManager.instance.LoseLevelButton.SetActive(true);
                HudManager.TestingWinButtonPressedBool = false;
                HudManager.TestingLoseButtonPressedBool = false;
            }
            else
            {
                HudManager.instance.WinLevelButton.SetActive(false);
                HudManager.instance.LoseLevelButton.SetActive(false);

                HudManager.TestingWinButtonPressedBool = false;
                HudManager.TestingLoseButtonPressedBool = false;
            }

            AnalyticsCallingLevelRestart();
        }
        if(GenericVariables.GetEnergy()==0 && !RewardedTimeManager.instance.ReloadEnengyTimerInProgress)
        {
            Debug.Log("not enough energy to restart");
            /*DateTime SavedTime = DateTimeExtension.GetSavedDate("SavedDateReloadEnergy");
            TimeSpan RemaningTime = SavedTime.Subtract(DateTime.Now);*/
            
            RewardedTimeManager.instance.EnergyConsumedReloadEnergy();

            
        }
         //   GenericVariables.RemoveEnergy(2);
        //}
    }

    public void LevelFailedHomeButton()
    {
        //popUps[3].GetComponent<Animator>().SetBool("BackPressed", true);


        //  MenuManager.Instance.DisablePopUps();
         DisableAll();
        LevelEndCelebrationManager.instance.DisableLevelEnd();

        //   setHUD();
  //      UIPlayer.SetActive(true);
        //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
          setMain(true);
        //  SettingsBackButton.gameObject.SetActive(false);
        //   GameConstants.score = 0;
           GamePlayManager.instance.ResetScore();
         LevelMenuLoadBool = false;
            Time.timeScale = 1;

            Loading(true); //Start Loading Screen
         sceneChange();                                                  //changing scene to menus
        //   StartCoroutine(DelaySceneChange());
       // StopAllCoroutines();
        //SceneManager.LoadScene("Menus");

        
    }



    public void onClickSoundOnOffButton()
    {
    //    Debug.Log("Sound on off");
        if (SoundBool)
        {
         //   Debug.Log("Sound on off  SoundBool== " + SoundBool);

            //  MusicBool = true;
            //  MusicOnOffButton();
            SoundBool = false;
            SoundOnImage.SetActive(false);
            SoundOffImage.SetActive(true);

        }
        else
        {
          //  Debug.Log("Sound on off  SoundBool== " + SoundBool);

            //    MusicBool = false;
            //   MusicOnOffButton();
            SoundBool = true;
            SoundOnImage.SetActive(true);
            SoundOffImage.SetActive(false);

            // SoundManagerNew.Instance.playMusic("GamePlayBG");
        }
        SoundManagerNew.Instance.OnOffSound(SoundBool);
        
    }
    public void MusicOnOffButton()
    {
        if (MusicBool )
        {
            MusicOnImage.SetActive(false);
            MusicOffImage.SetActive(true);
            MusicBool = false;
        }
        else
        {
            MusicOnImage.SetActive(true);
            MusicOffImage.SetActive(false);

            MusicBool = true;
       //     SoundManagerNew.Instance.playMusic("MenuBG");
        }
        SoundManagerNew.Instance.OnOffMusic(MusicBool);
        
    }

    public void LanguageButtonClick()
    {
        LanguagePanel.SetActive(true);
    }
    #endregion
    #region LevelEndWorking

    public IEnumerator ActiveLevelSuccessPanel()
    {
        //   LevelSuccessPanel.SetActive(true);

        // MenuManager.Instance.ShowPopUp(4);
        Shoot.share._enableTouch = false;

        MenuManager.Instance.headers[0].SetActive(false);               //disabling upper tab 



        if (GameConstants.GetLevelNumber() < 3 || CustomizationScript.instance.NewCelebrationBought)
        {
            CustomizationScript.instance.NewCelebrationBought = false;
            yield return new WaitForSeconds(2);
            MenuManager.Instance.ShowPopUp(4);
           // LevelCompleteMenu.Instance.CoinsText.text = Coins.ToString();

        }
        else
        {
            MenuManager.Instance.ShowPopUp(4);
           // LevelCompleteMenu.Instance.CoinsText.text = Coins.ToString();

        }

        GamePlayManager.instance.BonusLevel = false;
        for (int i = 0; i < GameConstants.BonusLevelList.Length; i++)           //Bonus Level Check
        {
            if (GameConstants.GetLevelNumber() == GameConstants.BonusLevelList[i])
            {
                GamePlayManager.instance.BonusLevel = true;
            }
        }
        if (!GamePlayManager.instance.BonusLevel)             //if there is no bonus level on this level than unlock next level
        {
            GamePlayManager.instance.UnlockNextLevel();
        }

        if (GamePlayManager.instance.timerbool)
        {
            GamePlayManager.instance.timerbool = false;
        }

        /*   GameObject CoinsText= GameObject.FindGameObjectWithTag("CoinsTextSucessPanel");
           CoinsText.GetComponent<Text>().text = Coins.ToString();*/
        //    MenuManager.Instance.SetLevelSuccessNumber();               //display which level was successful
        //StartCoroutine(ShowRateUs());


    }
    public void ActiveLevelFailPanel()
    {
        //LevelFailPanel.SetActive(true);
        MenuManager.Instance.headers[0].SetActive(false);               //disabling upper tab 

        MenuManager.Instance.ShowPopUp(5);
        //     MenuManager.Instance.SetLevelFailNumber();               //display which level was successful
        Shoot.share._enableTouch = false;
        // StartCoroutine(ShowRateUs());

    }
   

    #endregion
    #region ShopMenuRelated

    public void EnableShopMenu()                   //when loading mission menu coming back from gameplay scene       //ahmad added
    {
       // Debug.Log("enable mission pop up");
        headers[0].gameObject.SetActive(true); //Sets Main Tab active or inactive
        headers[1].gameObject.SetActive(true); //Sets Main Tab active or inactive
        mainBack.SetActive(true);
        StartCoroutine(delayShopMenu());
        //  MissionMenuLoadBool = false;

    }
    IEnumerator delayShopMenu()
    {
        yield return new WaitForSeconds(0.6f);
        UIAnimations.instance.MainButton(2);

     //   MainPanels[2].gameObject.SetActive(true);

    }
    #endregion

    #region MultiplayerRelated
    public void OnClickMultiplayerButton()
    {
        MultiplayerModeMenu.SetActive(true);
    
    } 
    
    #endregion

    #region MissionsRelated
    public void MissionButtonClicked()
    {
        popUps[1].SetActive(true);
    }

    public void EnableMissionPopUpMenu()                   //when loading mission menu coming back from gameplay scene       //ahmad added
    {
   //     Debug.Log("enable mission pop up");
        headers[1].gameObject.SetActive(true); //Sets Main Tab active or inactive
        mainBack.SetActive(true);
        StartCoroutine(delayMissionPopUp());
      //  MissionMenuLoadBool = false;

    }
    IEnumerator delayMissionPopUp()
    {
        yield return new WaitForSeconds(0.4f);
        popUps[1].SetActive(true);

    }
    public void MissionFailedRestartGame()            //restart game on level failed
    {
        /* if (GenericVariables.GetEnergy() > 1)
         {*/
        // MenuManager.Instance.DisablePopUps();
        if (GenericVariables.RemoveEnergy(1))
        {
            AdController.instance.ShowBannerAd();

            AnalyticsCallingLevelRestart();

            LevelEndCelebrationManager.instance.DisableLevelEnd();

            GamePlayManager.instance.MissionEndBool = false;
            DisableAll();
            MissionHudReseter();
            Shoot.share.reset();
            CameraManager.share.reset();
            GoalKeeper.share.reset();
            Loading(true);
        }
        if (GenericVariables.GetEnergy() == 0 && !RewardedTimeManager.instance.ReloadEnengyTimerInProgress)
        {
            Debug.Log("not enough energy to restart");
            /*DateTime SavedTime = DateTimeExtension.GetSavedDate("SavedDateReloadEnergy");
            TimeSpan RemaningTime = SavedTime.Subtract(DateTime.Now);*/


            RewardedTimeManager.instance.EnergyConsumedReloadEnergy();


        }
        //   GenericVariables.RemoveEnergy(2);
        //  }
    }

    public void MissionHudReseter()
    {
        HudManager.instance.TargetScoringPanel.SetActive(true);
        HudManager.instance.KeysPanel.SetActive(false);

        HudManager.instance.DistanceText.gameObject.SetActive(true);

        GamePlayManager.instance.ResetScore();
        GamePlayManager.instance.MissionDetailsSetter();
        if (GameConstants.GetMissionMode() == 1 || GameConstants.GetMissionMode() == 3 || GameConstants.GetMissionMode() == 5)      //if mission are of goal scoring
        {
            HudManager.instance.UpdateMissionTargetGoals();
            HudManager.instance.UpdateMissionGoalScored();
        }
        else
        {
            HudManager.instance.updateScore();
            HudManager.instance.UpdateMissionTargetScore();
        }
        HudManager.instance.UpdateKickRemaining("Increment");

    }
    public void ActiveMissionSuccessPanel(int Coins)
    {
        //   LevelSuccessPanel.SetActive(true);
        MenuManager.Instance.headers[0].SetActive(false);               //disabling upper tab 

        MenuManager.Instance.ShowPopUp(16);
        Shoot.share._enableTouch = false;
        /* GameObject CoinsText = GameObject.FindGameObjectWithTag("CoinsTextMissionSuccessPanel");

         CoinsText.GetComponent<Text>().text = Coins.ToString();*/
        MissionSuccessMenu.instance.CoinsText.text = Coins.ToString();
    }
    public void ActiveMissionFailPanel()
    {
        //LevelFailPanel.SetActive(true);
        MenuManager.Instance.headers[0].SetActive(false);               //disabling upper tab 

        MenuManager.Instance.ShowPopUp(17);
        Shoot.share._enableTouch = false;

    }

    #endregion

    #region TrophyMenuRelated
    public void EnableTrophyMenu()
    {
        TrophyMenu.SetActive(true);
    }

    #endregion

    #region RewardShot
    public void RewardShotHudReseter()
    {
        HudManager.instance.TargetScoringPanel.SetActive(true);
        HudManager.instance.KeysPanel.SetActive(false);

        HudManager.instance.DistanceText.gameObject.SetActive(true);

        GamePlayManager.instance.ResetScore();
        GamePlayManager.instance.RewardShotDetailsSetter();
        HudManager.instance.UpdateMissionTargetGoals();
        HudManager.instance.UpdateMissionGoalScored();
        HudManager.instance.UpdateKickRemaining("Increment");

    }
    public void RewardShot2HudReseter()
    {
        HudManager.instance.TargetScoringPanel.SetActive(true);
        HudManager.instance.KeysPanel.SetActive(false);

        HudManager.instance.DistanceText.gameObject.SetActive(true);

        GamePlayManager.instance.ResetScore();
        GamePlayManager.instance.RewardShot2DetailsSetter();
        HudManager.instance.UpdateMissionTargetGoals();
        HudManager.instance.UpdateMissionGoalScored();
        HudManager.instance.UpdateKickRemaining("Increment");

    }

    public void ActiveRewardKickCompletePanel(int Coins)
    {
        //   LevelSuccessPanel.SetActive(true);
        MenuManager.Instance.headers[0].SetActive(false);               //disabling upper tab 

        //  ShowPopUp(19);
        MenuManager.Instance.RewardKick_End_Menu.SetActive(true);
        Shoot.share._enableTouch = false;
  
        RewardShotEndMenu.instance.CoinsText.text = Coins.ToString();
    }

    public void RewardKickClaimButtonEnabler(bool active)
    {
        RewardKickClaimButton.gameObject.SetActive(active);
        if(active)
        {
            RewardKickTimer.gameObject.SetActive(false);
            if(MainPanels[0].gameObject.activeSelf)
            {

                RewardKickPopUpShow();
            }
        }
        else
        {

            RewardKickTimer.gameObject.SetActive(true);
        }

        /*   if (active)
           {
               InvokeRepeating("shake", 0, 0.5f);
           }*/
    }
    public void RewardKicksetTime(string time)
    {
       RewardKickTimerText.text = time;
        
    }
    public void RewardKickPopUpShow()
    {
        RewardKick_Start_Menu.SetActive(true);

    }
    #endregion
    #region UpperPanel
    public void EnergyReloadSetTime(string time)
    {
        EnergyReloadTimer.text = time;

    }
    public void EnergyReloadTimerEnabler(bool active)
    {
        EnergyReloadTimer.gameObject.SetActive(active);
    }
    #endregion
    #region AnalyticsCallingWorking

    public void AnalyticsCallingLevelStart()
    {
        if (GameConstants.GetGameMode() == "Classic")
        {
      //      Debug.Log("mode is Classic AnalyticsCallingLevelStart");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, GameConstants.GetLevelNumber(), AnalyticsManagerSoccerKicks.LevelStates.started);
            //Debug.Log(" Analytics GetLevelNumber +++ " + GameConstants.GetLevelNumber());
        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
       //     Debug.Log("mode is Missions AnalyticsCallingLevelStart");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(2, GameConstants.GetMissionNumber(), AnalyticsManagerSoccerKicks.LevelStates.started);
        }
        else if (GameConstants.GetGameMode() == "RewardShot2")
        {
                 Debug.Log("mode is RewardShot AnalyticsCallingLevelStart");
            GameConstants.SetRewardKickCountNumber(GameConstants.GetRewardKickCountNumber() + 1);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(3, GameConstants.GetRewardKickCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.started);
        }
        else if (GameConstants.GetGameMode() == "Multiplayer1")
        {
                 Debug.Log("mode is Mulitplayer AnalyticsCallingLevelStart");
            GameConstants.SetMultplayerMode1LevelCountNumber(GameConstants.GetMultplayerMode1LevelCountNumber() + 1);
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(1, GameConstants.GetMultplayerMode1LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.started);
        }
        else if (GameConstants.GetGameMode() == "Multiplayer2")
        {
                 Debug.Log("mode is Mulitplayer AnalyticsCallingLevelStart");
            GameConstants.SetMultplayerMode2LevelCountNumber(GameConstants.GetMultplayerMode2LevelCountNumber() + 1);
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(2, GameConstants.GetMultplayerMode2LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.started);
        }
    }
    public void AnalyticsCallingEndWin()
    {
        if (GameConstants.GetGameMode() == "Classic")
        {
         //   Debug.Log("mode is Classic AnalyticsCallingEndWin");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, GameConstants.GetLevelNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, GameConstants.GetLevelNumber(), AnalyticsManagerSoccerKicks.LevelStates.win);
        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
           // Debug.Log("mode is Missions AnalyticsCallingEndWin");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(2, GameConstants.GetMissionNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(2, GameConstants.GetMissionNumber(), AnalyticsManagerSoccerKicks.LevelStates.win);
        }
        else if (GameConstants.GetGameMode() == "RewardShot2")
        {
             Debug.Log("mode is RewardShot AnalyticsCallingEndWin");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(3, GameConstants.GetRewardKickCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(3, GameConstants.GetRewardKickCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.win);
        }
        else if (GameConstants.GetGameMode() == "Multiplayer1")
        {
             Debug.Log("mode is Mulitplayer AnalyticsCallingEndWin");
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(1, GameConstants.GetMultplayerMode1LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(1, GameConstants.GetMultplayerMode1LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.win);
        }   
        else if (GameConstants.GetGameMode() == "Multiplayer2")
        {
             Debug.Log("mode is Mulitplayer AnalyticsCallingEndWin");
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(2, GameConstants.GetMultplayerMode2LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(2, GameConstants.GetMultplayerMode2LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.win);
        } 

    }
    public void AnalyticsCallingLevelRestart()
    {
        if (GameConstants.GetGameMode() == "Classic")
        {
            //      Debug.Log("mode is Classic AnalyticsCallingLevelStart");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, GameConstants.GetLevelNumber(), AnalyticsManagerSoccerKicks.LevelStates.restarted);
        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
            //     Debug.Log("mode is Missions AnalyticsCallingLevelStart");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(2, GameConstants.GetMissionNumber(), AnalyticsManagerSoccerKicks.LevelStates.restarted);
        }
    }
    public void AnalyticsCallingEndLose()
    {
        if (GameConstants.GetGameMode() == "Classic")
        {
        //    Debug.Log("mode is Classic AnalyticsCallingEndLose");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, GameConstants.GetLevelNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, GameConstants.GetLevelNumber(), AnalyticsManagerSoccerKicks.LevelStates.lost);
        }
        else if (GameConstants.GetGameMode() == "Missions")
        {
         //   Debug.Log("mode is Missions AnalyticsCallingEndLose");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(2, GameConstants.GetMissionNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(2, GameConstants.GetMissionNumber(), AnalyticsManagerSoccerKicks.LevelStates.lost);
        }
        else if (GameConstants.GetGameMode() == "RewardShot2")
        {
            Debug.Log("mode is RewardShot AnalyticsCallingEndLose");
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(3, GameConstants.GetRewardKickCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(3, GameConstants.GetRewardKickCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.lost);
        }
        else if (GameConstants.GetGameMode() == "Multiplayer1")
        {
            Debug.Log("mode is Mulitplayer AnalyticsCallingEndLose");
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(1, GameConstants.GetMultplayerMode1LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(1, GameConstants.GetMultplayerMode1LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.lost);
        }
        else if (GameConstants.GetGameMode() == "Multiplayer2")
        {
            Debug.Log("mode is Mulitplayer AnalyticsCallingEndLose");
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(2, GameConstants.GetMultplayerMode2LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.ended);
            AnalyticsManagerSoccerKicks.Instance.MultiplayerAnalytics(2, GameConstants.GetMultplayerMode2LevelCountNumber(), AnalyticsManagerSoccerKicks.LevelStates.lost);
        } 
    }

    public void AnalyticsCallingBeforeLevelStart(int MenuState)
    {
        AnalyticsManagerSoccerKicks.Instance.LevelsAnalytics(1, MenuState, AnalyticsManagerSoccerKicks.LevelStates.started);

    }
    #endregion

    #region Testing_Buttons
    public void OnClickCoinTestButton()
    {
        GenericVariables.AddCoins(20000);
    }
    public void OnClickDiamondTestButton()
    {
        GenericVariables.AddDiamonds(150);
    } 
    public void OnClickFireTestButton()
    {
        GenericVariables.AddFire(20);
    } public void OnClickEnergyTestButton()
    {
        GenericVariables.AddEnergy(10);
    }
    #endregion
}
