﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PopUpController : MonoBehaviour //Code for basic pop Up functionalities
{
    // Start is called before the first frame update
    public static PopUpController sInstance = null;
    public List<GameObject> popUps;
    public Canvas c;
    public GameObject overlay;
    public static bool LevelMenuLoadBool;
    private static int currInd;

    public Text PenaltyLevel; //Level to be shown on penalty screen

    public static PopUpController instance
    {
        get
        {
            return sInstance;
        }
    }

    private void Awake()
    {

        if (sInstance == null)
        {
            sInstance = this;
        }
        else
        {
         //   Destroy(gameObject);
        }
    }
    void Start()
    {
        overlay.SetActive(false);
        currInd = 0;
     //   CameraChange();
    }
    
    // Update is called once per frame
    public void CameraChange()
    {
      //  MenuManager.Instance.mainCanvas.worldCamera = Camera.main; //Change camer space to hide player
    }
    public void ShowPopUp(int index) //Shows specific popUp
    {
        overlay.SetActive(true);
        currInd = index;
        popUps[index].SetActive(true);
        c.renderMode = RenderMode.ScreenSpaceOverlay;
    }
    public void ShowTournamentsPopUp(int index) //Shows specific popUp
    {
        currInd = index;
        popUps[index].SetActive(true);
        c.renderMode = RenderMode.ScreenSpaceOverlay;
    }
    public void ShowSettings()
    {
        DisableAll();
        c.renderMode = RenderMode.ScreenSpaceOverlay;
        MenuManager.Instance.showSettings(); //Show settings PopUp
        if (Shoot.share!=null)
        {
            Shoot.share._enableTouch = false;
        }
    }

    public void LoadLevel() //Loads current selected level on button click
    {
        DisableAll();
        c.renderMode = RenderMode.ScreenSpaceOverlay;
        MenuManager.Instance.setPanels(); //Turn all Panels off
       // MenuManager.Instance.setHUDActive(); //Turn HUD on
        MenuManager.Instance.Loading(true); //Start Loading Screen
        MenuManager.Instance.setMain(false); //Turn off Main Tabs
        MenuManager.Instance.SettingsBackButton.gameObject.SetActive(true); //Back Button must be active only in gameplay
      //  SceneManager.LoadScene(LevelButtonsController.selectedlevel); //Load Selected Level
        SceneManager.LoadScene("Gameplay"); //Load Selected Level
    }
    public void DisableAll()
    {
        MenuManager.Instance.DisablePopUps();// Disable all active pop ups
    }

    public void SettingsMainButton()
    {
        if(SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            MenuManager.Instance.DisablePopUps();
            //       MenuManager.Instance.setHUD();
            MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            MenuManager.Instance.setMain(true);
            MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
            GameConstants.score = 0;
            LevelMenuLoadBool = false;
            SceneManager.LoadScene(0); //Load scene for Main Menu
        }
        else
        {
             //If we are not at level then more games button pressed
        }
    }

    public void ShopBackPressed() //Back button in shop pressed
    {
        MenuManager.Instance.setShopPanel(false);
    }

    public void LevelSuccessNextButton()                    //to go back to level menu from gameplay scene call this
    {
        if (SceneManager.GetActiveScene().buildIndex > 0) //If we are at a level then go back to main
        {
            MenuManager.Instance.DisablePopUps();
        //    MenuManager.Instance.setHUD();
            //MenuManager.Instance.mainCanvas.renderMode = RenderMode.ScreenSpaceCamera;
            //UIPlayer.SetActive(true);
            MenuManager.Instance.setPanels();
          // Gilani  MenuManager.Instance.setLevelMenu(true);
            MenuManager.Instance.SettingsBackButton.gameObject.SetActive(false);
            SceneManager.LoadScene(0); //Load scene for Main Menu
            LevelMenuLoadBool = true;                                           //if level is being loaded
            GamePlayManager.instance.ResetScore();           //reset score //important
        //  ButtonFiller button = new ButtonFiller();
        //  button.FillButtons(GenericVariables.getTotalLevelsPassed());
        }
        else
        {
            //If we are not at level then more games button pressed
        }
    }

    public void NextButtonTournamentTreePanel()
    {
        //  this.gameObject.SetActive(false);
        if (popUps[5] != null)              
        {
            popUps[5].SetActive(false);

        }
        if (popUps[6] != null)
        {
            popUps[6].SetActive(false);

        }
        if (popUps[7] != null)
        {
            popUps[7].SetActive(false);

        }

    }

    public void NextClickTournamentLevelPassScreen()
    {
        MenuManager.Instance.DisablePopUps();
        TournamentHandler.instance.RoundEnd();
    }
    public void LevelFailedRestartGame()            //restart game on level failed
    {
        MenuManager.Instance.DisablePopUps();
        GamePlayManager.instance.LevelDetailsSetter();
        HudManager.instance.UpdateKickRemaining("Increment");
        GamePlayManager.instance.ResetScore();
        HudManager.instance.updateScore();
        HudManager.instance.StarsHudDisplayReset();
        Shoot.share.reset();
        CameraManager.share.reset();
        GoalKeeper.share.reset();
    }
}
