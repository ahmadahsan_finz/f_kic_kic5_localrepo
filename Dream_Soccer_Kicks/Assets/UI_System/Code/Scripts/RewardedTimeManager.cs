﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class RewardedTimeManager : MonoBehaviour //Script for rewarded timer
{
    //public Text txt_timer; 
    public static RewardedTimeManager instance;
    public bool ReloadEnengyTimerInProgress;
    public static DateTime SpinWheel_CurrentTime;
    public static DateTime SpinWheel_SavedTime;
    public static TimeSpan SpinWheel_RemaningTime;

    public static DateTime RewardKick_CurrentTime;
    public static DateTime RewardKick_SavedTime;
    public static TimeSpan RewardKick_RemaningTime;

    public static DateTime EnergyDrink_CurrentTime;
    public static DateTime EnergyDrink_SavedTime;
    public static TimeSpan EnergyDrink_RemaningTime;

    private void Awake()
    {
        if(instance==null)
        {
            instance = this;
        }
        else
        {
         //   Destroy(gameObject);
        }
    }
    void Start() 
    {

        if (GameConstants.getStartGamePlayTutorial() != "Yes")
        {
            StartCoroutine(CheckTimer());
            StartCoroutine(CheckTimerRewardKick());
        }
        StartCoroutine(CheckTimerEnergyDrink());
        /*if (GenericVariables.GetEnergy()==0 && !ReloadEnengyTimerInProgress)
        {
            EnergyConsumedReloadEnergy();
        }*/
    }



    /// <summary>
    ///  SPINWHEEL TIMER WORKING
    /// </summary>
    public void GetReward()
    {
       

        int minutes = 1440;
        SpinWheel_CurrentTime = DateTime.Now.AddMinutes(minutes); //How many minutes to add next time 1440 means 24 hours
        SpinWheel_CurrentTime.SaveDate("SavedDate");

        StopCoroutine(CheckTimer());
        StartCoroutine(CheckTimer());

        string temp = PluginCallBackManager.instance.NotificationTexts[5];
        //Daily Notifications
        GleyNotifications.Initialize();
        GleyNotifications.SendNotification("Soccer Kicks Strike", temp, new System.TimeSpan(0, minutes, 0), null, null, "Opened from Gley Notification");
    }
    public IEnumerator CheckTimer()
    {
        SpinWheel_SavedTime = DateTimeExtension.GetSavedDate("SavedDate");
        SpinWheel_RemaningTime = SpinWheel_SavedTime.Subtract(DateTime.Now);
        if (SpinWheel_RemaningTime.TotalMinutes > 0)
        {
            MenuManager.Instance.shakeClaimButton(false); // set button active false
            MenuManager.Instance.timerPanel.gameObject.SetActive(true);
        }
        else
        {
            MenuManager.Instance.shakeClaimButton(true); // Shake and Set button true
        }

        while (SpinWheel_RemaningTime.TotalMinutes > 0)
        {
            SpinWheel_RemaningTime = SpinWheel_SavedTime.Subtract(DateTime.Now);
            MenuManager.Instance.setTime(SpinWheel_RemaningTime.Hours + ":" + SpinWheel_RemaningTime.Minutes + ":" + SpinWheel_RemaningTime.Seconds); //set time in texts
            yield return new WaitForSeconds(1f); //Wait for one second wait in updating text
        }
        if (SpinWheel_RemaningTime.TotalMinutes <= 0)
        {
            MenuManager.Instance.shakeClaimButton(true); // Shake and Set button true
        }
        StopCoroutine(CheckTimer());
    }



    /// <summary>
    ///  REWARD KICK TIMER WORKING
    /// </summary>
    public void GetRewardKick()
    {
        int minutes = 720;
        RewardKick_CurrentTime = DateTime.Now.AddMinutes(minutes); //How many minutes to add next time 1440 means 24 hours
        RewardKick_CurrentTime.SaveDate("SavedDateRewardKick");
        StopCoroutine(CheckTimerRewardKick());
        StartCoroutine(CheckTimerRewardKick());

        string temp = PluginCallBackManager.instance.NotificationTexts[4];
        //Daily Notifications
        GleyNotifications.Initialize();
        GleyNotifications.SendNotification("Soccer Kicks Strike", temp, new System.TimeSpan(0, minutes, 0), null, null, "Opened from Gley Notification");
    }
    public IEnumerator CheckTimerRewardKick()
    {
        RewardKick_SavedTime = DateTimeExtension.GetSavedDate("SavedDateRewardKick");
        RewardKick_RemaningTime = RewardKick_SavedTime.Subtract(DateTime.Now);
        if (RewardKick_RemaningTime.TotalMinutes > 0)
        {
            MenuManager.Instance.RewardKickClaimButtonEnabler(false); // set button active false
                                                                      //  MenuManager.Instance.timerPanel.gameObject.SetActive(true);
        }
        else
        {
            MenuManager.Instance.RewardKickClaimButtonEnabler(true); // Shake and Set button true

            RewardOnGameStartMenu. RewardKickCloseState = RewardOnGameStartMenu.RewardKickDialogClose.unclosed;   //setting so dialog can show in levelmenu

        }

        while (RewardKick_RemaningTime.TotalMinutes > 0)
        {
            RewardKick_RemaningTime = RewardKick_SavedTime.Subtract(DateTime.Now);
            MenuManager.Instance.RewardKicksetTime(RewardKick_RemaningTime.Hours + ":" + RewardKick_RemaningTime.Minutes + ":" + RewardKick_RemaningTime.Seconds);
            yield return new WaitForSeconds(1f); //Wait for one second wait in updating text
        }
        if (RewardKick_RemaningTime.TotalMinutes <= 0)
        {
            MenuManager.Instance.RewardKickClaimButtonEnabler(true); // Shake and Set button true

            RewardOnGameStartMenu.RewardKickCloseState = RewardOnGameStartMenu.RewardKickDialogClose.unclosed;   //setting so dialog can show in levelmenu 

        }
        StopCoroutine(CheckTimerRewardKick());
    }

    public bool CheckTimerRewardKickForExternalUser()   //to simply check reward kick timer status
    {
        RewardKick_SavedTime = DateTimeExtension.GetSavedDate("SavedDateRewardKick");
        RewardKick_RemaningTime = RewardKick_SavedTime.Subtract(DateTime.Now);
        if (RewardKick_RemaningTime.TotalMinutes > 0)
        {
            return false; //if timer has not completed than returns false
        }
        else
        {
            return true;    //if timer has completed than returns true
        }   
    }



    /// <summary>
    ///  ENERGY TIMER WORKING
    /// </summary>
    public void EnergyConsumedReloadEnergy()
    {
        int minutes = 2;

        EnergyDrink_CurrentTime = DateTime.Now.AddMinutes(minutes); //How many minutes to add next time 1440 means 24 hours
        EnergyDrink_CurrentTime.SaveDate("SavedDateReloadEnergy");
        MenuManager.Instance.EnergyReloadTimerEnabler(true);

        StopCoroutine(CheckTimerEnergyDrink());
        StartCoroutine(CheckTimerEnergyDrink());

        
        string temp = PluginCallBackManager.instance.NotificationTexts[6];
        //Daily Notifications
        GleyNotifications.Initialize();
        GleyNotifications.SendNotification("Soccer Kicks Strike", temp, new System.TimeSpan(0, minutes, 0), null, null, "Opened from Gley Notification");
    }
    public IEnumerator CheckTimerEnergyDrink()
    {
        EnergyDrink_SavedTime = DateTimeExtension.GetSavedDate("SavedDateReloadEnergy");
        EnergyDrink_RemaningTime = EnergyDrink_SavedTime.Subtract(DateTime.Now);
    
     /*   if (RemaningTime.TotalMinutes <= 0)
        {
            GenericVariables.AddEnergy(1);
            ReloadEnengyTimerInProgress = false;

        }*/
            MenuManager.Instance.EnergyReloadTimerEnabler(true);
        while (EnergyDrink_RemaningTime.TotalMinutes > 0 && GenericVariables.GetEnergy()==0)
        {

            ReloadEnengyTimerInProgress = true;
            EnergyDrink_RemaningTime = EnergyDrink_SavedTime.Subtract(DateTime.Now);
            MenuManager.Instance.EnergyReloadSetTime(EnergyDrink_RemaningTime.Minutes + ":" + EnergyDrink_RemaningTime.Seconds);
            yield return new WaitForSeconds(1f); //Wait for one second wait in updating text
        }
        if (EnergyDrink_RemaningTime.TotalMinutes <= 0 && GenericVariables.GetEnergy()==0)
        {
            GenericVariables.AddEnergy(1);
            ReloadEnengyTimerInProgress = false;
            MenuManager.Instance.EnergyReloadTimerEnabler(false);

        }
        else if(GenericVariables.GetEnergy() > 0)
        {
            MenuManager.Instance.EnergyReloadTimerEnabler(false);
            StopCoroutine(CheckTimerEnergyDrink());

        }
        MenuManager.Instance.EnergyReloadTimerEnabler(false);

        StopCoroutine(CheckTimerEnergyDrink());
    }
    /* public void DayCheck()
     {
         stringDate = PlayerPrefs.GetString("DailyRewardKick", "01/01/2000 00:00:00");
         stringNextDate = PlayerPrefs.GetString("NextDateRewardKick", DateTime.Parse(stringDate).AddDays(1).ToString());
         nextDate = DateTime.Parse(stringNextDate);
         oldDate = DateTime.Parse(stringDate);
     }

     public void LateUpdate()
     {
         newDate = System.DateTime.Now;
         TimeSpan difference = newDate.Subtract(oldDate);

         if (difference.Days >= 1)
         {
             TimerTxt.text = "CLAIM";
             UIMenusManager.Instance.rewardAvailable = true;
         }
         else
         {
             TimeSpan remainingTimeForNewChallenge = nextDate.Subtract(System.DateTime.Now);
             TimerTxt.text = "REMAINING TIME: " + remainingTimeForNewChallenge.Hours.ToString("f0") + ":" + remainingTimeForNewChallenge.Minutes.ToString("f0") + ":" + remainingTimeForNewChallenge.Seconds.ToString("f0");
         }
     }*/



}

public static class DateTimeExtension
{
    public static void SaveDate(this DateTime _date, string Key)
    {
        string date;
        try
        {
            date = Convert.ToString(_date);


        }
        catch
        {
            date= DateTime.Now.ToString();

        }
        PlayerPrefs.SetString(Key, date);
    }
    public static DateTime GetSavedDate(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
           // Debug.Log(" DOES HAVE A KEY DATE");
            string date = PlayerPrefs.GetString(key);
            try
            {
                return Convert.ToDateTime(date);

            }
            catch
            {
                return DateTime.Now;

            }
        }
        else
        {
            //Debug.Log(" DOESNT HAVE A KEY DATE");

            return DateTime.Now;
        }
    }

    /*public static void SaveDate(this DateTime _date, string Key = "SavedDate")
    {
        string d = Convert.ToString(_date); 
        PlayerPrefs.SetString(Key, d);
    }
    public static DateTime GetSavedDate(string key = "SavedDate")
    { if (PlayerPrefs.HasKey("SavedDate")) 
        {
            string d = PlayerPrefs.GetString("SavedDate"); 
            return Convert.ToDateTime(d);
        } 
        else 
        { 
            return DateTime.Now; 
        }
    }*/

}
