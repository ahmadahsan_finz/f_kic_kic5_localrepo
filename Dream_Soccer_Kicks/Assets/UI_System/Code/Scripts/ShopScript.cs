﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ShopScript : MonoBehaviour
{
    public static ShopScript instance;
    public GameObject SubscriptionButton;
    public GameObject energyButton;
 //   [Header("First Buy Button")]
   // public Button FirstButton;
   // public int price;
  //  public int amount;
    [Header("Second Fire Buy Button")]
    public Button SecondFireBuyButton;
    public int SecondFireBuyprice;
    public int SecondFireBuyamount;

    [Header("ThirdFire Buy Button")]
    public Button ThirdFireBuyButton;
    public int ThirdFireBuyprice;
    public int ThirdFireBuyamount;

//    [Header("Component for in Gameplay")]
    public GameObject backButton;
    public GameObject ShopMenu;
  
    public static string ProductId;

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
        }

    }
    private void OnEnable()
    {
        PluginCallBackManager.fireRewardAdComplete_ += giveRewardFire;

        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
           
            //     Debug.Log("Customization menu on enable");
            AdController.instance.HideBannerAd();

            EnableDisableBackButton(true);
        }
        else
        {
            EnableDisableBackButton(false);

        }

        if (GameConstants.GetSubscriptionStatus() != "Yes")
        {
            SubscriptionButton.SetActive(true);
        }
        else
        {
            SubscriptionButton.SetActive(false);

        }
    }
    private void Start()
    {
        //   FirstButton.onClick.AddListener(() => onButtonPressed(price, amount)); //Add relevant listeners to each button
        SecondFireBuyButton.onClick.AddListener(() => onClickFirePurchase(SecondFireBuyprice, SecondFireBuyamount));
        ThirdFireBuyButton.onClick.AddListener(() => onClickFirePurchase(ThirdFireBuyprice, ThirdFireBuyamount));
    }
    public void onRechargeEnergyPressed(int price)
    {
        StartCoroutine(recharge(price));
        
    }

    public void onPurchase(int index)
    {

      
        if (index==0)        //"inapp.diamond25")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);
        }
        else if (index == 1)  //"inapp.diamond100")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);

        }
        else if (index == 2)   //"inapp.diamond300")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);

        }
        else if (index == 3)   //"inapp.diamond600")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString(); ;
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);

        }
        else if (index == 4)   // "inapp.diamond750")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);
        }
        else if (index == 5)   //"inapp.diamond900")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);
        }
        else if (index == 6)   // "inapp.diamond35000")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseDiamonds);
        }
        else if (index == 7)   //"inapp.coin250")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseCoins);
        }
        else if (index == 8)   //"inapp.coin650")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseCoins);
        }
        else if (index == 9)   // "inapp.coin900")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseCoins);
        }
        else if (index == 10)   // "inapp.coin35000")
        {
            ProductId = Purchaser.Instance.InAppIds_Consumable[index].ToString();
            Purchaser.Instance.BuyProductID(ProductId, PurchaseCoins);
        }

        //else if (index == 11)   // "subscription")
        //{
        //    ProductId = Purchaser.Instance.InAppIds_Subscription;
        //    Purchaser.Instance.BuyProductID(ProductId, PurchaseSubscription);
        //}

    }
    public void RemoveAds()
    {
        //Purchaser.Instance.BuyProductID(Purchaser.Instance.InAppIds_NonConsumable[0], AdConstants.disableAds);
        AdConstants.disableAds();
    }

    


    public void PurchaseDiamonds()
    {
        Debug.Log("Product ID is " + ProductId);
        if (ProductId == Purchaser.Instance.InAppIds_Consumable[0].ToString()) //"inapp.diamond25")
        {
            GenericVariables.AddDiamonds(25);
        } 
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[1].ToString()) //"inapp.diamond100")
        {
            GenericVariables.AddDiamonds(100);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[2].ToString()) //"inapp.diamond300")
        {
            GenericVariables.AddDiamonds(300);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[3].ToString()) //"inapp.diamond600")
        {
            GenericVariables.AddDiamonds(600);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[4].ToString()) //"inapp.diamond750")
        {
            GenericVariables.AddDiamonds(750);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[5].ToString()) //"inapp.diamond900")
        {
            GenericVariables.AddDiamonds(900);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[6].ToString()) //"inapp.diamond35000")
        {
            GenericVariables.AddDiamonds(35000);
        }
        
    }
    public void PurchaseCoins()
    {
        if (ProductId == Purchaser.Instance.InAppIds_Consumable[7].ToString()) // "inapp.coin250")
        {
            GenericVariables.AddCoins(250);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[8].ToString()) // "inapp.coin650")
        {
            GenericVariables.AddCoins(650);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[9].ToString()) //"inapp.coin900")
        {
            GenericVariables.AddCoins(900);
        }
        else if (ProductId == Purchaser.Instance.InAppIds_Consumable[10].ToString()) //"inapp.coin35000")
        {
            GenericVariables.AddCoins(35000);

        }
    }

    public void OnClickSubscriptionButton()
    {
        //MenuManager menumanager = new MenuManager;
        //menumanager. EnableSubscription_Offer_Menu();
        MenuManager.Instance.EnableSubscription_Offer_Menu();

    }

    //public void PurchaseSubscription()
    //{
    //    if (ProductId == Purchaser.Instance.InAppIds_Subscription.ToString()) // "")
    //    {
    //        GenericVariables.AddCoins(500);
    //        GenericVariables.AddDiamonds(500);
    //        GenericVariables.AddFire(5);

    //        int rand = UnityEngine.Random.Range(0, 3);

    //        switch (rand)
    //        {
    //            case 0:
    //                CustomizationScript.instance.UnlockBallsForSubscription();
    //                GameConstants.SetNewNotificationFootballMenu(1);
    //                GameConstants.SetNewNotificationCustomizationMenu(1);
    //        //        CustomiazationMenuScript.instance.checkNotificationOnIcons();
    //                BottomTabPanelScript.instance.EnableNotificationIndicatorOnCustomizationButton();


    //                break;
    //            case 1:
    //                CustomizationScript.instance.UnlockKitShirtForSubscription();
    //                GameConstants.SetNewNotificationFootballMenu(1);
    //                GameConstants.SetNewNotificationCustomizationMenu(1);
    //            //    CustomiazationMenuScript.instance.checkNotificationOnIcons();
    //                BottomTabPanelScript.instance.EnableNotificationIndicatorOnCustomizationButton();


    //                break;

    //            case 2:
    //                CustomizationScript.instance.UnlockKitShortsForSubscription();
    //                GameConstants.SetNewNotificationFootballMenu(1);
    //                GameConstants.SetNewNotificationCustomizationMenu(1);
    //              //  CustomiazationMenuScript.instance.checkNotificationOnIcons();
    //                BottomTabPanelScript.instance.EnableNotificationIndicatorOnCustomizationButton();


    //                break;
    //        }

    //        RemoveAds();

    //        Purchaser.Instance.GenerateProductSubscriptionReceipt(ProductId);
    //        PlayerPrefs.SetString("COINPERDAY",  DateTime.Now.ToString());

    //        GameConstants.SetSubscriptionStatus("Yes");
    //        EnableDisableSubscriptionButton(false);
    //        MenuManager.Instance.SubscriptionRewardShowMenu.SetActive(true);


    //    }
    //}

    public void onClickFireRewardAd()
    {
       GenericVariables.RewardAdIndex = 0;
        AdConstants.currentState = AdConstants.States.OnRewarded;
        AdController.instance.ChangeState();
    }
    public void onClickFirePurchase(int price, int amount)
    {
        MenuManager.Instance.FireBought(price, amount);
    }
    public void giveRewardFire()
    {
        GenericVariables.RewardAdIndex = -1;
        GenericVariables.AddFire(1);
    }
 
    public IEnumerator recharge(int price) //REcharge and show animation 
    {
        int count = PlayerPrefs.GetInt(GenericVariables.diamondStr);
        if (count >= price)
        {
            MenuManager.Instance.moveObject("energy", energyButton);
            MenuManager.Instance.moveConsumable();
            yield return new WaitForSeconds(2.5f);
            MenuManager.Instance.EnergyBought(price);
        }
        
    }
    public void EnableDisableBackButton(bool active)
    {
        backButton.SetActive(active);
    }

    public void BackButtonPressed()
    {
        // AdController.instance.ShowBannerAd();
      //  Time.timeScale = 1;
        // EnableDisableBackButton(false);
        ShopMenu.gameObject.SetActive(false);
    }

    public void EnableDisableSubscriptionButton(bool value)
    {
        SubscriptionButton.SetActive(value);
    }

    private void OnDisable()
    {
        PluginCallBackManager.fireRewardAdComplete_ -= giveRewardFire;
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {


            //  Debug.Log("Customization menu on disable");

          //  AdController.instance.ShowBannerAd();

            EnableDisableBackButton(false);
        }
    }



}
