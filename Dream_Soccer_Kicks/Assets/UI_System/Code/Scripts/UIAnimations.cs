﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIAnimations : MonoBehaviour
{
    public static UIAnimations instance;
    public static List<RectTransform> panel;
    Transform temp;
    public GameObject parent;
    public RectTransform[] Containerpanels;
    public float slideSpeed;
    
    public UnityEngine.UI.Image m_Fader;
    public float m_FadeInDuration;
    public float m_FadeOutDuration;

    private static int buttonInd;
    private static int currInd;
    public RewardedTimeManager timeManager;

    public GameObject BuyButton;

    // Start is called before the first frame update
    private void Awake()
    {            
        
        buttonInd = 0;
        currInd = -1;

        if(instance==null)
        {
            instance = this;
        }
        else
        {
            //Destroy(gameObject);
        }
    }
 
    private void Start()
    {
      //  Debug.Log("MenuManager.Instance.LevelMenuLoadBool is " + MenuManager.Instance.LevelMenuLoadBool);
        if (SoundManagerNew.Instance != null)
        {
            SoundManagerNew.Instance.playMusic("MenuBG");
        }
        SoundManagerNew.Instance.OnOffSound(MenuManager.SoundBool);
        SoundManagerNew.Instance.OnOffMusic(MenuManager.MusicBool);

        if (m_Fader)
        {
            m_Fader.gameObject.SetActive(true);
            FadeIn();
        }
        else
        {
            //Debug.Log("Image Not Found");
        }

        if (!MenuManager.Instance.LevelMenuLoadBool)             //if main menu is loaded
           {
               SetMainMenu();
       //     MainButton(0);
             //  SetLevelSelectMenu();
           } 
           else                         //if level menu is loaded
           {
            //  MenuManager.Instance.CameraChange();
           }


    }
    //LevelSelectMenu function Aqsa Gilani
    /*public void SetLevelSelectMenu()
    {
        // panel = MenuManager.Instance.LevelPanels;
        // panel = MenuManager.Instance.LoadLevelPanels();
        //     MenuManager.Instance.CameraChange();
        panel = MenuManager.Instance.MainPanels;
        temp = panel[0];
        temp.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        temp.gameObject.SetActive(true);
    }*/
    // Update is called once per frame
    public void SetMainMenu()
    {
        panel = MenuManager.Instance.MainPanels;
   //   MenuManager.Instance.CameraChange();
        temp = panel[0];
        temp.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        temp.gameObject.SetActive(true);
    }
    /*Gilani public void SetSphereLevelMenu()
     {
         // panel = MenuManager.Instance.LevelPanels;
         // panel = MenuManager.Instance.LoadLevelPanels();
         //     MenuManager.Instance.CameraChange();
         panel = MenuManager.Instance.MainPanels;
         temp = panel[0];
         temp.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
         temp.gameObject.SetActive(true);
     }*/
    public void MainButton(int index) //button press for animations
    {
       
            if (index == 6)  //fortune wheel back button pressed
            {
             //   if (GameConstants.getLevel2Tutorial() == "Yes")
               // if (GameConstants.getStartGamePlayTutorial() == "Yes" && SceneManager.GetActiveScene().buildIndex == 2)          //when fortune wheel back button is pressed and you are at level 2 

                if (SceneManager.GetActiveScene().buildIndex == 2)          //when fortune wheel back button is pressed and you are at level 2 
                {
                        index = 100;                    //assigning this index because it will be called below 

             //   Debug.Log("getStartGamePlayTutorial = YES");
                 //       TutorialManager.instance.TurnOffLevelTwoTutorial();
                        MenuManager.Instance.setFireAddPanel(false);
                        MenuManager.Instance.setSettingsButton(true);
                        MenuManager.Instance.timerPanel.SetActive(true);
                        UITutorialManager.instance.HandFortuneWheel2.SetActive(false);
                        FortuneWheelManager.Instance.Backbutton.SetActive(false);

                    //    timeManager.GetReward();
                    //MenuManager.Instance.LoadLevel();
                     MenuManager.Instance.levelSuccessTakeToLevelMenu();   

                 }
                 else
                 {
                  //      Debug.Log("getStartGamePlayTutorial = NO");

                        index = 0;
                        MenuManager.Instance.setFireAddPanel(false);
                        MenuManager.Instance.setSettingsButton(true);
                        MenuManager.Instance.timerPanel.SetActive(true);
                        UITutorialManager.instance.HandFortuneWheel2.SetActive(false);
                        FortuneWheelManager.Instance.Backbutton.SetActive(false);

                      //  timeManager.GetReward();

                    /*  if (GameConstants.getFirstStartUITutorial() == "Yes")           //if tutorial is enabled
                        {
                            UITutorialManager.instance.DisableBlackShader();                //disable UI tutorial
                        }*/
                    }

            }
            else if (index == 0 || index == 1) //if home or customize button pressed button pressed
            {
                    //        Debug.Log("main menu called from MainButton 1");
/* if(index==1)
 {
     MenuManager.Instance.MainPanels[1].gameObject.SetActive(true);
     MenuManager.Instance.MainPanels[2].gameObject.SetActive(false);
     MenuManager.Instance.MainPanels[2].gameObject.SetActive(false);
 }
*/
#if UNITY_IPHONE || UNITY_IOS

               if(index==1) //if customize menu
               {
                   AdConstants.currentState = AdConstants.States.OnMainMenu;
                   AdController.instance.ChangeState();
               }

#endif
                MenuManager.Instance.setFireAddPanel(false);

                MenuManager.Instance.setSettingsButton(true);
       
                int body = GenericVariables.getcurrBody();

              //   int shorts = GenericVariables.getcurrShorts();
          
                int skin = GenericVariables.getcurrSkin();
                
                int football = GenericVariables.getcurrFootball();

                int celebration = GenericVariables.getcurrCelebration();

            // MenuManager.Instance.ChangeBody(body);
                 CustomizationScript.instance.ChangeBody(body);

            //   CustomizationScript.instance.ChangeShorts(shorts);

                CustomizationScript.instance.ChangeSkin(skin);
            
                CustomizationScript.instance.ChangeFootball(football);

                CustomizationScript.instance.ChangeCelebrationWithoutPlaying(celebration);
              
            
            // CustomizationScript.instance.ChangeCelebration(celebration);
               // MenuManager.Instance.ChangeSkin(skin);
                 BuyButton.SetActive(false);

            }
            else if (index == 2 || index == 4) //if shop button pressed
                {
                    MenuManager.Instance.setEnergyBar(true);
                    MenuManager.Instance.setFireAddPanel(true);
                    MenuManager.Instance.setSettingsButton(false);
                    if(index==4)                                        //claim button clicked
                    {
                              UITutorialManager.instance.FortuneWheelClaimTutorial();

                /*        if (GameConstants.getStartGamePlayTutorial() == "Yes")           //if tutorial is enabled
                        {
                                Debug.Log("index==5 FortuneWheelClaimTutorial ");
                              UITutorialManager.instance.FortuneWheelClaimTutorial();
    
                           // UITutorialManager.instance.DisableUpperBottomPanelBlackShader();                //disable upper bottom panel black shader 
                        }
                */
                    }
            }
        
            if(index==100)       //check when back button of fortune wheel is pressed and you are at level 2
        {
                return;
            }
            if (index == 5)
        {
          //Gilani  SetSphereLevelMenu();
        }
            if(index==3)    //Career button pressed //ahmad
            {

#if UNITY_IPHONE || UNITY_IOS

           if (GameConstants.getFirstStartUITutorial() != "Yes")
                {
                AdConstants.currentState = AdConstants.States.OnMainMenu;
                AdController.instance.ChangeState();
                }
#endif

            //          Debug.Log("indexxxxx is + " + index);
            // AnalyticsManagerSoccerKicks.Instance.EventTransition(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_Career);
            AnalyticsManagerSoccerKicks.Instance.Event_Transitions(AnalyticsManagerSoccerKicks.Event_triggers.MainMenu_Career, AnalyticsManagerSoccerKicks.Event_State.Gameplay_Events);
            }
           buttonInd = index;
        //    Debug.Log("indexxxxx is + " + buttonInd);

         temp = panel[buttonInd];
        //temp.transform.parent = parent.transform;
        //temp.transform.SetSiblingIndex(1);
        if (buttonInd > currInd)
            {
      //          Debug.Log("(buttonInd > currInd) + " + buttonInd);

                MenuManager.Instance.setPanels();
                temp.gameObject.SetActive(true);
                temp.GetComponent<RectTransform>().anchoredPosition = Containerpanels[1].anchoredPosition; //Do right slide
            }
        else if (buttonInd < currInd)
            {
        //        Debug.Log("(buttonInd < currInd) + " + buttonInd);
                MenuManager.Instance.setPanels();
                temp.gameObject.SetActive(true);
                temp.GetComponent<RectTransform>().anchoredPosition = Containerpanels[0].anchoredPosition; //Do left slide
            }
            else
            {
                return;
            }
            //temp.gameObject.SetActive(true);
            currInd = buttonInd;
            BuyButton.SetActive(false);

          MenuManager.Instance.DisablePopUps();

        //Panel moves from it's own position to parent's position with slideSpeed
        //   iTween.MoveTo(temp.gameObject,iTween.Hash("position" , parent.transform.position, "speed", slideSpeed)); //Perform slide
        iTween.MoveTo(temp.gameObject, iTween.Hash("position", parent.transform.position, "time", slideSpeed, "ignoretimescale", true)); //Perform slide
           FadeIn();
        
    }

#region Fade Animations
    public void FadeIn()
    {
        if (m_Fader)
        {
            ResetFader();
            m_Fader.gameObject.SetActive(true);
            m_Fader.CrossFadeAlpha(0f, m_FadeInDuration, false);
        }
    }

    public void FadeOut()
    {
        if (m_Fader)
        {
            m_Fader.CrossFadeAlpha(1f, m_FadeOutDuration, false);
        }
    }

    public void ResetFader()
    {
        m_Fader.gameObject.SetActive(false);
        Color color = m_Fader.color;
        color.a = 1;
        m_Fader.color = color;
    }
#endregion

}
