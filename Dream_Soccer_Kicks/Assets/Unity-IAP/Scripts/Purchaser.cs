using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.UI;

// Placing the Purchaser class in the CompleteProject namespace allows it to interact with ScoreManager, 
// one of the existing Survival Shooter scripts.

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class Purchaser : MonoBehaviour, IStoreListener
{
    public static Purchaser Instance { set; get; }
    private static IStoreController m_StoreController;
    private static IExtensionProvider m_StoreExtensionProvider;



    private IAppleExtensions m_AppleExtensions;
    private ISamsungAppsExtensions m_SamsungExtensions;
    private IMicrosoftExtensions m_MicrosoftExtensions;
    private ITransactionHistoryExtensions m_TransactionHistoryExtensions;
    private IGooglePlayStoreExtensions m_GooglePlayStoreExtensions;
    Dictionary<string, string> introductory_info_dict;
    Dictionary<string, string> google_play_store_product_SKUDetails_json;


    //Buying Products with these IDs
    //#if UNITY_ANDROID
    [SerializeField]string[] AndroidInAppIds_Consumable = {
		"inapp.100Coins"};

	[SerializeField] string[] AndroidInAppIds_NonConsumable = {
		"inapp.removeads"
	};
	//#elif UNITY_IOS
	[SerializeField] string[] IOSInAppIds_Consumable = {
		"inapp.100Coins"};

	[SerializeField] string[] IOSInAppIds_NonConsumable = {
		"inapp.removeads" 
	};
	//#endif
	[HideInInspector]
    public string[] InAppIds_Consumable = {
		};
	
	[HideInInspector]
    public string[] InAppIds_NonConsumable = {
        
    };

    public string InAppIds_Subscription;


    string inAppToBuy;


    static Action OnProcessSuccessful = null;

    void Awake()
    {
        DontDestroyOnLoad(this);
        Instance = this;
		AssignKeys ();
    }

	void AssignKeys()
	{
		#if UNITY_ANDROID
		if(AndroidInAppIds_Consumable!=null){
		InAppIds_Consumable = new string[AndroidInAppIds_Consumable.Length];
		for (int i = 0; i < AndroidInAppIds_Consumable.Length; i++) {
			InAppIds_Consumable [i] = AndroidInAppIds_Consumable [i];
		}
		}
		if(AndroidInAppIds_NonConsumable!=null){
		InAppIds_NonConsumable = new string[AndroidInAppIds_NonConsumable.Length];
		for (int i = 0; i < AndroidInAppIds_NonConsumable.Length; i++) {
			InAppIds_NonConsumable [i] = AndroidInAppIds_NonConsumable [i];
		}
		}
		#elif UNITY_IOS
		if(IOSInAppIds_Consumable!=null){
		InAppIds_Consumable = new string[IOSInAppIds_Consumable.Length];
		for (int i = 0; i < IOSInAppIds_Consumable.Length; i++) {
		InAppIds_Consumable [i] = IOSInAppIds_Consumable [i];
		}
		}
		if(IOSInAppIds_NonConsumable!=null){
		InAppIds_NonConsumable = new string[IOSInAppIds_NonConsumable.Length];
		for (int i = 0; i < IOSInAppIds_NonConsumable.Length; i++) {
		InAppIds_NonConsumable [i] = IOSInAppIds_NonConsumable [i];
		}
		}
		#endif
	}

    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        for (int i = 0; i < InAppIds_Consumable.Length; i++)
        {
            builder.AddProduct(InAppIds_Consumable[i], ProductType.Consumable);
        }

        for (int i = 0; i < InAppIds_NonConsumable.Length; i++)
        {
            builder.AddProduct(InAppIds_NonConsumable[i], ProductType.NonConsumable);
        }

        builder.AddProduct(InAppIds_Subscription, ProductType.Subscription);

        //for (int i = 0; i < IAPGameConstants.consumableIAPKeys.Length; i++)
        //{
        //    builder.AddProduct(IAPGameConstants.consumableIAPKeys[i], ProductType.Consumable);
        //}

        //builder.AddProduct(IAPGameConstants.noAdsKey, ProductType.NonConsumable);
        //builder.AddProduct(IAPGameConstants.unlockAllCharacters, ProductType.NonConsumable);


        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }



    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            apple.RestoreTransactions((result) =>
            {
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }
//    bool isReceptChecked = false;

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
//        Debug.Log("OnInitialized: PASS");

        m_StoreController = controller;
        m_StoreExtensionProvider = extensions;



        // Get Receipt Here

        //if (info.isExpired() == Result.True)
        //{

        //}
     //   if (!isReceptChecked)
            CheckSubscriptionOnGameStart();
    }



    public void CheckSubscriptionOnGameStart()
    {
        SubscriptionInfo info = GetProductSubscriptionReceipt(InAppIds_Subscription.ToString());

        if (info != null)
        {
            if (info.isSubscribed() == Result.True || info.isFreeTrial() == Result.True ) //if subscribed
            {
                GameConstants.SetSubscriptionStatus("Yes");

                DateTime CurrentTime = DateTime.Now;
                DateTime coinTime = DateTime.Parse(PlayerPrefs.GetString("COINPERDAY"));
                try
                {
                    TimeSpan result = CurrentTime.Subtract(coinTime);
                    if (result.Days >= 1)
                    {
                        GenericVariables.AddCoins(500);
                        GenericVariables.AddDiamonds(500);
                        GenericVariables.AddFire(5);
                        int rand = UnityEngine.Random.Range(0, 3);

                        switch (rand)
                        {
                            case 0:
                                CustomizationScript.instance.UnlockBallsForSubscription();
                                GameConstants.SetNewNotificationFootballMenu(1);
                                GameConstants.SetNewNotificationCustomizationMenu(1);
                                CustomiazationMenuScript.instance.checkNotificationOnIcons();

                                break;
                            case 1:
                                CustomizationScript.instance.UnlockKitShirtForSubscription();
                                GameConstants.SetNewNotificationFootballMenu(1);
                                GameConstants.SetNewNotificationCustomizationMenu(1);
                                CustomiazationMenuScript.instance.checkNotificationOnIcons();


                                break;

                            case 2:
                                CustomizationScript.instance.UnlockKitShortsForSubscription();
                                GameConstants.SetNewNotificationFootballMenu(1);
                                GameConstants.SetNewNotificationCustomizationMenu(1);
                                CustomiazationMenuScript.instance.checkNotificationOnIcons();

                                break;
                        }

                        ShopScript.instance.RemoveAds();

                        PlayerPrefs.SetString("COINPERDAY", DateTime.Now.ToString());
                        ShopScript.instance. EnableDisableSubscriptionButton(false);
                        MenuManager.Instance.SubscriptionRewardShowMenu.SetActive(true);

                    }

                }
                catch (Exception e) { }

            }
            else if ( info.isSubscribed() == Result.False || info.isExpired() == Result.True || info.isFreeTrial() == Result.False)//not subscribed
            {
                GameConstants.SetSubscriptionStatus("No");
                AdConstants.EnableAds();
                MenuManager.Instance.EnableSubscription_Offer_Menu();   //enable subscription menu
                ShopScript.instance.EnableDisableSubscriptionButton(true); //enable subscription button in shop menu
            }
        }
        else
        {
           
            Debug.Log("NOT A VALID RECEIPT");
        }

 //       isReceptChecked = true;



    }

    private void OnDeferred(Product item)
    {
        Debug.Log("Purchase deferred: " + item.definition.id);
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        //Debug.Log("////////////////// 2" + "   " + args.purchasedProduct.definition.id + "  " + IAPGameConstants.unlockAllCharacters);

        if(String.Equals(args.purchasedProduct.definition.id, inAppToBuy, StringComparison.Ordinal))
        {
            SuccessfulPurchase();
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }

        
        return PurchaseProcessingResult.Complete;
    }


    void SuccessfulPurchase()
    {
        if(OnProcessSuccessful != null)
        {
            OnProcessSuccessful.Invoke();
        }
    }

    

    public void BuyProductID(string productId, Action OnSuccess = null)
    {

        if(OnSuccess != null)
            OnProcessSuccessful = OnSuccess;

        inAppToBuy = productId;


        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
				Instantiate(Resources.Load("InAppLoadingMenu"));

                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        else
        {
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }


    //void OldBuyMethod()
    //{
    //    if (String.Equals(args.purchasedProduct.definition.id, IAPGameConstants.consumableIAPKeys[0], StringComparison.Ordinal))
    //    {
    //        Debug.Log("you have purchased 100 coins...");
    //        SuccessfulPurchase();
    //    }
    //    else if (String.Equals(args.purchasedProduct.definition.id, IAPGameConstants.consumableIAPKeys[1], StringComparison.Ordinal))
    //    {
    //        Debug.Log("you have purchased 200 coins...");
    //        SuccessfulPurchase();
    //    }
    //    else if (String.Equals(args.purchasedProduct.definition.id, IAPGameConstants.consumableIAPKeys[2], StringComparison.Ordinal))
    //    {
    //        Debug.Log("you have purchased 500 coins...");
    //        SuccessfulPurchase();
    //    }
    //    else if (String.Equals(args.purchasedProduct.definition.id, IAPGameConstants.noAdsKey, StringComparison.Ordinal))
    //    {
    //        IAPGameConstants.shouldDisplayAds = false; //UMAIR NO ADS
    //        AdConstants.disableAds();
    //        Debug.Log("ads removed...");
    //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));

    //        SuccessfulPurchase();
    //    }
    //    else if (String.Equals(args.purchasedProduct.definition.id, IAPGameConstants.unlockAllCharacters, StringComparison.Ordinal))
    //    {
    //        Debug.Log("////////////////// 3");

    //        //PlayerSelectionManager.instance.UnlockAllCharacter_Successful();

    //        SuccessfulPurchase();

    //        Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
    //    }
    //    else
    //    {
    //        Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
    //    }
    //}

    #region Subscription Area

 // to Enable Subscriptions add this 'SUBSCRIPTION_MANAGER' in 'Scripting Define Symbol' field in Player Settings
    Dictionary<string, string> dict = new Dictionary<string, string>();
    Dictionary<string, string> product_details = new Dictionary<string, string>(); 


    //public void GetAllSubscriptionReceipt()
    //{
    //    foreach (Product item in m_StoreController.products.all)
    //    {
    //        // this is the usage of SubscriptionManager class
    //        if (item.receipt != null)
    //        {
    //            if (item.definition.type == ProductType.Subscription)
    //            {
    //                string intro_json = (dict == null || !dict.ContainsKey(item.definition.storeSpecificId)) ? null : dict[item.definition.storeSpecificId];
    //                SubscriptionManager p = new SubscriptionManager(item, intro_json);
    //                SubscriptionInfo info = p.getSubscriptionInfo();
    //                Debug.Log(info.getProductId());
    //                Debug.Log(info.getPurchaseDate());
    //                Debug.Log(info.getExpireDate());
    //                Debug.Log(info.isSubscribed());
    //                Debug.Log(info.isExpired());
    //                Debug.Log(info.isCancelled());
    //                Debug.Log(info.isFreeTrial());
    //                Debug.Log(info.isAutoRenewing());
    //                Debug.Log(info.getRemainingTime());
    //                Debug.Log(info.isIntroductoryPricePeriod());
    //                Debug.Log(info.getIntroductoryPrice());
    //                Debug.Log(info.getIntroductoryPricePeriod());
    //                Debug.Log(info.getIntroductoryPricePeriodCycles());
    //            }
    //            else
    //            {
    //                Debug.Log("the product is not a subscription product");
    //            }
    //        }
    //        else
    //        {
    //            Debug.Log("the product should have a valid receipt");
    //        }

    //    }
    //}




    public SubscriptionInfo GetProductSubscriptionReceipt(string prod)
    {
#if !UNITY_EDITOR && UNITY_IOS
        Product item = null;
        foreach (Product pro in m_StoreController.products.all)
        {

            if (prod.Equals(pro.definition.id))
            {
                item = pro;
            }
        }
        // this is the usage of SubscriptionManager class
        if (item.receipt != null)
        {
            if (item.definition.type == ProductType.Subscription)
            {
                string intro_json = (product_details == null || !product_details.ContainsKey(item.definition.storeSpecificId)) ? null : product_details[item.definition.storeSpecificId];
                SubscriptionManager p = new SubscriptionManager(item, intro_json);
                SubscriptionInfo info = p.getSubscriptionInfo();

                info.getProductId();
                info.getPurchaseDate();
                info.getExpireDate();
                info.isSubscribed();
                info.isExpired();
                info.isCancelled();
                info.isFreeTrial();
                info.isAutoRenewing();
                info.getRemainingTime();
                info.isIntroductoryPricePeriod();
                info.getIntroductoryPrice();
                info.getIntroductoryPricePeriod();
                info.getIntroductoryPricePeriodCycles();

                return info;
            }
            else
            {

                Debug.Log("the product is not a subscription product");
            }
        }
        else
        {
            Debug.Log("the product should have a valid receipt");
        }
#endif
        return null;

    }

    public void GenerateProductSubscriptionReceipt(string prod)
    {
#if !UNITY_EDITOR && UNITY_IOS
        Product item = null;
        foreach (Product pro in m_StoreController.products.all)
        {

            if (prod.Equals(pro.definition.id))
            {
                item = pro;
            }
        }
        // this is the usage of SubscriptionManager class
        if (item.receipt != null)
        {
            if (item.definition.type == ProductType.Subscription)
            {
                string intro_json = (product_details == null || !product_details.ContainsKey(item.definition.storeSpecificId)) ? null : product_details[item.definition.storeSpecificId];
                SubscriptionManager p = new SubscriptionManager(item, intro_json);
                SubscriptionInfo info = p.getSubscriptionInfo();

                info.getProductId();
                info.getPurchaseDate();
                info.getExpireDate();
                info.isSubscribed();
                info.isExpired();
                info.isCancelled();
                info.isFreeTrial();
                info.isAutoRenewing();
                info.getRemainingTime();
                info.isIntroductoryPricePeriod();
                info.getIntroductoryPrice();
                info.getIntroductoryPricePeriod();
                info.getIntroductoryPricePeriodCycles();

            }
            else
            {

                Debug.Log("Generating Receipt Failed! the product is not a subscription product");
            }
        }
        else
        {
            Debug.Log("Generating Receipt Failed! the product should have a valid receipt");
        }
#endif
    }


    #endregion


}