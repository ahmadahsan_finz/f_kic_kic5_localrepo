﻿#if UNITY_IOS || UNITY_ANDROID

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;
using System.IO;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif


public class BuildPostProcessor
{

    [PostProcessBuildAttribute(1)]
    public static void OnPostProcessBuild(BuildTarget target, string path)
    {

        if (target == BuildTarget.iOS)
        {
#if UNITY_IOS
// Read.
string projectPath = PBXProject.GetPBXProjectPath(path);
PBXProject project = new PBXProject();
project.ReadFromFile(projectPath);
//string targetName = PBXProject.GetUnityTargetName();
// string targetGUID = project.TargetGuidByName(targetName);
#if UNITY_2019_3_OR_NEWER
string targetId = project.GetUnityFrameworkTargetGuid();
#else
string targetId = project.TargetGuidByName(PBXProject.GetUnityTargetName());
#endif


AddFrameworks(project, targetId);

// Write.
File.WriteAllText(projectPath, project.WriteToString());


// Replace with your iOS AdMob app ID. Your AdMob app ID will look
// similar to this sample ID: ca-app-pub-3940256099942544~1458002511
// string appId = "ca-app-pub-9522824014864088~9949874301";

string plistPath = Path.Combine(path, "Info.plist");
PlistDocument plist = new PlistDocument();

plist.ReadFromFile(plistPath);
plist.root.SetBoolean("GADIsAdManagerApp", true);
File.WriteAllText(plistPath, plist.WriteToString());
#endif
        }
    }
#if UNITY_IOS
static void AddFrameworks(PBXProject project, string targetGUID) //
{
        /*
        // Frameworks (eppz! Photos, Google Analytics).
        project.AddFrameworkToProject(targetGUID, "AdSupport.framework", false); //universal
        project.AddFrameworkToProject(targetGUID, "AVFoundation.framework", false); //applovin
        project.AddFrameworkToProject(targetGUID, "CoreGraphics.framework", false); //applovin
        project.AddFrameworkToProject(targetGUID, "CoreMedia.framework", false); //applovin
        project.AddFrameworkToProject(targetGUID, "CoreTelephony.framework", false); //applovin & chartboost
        project.AddFrameworkToProject(targetGUID, "SafariServices.framework", false); //applovin
        project.AddFrameworkToProject(targetGUID, "StoreKit.framework", false); //Universal
        project.AddFrameworkToProject(targetGUID, "SystemConfiguration.framework", false); //applovin

        project.AddFrameworkToProject(targetGUID, "WebKit.framework", false); //universal

        project.AddFrameworkToProject(targetGUID, "libz.tbd", false);

        project.AddFrameworkToProject(targetGUID, "StoreKit.framework", false);
        project.AddFrameworkToProject(targetGUID, "CoreData.framework", false);
        project.AddFrameworkToProject(targetGUID, "GameKit.framework", false);
        project.AddFrameworkToProject(targetGUID, "libxml2.tbd", false);
        project.AddFrameworkToProject(targetGUID, "libsqlite3.tbd", false);
        project.AddFrameworkToProject(targetGUID, "Social.framework", false);
        */
        project.AddFrameworkToProject(targetGUID, "StoreKit.framework", false);
        project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-lxml2");

//project.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "inherited"); //Unity 4 Compatibilty

project.SetBuildProperty(targetGUID, "ENABLE_BITCODE", "NO");

// project.SetBuildProperty(targetGUID, "Enable Modules", "Yes"); //Unity 4 Compatibilty



}
/*
[PostProcessBuild]
public static void ChangeXcodePlist(BuildTarget buildTarget, string pathToBuiltProject) {

if (buildTarget == BuildTarget.iOS) {

// Get plist
string plistPath = pathToBuiltProject + "/Info.plist";
PlistDocument plist = new PlistDocument();
plist.ReadFromString(File.ReadAllText(plistPath));

// Get root
PlistElementDict rootDict = plist.root;
string admobAppId = "ca-app-pub-4921234158243313~9139696148";
// Change value of CFBundleVersion in Xcode plist
rootDict.SetString("GADApplicationIdentifier","");


// Write to file
File.WriteAllText(plistPath, plist.WriteToString());
}
}
*/


#endif
}
#endif