

using System.IO;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using UnityEditor;
using UnityEditor.Callbacks;

public static class XCodePostProcess
{
    [PostProcessBuild(100)]
    public static void OnPostProcessBuild(BuildTarget target, string path)
    {
        if (target == BuildTarget.iOS)
        {
            AddBuildFlag(path);
        }
    }

      
    public static void AddBuildFlag(string path)
    {
#if UNITY_IOS
        string projPath = Path.Combine(path, Path.Combine("Unity-iPhone.xcodeproj", "project.pbxproj"));
        UnityEditor.iOS.Xcode.PBXProject proj = new PBXProject();
        proj.ReadFromString(File.ReadAllText(projPath));
#if UNITY_2019_3_OR_NEWER
        string targetGUID = proj.GetUnityMainTargetGuid();
#else
        string targetGUID = proj.TargetGuidByName("Unity-iPhone");
#endif
        proj.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
        File.WriteAllText(projPath, proj.WriteToString());
#endif
    }
}
