﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public class FinzMenuItems
{
	static int go_count = 0, components_count = 0, missing_count = 0;


	[MenuItem("Finz/Shortcuts/Clear PlayerPrefs")]
    private static void NewMenuOption()
    {
        PlayerPrefs.DeleteAll();

		EditorUtility.DisplayDialog("Data Cleared", "PlayerPrefs are cleared now", "OK");
	}


	[MenuItem("Finz/Shortcuts/Add Coins")]
    private static void Add50CoinsOption()
    {
		int coins = PlayerPrefs.GetInt ("COINS_KEY", 50);
        PlayerPrefs.SetInt ("COINS_KEY", coins + 50);
		PlayerPrefs.Save ();
    }

	[MenuItem("Finz/Shortcuts/Find Missing Scripts")]
	private static void FindInSelected()
	{
	GameObject[] go = Selection.gameObjects;
	go_count = 0;
	components_count = 0;
	missing_count = 0;
	foreach (GameObject g in go)
	{
	FindInGO(g);
	}
	Debug.Log(string.Format("Searched {0} GameObjects, {1} components, found {2} missing", go_count, components_count, missing_count));
	}

	private static void FindInGO(GameObject g)
	{
	go_count++;
	Component[] components = g.GetComponents<Component>();
	for (int i = 0; i < components.Length; i++)
	{
	components_count++;
	if (components[i] == null)
	{
	missing_count++;
	string s = g.name;
	Transform t = g.transform;
	while (t.parent != null)
	{
	s = t.parent.name + "/" + s;
	t = t.parent;
	}
	Debug.Log(s + " has an empty script attached in position: " + i, g);
	}
	}

	foreach (Transform childT in g.transform)
	{
	FindInGO(childT.gameObject);
	}
	}

//	void SaveAdmobIds()
//	{
//#if UNITY_EDITOR
//		FinzAdmobPatch.SetAdmobAppID(, IOS_AppID);
//		UnityEditor.EditorUtility.SetDirty(this);
//#endif
//	}


	[MenuItem("Finz/Shortcuts/Divide Width&Height by 2 &2")]
	public static void Divide_W_H_2()
	{
		DivideSelected(2);
	}



	[MenuItem("Finz/Shortcuts/Multiply W&H by 2 &#2")]
	public static void Multiply_W_H_15()
	{
		MultiplySelected(2f);
	} 

	public static void DivideSelected(float by)
	{
		GameObject[] go = Selection.gameObjects;

		for (int i = 0; i < go.Length; i++)
		{
			if (go[i].transform.GetComponent<RectTransform>() == null)
			{
				Debug.Log("This is not a RectTransform", go[i].gameObject);
			}
			else
			{

				RectTransform rect = go[i].GetComponent<RectTransform>();
				UnityEditor.Undo.RecordObject(rect, "Resizing Object");
				rect.sizeDelta = new Vector2(rect.sizeDelta.x / by, rect.sizeDelta.y / by);
			}
		}
	}

    public static void MultiplySelected(float by)
    {
        GameObject[] go = Selection.gameObjects;

        for (int i = 0; i < go.Length; i++)
        {
            if (go[i].transform.GetComponent<RectTransform>() == null)
            {
                Debug.Log("This is not a RectTransform", go[i].gameObject);
            }
            else
            {

                RectTransform rect = go[i].GetComponent<RectTransform>();
                UnityEditor.Undo.RecordObject(rect, "Resizing Object");
                rect.sizeDelta = new Vector2(rect.sizeDelta.x * by, rect.sizeDelta.y * by);
            }
        }

    }
    
    [MenuItem("Finz/Open 1st Scene &1")]
    public static void openPlugin()
    {
        int i = 0;
        EditorSceneManager.SaveOpenScenes();
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

        EditorSceneManager.OpenScene(EditorBuildSettings.scenes[i].path);
    }



	[MenuItem("Finz/SDK/Enable Admob 5.4.0")]
	private static void AddAdmob5_4()
	{
		AddDefineSymbols.Add("USE_ADMOB_5_4_0");

	}

	[MenuItem("Finz/SDK/Disable Admob 5.4.0")]
	private static void RemoveAdmob5_4()
	{
		AddDefineSymbols.Clear("USE_ADMOB_5_4_0");

	}



	[MenuItem("Finz/Ads & Analytics/Enable Analytics/Firebase")]
	private static void AddFirebase()
	{
		AddDefineSymbols.Add("USE_FIREBASE");

	}

	[MenuItem("Finz/Ads & Analytics/Enable Analytics/Facebook Analytics")]
	private static void AddFacebookSDK()
	{
		AddDefineSymbols.Add("USE_FB_ANALYTICS");

	}

	[MenuItem("Finz/Android/ENABLE NATIVE RATE")]
	private static void ShowNativeRate()
	{
		AddDefineSymbols.Add("SHOW_NATIVE_RATE");

	}

	[MenuItem("Finz/Android/DISABLE NATIVE RATE")]
	private static void disableNativeRate()
	{
		AddDefineSymbols.Clear("SHOW_NATIVE_RATE");

	}

	[MenuItem("Finz/Ads & Analytics/Disable Analytics/Firebase")]
	private static void disableFirebase()
	{
		AddDefineSymbols.Clear("USE_FIREBASE");

	}

	[MenuItem("Finz/Ads & Analytics/Disable Analytics/Facebook Analytics")]
	private static void disableFacebookSDK()
	{
		AddDefineSymbols.Clear("USE_FB_ANALYTICS");

	}


	// Ads
	private static string[] CompanyAds = { "USE_UNITY_ADS", "USE_FACEBOOK", "USE_IRONSOURCE", "USE_APPLOVIN", "USE_ADCOLONY", "USE_TAPJOY", "USE_CHARTBOOST" };



	// Open App Ads
	[MenuItem("Finz/Ads & Analytics/Enable Ads/Open Ads (ADMOB 6.1.0 OR HIGHER)")]
	private static void AddOpenAds()
	{
		AddDefineSymbols.Add("USE_OPENADS");

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/Open Ads (ADMOB 6.1.0 OR HIGHER)")]
	private static void DisableOpenAds()
	{
		AddDefineSymbols.Clear("USE_OPENADS");

	}


	// Admob
	[MenuItem("Finz/Ads & Analytics/Enable Ads/ADMOB")]
	private static void AddAdmob()
	{
		AddDefineSymbols.Add("USE_ADMOB");

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/ADMOB")]
	private static void DisableAdmob()
	{
		AddDefineSymbols.Clear("USE_ADMOB");

	}


	// Unity
	[MenuItem("Finz/Ads & Analytics/Enable Ads/UNITY")]
	private static void AddUnityAds()
	{
		AddDefineSymbols.Add(CompanyAds[0]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/UNITY")]
	private static void DisableUnityAds()
	{
		AddDefineSymbols.Clear(CompanyAds[0]);

	}

	// Facebook
	[MenuItem("Finz/Ads & Analytics/Enable Ads/FACEBOOK")]
	private static void AddFacebookAds()
	{
		AddDefineSymbols.Add(CompanyAds[1]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/FACEBOOK")]
	private static void DisableFacebookAds()
	{
		AddDefineSymbols.Clear(CompanyAds[1]);

	}

	// Iron Source
	[MenuItem("Finz/Ads & Analytics/Enable Ads/IRON SOURCE")]
	private static void AddironSourceAds()
	{
		AddDefineSymbols.Add(CompanyAds[2]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/IRON SOURCE")]
	private static void DisableIronSourceAds()
	{
		AddDefineSymbols.Clear(CompanyAds[2]);

	}


	// AppLovin
	[MenuItem("Finz/Ads & Analytics/Enable Ads/APPLOVIN")]
	private static void AddAppLovinAds()
	{
		AddDefineSymbols.Add(CompanyAds[3]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/APPLOVIN")]
	private static void DisableAppLovinAds()
	{
		AddDefineSymbols.Clear(CompanyAds[3]);

	}


	// Ad Colony
	[MenuItem("Finz/Ads & Analytics/Enable Ads/ADCOLONY")]
	private static void AddAdColonyAds()
	{
		AddDefineSymbols.Add(CompanyAds[4]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/ADCOLONY")]
	private static void DisableAdColonyAds()
	{
		AddDefineSymbols.Clear(CompanyAds[4]);

	}


	// Tapjoy
	[MenuItem("Finz/Ads & Analytics/Enable Ads/TAPJOY")]
	private static void AddTapjoyAds()
	{
		AddDefineSymbols.Add(CompanyAds[5]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/TAPJOY")]
	private static void DisableTapjoyAds()
	{
		AddDefineSymbols.Clear(CompanyAds[5]);

	}


	// Chartboost
	[MenuItem("Finz/Ads & Analytics/Enable Ads/CHARTBOOST")]
	private static void AddChartboostAds()
	{
		AddDefineSymbols.Add(CompanyAds[6]);

	}

	[MenuItem("Finz/Ads & Analytics/Disable Ads/CHARTBOOST")]
	private static void DisableChartboostAds()
	{
		AddDefineSymbols.Clear(CompanyAds[6]);

	}

	// All Ads
	static int i;
	[MenuItem("Finz/Ads & Analytics/Enable Ads/All")]
	private static void AddAllads()
	{
		i = 0;
		while (i < CompanyAds.Length)
		{
			AddDefineSymbols.Add(CompanyAds[i]);
			i++;
		}

	}
    
	[MenuItem("Finz/Ads & Analytics/Disable Ads/All")]
	private static void DisableAllads()
	{
		i = 0;
		while (i < CompanyAds.Length)
		{
			AddDefineSymbols.Clear(CompanyAds[i]);
			i++;
		}

	}



	[MenuItem("Finz/Version")]
	private static void showDIalog()
	{
		EditorUtility.DisplayDialog("Finz Ads Light | V1","","OK");
	}

}
