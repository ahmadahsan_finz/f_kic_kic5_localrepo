﻿namespace FinzInternetAvailability
{
    /// <summary>
    /// Possible results
    /// </summary>
    public enum ConnectionResult
    {
        Working,
        NetorkCardDisabled,
        CannotReachWebsite
    }
}
