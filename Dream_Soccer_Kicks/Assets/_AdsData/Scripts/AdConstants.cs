﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AdConstants
{

	public enum States
	{

		OnGameOver,

		OnPlaying,

		OnBecomeActive,

		OnPause,

		OnMainMenu,

		OnMoreGames,

		OnHalfSummaryMenu,

		Default,

		OnRewarded,

		OnRewardedInterstitial
	};

	public static States currentState;


	public static string packageName = "games.mini.sports.soccer.revolution";

	public static bool isInternetAvaialble = false , shouldShowRateMenu = false;
	public static bool isfirstTime;

	public static bool isLowEndDevice = false;
	public static bool limitAds = false;

	public static bool sawRewarded = false;
	public static bool sawCompleteRewardedAd = false;
	public static bool isRewardedAdClosed = false;

	public static bool isCallFromShow = false;

	public static bool isInternetWorking = false;
	public static bool isInternetBeingChecked = false;
	public static int maxTryForInternet = 3;
	public static int curTryForInternet = 0;

	public static bool IsAdWasShowing = false;

	public static bool GetInternetStatus()
	{
		
		return isInternetAvaialble && isInternetReachable();
	}


	public static void CheckForInternet()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			shouldShowRateMenu = false;
			shouldShowRateMenu = false;
			isInternetAvaialble = false;
			isInternetWorking = false;
			Debug.Log("===============  Internet Checking Call Not Available =============== ");
		}

		else
		{
			shouldShowRateMenu = true;
			isInternetAvaialble = true;
			isInternetWorking = true;
			Debug.Log("=============== Internet Checking Call Available =============== ");

		}
	}

	private static bool isInternetReachable()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			//shouldShowRateMenu = false;
			//shouldShowRateMenu = false;
			//isInternetAvaialble = false;
			//isInternetWorking = false;
			//Debug.Log("===============  Internet Status Call Not Available =============== ");

			return false;
		}
		else
		{
			//shouldShowRateMenu = true;
			//isInternetAvaialble = true;
			//isInternetWorking = true;
			//Debug.Log("===============  Internet Status Call Available =============== ");

			return true;
		}

	}

	public static bool shouldDisplayAds()
	{

		return PlayerPrefs.GetInt("ADS_KEY", 1) == 1;

	}

	public static void disableAds()
	{
		PlayerPrefs.SetInt("ADS_KEY", 0);

		PlayerPrefs.Save();

		if (AdController.instance)
			AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.BANNER);

	}

	public static void EnableAds()
	{
		PlayerPrefs.SetInt("ADS_KEY", 1);

		PlayerPrefs.Save();

	}

	public static bool shouldDisplayRateMenu()
	{

		return PlayerPrefs.GetInt("RATE_KEY", 1) == 1;

	}

	public static void userHasRatedApp()
	{
		//PluginCallBacksManager.Instance.AdsInApp ();
		PlayerPrefs.SetInt("RATE_KEY", 0);

		PlayerPrefs.Save();

		if (AdController.instance)
			AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.BANNER);

	}



	public static bool checkPackageAppIsPresent(string package)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
	    AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
	    AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
	    AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

	    //take the list of all packages on the device
	    AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledPackages", 0);
	    int num = appList.Call<int>("size");
	    for (int i = 0; i < num; i++)
	    {
		    AndroidJavaObject appInfo = appList.Call<AndroidJavaObject>("get", i);
		    string packageNew = appInfo.Get<string>("packageName");
		    if (packageNew.CompareTo(package) == 0)
		    {
			    return true;
		    }
	    }
#elif UNITY_IOS
		if (shouldDisplayCPMenu())
			return false;
        else return true;
#elif UNITY_EDITOR
		return false;
#endif
		return false;
	}

	public static bool shouldDisplayCPMenu()
	{

		return PlayerPrefs.GetInt("CP_Menu", 1) == 1;

	}


	static bool checkPackageAppIsPresentForTesting(string package)
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject ca = up.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaObject packageManager = ca.Call<AndroidJavaObject>("getPackageManager");

		//take the list of all packages on the device
		AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledPackages", 0);
		int num = appList.Call<int>("size");
		for (int i = 0; i < num; i++)
		{
			AndroidJavaObject appInfo = appList.Call<AndroidJavaObject>("get", i);
			string packageNew = appInfo.Get<string>("packageName");
			if (packageNew.CompareTo(package) == 0)
			{
				return true;
			}
		}
#elif UNITY_IOS && !UNITY_EDITOR
			return false;
#elif UNITY_EDITOR
		return true;
#endif
		return false;
	}

	public static bool isTestingLicenseAvailable()
	{
		if (checkPackageAppIsPresentForTesting("com.finz.license.apk"))
		{
			return true;
		}
		else return false;
	}

	public static bool IsCurrentPackageAvailable(string packageName)
	{
		if (checkPackageAppIsPresentForTesting(packageName))
		{
			return true;
		}
		else return false;
	}



}
