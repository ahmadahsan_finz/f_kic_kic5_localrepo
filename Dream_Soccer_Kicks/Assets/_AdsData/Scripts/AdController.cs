﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using FinzInternetAvailability;
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
#if USE_ADCOLONY
using GoogleMobileAds.Api.Mediation.AdColony;
#endif
#if USE_CHARTBOOST
using GoogleMobileAds.Api.Mediation.Chartboost;
#endif
#if USE_APPLOVIN
using GoogleMobileAds.Api.Mediation.AppLovin;
#endif
#if USE_IRONSOURCE
using GoogleMobileAds.Api.Mediation.IronSource;
#endif
#if USE_TAPJOY
using GoogleMobileAds.Api.Mediation.Tapjoy;
#endif

#if USE_UNITY_ADS
using GoogleMobileAds.Api.Mediation.UnityAds;
#endif

public class AdController : MonoBehaviour
{

    public enum BannerAdTypes
    {
        BANNER, ADAPTIVE, NATIVE
    }

    public enum AdType
    {
        STATIC, INTERSTITIAL, REWARDED, REWARDED_INTERSTITIAL, OPEN_AD
    }



    //========================= REVIEW DIALOG CALLBACKS =========================//

    public delegate void reviewDialog();
    public static event reviewDialog reviewDialogMethod;

    //========================= LOAD SCENE CALLBACKS =========================//

    public delegate void LoadNextScene();
    public static event LoadNextScene loadNextScene;

    //========================= REWARDED INTERSTITIAL CALLBACKS =========================//

    // On rewarded Ad Load
    public delegate void rewardedInterstitialAdLoad();
    public static event rewardedInterstitialAdLoad rewardedInterstitialAdLoadMethod;

    // On rewarded Ad Load Failed
    public delegate void rewardedInterstitialAdLoadFailed();
    public static event rewardedInterstitialAdLoadFailed rewardedInterstitialAdLoadFailedMethod;

    // On rewarded Ad Show 
    public delegate void rewardedInterstitialAdShowing();
    public static event rewardedInterstitialAdShowing rewardedInterstitialAdShowingMethod;

    // On rewarded Ad Show Failed
    public delegate void rewardedInterstitialAdShowingFailed();
    public static event rewardedInterstitialAdShowingFailed rewardedInterstitialAdShowingFailedMethod;

    //========================= REWARDED CALLBACKS =========================//

    public delegate void rewardedVideoWatched();
    public static event rewardedVideoWatched gaveRewardMethod;

    // On rewarded Ad Load
    public delegate void rewardedAdLoad();
    public static event rewardedAdLoad rewardedAdLoadMethod;

    // On rewarded Ad Load Failed
    public delegate void rewardedAdLoadFailed();
    public static event rewardedAdLoadFailed rewardedAdLoadFailedMethod;

    // On rewarded Ad Show 
    public delegate void rewardedAdShowing();
    public static event rewardedAdShowing rewardedAdShowingMethod;

    // On rewarded Ad Show Failed
    public delegate void rewardedAdShowingFailed();
    public static event rewardedAdShowingFailed rewardedAdShowingFailedMethod;


    // On No rewarded Ad
    public delegate void NoRewardedAd();
    public static event NoRewardedAd noRewardedVideoMethod;

    // On rewarded Cancle
    public delegate void cancelRewardedAd();
    public static event cancelRewardedAd cancelRewardedAdMethod;


    //========================= CROSS PROMOTION CALLBACKS =========================//

    public delegate void crossPromotion();
    public static event crossPromotion crossPromotiongMethod;


    //========================= BANNER AD CALLBACKS =========================//

    public delegate void bannerLoadingFailed();
    public static event bannerLoadingFailed bannerLoadingFailedMethod;

    public delegate void bannerLoadingSuccessful();
    public static event bannerLoadingSuccessful bannerLoadingSuccessfulMethod;


    //========================= Adaptive BANNER AD CALLBACKS =========================//

    public delegate void adaptiveBannerLoadingFailed();
    public static event adaptiveBannerLoadingFailed adaptiveBannerLoadingFailedMethod;

    public delegate void adaptiveBannerLoadingSuccessful();
    public static event adaptiveBannerLoadingSuccessful adaptiveBannerLoadingSuccessfulMethod;

    //========================= NATIVE BANNER AD CALLBACKS =========================//

    public delegate void nativeBannerLoadingFailed();
    public static event nativeBannerLoadingFailed nativeBannerLoadingFailedMethod;

    public delegate void nativeBannerShow();
    public static event nativeBannerShow nativeBannerShowMethod;

    //========================= STATIC AD CALLBACKS =========================//

    // On Static Ad Load
    public delegate void staticAdLoad();
    public static event staticAdLoad staticAdLoadMethod;

    // On Static Ad Failed to Load
    public delegate void staticAdLoadFailed();
    public static event staticAdLoadFailed staticAdLoadFailedMethod;

    // On Static Ad Showing
    public delegate void staticAdShowing();
    public static event staticAdShowing staticAdShowingMethod;

    // On Static Ad Showing Failed
    public delegate void staticAdShowingFailed();
    public static event staticAdShowingFailed staticAdShowingFailedMethod;

    // On Static Ad Closed
    public delegate void staticAdClosed();
    public static event staticAdClosed staticAdClosedMethod;


    //========================= VIDEO AD CALLBACKS =========================//

    // On Video Ad Load
    public delegate void videoAdLoad();
    public static event videoAdLoad videoAdLoadMethod;

    // On Video Ad Load Failed
    public delegate void videoAdLoadFailed();
    public static event videoAdLoadFailed videoAdLoadFailedMethod;

    // On Video Ad Showing
    public delegate void videoAdShowing();
    public static event videoAdShowing videoAdShowingMethod;

    // On Video Ad Showing Failed
    public delegate void videoAdShowingFailed();
    public static event videoAdShowingFailed videoAdShowingFailedMethod;

    // On Video Ad Closed
    public delegate void videoAdClosed();
    public static event videoAdClosed videoAdClosedMethod;
    public static AdController instance;


    //========================= Ads Loading CALLBACKS =========================//
    public delegate void AdsLoadingUI();
    public static event AdsLoadingUI adsLoadingUIMethod;


    [HideInInspector]
    public bool loadAllRewardedAds = false;

    [HideInInspector]
    public InappReviewManager iapInstance;


    // Ads Loading Checks
    private bool isStaticAdBeingRequested = false;
    private bool isInterstitialAdBeingRequested = false;
    private bool isRewardedAdBeingRequested = false;
    private bool isRewardedInterstitialAdBeingRequested = false;
    private bool isOpenAdBeingRequested = false;
    private bool isBannerAdBeingRequested = false;
    private bool isAdaptiveBannerBeingRequested = false;
    private bool isNativeBannerAdBeingRequested = false;

    public bool IsBannerAdBeingRequested { get => isBannerAdBeingRequested; set => isBannerAdBeingRequested = value; }
    public bool IsAdaptiveBannerBeingRequested { get => isAdaptiveBannerBeingRequested; set => isAdaptiveBannerBeingRequested = value; }
    public bool IsNativeBannerAdBeingRequested { get => isNativeBannerAdBeingRequested; set => isNativeBannerAdBeingRequested = value; }
    public bool IsStaticAdBeingRequested { get => isStaticAdBeingRequested; set => isStaticAdBeingRequested = value; }
    public bool IsInterstitialAdBeingRequested { get => isInterstitialAdBeingRequested; set => isInterstitialAdBeingRequested = value; }
    public bool IsRewardedAdBeingRequested { get => isRewardedAdBeingRequested; set => isRewardedAdBeingRequested = value; }
    public bool IsRewardedInterstitialAdBeingRequested { get => isRewardedInterstitialAdBeingRequested; set => isRewardedInterstitialAdBeingRequested = value; }
    public bool IsOpenAdBeingRequested { get => isOpenAdBeingRequested; set => isOpenAdBeingRequested = value; }


    // Internet Controlling
    private bool checkInternetDuringGame = false;
    public bool CheckInternetDuringGame { get => checkInternetDuringGame; set => checkInternetDuringGame = value; }


    private bool isStaticAdLoaded = false;
    private bool isInterstitialLoaded = false;
    private bool isRewardedAdLoaded = false;
    private bool isRewardedInterstitialLoaded  = false;

    public bool IsStaticAdLoaded { get => isStaticAdLoaded; set => isStaticAdLoaded = value; }
    public bool IsInterstitialLoaded { get => isInterstitialLoaded; set => isInterstitialLoaded = value; }
    public bool IsRewardedAdLoaded { get => isRewardedAdLoaded; set => isRewardedAdLoaded = value; }
    public bool IsRewardedInterstitialLoaded { get => isRewardedInterstitialLoaded; set => isRewardedInterstitialLoaded = value; }


    // Old Plugins Variables
    int staticAdCount = 1;
    public bool sendAdPaidEvents = false;
    public bool showAdsOnBecomeActive = true;
    public bool Active_DontdestroyOnLoad = true;


    private void Awake()
    {
        instance = this;
        if (Active_DontdestroyOnLoad)
            DontDestroyOnLoad(this);
        Init();

    }

    #region INITIALIZATION
    private void Init()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep; //  to make screen keep active 

        iapInstance = GetComponent<InappReviewManager>();
       
    }
    #endregion

    #region Ads Methods



    #region BANNER AND ADAPTIVE BANNER

    // Banner Ad
    private void ShowBanner()
    {
        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus())
        {
            return;
        }
        RequestBannerAd();
    }

    public bool IsBannerAdAvailable()
    {
        return IsAdmobBannerAvailable();
    }

    private void Hidebanner()
    {
        HideAdmobBannerAd();

    }

    private void DestroyBanner()
    {
        DestroyAdmobBannerAd();
    }



    // Adaptive Banner Ad
    private void ShowAdaptivebanner()
    {
        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus())
        {
            return;
        }
        RequestAdaptiveBannerAd();

    }

    public bool IsAdaptiveBannerAdAvailable()
    {
        return IsAdmobAdaptiveBannerAvailable();
    }

    private void HideAdaptivebanner()
    {
        HideAdaptiveBannerAd();

    }

    private void DestroyAdatvebanner()
    {
        DestroyAdmobAdaptiveAd();
    }


    #endregion

    #region NATIVE BANNER

    private void ShowNativebanner()
    {
        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus())
        {
            return;
        }

        RequestNativeBannerAd();

    }
    public bool IsNativeBannerAdAvailable()
    {
        return IsAdmobNativeBannerAvailable();
    }


    private void HideNativebanner()
    {
        HideAdmobNativeAd();

    }

    private void DestroyNativebanner()
    {
        DestroyAdmobNativeAd();


    }

    #endregion

    #region STATIC AD

    private void ShowStatic()
    {
        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus() || !IsStaticAdAvailable())
        {
            return;
        }

        ShowStaticAd(true);

    }


    private void RequestStatic()
    {
        RequestAndLoadStaticlAd();
    }

    public bool IsStaticAdAvailable()
    {
        return IsStaticAdIsAvailable();
    }

    #endregion

    #region INTERSTITIAL AD

    private void ShowInterstitial()
    {
        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus() || !IsInterstitialAdAvailable())
        {
            return;
        }

        ShowInterstitialAd(true);

    }

    private void RequestInterstitial()
    {
        RequestAndLoadInterstitialAd();

    }

    public bool IsInterstitialAdAvailable()
    {
        return IsInterstitialAdIsAvailable();
    }

    #endregion

    #region REWARDED AD

    private void ShowRewarded()
    {
        if (!AdConstants.GetInternetStatus())
        {
            showNoAdAvailable = false;
            showNoRewardedAdDialog();
            return;

        }
        ShowRewardedAd();
    }


    private void RequestRewarded()
    {
        RequestAndLoadRewardedAd();
    }


    public bool IsRewardedAdAvailable()
    {
        return IsRewardedAdIsAvailable();
    }


    #endregion

    #region REWARDED INTERSTITIAL AD

    private void ShowRewardedInterstitial()
    {
        if (!AdConstants.GetInternetStatus())
        {
            showNoAdAvailable = false;
            showNoRewardedAdDialog();
            return;

        }
        ShowRewardedInterstitialAd();
    }


    private void RequestRewardedInterstitial()
    {
        RequestAndLoadRewardedInterstitialAd();
    }

    public bool IsRewardedInterstitialAdAvailable()
    {
        return IsRewardedInterstitialAdIsAvailable();
    }

    #endregion


    #region Testing

    public bool CanShowLogs()
    {
        return showLogs;
    }

    #endregion

    #endregion


    #region USER CallBacks


    public void LoadNextSceneMethod()
    {
        if (loadNextScene != null)
            loadNextScene();
    }


    public void RewardedInterstitialAdLoaded()
    {
        if (rewardedInterstitialAdLoadMethod != null)
        {
            rewardedInterstitialAdLoadMethod();
        }

    }


    public void RewardedInterstitialAdLoadFailed()
    {
        if (rewardedInterstitialAdLoadFailedMethod != null)
        {
            rewardedInterstitialAdLoadFailedMethod();
        }

    }

    public void RewardedInterstitialAdShowing()
    {
        if (rewardedInterstitialAdShowingMethod != null)
        {
            rewardedInterstitialAdShowingMethod();
        }

    }

    public void RewardedInterstitialAdShowingFailed()
    {
        if (rewardedInterstitialAdShowingFailedMethod != null)
            rewardedInterstitialAdShowingFailedMethod();


    }

    public void RewardedAdLoaded()
    {
        if (rewardedAdLoadMethod != null)
        {
            rewardedAdLoadMethod();
        }

    }


    public void RewardedAdLoadFailed()
    {
        if (rewardedAdLoadFailedMethod != null)
        {
            rewardedAdLoadFailedMethod();
        }

    }

    public void RewardedAdShowing()
    {
        if (rewardedAdShowingMethod != null)
            rewardedAdShowingMethod();


    }

    public void RewardedAdShowingFailed()
    {
        if (rewardedAdShowingFailedMethod != null)
            rewardedAdShowingFailedMethod();


    }

    public void GaveReward()
    {
        if (gaveRewardMethod != null)
            gaveRewardMethod();
    }

    public void NoReward()
    {
        if (noRewardedVideoMethod != null)
            noRewardedVideoMethod();
        else
            ShowNativeToast("NO REWARDED AD AVAILABLE");
    }


    public void CancelRewardedAd()
    {
        if (cancelRewardedAdMethod != null)
        {
            cancelRewardedAdMethod();
        }

    }

    public void BannerAdLoadingSuccessful()
    {
        if (bannerLoadingSuccessfulMethod != null)
        {
            bannerLoadingSuccessfulMethod();
        }

    }

    public void BannerLoadingFailed()
    {
        if (bannerLoadingFailedMethod != null)
        {
            bannerLoadingFailedMethod();
        }

    }

    public void AdaptiveBannerAdLoadingSuccessful()
    {
        if (adaptiveBannerLoadingSuccessfulMethod != null)
        {
            adaptiveBannerLoadingSuccessfulMethod();
        }

    }

    public void AdaptiveBannerLoadingFailed()
    {
        if (adaptiveBannerLoadingFailedMethod != null)
        {
            adaptiveBannerLoadingFailedMethod();
        }

    }

    public void NativeBannerLoadingFailed()
    {
        if (nativeBannerLoadingFailedMethod != null)
        {
            nativeBannerLoadingFailedMethod();
        }

    }

    public void NativeBannerShow()
    {
        if (nativeBannerShowMethod != null)
        {
            nativeBannerShowMethod();
        }

    }


    public void StaticAdLoaded()
    {
        if (staticAdLoadMethod != null)
        {
            staticAdLoadMethod();
        }

    }

    public void StaticAdLoadFailed()
    {
        if (staticAdLoadFailedMethod != null)
        {
            staticAdLoadFailedMethod();
        }

    }


    public void StaticAdShowing()
    {
        if (staticAdShowingMethod != null)
        {
            staticAdShowingMethod();
        }

    }

    public void StaticAdShowingFailed()
    {
        if (staticAdShowingFailedMethod != null)
        {
            staticAdShowingFailedMethod();
        }

    }

    public void StaticAdClosed()
    {
        if (staticAdClosedMethod != null)
        {
            staticAdClosedMethod();
        }

    }



    public void VideoAdLoaded()
    {
        if (videoAdLoadMethod != null)
        {
            videoAdLoadMethod();
        }

    }

    public void VideoAdLoadFailed()
    {
        if (videoAdLoadFailedMethod != null)
        {
            videoAdLoadFailedMethod();
        }

    }

    public void VideoAdShowing()
    {
        if (videoAdShowingMethod != null)
        {
            videoAdShowingMethod();
        }

    }

    public void VideoAdShowingFailed()
    {
        if (videoAdShowingFailedMethod != null)
        {
            videoAdShowingFailedMethod();
        }

    }

    public void VideoAdClosed()
    {
        if (videoAdClosedMethod != null)
        {
            videoAdClosedMethod();
        }

    }

    #endregion


    #region ADS CALLING

    #region Old Plugin Mehtods

    public void ChangeState()
    {

        /*
        if (AdConstants.currentState == AdConstants.States.OnRewarded)
        {
            if (!isRewardedVideoAvailable())
            {
                NoRewardedAdAvailable();

                return;
            }
        }
       // */
        if (AdConstants.currentState != AdConstants.States.OnRewarded && AdConstants.currentState != AdConstants.States.OnMoreGames && !AdConstants.shouldDisplayAds())
        {
            return;
        }


        switch (AdConstants.currentState)
        {
            case AdConstants.States.OnBecomeActive:

                if (showAdsOnBecomeActive)
                {
                    ShowAd(AdType.STATIC);
                }
                break;

            case AdConstants.States.OnMainMenu:

                //FinzDeviceSettings.checkDeviceCompatibility();

                ShowAd(AdType.STATIC);

                break;

            case AdConstants.States.OnPause:

                if (staticAdCount % 2 == 1)
                    ShowAd(AdType.STATIC);
                else ShowAd(AdType.INTERSTITIAL);


                staticAdCount++;

                break;

            case AdConstants.States.OnPlaying:
                break;

            case AdConstants.States.OnHalfSummaryMenu:
                ShowAd(AdType.INTERSTITIAL);

                break;

            case AdConstants.States.OnGameOver:

                //   FinzDeviceSettings.checkDeviceCompatibility();

                ShowAd(AdType.INTERSTITIAL);

                break;

            case AdConstants.States.OnMoreGames:

                break;

            case AdConstants.States.OnRewarded:
                ShowAd(AdType.REWARDED);
                break;

            case AdConstants.States.OnRewardedInterstitial:
                ShowAd(AdType.REWARDED_INTERSTITIAL);
                break;

                //case AdConstants.States.InternetCheck:

                //    if (_internetChecker != null)
                //        _internetChecker.CheckInternetAvailability(true);
                //    //  StartCoroutine(InternetCheck.Instance.CheckForInternetAvailability());

                //    break;
        }

        // FinzAnalysisManager.Instance.AdStates(AdConstants.currentState.ToString());


    }

    public void ShowBannerAd()
    {
        ShowBannerAd(BannerAdTypes.BANNER);
    }

    public void HideBannerAd()
    {
        HideBannerAd(BannerAdTypes.BANNER);
    }

    public void ShowAdaptiveBannerAd()
    {
        ShowBannerAd(BannerAdTypes.ADAPTIVE);
    }

    public void HideAdaptiveBannerAd()
    {
        HideBannerAd(BannerAdTypes.ADAPTIVE);
    }

    public void showNativeBannerAd()
    {
        ShowBannerAd(BannerAdTypes.NATIVE);
    }

    public void hideNativeBannerAd()
    {
        HideBannerAd(BannerAdTypes.NATIVE);
    }



    #endregion

    #region New Plugin Methods

    public void ShowBannerAd(BannerAdTypes type)
    {
        Debug.Log("Banner Logs");
#if UNITY_ANDROID
        if (AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE)
            return;
#endif

        Debug.Log("Banner Logs 2");

        switch (type)
        {
            case BannerAdTypes.BANNER:
                ShowBanner();
                break;

            case BannerAdTypes.ADAPTIVE:
                ShowAdaptivebanner();
                break;

            case BannerAdTypes.NATIVE:
                ShowNativebanner();
                break;
        }
    }


    public void DestroyBannerAd(BannerAdTypes type)
    {
        switch (type)
        {
            case BannerAdTypes.BANNER:
                DestroyBanner();
                break;

            case BannerAdTypes.ADAPTIVE:
                DestroyAdatvebanner();
                break;

            case BannerAdTypes.NATIVE:
                DestroyNativebanner();

                break;
        }
    }


    public void HideBannerAd(BannerAdTypes type)
    {

        switch (type)
        {
            case BannerAdTypes.BANNER:
                HideAdmobBannerAd();

                break;

            case BannerAdTypes.ADAPTIVE:
                HideAdmobAdaptiveBannerAd();

                break;

            case BannerAdTypes.NATIVE:
                HideAdmobNativeAd();

                break;
        }
    }



    public void LoadAd(AdType type)
    {
        switch (type)
        {
            case AdType.STATIC:
#if UNITY_ANDROID
                if (!AdConstants.shouldDisplayAds() || AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {

                    return;
                }
#elif UNITY_IOS
                if (!AdConstants.shouldDisplayAds()|| !AdConstants.GetInternetStatus())
                {
                    return;
                }
#endif
                RequestAndLoadStaticlAd();
                break;

            case AdType.INTERSTITIAL:
#if UNITY_ANDROID
                if (!AdConstants.shouldDisplayAds() || AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    return;
                }


#elif UNITY_IOS
               if (!AdConstants.shouldDisplayAds()|| !AdConstants.GetInternetStatus())
                {
                    return;
                }
#endif

                RequestAndLoadInterstitialAd();
                break;
            case AdType.REWARDED:
#if UNITY_ANDROID
                if (AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    showNoAdAvailable = false;
                    showNoRewardedAdDialog();
                    return;
                }

#elif UNITY_IOS
       if (!AdConstants.GetInternetStatus())
                {
                     showNoAdAvailable = false;
                     showNoRewardedAdDialog();
                    return;
                }
#endif
                RequestAndLoadRewardedAd();
                break;

            case AdType.REWARDED_INTERSTITIAL:
#if UNITY_ANDROID
                if (AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    showNoAdAvailable = false;
                    showNoRewardedAdDialog();

                    return;
                }
#elif UNITY_IOS
                if (!AdConstants.GetInternetStatus())
                {
                     showNoAdAvailable = false;
                     showNoRewardedAdDialog();

                    return;
                }
#endif
                RequestAndLoadRewardedInterstitialAd();

                break;

            case AdType.OPEN_AD:
#if USE_OPENADS
                if (AdConstants.GetInternetStatus() && AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus())
                    RequestAndLoadOpenAd();
#endif

                break;
        }
    }


    public void ShowAd(AdType type)
    {

        switch (type)
        {
            case AdType.STATIC:
#if UNITY_ANDROID
                if (!AdConstants.shouldDisplayAds() || AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    return;
                }

#endif
                ShowStaticAd(true);
                break;

            case AdType.INTERSTITIAL:
#if UNITY_ANDROID
                if (!AdConstants.shouldDisplayAds() || AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    return;
                }


#endif

                ShowInterstitialAd(true);
                break;
            case AdType.REWARDED:
#if UNITY_ANDROID
                if (AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    showNoAdAvailable = false;
                    showNoRewardedAdDialog();
                    return;
                }

#endif
                ShowRewardedAd();
                break;

            case AdType.REWARDED_INTERSTITIAL:
#if UNITY_ANDROID
                if (AdsLoadSequence[currentLoadIndex] != AdLoadStates.NONE || !AdConstants.GetInternetStatus())
                {
                    showNoAdAvailable = false;
                    showNoRewardedAdDialog();

                    return;
                }
#endif
                ShowRewardedInterstitialAd();

                break;

            case AdType.OPEN_AD:
#if USE_OPENADS
                if (AdConstants.shouldDisplayAds() || AdConstants.GetInternetStatus())
                     ShowAdIfAvailable();
#endif

                break;
        }

    }


    public void ShowAdsLoadingUI()
    {
        Instantiate(Resources.Load("AdsLoadingUI"), this.transform);
    }


    public void DestroyAdsLoadingUI()
    {
        if (adsLoadingUIMethod != null)
        {
            adsLoadingUIMethod();
        }
    }



    public void ShowNativeToast(string message)
    {
        ShowToast(message);
    }

    #endregion

    #endregion

    #region RATE US

    public void PromptRateMenu(bool ratePopUp = false)
    {
        // Show Rate us here
#if UNITY_IOS
        if (AdConstants.GetInternetStatus())

        {
	        
	        if (UnityEngine.iOS.Device.RequestStoreReview())
	        {
                if(ratePopUp) // this is for controlling time scale
		            Instantiate(Resources.Load("IOSRateMenuSupport"));
		        
	        }
        }
#elif UNITY_ANDROID //&& !UNITY_EDITOR

        if (AdConstants.shouldDisplayRateMenu())
        {
            DestroyNativebanner();
#if !SHOW_NATIVE_RATE
            if (reviewDialogMethod != null)
                reviewDialogMethod();
#elif SHOW_NATIVE_RATE
            if (reviewDialogMethod != null)
            {
                reviewDialogMethod();
            }
            else
            {
                if (ratePopUp)
                {
#if !UNITY_EDITOR
                        Time.timeScale = 0;
#endif

                }
                //  iapInstance.showRateMenu();
            }
#endif

            //  AdConstants.userHasRatedApp();
        }

#endif


    }

    #endregion


    #region Device Settings


    public void LowDeviceCheck()
    {
#if UNITY_ANDROID && !UNITY_EDITOR


        if (SystemInfo.operatingSystem.Contains("Android OS 8.1"))
        {
            AdConstants.isLowEndDevice = true;

            Debug.Log("---------its 8.1 device---------");
        }

        if (SystemInfo.operatingSystem.Contains("Android OS 8.1") && SystemInfo.systemMemorySize < 1024)
        {
            AdConstants.isLowEndDevice = true;
            AdConstants.limitAds = true;

            Debug.Log("---------its low ended 8.1 device---------");
        }
        if (SystemInfo.systemMemorySize < 1024 || SystemInfo.graphicsMemorySize < 512 || SystemInfo.batteryLevel < 0.2f)
        {
            AdConstants.limitAds = true;
            AdConstants.isLowEndDevice = true;
            Debug.Log("---------its a Low End Device---------");
        }



#endif



    }

    [ContextMenu("Limit Ads")]
    public void LimitDevice()
    {
        AdConstants.limitAds = true;
    }

    [ContextMenu("unLimit Ads")]
    public void unLimitDevice()
    {
        AdConstants.limitAds = false;
    }




    #endregion



    #region ADMOB AREA





    private AdController.AdType cur_AdType;
    private AdController.BannerAdTypes cur_BannerType;

    #region EditorMethod

    public static string androidID, iosId;
    public void SetAdmobIds()
    {
#if UNITY_EDITOR 
        androidID = ANDROID_AppID;
        iosId = IOS_AppID;
        FinzAdmobPatch.SetAdmobAppID(ANDROID_AppID, IOS_AppID);
        UnityEditor.EditorUtility.SetDirty(this);

        FinzAdmobPatch.AppIds idClass = new FinzAdmobPatch.AppIds();
        idClass.appID_Android = ANDROID_AppID;
        idClass.appID_IOS = IOS_AppID;

        string jsonString = JsonUtility.ToJson(idClass);
        File.WriteAllText(Application.dataPath + "/GoogleMobileAds/Resources/AppIds.json", jsonString);


#endif

    }


    public static string GetAndroidAppId()
    {
        return androidID;
    }

    public static string GetIOSAppId()
    {
        return iosId;
    }

    #endregion
    private BannerView bannerView, adaptiveBannerView, nativeBannerView;
    private InterstitialAd interstitialAd, staticAd;
    private RewardedAd rewardedAd;
    private RewardedInterstitialAd rewardedInterstitialAd;
#if USE_OPENADS
    private AppOpenAd openAd;
#endif
    private float deltaTime;

    [ShowIf("showAndroidIds")]
    private bool showAndroidIds = false;
    [ShowIf("showAndroidIds")]
    [Title("========= ANDROID IDS =========", titleAlignment: TitleAlignments.Centered)]
    [InlineButton("SetAdmobIds")]
    public string ANDROID_AppID;
    [ShowIf("showAndroidIds")]
    public string ANDROID_Banner;
    [ShowIf("showAndroidIds")]
    public string ANDROID_NativeBanner;
    [ShowIf("showAndroidIds")]
    public string ANDROID_Static;
    [ShowIf("showAndroidIds")]
    public string ANDROID_Interstitial;
    [ShowIf("showAndroidIds")]
    public string ANDROID_Rewarded;
    [ShowIf("showAndroidIds")]
    public string ANDROID_Rewarded_Interstitial;
#if USE_OPENADS
    [ShowIf("showAndroidIds")]
    public string ANDROID_OPEN_ADS;
#endif

    [ShowIf("showIOSIds")]
    private bool showIOSIds = false;
    [ShowIf("showIOSIds")]
    [Title("=========== IOS IDS ===========", titleAlignment: TitleAlignments.Centered)]
    [InlineButton("SetAdmobIds")]
    public string IOS_AppID;
    [ShowIf("showIOSIds")]
    public string IOS_Banner;
    [ShowIf("showIOSIds")]
    public string IOS_NativeBanner;
    [ShowIf("showIOSIds")]
    public string IOS_Static;
    [ShowIf("showIOSIds")]
    public string IOS_Interstitial;
    [ShowIf("showIOSIds")]
    public string IOS_Rewarded;
    [ShowIf("showIOSIds")]
    public string IOS_Rewarded_Interstitial;
#if USE_OPENADS
    [ShowIf("showIOSIds")]
    public string IOS_OPEN_ADS;
#endif
    public enum AdLoadStates
    {
        REWARDED, REWARDED_INTERSTITIAL, STATIC, INTERSTITIAL, OPEN_AD, NONE
    }
    [Title("======= ADS Load Sequence Settings =======", titleAlignment: TitleAlignments.Centered)]
    [HideInInspector]
    public bool showAdSequence = false;
    [ShowIf("showAdSequence")]
    [HideInInspector]
    public List<AdLoadStates> AdsLoadSequence = new List<AdLoadStates>();
    public static int currentLoadIndex = 0;

    public enum InternetStates
    {
        OnceOnGameStart,
        CheckDuringGame
    }

    [Title("========= Testing =========", titleAlignment: TitleAlignments.Centered)]
    public bool showTestingSettings;
    [ShowIf("showTestingSettings")]
    public string android_TestingDeviceId, ios_TestingDeviceId;
    [ShowIf("showTestingSettings")]
    public bool showLogs = true;

    [Title("========= Banner Settings =========", titleAlignment: TitleAlignments.Centered)]
    public bool showBannerSettings;
    [ShowIf("showBannerSettings")]
    [Title("========= Banner Ad =========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showBannerSettings")]
    public AdPosition banner_Position;
    [ShowIf("showBannerSettings")]
    public AdSize Banner_adSize;
    [ShowIf("showBannerSettings")]
    public bool HasSmartbanner;

    [Title("========= Adaptive Banner Ad =========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showBannerSettings")]
    public AdPosition adaptiveBannerPosition;
    [ShowIf("showBannerSettings")]
    public AdSize adaptiveBanner_Adsize;

    [Title("========= Native Banner Ad =========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showBannerSettings")]
    public AdPosition nativebannerPosition;
    [ShowIf("showBannerSettings")]
    [HideInInspector]
    public bool AdaptiveFullWidht = true;
    [HideInInspector]
    [ShowIf("showBannerSettings")]
    public int widht_AdaptiveBanner = 250;



    [Title("========= Rewarded Ads Settings =========", titleAlignment: TitleAlignments.Centered)]
    public bool showRewardedAdsSettings;
    [ShowIf("showRewardedAdsSettings")]
    public enum RewardsMethod { Update, OnApplicationPause, OnGaveReward }
    public RewardsMethod rewardMethod;

    [Title("======== Callbacks Settings Do not Change ========", titleAlignment: TitleAlignments.Centered)]
    public bool showCallBacks;

    [Title("======== Banner Ads Callbacks ========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showbannerCallbacks;
    [ShowIf("showbannerCallbacks")]
    public UnityEvent OnBannerLoadedEvent;
    [ShowIf("showbannerCallbacks")]
    public UnityEvent OnBannerFailedToLoadEvent;
    [ShowIf("showbannerCallbacks")]
    public UnityEvent OnBannerOpeningEvent;

    [Title("======== Adaptive Banner Ads Callbacks ========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showAdaptivebannerCallbacks;
    [ShowIf("showAdaptivebannerCallbacks")]
    public UnityEvent OnAdaptiveBannerLoadedEvent;
    [ShowIf("showAdaptivebannerCallbacks")]
    public UnityEvent OnAdaptiveBannerFailedToLoadEvent;
    [ShowIf("showAdaptivebannerCallbacks")]
    public UnityEvent OnAdaptiveBannerOpeningEvent;

    [Title("======== Native Banner Ads Callbacks ========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showNativebannerCallbacks;
    [ShowIf("showNativebannerCallbacks")]
    public UnityEvent OnNativeBannerLoadedEvent;
    [ShowIf("showNativebannerCallbacks")]
    public UnityEvent OnNativeBannerFailedToLoadEvent;
    [ShowIf("showNativebannerCallbacks")]
    public UnityEvent OnNativeBannerOpeningEvent;

    [Title("========== Static Ads Callbacks ==========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showStaticAdCallbacks;
    [ShowIf("showStaticAdCallbacks")]
    public UnityEvent OnStaticAdLoadedEvent;
    [ShowIf("showStaticAdCallbacks")]
    public UnityEvent OnStaticAdFailedToLoadEvent;
    [ShowIf("showStaticAdCallbacks")]
    public UnityEvent OnStaticAdOpeningEvent;
    [ShowIf("showStaticAdCallbacks")]
    public UnityEvent OnStaticAdFailedToShowEvent;
    [ShowIf("showStaticAdCallbacks")]
    public UnityEvent OnStaticAdClosedEvent;

    [Title("========== Interstitial Ads Callbacks ==========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showInterstitialAdCallbacks;
    [ShowIf("showInterstitialAdCallbacks")]
    public UnityEvent OnInterstitialAdLoadedEvent;
    [ShowIf("showInterstitialAdCallbacks")]
    public UnityEvent OnInterstitialAdFailedToLoadEvent;
    [ShowIf("showInterstitialAdCallbacks")]
    public UnityEvent OnInterstitialAdOpeningEvent;
    [ShowIf("showInterstitialAdCallbacks")]
    public UnityEvent OnInterstitialAdFailedToShowEvent;
    [ShowIf("showInterstitialAdCallbacks")]
    public UnityEvent OnInterstitialAdClosedEvent;


    [Title("========== Open Ads Callbacks ==========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showOpenAdCallbacks;
    [ShowIf("showOpenAdCallbacks")]
    public UnityEvent OnOpenAdLoadedEvent;
    [ShowIf("showOpenAdCallbacks")]
    public UnityEvent OnOpenAdFailedToLoadEvent;
    [ShowIf("showOpenAdCallbacks")]
    public UnityEvent OnOpenAdOpeningEvent;
    [ShowIf("showOpenAdCallbacks")]
    public UnityEvent OnOpenAdFailedToShowEvent;
    [ShowIf("showOpenAdCallbacks")]
    public UnityEvent OnOpenAdClosedEvent;


    [Title("========== Rewarded Ads Callbacks ==========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showRewardedAdCallbacks;
    [ShowIf("showRewardedAdCallbacks")]
    public UnityEvent OnRewardedAdLoadedEvent;
    [ShowIf("showRewardedAdCallbacks")]
    public UnityEvent OnRewardedAdFailedToLoadEvent;
    [ShowIf("showRewardedAdCallbacks")]
    public UnityEvent OnRewardedAdOpeningEvent;
    [ShowIf("showRewardedAdCallbacks")]
    public UnityEvent OnRewardedAdFailedToShowEvent;
    [ShowIf("showRewardedAdCallbacks")]
    public UnityEvent OnRewardedAdClosedEvent;
    [ShowIf("showRewardedAdCallbacks")]
    public UnityEvent OnRewardedAdEarnedRewardEvent;

    [Title("========== Rewarded Interstitial Ads Callbacks ==========", titleAlignment: TitleAlignments.Centered)]
    [ShowIf("showCallBacks")]
    public bool showRewardedInterstitialAdCallbacks;
    [ShowIf("showRewardedInterstitialAdCallbacks")]
    public UnityEvent OnRewardedInterstitialAdLoadedEvent;
    [ShowIf("showRewardedInterstitialAdCallbacks")]
    public UnityEvent OnRewardedInterstitialAdFailedToLoadEvent;
    [ShowIf("showRewardedInterstitialAdCallbacks")]
    public UnityEvent OnRewardedInterstitialAdOpeningEvent;
    [ShowIf("showRewardedInterstitialAdCallbacks")]
    public UnityEvent OnRewardedInterstitialAdFailedToShowEvent;
    [ShowIf("showRewardedInterstitialAdCallbacks")]
    public UnityEvent OnRewardedInterstitialAdClosedEvent;
    [ShowIf("showRewardedInterstitialAdCallbacks")]
    public UnityEvent OnRewardedInterstitialAdEarnedRewardEvent;


    #region UNITY MONOBEHAVIOR METHODS

    private void OnValidate()
    {
#if UNITY_ANDROID
        showAndroidIds = true;
        showIOSIds = false;
#elif UNITY_IOS

        showAndroidIds = false;
        showIOSIds = true;
#endif
    }

    void Start()
    {
        LowDeviceCheck();

        AdConstants.isInternetBeingChecked = true;
        FinzInternetAvailability.Network.IsAvailable(OnInternetProcessComplete);

    }

    void OnInternetProcessComplete(ConnectionResult connectionResult)
    {

        //if connection result is "Working" user has Internet access not just his card enabled
        if (connectionResult == ConnectionResult.Working)
        {
            AdConstants.isInternetAvaialble = true;

            Debug.Log("------Internet Available------");
            FinzAnalysisManager.Instance.InternetStatus = "INTERNET AVAILABLE";
        }
        else
        {
            AdConstants.isInternetAvaialble = false;
            FinzAnalysisManager.Instance.InternetStatus = "NO INTERNET";
        }


        AdConstants.shouldShowRateMenu = AdConstants.isInternetAvaialble;

        LoadNextSceneMethod();
        AdConstants.isInternetBeingChecked = false;
        InitializeAdmob();


    }

    public void InitializeAdmob()
    {
       
#if UNITY_EDITOR
        //rewardMethod = RewardsMethod.OnGaveReward;
#endif


        MobileAds.SetiOSAppPauseOnBackground(true);

        List<String> deviceIds = new List<String>() { AdRequest.TestDeviceSimulator };

        // Add some test device IDs (replace with your own device IDs).
        //  2021 / 04 / 27 12:03:16.929 25413 25413 Info Ads Use RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("8401D114203BAF22971ACD34B7751F65")) to get test ads on this device.

#if UNITY_IPHONE
        deviceIds.Add(ios_TestingDeviceId); // Iphone 7 plus
#elif UNITY_ANDROID
        deviceIds.Add(android_TestingDeviceId); // Oppo test Id

#endif

#if USE_ADCOLONY
        AdColonyAppOptions.SetGDPRRequired(true);
        AdColonyAppOptions.SetGDPRConsentString("1");
#endif

#if USE_CHARTBOOST
        Chartboost.AddDataUseConsent(CBGDPRDataUseConsent.Behavioral);
#endif

#if USE_APPLOVIN
    AppLovin.SetHasUserConsent(true);
#endif

#if USE_IRONSOURCE
        IronSource.SetConsent(true);
#endif

#if USE_TAPJOY
        Tapjoy.SetUserConsent("1");
        Tapjoy.SubjectToGDPR(true);
#endif

#if USE_UNITY_ADS
        UnityAds.SetGDPRConsentMetaData(true);
#endif

        // Configure TagForChildDirectedTreatment and test device IDs.
        RequestConfiguration requestConfiguration =
            new RequestConfiguration.Builder()
            .SetTagForChildDirectedTreatment(TagForChildDirectedTreatment.Unspecified)
            .SetTestDeviceIds(deviceIds).build();

        MobileAds.SetRequestConfiguration(requestConfiguration);


        AdsLoadSequence = new List<AdLoadStates>();
        AdsLoadSequence.Add(AdLoadStates.REWARDED);
        AdsLoadSequence.Add(AdLoadStates.STATIC);
#if USE_OPENADS
        AdsLoadSequence.Add(AdLoadStates.OPEN_AD);
#endif
        AdsLoadSequence.Add(AdLoadStates.INTERSTITIAL);
        AdsLoadSequence.Add(AdLoadStates.REWARDED_INTERSTITIAL);

        // Initialize the Google Mobile Ads SDK.
        if (AdConstants.GetInternetStatus())
        {
            //#if UNITY_EDITOR
            //             MobileAds.Initialize(HandleInitCompleteAction);
            //#else
            // Init_AdsLoadSequence();
            //LoadAds();
            //#endif
            Init_AdsLoadSequence();
        }
        else
        {
            Debug.Log("------ ADS INITIALIZING FAILED -------");

            Init_AdsLoadSequence();
            //LoadAds();

        }
    }


    private void Update()
    {

        if (rewardMethod == RewardsMethod.Update)
        {

            if (AdConstants.isRewardedAdClosed)
                {

                    if (AdConstants.sawRewarded)
                    {
                        GaveReward();
                        AdConstants.sawRewarded = false;
                    }
                    else
                    {
                        CancelRewardedAd();
                        AdConstants.sawRewarded = false;
                    }

                    AdConstants.isRewardedAdClosed = false;
                }

        }
    }


    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if (rewardMethod == RewardsMethod.OnApplicationPause)
            {
                if (AdConstants.isRewardedAdClosed)
                {
                    StartCoroutine(DecideForReward());

                }
            }
#if USE_OPENADS
   
            ShowAdIfAvailable();
#elif !USE_OPENADS

            //if (!AdConstants.IsAdWasShowing && AdConstants.curTryForInternet >= AdConstants.maxTryForInternet)
            //{
            //    Debug.Log("---------- Internet Tries Reset ----------");
            //    AdConstants.curTryForInternet = 0;
            //}
            //else
            if (AdConstants.IsAdWasShowing)
            {
                AdConstants.IsAdWasShowing = false;
            }
#endif
        }
    }




    private void HandleInitCompleteAction(InitializationStatus initstatus)
    {
        // Callbacks from GoogleMobileAds are not guaranteed to be called on
        // main thread.
        // In this example we use MobileAdsEventExecutor to schedule these calls on
        // the next Update() loop.
        MobileAdsEventExecutor.ExecuteInUpdate(() =>
        {
            Debug.Log("------ ADS INITIALIZING COMPLETED SUCCESSFULLY -------");

            //if (AdController.loadNextScene != null)
            //    AdController.loadNextScene();
            Init_AdsLoadSequence();
            //LoadAds();
        });
    }


#region Ads initailization

    private void Init_AdsLoadSequence()
    {

        if (AdConstants.GetInternetStatus())
        {
            if (AdConstants.limitAds || AdConstants.isLowEndDevice)
            {
                AdsLoadSequence = new List<AdLoadStates>();
                AdsLoadSequence.Add(AdLoadStates.REWARDED);
                AdsLoadSequence.Add(AdLoadStates.REWARDED_INTERSTITIAL);
            }
        }
        else
        {
            AdsLoadSequence = new List<AdLoadStates>();
        }
        // Checking for null ids
#if UNITY_ANDROID
        if (string.IsNullOrEmpty(ANDROID_Static) && AdsLoadSequence.Exists(x => x == AdLoadStates.STATIC))
        {
            AdsLoadSequence.Remove(AdLoadStates.STATIC);
        }
#elif UNITY_IOS
        if (string.IsNullOrEmpty(IOS_Static) && AdsLoadSequence.Exists(x => x == AdLoadStates.STATIC))
        {
            AdsLoadSequence.Remove(AdLoadStates.STATIC);
        }
#endif
        // Checking for null ids
#if UNITY_ANDROID
        if (string.IsNullOrEmpty(ANDROID_Interstitial) && AdsLoadSequence.Exists(x => x == AdLoadStates.INTERSTITIAL))
        {
            AdsLoadSequence.Remove(AdLoadStates.INTERSTITIAL);
        }
#elif UNITY_IOS

        if (string.IsNullOrEmpty(IOS_Interstitial) && AdsLoadSequence.Exists(x => x == AdLoadStates.INTERSTITIAL))
        {
            AdsLoadSequence.Remove(AdLoadStates.INTERSTITIAL);
        }
#endif
        // Checking for null ids
#if UNITY_ANDROID
        if (string.IsNullOrEmpty(ANDROID_Rewarded) && AdsLoadSequence.Exists(x => x == AdLoadStates.REWARDED))
        {
            AdsLoadSequence.Remove(AdLoadStates.REWARDED);
        }
#elif UNITY_IOS
        if (string.IsNullOrEmpty(IOS_Rewarded) && AdsLoadSequence.Exists(x => x == AdLoadStates.REWARDED))
        {
            AdsLoadSequence.Remove(AdLoadStates.REWARDED);
        }
#endif


        // Checking for null ids
#if UNITY_ANDROID
        if (string.IsNullOrEmpty(ANDROID_Rewarded_Interstitial) && AdsLoadSequence.Exists(x => x == AdLoadStates.REWARDED_INTERSTITIAL))
        {
            AdsLoadSequence.Remove(AdLoadStates.REWARDED_INTERSTITIAL);
        }
#elif UNITY_IOS
        if (string.IsNullOrEmpty(IOS_Rewarded_Interstitial) && AdsLoadSequence.Exists(x => x == AdLoadStates.REWARDED_INTERSTITIAL))
        {
            AdsLoadSequence.Remove(AdLoadStates.REWARDED_INTERSTITIAL);
        }
#endif

#if USE_OPENADS
        // Checking for null ids
#if UNITY_ANDROID
        if (string.IsNullOrEmpty(ANDROID_OPEN_ADS) && AdsLoadSequence.Exists(x => x == AdLoadStates.OPEN_AD))
        {
            AdsLoadSequence.Remove(AdLoadStates.OPEN_AD);
        }
#elif UNITY_IOS

        if (string.IsNullOrEmpty(IOS_OPEN_ADS) && AdsLoadSequence.Exists(x => x == AdLoadStates.OPEN_AD))
        {
            AdsLoadSequence.Remove(AdLoadStates.OPEN_AD);
        }
#endif

#endif

        AdsLoadSequence.Add(AdLoadStates.NONE);
        currentLoadIndex = 0;
        LoadAds();


    }

    void LoadAds()
    {
        StartCoroutine(LoadingAds());
      //  CheckAndLoadAd();
    }


    IEnumerator LoadingAds()
    {
        if (!AdConstants.limitAds)
        {
            yield return new WaitForSecondsRealtime(2f);
            RequestAndLoadRewardedAd();
            yield return new WaitUntil(() => isRewardedAdLoaded == true);
        }
        yield return new WaitForSecondsRealtime(2f);
        currentLoadIndex = 0;
        AdsLoadSequence = new List<AdLoadStates>();
        AdsLoadSequence.Add(AdLoadStates.NONE);
        if (!AdConstants.limitAds && !AdConstants.isLowEndDevice)
        {
            yield return new WaitForSecondsRealtime(2f);
            RequestAndLoadStaticlAd();
            //yield return new WaitUntil(() => isStaticAdLoaded == true);
            yield return new WaitForSecondsRealtime(2f);
            RequestAndLoadInterstitialAd();
            yield return new WaitForSecondsRealtime(2f);
            RequestAndLoadRewardedInterstitialAd();
        }
    
        //yield return new WaitUntil(() => isRewardedInterstitialLoaded == true);

        //yield return new WaitUntil(() => isInterstitialLoaded == true);



    }


#region HELPER METHODS

    private AdRequest CreateAdRequest()
    {
        return new AdRequest.Builder()
            .AddKeyword("unity-admob-sample")
            .Build();
    }

#endregion

    public bool checkForAdsSequenceIndex()
    {
        return currentLoadIndex >= 0 && currentLoadIndex < AdsLoadSequence.Count ? true : false;
    }


    public void CheckAndLoadAd()
    {
        if (currentLoadIndex < 0)
            return;

        switch (AdsLoadSequence[currentLoadIndex])
        {
            case AdLoadStates.REWARDED:
                RequestAndLoadRewardedAd();
                break;

            case AdLoadStates.REWARDED_INTERSTITIAL:
                RequestAndLoadRewardedInterstitialAd();

                break;

            case AdLoadStates.STATIC:
                RequestAndLoadStaticlAd();

                break;

            case AdLoadStates.INTERSTITIAL:
                RequestAndLoadInterstitialAd();

                break;

            case AdLoadStates.OPEN_AD:
#if USE_OPENADS
                RequestAndLoadOpenAd();
#endif
                break;

        }
    }

    public void IncrementLoadIndex()
    {
        currentLoadIndex = currentLoadIndex < AdsLoadSequence.Count - 1 ? currentLoadIndex + 1 : AdsLoadSequence.Count - 1;

    }

#endregion

#endregion

#region BANNER ADS

    public void RequestBannerAd()
    {        // These ad units are configured to always serve test ads.
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_Banner;
#elif UNITY_IPHONE
        string adUnitId = IOS_Banner;
#else
        string adUnitId = "unexpected_platform";
#endif

        if (string.IsNullOrEmpty(adUnitId) || IsBannerAdBeingRequested)
            return;

        cur_BannerType = AdController.BannerAdTypes.BANNER;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING BANNER AD -------");

        // Clean up banner before reusing
        if (bannerView != null)
        {
            ShowAdmobBannerAd();
            return;
          // bannerView.Destroy();
        }

        if (HasSmartbanner)
            Banner_adSize = AdSize.SmartBanner;
        else
            Banner_adSize = AdSize.Banner;

        IsBannerAdBeingRequested = true;
        // Create a 320x50 banner at top of the screen
        bannerView = new BannerView(adUnitId, Banner_adSize, banner_Position);

        

        // Add Event Handlers
        bannerView.OnAdLoaded += (sender, args) => OnBannerLoadedEvent.Invoke();
        bannerView.OnAdFailedToLoad += (sender, args) => OnBannerFailedToLoadEvent.Invoke();
        bannerView.OnAdOpening += (sender, args) => OnBannerOpeningEvent.Invoke();
        if (sendAdPaidEvents)
        {
            bannerView.OnPaidEvent += (sender, args) =>
            {
                MobileAdsEventExecutor.ExecuteInUpdate(() =>
                {
                    if (AdController.instance && AdController.instance.CanShowLogs())
                        Debug.Log("------ PAID EVENT BannerAd -------");
#if USE_ADJUST_SDK
                AdValue adValue = args.AdValue;
                // send ad revenue info to Adjust
                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                Adjust.trackAdRevenue(adRevenue);
#endif
            });
            };
        }
        // Load a banner ad
        bannerView.LoadAd(CreateAdRequest());

    }

    public void Callback_BannerAdLoaded()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ BANNER AD LOADED -------");

        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("BANNER");

        BannerAdLoadingSuccessful();
        IsBannerAdBeingRequested = false;

    }

    public void Callback_BannerAdLoadFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED BANNER AD -------");

        BannerLoadingFailed();
        IsBannerAdBeingRequested = false;

    }

    public void Callback_BannerAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING BANNER AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("BANNER");
        IsBannerAdBeingRequested = false;

    }



    public bool IsAdmobBannerAvailable()
    {
        return bannerView != null ? true : false;
    }

    public void HideAdmobBannerAd()
    {
        if (bannerView != null)
        {
            bannerView.Hide();
        }

    }

    public void ShowAdmobBannerAd()
    {
        if (bannerView != null)
        {
            bannerView.Show();
        }

    }

    public void DestroyAdmobBannerAd()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }

    }

#endregion

#region ADAPTIVE BANNER ADS

    public void RequestAdaptiveBannerAd()
    {
        AdaptiveFullWidht = true;
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_Banner;
#elif UNITY_IPHONE
        string adUnitId = IOS_Banner;
#else
        string adUnitId = "unexpected_platform";
#endif

        if (string.IsNullOrEmpty(adUnitId) || IsAdaptiveBannerBeingRequested)
            return;

        cur_BannerType = AdController.BannerAdTypes.ADAPTIVE;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING ADAPTIVE BANNER AD -------");

        // These ad units are configured to always serve test ads.


        // Clean up banner before reusing
        if (adaptiveBannerView != null)
        {
            ShowAdmobAdaptiveBannerAd();
            return;
            //adaptiveBannerView.Destroy();
        }

        AdSize adaptiveSize =
                     AdSize.GetLandscapeAnchoredAdaptiveBannerAdSizeWithWidth(AdaptiveFullWidht ? AdSize.FullWidth : widht_AdaptiveBanner);

        isAdaptiveBannerBeingRequested = true;

        // Create a 320x50 banner at top of the screen
        adaptiveBannerView = new BannerView(adUnitId, adaptiveSize, adaptiveBannerPosition);

        // Create a 320x50 banner at top of the screen

        // Add Event Handlers
        adaptiveBannerView.OnAdLoaded += (sender, args) => OnAdaptiveBannerLoadedEvent.Invoke();
        adaptiveBannerView.OnAdFailedToLoad += (sender, args) => OnAdaptiveBannerFailedToLoadEvent.Invoke();
        adaptiveBannerView.OnAdOpening += (sender, args) => OnAdaptiveBannerOpeningEvent.Invoke();
        adaptiveBannerView.OnPaidEvent += (sender, args) =>
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                if (AdController.instance && AdController.instance.CanShowLogs())
                    Debug.Log("------ PAID EVENT AdaptiveBannerAd -------");
#if USE_ADJUST_SDK
                 AdValue adValue = args.AdValue;
                 // send ad revenue info to Adjust
                 AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                 adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                 Adjust.trackAdRevenue(adRevenue);
#endif
            });
        };


        // Load a banner ad
        adaptiveBannerView.LoadAd(CreateAdRequest());

    }




    public void Callback_AdaptiveBannerAdLoaded()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ ADAPTIVE BANNER AD LOADED -------");

        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("ADAPTIVE_BANNER");

        AdaptiveBannerAdLoadingSuccessful();

        isAdaptiveBannerBeingRequested = false;

    }

    public void Callback_AdaptiveBannerAdLoadFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED ADAPTIVE BANNER AD -------");
        AdaptiveBannerLoadingFailed();
        isAdaptiveBannerBeingRequested = false;

    }

    public void Callback_AdaptiveBannerAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING ADAPTIVE BANNER AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("ADAPTIVE_BANNER");

        isAdaptiveBannerBeingRequested = false;

    }



    public void DestroyAdmobAdaptiveAd()
    {
        if (adaptiveBannerView != null)
        {
            adaptiveBannerView.Destroy();
        }

    }

    public bool IsAdmobAdaptiveBannerAvailable()
    {
        return adaptiveBannerView != null ? true : false;
    }

    public void HideAdmobAdaptiveBannerAd()
    {
        if (adaptiveBannerView != null)
        {
            adaptiveBannerView.Hide();
        }

    }


    public void ShowAdmobAdaptiveBannerAd()
    {
        if (adaptiveBannerView != null)
        {
            adaptiveBannerView.Show();
        }

    }


#endregion

#region NATIVE BANNER ADS

    public void RequestNativeBannerAd(int width = 200, AdPosition adaptivBanner_Pos = AdPosition.Top)
    {
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_NativeBanner;
#elif UNITY_IPHONE
        string adUnitId = IOS_NativeBanner;
#else
        string adUnitId = "unexpected_platform";
#endif


        if (string.IsNullOrEmpty(adUnitId) || IsNativeBannerAdBeingRequested)
            return;


        cur_BannerType = AdController.BannerAdTypes.NATIVE;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING NATIVE BANNER AD -------");
        // These ad units are configured to always serve test ads.


        // Clean up banner before reusing
        if (nativeBannerView != null)
        {
            ShowAdmobNativeAd();
            return;
           // nativeBannerView.Destroy();
        }

        isNativeBannerAdBeingRequested = true;


        nativeBannerView = new BannerView(adUnitId, AdSize.MediumRectangle, nativebannerPosition);

        // Create a 320x50 banner at top of the screen

        // Add Event Handlers
        nativeBannerView.OnAdLoaded += (sender, args) => OnNativeBannerLoadedEvent.Invoke();
        nativeBannerView.OnAdFailedToLoad += (sender, args) => OnNativeBannerFailedToLoadEvent.Invoke();
        nativeBannerView.OnAdOpening += (sender, args) => OnNativeBannerOpeningEvent.Invoke();
        if (sendAdPaidEvents)
        {
            nativeBannerView.OnPaidEvent += (sender, args) =>
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                if (AdController.instance && AdController.instance.CanShowLogs())
                    Debug.Log("------ PAID EVENT NativeBannerAd -------");
#if USE_ADJUST_SDK
                AdValue adValue = args.AdValue;
                // send ad revenue info to Adjust
                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                Adjust.trackAdRevenue(adRevenue);
#endif
            });
        };
        }
        // Load a banner ad
        nativeBannerView.LoadAd(CreateAdRequest());
    }


    public void Callback_NativeBannerAdLoaded()
    {

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ NATIVE BANNER AD LOADED -------");

        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("NATIVE_BANNER");

        NativeBannerShow();
        isNativeBannerAdBeingRequested = false;

    }

    public void Callback_NativeBannerAdLoadFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED NATIVE BANNER AD -------");

        NativeBannerLoadingFailed();
        isNativeBannerAdBeingRequested = false;
    }

    public void Callback_NativeBannerAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING NATIVE BANNER AD -------");

        isNativeBannerAdBeingRequested = false;
    }


    public bool IsAdmobNativeBannerAvailable()
    {
        return nativeBannerView != null ? true : false;
    }

    public void HideAdmobNativeAd()
    {
        if (nativeBannerView != null)
        {
            nativeBannerView.Hide();
        }

    }


    public void ShowAdmobNativeAd()
    {
        if (nativeBannerView != null)
        {
            nativeBannerView.Show();
        }

    }


    public void DestroyAdmobNativeAd()
    {
        if (nativeBannerView != null)
        {
            nativeBannerView.Destroy();
        }

    }

    #endregion

    #region OPEN ADS
#if USE_OPENADS
    private bool IsAdAvailable
    {
        get
        {
            return openAd != null;
        }
    }


    public void RequestAndLoadOpenAd()
    {
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_OPEN_ADS;
#elif UNITY_IPHONE
        string adUnitId = IOS_OPEN_ADS;
#else
        string adUnitId = "unexpected_platform";
#endif

        if (!AdConstants.shouldDisplayAds() ||  IsOpenAdBeingRequested || !AdConstants.GetInternetStatus() || AdConstants.limitAds || string.IsNullOrEmpty(adUnitId))
            return;

        cur_AdType = AdController.AdType.OPEN_AD;

        Debug.Log("---------- REQUESTING OPEN ADS ----------");
        AdRequest request = new AdRequest.Builder().Build();

         IsOpenAdBeingRequested = true;
        // Load an app open ad for portrait orientation
        AppOpenAd.LoadAd(adUnitId, ScreenOrientation.Landscape, request, ((appOpenAd, error) =>
        {

        if (error != null)
        {
                MobileAdsEventExecutor.ExecuteInUpdate(() =>
                {
                    OnOpenAdFailedToLoadEvent.Invoke();
                });
                return;
                }

            // App open ad is loaded.
            openAd = appOpenAd;
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                OnOpenAdLoadedEvent.Invoke();

            });
        }));
    }


    public void ShowAdIfAvailable()
    {
        if (!AdConstants.shouldDisplayAds() ||  IsOpenAdBeingRequested || !AdConstants.GetInternetStatus() || AdConstants.limitAds)
            return;
        cur_AdType = AdController.AdType.OPEN_AD;

        if (!IsAdAvailable || AdConstants.IsAdWasShowing)
        {
            AdConstants.IsAdWasShowing = false;

            if (!IsAdAvailable)
            {
              
                StartCoroutine(LoadOpenAdWithDelay());

            }

        }
        else
        {

            IsOpenAdBeingRequested = false;

            openAd.OnAdDidDismissFullScreenContent += (sender, args) => OnOpenAdClosedEvent.Invoke();
            openAd.OnAdFailedToPresentFullScreenContent += (sender, args) => OnOpenAdFailedToShowEvent.Invoke();
            openAd.OnAdDidPresentFullScreenContent += (sender, args) => OnOpenAdOpeningEvent.Invoke();

            openAd.Show();
        }
    }


#endif
    public void Callback_OpenAdLoaded()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ OPEN AD LOADED -------");

        if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.OPEN_AD)
        {
            IncrementLoadIndex();
            CheckAndLoadAd();
        }
        IsOpenAdBeingRequested = false;
    }

    public void Callback_OpenAdLoadFailed()
    {
        // Handle the error.
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED OPEN AD -------");

        if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.OPEN_AD)
        {
            IncrementLoadIndex();
            CheckAndLoadAd();
        }
        IsOpenAdBeingRequested = false;
    }

    public void Callback_OpenAdAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING OPEN AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("OPEN AD");


        IsOpenAdBeingRequested = false;
    }

    public void Callback_OpenAdShowingFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING FAILED OPEN AD -------");

        IsOpenAdBeingRequested = false;
#if USE_OPENADS
        RequestAndLoadOpenAd();
#endif
    }

    public void Callback_OpenAdClosed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ CLOSED OPEN AD -------");

        StartCoroutine(LoadOpenAdWithDelay());
        IsOpenAdBeingRequested = false;
        AdConstants.IsAdWasShowing = false;
    }



    IEnumerator LoadOpenAdWithDelay()
    {
        yield return new WaitForSecondsRealtime(2f);
#if USE_OPENADS
        RequestAndLoadOpenAd();
#endif
    }



    #endregion

    #region STATIC ADS

    public void RequestAndLoadStaticlAd()
    {
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_Static;
#elif UNITY_IPHONE
        string adUnitId = IOS_Static;
#else
        string adUnitId = "unexpected_platform";
#endif


        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus() || AdConstants.limitAds || IsStaticAdBeingRequested || string.IsNullOrEmpty(adUnitId))
        {
            currentLoadIndex = 0;
            AdsLoadSequence = new List<AdLoadStates>();
            AdsLoadSequence.Add(AdLoadStates.NONE);
            return;
        }
        cur_AdType = AdController.AdType.STATIC;


        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING STATIC AD -------");

        IsStaticAdBeingRequested = true;

        // Clean up interstitial before using it
        if (staticAd != null)
        {
            staticAd.Destroy();
        }

        staticAd = new InterstitialAd(adUnitId);



        // Add Event Handlers
        staticAd.OnAdLoaded += (sender, args) => OnStaticAdLoadedEvent.Invoke();
        staticAd.OnAdFailedToLoad += (sender, args) => OnStaticAdFailedToLoadEvent.Invoke();
        staticAd.OnAdOpening += (sender, args) => OnStaticAdOpeningEvent.Invoke();
#if !USE_ADMOB_5_4_0
        staticAd.OnAdFailedToShow += (sender, args) => OnStaticAdFailedToShowEvent.Invoke();
#endif
        staticAd.OnAdClosed += (sender, args) => OnStaticAdClosedEvent.Invoke();
        if (sendAdPaidEvents)
        {
            staticAd.OnPaidEvent += (sender, args) =>
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                if (AdController.instance && AdController.instance.CanShowLogs())
                    Debug.Log("------ PAID EVENT StaticlAd -------");
#if USE_ADJUST_SDK
                AdValue adValue = args.AdValue;
                // send ad revenue info to Adjust
                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                Adjust.trackAdRevenue(adRevenue);
#endif
            });
        };
        }


        // Load an interstitial ad
        staticAd.LoadAd(CreateAdRequest());
    }


    public void Callback_StaticAdLoaded()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ STATIC AD LOADED -------");

        isStaticAdLoaded = true;
        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.STATIC)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}

        StaticAdLoaded();
        IsStaticAdBeingRequested = false;

    }

    public void Callback_StaticAdLoadFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED STATIC AD -------");


        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.STATIC)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}

        isStaticAdLoaded = true;


        IsStaticAdBeingRequested = false;

        StaticAdLoadFailed();
    }

    public void Callback_StaticAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING STATIC AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("STATIC");



        StaticAdShowing();

        IsStaticAdBeingRequested = false;
    }

    public void Callback_StaticAdShowingFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING FAILED STATIC AD -------");




        StaticAdShowingFailed();

        IsStaticAdBeingRequested = false;
    }

    public void Callback_StaticAdClosed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ CLOSED STATIC AD -------");


        StaticAdClosed();

        IsStaticAdBeingRequested = false;

        AdConstants.IsAdWasShowing = true;

        StartCoroutine(LoadStaticAdWithDelay());

    }



    public bool IsStaticAdIsAvailable()
    {
        return staticAd != null ? true : false;
    }


    public void ShowStaticAd(bool isShowLoading = false)
    {

        cur_AdType = AdController.AdType.STATIC;

        if (staticAd != null && staticAd.IsLoaded())
        {
            IsStaticAdBeingRequested = false;
#if USE_OPENADS
            AdConstants.IsAdWasShowing = true;

            Debug.Log("-------- Open Ad Showing Ad :" + AdConstants.IsAdWasShowing);
#endif


            staticAd.Show();
        }
        else
        {

            StartCoroutine(LoadStaticAdWithDelay());
            if (AdController.instance && CanShowLogs())
                Debug.Log("------ STATIC AD IS NOT READY YET -------");
        }
    }

    public void DestroyStaticAd()
    {
        if (staticAd != null)
        {
            staticAd.Destroy();
        }
    }

    IEnumerator LoadStaticAdWithDelay()
    {
        yield return new WaitForSecondsRealtime(2f);
        RequestAndLoadStaticlAd();
    }

    #endregion

    #region INTERSTITIAL ADS

    public void RequestAndLoadInterstitialAd()
    {
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_Interstitial;
#elif UNITY_IPHONE
        string adUnitId = IOS_Interstitial;
#else
        string adUnitId = "unexpected_platform";
#endif

        if (!AdConstants.shouldDisplayAds() || !AdConstants.GetInternetStatus() || AdConstants.limitAds || IsInterstitialAdBeingRequested || string.IsNullOrEmpty(adUnitId))
        {
            currentLoadIndex = 0;
            AdsLoadSequence = new List<AdLoadStates>();
            AdsLoadSequence.Add(AdLoadStates.NONE);
            return;
        }
        cur_AdType = AdController.AdType.INTERSTITIAL;


        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING INTERSITIAL AD -------");




        // Clean up interstitial before using it
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }


        IsInterstitialAdBeingRequested = true;

        interstitialAd = new InterstitialAd(adUnitId);

        // Add Event Handlers
        interstitialAd.OnAdLoaded += (sender, args) => OnInterstitialAdLoadedEvent.Invoke();
        interstitialAd.OnAdFailedToLoad += (sender, args) => OnInterstitialAdFailedToLoadEvent.Invoke();
        interstitialAd.OnAdOpening += (sender, args) => OnInterstitialAdOpeningEvent.Invoke();
#if !USE_ADMOB_5_4_0
        interstitialAd.OnAdFailedToShow += (sender, args) => OnInterstitialAdFailedToShowEvent.Invoke();
#endif
        interstitialAd.OnAdClosed += (sender, args) => OnInterstitialAdClosedEvent.Invoke();
        if (sendAdPaidEvents)
        {
            interstitialAd.OnPaidEvent += (sender, args) =>
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                if (AdController.instance && AdController.instance.CanShowLogs())
                    Debug.Log("------ PAID EVENT InterstitialAd -------");
#if USE_ADJUST_SDK
                AdValue adValue = args.AdValue;
                // send ad revenue info to Adjust
                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                Adjust.trackAdRevenue(adRevenue);
#endif
            });
        };
        }
        // Load an interstitial ad
        interstitialAd.LoadAd(CreateAdRequest());
    }


    public void Callback_InterstitialAdLoaded()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ INTERSTITIAL AD LOADED -------");

        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.INTERSTITIAL)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}

        isInterstitialLoaded = true;

        VideoAdLoaded();
        IsInterstitialAdBeingRequested = false;

    }

    public void Callback_InterstitialAdLoadFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED INTERSTITIAL AD -------");

        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.INTERSTITIAL)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}


        isInterstitialLoaded = true;

        IsInterstitialAdBeingRequested = false;

        VideoAdLoadFailed();
    }

    public void Callback_InterstitialAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING INTERSTITIAL AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("INTERSTITIAL");

        VideoAdShowing();

        IsInterstitialAdBeingRequested = false;
    }

    public void Callback_InterstitialAdShowingFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING FAILED INTERSTITIAL AD -------");

        VideoAdShowingFailed();

        IsInterstitialAdBeingRequested = false;
    }

    public void Callback_InterstitialAdClosed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ CLOSED INTERSTITIAL AD -------");



        VideoAdClosed();
        IsInterstitialAdBeingRequested = false;

        AdConstants.IsAdWasShowing = true;
        StartCoroutine(LoadInterstitialAdWithDelay());

    }


    public bool IsInterstitialAdIsAvailable()
    {
        return interstitialAd != null ? true : false;
    }


    public void ShowInterstitialAd(bool isShowLoading = false)
    {
        cur_AdType = AdController.AdType.INTERSTITIAL;

        if (interstitialAd != null && interstitialAd.IsLoaded())
        {
            IsInterstitialAdBeingRequested = false;

#if USE_OPENADS
            AdConstants.IsAdWasShowing = true;

            Debug.Log("-------- Open Ad Showing Ad :" + AdConstants.IsAdWasShowing);


#endif


            interstitialAd.Show();


        }
        else
        {


           StartCoroutine(LoadInterstitialAdWithDelay());
            if (AdController.instance && CanShowLogs())
                Debug.Log("------ INTERSTITIAL AD IS NOT READY YET -------");
        }
    }



    public void DestroyInterstitialAd()
    {
        if (interstitialAd != null)
        {
            interstitialAd.Destroy();
        }
    }



    IEnumerator LoadInterstitialAdWithDelay()
    {
        yield return new WaitForSecondsRealtime(2f);
        RequestAndLoadInterstitialAd();
    }


    #endregion

    #region REWARDED ADS

    public void RequestAndLoadRewardedAd()
    {
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
        string adUnitId = ANDROID_Rewarded;
#elif UNITY_IPHONE
        string adUnitId = IOS_Rewarded;
#else
        string adUnitId = "unexpected_platform";
#endif

        if (!AdConstants.GetInternetStatus() || AdConstants.limitAds || IsRewardedAdBeingRequested || string.IsNullOrEmpty(adUnitId))
        {
            currentLoadIndex = 0;
            AdsLoadSequence = new List<AdLoadStates>();
            AdsLoadSequence.Add(AdLoadStates.NONE);
            return;
        }
        cur_AdType = AdController.AdType.REWARDED;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING REWARDED AD -------");


        if (string.IsNullOrEmpty(adUnitId) || adUnitId.Equals("unexpected_platform"))
            return;


        if (rewardedAd != null)
            rewardedAd = null;

        IsRewardedAdBeingRequested = true;
        // create new rewarded ad instance
        rewardedAd = new RewardedAd(adUnitId);

        // Add Event Handlers
        rewardedAd.OnAdLoaded += (sender, args) => OnRewardedAdLoadedEvent.Invoke();
        rewardedAd.OnAdFailedToLoad += (sender, args) => OnRewardedAdFailedToLoadEvent.Invoke();
        rewardedAd.OnAdOpening += (sender, args) => OnRewardedAdOpeningEvent.Invoke();
        rewardedAd.OnAdFailedToShow += (sender, args) => OnRewardedAdFailedToShowEvent.Invoke();
        rewardedAd.OnAdClosed += (sender, args) => OnRewardedAdClosedEvent.Invoke();
        rewardedAd.OnUserEarnedReward += (sender, args) => OnRewardedAdEarnedRewardEvent.Invoke();
        if (sendAdPaidEvents)
        {
            rewardedAd.OnPaidEvent += (sender, args) =>
        {
            MobileAdsEventExecutor.ExecuteInUpdate(() =>
            {
                if (AdController.instance && AdController.instance.CanShowLogs())
                    Debug.Log("------ PAID EVENT RewardedAd -------");
#if USE_ADJUST_SDK
                AdValue adValue = args.AdValue;
                // send ad revenue info to Adjust
                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                Adjust.trackAdRevenue(adRevenue);
#endif
            });
        };

        }

        // Create empty ad request
        rewardedAd.LoadAd(CreateAdRequest());
    }


    public void Callback_RewardedAdLoaded()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REWARDED AD LOADED -------");

        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.REWARDED)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}

        isRewardedAdLoaded = true;
        RewardedAdLoaded();
        IsRewardedInterstitialAdBeingRequested = false;
    }

    public void Callback_RewardedAdLoadFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED REWARDED AD -------");


        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.REWARDED)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}
        isRewardedAdLoaded = true;


        RewardedAdLoadFailed();

        IsRewardedAdBeingRequested = false;

        AdConstants.isCallFromShow = false;
    }

    public void Callback_RewardedAdShowing()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING REWARDED AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("REWARDED");

        RewardedAdShowing();

        IsRewardedAdBeingRequested = false;

        AdConstants.isCallFromShow = false;
    }

    public void Callback_RewardedAdShowingFailed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING FAILED REWARDED AD -------");

        NoRewardedAdAvailable();
        RewardedAdShowingFailed();

        IsRewardedAdBeingRequested = false;
    }

  

    public void Callback_RewardedAdClosed()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ CLOSED REWARDED AD -------");

        //AdConstants.isRewardedAdClosed = true;

        IsRewardedAdBeingRequested = false;
        StartCoroutine(LoadRewardedAdWithDelay());

        if (loadAllRewardedAds)
        {
            loadAllRewardedAds = false;
            IsRewardedInterstitialAdBeingRequested = false;
            StartCoroutine(LoadRewardedInterstitialAdWithDelay());
        }

        //if (rewardMethod == RewardsMethod.OnGaveReward)
        //{
        //    if (!AdConstants.sawRewarded)
        //    {

        //        CancelRewardedAd();
        //    }
        //    AdConstants.sawRewarded = false;

        //}
        AdConstants.IsAdWasShowing = true;
        if (rewardMethod == RewardsMethod.Update)
            StartCoroutine(GaveRewardWithDelay());
        else 
            AdConstants.isRewardedAdClosed = true;
    }


    public void Callback_RewardedAdEarnedReward()
    {
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ WATCHED REWARDED AD ------");

        AdConstants.sawCompleteRewardedAd = true;

        AdConstants.sawRewarded = true;
        if (rewardMethod == RewardsMethod.OnGaveReward)
            StartCoroutine(DecideForReward());

    }

    



    public bool IsRewardedAdIsAvailable()
    {
        return rewardedAd != null ? true : false;
    }


    public void ShowRewardedAd()
    {
        AdConstants.isCallFromShow = true;
        cur_AdType = AdController.AdType.REWARDED;

        if (rewardedAd != null && rewardedAd.IsLoaded())
        {
            IsRewardedAdBeingRequested = false;

#if USE_OPENADS
            AdConstants.IsAdWasShowing = true;
#endif
            rewardedAd.Show();
        }
        else if (IsRewardedInterstitialAdIsAvailable())
        {

            loadAllRewardedAds = true;

            ShowAd(AdType.REWARDED_INTERSTITIAL);


        }
        else
        {
            showNoAdAvailable = false;
            showNoRewardedAdDialog();
            StartCoroutine(LoadRewardedAdWithDelay());
            if (AdController.instance && loadAllRewardedAds)
            {
                StartCoroutine(LoadRewardedInterstitialAdWithDelay());

            }
        }
    }


    IEnumerator LoadRewardedAdWithDelay()
    {
        yield return new WaitForSecondsRealtime(2f);
        RequestAndLoadRewardedAd();
    }


    IEnumerator LoadRewardedInterstitialAdWithDelay()
    {
        yield return new WaitForSecondsRealtime(2f);
        RequestAndLoadRewardedInterstitialAd();
        loadAllRewardedAds = false;

    }


    public void RequestAndLoadRewardedInterstitialAd()
    {
#if UNITY_EDITOR
        string adUnitId = "Unused";
#elif UNITY_ANDROID
            string adUnitId = ANDROID_Rewarded_Interstitial;
#elif UNITY_IPHONE
            string adUnitId = IOS_Rewarded_Interstitial;
#else
            string adUnitId = "unexpected_platform";
#endif

        if (!AdConstants.GetInternetStatus() || AdConstants.limitAds || IsRewardedInterstitialAdBeingRequested || string.IsNullOrEmpty(adUnitId))
        {
            currentLoadIndex = 0;
            AdsLoadSequence = new List<AdLoadStates>();
            AdsLoadSequence.Add(AdLoadStates.NONE);
            return;
        }
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REQUESTING REWARDED INTERSTITIAL AD -------");
        // These ad units are configured to always serve test ads.


        if (string.IsNullOrEmpty(adUnitId) || adUnitId.Equals("unexpected_platform"))
            return;


        IsRewardedInterstitialAdBeingRequested = true;


        RewardedInterstitialAd.LoadAd(adUnitId, CreateAdRequest(), RewardedInterstitialAdLoadCallback) ;

        #region Old Code
        //        // Create an interstitial.
        //        RewardedInterstitialAd.LoadAd(adUnitId, CreateAdRequest(), (rewardedInterstitialAd, error) =>
        //        {

        //            if (error != null)
        //            {
        //                MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //                {
        //                    isRewardedInterstitialLoaded = true;

        //                    OnRewardedInterstitialAdFailedToLoadEvent.Invoke();
        //                });
        //                return;
        //            }
        //            this.rewardedInterstitialAd = rewardedInterstitialAd;
        //            MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //            {
        //                isRewardedInterstitialLoaded = true;

        //                OnRewardedInterstitialAdLoadedEvent.Invoke();

        //            });

        //                // Register for ad events.
        //                this.rewardedInterstitialAd.OnAdDidPresentFullScreenContent += (sender, args) =>
        //                {

        //                    MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //                    {
        //                        OnRewardedInterstitialAdOpeningEvent.Invoke();
        //                    });
        //                };
        //                this.rewardedInterstitialAd.OnAdDidDismissFullScreenContent += (sender, args) =>
        //                {
        //                    MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //                    {
        //                        OnRewardedInterstitialAdClosedEvent.Invoke();
        //                    });

        //                };
        //                this.rewardedInterstitialAd.OnAdFailedToPresentFullScreenContent += (sender, args) =>
        //                {
        //                    MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //                    {
        //                        OnRewardedInterstitialAdFailedToShowEvent.Invoke();

        //                    });
        //                };

        //            this.rewardedInterstitialAd.OnPaidEvent += (sender, args) =>
        //            {

        //                MobileAdsEventExecutor.ExecuteInUpdate(() =>
        //                {
        //                    if (AdController.instance && AdController.instance.CanShowLogs())
        //                        Debug.Log("------ PAID EVENT Rewarded Interstitial Ad -------");
        //#if USE_ADJUST_SDK
        //                AdValue adValue = args.AdValue;
        //                // send ad revenue info to Adjust
        //                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
        //                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
        //                Adjust.trackAdRevenue(adRevenue);
        //#endif
        //                });
        //            };

        //            });
        #endregion

    }

    private void RewardedInterstitialAdLoadCallback(RewardedInterstitialAd ad, AdFailedToLoadEventArgs error)
    {
         if (error == null)
        {
            rewardedInterstitialAd = ad;
            rewardedInterstitialAd.OnAdFailedToPresentFullScreenContent += (sender, args) => OnRewardedInterstitialAdFailedToShowEvent.Invoke();
            rewardedInterstitialAd.OnAdDidPresentFullScreenContent += (sender, args) => OnRewardedInterstitialAdOpeningEvent.Invoke();
            rewardedInterstitialAd.OnAdDidDismissFullScreenContent += (sender, args) => OnRewardedInterstitialAdClosedEvent.Invoke();
            if (sendAdPaidEvents)
            {
                this.rewardedInterstitialAd.OnPaidEvent += (sender, args) =>
                {

                    MobileAdsEventExecutor.ExecuteInUpdate(() =>
                    {
                        if (AdController.instance && AdController.instance.CanShowLogs())
                            Debug.Log("------ PAID EVENT Rewarded Interstitial Ad -------");
#if USE_ADJUST_SDK
                                AdValue adValue = args.AdValue;
                                // send ad revenue info to Adjust
                                AdjustAdRevenue adRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
                                adRevenue.setRevenue(adValue.Value / 1000000f, adValue.CurrencyCode);
                                Adjust.trackAdRevenue(adRevenue);
#endif
                                });
                };
            }
            OnRewardedInterstitialAdLoadedEvent.Invoke();
   
        }
        else
        {

            OnRewardedInterstitialAdFailedToLoadEvent.Invoke();
        }
    }

    public void Callback_RewardedInterstitialAdLoaded()
    {
        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;
        if (AdController.instance && CanShowLogs())
            Debug.Log("------ REWARDED INTERSTITIAL AD LOADED -------");


        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.REWARDED_INTERSTITIAL)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}


        isRewardedInterstitialLoaded = true;

        IsRewardedInterstitialAdBeingRequested = false;

        RewardedInterstitialAdLoaded();
    }

    public void Callback_RewardedInterstitialAdLoadFailed()
    {
        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ LOAD FAILED REWARDED INTERSTITIAL AD -------");


        //if (checkForAdsSequenceIndex() && AdsLoadSequence[currentLoadIndex] == AdLoadStates.REWARDED_INTERSTITIAL)
        //{
        //    IncrementLoadIndex();
        //    CheckAndLoadAd();
        //}


        isRewardedInterstitialLoaded = true;

        IsRewardedInterstitialAdBeingRequested = false;


        RewardedInterstitialAdLoadFailed();
    }

    public void Callback_RewardedInterstitialAdShowing()
    {

        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING REWARDED INTERSTITIAL AD -------");


        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("REWARDED_INTERSTITIAL");

        RewardedInterstitialAdShowing();
        IsRewardedInterstitialAdBeingRequested = false;
        AdConstants.isCallFromShow = false;

    }

    public void Callback_RewardedInterstitialAdShowingFailed()
    {
        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ SHOWING FAILED REWARDED INTERSTITIAL AD -------");

        NoRewardedAdAvailable();
        RewardedInterstitialAdShowingFailed();
        IsRewardedInterstitialAdBeingRequested = false;

        this.rewardedInterstitialAd = null;
    }



    public void Callback_RewardedInterstitialAdClosed()
    {
        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;


        if (AdController.instance && CanShowLogs())
            Debug.Log("------ CLOSED REWARDED INTERSTITIAL AD -------");


        IsRewardedInterstitialAdBeingRequested = false;
        StartCoroutine(LoadRewardedInterstitialAdWithDelay());

        if (loadAllRewardedAds)
        {
            loadAllRewardedAds = false;
            IsRewardedAdBeingRequested = false;
            StartCoroutine(LoadRewardedAdWithDelay());

        }

        //if (rewardMethod == RewardsMethod.OnGaveReward)
        //{
        //    if (!AdConstants.sawRewarded)
        //    {

        //        CancelRewardedAd();
        //    }

        //    AdConstants.sawRewarded = false;

        //}
        AdConstants.IsAdWasShowing = true;

        this.rewardedInterstitialAd = null;
        if (rewardMethod == RewardsMethod.Update)
            StartCoroutine(GaveRewardWithDelay());
        else
            AdConstants.isRewardedAdClosed = true;
    }


    public void Callback_RewardedInterstitialAdEarnedReward(Reward reward)
    {
        // Give Rewarded Here
        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ WATCHED REWARDED INTERSTITIAL AD ------");

        AdConstants.sawCompleteRewardedAd = true;

        AdConstants.sawRewarded = true;
        if (rewardMethod == RewardsMethod.OnGaveReward)
        {
            StartCoroutine(DecideForReward());
        }

    }

    IEnumerator GaveRewardWithDelay()
    {
        yield return new WaitForSecondsRealtime(2f);
        AdConstants.isRewardedAdClosed = true;
    }

    IEnumerator DecideForReward()
    {

        yield return new WaitForSecondsRealtime(1f);
        if (AdConstants.isRewardedAdClosed)
        {
            if (AdConstants.sawRewarded)
            {
                GaveReward();
                AdConstants.sawRewarded = false;
            }
            else
            {

                CancelRewardedAd();
                AdConstants.sawRewarded = false;
            }

            AdConstants.isRewardedAdClosed = false;
        }

    }

    public bool IsRewardedInterstitialAdIsAvailable()
    {
        return rewardedInterstitialAd != null;
    }
    bool isGaveReward = false;
    [HideInInspector]
    public bool showNoAdAvailable = false;
    public void ShowRewardedInterstitialAd()
    {
        cur_AdType = AdController.AdType.REWARDED_INTERSTITIAL;
        AdConstants.isCallFromShow = true;

        if (rewardedInterstitialAd != null)
        {
            IsRewardedInterstitialAdBeingRequested = false;

#if USE_OPENADS
            AdConstants.IsAdWasShowing = true;
#endif
            rewardedInterstitialAd.Show(Callback_RewardedInterstitialAdEarnedReward);
        }
        else if (IsRewardedAdIsAvailable())
        {

            loadAllRewardedAds = true;

            ShowAd(AdType.REWARDED);

        }
        else
        {
            showNoAdAvailable = false;
            showNoRewardedAdDialog();
            StartCoroutine(LoadRewardedInterstitialAdWithDelay());
            if (AdController.instance && loadAllRewardedAds)
            {
                StartCoroutine(LoadRewardedAdWithDelay());
                loadAllRewardedAds = false;

            }
        }
    }


    public void showNoRewardedAdDialog()
    {
        StopCoroutine(NoRewardedAdCoroutine());
        StartCoroutine(NoRewardedAdCoroutine());

    }

    System.Collections.IEnumerator NoRewardedAdCoroutine()
    {
        yield return new WaitForSecondsRealtime(0.1f);

        if (!showNoAdAvailable)
            NoRewardedAdAvailable();


        showNoAdAvailable = true;
    }

    ////////////////////////////////// NO RW AD ////////////////////////////////
    public void NoRewardedAdAvailable()
    {
        if (FinzAnalysisManager.Instance)
            FinzAnalysisManager.Instance.AdAnalysis("NO_REWARD");

        if (AdController.instance && CanShowLogs())
            Debug.Log("------ NO REWARDED AD AVAILABLE -------");


        // No Reward code Here

        NoReward();
    }



#region Ads Loading

    private void OnEnable()
    {
        AdController.adsLoadingUIMethod += OnAdLoadingComplete;
    }

    private void OnDisable()
    {
        AdController.adsLoadingUIMethod -= OnAdLoadingComplete;

    }

    public void OnAdLoadingComplete()
    {
        switch (cur_AdType)
        {
            case AdController.AdType.STATIC:

                if (AdController.instance && CanShowLogs())
                    Debug.Log("------ Loading Complete For STATIC AD -------");


                ShowStaticAd();

                break;


            case AdController.AdType.INTERSTITIAL:

                if (AdController.instance && CanShowLogs())
                    Debug.Log("------ Loading Complete For INTERSTITIAL AD -------");


                ShowInterstitialAd();

                break;
        }

    }


#endregion


#endregion

#region Native Area

    public void ShowToast(string message)
    {
        ShowToast(message);
    }
    AndroidJavaObject currentActivity;
    AndroidJavaClass Toast;
    public void InitNativeClass()
    {
#if SHOW_NATIVE_TOAST && !UNITY_EDITOR
        //currentActivity androidjavaobject must be assigned for toasts to access currentactivity;
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
#endif

    }
    public void ShowToast(string message, bool isLongToast = true)
    {

#if SHOW_NATIVE_TOAST && !UNITY_EDITOR
#if UNITY_ANDROID
        if (Toast != null)
            return;

        if (!isLongToast)
        {
            AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
            Toast = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", message);
            AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_SHORT"));
            toast.Call("show");
            Toast = null;
        }
        else
        {
            AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
            Toast = new AndroidJavaClass("android.widget.Toast");
            AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", message);
            AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>("LENGTH_LONG"));
            toast.Call("show");
            Toast = null;

        }
#elif UNITY_IOS

        NativeMessage msg = new NativeMessage("Alert", message);
#endif


#endif
    }



#endregion

#endregion




}

