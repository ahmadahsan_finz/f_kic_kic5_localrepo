﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if USE_FIREBASE
using Firebase.Analytics;
#endif
#if USE_FB_ANALYTICS
using Facebook.Unity;
#endif

using UnityEngine.Analytics;

public class FinzAnalysisManager : MonoBehaviour
{

    public static FinzAnalysisManager Instance;


    bool isFirstTime = true;

    public enum AnalyticsType
    {
        UNITY,
        FIREBASE,
        FACEBOOK,
        NONE
    }

    public AnalyticsType analyticsType;

    private string internetStatus;
    public string InternetStatus { get => internetStatus; set => internetStatus = value; }

    private string workingInternetType;
    public string WorkingInternetString { get => workingInternetType; set => workingInternetType = value; }

    private void Awake()
    {
        Instance = this;

        DontDestroyOnLoad(this.gameObject);
    }
    void Start()
    {
        try
        {
            firebaseAnalysis();

            facebookAnalysis();

            TenjinConnect();
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

#if !USE_FIREBASE
        analyticsType = AnalyticsType.UNITY;
#endif

    }

 



    void facebookAnalysis()
    {
#if USE_FB_ANALYTICS
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
#endif
    }

    void firebaseAnalysis()
    {
#if USE_FIREBASE && UNITY_IOS
        try
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;



                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);

                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    //   app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                }
                else
                {
                    Debug.LogError(System.String.Format(
                        "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

#endif
    }

    private void InitCallback()
    {
#if USE_FB_ANALYTICS
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
#endif
    }

    public void AdAnalysis(string adType)
    {

        switch (analyticsType)
        {
            case AnalyticsType.FIREBASE:
#if USE_FIREBASE
         
                try
                {
                    FirebaseAnalytics.LogEvent(
                          "Ads_Analysis",
                     new Parameter("Ads", adType)
                     );
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }

                Analytics.CustomEvent("Ads_Analysis", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,adType
                    } }
                );

#endif
                break;

            case AnalyticsType.UNITY:

                Analytics.CustomEvent("Ads_Analysis", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,adType
                    } }
                );

                break;

            case AnalyticsType.NONE:

                Debug.Log("No analytics sent");

                break;
        }

        if (isFirstTime)
        {
            if (!string.IsNullOrEmpty(workingInternetType) && !string.IsNullOrEmpty(internetStatus))
            {
                Debug.Log(internetStatus + " " + workingInternetType);
                InternetAnalysis(internetStatus + " " + workingInternetType);
            }
            else
            {
                if (!string.IsNullOrEmpty(workingInternetType))
                {
                    Debug.Log("Sending Internet Analytics with " + workingInternetType);
                    InternetAnalysis(workingInternetType);
                }
                if (!string.IsNullOrEmpty(internetStatus))
                {
                    Debug.Log("Sending Internet Analytics with " + internetStatus);
                    InternetAnalysis(internetStatus + " " + workingInternetType);
                }
            }
            GameVersion();
            isFirstTime = false;
        }

    }


    // Internet Analysis
    public void InternetAnalysis(string logString)
    {

        switch (analyticsType)
        {
            case AnalyticsType.FIREBASE:
#if USE_FIREBASE

                try
                {
                    FirebaseAnalytics.LogEvent(
                          "Internet_Analysis",
                     new Parameter("Internet", logString)
                     );
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }

                Analytics.CustomEvent("Internet_Analysis", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,logString
                    } }
                );

#endif
                break;

            case AnalyticsType.UNITY:

                Analytics.CustomEvent("Internet_Analysis", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,logString
                    } }
                );

                break;

            case AnalyticsType.NONE:

                Debug.Log("No analytics sent");

                break;
        }

    }


    public void miniEvents(string eventType)
    {

        switch (analyticsType)
        {
            case AnalyticsType.FIREBASE:
#if USE_FIREBASE
       
                try
                {
                    FirebaseAnalytics.LogEvent(
                              "Mini_Events",
                         new Parameter("Events", eventType)
                         );
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }

#endif
                break;

            case AnalyticsType.UNITY:

                Analytics.CustomEvent("Mini_Events", new Dictionary<string, object> {
                    {
                        "Events" ,eventType
                    } }
                );

                break;

            case AnalyticsType.NONE:

                Debug.Log("No analytics sent");

                break;
        }


    }

    public void AdStates(string adType)
    {
        Analytics.CustomEvent("Ads_States", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,adType
                    } }
                );
    }

    public void CrossPromoAds(string adType, string status)
    {
        Analytics.CustomEvent("CrossPromotions", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,adType+"_"+status
                    } }
                );
    }


    public void GameVersion()
    {
#if USE_FIREBASE
        try
        {
            FirebaseAnalytics.LogEvent("Game_Version", new Parameter("Version", "V_" + Application.version.ToString()));

            isFirstTime = false;
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

#endif
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }


    public void LevelStartAnalysis(string level)
    {
        switch (analyticsType)
        {
            case AnalyticsType.FIREBASE:
#if USE_FIREBASE
         
                try
                {

                    FirebaseAnalytics.LogEvent(
                          "Levels",
                     new Parameter("Started", level.ToString())
                     );
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
#endif
                break;

            case AnalyticsType.UNITY:

                AddUnityEvent("level_start", new Dictionary<string, object>{
                { "level_index", ""+level}
                });

                break;

            case AnalyticsType.NONE:

                Debug.Log("No analytics sent");

                break;
        }
    }

    public void LevelCompleteAnalysis(string level)
    {
        switch (analyticsType)
        {
            case AnalyticsType.FIREBASE:
#if USE_FIREBASE
      
                try
                {
                    FirebaseAnalytics.LogEvent(
                          "Levels",
                     new Parameter("Completed", level)
                     );
                }
                catch (Exception e)
                {
                    Debug.Log(e);
                }
#endif
                break;

            case AnalyticsType.UNITY:

                AddUnityEvent("level_complete", new Dictionary<string, object>{
                { "level_index", ""+level}
                });

                break;

            case AnalyticsType.NONE:

                Debug.Log("No analytics sent");

                break;
        }
        

    }

    void AddUnityEvent(string EventName, IDictionary<string, object> dictionary)
    {
        Analytics.CustomEvent("" + EventName, dictionary);
        //  Debug.Log(" EventName " + EventName);
    }

    public void TenjinConnect()
    {
#if USE_TENJIN
        BaseTenjin instance = Tenjin.getInstance("XXXPFH5FVBDAGYYNG1C4W8D9FNTXKXWJ");

        instance.SetAppStoreType(AppStoreType.other);

        // Sends install/open event to Tenjin
        instance.Connect();
#endif
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (!pauseStatus)
        {
            TenjinConnect();
        }
    }



}
