﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PluginCallbacksTest : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(this);
    }
    private void OnEnable()
    {
        AdController.gaveRewardMethod += EarnedReward;
        AdController.loadNextScene += LoadScene;

        //AdController.noRewardedVideoMethod += NoRewardedVideo; //AdController.noVideoDialogMethod += noVideoDialog;
        AdController.cancelRewardedAdMethod += CancelRewardedAd;

        AdController.rewardedAdLoadMethod += RewardedAdLoaded;
        AdController.rewardedAdLoadFailedMethod += RewardedAdLoadFailed;
        AdController.rewardedAdShowingMethod += ShowingRewardedAd;
        AdController.rewardedAdShowingFailedMethod += ShowingFailedRewardedAd;

        AdController.rewardedInterstitialAdLoadMethod += RewardedInterstitialAdLoaded;
        AdController.rewardedInterstitialAdLoadFailedMethod += RewardedInterstitialAdLoadFailed;
        AdController.rewardedInterstitialAdShowingMethod += ShowingInterstitialRewardedAd;
        AdController.rewardedInterstitialAdShowingFailedMethod += ShowingFailedRewardedInterstitialAd;

        AdController.reviewDialogMethod += promptReviewDialog;

        AdController.bannerLoadingSuccessfulMethod += BannerAdLoadingSuccessful;
        AdController.bannerLoadingFailedMethod += BannerAdFailedToLoad;

        AdController.adaptiveBannerLoadingSuccessfulMethod += AdaptiveBannerAdLoadingSuccessful;
        AdController.adaptiveBannerLoadingFailedMethod += AdaptiveBannerAdFailedToLoad;


        AdController.nativeBannerShowMethod += NativeBannerAdLoadingSuccessful;


        AdController.nativeBannerLoadingFailedMethod += NativeBannerAdFailedToLoad;

        AdController.staticAdLoadMethod += StaticAdLoaded;
        AdController.staticAdLoadFailedMethod += StaticAdLoadFailed;
        AdController.staticAdShowingMethod += StaticAdShowing;
        AdController.staticAdShowingFailedMethod += StaticAdShowingFailed;
        AdController.staticAdClosedMethod += StaticAdClosed;

        AdController.videoAdLoadMethod += VideoAdLoaded;
        AdController.videoAdLoadFailedMethod += VideoAdLoadFailed;
        AdController.videoAdShowingMethod += VideoAdShowing;
        AdController.videoAdShowingFailedMethod += VideoAdShowingFailed;
        AdController.videoAdClosedMethod += VideoAdClosed;

        //AdController.crossPromotiongMethod += ShowCrossPromotions;
    }

    private void OnDisable()
    {
        AdController.gaveRewardMethod -= EarnedReward;
        AdController.loadNextScene -= LoadScene;

        //AdController.noRewardedVideoMethod -= NoRewardedVideo;  //    AdController.noVideoDialogMethod -= noVideoDialog;
        AdController.cancelRewardedAdMethod -= CancelRewardedAd;
        AdController.rewardedAdShowingMethod -= ShowingRewardedAd;
        AdController.rewardedAdShowingFailedMethod -= ShowingFailedRewardedAd;
        AdController.rewardedAdLoadMethod -= RewardedAdLoaded;
        AdController.rewardedAdLoadFailedMethod -= RewardedAdLoadFailed;

        AdController.reviewDialogMethod -= promptReviewDialog;

        AdController.bannerLoadingSuccessfulMethod -= BannerAdLoadingSuccessful;
        AdController.bannerLoadingFailedMethod -= BannerAdFailedToLoad;

        AdController.adaptiveBannerLoadingSuccessfulMethod -= AdaptiveBannerAdLoadingSuccessful;
        AdController.adaptiveBannerLoadingFailedMethod -= AdaptiveBannerAdFailedToLoad;

        AdController.nativeBannerShowMethod -= NativeBannerAdLoadingSuccessful;
        AdController.nativeBannerLoadingFailedMethod -= NativeBannerAdFailedToLoad;

        AdController.staticAdLoadMethod -= StaticAdLoaded;
        AdController.staticAdLoadFailedMethod -= StaticAdLoadFailed;
        AdController.staticAdShowingMethod -= StaticAdShowing;
        AdController.staticAdShowingFailedMethod -= StaticAdShowingFailed;
        AdController.staticAdClosedMethod -= StaticAdClosed;

        AdController.videoAdLoadMethod -= VideoAdLoaded;
        AdController.videoAdLoadFailedMethod -= VideoAdLoadFailed;
        AdController.videoAdShowingMethod -= VideoAdShowing;
        AdController.videoAdShowingFailedMethod -= VideoAdShowingFailed;
        AdController.videoAdClosedMethod -= VideoAdClosed;

        //AdController.crossPromotiongMethod -= ShowCrossPromotions;

    }

    public void EarnedReward()
    {
        if (AdController.instance && AdController.instance.CanShowLogs())
            Debug.Log("Earned Reward");
    }


    public void CancelRewardedAd()
    {
        Debug.Log("Cancelled rewarded ad");

    }

    public void ShowingRewardedAd()
    {
        Debug.Log("Showing rewarded ad");

    }


    public void ShowingFailedRewardedAd()
    {
        Debug.Log("Showing Failed rewarded ad");

    }


    public void RewardedAdLoaded()
    {
        Debug.Log("rewarded ad loaded");

    }


    public void RewardedAdLoadFailed()
    {
        Debug.Log("Fail to load rewarded ad");

    }


    public void ShowingInterstitialRewardedAd()
    {
        Debug.Log("Showing rewarded Interstitial ad");

    }


    public void ShowingFailedRewardedInterstitialAd()
    {
        Debug.Log("Showing Failed rewarded Interstitial ad");

    }


    public void RewardedInterstitialAdLoaded()
    {
        Debug.Log("rewarded Interstitial ad loaded");

    }


    public void RewardedInterstitialAdLoadFailed()
    {
        Debug.Log("Fail to load rewarded Interstitial ad");

    }



    public void BannerAdFailedToLoad()
    {
        Debug.Log("banner failed to load");
        if (AdController.instance)
            AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.BANNER);

    }

    public void AdaptiveBannerAdFailedToLoad()
    {
        Debug.Log("adaptive banner failed to load");
        if (AdController.instance)
            AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.ADAPTIVE);

    }

    public void NativeBannerAdFailedToLoad()
    {
        Debug.Log("native banner failed to load");
        if (AdController.instance)
            AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.NATIVE);

    }

    public void BannerAdLoadingSuccessful()
    {
        Debug.Log("banner load successful");

    }
    public void AdaptiveBannerAdLoadingSuccessful()
    {
        Debug.Log("adaptive banner load successful");

    }

    public void NativeBannerAdLoadingSuccessful()
    {
        Debug.Log("native banner load successful");

    }


    public void StaticAdLoaded()
    {
        Debug.Log("static ad Loaded Successfully");

    }

    public void StaticAdLoadFailed()
    {
        Debug.Log("static ad Load Failed");

    }

    public void StaticAdShowing()
    {
        Debug.Log("static ad Showing Successfully");

    }

    public void StaticAdShowingFailed()
    {
        Debug.Log("static ad Showing Failed");

    }

    public void StaticAdClosed()
    {
        Debug.Log("static ad closed");

    }


    public void VideoAdLoaded()
    {
        Debug.Log("video ad Loaded Successfully");

    }

    public void VideoAdLoadFailed()
    {
        Debug.Log("video ad Load Failed");

    }


    public void VideoAdShowing()
    {
        Debug.Log("video ad Showing Successfully");

    }

    public void VideoAdShowingFailed()
    {
        Debug.Log("video ad Showing Failed");

    }


    public void VideoAdClosed()
    {
        Debug.Log("video ad closed");
    }


    public void NoRewardedVideo()
    {
        // will ad gave reward logic here.
        Debug.Log("=========================================================== PLUGIN No Rewards===========================================================");


    }

    public void promptReviewDialog()
    {
        if (AdConstants.shouldDisplayRateMenu())
        {
            Debug.Log("Rate event called");
            if (Screen.orientation == ScreenOrientation.Portrait)
                Instantiate(Resources.Load("PotraitNativeAndroidRateDialogMenu"));
            else
                Instantiate(Resources.Load("NativeAndroidRateDialogMenu"));

            if (AdController.instance)
                AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.NATIVE);
        }

    }


    public void ShowCrossPromotions()
    {
        Debug.Log("Show Customized Cross Promotion Here");
    }

    public void LoadScene()
    {
        if (SceneManager.sceneCountInBuildSettings > 1)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
