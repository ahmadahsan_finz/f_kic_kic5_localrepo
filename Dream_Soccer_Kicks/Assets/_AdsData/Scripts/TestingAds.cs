﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class TestingAds : MonoBehaviour
{
    public Text debugText;
    Vector2 textSize;
    void OnEnable()
    {
        if (debugText) textSize = debugText.gameObject.GetComponent<RectTransform>().sizeDelta;
        Application.logMessageReceived += ShowLogsOnText;

    }

    void OnDisable()
    {
        Application.logMessageReceived -= ShowLogsOnText;
     

    }

    public void ShowBanner()
    {
        if (AdController.instance)
            AdController.instance.ShowBannerAd();//(AdController.BannerAdTypes.BANNER);

    }
    public void HideBanner()
    {
        if (AdController.instance)
            AdController.instance.HideBannerAd();//(AdController.BannerAdTypes.BANNER);
    }

    public void DestroyBanner()
    {
        if (AdController.instance)
            AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.BANNER);//(AdController.BannerAdTypes.BANNER);
    }

    public void ShowAdaptiveBanner()
    {
        if (AdController.instance)
            AdController.instance.ShowAdaptiveBannerAd();
    }

    public void DestroyAdaptiveBanner()
    {
        if (AdController.instance)
            AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.ADAPTIVE);//(AdController.BannerAdTypes.ADAPTIVE);
    }

    public void HideAdaptiveBanner()
    {
        if (AdController.instance)
            AdController.instance.HideAdaptiveBannerAd();//(AdController.BannerAdTypes.ADAPTIVE);
    }

    public void ShowNativeBanner()
    {
        if (AdController.instance)
            AdController.instance.showNativeBannerAd();// (AdController.BannerAdTypes.NATIVE);
    }



    public void HideNativeBanner()
    {
        if (AdController.instance)
            AdController.instance.hideNativeBannerAd();//(AdController.BannerAdTypes.NATIVE);
    }


    public void DestroyNativeBanner()
    {
        if (AdController.instance)
            AdController.instance.DestroyBannerAd(AdController.BannerAdTypes.NATIVE);//(AdController.BannerAdTypes.NATIVE);
    }

    public void RequestStaticAd()
    {
        if (AdController.instance) 
            AdController.instance.LoadAd(AdController.AdType.STATIC);
            
    }

    public void ShowStaticAd()
    {
        if (AdController.instance)
        {
            //AdController.instance.ShowAd(AdController.AdType.STATIC);
            AdConstants.currentState = AdConstants.States.OnPause;
            AdController.instance.ChangeState();
        }
    }

    public void RequestInterstitilAd()
    {
        if (AdController.instance)
            AdController.instance.LoadAd(AdController.AdType.INTERSTITIAL);
    }

    public void ShowInterstitilAd()
    {
        if (AdController.instance)
        {
            //AdController.instance.ShowAd(AdController.AdType.STATIC);
            AdConstants.currentState = AdConstants.States.OnGameOver;
            AdController.instance.ChangeState();
        }
    }


    public void RequestRewardedAd()
    {
        if (AdController.instance && !AdController.instance.IsRewardedAdAvailable())
            AdController.instance.LoadAd(AdController.AdType.REWARDED);
    }

    public void ShowRewardedAd()
    {
        if (AdController.instance)
        {
            AdConstants.currentState = AdConstants.States.OnRewarded;
            AdController.instance.ChangeState();
        }
    }


    public void RequestRewardedInterstitialAd()
    {
        if (AdController.instance)
        {
            AdController.instance.LoadAd(AdController.AdType.REWARDED_INTERSTITIAL);
      
        }
    }

    public void ShowRewardedInterstitialAd()
    {
        if (AdController.instance)
        {
            AdConstants.currentState = AdConstants.States.OnRewardedInterstitial;
            AdController.instance.ChangeState();
        }
    }


    public void ShowNaiveToast(string msg)
    {
        if (AdController.instance)
            AdController.instance.ShowNativeToast(msg);
    }

    public void ShowRateUs(bool inGame)
    {
        if (AdController.instance)
            AdController.instance.PromptRateMenu(inGame);
    }



    public void ShowLogsOnText(string logString, string stackTrace, LogType type)
    {
        if (debugText)
            debugText.text = "\n======================================\n"+logString;
    }
  

}
