﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ApptrackingEditorMenu : MonoBehaviour
{
    [MenuItem("App Tracking/Create App Tracking Prefab")]
    private static void CreateAPptracking()
    {
        bool hasEventSystem = false;
        List<GameObject> allObjects = new List<GameObject>();
        allObjects.AddRange(UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects());
        GameObject parentObject = allObjects.Find(x=>x.gameObject.name == "SceneObjects");
        if (parentObject == null)
            parentObject = new GameObject("SceneObjects");
          
        if (allObjects.Count > 0)
        {

            foreach (GameObject go in allObjects)
            {
                if (!go.GetComponent<UnityEngine.EventSystems.EventSystem>() && !go.name.Equals("AppTrackingMenu"))
                    go.transform.SetParent(parentObject.transform);
                else if (go.GetComponent<UnityEngine.EventSystems.EventSystem>())
                    hasEventSystem = true;
            }
            parentObject.gameObject.SetActive(false);
        }

    
        ApptrackingPrompt appTrackingScript;
        foreach (GameObject go in allObjects)
        {
            appTrackingScript = go.transform.GetComponentInChildren<ApptrackingPrompt>();
             if (go.name.Equals("AppTrackingMenu")|| appTrackingScript != null)
            {
                if(!hasEventSystem)
                {
                    GameObject EventsystemObject = new GameObject("EventSystem");
                    EventsystemObject.AddComponent<UnityEngine.EventSystems.EventSystem>();
                    EventsystemObject.AddComponent<UnityEngine.EventSystems.StandaloneInputModule>();
                }

                appTrackingScript.SceneLoaderObject = parentObject;
                return;
            }
        }

        GameObject appTrackingMenu = Instantiate(Resources.Load("Apptracking_Prefabs/AppTrackingMenu")) as GameObject;
        appTrackingMenu.transform.SetAsLastSibling();
        appTrackingMenu.name = "AppTrackingMenu";
        appTrackingScript = appTrackingMenu.GetComponentInChildren<ApptrackingPrompt>();
        if(appTrackingScript != null)
        {
            appTrackingScript.SceneLoaderObject = parentObject;
        }
        else
        {
            Debug.LogError("App Tracking Prompt Script not Found!");
        }
    }
  
    
}
