using System.Collections.Generic;
using Balaso;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ApptrackingPrompt : MonoBehaviour
{
    public GameObject mainObject;
    [HideInInspector]
    public GameObject SceneLoaderObject;
    public Button btn_Ok;

    private void OnEnable()
    {
#if UNITY_ANDROID
MakeAllChildsIndependant();
Destroy(mainObject.transform.parent.gameObject);
        return;
#endif
        AppTrackingTransparency.OnAuthorizationRequestDone += StartLoadingScene;

        AppTrackingTransparency.AuthorizationStatus currentStatus = AppTrackingTransparency.TrackingAuthorizationStatus;
        Debug.Log(string.Format("Current authorization status: {0}", currentStatus.ToString()));
        if (currentStatus == AppTrackingTransparency.AuthorizationStatus.NOT_DETERMINED)
        {
            btn_Ok.onClick.RemoveAllListeners();
            btn_Ok.onClick.AddListener(OkButtonPressed);

        }
        else
        {
            MakeAllChildsIndependant();
            Destroy(mainObject.transform.parent.gameObject);
            return;
        }


    }

    private void OnDisable()
    {
        AppTrackingTransparency.OnAuthorizationRequestDone -= StartLoadingScene;

    }



    private void MakeAllChildsIndependant()
    {
        List<GameObject> allChilds = new List<GameObject>();
        for(int i = 0; i < SceneLoaderObject.transform.childCount; i++)
        {
            allChilds.Add(SceneLoaderObject.transform.GetChild(i).gameObject);
        }

        foreach(GameObject go in allChilds)
        {
            go.transform.SetParent(null);
        }


    }

    public void OkButtonPressed()
    {

#if UNITY_EDITOR
        StartLoadingScene(AppTrackingTransparency.AuthorizationStatus.AUTHORIZED);
#elif UNITY_IOS
            AppTrackingTransparency.RequestTrackingAuthorization();
#elif UNITY_ANDROID
MakeAllChildsIndependant();
#endif
        mainObject.SetActive(false);

    }

    private void StartLoadingScene(AppTrackingTransparency.AuthorizationStatus status)
    {
#if UNITY_IOS
        switch (status)
        {
            case AppTrackingTransparency.AuthorizationStatus.NOT_DETERMINED:
                Debug.Log("Custom AuthorizationStatus: NOT_DETERMINED");
#if USE_FACEBOOK
                AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
#endif
                break;
            case AppTrackingTransparency.AuthorizationStatus.RESTRICTED:
#if USE_FACEBOOK

                AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
#endif
                Debug.Log("Custom AuthorizationStatus: RESTRICTED");
                break;
            case AppTrackingTransparency.AuthorizationStatus.DENIED:
#if USE_FACEBOOK

                AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(false);
#endif
                Debug.Log("Custom AuthorizationStatus: DENIED");
                break;
            case AppTrackingTransparency.AuthorizationStatus.AUTHORIZED:
#if USE_FACEBOOK

                AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
#endif
                Debug.Log("Custom AuthorizationStatus: AUTHORIZED");
                break;
        }

        appTracking(status.ToString());
        MakeAllChildsIndependant();

        // Obtain IDFA
        Debug.Log(string.Format("IDFA: {0}", AppTrackingTransparency.IdentifierForAdvertising()));

        Destroy(mainObject.transform.parent.gameObject);
#endif
        }

        void appTracking(string condition)
    {
        Analytics.CustomEvent("AppTracking", new Dictionary<string, object> {
                    {
                        Application.version.ToString() ,condition
                    } }
                );
    }

}
